//
//  loginViewController.m
//  Database
//
//  Created by User on 30.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import "loginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "DBManager.h"
#import "webViewController.h"
#import "StructureOverViewController.h"
#import "PlaceOverViewController.h"
#import "NavigationViewController.h"
#import "SWRevealViewController.h"
#import "SSKeychain.h"
#import "LoadingInformationViewController.h"
#import "RegisterViewController.h"

#import "MBProgressHUD.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "Login.h"
#import "Message.h"
#import "StartTabBarViewController.h"

@interface loginViewController () <loadingInformationProtocol>
@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic) NSArray* inputViewConstraints;

@property (nonatomic) UIView* statusbarBackground;

@end

@implementation loginViewController

@synthesize passwordTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self registerForKeyboardNotifications];
    
    // initialising the db manager
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    
    // for moving view when keyboard is visible
    self.originalCenter = self.inputView.center;
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    self.passwordTextField.placeholder = NSLocalizedString(@"Password",@"");
    self.usernameTextField.placeholder = NSLocalizedString(@"Email address", @"");
    
    [self.forgotPasswordButton setTitle:NSLocalizedString(@"Forgot password?",@"") forState:UIControlStateNormal];
    [self.loginButton setTitle:NSLocalizedString(@"Login", @"") forState:UIControlStateNormal];
    
    // inputs
    self.originalCenter = self.inputView.center;
    
    self.passwordTextField.secureTextEntry = YES;
    
    self.textInputView.layer.cornerRadius = 5.0f;
    self.textInputView.layer.borderWidth = 1.0f;
    self.textInputView.layer.borderColor = [[UIColor colorWithRGBHex:0xcccccc] CGColor];
    
    self.loginButtonView = [self roundCornersOnView:self.loginButtonView onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:5.0f];
    self.loginButtonView.backgroundColor = [UIColor colorWithRGBHex:0x000000];
    
    self.headerImage.tintColor = [UIColor darkGrayColor];
    
    self.navigationItem.title = NSLocalizedString(@"Login",@"");
    
    [self.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundClicked:)]];
    [self.inputView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundClicked:)]];
    
    [self.linkedInButton setTitle:NSLocalizedString(@"LI_button_login", @"Login with LinkedIn") forState:UIControlStateNormal];
    [self.facebookButton setTitle:NSLocalizedString(@"FB_button_login", @"Login with Facebook") forState:UIControlStateNormal];
    [self.orLabel setText:NSLocalizedString(@"OR", @"")];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (IBAction)fbSignUpButtonClicked:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             
         } else if (result.isCancelled) {
             
         } else {
             
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                          
                          BOOL success = NO;
                          
                          MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                          hud.mode = MBProgressHUDModeIndeterminate;
                          hud.labelText = NSLocalizedString(@"Loading",@"");
                          
                          if (!error)
                          {
                              RegisterViewController *signup = [[RegisterViewController alloc] init];
                              
                              success = [signup fetchUserInfo:@{
                                                                @"email": result[@"email"],
                                                                @"first_name": result[@"first_name"],
                                                                @"last_name": result[@"last_name"],
                                                                @"profile_image": result[@"picture"][@"data"][@"url"],
                                                                @"client_id": result[@"id"],
                                                                @"client": @"facebook",
                                                                }];
                              
                              if(success) {
                                  [self afterLogin];
                                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                              } else {
                                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                  FBSDKLoginManager *fblogin = [[FBSDKLoginManager alloc] init];
                                  [fblogin logOut];
                              }
                              
                          }
                          else
                          {
                              [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                              NSLog(@"Error %@",error);
                              FBSDKLoginManager *fblogin = [[FBSDKLoginManager alloc] init];
                              [fblogin logOut];
                          }
                      }];
                 }
                 
             }
         }
     }];
}

- (void)loginButton:(FBSDKLoginButton *)fbLoginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    
    if (error) {
        
    } else if (result.isCancelled) {
        
    } else {
        
        if ([result.grantedPermissions containsObject:@"email"])
        {
            
            if ([FBSDKAccessToken currentAccessToken])
            {
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     
                     BOOL success = NO;
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                     hud.mode = MBProgressHUDModeIndeterminate;
                     hud.labelText = NSLocalizedString(@"Loading",@"");
                     
                     if (!error)
                     {
                         RegisterViewController *signup = [[RegisterViewController alloc] init];
                         
                         success = [signup fetchUserInfo:@{
                                                           @"email": result[@"email"],
                                                           @"first_name": result[@"first_name"],
                                                           @"last_name": result[@"last_name"],
                                                           @"profile_image": result[@"picture"][@"data"][@"url"],
                                                           @"client_id": result[@"id"],
                                                           @"client": @"facebook",
                                                           }];
                         if(success) {
                             [self afterLogin];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                         } else {
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                             FBSDKLoginManager *fblogin = [[FBSDKLoginManager alloc] init];
                             [fblogin logOut];
                         }
                         
                     }
                     else
                     {
                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                         NSLog(@"Error %@",error);
                         FBSDKLoginManager *fblogin = [[FBSDKLoginManager alloc] init];
                         [fblogin logOut];
                     }
                 }];
            }
        }
    }
}

- (IBAction)liSignUpButtonClicked:(id)sender {
    
    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil]
                                         state:@"some state"
                        showGoToAppStoreDialog:YES
                                  successBlock:^(NSString *returnState) {
                                      
                                      NSString *url = [NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,formatted-name,location,email-address,picture-url)?format=json"];
                                      
                                      MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                      hud.mode = MBProgressHUDModeIndeterminate;
                                      hud.labelText = NSLocalizedString(@"Loading",@"");
                                      
                                      if ([LISDKSessionManager hasValidSession]) {
                                          
                                          [[LISDKAPIHelper sharedInstance] getRequest:url
                                                                              success:^(LISDKAPIResponse *response) {
                                                                                  
                                                                                  dispatch_async(dispatch_get_main_queue(), ^(void){
                                                                                      
                                                                                      BOOL success = NO;
                                                                                      
                                                                                      NSError *error;
                                                                                      NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[response.data dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO] options:NSJSONReadingAllowFragments error:&error];
                                                                                      
                                                                                      RegisterViewController *signup = [[RegisterViewController alloc] init];
                                                                                      
                                                                                      success = [signup fetchUserInfo:@{
                                                                                                                        @"email": jsonDict[@"emailAddress"],
                                                                                                                        @"first_name": jsonDict[@"firstName"],
                                                                                                                        @"last_name": jsonDict[@"lastName"],
                                                                                                                        @"profile_image": jsonDict[@"pictureUrl"],
                                                                                                                        @"client_id": jsonDict[@"id"],
                                                                                                                        @"client": @"linkedin",
                                                                                                                        }];
                                                                                      
                                                                                      if(success) {
                                                                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                                                          [self afterLogin];
                                                                                      }
                                                                                      else {
                                                                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                                                      }
                                                                                      
                                                                                      
                                                                                  });
                                                                                  
                                                                                  
                                                                              }
                                                                                error:^(LISDKAPIError *apiError) {
                                                                                    NSLog(@" error LIAPI => %@ ", apiError);
                                                                                }];
                                      }
                                      else {
                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                      }
                                  }
                                    errorBlock:^(NSError *error) {
                                        NSLog(@"%s %@","error called! ", [error description]);
                                    }
     ];
}

- (void) afterLogin {
    
    if(([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue] != 0)) {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"itemsDownloaded"]) {
            
            self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
            self.sbvc.view.tag = 1;
            
            StartTabBarViewController *tabController = [StartTabBarViewController new];
            
            SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
            
            rvc.modalPresentationStyle = UIModalPresentationFullScreen;
            rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [self presentViewController:rvc animated:YES completion:^(void) {
                [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
            }];
            
        } else {
            
            LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
            livc.delegate = self;
            livc.skipUserInput = YES;
            livc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
            livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
            livc.providesPresentationContextTransitionStyle = YES;
            livc.definesPresentationContext = YES;
            [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self.navigationController presentViewController:livc animated:NO completion:^{}];
            
        }
        
    } else {
        
        self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
        self.sbvc.view.tag = 1;
        
        UIViewController *vc = [AppDelegate getStartViewController];
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:nvc];
        
        rvc.modalPresentationStyle = UIModalPresentationFullScreen;
        rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self presentViewController:rvc animated:YES completion:^(void){
            [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
        }];
    }
    
}


- (void) loginButtonDidLogOut:(FBSDKLoginButton *)fbLoginButton {
    // when a fb user click on logout
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// for layout of the text finputs
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0; //holds the corner
        //Determine which corner(s) should be changed
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    } else {
        return view;
    }
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_statusbarBackground removeFromSuperview];
}

// actual login function
- (IBAction)loginButtonClicked:(id)sender {
    
    int success = 0;
    
    NSString *loginName = self.usernameTextField.text;
    
    @try {
        
        if([loginName isEqualToString:@""] || [self.passwordTextField.text isEqualToString:@""] ) {
        
            [Message alert:NSLocalizedString(@"Please insert your email address and password.", @"Error message if the user forgots one of those for the login") Title:NSLocalizedString(@"Error",@"Error headline for error message") Tag:0];
            
        } else {
            
            NSString *email = self.usernameTextField.text;
            NSString *password = self.passwordTextField.text;
            
            Login* login = [Login new];
//            
//            login.url = [NSString stringWithFormat:@"%@user_validation.php?jsonLogin&customerID=%i&getJsonDynamicSettings", SECURED_API_URL, CUSTOMERID];
//            
            login.url = [NSString stringWithFormat:@"%@/api/admin/auth/if3/%i/%@/%@", SYSTEM_DOMAIN, CUSTOMERID, email, password];
            
//            login.postData = @{
//                               @"username": email,
//                               @"password": password
//                            };
            
            UNIHTTPJsonResponse* response = [login callAPI];
            
            NSInteger code = response.code;
            success = [response.body.JSONObject[@"success"] intValue];
            NSDictionary* apiUserData = response.body.JSONObject;

            if (code >= 200 && code < 300) {
                
                if(success == 1) {
                    
                    [login markUserAsLoggedIn: @{
                                                 @"token" : apiUserData[@"token"],
                                                 @"user_id": apiUserData[@"userID"],
                                                 @"username": apiUserData[@"loginName"],
                                                 @"first_name": apiUserData[@"firstName"],
                                                 @"last_name": apiUserData[@"lastName"],
                                                 @"profile_image": apiUserData[@"profileImage"],
                                                 @"dynamicUserSettings": apiUserData[@"dynamicUserSettings"]
                                                 }];
                    
                } else {
                    NSString *error_msg = (NSString *) apiUserData[@"error_message"];
                    [Message alert:error_msg Title:NSLocalizedString(@"Error", @"") Tag:0];
                }
                
            } else {
                [Message alert:NSLocalizedString(@"Connection interrupted.",@"Connection lost error") Title:NSLocalizedString(@"Error",@"") Tag:0];
            }
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [Message alert:NSLocalizedString(@"Login failed.",@"general / unknown login error") Title:NSLocalizedString(@"Error",@"") Tag:0];
    }
    if (success) {
        
        [SSKeychain setPassword:self.passwordTextField.text forService:@"PickalbatrosLogin" account:self.usernameTextField.text];
        
        if (([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue] != 0)) {
           
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"itemsDownloaded"]) {
                
                self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
                self.sbvc.view.tag = 1;
                
                /*OfferViewController *sovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];
                
                sovc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
                
                NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:sovc];*/
                
                StartTabBarViewController *tabController = [StartTabBarViewController new];
                
                SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
                
                rvc.modalPresentationStyle = UIModalPresentationFullScreen;
                rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:rvc animated:YES completion:^(void){
                    [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
                }];
                
            }
            else {
                LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
                livc.delegate = self;
                livc.skipUserInput = YES;
                livc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
                livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
                livc.providesPresentationContextTransitionStyle = YES;
                livc.definesPresentationContext = YES;
                [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
                
                [self.navigationController presentViewController:livc animated:NO completion:^{}];
            }
            
        } else {
            self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
            self.sbvc.view.tag = 1;
            
            UIViewController *vc = [AppDelegate getStartViewController];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
            
            SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:nvc];
            
            rvc.modalPresentationStyle = UIModalPresentationFullScreen;
            rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [self presentViewController:rvc animated:YES completion:^(void){[UIApplication sharedApplication].keyWindow.rootViewController = rvc;}];
        }
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"userLoggedIn"];
        
    }
}

- (IBAction)backgroundClicked:(id)sender {
    // resign responders
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    CGRect mainScreenBounds = [UIScreen mainScreen].bounds;
    [UIView animateWithDuration:0.6f animations:^{
        [self.view setFrame:CGRectMake(0,-150,mainScreenBounds.size.width,mainScreenBounds.size.height)];
        self.backgroundShadow.alpha = 0.45f;
    } completion:^(BOOL finished){

    }];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    // Assign new frame to your view
    CGRect mainScreenBounds = [UIScreen mainScreen].bounds;
    [UIView animateWithDuration:0.6f animations:^{
        [self.view setFrame:CGRectMake(0,0,mainScreenBounds.size.width,mainScreenBounds.size.height)];
        self.backgroundShadow.alpha = 0.0f;
    } completion:^(BOOL finished){}];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showLoginFromRoot"]){
    }
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications{
}

-(void)removeConstraintsButSaveLayoutForSubviewsIn:(UIView*) view{
    for(UIView *v in view.subviews){
        if(v.subviews.count > 0){
            [self removeConstraintsButSaveLayoutForSubviewsIn:v];
        }
        
        CGRect frame = v.frame;
        
        [v translatesAutoresizingMaskIntoConstraints];
        
        [v removeConstraints:v.constraints];
        
        [v setAutoresizesSubviews:NO];
        
        v.frame = frame;
    }
}

-(void)hideLoadingInformationView{
    
    self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
    self.sbvc.view.tag = 1;
    
    
    StartTabBarViewController *tabController = [StartTabBarViewController new];
    
    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
    
    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
    } completion:^(BOOL finished) {
        
    }];

    //[self presentViewController:rvc animated:YES completion:^(void){[UIApplication sharedApplication].keyWindow.rootViewController = rvc;}];
}

- (IBAction)forgotPasswordButtonClicked:(id)sender {
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    wvc.url = [NSURL URLWithString:[NSString stringWithFormat:@"https://ifbck.com/scripts/forgot_password/?L=%i", [languages currentLanguageID]]];
    wvc.urlGiven = YES;
    wvc.titleString = NSLocalizedString(@"Forgot password?",@"");
    [self.navigationController pushViewController:wvc animated:YES];
}

@end
