//
//  RegisterTableViewCell.m
//  KAMEHA
//
//  Created by User on 19.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "RegisterTableViewCell.h"

@implementation RegisterTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self.confirmButton setTitle:NSLocalizedString(@"Confirm registration", @"") forState:UIControlStateNormal];
    
    self.uploadImageView.layer.cornerRadius=self.uploadImageView.height/2;
    self.uploadImageView.layer.borderColor=[[UIColor colorWithRGBHex:0xEBEBF2] CGColor];
    self.uploadImageView.layer.borderWidth=1.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)cellSwitchValueChanged:(UISwitch *)sender {
    if([self.delegate respondsToSelector:@selector(switchSwitched:)]){
        [self.delegate switchSwitched:sender.on];
    }
}

- (IBAction)confirmButtonClicked:(id)sender {
    if([self.delegate respondsToSelector:@selector(confirmButtonClicked)]){
        [self.delegate confirmButtonClicked];
    }
}
@end
