//
//  SettingsViewController.m
//  KAMEHA
//
//  Created by User on 23.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "SettingsViewController.h"
#import "RegisterHeaderViewController.h"
#import "OnboardingViewController.h"
#import "SWRevealViewController.h"
#import "PlaceOverViewController.h"
#import "SSKeychain.h"
#import "NavigationViewController.h"
#import "AppVersionManagement.h"
#import "LoadingInformationViewController.h"

#import "MBProgressHUD.h"

#import "AppDelegate.h"

#import "DBManager.h"
#import "Message.h"

#import "API.h"
#import "UNIRest.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "gestgid-Swift.h"

#import "GooglePlacePicker/GMSPlacePicker.h"
#import "GoogleMaps/GoogleMaps.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

@interface SettingsViewController (){
    float startingViewHeight;
    int basicSettingsSection;
    int dynamicSettingsSection;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tableDataWithCheckIn, *dynamicSettings, *tableDataWithoutCheckIn, *tableData;

@property (nonatomic) NSArray *pickerData;
@property (nonatomic) NSIndexPath *selectedPickercell;

@property (nonatomic) NSString *loginEmail, *loginPassword, *loginFirstName, *loginLastName, *loginOldEmail;
@property (nonatomic) NSString *passwordNew1, *passwordNew2;
@property (nonatomic) NSUInteger wantsCheckIn;

@property (nonatomic) DBManager *dbManager;

@property (nonatomic) OnboardingViewController *obvc;

@property (nonatomic) long userID;

@property (nonatomic) RegisterTextField *currentTextField;

@property (nonatomic) localStorage *settingsStorage;
@property (nonatomic) localStorage *dynamicUserSettingsStorage;

@property (nonatomic) NSMutableDictionary *dynamicUserSettingsValues;

@property (nonatomic) UIActionSheet *imageSelectionActionSheet;

@property (nonatomic) GMSPlacePicker *placePicker;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    
    @try {
    
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18], NSForegroundColorAttributeName:[UIColor blackColor]}];
        
    _uploadImageActivityIndicatorHidden = YES;
    
    basicSettingsSection = 1;
    dynamicSettingsSection = 2;
    
    self.settingsStorage = [localStorage storageWithFilename:@"settings"];
    self.dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    //NSLog(@" über social? => %@ ", self.dynamicUserSettingsStorage);
    
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    
    [self registerForKeyboardNotifications];
    
    if(self.titleString == nil){
        //self.navigationItem.titleView = [NavigationTitle createNavTitle:NSLocalizedString(@"Profile",@"") SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    } else {
        //self.navigationItem.titleView = [NavigationTitle createNavTitle:self.titleString SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    }
        
    self.title = NSLocalizedString(@"Profile",@"");
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.pickerViewFrame.alpha = 0.0f;
    self.pickerViewFrame.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, self.pickerViewFrame.width, self.pickerViewFrame.height);
    
    self.profileImageFileName = [self.dynamicUserSettingsStorage objectForKey:@"profileImageName"];
    self.profileImageFilePath = [self.dynamicUserSettingsStorage objectForKey:@"profileImagePath"];
    
    self.uploadedImage = [self getProfileImageForImageName:self.profileImageFileName serverPath:self.profileImageFilePath];
    
    [self fillFormularFromDatabase];
    
    if ([self.settingType.text isEqualToString:@"generalSettings"]) {
        
        UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Logout",@"") style:UIBarButtonItemStylePlain target:self action:@selector(logoutButtonClicked)];
        self.navigationItem.rightBarButtonItem = logoutButton;
        
        self.tableDataWithCheckIn = [self.settingsStorage objectForKey:@"settings"];
        self.dynamicSettings = [self.settingsStorage objectForKey:@"dynamicSettings"];
                
        if(self.tableDataWithCheckIn == nil ||
           self.tableDataWithCheckIn.count == 0 ||
           self.dynamicSettings == nil ||
           self.dynamicSettings.count == 0){
            
            self.tableDataWithCheckIn = [@[
                                           [@{@"title":NSLocalizedString(@"", @""),
                                              @"sectionCells":[@[
                                                                 [@{@"type":@"uploadImageCell",
                                                                    @"inputTag":@0,
                                                                    @"label":NSLocalizedString(@"Foto hochladen\nund besonderen\nService erhalten", @""),
                                                                    @"placeHolder":@"",
                                                                    @"value":@"",
                                                                    @"secureInput":@NO,
                                                                    @"keyboardType":@(UIKeyboardTypeDefault),
                                                                    @"cellAccessory":@(UITableViewCellAccessoryNone),
                                                                    @"hideTextfield":@YES
                                                                    } mutableCopy],
                                                                 ] mutableCopy],
                                              } mutableCopy],
                                           [@{@"title":NSLocalizedString(@"Account", @""),
                                              @"sectionCells":[@[
                                                                 [@{@"type":@"inputCell",
                                                                    @"inputTag":@0,
                                                                    @"label":NSLocalizedString(@"Email", @""),
                                                                    @"placeHolder":@"",
                                                                    @"value":@"",
                                                                    @"secureInput":@NO,
                                                                    @"keyboardType":@(UIKeyboardTypeEmailAddress),
                                                                    @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                                                    @"hideTextfield":@NO
                                                                    } mutableCopy],
                                                                 [@{@"type":@"inputCell",
                                                                    @"inputTag":@1,
                                                                    @"label":NSLocalizedString(@"Password", @""),
                                                                    @"placeHolder":@"",
                                                                    @"value":@"",
                                                                    @"secureInput":@YES,
                                                                    @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                                                    @"hideTextfield":@YES
                                                                    } mutableCopy]
                                                                 ] mutableCopy]
                                              } mutableCopy],
                                           [@{@"title":NSLocalizedString(@"Contact Data", @""),
                                              @"sectionCells":[@[
                                                           [@{@"type":@"inputCell",
                                                                   @"inputTag":@20161028,
                                                                   @"label":NSLocalizedStringFromTable(@"Form of address", @"admin", @""),
                                                                   @"placeHolder":NSLocalizedStringFromTable(@"Form of address", @"admin", @""),
                                                                   @"value":@"",
                                                                   @"secureInput":@NO,
                                                                   @"attributesTableID":@21,
                                                                   @"dynamicSettings":@YES,
                                                                   @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                                                   @"hideTextfield":@YES,
                                                                   @"onSelect":^{[self salutationSelector];}
                                                           } mutableCopy],
                                                                 [@{@"type":@"inputCell",
                                                                    @"inputTag":@3,
                                                                    @"label":NSLocalizedString(@"First name", @""),
                                                                    @"placeHolder":NSLocalizedString(@"First name", @""),
                                                                    @"value":@"",
                                                                    @"secureInput":@NO,
                                                                    @"cellAccessory":@(UITableViewCellAccessoryNone),
                                                                    @"keyboardType":@(UIKeyboardTypeEmailAddress),
                                                                    } mutableCopy],
                                                                 [@{@"type":@"inputCell",
                                                                    @"inputTag":@4,
                                                                    @"label":NSLocalizedString(@"Last name", @""),
                                                                    @"placeHolder":NSLocalizedString(@"First name", @""),
                                                                    @"value":@"",
                                                                    @"secureInput":@NO,
                                                                    @"cellAccessory":@(UITableViewCellAccessoryNone),
                                                                    @"keyboardType":@(UIKeyboardTypeEmailAddress),
                                                                    } mutableCopy],
                                                                 ] mutableCopy]
                                              } mutableCopy],
                                           [@{@"title":@"",
                                              @"sectionCells":[@[
                                                                 [@{@"type":@"confirmCell",
                                                                    @"inputTag":@999,
                                                                    @"label":NSLocalizedString(@"Save changes",@""),
                                                                    @"placeHolder":@""
                                                                    } mutableCopy]
                                                                 ] mutableCopy]
                                              } mutableCopy]
                                           ] mutableCopy];
            
            
            if(self.dynamicSettings == nil ||
               self.dynamicSettings.count == 0)
//                self.dynamicSettings = [@[
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@5,
//                                         @"label":NSLocalizedString(@"Street", @""),
//                                         @"placeHolder":NSLocalizedString(@"Street", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@1,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone),
//                                         @"keyboardType":@(UIKeyboardTypeAlphabet),
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@6,
//                                         @"label":NSLocalizedString(@"Street No.", @""),
//                                         @"placeHolder":NSLocalizedString(@"Street No.", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@2,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone),
//                                         @"keyboardType":@(UIKeyboardTypeAlphabet),
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@7,
//                                         @"label":NSLocalizedString(@"ZIP Code", @""),
//                                         @"placeHolder":NSLocalizedString(@"ZIP Code", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@3,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone),
//                                         @"keyboardType":@(UIKeyboardTypeAlphabet),
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@8,
//                                         @"label":NSLocalizedString(@"City", @""),
//                                         @"placeHolder":NSLocalizedString(@"City", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@4,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone),
//                                         @"keyboardType":@(UIKeyboardTypeAlphabet),
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@9,
//                                         @"label":NSLocalizedString(@"Country", @""),
//                                         @"placeHolder":NSLocalizedString(@"Country", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@5,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone),
//                                         @"keyboardType":@(UIKeyboardTypeAlphabet),
//                                         } mutableCopy],
//                                      [@{@"type":@"separatorCell",
//                                         @"label":NSLocalizedString(@"Billing Address", @""),
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@10,
//                                         @"label":NSLocalizedString(@"Company", @""),
//                                         @"placeHolder":NSLocalizedString(@"Company", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@7,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone)
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@11,
//                                         @"label":NSLocalizedString(@"Street", @""),
//                                         @"placeHolder":NSLocalizedString(@"Street", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@10,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone)
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@12,
//                                         @"label":NSLocalizedString(@"Street No.", @""),
//                                         @"placeHolder":NSLocalizedString(@"Street No.", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@11,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone)
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@12,
//                                         @"label":NSLocalizedString(@"ZIP Code", @""),
//                                         @"placeHolder":NSLocalizedString(@"ZIP Code", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@12,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@12,
//                                         @"label":NSLocalizedString(@"City", @""),
//                                         @"placeHolder":NSLocalizedString(@"City", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@13,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone)
//                                         } mutableCopy],
//                                      [@{@"type":@"inputCell",
//                                         @"inputTag":@12,
//                                         @"label":NSLocalizedString(@"Country", @""),
//                                         @"placeHolder":NSLocalizedString(@"Country", @""),
//                                         @"value":@"",
//                                         @"secureInput":@NO,
//                                         @"attributesTableID":@15,
//                                         @"dynamicSettings":@YES,
//                                         @"cellAccessory":@(UITableViewCellAccessoryNone)
//                                         } mutableCopy],
//                                      ] mutableCopy];
                self.dynamicSettings = [@[
                        [@{@"type":@"separatorCell",
                                @"label":NSLocalizedString(@"About you", @""),
                        } mutableCopy],
                        [@{@"type":@"inputCell",
                                @"inputTag":@13,
                                @"label":NSLocalizedString(@"Birth date", @""),
                                @"placeHolder":NSLocalizedString(@"Birth date", @""),
                                @"value":@"",
                                @"secureInput":@NO,
                                @"attributesTableID":@18,
                                @"dynamicSettings":@YES,
                                @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                @"hideTextfield":@YES,
                                @"onSelect":^{[self birthDateSelector];}
                        } mutableCopy],
                        [@{@"type":@"inputCell",
                           @"inputTag":@13,
                           @"label":NSLocalizedString(@"Place of residence", @""),
                           @"placeHolder":NSLocalizedString(@"Place of residence", @""),
                           @"value":@"",
                           @"secureInput":@NO,
                           @"attributesTableID":@22,
                           @"dynamicSettings":@YES,
                           @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                           @"hideTextfield":@YES,
                           @"onSelect":^{[self pickPlace];}
                           } mutableCopy],
                        [@{@"type":@"inputCell",
                                @"inputTag":@13,
                                @"label":NSLocalizedStringFromTable(@"Business", @"admin", @""),
                                @"placeHolder":NSLocalizedStringFromTable(@"Business", @"admin", @""),
                                @"value":@"",
                                @"secureInput":@NO,
                                @"attributesTableID":@19,
                                @"dynamicSettings":@YES,
                                @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                @"hideTextfield":@YES,
                                @"onSelect":^{[self branchSelector];}
                        } mutableCopy],
                        [@{@"type":@"inputCell",
                                @"inputTag":@13,
                                @"label":NSLocalizedString(@"Lifestyle", @""),
                                @"placeHolder":NSLocalizedString(@"Lifestyle", @""),
                                @"value":@"",
                                @"secureInput":@NO,
                                @"attributesTableID":@20,
                                @"dynamicSettings":@YES,
                                @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                @"hideTextfield":@YES,
                                @"onSelect":^{[self lifeStyleSelector];}
                        } mutableCopy],
                ] mutableCopy];
            
            // getting values from log in
            NSMutableDictionary *tmpdynamicUserSettingsValues = [self.dynamicUserSettingsStorage objectForKey:@"dynamicUserSettingsKeyValues"];
            
            for(NSMutableDictionary *dynamicSetting in self.dynamicSettings){
                NSString *tmpValue = tmpdynamicUserSettingsValues[[NSString stringWithFormat:@"%@",dynamicSetting[@"attributesTableID"]]];
                if(tmpValue != nil && ![tmpValue isEqualToString:@""]){
                    dynamicSetting[@"value"] = tmpValue;
                }
            }
            
            NSString *tmpLastName = [self.dynamicUserSettingsStorage objectForKey:@"lastName"];
            if(tmpLastName != nil && ![tmpLastName isEqualToString:@""]){
                // fortunately this works in newer version of the sdk
                self.tableDataWithCheckIn[dynamicSettingsSection][@"sectionCells"][2][@"value"] = tmpLastName;
            }
            NSString *tmpFirstName = [self.dynamicUserSettingsStorage objectForKey:@"firstName"];
            if(tmpLastName != nil && ![tmpLastName isEqualToString:@""]){
                // fortunately this works in newer version of the sdk
                self.tableDataWithCheckIn[dynamicSettingsSection][@"sectionCells"][1][@"value"] = tmpFirstName;
            }
            
            self.dynamicUserSettingsValues = tmpdynamicUserSettingsValues;
         
            [self.settingsStorage setObject:self.dynamicSettings forKey:@"dynamicSettings"];
            
        }
        
        self.tableData = self.tableDataWithCheckIn;
        
        [self.tableData[dynamicSettingsSection][@"sectionCells"] removeObjectsInArray:self.dynamicSettings];
            
        [self.tableData[dynamicSettingsSection][@"sectionCells"] addObjectsFromArray:self.dynamicSettings];
        
        (self.tableData[basicSettingsSection][@"sectionCells"][0])[@"cellAccessory"] = @(UITableViewCellAccessoryDisclosureIndicator);
        (self.tableData[basicSettingsSection][@"sectionCells"][0])[@"hideTextfield"] = @YES;
        
        (self.tableData[basicSettingsSection][@"sectionCells"][1])[@"cellAccessory"] = @(UITableViewCellAccessoryDisclosureIndicator);
        (self.tableData[basicSettingsSection][@"sectionCells"][1])[@"hideTextfield"] = @YES;
        
        if([self.tableData[basicSettingsSection][@"sectionCells"] count] < 3){
            // cell for place selection
            if(SINGLEPLACEID==0)[self.tableData[basicSettingsSection][@"sectionCells"] addObject:[@{@"type":@"inputCell",
                                                                                 @"inputTag":@0,
                                                                                 @"label":NSLocalizedString(@"Location", @""),
                                                                                 @"open":@"places",
                                                                                 @"placeHolder":@"",
                                                                                 @"value":@"",
                                                                                 @"secureInput":@NO,
                                                                                 @"keyboardType": @(UIKeyboardTypeEmailAddress),
                                                                                 @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                                                                 @"hideTextfield":@YES
                                                                                 } mutableCopy]];
//            [self.tableData[basicSettingsSection][@"sectionCells"] addObject:[@{@"type":@"inputCell",
//                                                             @"inputTag":@0,
//                                                             @"label":NSLocalizedString(@"Update contents", @""),
//                                                             @"open":@"update",
//                                                             @"placeHolder":@"",
//                                                             @"value":@"",
//                                                             @"secureInput":@NO,
//                                                             @"keyboardType":@(UIKeyboardTypeEmailAddress),
//                                                             @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
//                                                             @"hideTextfield":@YES
//                                                             } mutableCopy]];
        }
    
    }
    
    if([self.settingType.text isEqualToString:@"eMailSettings"]){
        
        NSMutableArray *tempArray = [[self.settingsStorage objectForKey:@"settings"] mutableCopy];
        
            NSString *email = [self.dynamicUserSettingsStorage objectForKey:@"email"];
            
            tempArray = [@[
                           [@{@"title":NSLocalizedString(@"Email", @""),
                              @"sectionCells":[@[
                                                 [@{@"type":@"inputCell",
                                                    @"inputTag":@0,
                                                    @"label":NSLocalizedString(@"", @""),
                                                    @"placeHolder":NSLocalizedString(@"example@example.com",@""),
                                                    @"value":email,
                                                    @"secureInput":@NO,
                                                    @"keyboardType":@(UIKeyboardTypeEmailAddress),
                                                    @"placeHolderHidden":@NO,
                                                    @"cellAccessory":@(UITableViewCellAccessoryNone)
                                                    } mutableCopy],
                                                 ] mutableCopy]
                              } mutableCopy],
                           [@{@"title":@"",
                              @"sectionCells":[@[
                                                 [@{@"type":@"confirmCell",
                                                    @"inputTag":@999,
                                                    @"label":@"",
                                                    @"placeHolder":@""
                                                    } mutableCopy]
                                                 ] mutableCopy]
                              } mutableCopy]
                           ] mutableCopy];
            self.tableDataWithCheckIn = tempArray;
        
        self.tableData = self.tableDataWithCheckIn;
    }
    
    if([self.settingType.text isEqualToString:@"passwordSettings"]){
        self.tableDataWithCheckIn = [@[
                                      @{@"title":NSLocalizedString(@"Password",@""),
                                        @"sectionCells":@[
                                                [@{@"type":@"inputCell",
                                                  @"inputTag":@0,
                                                  @"inputSecure":@YES,
                                                  @"label":NSLocalizedString(@"Old password",@""),
                                                  @"placeHolder":@"●●●●",
                                                  @"placeHolderHidden":@NO,
                                                  @"cellAccessory":@(UITableViewCellAccessoryNone)
                                                  } mutableCopy],
                                                @{@"type":@"inputCell",
                                                  @"inputTag":@1,
                                                  @"secureInput":@YES,
                                                  @"value":@"",
                                                  @"label":NSLocalizedString(@"New password", @""),
                                                  @"placeHolder":NSLocalizedString(@"Password",@""),
                                                  @"placeHolderHidden":@NO,
                                                  @"cellAccessory":@(UITableViewCellAccessoryNone)
                                                  },
                                                @{@"type":@"inputCell",
                                                  @"inputTag":@2,
                                                  @"secureInput":@YES,
                                                  @"label":NSLocalizedString(@"Confirm", @""),
                                                  @"placeHolder":NSLocalizedString(@"Password", @""),
                                                  @"value":@"",
                                                  @"placeHolderHidden":@NO,
                                                  @"cellAccessory":@(UITableViewCellAccessoryNone)
                                                  }
                                                ]
                                        },
                                      @{@"title":@"",
                                        @"sectionCells":@[
                                                @{@"type":@"confirmCell",
                                                  @"inputTag":@999,
                                                  @"inputSecure":@YES,
                                                  @"label":@"",
                                                  @"placeHolder":@"",
                                                  @"placeHolderHidden":@NO,
                                                  @"cellAccessory":@(UITableViewCellAccessoryNone)
                                                  }
                                                ]
                                        }
                                      ] mutableCopy];
        
        self.tableData = self.tableDataWithCheckIn;
        
    }
    
    [self.tableView reloadData];
        
    } @catch (NSException *exception) {
        NSLog(@"ERROR => %@", exception);
    }
}

- (void)setValue:(id)value forAttributesTableID:(int)attributesTableID{
    for(NSMutableDictionary* section in self.tableData){
        for(NSMutableDictionary *row in section[@"sectionCells"]){
            if([row[@"attributesTableID"] integerValue] == attributesTableID){
                row[@"value"] = value;
                return;
            }
        }
    }
}

- (id)getValueForAttributesTableID:(int)attributesTableID{
    for(NSMutableDictionary* section in self.tableData){
        for(NSMutableDictionary *row in section[@"sectionCells"]){
            if([row[@"attributesTableID"] integerValue] == attributesTableID){
                return row[@"value"];
            }
        }
    }
    return nil;
}

- (void)pickPlace {
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    
    GMSPlacePicker *placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    
    [placePicker pickPlaceWithCallback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
        if (result) {
            GMSPlace *newPlace = result;
            
            [[GMSPlacesClient new] lookUpPlaceID:result.placeID callback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
                GMSPlace *placeToUse;
                
                if (result){
                    placeToUse = result;
                }
                else {
                    placeToUse = newPlace;
                }
                
                [self placePicked:placeToUse];
                
                self.placePicker = nil;
            }];
            
            
        }
    }];
    
    self.placePicker = placePicker;
}

- (void)placePicked:(GMSPlace*)place {
    NSMutableDictionary *placeInfo = [@{} mutableCopy];
    
    if (place.addressComponents != nil) {
        for (GMSAddressComponent *addressComponent in place.addressComponents) {
            if ([addressComponent.type isEqualToString:@"locality"]) {
                placeInfo[@"city"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"street_number"]) {
                placeInfo[@"street_number"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"postal_code"]) {
                placeInfo[@"zip"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"route"]) {
                placeInfo[@"street"] = addressComponent.name;
            }
                
            if ([addressComponent.type isEqualToString:@"country"]) {
                placeInfo[@"country"] = addressComponent.name;
            }
        }
    }
    
    placeInfo[@"formated_address"] = place.formattedAddress;
    
    [self setValue:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:placeInfo options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forAttributesTableID:22];
}

- (void)salutationSelector{

    __weak SingleSelectorViewController *msvc = [SingleSelectorViewController load:@"SingleSelectorViewController" fromStoryboard:@"SingleSelectorViewController"];
    msvc.title = NSLocalizedStringFromTable(@"Form of address", @"admin", @"");
    msvc.data = @[
            @{@"name":NSLocalizedStringFromTable(@"Mr.",@"admin",@"")},
            @{@"name":NSLocalizedStringFromTable(@"Ms.",@"admin",@"")},
    ];
    id value = [self getValueForAttributesTableID:21];
    if([value isKindOfClass:[NSString class]]){
        NSMutableArray *selected = [@[] mutableCopy];
        NSArray *components = [value componentsSeparatedByString:@", "];
        int i = 0;
        for(NSDictionary *data in msvc.data){
            if([components containsObject:data[@"name"]]){
                [selected addObject:@(i)];
            }
            i++;
        }
        NSLog(@"selected: %@", selected)
        msvc.selected = selected;
    }

    msvc.onHide = ^{
        NSMutableArray *selectedOptions = [@[] mutableCopy];
        for(int i = 0;i < msvc.data.count;i++){
            if([msvc.selected containsObject:@(i)]){
                [selectedOptions addObject:msvc.data[i][@"name"]];
            }
        }
        [self setValue:[selectedOptions componentsJoinedByString:@", "] forAttributesTableID:21];
    };


    [self.navigationController pushViewController:msvc animated:YES];
}

- (void)branchSelector {
    
    __weak MultipleSelectorViewController *msvc = [MultipleSelectorViewController load:@"MultipleSelectorViewController" fromStoryboard:@"MultipleSelectorViewController"];
    msvc.title = NSLocalizedStringFromTable(@"Business", @"admin", @"");
    msvc.data = @[
                  @{@"name":NSLocalizedStringFromTable(@"Fasion",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Consulting",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Travel",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Hospitality",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Media",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Technology",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Banking",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Modeling",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Automotive",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Retail",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"PR",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Marketing",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Advertising",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Education",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Internet",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Entrepreneur",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Arts",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Law",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"FMCG",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Non-Profit",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Public Service",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Sports",@"admin",@"")},
                  ];
    id value = [self getValueForAttributesTableID:19];
    if([value isKindOfClass:[NSString class]]){
        NSMutableArray *selected = [@[] mutableCopy];
        NSArray *components = [value componentsSeparatedByString:@", "];
        int i = 0;
        for(NSDictionary *data in msvc.data){
            if([components containsObject:data[@"name"]]){
                [selected addObject:@(i)];
            }
            i++;
        }
        NSLog(@"selected: %@", selected)
        msvc.selected = selected;
    }
    
    msvc.onHide = ^{
        NSMutableArray *selectedOptions = [@[] mutableCopy];
        for(int i = 0;i < msvc.data.count;i++){
            if([msvc.selected containsObject:@(i)]){
                [selectedOptions addObject:msvc.data[i][@"name"]];
            }
        }
        [self setValue:[selectedOptions componentsJoinedByString:@", "] forAttributesTableID:19];
    };
    
    
    [self.navigationController pushViewController:msvc animated:YES];
}

- (void)lifeStyleSelector {
    
    __weak MultipleSelectorViewController *msvc = [MultipleSelectorViewController load:@"MultipleSelectorViewController" fromStoryboard:@"MultipleSelectorViewController"];
    msvc.title = NSLocalizedString(@"Lifestyle", @"");
    msvc.data = @[
                  @{@"name":NSLocalizedStringFromTable(@"Glamorous oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Classic",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Business style",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Fashion oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Sports oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Design oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Health oriented",@"admin",@"")},
                  ];
    id value = [self getValueForAttributesTableID:20];
    if([value isKindOfClass:[NSString class]]){
        NSMutableArray *selected = [@[] mutableCopy];
        NSArray *components = [value componentsSeparatedByString:@", "];
        int i = 0;
        for(NSDictionary *data in msvc.data){
            if([components containsObject:data[@"name"]]){
                [selected addObject:@(i)];
            }
            i++;
        }
        
        msvc.selected = selected;
    }
    
    msvc.onHide = ^{
        NSMutableArray *selectedOptions = [@[] mutableCopy];
        for(int i = 0;i < msvc.data.count;i++){
            if([msvc.selected containsObject:@(i)]){
                [selectedOptions addObject:msvc.data[i][@"name"]];
            }
        }
        [self setValue:[selectedOptions componentsJoinedByString:@", "] forAttributesTableID:20];
    };
    
    
    [self.navigationController pushViewController:msvc animated:YES];
}

- (void)birthDateSelector {
    id dateString = [self getValueForAttributesTableID:18];
    
    NSDate *date = [NSDate new];
    if([dateString isKindOfClass:[NSString class]]){
        NSDate *dateFromString = [miscFunctions dateFromEnString:dateString onlyDate:true onlyTime:false];
        if(dateFromString != nil){
            date = dateFromString;
        }
    }
    
    CWDatePicker *picker = [[CWDatePicker alloc] initWithDate:date fromView:self.view];
    [picker hideTimeSwitcher];
    [picker setShowTime:NO];
    [picker setSelected:^(NSDate *date) {
        NSString *formattedDate = [miscFunctions enStringFromDate:date onlyDate:true onlyTime:false];
        NSLog(@"saving date: %@", formattedDate)
        [self setValue:formattedDate forAttributesTableID:18];
    }];
    [picker show];
}

+ (BOOL)updateDynamicSettings:(NSArray*)dynamicSettingsArray forUserID:(int)userID validationPassword:(NSString*)password {
    return [SettingsViewController updateDynamicSettings:dynamicSettingsArray forUserID:userID validationPassword:password ignoreLocalUpdates:NO];
}

+ (BOOL)updateDynamicSettings:(NSArray*)dynamicSettingsArray forUserID:(int)userID validationPassword:(NSString*)password ignoreLocalUpdates:(BOOL)ignoreLocalUpdates{
    
    NSMutableDictionary *settingsDict = [[NSMutableDictionary alloc] init];
    
    for(NSMutableDictionary *dict in dynamicSettingsArray){
        NSString *key = [NSString stringWithFormat:@"%@", dict[@"attributesTableID"] ];
        if(dict[@"value"]!=nil && ![dict[@"value"] isKindOfClass:[NSNull class]]){
            settingsDict[key] = dict[@"value"];
        }
    }
    
    localStorage *dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    NSInteger success = 0;
    
    NSInteger overSocial = [[dynamicUserSettingsStorage objectForKey:@"overSocial"] intValue];
    NSString *client_id = [dynamicUserSettingsStorage objectForKey:@"client_id"];
    NSString *client = [dynamicUserSettingsStorage objectForKey:@"client"];
    
    @try {
        
        NSError *error;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:settingsDict
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSString *url = [NSString stringWithFormat:@"%@user_update.php?customerID=%i", SECURED_API_URL, CUSTOMERID];
        
        NSDictionary *postData;
        
        if (overSocial == NO) {
                        
            postData =  @{
                          @"userID": [@(userID) stringValue],
                          @"jsonUpdateDynamicSettings": jsonString,
                          @"validationPassword": password,
                          @"overSocial": [@(overSocial) stringValue]
                          };
            
        } else {
            
            postData = @{
                         @"userID": [@(userID) stringValue],
                         @"jsonUpdateDynamicSettings": jsonString,
                         @"client_id": client_id,
                         @"client": client,
                         @"overSocial": [@(overSocial) stringValue]
                         };
        }
        
        API* api = [API new];
        
        UNIHTTPJsonResponse* response = [api request:url :postData];
        NSDictionary* responseData = response.body.JSONObject;
        
        NSInteger statusCode = response.code;
        
        if (statusCode >= 200 && statusCode < 300) {
            success = [responseData[@"success"] integerValue];
        }
        
    } @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    
    if (success && settingsDict.count > 0) {
        if(!ignoreLocalUpdates){
            localStorage *dynamicUserSettings = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
            [dynamicUserSettings setObject:settingsDict forKey:@"dynamicUserSettingsKeyValues"];
        }
        
        return YES;
    }
    
    return NO;
}

- (void)viewDidLayoutSubviews{
    if(startingViewHeight == 0.0){
        startingViewHeight = self.view.height;
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    if(![self.settingType.text isEqualToString:@"passwordSettings"]){
        self.loginPassword = [SSKeychain passwordForService:@"PickalbatrosLogin" account:self.loginEmail];
    }
    
    if([self.settingType.text isEqualToString:@"eMailSettings"]){
        self.loginPassword = [SSKeychain passwordForService:@"PickalbatrosLogin" account:self.loginOldEmail];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    return sectionCellArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *sectionDict = self.tableData[indexPath.section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSDictionary *cellDict = sectionCellArray[indexPath.row];
    
    if([cellDict[@"type"] isEqualToString:@"separatorCell"]){
        return 30;
    } else if([cellDict[@"type"] isEqualToString:@"switchCell"]) {
        return 75;
    } else if([cellDict[@"type"] isEqualToString:@"confirmCell"]){
        return 90;
    } else if([cellDict[@"type"] isEqualToString:@"uploadImageCell"]){
        return 90;
    } else {
        return 45;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    
    RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
    
    rhvc.view.tag = 0;
    
    rhvc.labelHeaderTitle.text = sectionDict[@"title"];
    
    return rhvc.view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    if([sectionDict[@"title"] isEqualToString:@""]){
        return 0.0f;
    } else {
        return 30.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *sectionDict = self.tableData[indexPath.section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSDictionary *cellDict = sectionCellArray[indexPath.row];
    
    if ([cellDict[@"type"] isEqualToString:@"separatorCell"]) {
        
        RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"separatorCell"];
        cell.backgroundColor = rhvc.view.backgroundColor;
        
        rhvc.labelHeaderTitle.text = cellDict[@"label"];
        UIView *separatorView = rhvc.view;
        separatorView.height = 30;
        separatorView.tag = 0;
        
        separatorView.frame = cell.contentView.frame;
        
        [cell addSubview:separatorView];
        cell.separatorInset = UIEdgeInsetsMake(0, [UIScreen mainScreen].bounds.size.width, cell.height, 0);
        
        return cell;
    }
    
    RegisterTableViewCell *cell = (RegisterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellDict[@"type"]];
    
    if(indexPath.row!=sectionCellArray.count-1){
        
        if([sectionCellArray[indexPath.row+1][@"type"] isEqualToString:@"separatorCell"]){
            cell.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
        }
        
        else {
            cell.separatorInset = UIEdgeInsetsMake(cell.height, 15, 0, 0);
        }
        
    } else {
        cell.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
    }
    
    if ([cellDict[@"type"] isEqualToString:@"confirmCell"]) {
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.confirmButton setTitle:cellDict[@"label"] forState:UIControlStateNormal];
    }
    
    if([cellDict[@"type"] isEqualToString:@"uploadImageCell"]){
        RegisterTableViewCell *cell = (RegisterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellDict[@"type"]];
        cell.uploadedImageView.image = self.uploadedImage;
        cell.uploadImageActivityIndicator.hidden = self.uploadImageActivityIndicatorHidden;
        
        // image is already uploaded or existing locally
        if(self.uploadedImage!=nil){
            // hide placeholder
            cell.uploadPlaceHolderImage.image = nil;
            cell.uploadImageActivityIndicator.hidden = YES;
            
            NSLog(@"image existing");
            cell.uploadImageLabel.text = nil;
            NSString *userName = @"";
            if(![self stringEmpty:self.loginFirstName] && ![self stringEmpty:self.loginFirstName]){
                userName = [NSString stringWithFormat:@"%@ %@",self.loginFirstName,self.loginLastName];
            }
            else if(![self stringEmpty:self.loginLastName]){
                userName = self.loginFirstName;
            }
            else if(![self stringEmpty:self.loginLastName]){
                userName = self.loginLastName;
            }
            cell.userNameLabel.text = userName;
            cell.welcomeLabel.text = NSLocalizedString(@"Welcome", @"");
            cell.uploadImageLabel.text = nil;
            
        } else if (self.uploadingImage!=nil) {
            
            // hide placeholder
            NSLog(@"image is beeing uploaded");
            cell.uploadImageLabel.text = NSLocalizedString(@"Upload photo\nand  profit from\na special service", @"");
            cell.uploadPlaceHolderImage.image = nil;
            cell.userNameLabel.text = nil;
            cell.welcomeLabel.text = nil;
            cell.uploadImageActivityIndicator.hidden = NO;
            
        } else {
            
            NSLog(@"uploaded image is nil");
            cell.uploadImageLabel.text = NSLocalizedString(@"Upload photo\nand  profit from\na special service", @"");
            cell.userNameLabel.text = nil;
            cell.welcomeLabel.text = nil;
            cell.uploadImageActivityIndicator.hidden = YES;
        }
        
        [cell.uploadImageActivityIndicator startAnimating];
        return cell;
    }
    
        [cell.saveButton setTitle:NSLocalizedString(@"Save changes", @"") forState:UIControlStateNormal];
        cell.accessoryType = [cellDict[@"cellAccessory"] intValue];
        cell.textInput.hidden = [cellDict[@"hideTextfield"] boolValue];
        cell.delegate = self;
    
        cell.textInput.tag = [cellDict[@"inputTag"] intValue];
        cell.textInput.delegate = self;
        cell.textInput.placeholder = cellDict[@"placeHolder"];
    
        cell.labelDescription.text = cellDict[@"label"];
    
        cell.textInput.text = cellDict[@"value"];
        cell.textInput.selectedIndexPath = indexPath;
        cell.textInput.dynamicSetting = [cellDict[@"dynamicSettings"] boolValue];
    
        self.currentTextField = cell.textInput;
    
        if(cellDict[@"keyboardType"] != nil){
            [cell.textInput setKeyboardType:[cellDict[@"keyboardType"] intValue]];
        }
        
        cell.textInput.secureTextEntry = [cellDict[@"secureInput"] boolValue];
    
    if ([self.settingType.text isEqualToString:@"generalSettings"]) {
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableDictionary *sectionDict = self.tableData[indexPath.section];
    NSMutableArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSMutableDictionary *cellDict = sectionCellArray[indexPath.row];
    if(cellDict[@"onSelect"]) {
        ((void(^)()) cellDict[@"onSelect"])();
        return;
    }
    
    if([self.settingType.text isEqualToString:@"generalSettings"]){
        // image upload cell selected
        if(indexPath.section == 0 && indexPath.row == 0){
            // open image selection popup
            if(self.uploadedImage!=nil){
                self.imageSelectionActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Camera",@""),NSLocalizedString(@"Album",@""),NSLocalizedString(@"Delete photo",@""), nil];
                [self.imageSelectionActionSheet setDestructiveButtonIndex:2];
            }
            else {
                self.imageSelectionActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Camera",@""),NSLocalizedString(@"Album",@""), nil];
            }
            [self.imageSelectionActionSheet showInView:self.view];
            return;
        }
        // email settings cell selected
        if(indexPath.section == basicSettingsSection && indexPath.row == 0){
            // email cell
            SettingsViewController *svc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
            svc.settingType.text = @"eMailSettings";
            svc.titleString = NSLocalizedString(@"Email",@"");
            [self.navigationController pushViewController:svc animated:YES];
            return;
        }
        // password settings cell selected
        if(indexPath.section == basicSettingsSection && indexPath.row == 1){
            // password cell
            SettingsViewController *svc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
            svc.settingType.text = @"passwordSettings";
            svc.titleString = NSLocalizedString(@"Password",@"");
            [self.navigationController pushViewController:svc animated:YES];
            return;
        }
        // other cells
        if(indexPath.section == basicSettingsSection){
            // other cells in the first section
            NSDictionary *sectionDict = self.tableData[indexPath.section];
            NSArray *sectionCellArray = sectionDict[@"sectionCells"];
            NSDictionary *cellDict = sectionCellArray[indexPath.row];
            
            if([cellDict[@"open"] isEqualToString:@"places"]){
                // location cell
                UIViewController *vc = [AppDelegate getStartViewController];
                //povc.title = NSLocalizedString(@"Locations",@"");
                [self.navigationController pushViewController:vc animated:YES];
                return;
            }
            if([cellDict[@"open"] isEqualToString:@"update"]){
                // update cell
                [[[AppVersionManagement alloc] init] forceUpdateWithQuestion];
                return;
            }
        }
    }
    
    RegisterTableViewCell *rtvc = (RegisterTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if([cellDict[@"type"] isEqualToString:@"inputCell"]){
        [rtvc.textInput becomeFirstResponder];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField*)aTextField{
    [aTextField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    RegisterTextField *nt = (RegisterTextField*)textField;
    
    NSIndexPath *indexPath = nt.selectedIndexPath;
    NSMutableDictionary *sectionDict = self.tableData[indexPath.section];
    NSMutableArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSMutableDictionary *cellDict = sectionCellArray[indexPath.row];
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
        if(textField.tag == 0){
            self.loginEmail = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
        } else if (textField.tag == 1){
            self.loginPassword = newString;
            textField.secureTextEntry = YES;
        } else if (textField.tag == 3){
            self.loginFirstName = newString;
        } else if (textField.tag == 4){
            self.loginLastName = newString;
        }

    if(textField.secureTextEntry != YES){
        cellDict[@"value"] = newString;
    }

    return YES;
}

- (void)switchSwitched:(BOOL)selected{
    if([self.settingType.text isEqualToString:@"generalSettings"]){
        if(selected){
            self.tableData = self.tableDataWithCheckIn;
            self.wantsCheckIn = 1;
        } else {
            self.tableData = self.tableDataWithoutCheckIn;
            self.wantsCheckIn = 0;
        }
        [self.tableView reloadData];
    }
}

- (void) confirmButtonClicked {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");

    // default values
    NSInteger success = 0;
    NSInteger userID = self.userID;
    
    [[NSUserDefaults standardUserDefaults] setInteger:userID forKey:@"loggedInUserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *firstName = self.loginFirstName;
    NSString *lastName = self.loginLastName;
    NSString *loginName = self.loginEmail;

    NSString *loginPassword = self.loginPassword;
    NSString *newPasswordString1 = self.passwordNew1;
    NSString *newPasswordString2 = self.passwordNew2;
    
    NSInteger overSocial = [[self.dynamicUserSettingsStorage objectForKey:@"overSocial"] intValue];
    NSString *client_id = [self.dynamicUserSettingsStorage objectForKey:@"client_id"];
    NSString *client = [self.dynamicUserSettingsStorage objectForKey:@"client"];
    
    if([self.settingType.text isEqualToString:@"generalSettings"]){
        @try {
            NSError *error;
            
            NSMutableDictionary *settingsDict = [[NSMutableDictionary alloc] init];
            for(NSMutableDictionary *dict in self.dynamicSettings){
                if(dict[@"value"]!=nil && ![dict[@"value"] isKindOfClass:[NSNull class]])
                    settingsDict[[NSString stringWithFormat:@"%@", dict[@"attributesTableID"]]] = dict[@"value"];
            }
            
            NSMutableDictionary *passwordSection = [@{@"title":NSLocalizedString(@"Password",@""),
                 @"sectionCells":@[
                         @{@"type":@"inputCell",
                           @"inputTag":@1,
                           @"inputSecure":@YES,
                           @"label":NSLocalizedString(@"", @""),
                           @"placeHolder":@"●●●●",
                           @"placeHolderHidden":@NO,
                           @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
                           },
                         ]
                 } mutableCopy];
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:settingsDict
                                                               options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            if ((loginPassword == nil || [loginPassword isEqualToString:@""]) && overSocial == NO) {
                
                [Message alert:NSLocalizedString(@"Please insert your password for confirmation.", @"") Title: NSLocalizedString(@"Error", @"")Tag: 0];
                
                if(self.tableData[[self.tableData count]-1] != passwordSection){
                    [self.tableData insertObject:passwordSection atIndex:[self.tableData count]-1];
                }
                [self.tableView reloadData];
                
            } else if(true) {
                
                NSString *url = [NSString stringWithFormat:@"%@user_update.php?jsonUpdate&customerID=%i", SECURED_API_URL, APPID];
                
                NSDictionary *postData;
                
                if (overSocial == NO) {
                    
                    postData = @{
                                               @"userID": [@(userID) stringValue],
                                               @"setFirstName": firstName,
                                               @"setLastName": lastName,
                                               @"validationPassword": self.loginPassword,
                                               @"jsonUpdateDynamicSettings": jsonString,
                                               @"overSocial": [@(overSocial) stringValue]
                                               };

                } else {
                    
                    postData = @{
                                               @"userID": [@(userID) stringValue],
                                               @"setFirstName": firstName,
                                               @"setLastName": lastName,
                                               @"jsonUpdateDynamicSettings": jsonString,
                                               @"overSocial": [@(overSocial) stringValue],
                                               @"client_id": client_id,
                                               @"client": client
                                               };
                }
                
                NSLog(@" postData 1 => %@", postData);
                
                NSLog(@" url 1 => %@", url);
                
                API* api = [API new];
                
                UNIHTTPJsonResponse* response = [api request:url :postData];

                NSInteger statusCode = response.code;
                
                NSLog(@" response.body  %@", response.body.JSONObject);
                
                if (statusCode >= 200 && statusCode < 300)
                {
                    
                    NSDictionary *jsonData = response.body.JSONObject;
                    
                    NSLog(@" test jsonData %@", jsonData);
                    
                    success = [jsonData[@"success"] integerValue];

                    if (success == 1) {

                        NSDictionary *editedRow = jsonData[@"editedRow"];
                        
                        NSString *login, *firstName, *lastName;
                        
                        login = editedRow[@"login"];
                        firstName = editedRow[@"firstName"];
                        lastName = editedRow[@"lastName"];
                        
                        localStorage *dynamicSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
                        
                        [dynamicSettingsStorage setObject:firstName forKey:@"firstName"];
                        [dynamicSettingsStorage setObject:lastName forKey:@"lastName"];
                        [dynamicSettingsStorage setObject:loginName forKey:@"email"];
                        
                        NSString *query = [NSString stringWithFormat:@"UPDATE users SET first_name = '%@', last_name = '%@' , wants_check_in = %li WHERE userID = %li;", firstName, lastName, (long)self.wantsCheckIn, (long)userID];
                        
                        NSLog(@"Querry to run: %@", query);
                        
                        [SettingsViewController updateDynamicSettings:self.dynamicSettings forUserID:(int)userID validationPassword:loginPassword];
                        
                        // Execute the query.
                        [self.dbManager executeQuery:query];
                        
                        
                    } else {
                        NSString *error_msg = (NSString *) jsonData[@"error_message"];
                        
                        NSLog(@"Error should be shown: %@", error_msg);
                        [Message alert:error_msg Title: NSLocalizedString(@"Error", @"")Tag: 0];
                    }
                    
                } else {
                    [Message alert:NSLocalizedString(@"Connection interrupted.", @"") Title:NSLocalizedString(@"Error", @"") Tag:0];
                }
                
            } else {
                // password doesn't match
                [Message alert:NSLocalizedString(@"Passwords doesn't match.", @"") Title:NSLocalizedString(@"Error", @"") Tag:0];
            }
            
        } @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            [Message alert:NSLocalizedString(@"Error", @"") Title:@"" Tag:0];
        }
        
        if (success) {
            [Message alert:NSLocalizedString(@"Saved", @"") Title:@"" Tag:0];
            
            [self.settingsStorage setObject:self.tableDataWithCheckIn forKey:@"settings"];
            [self.settingsStorage setObject:self.dynamicSettings forKey:@"dynamicSettings"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    
    }
    
    if ([self.settingType.text isEqualToString:@"passwordSettings"]) {
        
        @try {
            
            if (([loginPassword isEqualToString:@""] || loginPassword == nil) && overSocial == NO) {
                
                [Message alert:@"Bitte aktuelles Passwort eingeben" Title:NSLocalizedString(@"Error",@"") Tag:0];
                
            } else if ([newPasswordString1 isEqualToString:newPasswordString2] && (newPasswordString1 != nil && ![newPasswordString1 isEqualToString:@""])) {
                
                NSString *post =[[NSString alloc] initWithFormat:@"userID=%li&setPassword=%@&validationPassword=%@", (long)userID, newPasswordString1, loginPassword];
                NSLog(@"PostData: %@",post);
                
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@user_update.php?jsonUpdate&customerID=%i",SECURED_API_URL, APPID]];
                
                NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                
                NSError *error;
                NSHTTPURLResponse *response = nil;
                NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                NSLog(@"Response code: %ld", (long)[response statusCode]);
                
                if ([response statusCode] >= 200 && [response statusCode] < 300)
                {
                    NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                    NSLog(@"Response (settingsviewcontroller) ==> %@", responseData);
                    
                    NSError *error = nil;
                    NSDictionary *jsonData = [NSJSONSerialization
                                              JSONObjectWithData:urlData
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
                    
                    success = [jsonData[@"success"] integerValue];
                    
                    NSLog(@"Success: %ld",(long)success);
                    
                    if(success == 1) {
                        NSLog(@"Login SUCCESS");
                    } else {
                        NSString *error_msg = (NSString *) jsonData[@"error_message"];
                        
                        NSLog(@"Error should be shown: %@", error_msg);
                        [Message alert:error_msg Title:NSLocalizedString(@"Error", @"") Tag:0];
                    }
                    
                } else {
                    
                    [Message alert:NSLocalizedString(@"Connection interrupted.", @"Connection lost error") Title:NSLocalizedString(@"Error", @"") Tag:0];
                }
                
            } else {
                [Message alert:NSLocalizedString(@"Passwords doesn't match.", @"") Title:NSLocalizedString(@"Error", @"") Tag:0];
            }
            
        } @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            [Message alert:NSLocalizedString(@"Error",@"") Title:@"" Tag:0];
        }
        
        if (success) {
            [Message alert:NSLocalizedString(@"Saved", @"") Title:@"" Tag:0];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if ([self.settingType.text isEqualToString:@"eMailSettings"]) {
        
        @try {
            
            if(([loginPassword isEqualToString:@""] || loginPassword == nil) && overSocial == NO) {
            
                [Message alert:NSLocalizedString(@"Please insert your password for confirmation.", @"") Title:NSLocalizedString(@"Error", @"") Tag:0];
                
                NSMutableDictionary *passwordSection = [@{@"title":NSLocalizedString(@"Password",@""),
                                                                                     @"sectionCells":@[
                                                                                                       @{@"type":@"inputCell",
                                                                                                         @"inputTag":@1,
                                                                                                         @"inputSecure":@YES,
                                                                                                         @"label":NSLocalizedString(@"", @""),
                                                                                                         @"placeHolder":@"●●●●",
                                                                                                         @"placeHolderHidden":@NO,
                                                                                                         @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
                                                                                                         },
                                                                                                       ]
                                                                                               } mutableCopy];
                [self.tableData insertObject:passwordSection atIndex:[self.tableData count]-1];
                [self.tableView reloadData];
                
            } else if (self.loginEmail != nil && ![self.loginEmail isEqualToString:@""]) {
                
                NSDictionary *postData;
                
                if (overSocial == NO) {
                    
                    postData = @{
                                 @"userID": [@(userID) stringValue],
                                 @"setUsername": loginName,
                                 @"validationPassword": loginPassword,
                                 @"overSocial": [@(overSocial) stringValue]
                                 };
                    
                } else {
                    
                    postData = @{
                                 @"userID": [@(userID) stringValue],
                                 @"setUsername": loginName,
                                 @"client_id": client_id,
                                 @"client": client,
                                 @"overSocial": [@(overSocial) stringValue]
                                 };
                }
                
                NSString *url= [NSString stringWithFormat:@"%@user_update.php?jsonUpdate&customerID=%i", SECURED_API_URL, APPID];
                
                API* api = [API new];
                
                NSLog(@" postData email API => %@ ", postData);
                
                UNIHTTPJsonResponse* response = [api request:url :postData];
                
                NSInteger statusCode = response.code;
                NSDictionary *jsonData = response.body.JSONObject;

                if (statusCode >= 200 && statusCode < 300)
                {

                    NSLog(@"Response (settingsviewcontroller) ==> %@", jsonData);
                    
                    success = [jsonData[@"success"] integerValue];
                    
                    NSLog(@"Success: %ld",(long)success);
                    
                    if(success == 1){
                        NSLog(@"Login SUCCESS");
                        
                        NSDictionary *editedRow = jsonData[@"editedRow"];
                        
                        NSString *login;
                        
                        login = editedRow[@"login"];
                        userID = [editedRow[@"id"] intValue];
                        
                        NSString *query = [NSString stringWithFormat:@"UPDATE users SET login = '%@' WHERE userID = %li;", login, (long)userID];
                        
                        NSLog(@"Querry to run: %@", query);
                        
                        [self.dynamicUserSettingsStorage setObject:login forKey:@"email"];
                        
                        [SSKeychain setPassword:self.loginPassword forService:@"PickalbatrosLogin" account:login];
                        
                        // Execute the query.
                        [self.dbManager executeQuery:query];
                        
                    } else {
                        NSString *error_msg = (NSString *) jsonData[@"error_message"];
                        
                        NSLog(@"Error should be shown: %@", error_msg);
                        [Message alert:error_msg Title:NSLocalizedString(@"Error", @"") Tag:0];
                    }
                    
                } else {
                    [Message alert:NSLocalizedString(@"Connection interrupted.", @"Connection lost error") Title:NSLocalizedString(@"Error", @"") Tag:0];
                }
                
            } else {
                [Message alert:NSLocalizedString(@"Please insert a new email address.", @"") Title:NSLocalizedString(@"Error", @"") Tag:0];
            }
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            [Message alert:NSLocalizedString(@"Error",@"") Title:@"" Tag:0];
        }
        if (success) {
            [Message alert:NSLocalizedString(@"Saved",@"") Title:@"" Tag:0];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }

    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITableViewCell *cell;
    
    cell = (UITableViewCell *) textField.superview.superview;
    
    
    [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)fillFormularFromDatabase {
    
    NSString *query = @"select * from users where is_logged_in = 1;";

    NSArray *tempArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    if (tempArray.count > 0) {
        
        self.loginFirstName = [self.dynamicUserSettingsStorage objectForKey:@"firstName"];
        
        NSInteger indexOfFirstID = [self.dbManager.arrColumnNames indexOfObject:@"userID"];
        int userID = [tempArray[0][indexOfFirstID] intValue];
        
        self.userID = (long) userID;
        
        self.loginLastName = [self.dynamicUserSettingsStorage objectForKey:@"lastName"];
        
        NSString *loginName = [self.dynamicUserSettingsStorage objectForKey:@"email"];
        self.loginOldEmail = loginName;
        self.loginEmail = loginName;
    }

}

- (void)logoutButtonClicked {

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"You have been logged out.",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",@"") , nil];
    
    [av show];
    
    self.obvc.modalPresentationStyle = UIModalPresentationFullScreen;
    self.obvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [AdminAPI shared].authToken = @"";
    [AdminAPI shared].if3AuthToken = @"";
    
    UIStoryboard *LoginAndregisterBoard = [UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil];
    self.obvc = (OnboardingViewController *)[LoginAndregisterBoard instantiateInitialViewController];
    
    [self.navigationController presentViewController:self.obvc animated:NO completion:^{
        
        [deviceTokenHandling userLogoutForToken:@"" forUserID:self.userID];
        
        // set all other users as outlogged
        NSString *logOutUserQuery = [NSString stringWithFormat:@"update users set is_logged_in = 0;"];
        
        NSLog(@"Querry to run: %@", logOutUserQuery);
        
        // Execute the query.
        [self.dbManager executeQuery:logOutUserQuery];
        
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"all users marked as logged out");
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedInUserID"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userLoggedIn"];
        if(SINGLEPLACEID==0)[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedPlaceID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [UIApplication sharedApplication].keyWindow.rootViewController = self.obvc;
        
        [self.settingsStorage clearFile];
        [self.dynamicUserSettingsStorage clearFile];
    }];
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    [deviceTokenHandling userLogoutForToken:@"" forUserID:self.userID];
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    self.navigationController.view.frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - kbSize.height + 50);
    
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{

}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.view layoutIfNeeded];
}

- (IBAction)pickerViewDone:(UIButton *)sender {
    
    [UIView animateWithDuration:0.6 animations:^{
        self.pickerViewFrame.y = [UIScreen mainScreen].bounds.size.height;
        self.pickerViewFrame.alpha = 0;
    } completion:^(BOOL finished){}];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if(pickerView.tag == 0){
        return 1;
    } else {
        return 1;
    }
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.pickerData.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.pickerData[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    RegisterTableViewCell *c = (RegisterTableViewCell*)[self.tableView cellForRowAtIndexPath:self.selectedPickercell];
    c.labelDescription.text = self.pickerData[row];
}


#pragma mark imagePicker delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.uploadingImage = selectedImage;
    self.uploadedImage = nil;
    [self uploadImage:selectedImage];
    [self.tableView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)uploadImage:(UIImage*)image{
    self.uploadImageActivityIndicatorHidden = NO;
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    NSURL *uploadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@image_upload.php",SECURED_API_URL]];
    [self uploadFile:image forField:[NSString stringWithFormat:@"file"] URL:uploadURL parameters:nil];
}

- (void)uploadFile:(UIImage *)image
          forField:(NSString *)fieldName
               URL:(NSURL*)url
        parameters:(NSDictionary *)parameters{
    //NSString *filename = [imagePath lastPathComponent];
    NSString *filename = @"profile_image.jpg";
    NSData *imageData = UIImageJPEGRepresentation(image, 85);
    
    NSMutableData *httpBody = [NSMutableData data];
    NSString *boundary = [self generateBoundaryString];
    NSString *mimetype = [self contentTypeForImageData:imageData];
    
    // configure the request
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set content type
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    
    if (imageData) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:imageData];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:httpBody];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError)
        {
            NSLog(@"sendAsynchronousRequest error=%@", connectionError);
            return;
        }
        
        [self uploadDone:data];
        
    }];
}
- (NSString *)generateBoundaryString{
    // generate boundary string
    //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    //
    // Note in iOS 6 and later, you can just:
    //
    //    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
    
    CFUUIDRef  uuid;
    NSString  *uuidStr;
    
    uuid = CFUUIDCreate(NULL);
    assert(uuid != NULL);
    
    uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    assert(uuidStr != NULL);
    
    CFRelease(uuid);
    
    return [NSString stringWithFormat:@"Boundary-%@", uuidStr];
}
- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

-(void)uploadDone:(NSData*)data{
    
    // parse the server output
    NSError *error;
    NSDictionary *jsonData = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:NSJSONReadingMutableContainers
                              error:&error];
    
    int success = [jsonData[@"success"] intValue];
    
    NSString *fileName = jsonData[@"fileName"];
    NSString *filePath = jsonData[@"filePath"];
    
    if(success){
        // updating the image cell, removing loading indicator and refresh this cell
        self.uploadedImage = self.uploadingImage;
        self.uploadImageActivityIndicatorHidden = YES;
        NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        self.profileImageFilePath = filePath;
        self.profileImageFileName = fileName;
        
        NSString *fileName = self.profileImageFileName;
        NSString *filePath = (self.profileImageFilePath!=nil)?self.profileImageFilePath:@"";
        
        if([self stringEmpty:fileName]){
            [self.dynamicUserSettingsStorage removeObjectForKey:@"profileImageName"];
            [self.dynamicUserSettingsStorage removeObjectForKey:@"profileImagePath"];
        }
        else {
            [self.dynamicUserSettingsStorage setObject:fileName forKey:@"profileImageName"];
            [self.dynamicUserSettingsStorage setObject:filePath forKey:@"profileImagePath"];
            NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            imageMethods *images = [[imageMethods alloc] init];
            NSString *imageType = @"";
            if([fileName hasSuffix:@".png"] ||
               [fileName hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([fileName hasSuffix:@".jpg"] ||
                    [fileName hasSuffix:@".jpeg"] ||
                    [fileName hasSuffix:@".JPEG"] ||
                    [fileName hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            [images saveImage:self.uploadedImage withFileName:fileName ofType:imageType inDirectory:documentsPath];
            
        }
    }
    
    NSLog(@"success: %i\nfileName: %@",success,fileName);
    
    [self saveProfileSettingsImageWithPath:filePath andName:fileName];
    
}

-(void) saveProfileSettingsImageWithPath:(NSString*)imagePath andName:(NSString*)imageName{
    
    if(imageName==nil)imageName=@"";
    if(imagePath==nil)imagePath=@"";
    
    // default values
    NSInteger success = 0;
    NSInteger userID = self.userID;
    
    
    NSInteger overSocial = [[self.dynamicUserSettingsStorage objectForKey:@"overSocial"] intValue];
    NSString *client_id = [self.dynamicUserSettingsStorage objectForKey:@"client_id"];
    NSString *client = [self.dynamicUserSettingsStorage objectForKey:@"client"];
    
    NSString *profileImageAndPath = [NSString stringWithFormat:@"%@%@",imagePath,imageName];
    NSLog(@"profileImageAndPath: %@",profileImageAndPath);
    
    @try {
        NSError *error;
        
        if(true){
            
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@user_update.php?jsonUpdate&customerID=%i",SECURED_API_URL, CUSTOMERID]];
            
            NSData *postData;
            
            if (overSocial == NO) {
                
                postData = [[@{
                             @"userID": [@(userID) stringValue],
                             @"validationPassword": self.loginPassword,
                             @"overSocial": [@(overSocial) stringValue],
                             @"setProfileImage": profileImageAndPath,
                             } urlEncodedString] dataUsingEncoding:NSUTF8StringEncoding];
                
            } else {
                
                postData = [[@{
                             @"userID": [@(userID) stringValue],
                             @"overSocial": [@(overSocial) stringValue],
                             @"client_id": client_id,
                             @"client": client,
                             @"setProfileImage": profileImageAndPath,
                             } urlEncodedString] dataUsingEncoding:NSUTF8StringEncoding];
            }
            
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            NSLog(@"Response code: %ld", (long)[response statusCode]);
            
            if ([response statusCode] >= 200 && [response statusCode] < 300)
            {
                NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"Response (settingsviewcontroller) ==> %@", responseData);
                
                NSError *error = nil;
                NSDictionary *jsonData = [NSJSONSerialization
                                          JSONObjectWithData:urlData
                                          options:NSJSONReadingMutableContainers
                                          error:&error];
                
                success = [jsonData[@"success"] integerValue];
                
                NSLog(@"Success: %ld",(long)success);
                
                
            } else {
                
            }
            
        } else {
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
    }
    if (success) {
        
        
    }

}

-(UIImage*)getProfileImageForImageName:(NSString*)imageName serverPath:(NSString*)serverPath{
    
    imageMethods *images = [[imageMethods alloc] init];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    UIImage *cellImage = nil;
    
    if (![imageName isEqualToString:@""] && imageName != nil) {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageName]]]) {
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageName ofType:@"png" inDirectory:documentsPath];
            
            return cellImage;
            
        } else if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]]]) {
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageName ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        } else {
            //Get Image From URL
            
            NSString *fullImagePath = [NSString stringWithFormat:@"%@%@",serverPath, imageName];
            
            UIImage * imageFromURL = [images getImageFromURL:fullImagePath];
            
            NSString *imageType = @"";
            if ([imageName hasSuffix:@".png"] ||
                [imageName hasSuffix:@".PNG"]) {
                imageType = @"png";
            }
            else if([imageName hasSuffix:@".jpg"] ||
                    [imageName hasSuffix:@".jpeg"] ||
                    [imageName hasSuffix:@".JPEG"] ||
                    [imageName hasSuffix:@".JPG"]){
                imageType = @"jpg";
            } else if([images.mimeType isEqualToString:@"image/jpeg"]) {
                imageType = @"jpg";
            }
            
            if ([imageName rangeOfString: @".jpg" ].location != NSNotFound) {
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageName ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageName ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
            return cellImage;
        }
        
    }
    
    return cellImage;
}


-(BOOL)stringEmpty:(NSString*)string{
    if(string==nil){
        return true;
    }
    
    if([string isEqualToString:@""]){
        return true;
    }
    
    if([string isKindOfClass:[NSNull class]]){
        return true;
    }
    
    return false;
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(actionSheet==self.imageSelectionActionSheet){
        if(buttonIndex==2){
            self.uploadedImage = nil;self.uploadingImage = nil;
            self.profileImageFileName = nil;
            
            self.uploadImageActivityIndicatorHidden = YES;
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [self saveProfileSettingsImageWithPath:@"" andName:@""];
            
            return;
        }
        else if(buttonIndex==0 || buttonIndex==1){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];

            picker.navigationBar.tintColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
            picker.navigationBar.barTintColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];
            
            [picker.navigationBar setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: picker.navigationBar.tintColor,
                                                            NSFontAttributeName: [UIFont fontWithName:@"Arial" size:17.0f]
                                                            }];
            
            picker.delegate = self;
            picker.allowsEditing = YES;
            if(buttonIndex==0){
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                }
            }
            if(buttonIndex==1){
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
            
            UIView *navigationBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
            navigationBackgroundView.backgroundColor = [UIColor whiteColor];
            
            [picker.navigationController.view addSubview:navigationBackgroundView];
            
            [self presentViewController:picker animated:YES completion:nil];
        }
        return;
    }
}

@end
