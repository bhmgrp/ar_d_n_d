//
//  loginViewController.h
//  Database
//
//  Created by User on 30.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SidebarViewController.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface loginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic) SidebarViewController *sbvc;

// outlets begin
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *backgroundShadow;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;

// textfields
@property (weak, nonatomic) IBOutlet UIView *userTextfieldView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (weak, nonatomic) IBOutlet UIView *passwordTextfieldView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIView *textInputView;

@property (weak, nonatomic) IBOutlet UIView *inputView;

// login button
@property (weak, nonatomic) IBOutlet UIView *loginButtonView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *linkedInButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;

- (IBAction)loginButtonClicked:(id)sender;
- (IBAction)liSignUpButtonClicked:(id)sender;
- (IBAction)fbSignUpButtonClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
- (IBAction)forgotPasswordButtonClicked:(id)sender;

// outlets end



// other propertys
@property (nonatomic) CGPoint originalCenter;
@property (nonatomic) CGPoint movedCenter;

// other methods
// for rounded textinputs
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;


@end
