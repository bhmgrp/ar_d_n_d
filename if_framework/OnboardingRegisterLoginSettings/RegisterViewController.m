//
//  RegisterViewController.m
//  KAMEHA
//
//  Created by User on 19.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterHeaderViewController.h"
#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import "PlaceOverViewController.h"
#import "NavigationViewController.h"
#import "StructureOverViewController.h"
#import "WebViewController.h"

#import "SSKeychain.h"

#import "AppDelegate.h"

#import "DBManager.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "Login.h"
#import "Message.h"
#import "StartTabBarViewController.h"

#import "gestgid-Swift.h"

#import "GooglePlacePicker/GMSPlacePicker.h"
#import "GoogleMaps/GoogleMaps.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

@interface RegisterViewController () <loadingInformationProtocol>{
    float startingViewHeight;
    int dynamicSettingsSection;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tableDataWithCheckIn, *dynamicSettings, *tableData;

@property (nonatomic) NSString *loginEmail, *loginPassword, *loginFirstName, *loginLastName, *loginStreet, *loginStreetNo, *loginCity, *loginCountry;

@property (nonatomic) DBManager *dbManager;

@property (nonatomic) NSIndexPath *currentPath;

@property (nonatomic) RegisterTextField *currentTextField;

@property (nonatomic) localStorage *settingsStorage;
@property (nonatomic) localStorage *dynamicUserSettingsStorage;

@property (nonatomic) UIActionSheet *imageSelectionActionSheet;

@property (nonatomic) GMSPlacePicker *placePicker;

typedef void (^onSelectRow)();

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    dynamicSettingsSection = 2;
    
    self.settingsStorage = [localStorage storageWithFilename:@"settings"];
    self.dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];
    
    [self registerForKeyboardNotifications];
    
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    
    self.navigationItem.title = NSLocalizedString(@"Registration",@"");
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.profileImageFileName = [self.dynamicUserSettingsStorage objectForKey:@"profileImageName"];
    self.profileImageFilePath = [self.dynamicUserSettingsStorage objectForKey:@"profileImagePath"];
    
    self.uploadedImage = [self getProfileImageForImageName:self.profileImageFileName serverPath:self.profileImageFilePath];
    
    _uploadImageActivityIndicatorHidden = YES;
    
    self.tableDataWithCheckIn = [@[
                                   [@{@"title":NSLocalizedString(@"", @""),
                                      @"sectionCells":[@[
                                                         [@{@"type":@"uploadImageCell",
                                                            @"inputTag":@0,
                                                            @"label":NSLocalizedString(@"Foto hochladen\nund besonderen\nService erhalten", @""),
                                                            @"placeHolder":@"",
                                                            @"value":@"",
                                                            @"secureInput":@NO,
                                                            @"keyboardType":[NSNumber numberWithInt:UIKeyboardTypeDefault],
                                                            @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone],
                                                            @"hideTextfield":@YES
                                                            } mutableCopy],
                                                         ] mutableCopy],
                                      } mutableCopy],
                                   [@{@"title":NSLocalizedString(@"Account", @""),
                                      @"sectionCells":[@[
                                                         [@{@"type":@"inputCell",
                                                            @"inputTag":@0,
                                                            @"label":NSLocalizedString(@"Email", @""),
                                                            @"placeHolder":NSLocalizedString(@"example@example.com",@"email placeholder"),
                                                            @"value":@"",
                                                            @"secureInput":@NO,
                                                            @"keyboardType":[NSNumber numberWithInt:UIKeyboardTypeEmailAddress]
                                                            } mutableCopy],
                                                         [@{@"type":@"inputCell",
                                                            @"inputTag":@1,
                                                            @"label":NSLocalizedString(@"Password", @""),
                                                            @"placeHolder":@"●●●●",
                                                            @"value":@"",
                                                            @"secureInput":@YES
                                                            } mutableCopy]
                                                         ] mutableCopy]
                                      } mutableCopy],
                                   [@{@"title":NSLocalizedString(@"Contact data", @""),
                                      @"sectionCells":[@[
                                                   [@{@"type":@"inputCell",
                                                           @"inputTag":@20161028,
                                                           @"label":NSLocalizedStringFromTable(@"Form of address", @"admin", @""),
                                                           @"placeHolder":NSLocalizedStringFromTable(@"Form of address", @"admin", @""),
                                                           @"value":@"",
                                                           @"secureInput":@NO,
                                                           @"attributesTableID":@21,
                                                           @"dynamicSettings":@YES,
                                                           @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                                           @"hideTextfield":@YES,
                                                           @"onSelect":^{[self salutationSelector];}
                                                   } mutableCopy],
                                                         [@{@"type":@"inputCell",
                                                            @"inputTag":@3,
                                                            @"label":NSLocalizedString(@"First name", @""),
                                                            @"placeHolder":NSLocalizedString(@"First name", @""),
                                                            @"value":@"",
                                                            @"secureInput":@NO,
                                                            } mutableCopy],
                                                         [@{@"type":@"inputCell",
                                                            @"inputTag":@4,
                                                            @"label":NSLocalizedString(@"Last name", @""),
                                                            @"placeHolder":NSLocalizedString(@"Last name", @""),
                                                            @"value":@"",
                                                            @"secureInput":@NO,
                                                            } mutableCopy],
                                                         ] mutableCopy]
                                      } mutableCopy],
                                   [@{@"title":@"",
                                      @"sectionCells":[@[
                                                         [@{@"type":@"confirmCell",
                                                            @"inputTag":@999,
                                                            @"label":@"",
                                                            @"placeHolder":@""
                                                            } mutableCopy]
                                                         ] mutableCopy]
                                      } mutableCopy]
                                   ] mutableCopy];
    
    
//    self.dynamicSettings = [@[
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@5,
//                                 @"label":NSLocalizedString(@"Street", @""),
//                                 @"placeHolder":NSLocalizedString(@"Street", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@1,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@6,
//                                 @"label":NSLocalizedString(@"Street No.", @""),
//                                 @"placeHolder":NSLocalizedString(@"Street No.", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@2,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@7,
//                                 @"label":NSLocalizedString(@"ZIP Code", @""),
//                                 @"placeHolder":NSLocalizedString(@"ZIP Code", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@3,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@8,
//                                 @"label":NSLocalizedString(@"City", @""),
//                                 @"placeHolder":NSLocalizedString(@"City", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@4,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@9,
//                                 @"label":NSLocalizedString(@"Country", @""),
//                                 @"placeHolder":NSLocalizedString(@"Country", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@5,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"separatorCell",
//                                 @"label":NSLocalizedString(@"Billing Address", @""),
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@10,
//                                 @"label":NSLocalizedString(@"Company", @""),
//                                 @"placeHolder":NSLocalizedString(@"Company", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@7,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@11,
//                                 @"label":NSLocalizedString(@"Street", @""),
//                                 @"placeHolder":NSLocalizedString(@"Street", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@10,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@12,
//                                 @"label":NSLocalizedString(@"Street No.", @""),
//                                 @"placeHolder":NSLocalizedString(@"Street No.", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@11,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@12,
//                                 @"label":NSLocalizedString(@"ZIP Code", @""),
//                                 @"placeHolder":NSLocalizedString(@"ZIP Code", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@12,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@12,
//                                 @"label":NSLocalizedString(@"City", @""),
//                                 @"placeHolder":NSLocalizedString(@"City", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@13,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              [@{@"type":@"inputCell",
//                                 @"inputTag":@12,
//                                 @"label":NSLocalizedString(@"Country", @""),
//                                 @"placeHolder":NSLocalizedString(@"Country", @""),
//                                 @"value":@"",
//                                 @"secureInput":@NO,
//                                 @"attributesTableID":@15,
//                                 @"dynamicSettings":@YES,
//                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone]
//                                 } mutableCopy],
//                              ] mutableCopy];
    
    self.dynamicSettings = [@[
                              [@{@"type":@"separatorCell",
                                 @"label":NSLocalizedString(@"About you", @""),
                                 } mutableCopy],
                              [@{@"type":@"inputCell",
                                 @"inputTag":@13,
                                 @"label":NSLocalizedString(@"Birth date", @""),
                                 @"placeHolder":NSLocalizedString(@"Birth date", @""),
                                 @"value":@"",
                                 @"secureInput":@NO,
                                 @"attributesTableID":@17,
                                 @"dynamicSettings":@YES,
                                 @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                 @"hideTextfield":@YES,
                                 @"onSelect":^{[self birthDateSelector];}
                                 } mutableCopy],
                              [@{@"type":@"inputCell",
                                 @"inputTag":@13,
                                 @"label":NSLocalizedString(@"Place of residence", @""),
                                 @"placeHolder":NSLocalizedString(@"Place of residence", @""),
                                 @"value":@"",
                                 @"secureInput":@NO,
                                 @"attributesTableID":@22,
                                 @"dynamicSettings":@YES,
                                 @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                 @"hideTextfield":@YES,
                                 @"onSelect":^{[self pickPlace];}
                                 } mutableCopy],
                              [@{@"type":@"inputCell",
                                 @"inputTag":@13,
                                 @"label":NSLocalizedString(@"Branche", @""),
                                 @"placeHolder":NSLocalizedString(@"Branche", @""),
                                 @"value":@"",
                                 @"secureInput":@NO,
                                 @"attributesTableID":@19,
                                 @"dynamicSettings":@YES,
                                 @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                 @"hideTextfield":@YES,
                                 @"onSelect":^{[self branchSelector];}
                                 } mutableCopy],
                              [@{@"type":@"inputCell",
                                 @"inputTag":@13,
                                 @"label":NSLocalizedString(@"Lifestyle", @""),
                                 @"placeHolder":NSLocalizedString(@"Lifestyle", @""),
                                 @"value":@"",
                                 @"secureInput":@NO,
                                 @"attributesTableID":@20,
                                 @"dynamicSettings":@YES,
                                 @"cellAccessory":@(UITableViewCellAccessoryDisclosureIndicator),
                                 @"hideTextfield":@YES,
                                 @"onSelect":^{[self lifeStyleSelector];}
                                 } mutableCopy],
                              ] mutableCopy];
    
    [self.settingsStorage setObject:self.tableDataWithCheckIn forKey:@"settings"];
    
    [self.settingsStorage setObject:self.dynamicSettings forKey:@"dynamicSettings"];
    
    // adding the dynamic settings below the static contact data (first & last name)
    self.tableData = self.tableDataWithCheckIn;
    
    [self.tableData[dynamicSettingsSection][@"sectionCells"] addObjectsFromArray:self.dynamicSettings];
    
    [self.tableView reloadData];
}

- (void)setValue:(id)value forAttributesTableID:(int)attributesTableID{
    for(NSMutableDictionary* section in self.tableData){
        for(NSMutableDictionary *row in section[@"sectionCells"]){
            if([row[@"attributesTableID"] integerValue] == attributesTableID){
                row[@"value"] = value;
                return;
            }
        }
    }
}

- (id)getValueForAttributesTableID:(int)attributesTableID{
    for(NSMutableDictionary* section in self.tableData){
        for(NSMutableDictionary *row in section[@"sectionCells"]){
            if([row[@"attributesTableID"] integerValue] == attributesTableID){
                return row[@"value"];
            }
        }
    }
    return nil;
}

- (void)pickPlace {
    
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    
    GMSPlacePicker *placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    [UINavigationBar appearance].barStyle = UIBarStyleDefault;
    [UINavigationBar appearance].barTintColor = [UIColor whiteColor];
    
    [placePicker pickPlaceWithCallback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
        if (result) {
            GMSPlace *newPlace = result;
            
            [[GMSPlacesClient new] lookUpPlaceID:result.placeID callback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
                GMSPlace *placeToUse;
                
                if (result){
                    placeToUse = result;
                }
                else {
                    placeToUse = newPlace;
                }
                
                [self placePicked:placeToUse];
                
                self.placePicker = nil;
            }];
            
            
        }
    }];
    
    self.placePicker = placePicker;
}

- (void)placePicked:(GMSPlace*)place {
    NSMutableDictionary *placeInfo = [@{} mutableCopy];
    
    if (place.addressComponents != nil) {
        for (GMSAddressComponent *addressComponent in place.addressComponents) {
            if ([addressComponent.type isEqualToString:@"locality"]) {
                placeInfo[@"city"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"street_number"]) {
                placeInfo[@"street_number"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"postal_code"]) {
                placeInfo[@"zip"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"route"]) {
                placeInfo[@"street"] = addressComponent.name;
            }
            
            if ([addressComponent.type isEqualToString:@"country"]) {
                placeInfo[@"country"] = addressComponent.name;
            }
        }
    }
    
    placeInfo[@"formated_address"] = place.formattedAddress;
    
    [self setValue:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:placeInfo options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forAttributesTableID:22];
}

- (void)salutationSelector{

    __weak SingleSelectorViewController *msvc = [SingleSelectorViewController load:@"SingleSelectorViewController" fromStoryboard:@"SingleSelectorViewController"];
    msvc.title = NSLocalizedStringFromTable(@"Form of address", @"admin", @"");
    msvc.data = @[
            @{@"name":NSLocalizedStringFromTable(@"Mr.",@"admin",@"")},
            @{@"name":NSLocalizedStringFromTable(@"Ms.",@"admin",@"")},
    ];
    id value = [self getValueForAttributesTableID:21];
    if([value isKindOfClass:[NSString class]]){
        NSMutableArray *selected = [@[] mutableCopy];
        NSArray *components = [value componentsSeparatedByString:@", "];
        int i = 0;
        for(NSDictionary *data in msvc.data){
            if([components containsObject:data[@"name"]]){
                [selected addObject:@(i)];
            }
            i++;
        }
        NSLog(@"selected: %@", selected)
        msvc.selected = selected;
    }

    msvc.onHide = ^{
        NSMutableArray *selectedOptions = [@[] mutableCopy];
        for(int i = 0;i < msvc.data.count;i++){
            if([msvc.selected containsObject:@(i)]){
                [selectedOptions addObject:msvc.data[i][@"name"]];
            }
        }
        [self setValue:[selectedOptions componentsJoinedByString:@", "] forAttributesTableID:21];
    };


    [self.navigationController pushViewController:msvc animated:YES];
}

- (void)branchSelector {
    
    __weak MultipleSelectorViewController *msvc = [MultipleSelectorViewController load:@"MultipleSelectorViewController" fromStoryboard:@"MultipleSelectorViewController"];
    msvc.title = NSLocalizedString(@"Branche", @"");
    msvc.data = @[
                  @{@"name":NSLocalizedStringFromTable(@"Fasion",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Consulting",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Travel",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Hospitality",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Media",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Technology",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Banking",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Modeling",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Automotive",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Retail",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"PR",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Marketing",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Advertising",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Education",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Internet",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Entrepreneur",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Arts",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Law",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"FMCG",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Non-Profit",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Public Service",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Sports",@"admin",@"")},
                  ];
    id value = [self getValueForAttributesTableID:19];
    if([value isKindOfClass:[NSString class]]){
        NSMutableArray *selected = [@[] mutableCopy];
        NSArray *components = [value componentsSeparatedByString:@", "];
        int i = 0;
        for(NSDictionary *data in msvc.data){
            if([components containsObject:data[@"name"]]){
                [selected addObject:@(i)];
            }
            i++;
        }
        msvc.selected = selected;
    }
    
    msvc.onHide = ^{
        NSMutableArray *selectedOptions = [@[] mutableCopy];
        for(int i = 0;i < msvc.data.count;i++){
            if([msvc.selected containsObject:@(i)]){
                [selectedOptions addObject:msvc.data[i][@"name"]];
            }
        }
        [self setValue:[selectedOptions componentsJoinedByString:@", "] forAttributesTableID:19];
    };
    
    
    [self.navigationController pushViewController:msvc animated:YES];
}

- (void)lifeStyleSelector {
    
    __weak MultipleSelectorViewController *msvc = [MultipleSelectorViewController load:@"MultipleSelectorViewController" fromStoryboard:@"MultipleSelectorViewController"];
    msvc.title = NSLocalizedString(@"Lifestyle", @"");
    msvc.data = @[
                  @{@"name":NSLocalizedStringFromTable(@"Glamorous oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Classic",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Business style",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Fasion oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Sports oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Design oriented",@"admin",@"")},
                  @{@"name":NSLocalizedStringFromTable(@"Health oriented",@"admin",@"")},
                  ];
    id value = [self getValueForAttributesTableID:20];
    if([value isKindOfClass:[NSString class]]){
        NSMutableArray *selected = [@[] mutableCopy];
        NSArray *components = [value componentsSeparatedByString:@", "];
        int i = 0;
        for(NSDictionary *data in msvc.data){
            if([components containsObject:data[@"name"]]){
                [selected addObject:@(i)];
            }
            i++;
        }
        
        msvc.selected = selected;
    }
    
    msvc.onHide = ^{
        NSMutableArray *selectedOptions = [@[] mutableCopy];
        for(int i = 0;i < msvc.data.count;i++){
            if([msvc.selected containsObject:@(i)]){
                [selectedOptions addObject:msvc.data[i][@"name"]];
            }
        }
        [self setValue:[selectedOptions componentsJoinedByString:@", "] forAttributesTableID:20];
    };
    
    
    [self.navigationController pushViewController:msvc animated:YES];
}

- (void)birthDateSelector {
    id dateString = [self getValueForAttributesTableID:17];
    
    NSDate *date = [NSDate new];
    if([dateString isKindOfClass:[NSString class]]){
        NSDate *dateFromString = [miscFunctions dateFromEnString:dateString onlyDate:true onlyTime:false];
        if(dateFromString != nil){
            date = dateFromString;
        }
    }
    
    CWDatePicker *picker = [[CWDatePicker alloc] initWithDate:date fromView:self.view];
    [picker hideTimeSwitcher];
    [picker setShowTime:NO];
    [picker setSelected:^(NSDate *date) {
        NSString *formattedDate = [miscFunctions enStringFromDate:date onlyDate:true onlyTime:false];
        [self setValue:formattedDate forAttributesTableID:17];
    }];
    [picker show];
}

-(void)viewDidLayoutSubviews{
    if(startingViewHeight == 0.0){
        startingViewHeight = self.view.height;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


-(BOOL)fetchUserInfo:(NSDictionary*) result
{
    
    NSString *email = result[@"email"];
    NSString *client_id = result[@"client_id"];
    NSString *client = result[@"client"];
    NSString *first_name = result[@"first_name"];
    NSString *last_name = result[@"last_name"];
    NSString *profile_image = result[@"profile_image"];
    
    BOOL success = NO;
    int userID = 0;
    
    @try {
        
        Login* login = [Login new];
        
        login.overSocial = YES;
        
        login.url = [NSString stringWithFormat:@"%@/api/admin/auth/if3-social-signup", SYSTEM_DOMAIN];//[NSString stringWithFormat:@"%@social_signup.php", SECURED_API_URL];
        
        login.postData = @{
                           @"username": email,
                           @"client_id": client_id,
                           @"client": client,
                           @"first_name": first_name,
                           @"last_name": last_name,
                           @"profile_image": profile_image,
                           @"customer_id": [@(CUSTOMERID) stringValue]
                           };
        
        NSLog(@" opening url: %@", login.url);
        NSLog(@" post: %@", login.postData);
        
        localStorage *dynamicSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
        [dynamicSettingsStorage setObject:client_id forKey:@"client_id"];
        [dynamicSettingsStorage setObject:client forKey:@"client"];
        
        UNIHTTPJsonResponse* response = [login callAPI];
        
        NSInteger code = response.code;
        NSString* status = response.body.JSONObject[@"status"];
        NSDictionary* apiUserData = response.body.JSONObject[@"data"];
        
        NSLog(@" response => %@ ", [[NSString alloc] initWithData:response.rawBody encoding:NSUTF8StringEncoding]);
        NSLog(@" fetchUserInfo status => %@ ", status);
        NSLog(@" fetchUserInfo apiUserData => %@ ", apiUserData);
        
        
        self.loginEmail = apiUserData[@"username"];
        self.loginFirstName = first_name;
        self.loginLastName = last_name;
        userID = [apiUserData[@"user_id"] intValue];
        
        if (code >= 200 && code < 300 && [status isEqual:@"success"]) {
            [login markUserAsLoggedIn: apiUserData];
            success = YES;
        }
        
        if([status isEqual:@"error"]) {
            
            NSArray *error = response.body.JSONObject[@"message"];
            
            if ([error count] > 0) {
                
                if ([error[0] count] > 0) {
                    [Message alert: error[0][0] Title:NSLocalizedString(@"Error", @"") Tag:0];
                }
            }
        }
        
    } @catch (NSException * e) {
        
        NSLog(@"Exception: %@", e);
        [Message alert:@"" Title:NSLocalizedString(@"Error", @"") Tag:0];
        
    } @finally {
        
        if (success) {
            
            NSLog(@"fetch userinfo success => %d", success);
            
            [SSKeychain setPassword:self.loginPassword forService:@"PickalbatrosLogin" account: self.loginEmail];
            
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"userLoggedIn"];
            
            NSString *pwFromKeyChain = [SSKeychain passwordForService:@"PickalbatrosLogin" account:self.loginEmail];
            
            if ([SettingsViewController updateDynamicSettings:self.dynamicSettings forUserID:(int)userID validationPassword:pwFromKeyChain]) {
                
                [self.settingsStorage setObject:self.tableDataWithCheckIn forKey:@"settings"];
                [self.settingsStorage setObject:self.dynamicSettings forKey:@"dynamicSettings"];
                
            }
            
            return YES;
        }
        
        return NO;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    return sectionCellArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *sectionDict = self.tableData[indexPath.section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSDictionary *cellDict = sectionCellArray[indexPath.row];
    
    if([cellDict[@"type"] isEqualToString:@"separatorCell"]){
        return 30;
    } else if([cellDict[@"type"] isEqualToString:@"switchCell"]) {
        return 75;
    } else if([cellDict[@"type"] isEqualToString:@"confirmCell"]){
        return 112;
    } else if([cellDict[@"type"] isEqualToString:@"uploadImageCell"]){
        return 90;
    } else {
        return 45;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    
    RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
    
    rhvc.view.tag = 0;
    
    rhvc.labelHeaderTitle.text = sectionDict[@"title"];
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    [rhvc.view addGestureRecognizer:tgr];
    
    return rhvc.view;
}

-(void)hideKeyboard{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    if([sectionDict[@"title"] isEqualToString:@""]){
        return 0.1f;
    } else {
        return 30;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *sectionDict = self.tableData[indexPath.section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSDictionary *cellDict = sectionCellArray[indexPath.row];
    
    if([cellDict[@"type"] isEqualToString:@"separatorCell"]){
        RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"separatorCell"];
        cell.backgroundColor = rhvc.view.backgroundColor;
        
        rhvc.labelHeaderTitle.text = cellDict[@"label"];
        UIView *separatorView = rhvc.view;
        separatorView.height = 30;
        separatorView.tag = 0;
        
        separatorView.frame = cell.contentView.frame;
        
        [cell addSubview:separatorView];
        cell.separatorInset = UIEdgeInsetsMake(0, [UIScreen mainScreen].bounds.size.width, cell.height, 0);
        return cell;
    }
    
    RegisterTableViewCell *cell = (RegisterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellDict[@"type"]];
    
    if(indexPath.row!=sectionCellArray.count-1){
        if([sectionCellArray[indexPath.row+1][@"type"] isEqualToString:@"separatorCell"]){
            cell.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
        }
        else {
            cell.separatorInset = UIEdgeInsetsMake(cell.height, 15, 0, 0);
        }
    }
    else {
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }
    
    if ([cellDict[@"type"] isEqualToString:@"confirmCell"]) {
        cell.backgroundColor = [UIColor clearColor];
        
        NSString *ppString = [NSString stringWithFormat:NSLocalizedString(@"By registering you accept the %@.", @""),NSLocalizedString(@"privacy policy",@"")];
        
        NSDictionary *attributes = @{
                                     NSForegroundColorAttributeName: [UIColor grayColor],
                                     NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleNone],
                                     NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:14.0f],
                                     };
        
        NSMutableAttributedString *ppText = [[NSMutableAttributedString alloc] initWithString:ppString attributes:attributes];
        
        [ppText addAttributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle]} range:[ppString rangeOfString:NSLocalizedString(@"privacy policy",@"")]];
        
        
        cell.privacyPolicyLabel.attributedText = ppText;
        
        UIGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPrivacyPolicy)];
        
        [cell.privacyPolicyOverlayView addGestureRecognizer:tap];
    }
    
    if ([cellDict[@"type"] isEqualToString:@"uploadImageCell"]) {
        
        RegisterTableViewCell *cell = (RegisterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellDict[@"type"]];
        cell.uploadedImageView.image = self.uploadedImage;
        cell.uploadImageActivityIndicator.hidden = self.uploadImageActivityIndicatorHidden;
        
        // image is already uploaded or existing locally
        if(self.uploadedImage!=nil){
            // hide placeholder
            cell.uploadPlaceHolderImage.image = nil;
            cell.uploadImageActivityIndicator.hidden = YES;
            cell.uploadImageLabel.text = nil;
            NSString *userName = @"";
            if(![self stringEmpty:self.loginFirstName] && ![self stringEmpty:self.loginFirstName]){
                userName = [NSString stringWithFormat:@"%@ %@",self.loginFirstName,self.loginLastName];
            }
            else if(![self stringEmpty:self.loginLastName]){
                userName = self.loginFirstName;
            }
            else if(![self stringEmpty:self.loginLastName]){
                userName = self.loginLastName;
            }
            cell.userNameLabel.text = userName;
            cell.welcomeLabel.text = NSLocalizedString(@"Welcome", @"");
            cell.uploadImageLabel.text = nil;
        }
        // image is beeing uploaded
        else if(self.uploadingImage!=nil){
            // hide placeholder
            cell.uploadImageLabel.text = NSLocalizedString(@"Upload photo\nand  profit from\na special service", @"");
            cell.uploadPlaceHolderImage.image = nil;
            cell.uploadImageActivityIndicator.hidden = NO;
            cell.userNameLabel.text = nil;
            cell.welcomeLabel.text = nil;
        }
        else{
            NSLog(@"uploaded image is nil");
            cell.uploadImageLabel.text = NSLocalizedString(@"Upload photo\nand  profit from\na special service", @"");
            cell.uploadImageActivityIndicator.hidden = YES;
            cell.userNameLabel.text = nil;
            cell.welcomeLabel.text = nil;
        }
        
        [cell.uploadImageActivityIndicator startAnimating];
        return cell;
    }
    
    cell.accessoryType = [cellDict[@"cellAccessory"] intValue];
    cell.textInput.hidden = [cellDict[@"hideTextfield"] boolValue];
    cell.delegate = self;
    cell.textInput.tag = [cellDict[@"inputTag"] intValue];
    cell.textInput.delegate = self;
    cell.textInput.placeholder = cellDict[@"placeHolder"];
    cell.labelDescription.text = cellDict[@"label"];
    cell.textInput.text = cellDict[@"value"];
    cell.textInput.selectedIndexPath = indexPath;
    cell.textInput.dynamicSetting = [cellDict[@"dynamicSettings"] boolValue];
    
    
    if(cellDict[@"keyboardType"] != nil){
        [cell.textInput setKeyboardType:[cellDict[@"keyboardType"] intValue]];
    }
    
    cell.textInput.secureTextEntry = [cellDict[@"secureInput"] boolValue];
    
    if([cellDict[@"inputTag"] intValue] == 1){
        NSString *string = [self.loginPassword stringByReplacingOccurrencesOfString:@"\\d" withString:@"●" options:0 range:NSMakeRange(0, [self.loginPassword length])];
        
        cell.textInput.text = string;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableDictionary *sectionDict = self.tableData[indexPath.section];
    NSMutableArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSMutableDictionary *cellDict = sectionCellArray[indexPath.row];
    if(cellDict[@"onSelect"]) {
        ((void(^)()) cellDict[@"onSelect"])();
    }
        
    
    // image upload cell selected
    if(indexPath.section == 0 && indexPath.row == 0){
        // open image selection popup
        if(self.uploadedImage!=nil){
            self.imageSelectionActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Camera",@""),NSLocalizedString(@"Album",@""),NSLocalizedString(@"Delete photo",@""), nil];
            [self.imageSelectionActionSheet setDestructiveButtonIndex:2];
        }
        else {
            self.imageSelectionActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Camera",@""),NSLocalizedString(@"Album",@""), nil];
        }
        [self.imageSelectionActionSheet showInView:self.view];
        return;
    }

}

- (BOOL)textFieldShouldReturn:(UITextField*)aTextField{
    [aTextField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    RegisterTextField *nt = (RegisterTextField*)textField;
    
    NSIndexPath *indexPath = nt.selectedIndexPath;
    NSMutableDictionary *sectionDict = self.tableData[indexPath.section];
    NSMutableArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSMutableDictionary *cellDict = sectionCellArray[indexPath.row];
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if([cellDict[@"dynamicSettings"] boolValue]){

    } else {
        if(textField.tag == 0){
            self.loginEmail = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [textField setKeyboardType:UIKeyboardTypeEmailAddress];
            [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
        } else if (textField.tag == 1){
            self.loginPassword = newString;
            textField.secureTextEntry = YES;
        } else if (textField.tag == 3){
            self.loginFirstName = newString;
        } else if (textField.tag == 4){
            self.loginLastName = newString;
        }
    }
    
    if(textField.secureTextEntry != YES){
        [cellDict setObject:newString forKey:@"value"];
    }
    
    return YES;
}

-(void)switchSwitched:(BOOL)selected{

}

-(void) confirmButtonClicked {
    
    int success = 0;
    int userID = 0;
    
    NSString *firstName = self.loginFirstName == nil ? @"" : self.loginFirstName;
    NSString *lastName = self.loginLastName == nil ? @"" : self.loginLastName;
    NSString *loginName = self.loginEmail == nil ? @"" : self.loginEmail;
    
    @try {
        
        if ([loginName isEqualToString:@""] || [self.loginPassword isEqualToString:@""] ) {
        
            [Message alert:NSLocalizedString(@"Please insert your email address and password.",@"Error message if the user forgots one of those for the login") Title:NSLocalizedString(@"Error",@"") Tag:0];
            
        } else if (true) {
            NSString *post =[[NSString alloc] initWithFormat:@"username=%@&password=%@&firstName=%@&lastName=%@",loginName ,self.loginPassword, firstName, lastName];
            
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@user_registration.php?jsonRegister&customerID=%i",SECURED_API_URL, CUSTOMERID ]];
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            NSError *error;
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if ([response statusCode] >= 200 && [response statusCode] < 300)
            {
                NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"Response (confirmButtonClicked) ==> %@", responseData);
                
                NSError *error = nil;
                NSDictionary *jsonData = [NSJSONSerialization
                                          JSONObjectWithData:urlData
                                          options:NSJSONReadingMutableContainers
                                          error:&error];
                
                success = [jsonData[@"success"] intValue];
                userID = [jsonData[@"userID"] intValue];
                NSString *userToken = @"";
                
                if (success == 1) {
                    
                    [deviceTokenHandling updateDeviceToken:@"" forUserID:userID];
                    [[NSUserDefaults standardUserDefaults] setInteger:userID forKey:@"loggedInUserID"];
                    [[NSUserDefaults standardUserDefaults] setObject:loginName forKey:@"loggedInUserName"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    // set all other users as outlogged
                    NSString *logOutUserQuery = [NSString stringWithFormat:@"update users set is_logged_in = 0;"];
                    
                    // Execute the query.
                    [self.dbManager executeQuery:logOutUserQuery];
                    
                    // If the query was successfully executed then pop the view controller.
                    if (self.dbManager.affectedRows != 0) {
                        NSLog(@"all users marked as logged out");
                        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                        [loginManager logOut];
                    }
                    else{
                        NSLog(@"Could not execute the query.");
                    }
                    
                    localStorage *dynamicSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
                    
                    [dynamicSettingsStorage setObject:firstName forKey:@"firstName"];
                    [dynamicSettingsStorage setObject:lastName forKey:@"lastName"];
                    [dynamicSettingsStorage setObject:loginName forKey:@"email"];
                    // Prepare the query string.
                    NSString *query = [NSString stringWithFormat:@"insert into users (userID, login, first_name, last_name, is_logged_in, wants_check_in, security_token) values(%li, '%@', '%@', '%@', %i, %i, '%@');", (long)userID, loginName, firstName, lastName, 1, 1, userToken];

                    
                    // Execute the query.
                    [self.dbManager executeQuery:query];
                    
                    // If the query was successfully executed then pop the view controller.
                    if (self.dbManager.affectedRows != 0) {
                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                    }
                    else{
                        NSLog(@"Could not execute the query.");
                    }
                    
                } else {
                    NSString *error_msg = (NSString *) jsonData[@"error_message"];
                    
                    NSLog(@"Error should be shown: %@", error_msg);
                    [Message alert:error_msg Title:NSLocalizedString(@"Error", @"") Tag:0];
                }
                
            } else {
                [Message alert:NSLocalizedString(@"Connection interrupted.", @"") Title:NSLocalizedString(@"Error", @"") Tag:0];
            }
            
        } else {
            // password doesn't match
            [Message alert:NSLocalizedString(@"Passwords doesn't match.", @"Error if the confirmation password doesn't match the first password") Title:NSLocalizedString(@"Error", @"") Tag:0];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [Message alert:@"" Title:NSLocalizedString(@"Error",@"") Tag:0];
    }
    if (success) {
                
        [SSKeychain setPassword:self.loginPassword forService:@"PickalbatrosLogin" account:self.loginEmail];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"userLoggedIn"];
        
        NSString *pwFromKeyChain = [SSKeychain passwordForService:@"PickalbatrosLogin" account:self.loginEmail];
        
        // update settings and present next page
        if ([SettingsViewController updateDynamicSettings:self.dynamicSettings forUserID:(int)userID validationPassword:pwFromKeyChain]) {
            
            [self.settingsStorage setObject:self.tableDataWithCheckIn forKey:@"settings"];
            [self.settingsStorage setObject:self.dynamicSettings forKey:@"dynamicSettings"];
            
            if(([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue] != 0)) {
                
                if([[NSUserDefaults standardUserDefaults] boolForKey:@"itemsDownloaded"]) {
                    
                    StartTabBarViewController *tabController = [StartTabBarViewController new];
                    
                    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
                    
                    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
                    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                else {
                    LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
                    livc.delegate = self;
                    livc.skipUserInput = YES;
                    livc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
                    livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
                    livc.providesPresentationContextTransitionStyle = YES;
                    livc.definesPresentationContext = YES;
                    [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
                    
                    [self.navigationController presentViewController:livc animated:NO completion:^{}];
                    
                }
                
            } else {
                UIViewController *vc = [AppDelegate getStartViewController];

                
                SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:vc];
                
                rvc.modalPresentationStyle = UIModalPresentationFullScreen;
                rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:rvc animated:YES completion:^(void){[UIApplication sharedApplication].keyWindow.rootViewController = rvc;}];
            }
            
            [Message alert:@"" Title:NSLocalizedString(@"Successfully registered",@"Success message for registration") Tag:0];
        }
    }
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    UITableViewCell *cell;
    
    cell = (UITableViewCell *) textField.superview.superview;
    NSIndexPath *cellPath = [self.tableView indexPathForCell:cell];
    _currentPath = cellPath;
    [self.tableView scrollToRowAtIndexPath:cellPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    self.view.height = startingViewHeight - kbSize.height;
    [self.tableView layoutIfNeeded];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    
    self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.tableView layoutIfNeeded];
}

-(void)hideLoadingInformationView{
    
    StartTabBarViewController *tabController = [StartTabBarViewController new];
    
    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
    
    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark imagePicker delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.uploadingImage = selectedImage;
    self.uploadedImage = nil;
    NSLog(@"image info: %@",info);
    [self uploadImage:selectedImage];
    [self.tableView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)uploadImage:(UIImage*)image{
    self.uploadImageActivityIndicatorHidden = NO;
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    NSURL *uploadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@image_upload.php",SECURED_API_URL]];
    [self uploadFile:image forField:[NSString stringWithFormat:@"file"] URL:uploadURL parameters:nil];
}

- (void)uploadFile:(UIImage *)image
          forField:(NSString *)fieldName
               URL:(NSURL*)url
        parameters:(NSDictionary *)parameters{
    
    NSString *filename = @"profile_image.jpg";
    NSData *imageData = UIImageJPEGRepresentation(image, 85);
    
    NSMutableData *httpBody = [NSMutableData data];
    NSString *boundary = [self generateBoundaryString];
    NSString *mimetype = [self contentTypeForImageData:imageData];
    
    // configure the request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    if (imageData) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:imageData];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:httpBody];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError)
        {
            NSLog(@"sendAsynchronousRequest error=%@", connectionError);
            self.uploadedImage = nil;self.uploadingImage = nil;
            self.profileImageFileName = nil;
            
            self.uploadImageActivityIndicatorHidden = YES;
            
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            return;
        }
        
        [self uploadDone:data];
        
    }];
}
- (NSString *)generateBoundaryString{
    // generate boundary string
    //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    //
    // Note in iOS 6 and later, you can just:
    //
    //    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
    
    CFUUIDRef  uuid;
    NSString  *uuidStr;
    
    uuid = CFUUIDCreate(NULL);
    assert(uuid != NULL);
    
    uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    assert(uuidStr != NULL);
    
    CFRelease(uuid);
    
    return [NSString stringWithFormat:@"Boundary-%@", uuidStr];
}
- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

-(void)uploadDone:(NSData*)data{
    
    // parse the server output
    NSError *error;
    NSDictionary *jsonData = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:NSJSONReadingMutableContainers
                              error:&error];
    
    int success = [jsonData[@"success"] intValue];
    
    NSString *fileName = jsonData[@"fileName"];
    NSString *filePath = jsonData[@"filePath"];
    
    if(success){
        // updating the image cell, removing loading indicator and refresh this cell
        self.uploadedImage = self.uploadingImage;
        self.uploadImageActivityIndicatorHidden = YES;
        NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        self.profileImageFilePath = filePath;
        self.profileImageFileName = fileName;
        
        // store information about the image in the app (also save the image in the documents folder)
        NSString *fileName = self.profileImageFileName;
        NSString *filePath = (self.profileImageFilePath!=nil)?self.profileImageFilePath:@"";
        
        if([self stringEmpty:fileName]){
            [self.dynamicUserSettingsStorage removeObjectForKey:@"profileImageName"];
            [self.dynamicUserSettingsStorage removeObjectForKey:@"profileImagePath"];
        }
        else {
            [self.dynamicUserSettingsStorage setObject:fileName forKey:@"profileImageName"];
            [self.dynamicUserSettingsStorage setObject:filePath forKey:@"profileImagePath"];
            NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            imageMethods *images = [[imageMethods alloc] init];
            NSString *imageType = @"";
            if([fileName hasSuffix:@".png"] ||
               [fileName hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([fileName hasSuffix:@".jpg"] ||
                    [fileName hasSuffix:@".jpeg"] ||
                    [fileName hasSuffix:@".JPEG"] ||
                    [fileName hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            [images saveImage:self.uploadedImage withFileName:fileName ofType:imageType inDirectory:documentsPath];
        }
    }
    
    NSLog(@"success: %i\nfileName: %@",success,fileName);
    
}

-(UIImage*)getProfileImageForImageName:(NSString*)imageName serverPath:(NSString*)serverPath{
    
    imageMethods *images = [[imageMethods alloc] init];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    UIImage *cellImage = nil;
    
    if(![imageName isEqualToString:@""] && imageName != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageName]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageName ofType:@"png" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageName ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else {
            //Get Image From URL
            
            
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@%@",SYSTEM_DOMAIN,serverPath,imageName]];
            NSString *imageType = @"";
            if([imageName hasSuffix:@".png"] ||
               [imageName hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([imageName hasSuffix:@".jpg"] ||
                    [imageName hasSuffix:@".jpeg"] ||
                    [imageName hasSuffix:@".JPEG"] ||
                    [imageName hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageName ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageName ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
            return cellImage;
        }
        
    }
    
    return cellImage;
}


-(BOOL)stringEmpty:(NSString*)string{
    if(string==nil){
        return true;
    }
    
    if([string isEqualToString:@""]){
        return true;
    }
    
    if([string isKindOfClass:[NSNull class]]){
        return true;
    }
    
    return false;
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(actionSheet==self.imageSelectionActionSheet){
        if(buttonIndex==2){
            self.uploadedImage = nil;self.uploadingImage = nil;
            self.profileImageFileName = nil;
            
            self.uploadImageActivityIndicatorHidden = YES;
            
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            return;
        }
        else if(buttonIndex==0 || buttonIndex==1){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.navigationBar.tintColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
            picker.navigationBar.barTintColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];
            
            [picker.navigationBar setTitleTextAttributes: @{
                                  NSForegroundColorAttributeName: picker.navigationBar.tintColor,
                                  NSFontAttributeName: [UIFont fontWithName:@"Arial" size:17.0f]
                                  }];
            
            picker.delegate = self;
            picker.allowsEditing = YES;
            if(buttonIndex==0){
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                }
            }
            if(buttonIndex==1){
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
            
            UIView *navigationBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
            navigationBackgroundView.backgroundColor = [UIColor whiteColor];
            
            [picker.navigationController.view addSubview:navigationBackgroundView];
            
            [self presentViewController:picker animated:YES completion:nil];
        }
        return;
    }
}


-(void)showPrivacyPolicy{
    WebViewController *wvc = [WebViewController getWebViewController];
    wvc.url = [NSURL URLWithString:@"http://ardnd.com/datenschutz/"];
    wvc.urlGiven = YES;
    
    wvc.titleString = NSLocalizedString(@"privacy policy",@"");
    
    [self.navigationController pushViewController:wvc animated:YES];
}

@end
