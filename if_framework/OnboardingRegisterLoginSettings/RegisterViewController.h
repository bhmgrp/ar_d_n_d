//
//  RegisterViewController.h
//  KAMEHA
//
//  Created by User on 19.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RegisterTableViewCell.h"
#import "RegisterTextField.h"
#import "SidebarViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface RegisterViewController : UIViewController <
    UITableViewDataSource,
    UITableViewDelegate,
    UITextFieldDelegate,
    registerCellDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UIActionSheetDelegate
>

@property (nonatomic) SidebarViewController *sbvc;

@property (nonatomic) NSString* profileImageFileName;
@property (nonatomic) NSString* profileImageFilePath;
@property (nonatomic) UIImage *uploadedImage;
@property (nonatomic) UIImage *uploadingImage;
@property (nonatomic) NSString *uploadImageLabelText;
@property (nonatomic) BOOL uploadImageActivityIndicatorHidden;

- (BOOL) fetchUserInfo: (NSDictionary*) result;

@end
