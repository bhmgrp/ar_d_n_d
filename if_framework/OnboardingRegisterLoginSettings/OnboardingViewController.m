//
// Created by Nicolas Tichy on 1/30/15.
// Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "OnboardingViewController.h"
#import "OnboardingCollectionViewCell.h"
#import "SWRevealViewController.h"
#import "PlaceOverViewController.h"
#import "StructureOverViewController.h"
#import "NavigationViewController.h"
#import "RegisterViewController.h"
#import "loginViewController.h"
#import "LoadingInformationViewController.h"
#import "StartTabBarViewController.h"
#import "RegisterViewController.h"

#import "MBProgressHUD.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "AppDelegate.h"


@interface OnboardingViewController () <loadingInformationProtocol>

@property (strong, nonatomic) NSArray *array;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *buttonSignUpOrLogIn;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UIButton *buttonRegister;

@end


@implementation OnboardingViewController
{
    
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setupCollectionView];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    [[UINavigationBar appearance]setBarTintColor:[UIColor blackColor]];
    self.navigationController.navigationBar.translucent = YES;
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes: @{
                                                                }];
    self.navigationItem.title = @"";
    
    self.buttonRegister.layer.cornerRadius = 5.0f;
    self.buttonRegister.layer.masksToBounds = YES;
    
    self.buttonLogin.layer.cornerRadius = 5.0f;
    self.buttonLogin.layer.masksToBounds = YES;
    
    [self.skipButton setTitle:NSLocalizedString(@"Proceed without registration", @"Skip button for onboarding page") forState:UIControlStateNormal];
    [self.registerButton setTitle:NSLocalizedString(@"Register",@"") forState:UIControlStateNormal];
    [self.loginButton setTitle:NSLocalizedString(@"Login_startscreen", @"") forState:UIControlStateNormal];
    [self.buttonLogin setTitle:NSLocalizedString(@"Login_startscreen", @"") forState:UIControlStateNormal];
    
    [self.liButton setTitle:NSLocalizedString(@"LI_button", @"Login with LinkedIn") forState:UIControlStateNormal];
    [self.fbButton setTitle:NSLocalizedString(@"FB_button", @"Login with Facebook") forState:UIControlStateNormal];
    
    self.array = @[@"onboarding_01",@"onboarding_02",@"onboarding_03"];
    
    if ([[languages currentLanguageKey] isEqualToString:@"de"]) {
        self.array = @[@"onboarding_01_de",@"onboarding_02_de", @"onboarding_03_de"];
    }
    
    self.pageControl.numberOfPages = self.array.count;
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    
    self.collectionView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

// Once the button is clicked, show the login dialog
- (IBAction)fbSignUpButtonClicked:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             
         } else if (result.isCancelled) {
             
         } else {
             
             if ([result.grantedPermissions containsObject:@"email"])
             {
              
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                        
                          BOOL success = NO;
                          
                          [miscFunctions showLoadingAnimationForView:self.view];
                          
                          if (!error)
                          {
                              RegisterViewController *signup = [[RegisterViewController alloc] init];
                              
                              success = [signup fetchUserInfo:@{
                                                      @"email": result[@"email"],
                                                      @"first_name": result[@"first_name"],
                                                      @"last_name": result[@"last_name"],
                                                      @"profile_image": result[@"picture"][@"data"][@"url"],
                                                      @"client_id": result[@"id"],
                                                      @"client": @"facebook",
                                                      }];
                              
                              if(success) {
                                  [self afterLogin];
                                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                              } else {
                                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                 FBSDKLoginManager *fblogin = [[FBSDKLoginManager alloc] init];
                                 [fblogin logOut];
                              }
 
                          }
                          else
                          {
                              [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                              NSLog(@"Error %@",error);
                              FBSDKLoginManager *fblogin = [[FBSDKLoginManager alloc] init];
                              [fblogin logOut];
                          }
                      }];
                 }
 
             }
         }
     }];
}

- (IBAction)liSignUpButtonClicked:(id)sender {
    
    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil]
                                         state:@"some state"
                        showGoToAppStoreDialog:YES
                                  successBlock:^(NSString *returnState) {
                                      
                                      NSString *url = [NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,formatted-name,location,email-address,picture-url)?format=json"];
                                      
                                      [miscFunctions showLoadingAnimationForView:self.view];
                                      
                                      if ([LISDKSessionManager hasValidSession]) {
                                          
                                          [[LISDKAPIHelper sharedInstance] getRequest:url
                                                                              success:^(LISDKAPIResponse *response) {
                                                                                  
                                                                                  dispatch_async(dispatch_get_main_queue(), ^(void){
                                                                                      
                                                                                      BOOL success = NO;
                                                                                      
                                                                                      NSError *error;
                                                                                      NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[response.data dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO] options:NSJSONReadingAllowFragments error:&error];
                                                                                      
                                                                                      RegisterViewController *signup = [[RegisterViewController alloc] init];
                                                                                      
                                                                                      success = [signup fetchUserInfo:@{
                                                                                                                        @"email": jsonDict[@"emailAddress"],
                                                                                                                        @"first_name": jsonDict[@"firstName"],
                                                                                                                        @"last_name": jsonDict[@"lastName"],
                                                                                                                        @"profile_image": jsonDict[@"pictureUrl"],
                                                                                                                        @"client_id": jsonDict[@"id"],
                                                                                                                        @"client": @"linkedin",
                                                                                                                        }];
                                                                                      
                                                                                      if(success) {
                                                                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                                                          [self afterLogin];
                                                                                      }
                                                                                      else {
                                                                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                                                      }
                                                                                      
                                                                                      
                                                                                  });
                                                                                  
                                                                                  
                                                                              }
                                                                                error:^(LISDKAPIError *apiError) {
                                                                                    NSLog(@" error LIAPI => %@ ", apiError);
                                                                                }];
                                      }
                                      else {
                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                      }
                                  }
                                    errorBlock:^(NSError *error) {
                                        NSLog(@"%s %@","error called! ", [error description]);
                                    }
     ];
}

- (void) afterLogin {
    
    if(([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue] != 0)) {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"itemsDownloaded"]) {
            
            self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
            self.sbvc.view.tag = 1;
            
            StartTabBarViewController *tabController = [StartTabBarViewController new];
            
            SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
            
            rvc.modalPresentationStyle = UIModalPresentationFullScreen;
            rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
            } completion:^(BOOL finished) {
                
            }];
            
        } else {
            
            LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
            livc.delegate = self;
            livc.skipUserInput = YES;
            livc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
            livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
            livc.providesPresentationContextTransitionStyle = YES;
            livc.definesPresentationContext = YES;
            [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self.navigationController presentViewController:livc animated:NO completion:^{}];
            
        }
        
    } else {
        
        self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
        self.sbvc.view.tag = 1;
        
        UIViewController *vc = [AppDelegate getStartViewController];
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:nvc];
        
        rvc.modalPresentationStyle = UIModalPresentationFullScreen;
        rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
        } completion:^(BOOL finished) {
            
        }];
    }

}

-(void)viewDidLayoutSubviews{
    [self.buttonRegister addViewWithColor:[UIColor whiteColor] alpha:0.7f behindSelfInView:self.buttonRegister.superview];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = [self horizontalPageNumber:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        self.pageControl.currentPage = [self horizontalPageNumber:scrollView];
    }
}

- (NSInteger)horizontalPageNumber:(UIScrollView *)scrollView
{
    CGPoint contentOffset = scrollView.contentOffset;
    CGSize viewSize = scrollView.bounds.size;

    NSInteger horizontalPage = (NSInteger) MAX(0.0, contentOffset.x / viewSize.width);

    return horizontalPage;
}

-(void)setupCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OnboardingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OnboardingCollectionViewCell" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:self.array[(NSUInteger) indexPath.row]];
    return cell;
}

- (IBAction)buttonRegisterClicked:(id)sender {
    RegisterViewController *rvc = [RegisterViewController loadNowFromStoryboard:@"LoginAndRegister"];
    
    [self.navigationController.navigationBar setTitleTextAttributes: @{
                                                                       NSForegroundColorAttributeName: [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR],
                                                                       NSFontAttributeName: [UIFont fontWithName:@"Arial" size:17.0f]
                                                                       }];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];

    [self.navigationController pushViewController:rvc animated:YES];
}

- (IBAction)buttonLoginClicked:(id)sender {
    
    loginViewController *vc = [loginViewController loadNowFromStoryboard:@"LoginAndRegister"];
    
    [self.navigationController.navigationBar setTitleTextAttributes: @{
                                                                       NSForegroundColorAttributeName: [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR],
                                                                       NSFontAttributeName: [UIFont fontWithName:@"Arial" size:17.0f]
                                                                       }];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showMainViewController
{

}

- (IBAction)buttonSkipTouched:(id)sender
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Without registering you can not receive a special service",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:NSLocalizedString(@"OK",@"") , nil];
    
    [av show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{

    if(buttonIndex != 0){
        NSLog(@"checking");
        if(([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue] != 0)){
            NSLog(@"place id has been selected");
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"itemsDownloaded"]){
                NSLog(@"items have been downloaded");
                self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
                self.sbvc.view.tag = 1;
                
                StartTabBarViewController *tabController = [StartTabBarViewController new];
                
                SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
                
                rvc.modalPresentationStyle = UIModalPresentationFullScreen;
                rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
                } completion:^(BOOL finished) {
                    
                }];
                
                return;
                
            }
            else {
                LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
                livc.delegate = self;
                livc.skipUserInput = YES;
                livc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
                livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
                livc.providesPresentationContextTransitionStyle = YES;
                livc.definesPresentationContext = YES;
                [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];

                
                [self.navigationController presentViewController:livc animated:NO completion:^{}];
                
            }
            
        } else {
            self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
            self.sbvc.view.tag = 1;
            
            UIViewController *vc = [AppDelegate getStartViewController];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
            
            SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:nvc];
            
            rvc.modalPresentationStyle = UIModalPresentationFullScreen;
            rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
            } completion:^(BOOL finished) {
                
            }];
            
            
        }
    }
}

-(void)hideLoadingInformationView{
    
    self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
    self.sbvc.view.tag = 1;
    
    
    StartTabBarViewController *tabController = [StartTabBarViewController new];
    
    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
    
    
    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [UIView transitionWithView:[UIApplication sharedApplication].keyWindow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
    } completion:^(BOOL finished) {
        
    }];

}

@end
