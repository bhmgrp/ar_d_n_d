//
// Created by Nicolas Tichy on 2/5/15.
// Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomPageControl : UIPageControl

@property(nonatomic, retain) UIImage* activeImage;
@property(nonatomic, retain) UIImage* inactiveImage;

@end