//
//  SidebarViewController.m
//  pickalbatros
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "SidebarViewController.h"
#import "RegisterHeaderViewController.h"
#import "MBProgressHUD.h"
#import "SettingsViewController.h"
#import "NavigationViewController.h"
#import "PlaceOverViewController.h"
#import "StructureOverViewController.h"
#import "OnboardingViewController.h"
#import "WebViewController.h"
#import "notificationListViewController.h"
#import "SidebarTableViewCell.h"
#import "OfferViewController.h"

#import "StartTabBarViewController.h"

#import "ImageViewController.h"

#import "AppDelegate.h"

#import "imageMethods.h"

#import "DBManager.h"

@interface SidebarViewController (){
    imageMethods *images;
    NSString *documentsPath;

    NSMutableArray *_tableData;
    NSMutableDictionary *_treestructuresSection;
    NSMutableArray *_treestructuresSectionCells;
    NSMutableArray *_checkoutSectionCells;
    NSMutableDictionary *_checkoutSection;
}

@property (nonatomic) NSDictionary *selectedItem;

@property (nonatomic) BOOL startMarked;

@property (nonatomic) localStorage *placesStorage, *structuresStorage;

@property (nonatomic) DBManager *dbManager;

@property (nonatomic) NSString *unreadMessagesBadgeString;

@end

@implementation SidebarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    images = [[imageMethods alloc] init];
    documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    self.placesStorage = [localStorage storageWithFilename:@"places"];
    self.structuresStorage = [localStorage storageWithFilename:@"structures"];
    
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadSidebarData) name:@"sidebarUpdate" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationUpdate) name:NOTIFICATION_CAME_IN object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadSidebarData) name:TERMINAL_MODE_UPDATE object:nil];
    
    
    _unreadMessagesBadgeString = [NSString stringWithFormat:@"%i",[self getAmountOfMessagesFromDB]];
    
    
    [self loadSidebarData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _tableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *cellData = _tableData[indexPath.section][@"sectionCells"][indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    SidebarTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:cellData[@"identifier"]];
    if(cellData[@"selected"] && !_startMarked){
        // ouch
        _startMarked = YES;
    }
    c.itemImageView.image = [self getSidebarImageForString:cellData[@"icon"]];
   
    c.pointLabel.text = cellData[@"cellName"];
    c.messagesAmountLabel.text = _unreadMessagesBadgeString;
    
    if(c != nil)cell = c;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_tableData[section][@"sectionCells"] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int add = (SINGLEPLACEID==0)?1:0;
    
    if(indexPath.section==2){
        if(indexPath.row < NUMBEROFTABSINTABVIEW){
            StartTabBarViewController *tabController = [StartTabBarViewController new];
            
            tabController.selectedIndex = indexPath.row+add;
            
            [self.revealViewController pushFrontViewController:tabController animated:YES];
            
            return;
        }
    }
    
    NSDictionary *cellData = _tableData[indexPath.section][@"sectionCells"][indexPath.row];
    
    if ([cellData[@"open"] isEqualToString:@"settings"]) {
        
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1) {
            
            SettingsViewController *svc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:svc];
            
            svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:svc.revealViewController action:@selector(revealToggle:)];
            [svc.navigationItem.leftBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
            svc.settingType.text = @"generalSettings";
            [self.revealViewController pushFrontViewController:nvc animated:YES];
            
        } else {
            OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
            
            [self.revealViewController pushFrontViewController:ovc animated:YES];
            if(SINGLEPLACEID==0)[[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"selectedPlaceID"];
        }
    }
    
    if ([cellData[@"open"] isEqualToString:@"start"]) {
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != 0){
            
            
            StartTabBarViewController *tabController = [StartTabBarViewController new];
            
            [self.revealViewController pushFrontViewController:tabController animated:YES];

        } else {
            PlaceOverViewController *povc = [PlaceOverViewController loadNowFromStoryboard:@"PlaceOverView"];
            povc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
            povc.showSidebarButton = YES;
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:povc];
            [self.revealViewController pushFrontViewController:nvc animated:YES];
        }

    }
    if([cellData[@"open"] isEqualToString:@"structure"]){
        NSDictionary *jsonElement = cellData[@"jsonElement"];
        if([jsonElement[@"description"] hasPrefix:@"ifbck"]){
            _selectedItem = jsonElement;
            [self openViewForUrl:jsonElement[@"description"]];
            return;
        }
        if([jsonElement[@"description"] hasPrefix:@"http"]){
            
            // check if it's an ifeedback url
            NSString *regex = @"http(s)?://([a-zA-Z0-9]+\\.)?ifbck.com/.+";
            
            NSPredicate *testString = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if([testString evaluateWithObject:jsonElement[@"description"]]){
                [self loadIfeedbackModelWithUrl:jsonElement[@"description"] title:jsonElement[@"name"]];
                return;
            }
            
            WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
            wvc.url = [NSURL URLWithString:jsonElement[@"description"]];
            wvc.urlGiven = YES;
                        wvc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:wvc.revealViewController action:@selector(revealToggle:)];
            wvc.titleString = jsonElement[@"name"];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:wvc];
            [self.revealViewController pushFrontViewController:nvc animated:YES];
        } else {
            MasterViewControllerNew *mvcn = [MasterViewControllerNew loadNowFromStoryboard:@"TreeStructure"];
            mvcn.elementID = [cellData[@"root_id"] intValue];
            mvcn.navigationItem.titleView = [NavigationTitle createNavTitle:cellData[@"cellName"] SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
            mvcn.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:mvcn.revealViewController action:@selector(revealToggle:)];
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:mvcn];
            [self.revealViewController pushFrontViewController:nvc animated:YES];
        }
    }
    if([cellData[@"open"] isEqualToString:@"messages"]){
        //new instance of webViewController
        notificationListViewController *wv = [notificationListViewController loadNowFromStoryboard:@"Notification"];
        wv.showSidebarButton = YES;
        
        //go to instance
        
        UIViewController* dvc = wv;
        NavigationViewController* nc = [[NavigationViewController alloc] initWithRootViewController:dvc];
        
        [self.revealViewController pushFrontViewController:nc animated:YES];

    }
    if([cellData[@"open"] isEqualToString:@"web"]){
        if([cellData[@"url"] hasPrefix:@"tel:"] || [cellData[@"url"] hasPrefix:@"mailto:"]){
            UIWebView *wv = [[UIWebView alloc] init];
            wv.alpha = 0.0f;
            [self.view addSubview:wv];
            [wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cellData[@"url"]]]];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            return;
        }
        
        // check if it's an ifeedback url
        NSString *regex = @"http(s)?://([a-zA-Z0-9]+\\.)?ifbck.com/.+";
        
        NSPredicate *testString = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        
        if([testString evaluateWithObject:cellData[@"url"]]){
            [self loadIfeedbackModelWithUrl:cellData[@"url"] title:cellData[@"cellName"]];
            return;
        }
        
        WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
        wvc.url = [NSURL URLWithString:cellData[@"url"]];
        wvc.urlGiven = YES;
        wvc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:wvc.revealViewController action:@selector(revealToggle:)];
        wvc.titleString = cellData[@"cellName"];
        
        if(![cellData[@"html"] isEqualToString:@""] && cellData[@"html"] != nil){
            wvc.htmlString = cellData[@"html"];
            wvc.useHTML = YES;
        }
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:wvc];
        [self.revealViewController pushFrontViewController:nvc animated:YES];
    }
    if([cellData[@"open"] isEqualToString:@"locations"]){
        
        StartTabBarViewController *tabController = [StartTabBarViewController new];
        
        tabController.selectedIndex = 0;
        
        [self.revealViewController pushFrontViewController:tabController animated:YES];
        
    }
    
    // special stuff for kameha
    if([cellData[@"open"] isEqualToString:@"kameha_pass"]){

    }
    
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSString *sectionTitle = _tableData[section][@"sectionTitle"];
    RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
    
    rhvc.view.tag = 0;
    
    rhvc.labelHeaderTitle.text = sectionTitle;
    
    return rhvc.view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString *sectionTitle = _tableData[section][@"sectionTitle"];
    if([sectionTitle isEqualToString:@""] || sectionTitle == nil){
        return 0;
    } else {
        return 30;
    }
}

- (void)downloadItems
{
    // Download the json file
    
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_jsonFileUrl];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
    
    // Loop through Json objects, create question objects and add them to our questions array
//    NSString *url = [NSString stringWithFormat:@"%@/kameha-app",SYSTEM_DOMAIN];
//    
//    if(IDIOM == IPAD){
//        url = [NSString stringWithFormat:@"%@/kameha-app-t",SYSTEM_DOMAIN];
//    }
    
    _treestructuresSectionCells = [@[] mutableCopy];
//    [_treestructuresSectionCells addObject:@{@"cellName":@"iFeedback®",
//                                             @"icon":@"sb_feedback",
//                                             @"open":@"web",
//                                             @"url":url,
//                                             @"identifier":@"sideBarPoint"}];
    
     int numOfElements = (int)[jsonArray count];
    int i = 0;
    
     for (i = 0; i < numOfElements; i++){
         NSDictionary *jsonElement = jsonArray[i];
         NSString *cellName = jsonElement[@"name"];
         NSString *cellIcon = [NSString stringWithFormat:@"%@-%@",jsonElement[@"tstamp"],jsonElement[@"uid"]];
         
         if(!([jsonElement[@"hide_in_terminal_mode"] boolValue] && [globals sharedInstance].terminalMode))
             [_treestructuresSectionCells addObject:@{
                                                      @"cellName":cellName,
                                                      @"icon":cellIcon,
                                                      @"open":@"structure",
                                                      @"root_id":jsonElement[@"root_element_id"],
                                                      @"identifier":@"sideBarPoint",
                                                      @"jsonElement":jsonElement
                                                      }];
     }
    if(i>0){
        //_treestructuresSection = [@{@"sectionTitle":@"Hotel",@"sectionCells":_treestructuresSectionCells} mutableCopy];
        //[_tableData insertObject:_treestructuresSection atIndex:2];
        
        [_tableView reloadData];
    }

}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"Connection failed, Error: %@" ,error);
    [[[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

-(void)loadSidebarData{
    //int singlePlaceID = SINGLEPLACEID;
    
    NSMutableArray *tempData = [@[@{@"sectionTitle":@"",
                            @"sectionCells":@[
                                    @{@"cellName":NSLocalizedString(@"Start",@""),
                                      @"icon":@"sb_home",
                                      @"open":@"start",
                                      @"selected":@1,
                                      @"identifier":@"sideBarPoint",},
                                    ]
                            }] mutableCopy];
    _tableData = [tempData mutableCopy];
    
    
    //NSMutableArray *sectionCells = [@[] mutableCopy];
//
//    if(![globals sharedInstance].terminalMode)
//        [sectionCells addObject:@{@"cellName":NSLocalizedString(@"Profile",@""),
//                                  @"icon":@"sb_profile",
//                                  @"open":@"settings",
//                                  @"identifier":@"sideBarPoint",}];
//
//    [sectionCells addObject:@{@"cellName":@"Angebote verwalten",
//                              @"icon":@"ardnd_icon_template",
//                              @"open":@"web",
//                              @"url":@"https://ifbck.com/admin/ar-d-n-d/login",
//                              @"identifier":@"sideBarPoint"
//                              }];
//
//
//    if(![globals sharedInstance].terminalMode && singlePlaceID==0)
//        [sectionCells addObject:@{@"cellName":NSLocalizedString(@"Location",@""),
//                                  @"icon":@"sb_location",
//                                  @"open":@"locations",
//                                  @"identifier":@"sideBarPoint",}];
//
//        [sectionCells addObject:@{@"cellName":NSLocalizedString(@"Messages",@""),
//                                  @"icon":@"sb_messages",
//                                  @"open":@"messages",
//                                  @"identifier":@"sideBarPointMessages",
//                                  @"icon":@"sb_messages",
//                                  @"unreadMessages":_unreadMessagesBadgeString}];
//
//    tempData = [@{@"sectionTitle":NSLocalizedString(@"Account",@""),
//                                @"sectionCells":sectionCells
//                                } mutableCopy];

    
//    [_tableData addObject:tempData];
    
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] != nil && [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedPlaceID"] != 0){
        
        
        int placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
        
        NSArray *tempArray = (NSArray*)[self.structuresStorage objectForKey:[NSString stringWithFormat:@"structureListForPlace%i",placeID]];
        
        if(tempArray == nil || tempArray.count == 0){
            
            
            // defining the URL
            NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonStructuresForPlace=%i&languageID=%i", API_URL, IFBCKPID, placeID, [languages currentLanguageID]];
            // Set the URL where we load from
            
            _jsonFileUrl = [NSURL URLWithString:urlString];
            
            [self downloadItems];
        } else {
            int i = 0;
            
            
//            NSString *url = [[globals sharedInstance] getIFeedbackLink];
            
            int numOfElements = (int)[tempArray count];
            _treestructuresSectionCells = [@[] mutableCopy];
//            [_treestructuresSectionCells addObject:@{@"cellName":@"iFeedback®",
//                                                     @"icon":@"sb_feedback",
//                                                     @"open":@"web",
//                                                     @"url":url,
//                                                     @"identifier":@"sideBarPoint"}];
            for (i = 0; i < numOfElements; i++){
                NSDictionary *jsonElement = tempArray[i];
                NSString *cellName = jsonElement[@"name"];
                NSString *structureID = jsonElement[@"root_element_id"];
                NSString *cellIcon = [NSString stringWithFormat:@"%@-%@",jsonElement[@"tstamp"],jsonElement[@"uid"]];
                
                if(!([jsonElement[@"hide_in_terminal_mode"] boolValue] && [globals sharedInstance].terminalMode))
                    [_treestructuresSectionCells addObject:@{
                                                             @"cellName":cellName,
                                                             @"icon":cellIcon,
                                                             @"open":@"structure",
                                                             @"root_id":structureID,
                                                             @"jsonElement":jsonElement,
                                                             @"identifier":@"sideBarPoint"
                                                             }];
            }
            if(i>0){
                //_treestructuresSection = [@{@"sectionTitle":NSLocalizedString(@"Hotel",@""),@"sectionCells":_treestructuresSectionCells} mutableCopy];
                //[_tableData addObject:_treestructuresSection];
                
            }
        }
        
        NSArray *tempCheckoutSectionsArray = [[localStorage storageWithFilename:@"checkout"] objectForKey:[NSString stringWithFormat:@"checkoutElementsForPlace%i",placeID]];
        
        for(NSDictionary *tempCheckoutDictionary in tempCheckoutSectionsArray){
            NSArray *tempCheckoutCells = tempCheckoutDictionary[@"sectionCells"];
            _checkoutSectionCells = [[NSMutableArray alloc] init];
            int i = 0;
            for (NSDictionary *jsonElement in tempCheckoutCells){
                if([jsonElement isKindOfClass:[NSArray class]]){
                    continue;
                }
                NSString *cellName = jsonElement[@"label"];
                NSString *url = jsonElement[@"url"];
                NSString *html = jsonElement[@"html"];
                NSString *cellIcon = (![jsonElement[@"icon"] isKindOfClass:[NSNull class]] && jsonElement[@"icon"] != nil)?jsonElement[@"icon"]:@"";
                [self loadImageWithName:cellIcon];
                
        
                if(!([jsonElement[@"hide_in_terminal_mode"] intValue] && [globals sharedInstance].terminalMode)){
                    [_checkoutSectionCells addObject:@{
                                                       @"cellName":cellName,
                                                       @"icon":cellIcon,
                                                       @"open":@"web",
                                                       @"url":url,
                                                       @"jsonElement":jsonElement,
                                                       @"html":html,
                                                       @"identifier":@"sideBarPoint"
                                                       }];
                    i++;
                }
            }
            if(i>0){
                _checkoutSection = [@{@"sectionTitle":tempCheckoutDictionary[@"sectionTitle"],@"sectionCells":_checkoutSectionCells} mutableCopy];
                [_tableData addObject:_checkoutSection];
            }
            
        }
        [_tableView reloadData];
    }
}

-(int)getAmountOfMessagesFromDB{
    NSString *query = @"select * from notifications WHERE has_been_read = 0";
    
    NSArray *tempArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    int amount = (int) tempArray.count;
    NSLog(@"amount of notifications: %i",amount);
    return amount;
}

-(void)notificationUpdate{
    int unread = [self getAmountOfMessagesFromDB];
    NSLog(@"updating unread messages: %i",unread);
    _unreadMessagesBadgeString = [NSString stringWithFormat:@"%i", unread];
    [self.tableView reloadData];
}

-(UIImage*)loadImageWithName:(NSString*)imageName{
    NSString *imageNameFromTstamp = [[imageName lastPathComponent] stringByDeletingPathExtension];
    UIImage *cellImage = nil;
    NSString *structureImageSm = imageName;
    if(![structureImageSm isEqualToString:@""] && structureImageSm != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            return cellImage;
        }
        else {
            //Get Image From URL
            if(![structureImageSm hasPrefix:@"files"] && ![structureImageSm hasPrefix:@"/files"]){
                return nil;
            }
            
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm,24*3.0]];
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
            return cellImage;
        }
        
    }
    
    return cellImage;
}

-(UIImage*)getSidebarImageForString:(NSString*)imageString{
    
    if([imageString isEqualToString:@""]){
        return nil;
    }
    
    UIImage* returnImage = nil;
    returnImage = [UIImage imageNamed:imageString];
    
    if([imageString hasPrefix:@"http"]){
        imageString = [imageString stringByReplacingOccurrencesOfString:SECURED_API_URL withString:@""];
        imageString = [imageString stringByReplacingOccurrencesOfString:@"https://" withString:@""];
    }

    
    if(returnImage==nil){
        returnImage = [self loadImageWithName:imageString];
    }
    
    return returnImage;
}

-(void)openViewForUrl:(NSString*)viewUrl{
    
    NSString *classString = [viewUrl stringByReplacingOccurrencesOfString:@"ifbck://" withString:@""];
    
    if([classString isEqualToString:@"ImageViewController"]){
        if([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1){
            Class theClass = NSClassFromString(classString);
            UIViewController *vc = (UIViewController*)[theClass loadNowFromStoryboard:classString];
            
            ImageViewController *ivc = (ImageViewController*)vc;
            
            ivc.imageName = _selectedItem[@"image_big"];
            
            [self.navigationController pushViewController:ivc animated:YES];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
            vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:vc.revealViewController action:@selector(revealToggle:)];
            [self.revealViewController pushFrontViewController:nvc animated:YES];
        } else {
            OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];

            [self.revealViewController pushFrontViewController:ovc animated:YES];

        }
        return;
    }
    
    
    Class theClass = NSClassFromString(classString);
    UIViewController *vc = [theClass loadNowFromStoryboard:classString];
    
    if(vc!=nil){
        
        vc.navigationItem.titleView = [NavigationTitle createNavTitle:_selectedItem[@"name"] SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
                
        if([vc isKindOfClass:[OfferViewController class]]){
            OfferViewController *ovc = (OfferViewController*)vc;
            int currentlySelectedOfferCampaignID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedOfferCampaignID"] intValue];
            
            int selectedOfferCampaignID = [_selectedItem[@"campaign_id"] intValue];
            
            ovc.offerCampaignID = selectedOfferCampaignID;
            
            ovc.indexToOpen = 1;
            
            if(currentlySelectedOfferCampaignID!=selectedOfferCampaignID){
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:selectedOfferCampaignID] forKey:@"selectedOfferCampaignID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        
            vc = ovc;
        }
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:vc.revealViewController action:@selector(revealToggle:)];
        [self.revealViewController pushFrontViewController:nvc animated:YES];

    }
    else {
        NSString *versionString = [NSString stringWithFormat:@"v.%@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        
        [[[UIAlertView alloc] initWithTitle:@"Could not open" message:[NSString stringWithFormat:@"Maybe the app is not up to date?\n%@",versionString] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
}

-(void)revealController:(SWRevealViewController *)revealController didAddViewController:(UIViewController *)viewController forOperation:(SWRevealControllerOperation)operation animated:(BOOL)animated{

}


-(void)loadIFeedbackModelWithUrl:(NSString*)url{
    [self loadIfeedbackModelWithUrl:url title:nil];
}

-(void)loadIfeedbackModelWithUrl:(NSString*)url title:(NSString*)title{
    if(title==nil){
        title = @"iFeedback®";
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");
    
    _iFeedbackModel = [iFeedbackModel new];
    _iFeedbackModel.delegate = self;
    _iFeedbackModel.titleString = title;
    _iFeedbackModel.entryPoint = [url lastPathComponent];
    _iFeedbackModel.entryURL = url;
    [_iFeedbackModel loadClient];
}

-(void)iFeedbackModelDidLoadData{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    UIViewController *vc = [_iFeedbackModel loadIFeedbackViewController];
    
    
    vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:vc.revealViewController action:@selector(revealToggle:)];
    
    NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
    [self.revealViewController pushFrontViewController:nvc animated:YES];
}

-(void)iFeedbackModelDidLoadDataWithViewController:(UIViewController *)viewController forIndex:(NSUInteger)index{
    
}

@end
