//
//  gestgidPlaceSelectTableViewController.m
//  if_framework
//
//  Created by Christopher on 8/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "gestgidPlaceSelectTableViewController.h"

@interface gestgidPlaceSelectTableViewController ()

@end

@implementation gestgidPlaceSelectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)preferredContentSize {
    float height = [UIScreen mainScreen].bounds.size.height * 1.0f-100;
    float width = [UIScreen mainScreen].bounds.size.width * 1.0f;
    
    [self.tableView layoutIfNeeded];
    
    float tableViewContentHeight = self.tableView.contentSize.height;
    
    if(tableViewContentHeight<height){
        height = tableViewContentHeight-1;
        self.tableView.scrollEnabled = NO;
    }
    else {
        self.tableView.scrollEnabled = YES;
    }
    
    return CGSizeMake(width, height);
}

@end
