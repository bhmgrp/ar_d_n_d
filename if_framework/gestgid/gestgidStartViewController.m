//
//  GuestGuideStartViewController.m
//  if_framework
//
//  Created by User on 11/4/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "gestgidStartViewController.h"
#import "NavigationViewController.h"
#import "LoadingInformationViewController.h"
#import "SettingsViewController.h"
#import "SSKeychain.h"
#import "AppDelegate.h"

#import "WebViewController.h"

#import "gestgidTableViewCell.h"

#import "gestgidPlaceSelectTableViewController.h"

#import "StartTabBarViewController.h"

#import "MBProgressHUD.h"

#import "OfferViewController.h"

#import "API.h"

#import "gestgid-Swift.h"

@interface gestgidStartViewController () <loadingInformationProtocol>

@property (nonatomic) NSURL *jsonFileUrl;

@property (nonatomic) NSMutableArray *searchResults;

@property (nonatomic) NSMutableArray *allPlaces, *allOffers, *allPeople;

@property (nonatomic) NSMutableArray *followedLocations;
@property (nonatomic) NSMutableDictionary *placesInfo;
@property (nonatomic) NSMutableDictionary *filteredPlacesInfo;
@property (nonatomic) NSArray *sortedCitiesKeys;

@property (nonatomic) NSMutableArray *filteredResults;


@property (nonatomic) localStorage* placeStorage;

@property (nonatomic) NSIndexPath *selectedIndexPath;

@property (nonatomic) gestgidPlaceSelectTableViewController *placeSelectViewController;
@property (nonatomic) UIView *placeSelectViewWrapper;
//@property (nonatomic) UIAlertController *selectPlaceAlertController;
@property (nonatomic) NSInteger selectedCityIndex;

@property (nonatomic) UIView *selectedIndexIndicator;

@property (nonatomic) UIStatusBarStyle preferedBarStyle;

@property (nonatomic) BOOL searching;

@property (nonatomic) NSMutableArray *followedCities;

@property (nonatomic) NSInteger loggedInUserID;

@end

@implementation gestgidStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _placesInfo = [NSMutableDictionary new];
    _filteredPlacesInfo = [NSMutableDictionary new];
    _preferedBarStyle = UIStatusBarStyleLightContent;
    // Do any additional setup after loading the view.
    
    _followedCities = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followedCities"] mutableCopy];
    
    _loggedInUserID = [[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"];
    
    _datePickerBar.width = [UIScreen mainScreen].bounds.size.width;
    
    _filteredResults = [@[] mutableCopy];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:nil action:nil];
    
    UIColor *searchTintColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSearch)];

    _searchFrameView.layer.borderWidth = 1.5f;
    _searchFrameView.layer.borderColor = [searchTintColor CGColor];
    _searchFrameViewFrame = CGRectMake(_searchFrameView.x, _searchFrameView.y, _searchFrameView.width, _searchFrameView.height);
    
    _searchLabel.tintColor = searchTintColor;
    
    [_searchFrameView addGestureRecognizer:tab];
    
    _searchIcon.tintColor = searchTintColor;
    
    _registerButton.layer.cornerRadius = 5.0f;
    _registerButton.layer.borderWidth = 1.5f;
    _registerButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    _headerImageView.image = [_headerImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _headerImageView.tintColor = [UIColor whiteColor];

    _placeStorage = [localStorage storageWithFilename:@"places"];
    
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    
    self.placeSelectViewController = [[gestgidPlaceSelectTableViewController alloc] init];
    self.placeSelectViewController.tableView.delegate = self;
    self.placeSelectViewController.tableView.dataSource = self;
    self.placeSelectViewController.tableView.backgroundColor = [UIColor clearColor];
    self.placeSelectViewController.tableView.separatorInset = UIEdgeInsetsZero;
    self.placeSelectViewController.tableView.separatorColor = [UIColor darkGrayColor];
    
    
    //NavigationViewController *nvc = (NavigationViewController*)self.navigationController;
    
    //[nvc fadeNavigationBarBackground:YES animated:YES finished:^(void){
    //    [nvc setNavigationBarHidden:YES animated:YES];
    //}];
    
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    
    
    [self registerForKeyboardNotifications];
    
    [self localizeOutlets];

    self.title = @"AR D'N'D"; //NSLocalizedString(@"Find your host ...", @"");

    [self setSidebarButton];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id response = [AdminAPI getFromUrl:@"ardnd/user/get-followed-locations"];
        
        if ([response isKindOfClass:[NSArray class]]){
            
            self.followedLocations = [response mutableCopy];
            [self formPlacesInfo];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_tableSearchBar setSelectedScopeButtonIndex:0];
                [self searchBar:_tableSearchBar selectedScopeButtonIndexDidChange:0];
                
                [_tableView reloadData];
                
                
            });
        }
    });
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return _preferedBarStyle;
}

- (void)localizeOutlets{
    _searchLabel.text = NSLocalizedString(@"Find your host ...", @"");
    
    _areYouHotelierLabel.text = NSLocalizedString(@"Do you want to be a host?", @"");
    
    [_registerButton setTitle:NSLocalizedString(@"Submit your application", @"") forState:UIControlStateNormal];
    
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:14.0f],
                                 };
    
    //NSMutableAttributedString *isAServiceOfText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"gestgid %@ ",NSLocalizedString(@"is a service of the", @"")] attributes:attributes];
    
    NSMutableAttributedString *isAServiceOfText = [[NSMutableAttributedString alloc] initWithString:@"ANOTHER REASON TO DRINK N’ DINE TOGETHER" attributes:attributes];
    
    //attributes = @{
    //               NSForegroundColorAttributeName: [UIColor whiteColor],
    //               NSFontAttributeName: [UIFont fontWithName:@"Helvetica-bold" size:14.0f],
    //               };
    
    //[isAServiceOfText appendAttributedString:[[NSAttributedString alloc] initWithString:@"BHM GROUP" attributes:attributes]];
    
    _gestgidIsAServiceLabel.attributedText = isAServiceOfText;
    if([UIScreen mainScreen].bounds.size.height < 568){
        _gestgidIsAServiceLabel.attributedText = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self loadSearchBar];
    
    [self loadData];
    
    if(_tableSearchBar.alpha==1){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    
    //NavigationViewController *nvc = (NavigationViewController*)self.navigationController;
    
    //[nvc fadeNavigationBarBackground:YES animated:YES finished:^(void){
    //    [nvc setNavigationBarHidden:YES animated:YES];
    //}];
    
    if(self.searching){
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLayoutSubviews{
    _searchFrameViewFrame = CGRectMake(_searchViewWrapper.x, _searchViewWrapper.y, _searchViewWrapper.width, _searchViewWrapper.height);
}


- (void)loadSearchBar{
    
    // colors
    // gold: 0xcc9933
    
    if(_tableSearchBar.delegate==nil){
        _tableSearchBar.selectedScopeButtonIndex = 0;
        _tableSearchBar.delegate = self; // so we can monitor text changes + others
        _tableSearchBar.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
        _tableSearchBar.tintColor = [UIColor colorWithRGBHex:0x000000];
        _tableSearchBar.barTintColor = [UIColor colorWithRGBHex:0x000000];
        _tableSearchBar.layer.borderWidth = 1.0f;
        _tableSearchBar.layer.borderColor = [[UIColor colorWithRGBHex:0xffffff] CGColor];
        _tableSearchBar.backgroundImage = [UIImage new];
        _tableSearchBar.keyboardAppearance = UIKeyboardAppearanceLight;
        _tableSearchBar.placeholder = NSLocalizedString(@"Find your host ...", @"");
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hostMode"])
            _tableSearchBar.showsCancelButton = NO;
        
        [[UIButton appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor blackColor]];
        
    }
    
    
    if(_scopeTabBar.items.count!=4){
        NSMutableArray *items = [@[] mutableCopy];
        UITabBarItem *item1, *item2, *item3, *item4;
        [items addObject: item1 = [[UITabBarItem alloc] initWithTitle:nil image:[imageMethods resizeImage:[UIImage imageNamed:@"bookmark.png"] forSize:CGSizeMake(26, 26)] tag:0]];
        [items addObject: item2 = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"ios7-location-outline.png"] tag:1]];
        [items addObject: item3 = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"ios7-contact-outline.png"] tag:2]];
        [items addObject: item4 = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"ardnd_icon_template.png"] tag:3]];
        
        [item1 setImageInsets:UIEdgeInsetsMake(4, 0, -4, 0)];
        [item2 setImageInsets:UIEdgeInsetsMake(4, 0, -4, 0)];
        [item3 setImageInsets:UIEdgeInsetsMake(4, 0, -4, 0)];
        [item4 setImageInsets:UIEdgeInsetsMake(4, 0, -4, 0)];
        
        [_scopeTabBar setItems:items];
        [_scopeTabBar setBarStyle:UIBarStyleBlack];
        [_scopeTabBar setBackgroundColor:[UIColor colorWithRGBHex:0xffffff]];
        [_scopeTabBar setTintColor:[UIColor colorWithRGBHex:0x000000]];
        [_scopeTabBar setBarTintColor:[UIColor colorWithRGBHex:0x000000]];
        [_scopeTabBar setBackgroundImage:[UIImage new]];
        [_scopeTabBar setSelectedItem:item1];
        [_scopeTabBar setDelegate:self];
        
        UIView *bar = [[UIView alloc] init];
        [bar setBackgroundColor:[UIColor grayColor]];
        [bar setFrame:CGRectMake(0, _scopeTabBar.height, self.view.width, 0.5)];
        [_scopeTabBar addSubview:bar];
        
        UIView *bar2 = [[UIView alloc] init];
        [bar2 setBackgroundColor:[UIColor grayColor]];
        [bar2 setFrame:CGRectMake(0, 0, self.view.width, 0.5)];
        [_scopeTabBar addSubview:bar2];
    }
    
    if(_selectedIndexIndicator==nil){
        _selectedIndexIndicator = [[UIView alloc] init];
        [_selectedIndexIndicator setBackgroundColor:[UIColor colorWithRGBHex:0x000000]];
        [_selectedIndexIndicator setFrame:CGRectMake(self.view.width*1/4 * 0, _scopeTabBar.height-2, self.view.width*1/4, 3)];
        
        [_scopeTabBar addSubview:_selectedIndexIndicator];
    }
    
    

    
    //_tableView.tableHeaderView = _tableSearchBar;
    if(_filteredResults==nil || _filteredResults.count==0){
        _filteredResults = [_searchResults mutableCopy];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    
    self.definesPresentationContext = YES;
    
    if(_datePickerBar==nil){
        _datePickerBar = [[DIDatepicker alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 60)];
        [_datePickerBar setBackgroundColor:[UIColor whiteColor]];
        [_datePickerBar setHidden:NO];
        [_datePickerBar setSelectedDateBottomLineColor:[UIColor colorWithRGBHex:0x000000]];
        [_datePickerBar fillDatesFromDate:[NSDate date] numberOfDays:31];
        [_datePickerBar selectDateAtIndex:0];
        [_datePickerBar addTarget:self action:@selector(datePickerUpdate:) forControlEvents:UIControlEventValueChanged];
    }
    
}


- (void)datePickerUpdate:(DIDatepicker*)datePicker{
    [self searchBar:_tableSearchBar textDidChange:_tableSearchBar.text];
}

- (void)openSearch {
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hostMode"]){
        [self startSearch];
    }
    else {
        [self.tabBarController setSelectedIndex:0];
    }
}

- (void)startSearch{
    self.searching = true;
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    [self.navigationController setNavigationBarHidden:YES animated:YES];

    _searchBackground.alpha = 0.0f;
    _tableView.alpha = 0.0f;
    
    //[_tableSearchBar becomeFirstResponder];
    [self searchBar:_tableSearchBar textDidChange:_tableSearchBar.text];
    [UIView animateWithDuration:0.3 animations:^(void){
        _searchBackground.alpha = 1.0f;
        _tableView.alpha = 1.0f;
        _tableSearchBar.alpha = 1.0f;
        _scopeTabBar.alpha = 1.0f;
        
        //self.tabBarController.tabBar.alpha = 0;
        //self.tabBarController.tabBar.height = 0;
        //self.tabBarController.tabBar.y = [UIScreen mainScreen].bounds.size.height;
    } completion:^(BOOL finished){
        //[self.tabBarController.tabBar layoutSubviews];
        //[self.tabBarController.tabBar layoutIfNeeded];
    }];
}

- (void)hideSearchTable{
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    self.searching = false;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [UIView animateWithDuration:0.5 animations:^(void){
        _searchBackground.alpha = 0.0f;
        _tableView.alpha = 0.0f;
        _tableSearchBar.alpha = 0.0f;
        _scopeTabBar.alpha = 0.0f;
        
        //self.tabBarController.tabBar.alpha = 1;
        //self.tabBarController.tabBar.height = 49;
        //self.tabBarController.tabBar.y = [UIScreen mainScreen].bounds.size.height - 49;
    } completion:^(BOOL finished){
        //[self.tabBarController.tabBar layoutSubviews];
        //[self.tabBarController.tabBar layoutIfNeeded];
    }];
    
}

- (void)formPlacesInfo {
    self.placesInfo = [NSMutableDictionary new];
    
    for (NSDictionary* item in self.followedLocations) {
        NSString *cityName = item[@"city"];
        if (cityName == nil)
            continue;
    
        NSMutableDictionary *place = [NSMutableDictionary new];
        NSString *placeName = item[@"name"];
        if (placeName == nil)
            continue;
        [place addObjectIfNotNull:item[@"name"] forKey:@"place_name"];
        
        __block NSString *imageString = item[@"post_image"];
        if (imageString != nil)
            [place addObjectIfNotNull:item[@"post_image"] forKey:@"image"];
        else [place addObjectIfNotNull:@"" forKey:@"image"];
        
        if ([self.placesInfo.allKeys containsObject:cityName]) {
            [((NSMutableArray *)self.placesInfo[cityName]) addObject:place];
        } else {
            NSMutableArray *placesArray = [NSMutableArray arrayWithObject:place];
            [self.placesInfo addObjectIfNotNull:placesArray forKey:cityName];
        }
    }
    self.filteredPlacesInfo = self.placesInfo;
    self.sortedCitiesKeys = [self.placesInfo.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (void)loadData{

    _searchResults = [@[] mutableCopy];
    _allPlaces = [@[] mutableCopy];
    _allPeople = [@[] mutableCopy];
    _allOffers = [@[] mutableCopy];
        
    _jsonFileUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@ardnd/is_host=%li",SECURED_API_URL,(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"hostModeActivated"]]];
    
    NSLog(@" ardnd - main url: %@", _jsonFileUrl);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // load data in background
        NSError *error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:_jsonFileUrl] options:NSJSONReadingAllowFragments error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            for(NSDictionary* dict in jsonArray){
                [_allPlaces addObject:dict];
                [_allOffers addObjectsFromArray:dict[@"offers"]];
                [_allPeople addObjectsFromArray:dict[@"cooks"]];
            }
            
            // update interface in main queue
            [self searchBar:_searchBar textDidChange:_searchBar.text];
        });
    });
}

#pragma mark table view delegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_tableSearchBar.selectedScopeButtonIndex == 0) {
        return 1;
    }
    
    // selected a place
    if(tableView==_placeSelectViewController.tableView){
        return 2;
    }
    
    // place search
    if(_tableSearchBar.selectedScopeButtonIndex==1)
        return 1;
    
    // other searches
    else
        return _filteredResults.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // list after selecting a city
    if(tableView==_placeSelectViewController.tableView)
        return section==1?[_filteredResults[_selectedCityIndex][@"offers"] count]:_followedLocations.count;
    
    // cook seach
    if(_tableSearchBar.selectedScopeButtonIndex==2)
        return [_filteredResults[section][@"cooks"] count];
    // offer search
    else if(_tableSearchBar.selectedScopeButtonIndex==3)
        return [_filteredResults[section][@"offers"] count];
    
    if (_tableSearchBar.selectedScopeButtonIndex==0)
        return _filteredPlacesInfo.count;
       
    // place search
        return [_filteredResults count];

    
    return [_filteredResults count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if((tableView==_placeSelectViewController.tableView && indexPath.section == 0) || _tableSearchBar.selectedScopeButtonIndex == 0) {
        return 240;
    }
    
    if(tableView==_placeSelectViewController.tableView || _tableSearchBar.selectedScopeButtonIndex==3 || _tableSearchBar.selectedScopeButtonIndex==0){
        NSInteger section = indexPath.section;
        
        if(tableView==_placeSelectViewController.tableView)
            section = _selectedCityIndex;
        
        if(![_filteredResults[section][@"offers"][indexPath.row][@"image_header"] isEmptyString])
            return 240;
    }
    if(_tableSearchBar.selectedScopeButtonIndex==1){
            return 240;
    }
    return 60;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    // after we have selected a city
    if(tableView==_placeSelectViewController.tableView){
        if (section==0) {
            return [NSString stringWithFormat:@"   %@", NSLocalizedStringFromTable(@"Followed locations", @"admin", @"")];
        }
        else {
            return [NSString stringWithFormat:@"   %@", NSLocalizedStringFromTable(@"Offers", @"admin", @"")];
        }
    }
    
    // place search
    if(_tableSearchBar.selectedScopeButtonIndex==1 || _tableSearchBar.selectedScopeButtonIndex == 0)
        return nil;
    
    
    // other searches
    else
        return [NSString stringWithFormat:@"   %@", _filteredResults[section][@"name"]];
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    view.tintColor = [UIColor colorWithRGBHex:0xececec];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if((tableView==_placeSelectViewController.tableView && indexPath.section == 0)) {
        NSString *identifier = @"cityCell";
        
        gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:identifier];
        
        c.tag = indexPath.row;
        
        [c.followPlaceButton removeFromSuperview];
        
        c.labelPlaceName.text = (_followedLocations[indexPath.row][@"city"] != nil && ![_followedLocations[indexPath.row][@"city"] isEqual:@""])? _followedLocations[indexPath.row][@"city"] : _followedLocations[indexPath.row][@"name"];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
            NSString *version = _followedLocations[indexPath.row][@"post_image"];
            
            if (version == nil) {
                version = @"";
            }
            
            version = version.lastPathComponent;
            
            UIImage *image = [imageMethods UIImageWithServerPathName:_followedLocations[indexPath.row][@"post_image"]
                                                   versionIdentifier:[NSString stringWithFormat:@"followed_location%@",version]
                                                               width:[UIScreen mainScreen].bounds.size.width];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(image && c.tag == indexPath.row){
                    // update cell in main queue
                        c.offerImage.image = image;
                        [c setNeedsLayout];
                }
                else {
                        c.offerImage.image = nil;
                        [c setNeedsLayout];
                }
            });
        });
        
        return c;
    }
    
    // locations search
    if (_tableSearchBar.selectedScopeButtonIndex == 0) {
        NSString *identifier = @"cityCell";
        
        gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:identifier];
        
        c.wantsSpacing = YES;
        c.tag = indexPath.row;
        
        [c.followPlaceButton removeFromSuperview];
        
        NSString *cellKey = self.sortedCitiesKeys[indexPath.row];
        NSArray *cellDict = [_filteredPlacesInfo objectForKey:cellKey];
        c.labelPlaceName.text = cellKey;
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
            int i = 0;
            NSString *version = nil;
            while (i < cellDict.count && [cellDict[i][@"image"] isEqual: @""]) {
                version = cellDict[i][@"image"];
                i++;
            }
            
            if (version == nil) {
                version = @"";
            }
            
            version = version.lastPathComponent;
            
            UIImage *image = [imageMethods UIImageWithServerPathName:cellDict[0][@"image"]
                                                   versionIdentifier:[NSString stringWithFormat:@"followed_location%@",version]
                                                               width:[UIScreen mainScreen].bounds.size.width];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(image && c.tag == indexPath.row){
                    // update cell in main queue
                    c.offerImage.image = image;
                    [c setNeedsLayout];
                }
                else {
                    c.offerImage.image = nil;
                    c.offerImage.backgroundColor = UIColor.grayColor;
                    [c setNeedsLayout];
                }
            });
        });
        
        return c;
    }
    
    // offer search
    if(tableView==_placeSelectViewController.tableView || _tableSearchBar.selectedScopeButtonIndex==3 || _tableSearchBar.selectedScopeButtonIndex==0){
        NSInteger section = indexPath.section;
        
        if(tableView==_placeSelectViewController.tableView)
            section = _selectedCityIndex;
    
        
        BOOL hasImage = ![_filteredResults[section][@"offers"][indexPath.row][@"image_header"] isEmptyString];
        
        NSString *identifier = @"offerCell";
        
        if(hasImage){
            identifier = @"offerCellImage";
        }
        
        gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:identifier];
        [c.loadingIndicator startAnimating];
        c.labelPlaceName.text = _filteredResults[section][@"offers"][indexPath.row][@"name"];
        c.labelPlaceAddress.text = [NSString stringWithFormat:@"Angebot von %@", _filteredResults[section][@"offers"][indexPath.row][@"cook_name"]];
        c.tag = indexPath.row;
        
        if(hasImage){
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
                UIImage *image = [imageMethods UIImageWithServerPathName:_filteredResults[section][@"offers"][indexPath.row][@"image_header"]
                                                       versionIdentifier:[NSString stringWithFormat:@"offer_id%@",_filteredResults[section][@"offers"][indexPath.row][@"uid"]]
                                                                   width:[UIScreen mainScreen].bounds.size.width];
                
                    // update cell in main queue
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(image && c.tag == indexPath.row){
                        c.offerImage.image = image;
                        [c.loadingIndicator stopAnimating];
                        [c setNeedsLayout];
                        }
                    });
            });
        }
        
        if ([_filteredResults[section][@"offers"][indexPath.row][@"hidden"] integerValue] == 1) {
            c.offerImage.alpha = 0.3;
        }
        else {
            c.offerImage.alpha = 1;
        }
        
        if(tableView==_placeSelectViewController.tableView){
            //c.backgroundColor = [UIColor clearColor];
            //c.backgroundView.backgroundColor = [UIColor clearColor];
            //c.contentView.backgroundColor = [UIColor clearColor];
            
            //c.labelPlaceName.textColor = [UIColor blackColor];
            //c.labelPlaceAddress.textColor = [UIColor darkGrayColor];
        }
        
        return c;
    }
    
    // people search
    else if(_tableSearchBar.selectedScopeButtonIndex==2){
        gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:@"placeCell"];
        c.labelPlaceName.text = _filteredResults[indexPath.section][@"cooks"][indexPath.row][@"name"];
        c.labelPlaceAddress.text = [NSString stringWithFormat:@"Angebote: %@", _filteredResults[indexPath.section][@"cooks"][indexPath.row][@"num_offers"]];
        
        return c;
    }
    
    // place search
    else{
        
        BOOL hasImage = (_filteredResults[indexPath.row][@"image"]!=nil && ![_filteredResults[indexPath.row][@"image"] isEmptyString]);
        
        NSString *identifier = @"cityCell";
        
        
        gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:identifier];
        
        NSDictionary *placeDict = _filteredResults[indexPath.row];
        
        [c.loadingIndicator startAnimating];
        
        c.labelPlaceName.text = [placeDict[@"name"] uppercaseString];
    
        NSString *placeAddressString = [NSString stringWithFormat:@"%@: %@\n%@: %@",NSLocalizedString(@"Hosts",@""),placeDict[@"num_cooks"],NSLocalizedString(@"Angebote",@""),placeDict[@"num_offers"]];
    
        c.labelPlaceAddress.text = placeAddressString;
        
        c.tag = indexPath.row;
        
        c.followPlaceButton.tag = indexPath.row;
        
        [c.followPlaceButton addTarget:self action:@selector(followPlaceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (_loggedInUserID != 0) {
            if([_followedCities containsObject:placeDict[@"name"]]) {
                [c.followPlaceButton setBackgroundColor:[UIColor clearColor]];
                [c.followPlaceButton setTitle:@"- unfollow" forState:UIControlStateNormal];
            } else {
                [c.followPlaceButton setBackgroundColor:[UIColor whiteColor]];
                [c.followPlaceButton setTitle:@"+ follow" forState:UIControlStateNormal];
            }
        }
        else {
            c.followPlaceButton.hidden = true;
        }
        
        if(hasImage){
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
                UIImage *image = [imageMethods UIImageWithServerPathName:_filteredResults[indexPath.row][@"image"]
                                                       versionIdentifier:[NSString stringWithFormat:@"city%@",_filteredResults[indexPath.row][@"name"]]
                                                                   width:[UIScreen mainScreen].bounds.size.width];
                
                    // update cell in main queue
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(image && c.tag == indexPath.row){
                            c.offerImage.image = image;
                            [c setNeedsLayout];
                        }
                        else {
                            c.offerImage.image = nil;
                            [c setNeedsLayout];
                        }
                    });
            });
        }
        
        return c;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    
    if(tableView == self.placeSelectViewController.tableView || _tableSearchBar.selectedScopeButtonIndex == 0){
        //[_selectPlaceAlertController dismissViewControllerAnimated:YES completion:nil];
        if (indexPath.section == 1) {
            NSDictionary *info = _filteredResults[_selectedCityIndex][@"offers"][indexPath.row];
            [self showOfferForInfo:info];
        }
        else {
            PlacesForLocationTableViewController *vc = [PlacesForLocationTableViewController load:@"PlacesForLocationTableViewController" fromStoryboard:@"PlacesForLocationTableViewController"];
            
            NSString *cityName = self.sortedCitiesKeys[indexPath.row];
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"place_name" ascending:YES];
            vc.followedLocations = [self.placesInfo[cityName] sortedArrayUsingDescriptors:@[sortDescriptor]];
            vc.title = cityName;
            
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            [self.navigationController pushViewController:vc animated:true];
        }
        
        return;
    }
    
    NSInteger scope = _tableSearchBar.selectedScopeButtonIndex;
    
    if(scope==2 || scope==3 || scope==0){
        // people search
        if(_tableSearchBar.selectedScopeButtonIndex==2){
            NSMutableDictionary *info = [_filteredResults[indexPath.section][@"cooks"][indexPath.row] mutableCopy];
            info[@"show_all_offers"] = @(YES);
            [self showOfferForInfo:info];
            return;
        }
        // offer search
        else if(_tableSearchBar.selectedScopeButtonIndex==3 || _tableSearchBar.selectedScopeButtonIndex==0){
            NSDictionary *info = _filteredResults[indexPath.section][@"offers"][indexPath.row];
            [self showOfferForInfo:info];
            return;
        }
    }
    else {
        self.selectedCityIndex = indexPath.row;
        
        UIAlertControllerStyle alertStyle = UIAlertControllerStyleAlert;
        if(IDIOM!=IPAD)
            alertStyle = UIAlertControllerStyleActionSheet;
        
        //[self.placeSelectViewController.tableView reloadData];
        
        //_selectPlaceAlertController = [UIAlertController alertControllerWithTitle:_filteredResults[indexPath.row][@"name"] message:nil preferredStyle:alertStyle];
        //[_selectPlaceAlertController setValue:self.placeSelectViewController forKey:@"contentViewController"];
        
        
        //[_selectPlaceAlertController addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Cancel",@"ifbckLocalizable", @"") style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
        //}]];
        
        
        //_selectPlaceAlertController.view.height = self.view.height - 30;
        
        //_selectPlaceAlertController.popoverPresentationController.sourceView = [tableView cellForRowAtIndexPath:indexPath];
        //_selectPlaceAlertController.popoverPresentationController.sourceRect = [tableView cellForRowAtIndexPath:indexPath].bounds;
        
        //[self presentViewController:_selectPlaceAlertController animated:YES completion:nil];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            id response = [AdminAPI postWithUrl:@"ardnd/user/get-followed-locations" post:@{@"city":_filteredResults[indexPath.row][@"name"]}];
            
            if ([response isKindOfClass:[NSDictionary class]]){
                id data = ((NSDictionary*) response)[@"data"];
                
                if (data != nil && [data isKindOfClass:[NSArray class]]) {
                    self.followedLocations = [data mutableCopy];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_placeSelectViewController.tableView reloadData];
                    });
                }
            }
        });
        
        _placeSelectViewController = [[gestgidPlaceSelectTableViewController alloc] init];
        _placeSelectViewController.tableView.delegate = self;
        _placeSelectViewController.tableView.dataSource = self;
        _placeSelectViewController.tableView.backgroundColor = [UIColor whiteColor];
        _placeSelectViewController.tableView.separatorInset = UIEdgeInsetsZero;
        _placeSelectViewController.tableView.tableFooterView = [UIView new];
        _placeSelectViewController.tableView.contentInset = UIEdgeInsetsZero;
        _placeSelectViewController.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        _placeSelectViewWrapper = [[UIView alloc] initWithFrame:self.view.frame];
        _placeSelectViewWrapper.y = self.view.height;
        _placeSelectViewWrapper.backgroundColor = [UIColor whiteColor];
        
        UIToolbar *viewToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 20, _placeSelectViewWrapper.width, 44)];;
        [viewToolBar setBarStyle:UIBarStyleDefault];
        [viewToolBar setTintColor:[UIColor blackColor]];
        [viewToolBar setShadowImage:[UIImage new] forToolbarPosition:UIBarPositionTop];
        [viewToolBar setBackgroundImage:[UIImage new] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
        
        UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(hidePlaceSelectView)];
        
        UIBarButtonItem *nameItem = [[UIBarButtonItem alloc] initWithTitle:_filteredResults[indexPath.row][@"name"] style:UIBarButtonItemStylePlain target:nil action:nil];
        
        cancelItem.tintColor = [UIColor blackColor];
        
        nameItem.tintColor = [UIColor blackColor];
        
        [viewToolBar setItems:@[
                                cancelItem,
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil],
                                nameItem,
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil],
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil],
                                ]];
        
        [_placeSelectViewWrapper addSubview:viewToolBar];
        
        [_placeSelectViewWrapper addSubview:_placeSelectViewController.tableView];
        
        _placeSelectViewController.tableView.frame = CGRectMake(0, 64, _placeSelectViewWrapper.width, _placeSelectViewWrapper.height-64);
        
        [self.view addSubview:_placeSelectViewWrapper];
        
        [_placeSelectViewController didMoveToParentViewController:self];
        [self showPlaceSelectView];
        
        
    }
}

- (void)showOfferForInfo:(NSDictionary*)info{
    NSInteger offerID = [info[@"uid"] integerValue];
    
    
    if(offerID>0 && !info[@"show_all_offers"]){
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        OfferViewController *ovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];
        
        ovc.offerID = offerID;
        
        ovc.title = info[@"name"];
        
        [self.navigationController pushViewController:ovc animated:YES];
    }
    else {
        NSInteger offerCampaignID = [info[@"offer_campaign_id"] integerValue];
        
        if(offerCampaignID>0){
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            OfferViewController *ovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];
            
            ovc.offerCampaignID = offerCampaignID;
            ovc.navigationItem.titleView = [NavigationTitle createNavTitle:info[@"name"] SubTitle:NSLocalizedString(@"Offers", @"")];
            
            [self.navigationController pushViewController:ovc animated:YES];
            return;
        }
        
        NSInteger userID = [info[@"id"] integerValue];
        
        if(userID>0){
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            OfferViewController *ovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];
            
            ovc.userID = userID;
            ovc.navigationItem.titleView = [NavigationTitle createNavTitle:info[@"name"] SubTitle:NSLocalizedString(@"Offers", @"")];
            
            [self.navigationController pushViewController:ovc animated:YES];
        }
    }
}

- (void)hasSelectedPlaceID:(int)placeID :(int)pid :(NSString*)name{
    [[globals sharedInstance] setPID:pid];
    [[globals sharedInstance] setPlaceID:pid];
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:placeID forKey:@"selectedPlaceID"];
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"selectedPlaceName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //save to dynamic settings:
    NSMutableArray *dynamicSettingsArray = [@[
                                              [@{@"type":@"inputCell",
                                                 @"inputTag":@0,
                                                 @"label":@"",
                                                 @"placeHolder":@"",
                                                 @"value":[NSNumber numberWithInt:placeID],
                                                 @"secureInput":@NO,
                                                 @"attributesTableID":@6,
                                                 @"dynamicSettings":@YES,
                                                 @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone],
                                                 @"keyboardType":[NSNumber numberWithInt:UIKeyboardTypeAlphabet],
                                                 } mutableCopy],
                                              ] mutableCopy];
    int userID = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"];
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedInUserName"];
    NSString *validationPassword = [SSKeychain passwordForService:@"PickalbatrosLogin" account:userName];
    
    [SettingsViewController updateDynamicSettings:dynamicSettingsArray forUserID:userID validationPassword:validationPassword ignoreLocalUpdates:YES];
    
    NSLog(@"dynamic settings updated, finished in this view");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
    
    LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
    livc.delegate = self;
    livc.pID = pid;
    livc.placeID = placeID;
    livc.skipUserInput = YES;
    livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
    livc.providesPresentationContextTransitionStyle = YES;
    livc.definesPresentationContext = YES;
    [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    //[self hideSearchTable];
    
    //[_searchController dismissViewControllerAnimated:NO completion:^(void){
        [self presentViewController:livc animated:NO completion:^{}];
    //}];
}

- (void) showPlaceSelectView{
    [UIView animateWithDuration:0.3 animations:^{
        _placeSelectViewWrapper.y = 0;
    }];
}

- (void) hidePlaceSelectView{
    [UIView animateWithDuration:0.3 animations:^{
        _placeSelectViewWrapper.y = self.view.height;
    }];
}

#pragma mark - tab bar delegate

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    [UIView animateWithDuration:0.3 animations:^{
        _selectedIndexIndicator.x = self.view.width*1/4 * item.tag;
    }];
    
    if(item.tag==0){
        [self.tableSearchBar resignFirstResponder];
        //_tableView.tableHeaderView = _datePickerBar;
        //if(_filteredResults.count>0)
        // [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        // TODO: implement logic for getting the followed locations
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            id response = [AdminAPI getFromUrl:@"ardnd/user/get-followed-locations"];
            
            if ([response isKindOfClass:[NSArray class]]){
                
                self.followedLocations = [response mutableCopy];
                [self formPlacesInfo];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_tableSearchBar setSelectedScopeButtonIndex:item.tag];
                    [self searchBar:_tableSearchBar selectedScopeButtonIndexDidChange:item.tag];
                    
                    [_tableView reloadData];
                    
                    
                });
            }
        });
        
        return;
    }
    else {
        if(_tableSearchBar.selectedScopeButtonIndex==1){
            _tableView.tableHeaderView = nil;
        }
    }
        
    
    [_tableSearchBar setSelectedScopeButtonIndex:item.tag];
    [self searchBar:_tableSearchBar selectedScopeButtonIndexDidChange:item.tag];
    
}

#pragma mark - search bar delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hostMode"])
        [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //[searchBar setShowsCancelButton:NO animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [self hideSearchTable];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSString *keyToSearch = @"name";
    
    NSInteger scope = self.scopeTabBar.selectedItem.tag;
    
    switch (scope) {
        case 0: {
            if ([searchText isEqualToString:@""]) {
                self.filteredPlacesInfo = self.placesInfo;
                self.sortedCitiesKeys = [self.filteredPlacesInfo.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                [self.tableView reloadData];
                return;
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
            NSArray *results = [self.placesInfo.allKeys filteredArrayUsingPredicate:predicate];
            self.filteredPlacesInfo = [NSMutableDictionary new];
            [results enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                id place = [self.placesInfo objectForKey:obj];
                [self.filteredPlacesInfo addObjectIfNotNull:place forKey:obj];
            }];
            self.sortedCitiesKeys = [self.filteredPlacesInfo.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            [self.tableView reloadData];
            return;
        }
        case 1:
            keyToSearch = @"name";
            break;
            
        case 2:
            keyToSearch = @"cook_names";
            break;
            
        case 3:
            keyToSearch = @"offer_names";
            break;
            
        default:
            keyToSearch = @"name";
            break;
    }
    
    // update the filtered array based on the search text
    NSMutableArray *searchResults = [_allPlaces mutableCopy];
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        NSLog(@" Searching for: %@", strippedString);
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    else if(scope!=0){
        NSLog(@" Searching - dont know 1");
        _filteredResults = [searchResults mutableCopy];
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
        return;
    }
    else {
        NSLog(@" Searching - dont know 2");
        NSMutableArray *places = [@[] mutableCopy];
        for(NSDictionary *place in searchResults){
            NSMutableDictionary *newPlace = [place mutableCopy];
            
            NSMutableArray *newOffers = [@[] mutableCopy];
            for(NSDictionary *offer in newPlace[@"offers"]){
                if([offer[@"starttime"] integerValue] < [_datePickerBar.selectedDate timeIntervalSince1970] &&
                   [offer[@"endtime"] integerValue] > [_datePickerBar.selectedDate timeIntervalSince1970]){
                    [newOffers addObject:offer];
                }
            }
            newPlace[@"offers"] = newOffers;
            
            if([newPlace[@"offers"] count] > 0)
                [places addObject:newPlace];
        }
        
        _filteredResults = [places mutableCopy];
        [self.tableView reloadData];
        return;
    }
    
    NSLog(@" Searching - filtering");
    
    NSLog(@" Searching - current array: %@", searchResults);
    // build all the "AND" expressions for each value in the searchString
    //
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
        //
        // example if searchItems contains "iphone 599 2007":
        //      name CONTAINS[c] "iphone"
        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
        //
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        NSExpression *lhs = [NSExpression expressionForKeyPath:keyToSearch];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        
        // at this OR predicate to our master AND predicate
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }
    
    // match up the fields of the Product object
    NSCompoundPredicate *finalCompoundPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
        
    if(scope==2 || scope==3 || scope==0){
        NSMutableArray *andMatchPredicates = [NSMutableArray array];
        for (NSString *searchString in searchItems) {
            NSMutableArray *searchItemsPredicate = [NSMutableArray array];
            
            NSExpression *lhs = [NSExpression expressionForKeyPath:@"name"];
            NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
            NSPredicate *finalPredicate = [NSComparisonPredicate
                                           predicateWithLeftExpression:lhs
                                           rightExpression:rhs
                                           modifier:NSDirectPredicateModifier
                                           type:NSContainsPredicateOperatorType
                                           options:NSCaseInsensitivePredicateOption];
            [searchItemsPredicate addObject:finalPredicate];
            
            NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
            [andMatchPredicates addObject:orMatchPredicates];
        }
        NSCompoundPredicate *finalCompoundPredicate =
        [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
        
        NSMutableArray *places = [@[] mutableCopy];
        
        NSLog(@" Searching - now filtering for some places");
        
        for(NSDictionary *place in searchResults){
            NSMutableDictionary *newPlace = [place mutableCopy];
            
            if(scope==0){
                NSMutableArray *newOffers = [@[] mutableCopy];
                for(NSDictionary *offer in newPlace[@"offers"]){
                    if([offer[@"starttime"] integerValue] < [_datePickerBar.selectedDate timeIntervalSince1970] &&
                       [offer[@"endtime"] integerValue] > [_datePickerBar.selectedDate timeIntervalSince1970]){
                        [newOffers addObject:offer];
                    }
                }
                newPlace[@"offers"] = newOffers;
            }
            
            if(scope==2){
                newPlace[@"cooks"] = [[newPlace[@"cooks"] filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
                if([newPlace[@"cooks"] count] > 0)
                    [places addObject:newPlace];
            }
            if(scope==3 || scope==0){
                newPlace[@"offers"] = [[newPlace[@"offers"] filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
                if([newPlace[@"offers"] count] > 0)
                    [places addObject:newPlace];
            }
        }
        
        NSLog(@" Searching - setting the places");
        
        searchResults = places;
    }
    
    
    
    // hand over the filtered results to our search results table
    //tableController.filteredProducts = searchResults;
    _filteredResults = [searchResults mutableCopy];
    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    [self searchBar:_tableSearchBar textDidChange:_tableSearchBar.text];
}

#pragma mark - UISearchResultsUpdating

/*
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *keyToSearch = @"name";
    
    NSInteger scope = searchController.searchBar.selectedScopeButtonIndex;
    
    if(scope==1)
        keyToSearch = @"cook_names";
    else if(scope==2)
        keyToSearch = @"offer_names";
    else
        keyToSearch = @"name";
    
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [_allPlaces mutableCopy];
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    else {
        NSLog(@"setting all data");
        _filteredResults = [searchResults mutableCopy];
        [self.tableView reloadData];
        return;
    }
    
    // build all the "AND" expressions for each value in the searchString
    //
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
        //
        // example if searchItems contains "iphone 599 2007":
        //      name CONTAINS[c] "iphone"
        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
        //
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        NSExpression *lhs = [NSExpression expressionForKeyPath:keyToSearch];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        
        // at this OR predicate to our master AND predicate
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }
    
    // match up the fields of the Product object
    NSCompoundPredicate *finalCompoundPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
    
    if(scope==1 || scope == 2){
        NSMutableArray *andMatchPredicates = [NSMutableArray array];
        for (NSString *searchString in searchItems) {
            NSMutableArray *searchItemsPredicate = [NSMutableArray array];
            
            NSExpression *lhs = [NSExpression expressionForKeyPath:@"name"];
            NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
            NSPredicate *finalPredicate = [NSComparisonPredicate
                                           predicateWithLeftExpression:lhs
                                           rightExpression:rhs
                                           modifier:NSDirectPredicateModifier
                                           type:NSContainsPredicateOperatorType
                                           options:NSCaseInsensitivePredicateOption];
            [searchItemsPredicate addObject:finalPredicate];
            
            NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
            [andMatchPredicates addObject:orMatchPredicates];
        }
        NSCompoundPredicate *finalCompoundPredicate =
        [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
        
        NSMutableArray *places = [@[] mutableCopy];
        
        for(NSDictionary *place in searchResults){
            NSMutableDictionary *newPlace = [place mutableCopy];
            
            if(scope==1)
                newPlace[@"cooks"] = [[newPlace[@"cooks"] filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
            if(scope==2)
                newPlace[@"offers"] = [[newPlace[@"offers"] filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
            
            [places addObject:newPlace];
        }
        
        searchResults = places;
    }
    
    // hand over the filtered results to our search results table
    //tableController.filteredProducts = searchResults;
    _filteredResults = [searchResults mutableCopy];
    [self.tableView reloadData];
    
}
*/

#pragma mark - UISearchControllerDelegate

- (void)presentSearchController:(UISearchController *)searchController {
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    //[self hideSearchTable];
}

#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 49);
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{

}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}


#pragma mark LoadingInformationProtocoll delegate methods

- (void)hideLoadingInformationView{
    
    NSLog(@"moving to start page");
    
    StartTabBarViewController *tabController = [StartTabBarViewController new];
    
    
    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:[SidebarViewController loadNowFromStoryboard:@"Sidebar"] frontViewController:tabController];
    
    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self.navigationController pushViewController:tabController animated:YES];
//    
//    [self presentViewController:rvc animated:YES completion:^{
//        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
//    }];
}

- (IBAction)registerButtonClicked:(id)sender {
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    wvc.titleString = @"gestgid";
    wvc.url = [NSURL URLWithString:NSLocalizedString(@"http://ardnd.com/#1",@"")];
    wvc.urlGiven = YES;
    [self.navigationController pushViewController:wvc animated:YES];
}

#pragma mark - follow button

- (void)followPlaceButtonClicked:(UIButton*)button {
    NSLog(@" follow place button clicked");
    
    NSDictionary *place = _filteredResults[button.tag];
    
    NSLog(@" now following place: %@", place);
    
    if(_followedCities == nil)
        _followedCities = [@[] mutableCopy];
    
    if([_followedCities containsObject:place[@"name"]]) {
        [_followedCities removeObject:place[@"name"]];
        
        [button setTitle:@"+ follow" forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor whiteColor]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self unfollowCity:place[@"name"]];
        });
        
    } else {
        [_followedCities addObject:place[@"name"]];
        
        [button setTitle:@"- unfollow" forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self followCity:place[@"name"]];
        });
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:_followedCities forKey:@"followedCities"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)followCity:(NSString*)city {
    [API postToURL:[NSString stringWithFormat:@"%@admin/user/%li/follow-city",SECURED_API_URL,(long)_loggedInUserID] WithPost:@{@"city":city}];
}

- (void)unfollowCity:(NSString*)city {
    [API postToURL:[NSString stringWithFormat:@"%@admin/user/%li/unfollow-city",SECURED_API_URL,(long)_loggedInUserID] WithPost:@{@"city":city}];
}

@end
