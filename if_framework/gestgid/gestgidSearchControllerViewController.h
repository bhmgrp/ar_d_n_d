//
//  gestgidSearchControllerViewController.h
//  if_framework
//
//  Created by User on 12/7/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface gestgidSearchControllerViewController : UISearchController


-(void) overWriteSearchBar:(UITextField*)textField;

@end
