//
//  gestgidTableViewCell.m
//  if_framework
//
//  Created by User on 12/8/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "gestgidTableViewCell.h"

@interface gestgidTableViewCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewBottomConstraint;

@end

@implementation gestgidTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.wantsSpacing = NO;
}

- (void)setWantsSpacing:(BOOL)wantsSpacing
{
    _wantsSpacing = wantsSpacing;
    
    if (_wantsSpacing == YES)
        self.imageViewBottomConstraint.constant = 10;
    else self.imageViewBottomConstraint.constant = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
