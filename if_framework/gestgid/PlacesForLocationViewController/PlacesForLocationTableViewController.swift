//
//  PlacesForLocationTableViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 5/29/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

class PlacesForLocationTableViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @objc var followedLocations: [[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorColor = UIColor.white
    }
    
    // MARK: table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followedLocations.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell") as! gestgidTableViewCell
        let placeData = followedLocations[indexPath.row]
        
        cell.labelPlaceName.text = placeData["place_name"] as? String
        cell.tag = indexPath.row
        
        let version = placeData["image"] as? NSString
        
        if (version == "") {
            cell.offerImage.backgroundColor = UIColor.lightGray
            return cell
        }
        
        let versionString = version?.lastPathComponent
        DispatchQueue.global().async {
            let image = imageMethods.uiImage(withServerPathName: version as String?, versionIdentifier: "followed_location" + versionString!, width: Float(UIScreen.main.bounds.size.width))
            DispatchQueue.main.async {
                if image != nil && cell.tag == indexPath.row {
                    cell.offerImage.image = image
                    cell.setNeedsLayout()
                } else {
                    cell.offerImage.image = nil
                    cell.setNeedsLayout()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = get(viewController: "LocationCollectionViewController") as! LocationCollectionViewController
        
        vc.filters = "location=\((followedLocations[indexPath.row]["place_name"] as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        vc.title = followedLocations[indexPath.row]["place_name"] as? String
        tableView.deselectRow(at: indexPath, animated: true)
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
