//
//  GuestGuideStartViewController.h
//  if_framework
//
//  Created by User on 11/4/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DIDatepicker.h"
#import "PlaceOverViewController.h"

@interface gestgidStartViewController : UIViewController <
UITableViewDataSource,
UITableViewDelegate,
UISearchControllerDelegate,
UISearchBarDelegate,
UITabBarDelegate
>


@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;


@property (weak, nonatomic) IBOutlet UIView *searchViewWrapper;
@property (weak, nonatomic) IBOutlet UIView *searchFrameView;
@property (nonatomic) CGRect searchFrameViewFrame;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UIImageView *searchIcon;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *searchBackground;


@property (weak, nonatomic) IBOutlet UILabel *areYouHotelierLabel;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;
- (IBAction)registerButtonClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *gestgidIsAServiceLabel;

@property (weak, nonatomic) IBOutlet UISearchBar *tableSearchBar;
@property (weak, nonatomic)IBOutlet UITabBar *scopeTabBar;
@property (nonatomic)  DIDatepicker *datePickerBar;

@property (nonatomic, strong) UISearchController *searchController;

@property (nonatomic, strong) UITableViewController *resultsTableController;

- (void)startSearch;

@end
