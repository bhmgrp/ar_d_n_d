//
//  gestgidTableViewCell.h
//  if_framework
//
//  Created by User on 12/8/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface gestgidTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelPlaceName;

@property (weak, nonatomic) IBOutlet UILabel *labelPlaceAddress;

@property (weak, nonatomic) IBOutlet UIImageView *offerImage;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (weak, nonatomic) IBOutlet UIButton *followPlaceButton;

@property (nonatomic)   BOOL    wantsSpacing;

@end
