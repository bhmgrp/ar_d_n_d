//
//  HashtagStreamViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 5/16/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

class HashtagStreamViewController: StartStreamViewController {
    
    var collectionViewLayout :Bool = false
    
    @IBAction override func scrollUpButtonPressed(_ sender: UIButton) {
        self.tableView.setContentOffset(CGPoint.init(x:0,y:60), animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func initNavigationItems() {

    }
    
    override func searchForFilters() {
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! AdminTableViewCell
        
        guard collectionViewLayout == true else {
            return cell
        }
        
        cell.button2pressed = {
            let vc = get(viewController: "LocationCollectionViewController") as! LocationCollectionViewController
            
            vc.title = self.data[indexPath.row]["location_name"] as? String ?? ""
            vc.filters = "location=\((self.data[indexPath.row]["location_name"] as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            
            if let meta = self.data[indexPath.row]["meta"] as? [String:Any] {
                if let placeId = meta["gmsPlaceId"] as? String {
                    vc.placeId = placeId
                }
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        return cell
    }
}
