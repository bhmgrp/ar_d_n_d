//
//  StartTabBarViewController.m
//  if_framework
//
//  Created by User on 3/4/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "gestgid-Swift.h"

#import "StartTabBarViewController.h"

#import "gestgidStartViewController.h"

#import "NavigationViewController.h"
#import "StructureOverViewController.h"

#import "notificationListViewController.h"

#import "WebViewController.h"
#import "OnboardingViewController.h"
#import "SettingsViewController.h"

@interface StartTabBarViewController ()<SFSafariViewControllerDelegate>{
    int numOfTabs;
}

@property (nonatomic) StructureOverViewController *startViewController;

@property (nonatomic) NSMutableArray *customViewControllers;

@property (nonatomic) BOOL hideFirstElement;

typedef enum settingsModes {
    modeProfile = 0,
    modeOffers = 1
} settingsModes;

@property (nonatomic) settingsModes settingsMode;

@end

@implementation StartTabBarViewController

- (void)viewDidLoad {
    //[[NSUserDefaults standardUserDefaults] setBool:false forKey:@"hostModeActivated"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [globals sharedInstance];
    
     numOfTabs = NUMBEROFTABSINTABVIEW;
    
    [super viewDidLoad];
    
    [self initStyle];
    
    _customViewControllers = [@[] mutableCopy];

    [self initViewControllers];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // always select the last item (more page)
    [self setSelectedIndex:0];
    [self tabBarController:self didSelectViewController:_customViewControllers[0]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initViewControllers) name:TERMINAL_MODE_UPDATE object:nil];
        
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.delegate = self;
}

-(void)initStyle{
    [self.tabBar setTintColor:[UIColor colorWithRGBHex:0x000000]];
    
    [self.tabBar setBarTintColor:[UIColor clearColor]];
    [self.tabBar setTranslucent:NO];
    [self.tabBar setBackgroundImage:[UIImage new]];
    [self.tabBar setShadowImage:[UIImage new]];
    [self.tabBar setBarStyle:UIBarStyleDefault];
    [self.tabBar setItemPositioning:UITabBarItemPositioningFill];
    //[self.tabBar setBackgroundColor:[UIColor colorWithRGBHex:0xffffff]];
    
    
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIView *barBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    UIView *barBackground = [[UIView alloc] init];
    [barBackground setBackgroundColor:[UIColor whiteColor]];
    [barBackground setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.tabBar.height)];
    
    [self.tabBar addSubview:barBackground];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    
    [self.tabBar setBackgroundColor:[UIColor whiteColor]];
    [self.tabBar setBarTintColor:[UIColor whiteColor]];
    
    UIView *bar2 = [[UIView alloc] init];
    [bar2 setBackgroundColor:[UIColor grayColor]];
    [bar2 setFrame:CGRectMake(0, 0, self.view.width, 0.5)];
    [self.tabBar addSubview:bar2];
    
    self.moreNavigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.moreNavigationController.delegate = self;
    
    [[UIBarButtonItem appearance]setTintColor: [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateRequestsBadge{
    self.tabBar.items[1].badgeValue = [NSString stringWithFormat:@"%li",(long)[self.tabBar.items[1].badgeValue integerValue]+1 ];
}

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if(viewController==_customViewControllers[_customViewControllers.count-1]){
        if([[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"]==0){
            
            OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
            
            ovc.modalPresentationStyle = UIModalPresentationFullScreen;
            ovc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [self presentViewController:ovc animated:YES completion:^{
                [UIApplication sharedApplication].keyWindow.rootViewController = ovc;
            }];
            
            return NO;
        }
        else {
            [self modeSelector];
            
            return NO;
        }
    }
    
    return YES;
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if(viewController.tintColor == nil){
        tabBarController.tabBar.tintColor = [UIColor blackColor];
    }
    else {
        tabBarController.tabBar.tintColor = viewController.tintColor;
    }
}

- (void)modeSelector{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Profile", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self modeGuest];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"My Offers", @"admin", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self myOffers];
    }]];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hostModeActivated"]){
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Activate moderator mode", @"admin", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self modeHost];
        }]];
    }
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(self.tabBar.width/4*3 + ((self.tabBar.width/4)/2), self.view.height - self.tabBar.height, self.tabBar.width/4, self.tabBar.height);
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)myOffers{
    self.settingsMode = modeOffers;
    
    [self setViewControllers:[self configureAndGetViewControllers] animated:false];
    [self setSelectedIndex:self.tabBar.items.count-1];
}

- (void)modeHost{
    self.settingsMode = modeProfile;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hostModeActivated"]){
//        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hostMode"]){
//            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"hostMode"];
//            
//            [self initViewControllers];
//        }
        
        [self setViewControllers:[self configureAndGetViewControllers] animated:false];
        [self setSelectedIndex:self.tabBar.items.count-1];
    }
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"Please insert your invitation code.", @"admin", @"") message:NSLocalizedStringFromTable(@"If you don't have one, apply to become an influencer at www.ardnd.com.", @"admin", @"") preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.secureTextEntry = true;
        }];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if([((NSString*)alert.textFields[0].text) hasPrefix:@"ARDNDHOST0"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hostModeActivated"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self modeHost];
            }
            else {
                [self presentViewController:alert animated:true completion:^{
                    
                }];
            }
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self presentViewController:alert animated:true completion:^{
            
        }];
    }
    
}

- (void)modeGuest{
    self.settingsMode = modeProfile;
    
    [self setViewControllers:[self configureAndGetViewControllers] animated:false];
    [self setSelectedIndex:self.tabBar.items.count-1];
}

// method called, when we switch the tabs
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    tabBar.items[item.tag].badgeValue = nil;
    if(item.tag==0){
        //[self.tabBar setHidden:YES];
        //[self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    //[self setSidebarButton];
    
    self.title = item.title;
}

-(void)setSelectedIndex:(NSUInteger)selectedIndex{
    [super setSelectedIndex:selectedIndex];
    [self tabBarController:self didSelectViewController:_customViewControllers[selectedIndex]];
}

-(void)initViewControllers{
    [self setViewControllers:[self configureAndGetViewControllers] animated:YES];
    if(_customViewControllers.count==1){
        self.tabBar.hidden = YES;
    }
    else {
        self.tabBar.hidden = NO;
    }
}

-(void)updateViewControllers{
    [self updateViewControllersAnimated:NO];
}

-(void)updateViewControllersAnimated:(BOOL)animated{
    [self setViewControllers:_customViewControllers animated:animated];
    if(_customViewControllers.count==1){
        self.tabBar.hidden = YES;
    }
    else {
        self.tabBar.hidden = NO;
    }
}

-(NSArray*)configureAndGetViewControllers{
    
    NSString *startStoryboardName = @"StructureOverViewInline";
    if(IDIOM==IPAD){
        startStoryboardName = @"StructureOverViewInline-iPad";
    }
    
    _startViewController = [StructureOverViewController loadNowFromStoryboard:startStoryboardName];
    _startViewController.view.tag = 0;

    _customViewControllers = [[NSMutableArray alloc] init];

    
    NSLog(@" customViewControllers: %@", _customViewControllers);

    
    //// BEGIN SEARCH
    {
//        UIViewController *vc = [StartStreamViewController load:@"StartStreamViewController" fromStoryboard:@"StartStreamViewController"]; //[gestgidStartViewController loadNowFromStoryboard:@"gestgidStart"]; // should be master view for structure element 1
        UIViewController *vc = [StartStreamCollectionViewController load:@"StartStreamCollectionViewController" fromStoryboard:@"StartStreamCollectionViewController"];
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        // set tab bar item
        nvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Home",@"")
                                                       image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-home-outline.png"] forSize:CGSizeMake(25, 25)]
                                               selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-home.png"] forSize:CGSizeMake(25, 25)]];
        
        [_customViewControllers addObject:nvc];
    }
    //// END SEARCH
    
    
    //// BEGIN REQUESTS
    {
        OfferRequestsTableViewController *vc = [OfferRequestsTableViewController load:@"OfferRequestsTableViewController" fromStoryboard:@"OfferRequestsTableViewController"];
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        nvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Requests", @"")
                                                       image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-chatboxes-outline.png"] forSize:CGSizeMake(25, 25)]
                                               selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-chatboxes.png"] forSize:CGSizeMake(25, 25)]];
        nvc.tabBarItem.tag = 1;
        
        [_customViewControllers addObject:nvc];
    }
    //// END REQUESTS
    
    
    //// BEGIN SEARCH
    {
        gestgidStartViewController *vc = [gestgidStartViewController loadNowFromStoryboard:@"gestgidStart"]; // should be master view for structure element 1
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        // set tab bar item
        nvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil//NSLocalizedString(@"Search",@"")
                                                       image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-search.png"] forSize:CGSizeMake(45, 45)]
                                               selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-search-strong.png"] forSize:CGSizeMake(45, 45)]];
        
        nvc.tintColor = [UIColor colorWithRGBHex:0xcc9933];
        nvc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        //[UITabBarItem appearanceWhenContainedIn:[vc0 class], nil];
        
        [_customViewControllers addObject:nvc];
        [vc startSearch];
    }
    //// END SEARCH
    
    
    //// BEGIN MESSAGES
    {
        UserChatViewController *vc = [UserChatListViewController load:@"UserChatListViewController" fromStoryboard:@"UserChatListViewController"];
        
        NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        nvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Messages", @"")
                                                       image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-chatbubble-outline.png"] forSize:CGSizeMake(25, 25)]
                                               selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-chatbubble.png"] forSize:CGSizeMake(25, 25)]];
        nvc.tabBarItem.tag = 4;
        
        [_customViewControllers addObject:nvc];
    }
    //// END MESSAGES
    
    
    //// BEGIN SETTINGS
    {
        if (self.settingsMode == modeProfile) {
            SettingsViewController *vc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
            
            nvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Me", @"admin" ,@"")
                                                            image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-contact-outline.png"] forSize:CGSizeMake(25, 25)]
                                                    selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-contact.png"] forSize:CGSizeMake(25, 25)]];
            nvc.tabBarItem.tag = 2;
            
            [_customViewControllers addObject:nvc];
        }
        
        else if (self.settingsMode == modeOffers) {
            MyOffersListViewController *vc = [MyOffersListViewController load:@"MyOffersListViewController" fromStoryboard:@"MyOffersListViewController"];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
            
            nvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Me", @"admin" ,@"")
                                                            image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-contact-outline.png"] forSize:CGSizeMake(25, 25)]
                                                    selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-contact.png"] forSize:CGSizeMake(25, 25)]];
            
            [_customViewControllers addObject:nvc];
        }
        
    }
    //// END SETTINGS
    
    
    
    //_startViewController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:4];
    //_startViewController.tabBarItem.tag = 3;
    
    //[_customViewControllers addObject:_startViewController];
    
    // return the view controllers
    
    //if([[NSUserDefaults standardUserDefaults] boolForKey:@"hostMode"]){
    //    MyOffersListViewController *molvc = [MyOffersListViewController load:@"MyOffersListViewController" fromStoryboard:@"MyOffersListViewController"];
    //
    //    NavigationViewController *nvc1 = [[NavigationViewController alloc] initWithRootViewController:molvc];
    //
    //    nvc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedStringFromTable(@"My Offers", @"admin", @"")
    //                                                    image:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-cart-outline.png"] forSize:CGSizeMake(25, 25)]
    //                                            selectedImage:[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-cart.png"] forSize:CGSizeMake(25, 25)]];
    //
    //    [_customViewControllers addObject:nvc1];
    //}
    
    return _customViewControllers;
}

- (void)setFrame:(CGRect)frame OfChildViewControllersForViewController:(UIViewController*) viewController{
    viewController.view.frame = frame;
    [viewController.view layoutIfNeeded];
    if(viewController.childViewControllers.count>0){
        for(UIViewController *vc in viewController.childViewControllers){
            [self setFrame:frame OfChildViewControllersForViewController:vc];
        }
    }
    if([viewController isKindOfClass:[UINavigationController class]]){
        for(UIViewController *vc in ((UINavigationController *)viewController).viewControllers){
            [self setFrame:frame OfChildViewControllersForViewController:vc];
        }
    }
    if([viewController isKindOfClass:[SWRevealViewController class]]){
        UIViewController *vc = ((SWRevealViewController *)viewController).frontViewController;
        [self setFrame:frame OfChildViewControllersForViewController:vc];
    }
}

- (void)ifbckDidLoadViewController:(UIViewController *)viewController atIndex:(NSInteger)index {

    
    CGRect frame;

    frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 49);
    UIViewController *vcWrapper = [UIViewController new];
    [vcWrapper.view setFrame: frame];
    UIView *containerView = [[UIView alloc] initWithFrame:frame];
    [vcWrapper.view addSubview:containerView];
    [vcWrapper.view setBackgroundColor:[UIColor whiteColor]];
    [vcWrapper addChildViewController:viewController];
    [self setFrame:frame OfChildViewControllersForViewController:viewController];
    [containerView addSubview:viewController.view];

    [viewController didMoveToParentViewController:vcWrapper];

    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);

    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;
    tabButton = [_startViewController loadImageForItem:_startViewController.data[index]];

    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // get title
    tabBarItemTitle = _startViewController.data[index][@"name"];

    // set tab bar item
    vcWrapper.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:index+1];
    
    if(_hideFirstElement)
        index = index-1;
    
    _customViewControllers[index+1] = vcWrapper;

    [self updateViewControllersAnimated:YES];
}

// pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
    
    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

// know the current state
- (BOOL)tabBarIsVisible {
    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

@end
