//
//  MessageGenerator.swift
//  if_framework
//
//  Created by Christopher on 10/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import Chatto
import ChattoAdditions

func createTextMessage(fromMessage: UserMessage) -> DemoTextMessageModel{
    return createTextMessage(uid: "1", fromMessage: fromMessage)
}

func createTextMessage(uid: String, fromMessage: UserMessage) -> DemoTextMessageModel{
    let messageModel = MessageModel(uid: uid, senderId: "\(fromMessage.from_user_id)", type: TextMessageModel<MessageModel>.chatItemType, isIncoming: false, date: Date(timeIntervalSince1970:fromMessage.tstamp), status: .success)
    let textMessageModel = DemoTextMessageModel(messageModel: messageModel, text: fromMessage.content!)
    return textMessageModel
}

func createTextMessageModel(_ uid: String, text: String, isIncoming: Bool) -> DemoTextMessageModel {
    let messageModel = createMessageModel(uid, isIncoming: isIncoming, type: TextMessageModel<MessageModel>.chatItemType)
    let textMessageModel = DemoTextMessageModel(messageModel: messageModel, text: text)
    return textMessageModel
}


func createMessageModel(_ uid: String, isIncoming: Bool, type: String) -> MessageModel {
    let senderId = isIncoming ? "1" : "2"
    let messageStatus = isIncoming || arc4random_uniform(100) % 3 == 0 ? MessageStatus.success : .failed
    let messageModel = MessageModel(uid: uid, senderId: senderId, type: type, isIncoming: isIncoming, date: Date(), status: messageStatus)
    return messageModel
}

func createPhotoMessageModel(_ uid: String, image: UIImage, size: CGSize, isIncoming: Bool) -> DemoPhotoMessageModel {
    let messageModel = createMessageModel(uid, isIncoming: isIncoming, type: PhotoMessageModel<MessageModel>.chatItemType)
    let photoMessageModel = DemoPhotoMessageModel(messageModel: messageModel, imageSize:size, image: image)
    return photoMessageModel
}

extension TextMessageModel {
    static var chatItemType: ChatItemType {
        return "text"
    }
}

extension PhotoMessageModel {
    static var chatItemType: ChatItemType {
        return "photo"
    }
}

class MessageGenerator {
    var messages = [UserMessage]()
    
    var userID:Int = UserDefaults.standard.integer(forKey: "loggedInUserID")
    
    var from:Int
    var to:Int
    
    var nextMessageId:Int = 0
    
    public var onMessageAdded: ((_ message: UserMessage) -> Void)?
    public var onMessagesAdded: (() -> Void)?
    
    init(from: Int, to: Int){
        
        self.from = from
        self.to = to
        
        self.reload()
        
        UserMessages.shared.onNewMessage = { message in
            if(message.from_user_id == Int32(self.to)) {
                self.addMessage(message: message)
            }
        }
    }
    
    func generate(id: Int) -> MessageModelProtocol{
        return generate(message: self.messages[id])
    }
    
    func generateText(id: String, text: String, isIncoming: Bool) -> DemoTextMessageModel{
        return createTextMessageModel(id, text: "test", isIncoming: isIncoming)
    }
    
    func generate(message: UserMessage) -> MessageModelProtocol {
        let uid = "\(self.nextMessageId)"
        self.nextMessageId += 1
        let messageModel = MessageModel(uid: uid, senderId: "\(message.from_user_id)", type: TextMessageModel<MessageModel>.chatItemType, isIncoming: "\(message.from_user_id)" != "\(userID)", date: Date(timeIntervalSince1970:message.tstamp), status: .success)
        let textMessageModel = DemoTextMessageModel(messageModel: messageModel, text: message.content!)
        return textMessageModel
    }
    
    func addMessage(message:UserMessage){
        self.onMessageAdded?(message)
    }
    
    func reload(){
        let moc: NSManagedObjectContext! = CoreDataStack.sharedStack.managedObjectContext
        let request:NSFetchRequest<UserMessage> = UserMessage.fetchRequest()
        
        // only get messages that are
        let compound = NSCompoundPredicate(orPredicateWithSubpredicates: [
            // (from myself AND to him)
            NSCompoundPredicate(andPredicateWithSubpredicates:[NSPredicate(format: "from_user_id = %i", from), NSPredicate(format: "to_user_id = %i", to)]),
            // OR (from him and to myself)
            NSCompoundPredicate(andPredicateWithSubpredicates:[NSPredicate(format: "from_user_id = %i", to), NSPredicate(format: "to_user_id = %i", from)]),
            ]
        )
        
        request.predicate = compound
        request.sortDescriptors = [NSSortDescriptor(key: "tstamp", ascending: false)]
        
        // show all current messages
        self.messages = [UserMessage]()
        var results:[UserMessage]?
        var newestTstamp = 0.0
        do {
            results = try moc.fetch(request)
            
            if results != nil {
                for result in results! {
                    self.messages.append(result)
                    if newestTstamp == 0.0 {
                        newestTstamp = result.tstamp
                    }
                }
                self.nextMessageId = self.messages.count
            }
        }
        catch {
            print(error)
        }
        
        self.onMessagesAdded?()
        
        self.syncFromServer(newestTstamp: newestTstamp)
    }
    
    func syncFromServer(newestTstamp: Double){

        
        DispatchQueue.global().async {
            let response = API.getFrom(SECURED_API_URL+"my-messages/\(self.from)/\(self.to)/\(newestTstamp)")
            
            switch response {
            case is [String:Any]:
                
                break
            case is [[String:Any]]:
                DispatchQueue.main.async {
                    var count = 0
                    
                    for message in response as! [[String:Any]] {
                        
                        if let from_user_id = message["from_user_id"] as? String, let to_user_id = message["to_user_id"] as? String {
                            let tstamp = (message["tstamp"] as! NSString).doubleValue
                            
                            let userMessage = UserMessage.create()
                            
                            userMessage.content = message["content"] as? String
                            userMessage.from_user_id = (from_user_id as NSString).intValue
                            userMessage.to_user_id = (to_user_id as NSString).intValue
                            userMessage.tstamp = tstamp
                            
                            self.messages.append(userMessage)
                            
                            count += 1
                            
                            self.onMessageAdded?(userMessage)
                        }
                    }
                    
                    CoreDataStack.sharedStack.saveContext()
                    
                    if count > 0 {
                    }
                }
                break
            default:
                
                break
            }
        }
//        // get the highest tstamp
//        request.fetchLimit = 1
//        request.returnsDistinctResults = true
//        request.propertiesToFetch = ["tstamp"]
//
//
//        do {
//            results = try moc.fetch(request)
//            
//            var newestTstamp = 0.0
//            
//            if(results != nil && results!.count>0){
//                if let result = results?[0] {
//                    newestTstamp = result.tstamp
//                }
//            }
//            
//            DispatchQueue.global().async {
//                let response = API.getFrom(SECURED_API_URL+"my-messages/\(self.from)/\(self.to)/\(newestTstamp)")
//                
//                switch response {
//                case is [String:Any]:
//                    
//                    break
//                case is [[String:Any]]:
//                    DispatchQueue.main.async {
//                        var count = 0
//                        
//                        for message in response as! [[String:Any]] {
//                            
//                            if let from_user_id = message["from_user_id"] as? String, let to_user_id = message["to_user_id"] as? String {
//                                let tstamp = (message["tstamp"] as! NSString).doubleValue
//                                
//                                let userMessage = UserMessage.create()
//                                
//                                userMessage.content = message["content"] as? String
//                                userMessage.from_user_id = (from_user_id as NSString).intValue
//                                userMessage.to_user_id = (to_user_id as NSString).intValue
//                                userMessage.tstamp = tstamp
//                                
//                                self.messages.append(userMessage)
//                                
//                                count += 1
//                                
//                                self.onMessageAdded?(userMessage)
//                            }
//                        }
//                        
//                        CoreDataStack.sharedStack.saveContext()
//                        
//                        if count > 0 {
//                        }
//                    }
//                    break
//                default:
//                    
//                    break
//                }
//            }
//        }
//        catch {
//            print(error)
//        }
    }
}
