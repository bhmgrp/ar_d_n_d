//
//  SidebarTableViewCell.m
//  Pickalbatros
//
//  Created by User on 09.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "SidebarTableViewCell.h"

@implementation SidebarTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    UIView *v = self.messagesAmountLabelBackground;
    [v.layer setCornerRadius:v.height / 2];
    [v.layer setBorderColor:[[UIColor colorWithRGBHex:0x000000] CGColor]];
    [v.layer setBorderWidth:0.5f];
    //[self.messagesAmountLabel.layer setBorderWidth:1.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
