//
//  AppVersionManagement.m
//  Pickalbatros
//
//  Created by User on 12.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "AppVersionManagement.h"
#import "PlaceOverViewController.h"
#import "MBProgressHUD.h"
#import "LoadingInformationViewController.h"

@interface AppVersionManagement()
@property (nonatomic) NSMutableData *downloadedData;
@property (nonatomic) NSMutableDictionary *responseData;
@property (nonatomic,strong) NSUserDefaults *userDefaults;
@property (nonatomic) int newTimestamp;
@property (nonatomic) UIAlertView *updateAlertView;
@end
@implementation AppVersionManagement


/*
-(instancetype)init{
    self = [super init];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAvailable) name:@"updateForPlaceAvailable" object:nil];
    return self;
}
*/

- (void)checkForUpdates{
    if(!_userDefaults){
        _userDefaults = [NSUserDefaults standardUserDefaults];
    }
    int placeID = ([_userDefaults objectForKey:@"selectedPlaceID"]!=nil && ![[_userDefaults objectForKey:@"selectedPlaceID"] isKindOfClass:[NSNull class]])?[[_userDefaults objectForKey:@"selectedPlaceID"] intValue]:0;
    
    NSURL *url = [NSURL URLWithString:
                  [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonVersiontimeForPlace=%i",API_URL, IFBCKPID, placeID]
                  ];
    NSLog(@"check for updates url: %@",url);

    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    // Parse the JSON that came in
    NSError *error;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
    _responseData = [jsonArray mutableCopy];
    //NSLog(@"%@",_responseData);
    int currentTimestamp = [[_userDefaults objectForKey:@"placeVersionTimestamp"] intValue];
    
    int newTimestamp = [_responseData[@"tstamp"] intValue];
    _newTimestamp = newTimestamp;
    NSLog(@"current: %i, new: %i", currentTimestamp, newTimestamp);
    
    if(newTimestamp>currentTimestamp && currentTimestamp>0){
        NSLog(@"update available");
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"updateForPlaceAvailable" object:nil];
        [self updateAvailable];
    }
    
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    //NSLog(@"Connection failed, Error: %@" ,error);
    //[[[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

-(void)updateAvailable{
    /*
     _updateAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                message:NSLocalizedString(@"There is an update available for the selected place, do you want to download it now?",@"")
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"Cancel",@"")
                                      otherButtonTitles:NSLocalizedString(@"Yes", @""), nil];
    
    [_updateAlertView show];
    */
    
    LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
    //[livc showAsOverlay];
    livc.providesPresentationContextTransitionStyle = YES;
    livc.definesPresentationContext = YES;
    [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;

    //[livc showAsOverlayOnViewController:vc];
    
    [vc presentViewController:livc animated:NO completion:^{}];
    
}

-(void)forceUpdateWithQuestion{
    
    LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
    //[livc showAsOverlay];
    livc.headLineText = NSLocalizedString(@"Do you want to download all information for the selected place?",@"");
    livc.providesPresentationContextTransitionStyle = YES;
    livc.definesPresentationContext = YES;
    livc.placeID = (int)[globals sharedInstance].placeID;
    [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    //[livc showAsOverlayOnViewController:vc];
    
    [vc presentViewController:livc animated:NO completion:^{}];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"button index: %ld",buttonIndex);
    if(buttonIndex != 0){
        [self updateAllData];
        NSNumber *newTimestamp = @(_newTimestamp);
        [_userDefaults setObject:newTimestamp forKey:@"placeVersionTimestamp"];
        NSLog(@"new timestamp: %@", newTimestamp);
        [_userDefaults synchronize];
        NSLog(@"updating");
    }
}

-(void)updateAllData{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"updateStructures" object:nil];
    //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //hud.mode = MBProgressHUDModeIndeterminate;
    //hud.labelText = @"Loading";
    NSDictionary *placeDict = [[localStorage storageWithFilename:@"currentPlace"] objectForKey:@"selectedPlaceDictionary"];
    int parentID = [placeDict[@"id_parent_place"] intValue];
    [[localStorage storageWithFilename:@"homeModel"] clearFile];
    [[localStorage storageWithFilename:@"places"] clearFile];
    [[localStorage storageWithFilename:@"structures"] clearFile];
    [[localStorage storageWithFilename:@"checkout"] clearFile];
    
    PlaceOverViewController *povc = [[PlaceOverViewController alloc] init];
    povc.placeID = parentID;
    povc.ignoreImages = YES;
    //NSLog(@"parentID: %i", parentID);
    [povc loadData];
    //[hud hide:YES];
}

@end
