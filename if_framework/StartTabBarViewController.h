//
//  StartTabBarViewController.h
//  if_framework
//
//  Created by User on 3/4/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iFeedbackModel.h"

@interface StartTabBarViewController : UITabBarController <ifbckDelegate, UINavigationControllerDelegate, UITabBarDelegate, UITabBarControllerDelegate>

- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion;
- (BOOL)tabBarIsVisible;

@end
