//
//  DefaultProfileCollectionViewCell.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/23/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class DefaultProfileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
