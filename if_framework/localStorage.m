//
//  LocalStorage.m
//  pickalbatros
//
//  Created by User on 12.05.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "localStorage.h"

@implementation localStorage

+(localStorage*)defaultStorage{
    localStorage *storage = [[localStorage alloc] init];
    storage.fileName = @"data.plist";
    return storage;
}
+(localStorage*)storageWithFilename:(NSString*)filename{
    return [self storageWithFilename:filename atPath:nil];
}
+(localStorage*)storageWithFilename:(NSString*)filename atPath:(NSString*)path{
    localStorage *storage = [[localStorage alloc] init];
    
    // init filemanager and define paths
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = documentPaths[0];
    NSString *folderPath = [documentsDirectoryPath stringByAppendingPathComponent:path];

    
    // create the folder if it's not existing
    BOOL isDir;
    if (![fileManager fileExistsAtPath:folderPath isDirectory:&isDir]) {
        NSError *error;
        [fileManager createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    
    // return the storage
    storage.fileName = [NSString stringWithFormat:@"%@.plist",filename];
    storage.filePath = path;
    return storage;
}


-(BOOL)writeDictionary:(NSDictionary *)dict forKey:(id)key{
    NSMutableDictionary *fileData;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *path;
    if(_filePath!=nil){
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",_filePath,_fileName]];
    }
    else {
        path = [documentsDirectory stringByAppendingPathComponent:_fileName];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath: path])
    {
        fileData = [[NSMutableDictionary alloc] init];
        if ([fileData initWithContentsOfFile: path])
        {
            fileData = [fileData initWithContentsOfFile: path];
        }
        
    }
    else
    {
        [fileManager createFileAtPath:path contents:nil attributes:nil];
        
        fileData = [[NSMutableDictionary alloc] initWithCapacity:0];
        
    }
    
    fileData[key] = dict;
    
    BOOL wrote = [fileData writeToFile:path atomically:YES];
    return wrote;
}

-(id)getObjectForKey:(id)key{
    NSMutableDictionary *fileData;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *path;
    if(_filePath!=nil){
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",_filePath,_fileName]];
    }
    else {
        path = [documentsDirectory stringByAppendingPathComponent:_fileName];
    }
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(self.fileName != nil){
        if ([fileManager fileExistsAtPath: path])
        {
            fileData = [[NSMutableDictionary alloc] init];
            if ([fileData initWithContentsOfFile: path])
            {
                fileData = [fileData initWithContentsOfFile: path];
            }
            id obj = fileData[key];
            if([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSArray class]]){
                obj = [obj mutableCopy];
            }
            return obj;
        }
    }
    return nil;
}

-(id)objectForKey:(id)key{
    return [self getObjectForKey:key];
}

-(id)removeNilFromObject:(id)object{
    if(object==nil){
        return @"";
    }
    if([object isKindOfClass:[NSNull class]]){
        return @"";
    }
    
    if([object isKindOfClass:[NSArray class]] ||
       [object isKindOfClass:[NSMutableArray class]]){
        
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        NSArray *array = (NSArray*)object;
        
        for(id object in array){
            if(object!=nil && ![object isKindOfClass:[NSNull class]]){
                [tempArray addObject:[self removeNilFromObject:object]];
            }
        }
        
        return tempArray;
    }
    if([object isKindOfClass:[NSDictionary class]] ||
       [object isKindOfClass:[NSMutableDictionary class]]){
        
        NSMutableDictionary *tempDict = [@{} mutableCopy];
        
        NSDictionary *dict = (NSDictionary*) object;
        
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
            if(object!=nil && ![object isKindOfClass:[NSNull class]]){
                tempDict[key]=[self removeNilFromObject:object];
            }
        }];
        
        return tempDict;
    }
    
    return object;
}

-(void)setObject:(id)object forKey:(id<NSCopying>)key{
    NSMutableDictionary *fileData;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *path;
    if(_filePath!=nil){
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",_filePath,_fileName]];
    }
    else {
        path = [documentsDirectory stringByAppendingPathComponent:_fileName];
    }
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([object isKindOfClass:[NSNull class]]){
        return;
    }
    if(object==nil){
        return;
    }
    
    object = [self removeNilFromObject:object];
    
    if ([fileManager fileExistsAtPath: path])
    {
        fileData = [[NSMutableDictionary alloc] init];
        if ([fileData initWithContentsOfFile: path])
        {
            fileData = [fileData initWithContentsOfFile: path];
        }
        
    }
    else
    {
        [fileManager createFileAtPath:path contents:nil attributes:nil];
        
        fileData = [[NSMutableDictionary alloc] initWithCapacity:0];
        
    }
    
    fileData[key] = object;
    BOOL wrote = false;
    
    @try{
        wrote = [fileData writeToFile:path atomically:YES];
    }
    @catch (NSException * e) {
        
    }
}


-(void)removeObjectForKey:(id<NSCopying>)key{
    NSMutableDictionary *fileData;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *path;
    if(_filePath!=nil){
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",_filePath,_fileName]];
    }
    else {
        path = [documentsDirectory stringByAppendingPathComponent:_fileName];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath: path])
    {
        fileData = [[NSMutableDictionary alloc] init];
        if ([fileData initWithContentsOfFile: path])
        {
            fileData = [fileData initWithContentsOfFile: path];
        }
        
    }
    else
    {
        [fileManager createFileAtPath:path contents:nil attributes:nil];
        
        fileData = [[NSMutableDictionary alloc] initWithCapacity:0];
        
    }
    
    [fileData removeObjectForKey:key];
    
    BOOL wrote = false;
    
    @try{
        wrote = [fileData writeToFile:path atomically:YES];
    }
    @catch (NSException * e) {
        
    }
}

-(void)clearFile{
    NSMutableDictionary *fileData;
    fileData = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *path;
    if(_filePath!=nil){
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",_filePath,_fileName]];
    }
    else {
        path = [documentsDirectory stringByAppendingPathComponent:_fileName];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        [fileManager createFileAtPath:path contents:nil attributes:nil];
        
        fileData = [[NSMutableDictionary alloc] initWithCapacity:0];
        
    }
    
    BOOL wrote = false;
    
    @try{
        wrote = [fileData writeToFile:path atomically:YES];
    }
    @catch (NSException * e) {
        
    }
}

-(void)logCurrentContent{
    NSMutableDictionary *fileData;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *path;
    if(_filePath!=nil){
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",_filePath,_fileName]];
    }
    else {
        path = [documentsDirectory stringByAppendingPathComponent:_fileName];
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    if ([fileManager fileExistsAtPath: path])
    {
        fileData = [[NSMutableDictionary alloc] init];
        if ([fileData initWithContentsOfFile: path])
        {
            fileData = [fileData initWithContentsOfFile: path];
        }
        
        NSLog(@"contents of file %@:\n%@",self.fileName,fileData);
    }
    else {
        NSLog(@"file %@ is not existing",self.fileName);
    }
}

@end
