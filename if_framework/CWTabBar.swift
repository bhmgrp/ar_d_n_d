//
//  CWTabBar.swift
//  if_framework
//
//  Created by Christopher on 4/4/17.
//  Copyright © 2017 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class CWTabBar: UITabBar {

    var separator = UIView()

    var underLine = UIView()
    
    var highLightLine = UIView()
    
    
    override init(frame:CGRect){
        super.init(frame: frame)
        
        separator.height = frame.height - 30
        separator.width = 1
        separator.x = frame.width / 2
        separator.y = 15
        separator.backgroundColor = .lightGray
        
        self.addSubview(separator)
        
        
        underLine.height = 1
        underLine.width = frame.width
        underLine.x = 0
        underLine.y = frame.height - 1
        underLine.backgroundColor = .lightGray
        
        self.addSubview(underLine)
        
        
        highLightLine.height = 4
        highLightLine.width = frame.width / 2
        highLightLine.x = 0
        highLightLine.y = frame.height - 4
        highLightLine.backgroundColor = UIColor(rgbHex: 0xcc9933)
        
        self.addSubview(highLightLine)
        
        self.tintColor = UIColor(rgbHex: 0xcc9933)
        
        
        //UITabBarItem.appearance().titlePositionAdjustment = UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))
        //UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name:"HelveticaNeue", size:16.0)!], for: .normal)
        
        
        //titlePositionAdjustment = UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))
//
        
        
//        startTopTabBar *bar = [super init];
//        
//        bar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 49);
//        bar.tintColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
//        bar.itemPositioning = UITabBarItemPositioningFill;
//        
//        UITabBarItem  *barItem1 = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"All companies",@"") image:nil tag:0];
//        
//        UITabBarItem  *barItem2 = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"My company", @"") image:nil tag:1];
//        
//        
//        [bar setItems:@[barItem1,barItem2] animated:YES];
//        
//        [bar setSelectedItem:bar.items[0]];
//        
//        
//        [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitlePositionAdjustment:UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))];
//        
//        [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:16]} forState:UIControlStateNormal];
//        
//        [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]} forState:UIControlStateSelected];
//        
//        
//        // separator
//        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-0.25, 15, 0.5, 49-15-15)];
//        
//        separatorView.backgroundColor = [UIColor colorWithRGBHex:0x929292 withAlpha:1.0];
//        
//        [bar addSubview:separatorView];
//        
//        
//        // underline
//        UIView *underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, bar.height-1, [UIScreen mainScreen].bounds.size.width, 1.0f)];
//        
//        underlineView.backgroundColor = [UIColor lightGrayColor];
//        
//        [bar addSubview:underlineView];
//        
//        
//        // highlight underline
//        _highlightUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, bar.height-4, [UIScreen mainScreen].bounds.size.width/2-0.25, 4.0f)];
//        
//        _highlightUnderlineView.backgroundColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR withAlpha:1.0];
//        
//        [bar addSubview:_highlightUnderlineView];
//        
//        
//        [bar layoutSubviews];
//        [bar layoutIfNeeded];
//        
//        
//        return bar;
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
