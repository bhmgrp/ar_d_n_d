//
//  WebViewController.h
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <WebKit/WebKit.h>

#import <SafariServices/SafariServices.h>

@interface WebViewController : UIViewController <UIWebViewDelegate, WKNavigationDelegate, WKScriptMessageHandler>


@property (nonatomic) WKWebView *webView;


@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSURL *url;
@property (nonatomic) BOOL urlGiven, hideSharebutton;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL hideSideBarButton;
@property (strong, nonatomic) NSString *currentURL;

@property (nonatomic) NSString *htmlString;
@property (nonatomic) BOOL useHTML;

-(void)hideModalWebView;
-(void)setContentOffsetToZero;

+(WebViewController*)getWebViewController;

@end

@interface NSHTTPCookie(jsstring)
- (NSString *)wn_javascriptString;
@end
