//
//  CommentsViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/30/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

protocol CommentsChangeDelegate: AnyObject {
    func didAddComment(_ comment:[String:Any], postId: String)
    func didDeleteComment(postId: String)
}

class CommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var addCommentTextField: UITextField!
    @IBOutlet fileprivate weak var profilePictureImageView: UIImageView!
    @IBOutlet fileprivate weak var commentTextFieldBottom: NSLayoutConstraint!
    
    var postInfo = Comment(text: "", author: "")
    var comments: [Comment] = []
    var profileImage: UIImage = UIImage(named: "emptyProfileImage")!
    var userName: String = ""
    
    weak var delegate: CommentsChangeDelegate?
    
    // used for adding comment
    var newComment: Comment?
    var indexPathForNewComment: IndexPath?
    
    // used to get to adding comments when view controller appears
    var wantsToAddComment: Bool = false
    
    // MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        comments.insert(postInfo, at: 0)
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.bounds.width / 2
        profilePictureImageView.layer.masksToBounds = true
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: addCommentTextField.frame.height))
        addCommentTextField.leftView = paddingView
        addCommentTextField.leftViewMode = UITextFieldViewMode.always
        addCommentTextField.returnKeyType = .send
        
        tableView.keyboardDismissMode = .onDrag
        
        addCommentTextField.layer.cornerRadius = addCommentTextField.bounds.height / 2
        addCommentTextField.layer.borderWidth = 1.0
        addCommentTextField.layer.borderColor = UIColor.lightGray.cgColor
        addCommentTextField.clipsToBounds = true
        
        let dynamicStorage = localStorage(filename: "dynamicUserSettingsValues")
        if let userName = dynamicStorage?.object(forKey: "firstName") as? String {
            self.userName = userName
        }
        if let userSurname = dynamicStorage?.object(forKey: "lastName") as? String {
            self.userName += userSurname
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDissmiss), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        profilePictureImageView.image = profileImage
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if wantsToAddComment == true {
            addCommentTextField.becomeFirstResponder()
        }
    }
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + comments[section].discussion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentsTableViewCell") as! CommentsTableViewCell
        
        var comment: Comment?
        let screenSize = UIScreen.main.bounds.width
        
        if indexPath.row == 0 {
            // first element should always be post itself
            if indexPath.section == 0 {
                comment = postInfo
            } else {
                comment = comments[indexPath.section]
                cell.separatorInset = UIEdgeInsetsMake(0, screenSize * 2, 0, 0)
            }
        } else {
            comment = comments[indexPath.section].discussion[indexPath.row - 1]
            cell.separatorInset = UIEdgeInsetsMake(0, screenSize * 2, 0, 0)
        }
        
        cell.setComment(comment: comment!)
        
        cell.profileButtonTapHandler = {
            let vcc = get(viewController: "ProfileCollectionViewController") as! ProfileCollectionViewController
            vcc.title = comment!.author
            vcc.filters = "filters[if3_users.id]=\(comment!.profileId)"

            vcc.profileId = comment!.profileId

            self.navigationController?.pushViewController(vcc, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        guard indexPath.section != 0 else {
            return []
        }
        
        var replingComment: Comment?
        if indexPath.row != 0 {
            let comment = self.comments[indexPath.section]
            replingComment = comment.discussion[indexPath.row - 1]
        } else {
            replingComment = self.comments[indexPath.section]
        }
        
        let replyAction = UITableViewRowAction(style: .normal, title: "Reply") { [weak self] (action, indexPath) in
            self?.addCommentTextField.attributedText = NSAttributedString(string: "@\(replingComment!.author ) ", attributes: nameFontAttribute)
            self?.addCommentTextField.becomeFirstResponder()
            
            if let userName = self?.userName {
                self?.newComment = Comment(text: "", author: userName)
            }
            
            if let userImage = self?.profileImage {
                self?.newComment?.authorImage = userImage
            }

            self?.newComment?.isReply = true
            self?.newComment?.commentsId = (replingComment?.commentID)!
            
            replingComment = self?.comments[indexPath.section]
            
            if let discussionCommentCount = replingComment?.discussion.count, discussionCommentCount != 0 {
                self?.indexPathForNewComment = IndexPath(row: tableView.numberOfRows(inSection: indexPath.section), section: indexPath.section)
            } else {
                self?.indexPathForNewComment = IndexPath(row: 1, section: indexPath.section)
            }
        }
        
        guard let authorId = replingComment?.profileId, LoggedUserCredentials.sharedInstance.userHasRights(userId: authorId) == true else {
            return [replyAction]
        }
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] (action, indexPath) in
            var commentId = ""
            var forString = ""
            if indexPath.row != 0 {
                commentId = (self?.comments[indexPath.section].discussion[indexPath.row - 1].commentID)!
                forString = "comment"
                self?.comments[indexPath.section].discussion.remove(at: indexPath.row - 1)
                self?.tableView.deleteRows(at: [indexPath], with: .bottom)
            } else {
                commentId = (self?.comments[indexPath.section].commentID)!
                forString = "post"
                self?.tableView.beginUpdates()
                self?.tableView.deleteSections([indexPath.section], with: .bottom)
                self?.comments.remove(at: indexPath.section)
                self?.tableView.endUpdates()
            }
            
            _ = AdminAPI.delete(url: "ardnd/post/comments/delete/\(commentId)?for=\(forString)")
            self?.delegate?.didDeleteComment(postId: (self?.postInfo.commentID)!)
        }
        
        return [deleteAction, replyAction]
    }
    
    // MARK: textfield delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard LoggedUserCredentials.sharedInstance.hasLoggedUser() == true else {
            CWAlert.showNotification(withTitle: "Error".localized, message: "To do this you should log in first.".localized, onViewController: self)
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let textFieldText = textField.text, textFieldText != "" else {
            return
        }
        
        var comment: Comment?
        var newCommentDictionary: [String:Any] = [:]
        
        let saveComment = {
            DispatchQueue.global().async {
                let response = AdminAPI.saveComment(url: "ardnd/post/comments/save", post: newCommentDictionary)
                if let res = response as? [String:Any]{
                    if let data = res["data"] as? [String:Any] {
                        if let commentId = data["id"] as? Int {
                            DispatchQueue.main.async {
                                comment?.commentID = String(commentId)
                            }
                        }
                    }
                }
            }
        }
        
        if self.newComment != nil {
            comment = self.newComment
            comment!.text = textFieldText
            let replingComment = comments[(self.indexPathForNewComment?.section)!]
            tableView.beginUpdates()
            replingComment.discussion.append(comment!)
            tableView.insertRows(at: [self.indexPathForNewComment!], with: .bottom)
            tableView.endUpdates()
            
            newCommentDictionary["id"] = newComment!.commentsId
            newCommentDictionary["text"] = newComment!.text
            newCommentDictionary["for"] = "comment"
        } else {
            comment = Comment(text: textFieldText, author: userName, authorImage: profileImage)
            self.indexPathForNewComment = IndexPath(item: 0, section: comments.count)
            tableView.beginUpdates()
            tableView.insertSections([comments.count] , with: .top)
            comments.append(comment!)
            tableView.endUpdates()
            
            newCommentDictionary["id"] = postInfo.commentsId
            newCommentDictionary["text"] = comment!.text
            newCommentDictionary["for"] = "post"
            newCommentDictionary["author_name"] = userName
            self.delegate?.didAddComment(newCommentDictionary, postId: self.postInfo.commentID)
        }
        
        textField.text = " "
        saveComment()
        
        newComment = nil
        indexPathForNewComment = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: keyboard notifications
    
    @objc func handleKeyboardShow(_ notification: NSNotification) {
        let keyboardFrame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double

        guard commentTextFieldBottom.constant == 0 else {
            return
        }

        UIView.animate(withDuration: animationDuration) {
            self.commentTextFieldBottom.constant += keyboardFrame.size.height - 50
            self.view.layoutIfNeeded()
        }
    }

    @objc func handleKeyboardDissmiss(_ notification: NSNotification) {
        let animationDuration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double

        UIView.animate(withDuration: animationDuration) {
            self.commentTextFieldBottom.constant = 0
             self.view.layoutIfNeeded()
        }
    }
}
