//
//  ProfileStreamViewController.swift
//  if_framework
//
//  Created by Christopher on 4/3/17.
//  Copyright © 2017 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit
import MapKit

import GooglePlaces

class LocationStreamViewController: StartStreamViewController {
    
    let followedLocationsStorage = localStorage(filename: "followedLocations")
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    var placeId:String = "" {
        didSet {
            self.place["gmsPlaceId"] = self.placeId
        }
    }
    
    @IBAction override func scrollUpButtonPressed(_ sender: UIButton) {
        self.tableView.setContentOffset(CGPoint.init(x:0,y:-65), animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    var place:[String:Any] = [:]
    
    override func viewDidLoad() {
        self.place["name"] = self.title
        
        super.viewDidLoad()
        
        self.tableView.tableHeaderView = self.headerView
        self.tableView.tableHeaderView?.addSubview(self.refreshControl)
        self.tableView.contentInset = .zero
        
        self.initHeader()
    }
    
    override func searchForFilters() {
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func initNavigationItems() {

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Follow".localized, style: .plain, target: self, action: #selector(LocationStreamViewController.followLocation as (LocationStreamViewController) -> () -> ()))
        self.ifIndexOfPlace(place: self.place, then: { (_, _) in
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Unfollow".localized, style: .plain, target: self, action: #selector(LocationStreamViewController.unfollowLocation as (LocationStreamViewController) -> () -> ()))
        }, else: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    var placeClient:GMSPlacesClient?
    func initHeader(){
        self.placeClient = GMSPlacesClient.init()
        
        if self.placeClient != nil{
            if self.placeId != "" {
                NSLog(" LocationStreamVC - showing place for id")
                self.initMapviewFor(gmsPlaceId: self.placeId)
            }
            
            else {
                NSLog(" LocationStreamVC - showing place by name")
                self.placeClient?.autocompleteQuery(self.title ?? "NO LOCATION FOUND", bounds: nil, filter: nil, callback: { (prediction, error) in
                    if prediction != nil {
                        if let placeID = prediction?.first?.placeID {
                            self.initMapviewFor(gmsPlaceId: placeID)
                        }
                    }
                })
            }
        }
    }
    
    func initMapviewFor(gmsPlaceId:String){
        self.placeClient?.lookUpPlaceID(gmsPlaceId, callback: { (gmsPlace, err) in
            if let place = gmsPlace {
                self.mapView.centerCoordinate = place.coordinate
                self.mapView.setRegion(MKCoordinateRegion(center: place.coordinate, span: MKCoordinateSpanMake(0.075, 0.075)), animated: true)
                self.mapView.addAnnotation({let annotation = MKPointAnnotation();annotation.coordinate = place.coordinate;return annotation}())
                
                if place.addressComponents != nil {
                    var gmsAdressParts:[String:Any] = [:]
                    for component:GMSAddressComponent in place.addressComponents! {
                        if component.type == kGMSPlaceTypeLocality {
                            self.place["city"] = component.name
                        }
                        gmsAdressParts[component.type] = component.name
                    }
                    self.place["gmsAdressParts"] = gmsAdressParts
                    self.place["gmsPlaceId"] = place.placeID
                }
            }
        })
    }
    
    func ifIndexOfPlace(place:[String:Any],then doThen:((Int, inout [[String:Any]])->Void)?,else doElse:((inout [[String:Any]])->Void)?){
        
        if var places = self.followedLocationsStorage?.getObjectForKey("followedLocations") as? [[String:Any]] {
            if let index = places.index(where: { (element) -> Bool in
                return element["name"] as? String == place["name"] as? String
            }) {
                NSLog(" LocationStreamVC - having this place")
                if doThen != nil {
                    doThen!(index, &places)
                }
            }
            else {
                if doElse != nil {
                    doElse!(&places)
                }
            }
        }
        else {
            NSLog(" LocationStreamVC - dont have any places yet")
            var places = [] as! [[String:Any]]
            if doElse != nil {
                doElse!(&places)
            }
        }
    }
    
    func addToPlaces(place:[String:Any]){
        self.ifIndexOfPlace(place: place, then: { (index, places) in
            places.remove(at: index)
            places.append(place)
            
            self.followedLocationsStorage?.setObject(places, forKey: "followedLocations" as NSCopying)
        }, else: { (places) in
            places.append(place)
            
            self.followedLocationsStorage?.setObject(places, forKey: "followedLocations" as NSCopying)
        })
    }
    
    func removeFromPlaces(place:[String:Any]) {
        self.ifIndexOfPlace(place: place, then: { (index, places) in
            places.remove(at:index)
            self.followedLocationsStorage?.setObject(places, forKey: "followedLocations" as NSCopying)
        }, else: nil)
    }
    
    @objc func followLocation() {
        self.followLocation(whenDone:nil)
    }
    
    func followLocation(whenDone:(()->Void)?) {
        self.showLoading()
        DispatchQueue.global().async {
            _ = AdminAPI.post(url: "ardnd/user/follow-location", post: ["location":self.place])
            
            DispatchQueue.main.async {
                self.endLoading()
                
                self.addToPlaces(place: self.place)
                
                self.initNavigationItems()

                if whenDone != nil {
                    whenDone!()
                }
            }
        }
    }
    
    @objc func unfollowLocation() {
        self.unfollowLocation(whenDone:nil)
    }

    func unfollowLocation(whenDone:(()->Void)?) {
        self.showLoading()
        DispatchQueue.global().async {
            _ = AdminAPI.post(url: "ardnd/user/unfollow-location", post: ["location":self.place])
            
            DispatchQueue.main.async {
                self.endLoading()
                
                self.removeFromPlaces(place: self.place)
                
                self.initNavigationItems()

                if whenDone != nil {
                    whenDone!()
                }
            }
        }
    }
}
