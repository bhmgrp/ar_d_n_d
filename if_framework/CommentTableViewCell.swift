//
//  CommentTableViewCell.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/30/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class CommentsTableViewCell: UITableViewCell {
    
    fileprivate var comment: Comment = Comment(text: "", author: "")

    @IBOutlet fileprivate weak var commentLabel: UILabel!
    @IBOutlet fileprivate weak var profileImageView: UIImageView!
    @IBOutlet fileprivate weak var profileImageViewLeading: NSLayoutConstraint!
    
    var profileButtonTapHandler:()->Void = {}
    @IBAction func profileButtonTap(_ sender: Any) {
        profileButtonTapHandler()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageView.layer.cornerRadius = profileImageView.bounds.width / 2
        profileImageView.layer.masksToBounds = true
    }
    
    func setComment(comment :Comment) {
        self.comment = comment
        
        if comment.isReply == true {
            profileImageViewLeading.constant = 50
        }
        
        let commentString = NSMutableAttributedString(string: comment.author + " ", attributes: nameFontAttribute)
        let commentText = NSMutableAttributedString(string: comment.text, attributes: commentFontAttribute)
        commentString.append(commentText)
        
        commentLabel.attributedText = commentString

        if comment.authorImage != nil {
            profileImageView.image = comment.authorImage
        } else {
            let userImageString = comment.authorImageUrl
            if userImageString.count != 0 {
                var userImage:UIImage? = nil
                
                if(userImageString.hasPrefix("http")){
                    userImage = imageMethods.uiImage(withWebPath: userImageString)
                }
                else {
                    userImage = imageMethods.uiImage(withServerPathName: userImageString, width: 36)
                }
                
                DispatchQueue.main.async {
                    if userImage != nil  {
                        comment.authorImage = userImage
                        self.profileImageView.image = userImage
                    }
                    else {
                        self.profileImageView.image = UIImage(named: "emptyProfileImage")
                    }
                }
            } else {
                self.profileImageView.image = UIImage(named: "emptyProfileImage")
            }
        }
    }
}
