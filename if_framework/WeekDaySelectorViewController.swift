//
//  WeekDaySelectorViewController.swift
//  if_framework
//
//  Created by Christopher on 10/21/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

let weekdays:[String] = [
    "Mondays".localized,
    "Tuesdays".localized,
    "Wednesdays".localized,
    "Thursdays".localized,
    "Fridays".localized,
    "Saturdays".localized,
    "Sundays".localized,
]
let weekdaysShort:[String] = [
    "Mon".localized,
    "Tue".localized,
    "Wed".localized,
    "Thu".localized,
    "Fri".localized,
    "Sat".localized,
    "Sun".localized,
]

class WeekDaySelectorViewController: MultipleSelectorViewController {

    override func viewDidLoad() {
        self.data = [
            ["name":"Mondays".localized],
            ["name":"Tuesdays".localized],
            ["name":"Wednesdays".localized],
            ["name":"Thursdays".localized],
            ["name":"Fridays".localized],
            ["name":"Saturdays".localized],
            ["name":"Sundays".localized],
        ]
        
        super.viewDidLoad()
    }
}
