//
//  ColorPickerViewController.swift
//  if_framework
//
//  Created by Christopher on 10/5/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class ColorPickerViewController: UIViewController {
    
    // MARK: outlets
    @IBOutlet var colorWell:ColorWell!
    @IBOutlet var colorPicker:ColorPicker!
    @IBOutlet var huePicker:HuePicker!
    
    
    // MARK: variables
    var color:UIColor? = UIColor.white
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height*0.75)
        colorWell.color = color!
        
        let pickerController = ColorPickerController(svPickerView: colorPicker!, huePickerView: huePicker!, colorWell: colorWell!)
        pickerController.color = color
        
        pickerController.onColorChange = {(color, finished) in
            if finished {
                self.color = color
            } else {
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height*0.75)
    }
    
}
