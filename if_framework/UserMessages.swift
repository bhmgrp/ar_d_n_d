//
//  UserUnreadMessages.swift
//  if_framework
//
//  Created by Christopher on 10/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class UserMessages: NSObject {
@objc    static let shared = UserMessages()
    
    let unreadStorage = localStorage(filename: "userUnreadMessages")
    let newestMessageStorage = localStorage(filename: "userNewestMessages")
    var unreadMessages:[String:String] = [:]
    var newestMessages:[String:[String:String]] = [:]
    
    var onNewMessage:((_ message:UserMessage)->Void)? = { message in }
    
    override init() {
        super.init()
        
        self.unreadMessages = self.unreadStorage?.object(forKey: "unreadMessages") as? [String:String] ?? [:]
        self.newestMessages = self.newestMessageStorage?.object(forKey: "newestMessages") as? [String:[String:String]] ?? [:]
    }
    
@objc    func addMessage(message: [String:Any]){
        let userMessage = UserMessage.create()
        
        userMessage.from_user_id = Int32(message["from"] as? String ?? "0")!
        userMessage.to_user_id = Int32(message["to"] as? String ?? "0")!
        userMessage.content = message["content"] as? String ?? ""
        userMessage.tstamp = Double(message["tstamp"] as? String ?? "0")!
        
        CoreDataStack.sharedStack.saveContext()
        
        if self.onNewMessage != nil {
            self.onNewMessage!(userMessage)
        }
        
        let fromString = "\(message["from"] as? String ?? "0")"
        
        self.newestMessages[fromString] = ["content":message["content"] as? String ?? "","tstamp":message["tstamp"] as? String ?? "0"]
        
        self.addUnread(fromUser: fromString)
    }
    
    func addUnread(fromUser from: String){
        self.unreadMessages[from] = "\( Int(self.unreadMessages[from] ?? "0")! + 1 )"
        
        self.save()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateChatsNotification), object: nil)
    }
    
    func updateNewestMessage(message: UserMessage) {
        let fromString = "\(message.from_user_id)"
        
        self.newestMessages[fromString] = ["content":"\(message.content ?? "")","tstamp":"\(message.tstamp)"]
        
        self.addUnread(fromUser: fromString)
    }
    
    func markAsRead(fromUser from: String){
        self.unreadMessages[from] = "0"
        
        self.save()
    }
    
    func save(){
        self.unreadStorage?.setObject(self.unreadMessages, forKey: "unreadMessages" as NSCopying!)
        self.newestMessageStorage?.setObject(self.newestMessages, forKey: "newestMessages" as NSCopying!)
    }
}
