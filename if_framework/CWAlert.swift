//
//  CWAlert.swift
//  if_framework
//
//  Created by Christopher on 10/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class CWAlert {
    
    static func showNotification(withTitle: String?,message: String?, onViewController: UIViewController){
        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: { (nil) in
            
        }))
        
        onViewController.present(alert, animated: true, completion: { 
            
        })
    }
    
    static func showActionSheet(withTitle: String?, message: String?, onViewController: UIViewController, fromView:UIView, fromRect:CGRect){
        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: .actionSheet)
        
        if let popPresentationController : UIPopoverPresentationController = alert.popoverPresentationController {
            popPresentationController.sourceView = fromView
            popPresentationController.sourceRect = fromRect
        }
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: { (nil) in
            
        }))
        
        onViewController.present(alert, animated: true, completion: {
            
        })
    }
    
    static func toolTip(withTitle: String?, message: String?, onViewController: UIViewController, fromView:UIView, fromRect:CGRect){
        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: .actionSheet)
        
        if let popPresentationController : UIPopoverPresentationController = alert.popoverPresentationController {
            popPresentationController.sourceView = fromView
            popPresentationController.sourceRect = fromRect
        }
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: { (nil) in
            
        }))
        
        onViewController.present(alert, animated: true, completion: {
            
        })
    }
}
