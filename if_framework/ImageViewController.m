//
//  ImageViewController.m
//  if_framework
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "ImageViewController.h"
#import "imageMethods.h"

#import "MBProgressHUD.h"

@interface ImageViewController (){
    imageMethods *images;
    NSString *documentsPath;
}

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    images = [[imageMethods alloc] init];
    documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self performSelector:@selector(loadImage) withObject:nil afterDelay:0.1f];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadImage{
    
    _imageView.image = [self getImageForString:_imageName];
}

-(UIImage*)loadImageWithName:(NSString*)imageName{
    NSString *imageNameFromTstamp = [[imageName lastPathComponent] stringByDeletingPathExtension];
    UIImage *cellImage = nil;
    NSString *structureImageSm = imageName;
    if(![structureImageSm isEqualToString:@""] && structureImageSm != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return cellImage;
        }
        else {
            //Get Image From URL
            if(![structureImageSm hasPrefix:@"files"] && ![structureImageSm hasPrefix:@"/files"]){
                return nil;
            }
            
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@?h=%f",SYSTEM_DOMAIN,structureImageSm,self.view.height*2.0]];
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return cellImage;
        }
        
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    return cellImage;
}

-(UIImage*)getImageForString:(NSString*)imageString{
    
    if([imageString isEqualToString:@""]){
        return nil;
    }
    
    UIImage* returnImage = nil;
    returnImage = [UIImage imageNamed:imageString];
    
    if([imageString hasPrefix:@"http"]){
        imageString = [imageString stringByReplacingOccurrencesOfString:SECURED_API_URL withString:@""];
        imageString = [imageString stringByReplacingOccurrencesOfString:@"https://" withString:@""];
    }
    
    
    if(returnImage==nil){
        returnImage = [self loadImageWithName:imageString];
    }
    
    
    
    
    return returnImage;
}

@end
