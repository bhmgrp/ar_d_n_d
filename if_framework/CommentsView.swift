//
//  CommentsView.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/27/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

let inset :CGFloat = 8
let fontSize :CGFloat = 17
let maximumCommentsNumber :Int = 5

let nameFontAttribute: [NSAttributedStringKey : Any] = [NSAttributedString.Key(rawValue: NSAttributedStringKey.font.rawValue):UIFont.boldSystemFont(ofSize: fontSize)]
let commentFontAttribute: [NSAttributedStringKey : Any] = [NSAttributedString.Key(rawValue: NSAttributedStringKey.font.rawValue):UIFont.systemFont(ofSize: fontSize)]

protocol CommentsViewDelegate :class{
    func showAllCommentsButtonTapped()
}

class CommentsView: UIView {
    
    @IBOutlet weak var commentsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var commentsContainer: UIView!
    @IBOutlet weak var commentsContainerHeight: NSLayoutConstraint!
    
    weak var delegate :CommentsViewDelegate?
    
    @IBOutlet weak var showAllCommentsButton: UIButton!
    @IBAction func showAllCommentsButtonTap(_ sender: Any) {
        delegate?.showAllCommentsButtonTapped()
    }
    
    var comments: [String] = []
    var authors: [String] = []
    
    func setComments(_ comments:[String], withAuthors authors:[String]) {
        var commentsArray:[String]
        if comments.count > maximumCommentsNumber {
            commentsArray = Array(comments.prefix(maximumCommentsNumber))
        } else {
            commentsArray = comments
        }
        
        self.comments = commentsArray
        self.authors = authors
    }
    
    func setUpView() {
        guard commentsContainer != nil else {
            return
        }
        
        clearView()
        for (index, comment) in comments.enumerated() {
            let initialHeight = commentsContainer.height
            let label = UILabel(frame: CGRect(x: inset, y: initialHeight + inset, width: 0, height: 0))
            let commentString = NSMutableAttributedString(string: authors[index] + " ", attributes: nameFontAttribute)
            let commentText = NSMutableAttributedString(string: comment, attributes: commentFontAttribute)
            commentString.append(commentText)
            label.attributedText = commentString
            
            label.sizeToFit()
            commentsContainer.addSubview(label)
            commentsContainerHeight.constant += label.height
            commentsViewHeight.constant += label.height
        }
    }
    
    func clearView() {
        for view in commentsContainer.subviews {
            view.removeFromSuperview()
        }
        
        commentsContainerHeight.constant = 0
        commentsViewHeight.constant = 0
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        setUpView()
    }
}
