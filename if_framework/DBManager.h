//
//  DBManager.h
//  localDBtest
//
//  Created by User on 08.12.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename NS_DESIGNATED_INITIALIZER;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;


-(NSArray *)loadDataFromDB:(NSString *)query;

-(void)executeQuery:(NSString *)query;

@end
