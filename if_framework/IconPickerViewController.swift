//
//  IconPickerViewController.swift
//  if_framework
//
//  Created by Christopher on 10/6/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class IconPickerViewController: AdminViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    public var selectedIconPath:String? = nil
    public var selectedIcon:(String)->Void = {withString in}
    public var hidesWhenSelected:Bool = false
    
    let cellWidth = (UIScreen.main.bounds.size.width-6*10)/5
    var icons:NSArray? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadIcons()
        
        title = "Select an icon".localized
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadIcons(){
        self.showLoading()
        DispatchQueue.global().async {
            self.icons = API.getFrom(SECURED_API_URL + "gestgid/icons") as? NSArray
            
            DispatchQueue.main.async {
                self.collectionView.delegate = self
                self.collectionView.dataSource = self
                self.collectionView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10)
                self.endLoading()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.icons?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "icon", for: indexPath) as! IconPickerCollectionViewCell
        
        cell.tag = indexPath.row
        
        DispatchQueue.global().async {
            let image:UIImage? = imageMethods.resize(imageMethods.uiImage(withServerPathName: (self.icons?[indexPath.row] as! String), width: Float(self.cellWidth)), for: self.tileSize())
            
            DispatchQueue.main.async {
                if(indexPath.row==cell.tag && image != nil){
                    cell.iconImage.image = image
                    cell.setNeedsLayout()
                }
                else {
                    cell.iconImage.image = nil
                    cell.setNeedsLayout()
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        return self.tileSize()
    }
    
    func tileSize() -> CGSize{
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let path = (self.icons?[indexPath.row] as! String)
        self.selectedIconPath = path
        self.selectedIcon(path)
        
        if(self.hidesWhenSelected){
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
}
