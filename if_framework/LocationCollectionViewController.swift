
//  LocationCollectionViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/25/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation
import MapKit
import GooglePlaces

class LocationCollectionViewController: StartStreamCollectionViewController, LocationHeaderDelegate {

    var place:[String:Any] = [:]
    var placeId:String = "" {
        didSet {
            self.place["gmsPlaceId"] = self.placeId
        }
    }
    
    var placeClient:GMSPlacesClient?
    var placeRepresentation:GMSPlace? {
        didSet {
            collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    let followedLocationsStorage = localStorage(filename: "followedLocations")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableLayout.delegate = self
        
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let cellNib = UINib(nibName: "LocationCollectionViewCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: "locationCollectionViewCell")
        
        let headerNib = UINib(nibName: "LocationCollectionViewHeader", bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "locationCollectionViewHeader")
        
        gridLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 400)
        tableLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 400)
        tableLayout.spacingsHeight = 260
        
        viewMode = .gridViewMode
        
        initClient()
        loadData()
        
        if #available(iOS 11.0, *){
            
        } else {
            collectionView.contentInset = UIEdgeInsetsMake(-65, 0, 0, 0)
        }
    }
    
    override func initNavigationItems() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Follow".localized, style: .plain, target: self, action: #selector(LocationStreamViewController.followLocation as (LocationStreamViewController) -> () -> ()))
        self.ifIndexOfPlace(place: self.place, then: { (_, _) in
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Unfollow".localized, style: .plain, target: self, action: #selector(LocationStreamViewController.unfollowLocation as (LocationStreamViewController) -> () -> ()))
        }, else: nil)
    }
    
    func ifIndexOfPlace(place:[String:Any],then doThen:((Int, inout [[String:Any]])->Void)?,else doElse:((inout [[String:Any]])->Void)?){
        
        if var places = self.followedLocationsStorage?.getObjectForKey("followedLocations") as? [[String:Any]] {
            if let index = places.index(where: { (element) -> Bool in
                return element["name"] as? String == place["name"] as? String
            }) {
                NSLog(" LocationStreamVC - having this place")
                if doThen != nil {
                    doThen!(index, &places)
                }
            }
            else {
                if doElse != nil {
                    doElse!(&places)
                }
            }
        }
        else {
            NSLog(" LocationStreamVC - dont have any places yet")
            var places = [] as! [[String:Any]]
            if doElse != nil {
                doElse!(&places)
            }
        }
    }
    
    override func searchForFilters() {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "locationCollectionViewHeader", for: indexPath) as! LocationCollectionViewHeader
        
        header.delegate = self
        header.place = placeRepresentation
        
        header.gridButton.addTarget(self, action: #selector(setGridViewMode), for: .touchUpInside)
        header.tableButton.addTarget(self, action: #selector(setTableViewMode), for: .touchUpInside)
        
        if viewMode == .gridViewMode {
            header.gridButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
            header.tableButton.tintColor = UIColor.lightGray
        } else {
            header.gridButton.tintColor = UIColor.lightGray
            header.tableButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
        }
        
        guard placeRepresentation != nil else {
            return header
        }
        
        header.mapView.centerCoordinate = (placeRepresentation?.coordinate)!
//        header.mapView.setRegion(MKCoordinateRegion(center: (placeRepresentation?.coordinate)!, span: MKCoordinateSpanMake(0.025, 0.025)), animated: true)
        let region = MKCoordinateRegionMakeWithDistance((placeRepresentation?.coordinate)!, 250, 250)
        let adjustedRegion = header.mapView.regionThatFits(region)
        header.mapView.setRegion(adjustedRegion, animated: true)
        header.mapView.addAnnotation({
            let annotation = MKPointAnnotation();
            annotation.coordinate = (placeRepresentation?.coordinate)!;
            return annotation
            }())
        
        if placeRepresentation?.addressComponents != nil {
                var gmsAdressParts:[String:Any] = [:]
            for component:GMSAddressComponent in (placeRepresentation?.addressComponents!)! {
                    if component.type == kGMSPlaceTypeLocality {
                        self.place["city"] = component.name
                    }
                    gmsAdressParts[component.type] = component.name
                }
                self.place["gmsAdressParts"] = gmsAdressParts
                self.place["gmsPlaceId"] = placeRepresentation?.placeID
            }
        
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch viewMode {
        case .gridViewMode:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
            
        case .tableViewMode:
            
            var cell: LocationCollectionViewCell
            if let offerId = data[indexPath.item]["entity_type"] as? String, offerId == "1" {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offerLocationCollectionViewCell", for: indexPath) as! LocationCollectionViewCell
            } else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "locationCollectionViewCell", for: indexPath) as! LocationCollectionViewCell
            }
            
            cell.tag = indexPath.item
            cell.delegate = self
            
            cell.immediateCommentTextField.delegate = self
            cell.immediateCommentTextField.tag = indexPath.row
            
            let headerText = self.data[indexPath.row]["user_name"] as? String ?? ""
            let postText = self.data[indexPath.row]["content"] as? String ?? ""
            
            cell.commentProfileImageView.image = LoggedUserCredentials.sharedInstance.userImage
            
            // take only top 5 comments which is not replies
            if let allComments = data[indexPath.row]["comments"] as? [[String:Any]] {
                let comments = getCommentsFrom(array: allComments)
                cell.setComments(comments)
                
                cell.showAllCommentsTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.topLeftImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: false)
                }
                
                cell.commentButtonTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.topLeftImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: true)
                }
            }
            
            // handling user image
            
            if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
                if self.imageMap[userImageString] != nil {
                    cell.topLeftImageView.image = self.imageMap[userImageString]
                }
                else {
                    DispatchQueue.global().async {
                        var userImage:UIImage? = nil
                        
                        if(userImageString.hasPrefix("http")){
                            userImage = imageMethods.uiImage(withWebPath: userImageString)
                        }
                        else {
                            userImage = imageMethods.uiImage(withServerPathName: userImageString, width: 36)
                        }
                        
                        DispatchQueue.main.async {
                            if userImage != nil && cell.tag == indexPath.row {
                                self.imageMap[userImageString] = userImage
                                cell.topLeftImageView.image = userImage
                            }
                            else {
                                cell.topLeftImageView.image = UIImage(named: "emptyProfileImage")
                            }
                        }
                    }
                }
            }
            else {
                cell.topLeftImageView.image = UIImage(named: "emptyProfileImage")
            }
            
            // handling post image
            
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let image = self.imageMap[postImageString]
                    
                    cell.mainImageView.image = image
                }
//                    else if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
//                    imageMap[postImageString] = image
//                    cell.mainImageView?.image = image
//                }
//                else {
//                    let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
//                    imageDownloadOperation.completionHandler = { [weak self] (image) in
//                        guard let strongSelf = self else {
//                            return
//                        }
//
//                        DispatchQueue.main.async {
//                            cell.mainImageView?.image = image
//                        }
//
//                        strongSelf.imageMap[postImageString] = image
//                        strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
//                    }
//
//                    imageDownloadOperationQueue.addOperation(imageDownloadOperation)
//                    imageDownloadOperations[indexPath] = imageDownloadOperation
//                }
                else {
                    DispatchQueue.global().async {
                        let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
                        DispatchQueue.main.async {
                            if postImage != nil && cell.tag == indexPath.row {
                                self.imageMap[postImageString] = postImage
                                let image = self.imageMap[postImageString]

                                cell.mainImageView?.image = image
                            }
                        }
                    }
                }
            }
            
            cell.profilePictureTapHandler = {
                self.getProfileScreenHandler(indexPath: indexPath)
            }
            
            cell.headerLabel.text = headerText
            cell.postTextLabel.text = postText
            if cell.postTextLabel.text != "" {
                cell.postTextLabel.sizeToFit()
                cell.postTextLabelHeight.constant = cell.postTextLabel.height
            } else {
                cell.postTextLabelHeight.constant = 0
            }
            
            cell.offerButtonTapHandler = {
                self.offerButtonTapHandler(indexPath: indexPath)
            }
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            if cell.hashtagsTextView.text != "" {
                cell.hashtagsTextView?.sizeToFit()
                cell.hashtagsTextViewHeight?.constant = (cell.hashtagsTextView?.height)!
            } else {
                cell.hashtagsTextViewHeight?.constant = 0
            }
            
            let likesCount = data[indexPath.row]["numbers_likes"] as? String ?? "0"
            cell.likeButton.setTitle(likesCount, for: .normal)
            
            if let likedPost = data[indexPath.row]["liked_by_me"] as? Bool {
                cell.liked = likedPost
            }
            
            cell.likeButtonTapHandler = {
                cell.liked = !cell.liked
                // TODO: toggle like api
            }
            
            cell.shareButtonTapHandler = {
                let text = cell.postTextLabel.text ?? ""
                let image = cell.mainImageView.image ?? UIImage()
                self.shareButtonHandler(text: text, image: image)
            }
            
            return cell
        }
    }
    
    // MARK: handling map
    
    func initClient(){
        self.placeClient = GMSPlacesClient.init()
        
        if self.placeClient != nil{
            if self.placeId != "" {
                NSLog(" LocationStreamVC - showing place for id")
                self.initMapviewFor(gmsPlaceId: self.placeId)
            }
                
            else {
                NSLog(" LocationStreamVC - showing place by name")
                self.placeClient?.autocompleteQuery(self.title ?? "NO LOCATION FOUND", bounds: nil, filter: nil, callback: { (prediction, error) in
                    if prediction != nil {
                        if let placeID = prediction?.first?.placeID {
                            self.initMapviewFor(gmsPlaceId: placeID)
                        }
                    }
                })
            }
        }
    }
    
    func initMapviewFor(gmsPlaceId:String){
        self.placeClient?.lookUpPlaceID(gmsPlaceId, callback: { (gmsPlace, err) in
            guard gmsPlace != nil else {
                return
            }
            
            self.placeRepresentation = gmsPlace
            self.collectionView.reloadData()
        })
    }
    
    // MARK: location header delegate
    func locationHeaderWantsToOpenUrl(url: NSURL) {
        let webViewController = WebViewController.getWebViewController()
        webViewController?.titleString = title
        webViewController?.url = url as URL?
        webViewController?.setContentOffsetToZero()
        
        self.navigationController?.pushViewController(webViewController!, animated: true)
    }
}
