//
//  AdminAPI.swift
//  Dashboard
//
//  Created by Christopher on 11/9/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class AdminAPI: NSObject {
    enum postMethods:String {
        case post = "POST"
        case patch = "PATCH"
        case get = "GET"
        case delete = "DELETE"
    }
    
@objc    static let shared:AdminAPI = AdminAPI()
    
@objc    var useIf3Auth:Bool = false
    
@objc    var authToken:String {
        didSet{
            //localStorage(filename: "token").setObject(self.authToken, forKey: "token" as NSCopying!)
            SSKeychain.setPassword(self.authToken, forService: "com.bhmms.dashboard.auth_token", account: "main_acc")
        }
    }
    
@objc    var if3AuthToken:String {
        didSet{
            //localStorage(filename: "token").setObject(self.if3AuthToken, forKey: "if3token" as NSCopying!)
            SSKeychain.setPassword(self.authToken, forService: "com.bhmms.dashboard.if3_auth_token", account: "main_acc")
        }
    }
    
    override init() {
        //self.authToken = localStorage(filename: "token").getObjectForKey("token") as? String ?? ""
        self.authToken = SSKeychain.password(forService: "com.bhmms.dashboard.auth_token", account: "main_acc") ?? ""
        self.if3AuthToken = SSKeychain.password(forService: "com.bhmms.dashboard.if3_auth_token", account: "main_acc") ?? ""
    }
    
@objc    static func generateUrl(fromUrl:String)->String{
        var connector:String = "?"
        
        if fromUrl.contains("?") {
            connector = "&"
        }
        
        return "\(SYSTEM_DOMAIN)/api/\(fromUrl)\(connector)access_token=\(self.shared.authToken)&if3_access_token=\(self.shared.if3AuthToken)"
    }
    
 @objc static func download(url:String,fileName:String) -> Any {
        
        let yourURL = URL(string: self.generateUrl(fromUrl: url))
        
        NSLog(" ADMIN | API - downloading from url: \(yourURL!)");
        
        let urlRequest = URLRequest(url: yourURL!)
        do {
            let theData = try NSURLConnection.sendSynchronousRequest(urlRequest, returning: nil)
            
            var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last
            
            docURL = docURL?.appendingPathComponent(fileName)
            
            if docURL != nil {
                try theData.write(to: docURL!, options: .atomicWrite)
                
                return docURL!
            }
        }
        catch {
            return ""
        }
        
        
        return ""
    }
    
 @objc   static func get(fromUrl:String) -> Any{
        let newUrl = self.generateUrl(fromUrl: fromUrl)
        
//        NSLog(" ADMIN | API - get from url: \(newUrl)");
        
        let response = self.get(url: newUrl)
        
        //NSLog(" ADMIN | API - response: \(response)")
        
        return response
    }
    
 @objc    static func post(url:String,post:[String:Any]) -> Any{
        let newUrl = self.generateUrl(fromUrl: url)
        
        NSLog(" ADMIN | API - post to: \(newUrl)");
        
        let response = self.request(url: newUrl, post: post, method: .post)
        
        //NSLog(" ADMIN | API - response: \(response)")
        
        return response
    }
    
 @objc    static func saveComment(url:String, post:[String:Any]) -> Any{
        let newURL = self.generateUrl(fromUrl: url)
        
        let response = self.request(url: newURL, post: post, method: .post)
        
        return response
    }
    
 @objc    static func update(url:String,post:[String:Any]) -> Any{
        let newUrl = self.generateUrl(fromUrl: url)
        
        NSLog(" ADMIN | API - patch to: \(newUrl)");
        
        let response = self.request(url: newUrl, post: post, method: .patch)
        
        //NSLog(" ADMIN | API - response: \(response)")
        
        return response
    }
    
 @objc    static func delete(url:String) -> Any{
        let newUrl = self.generateUrl(fromUrl: url)
        
        NSLog(" ADMIN | API - delete to: \(newUrl)");
        
        let response = self.request(url: newUrl, post: [:], method: .delete)
        
        //NSLog(" ADMIN | API - response: \(response)")
        
        return response
    }
    
 @objc    static func create(url:String,post:[String:Any]) -> Any{
        return self.post(url: url, post: post)
    }
    
    
    private static func request(url:String,post:Data,method:postMethods,callback:@escaping ([String:Any?])->Void) {
        let urll = URL(string: url) ?? (URL(string: "\(SYSTEM_DOMAIN)/api/admin/locations")!)
        var request = URLRequest(url: urll)
        request.httpMethod = method.rawValue
        if !post.isEmpty {
            request.httpBody = post
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            do {
                if error != nil {
                    callback(["success":0,"error":error!.localizedDescription])
                } else if data != nil && !(data?.isEmpty)!{
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions(rawValue: UInt(0)))
                    if let httpResponse = response as? HTTPURLResponse {
                        var success = (200 <= httpResponse.statusCode && httpResponse.statusCode < 300) ? 1 : 0
                        
                        if let dictData = jsonData as? [String:Any] {
                            if let dictSuccess = dictData["success"] as? String {
                                success = Int(dictSuccess) ?? 0
                            }
                            if let dictSuccess = dictData["success"] as? Int {
                                success = dictSuccess
                            }
                        }
                        
                        callback(["success":success, "data":jsonData, "status":httpResponse.statusCode, "response":httpResponse])
                        return
                    }
                }
                else {
                    if let httpResponse = response as? HTTPURLResponse {
                        callback(["success":(200 <= httpResponse.statusCode && httpResponse.statusCode < 300) ? 1 : 0, "status":httpResponse.statusCode, "response":httpResponse, "raw-data":String(data:data ?? Data(), encoding:.utf8) ?? ""])
                        return
                    }
                }
                
                callback(["success":0, "raw-data":String(data:data ?? Data(), encoding:.utf8) ?? ""])
                return
            }
            catch {
                callback(["success":0, "error":error.localizedDescription, "raw-data":String(data:data ?? Data(), encoding:.utf8) ?? ""])
                return
            }
        })
        task.resume()
    }
    
    private static func request(url:String,post:[String:Any],method:postMethods) -> [String:Any?]{
        
        do {
            NSLog(" ADMIN | API - post dict: \(post)")
            
            var newPost = post
            
            if (self.shared.useIf3Auth) {
                newPost.update(other: self.if3AuthorizationDict())
            }
            
            let jsonPostData = try JSONSerialization.data(withJSONObject: newPost, options: .prettyPrinted)
        
        
            NSLog("%@",String(data: jsonPostData, encoding: .utf8)!)
            
            var returnValue:[String:Any?]? = nil
            
            let callback = {(object:[String:Any?]?) in
                returnValue = object
            }
            
            self.request(url: url, post: jsonPostData, method: method, callback: callback)
            
            while true {
                if let returnValue = returnValue {
                    self.check(response: returnValue)
                    
                    return returnValue
                }
                else {
                    // SOMEHOW WE NEED THIS IN ORDER TO WORK
                    print("", separator:"", terminator:"")
                }
            }
        }
        catch {
            NSLog(" ADMIN | API - error: \(error)")
            
            return ["success":0,"error":error.localizedDescription]
        }
    }
    
    private static func get(url:String) -> Any {
        
        var returnValue:[String:Any?]? = nil
        
        let callback = {(object:[String:Any?]) in
            returnValue = object
        }
        
        var newUrl = url
    
        if (self.shared.useIf3Auth) {
            newUrl = newUrl + (self.if3AuthorizationDict() as NSDictionary).urlEncodedString
            NSLog(" ADMIN | API - updated url: \(newUrl)")
        }
        
        self.request(url: newUrl, post: Data(), method: .get, callback: callback)
        
        while true {
            if let returnValue = returnValue {
                self.check(response: returnValue)
                
                if let returnData = returnValue["data"] {
                    return returnData ?? []
                }
                
                return returnValue
            }
            else {
                // SOMEHOW WE NEED THIS IN ORDER TO WORK
                print("", separator:"", terminator:"")
            }
        }
    }
    
static func check(response:[String:Any?]){
        NSLog(" ADMIN | API - checking response")
        
        if response["status"] as? Int == 401 || response["status"] as? String == "401" {
            NSLog(" ADMIN | API - not authorized")
            DispatchQueue.main.async {
                notAuthorizedError()
            }
        }
    }
    
 @objc   static func notAuthorizedError(){
        let rvc = (UIApplication.shared.keyWindow?.rootViewController)!
        var vc = rvc
    
        if rvc.presentedViewController != nil {
            vc = rvc.presentedViewController!
        }
        
        
        let alert = UIAlertController(title: "Not authorized".localized, message: "Please provide your dashboard username and password in order to proceed.", preferredStyle: .alert)
        
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = "Username".localized
        }
        
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = "Password".localized
            textField.isSecureTextEntry = true
        }
        
        alert.addAction(UIAlertAction(title: "Log In".localized, style: .default, handler: { (nil) in
            DispatchQueue.global().async {
                let urlString = "\(SYSTEM_DOMAIN)/api/admin/auth"
                NSLog(" ADMIN | API - authing: \(urlString)")
                
                let response = self.request(url: urlString, post: ["username":alert.textFields?[0].text ?? "","password":alert.textFields?[1].text ?? ""], method: .post)
                
                if response["success"] as? Int == 0 {
                    vc.present(alert, animated:true, completion:nil)
                }
                else {
                    if let responseData =  response["data"] as? [String:Any] {
                        NSLog(" ADMIN | API - auth response data: \(responseData)")
                        if let token = responseData["token"] as? String {
                            NSLog(" ADMIN | API - token: \(token)")
                            self.shared.authToken = token
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AdminLoggedIn"), object: nil)
                        }
                    }
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        
        vc.present(alert, animated: true, completion:nil)
    }
    
 @objc   static func if3AuthorizationDict() -> [String:Any]{
        // default values
        let userID = UserDefaults.standard.integer(forKey: "loggedInUserID")
        
        let dynamicUserSettingsStorage:localStorage = localStorage(filename: "dynamicUserSettingsValues")
        let loginName = dynamicUserSettingsStorage.object(forKey: "email") as? String ?? ""
        let loginPassword = SSKeychain.password(forService: "PickalbatrosLogin", account: loginName) ?? ""
        let overSocial = dynamicUserSettingsStorage.object(forKey: "overSocial") as? Int == 1 || dynamicUserSettingsStorage.object(forKey: "overSocial") as? String == "1"
        let client_id = dynamicUserSettingsStorage.object(forKey: "client_id") as? String ?? ""
        let client = dynamicUserSettingsStorage.object(forKey: "client") as? String ?? ""
        
        if (userID==0 || (loginName.isEmpty && !overSocial)){
            return [:]
        }
        else if(loginPassword.isEmpty && !overSocial){
            //                UIAlertController *revalidatePasswordController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Password required", 0)
            //                    message:NSLocalizedString(@"Your password is required for authorization. If your password isn't saved after this message, try reinstalling the app.", @"")
            //                    preferredStyle:UIAlertControllerStyleAlert];
            //
            //                [revalidatePasswordController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            //                    textField.secureTextEntry = YES;
            //                    }];
            //
            //                [revalidatePasswordController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            //
            //                    }]];
            //
            //                [revalidatePasswordController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //                    loginPassword = revalidatePasswordController.textFields[0].text;
            //
            //                    [SSKeychain setPassword:loginPassword forService:@"PickalbatrosLogin" account:loginName];
            //                    }]];
            //
            //                [[UIApplication sharedApplication].keyWindow.rootViewController showViewController:revalidatePasswordController sender:nil];
            
            return [:]
        }
        
        
        if (overSocial == false) {
            
            return [
                "customerID": CUSTOMERID,
                "userID": userID,
                "validationPassword": loginPassword,
                "overSocial": overSocial,
                //#warning remove!!!
                //                     @"debug":@YES
            ]
            
        } else {
            
            return [
                "customerID": CUSTOMERID,
                "userID": userID,
                "validationPassword": loginPassword,
                "overSocial": overSocial,
                "client_id": client_id,
                "client": client,
                //#warning remove!!!
                //                     @"debug":@YES
            ]
        }
    }
    
}
