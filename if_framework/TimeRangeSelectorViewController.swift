//
//  TimeRangeSelectorViewController.swift
//  if_framework
//
//  Created by Christopher on 10/24/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class TimeRangeSelectorViewController: AdminViewController {

    var from:String = ""
    var to:String = ""
    
    var fromDatePicker:CWDatePicker? = nil
    var toDatePicker:CWDatePicker? = nil
    var onHide:()->Void = {}
    
    @IBOutlet weak var buttonFrom: UIButton!
    @IBOutlet weak var buttonTo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hide))
        
        if !self.from.isEmpty {
            self.buttonFrom.setTitle(self.from, for: .normal)
        }
        else {
            self.buttonFrom.setTitle("From".localized, for: .normal)
        }
        
        if !self.from.isEmpty {
            self.buttonTo.setTitle(self.to, for: .normal)
        }
        else {
            self.buttonTo.setTitle("To".localized, for: .normal)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.onHide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        if(sender == self.buttonFrom){
            self.toDatePicker?.isHidden = true
            if(fromDatePicker == nil) {
                fromDatePicker = CWDatePicker(withDate: Date(), fromView: self.view)
                fromDatePicker?.hideTimeSwitcher()
                fromDatePicker?.hideToolBar()
                fromDatePicker?.onlyTime = true
                fromDatePicker?.updated = {date in
                    let calendar = NSCalendar.autoupdatingCurrent
                    let components = calendar.dateComponents([.hour, .minute], from: date)
                    let hour = components.hour
                    let minute = components.minute
                    
                    self.from = "\(String(format: "%02d", hour!)):\(String(format: "%02d", minute!))"
                    self.buttonFrom.setTitle(self.from, for: .normal)
                }
                fromDatePicker?.show()
            }
            else {
                fromDatePicker!.isHidden = false
            }
        }
        
        if(sender == self.buttonTo){
            fromDatePicker?.isHidden = true
            if(toDatePicker == nil) {
                toDatePicker = CWDatePicker(withDate: Date(), fromView: self.view)
                toDatePicker?.hideTimeSwitcher()
                toDatePicker?.hideToolBar()
                toDatePicker?.onlyTime = true
                toDatePicker?.updated = {date in
                    let calendar = NSCalendar.autoupdatingCurrent
                    let components = calendar.dateComponents([.hour, .minute], from: date)
                    let hour = components.hour
                    let minute = components.minute
                    
                    self.to = "\(String(format: "%02d", hour!)):\(String(format: "%02d", minute!))"
                    self.buttonTo.setTitle(self.to, for: .normal)
                }
                toDatePicker?.show()
            }
            else {
                toDatePicker!.isHidden = false
            }
        }
    }
    
    @objc func hide() {
        if(self.navigationController != nil){
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {
            self.dismiss(animated: true, completion: {
                
            })
        }
    }
    
}
