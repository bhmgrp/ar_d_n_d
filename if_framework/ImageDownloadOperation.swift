//
//  ImageDownloadOperation.swift
//  gestgid
//
//  Created by Vadym Patalakh on 6/9/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

typealias ImageDownloadCompletionHandlerType = ((UIImage) -> ())?

class ImageDownloadOperation: Operation {
    var urlString: String
    var image: UIImage?
    var completionHandler: ImageDownloadCompletionHandlerType
    
    init(imageUrlString: String) {
        urlString = imageUrlString
    }
    
    override func main() {
        if isCancelled {
            return
        }
        
        guard let image = imageMethods.uiImage(withServerPathName: urlString, width: Float(UIScreen.main.bounds.width)) else {
                return
            }
        
        self.image = image
        completionHandler?(image)
    }
}
