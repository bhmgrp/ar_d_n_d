//
//  ViewController.m
//  localDBtest
//
//  Created by User on 05.12.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import "notificationListViewController.h"
#import "DBManager.h"
#import "webViewController.h"
#import "notificationListTableViewCell.h"
#import "SWRevealViewController.h"

@interface notificationListViewController ()
@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, strong) NSArray *notificationInfoArray;


-(void)loadData;
@end

@implementation notificationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:NOTIFICATION_CAME_IN object:nil];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    if(_showSidebarButton == YES){
        //[self.sidebarButton setTintColor:[UIColor ]];
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        self.navigationItem.leftBarButtonItem = self.sidebarButton;
    }

    self.navigationItem.title = NSLocalizedString(@"Messages",@"");
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    // Initialize the dbManager object.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"db_template.sql"];
    
    [self loadData];
    
    self.navigationItem.backBarButtonItem.title = @"";
    [self.navigationItem.backBarButtonItem setTitle:@""];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (IBAction)addRecord:(id)sender {
    [self performSegueWithIdentifier:@"showNotificationInfo" sender:self];
}

-(void)loadData{
    // Form the query.
    NSString *query = @"select * from notifications";
    
    // Get the results.
    if (self.notificationInfoArray != nil) {
        self.notificationInfoArray = nil;
    }
    
    NSArray *tempArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    self.notificationInfoArray = [[tempArray reverseObjectEnumerator] allObjects];
    
    // Reload the table view.
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (self.notificationInfoArray.count>0)?self.notificationInfoArray.count:1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (self.notificationInfoArray.count>0)?80.0:[UIScreen mainScreen].bounds.size.height-64;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.notificationInfoArray.count==0){
        return [tableView dequeueReusableCellWithIdentifier:@"noMessagesCell"];
    }
    
    // defining the indexes
    NSInteger indexOfTitle = [self.dbManager.arrColumnNames indexOfObject:@"notificationTitle"];
    NSInteger indexOfDescription = [self.dbManager.arrColumnNames indexOfObject:@"notificationDescription"];
    NSInteger indexOfUrl = [self.dbManager.arrColumnNames indexOfObject:@"notificationURL"];
    NSInteger indexOfhas_been_read = [self.dbManager.arrColumnNames indexOfObject:@"has_been_read"];
    
    // defining the objects at the indexes
    NSString *notificationTitle = [NSString stringWithFormat:@"%@", (self.notificationInfoArray)[indexPath.row][indexOfTitle]];
    NSString *notificationDescription = [NSString stringWithFormat:@"%@", (self.notificationInfoArray)[indexPath.row][indexOfDescription]];
    NSString *notificationUrl = [NSString stringWithFormat:@"%@", (self.notificationInfoArray)[indexPath.row][indexOfUrl]];
    int has_been_read = [(self.notificationInfoArray)[indexPath.row][indexOfhas_been_read] intValue];
    NSString *identifier = @"";
    // fill the cells with these information
    if(has_been_read == 0){
        if(notificationUrl == nil || [notificationUrl isEqualToString:@""] || [notificationUrl isEqualToString:@"(null)"]){
            identifier = @"unreadCellWithoutURL";
        } else {
            identifier = @"unreadCellWithURL";
        }
    } else {
        if(notificationUrl == nil || [notificationUrl isEqualToString:@""] || [notificationUrl isEqualToString:@"(null)"]){
            identifier = @"readCellWithoutURL";
        } else {
            identifier = @"readCellWithURL";
        }
    }
    
    // Dequeue the cell.
    notificationListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    //cell.cellDescription.textContainer.lineFragmentPadding = 0;
    cell.cellDescription.text = notificationDescription;
    cell.cellTitle.text = notificationTitle;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the selected record.
        // Find the record ID.
        int recordIDToDelete = [(self.notificationInfoArray)[indexPath.row][0] intValue];
        
        // Prepare the query.
        NSString *query = [NSString stringWithFormat:@"delete from notifications where notificationID=%d", recordIDToDelete];
        
        // Execute the query.
        [self.dbManager executeQuery:query];
        
        // Reload the table view.
        [self loadData];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger indexOfTitle = [self.dbManager.arrColumnNames indexOfObject:@"notificationTitle"];
    NSString *notificationTitle = [NSString stringWithFormat:@"%@", (self.notificationInfoArray)[indexPath.row][indexOfTitle]];
    
    NSInteger indexOfUrl = [self.dbManager.arrColumnNames indexOfObject:@"notificationURL"];
    NSString *notificationUrl = [NSString stringWithFormat:@"%@", (self.notificationInfoArray)[indexPath.row][indexOfUrl]];
    
    NSInteger indexOfID = [self.dbManager.arrColumnNames indexOfObject:@"notificationID"];
    int notificationID = [(self.notificationInfoArray)[indexPath.row][indexOfID] intValue];
    
    NSInteger indexOfhas_been_read = [self.dbManager.arrColumnNames indexOfObject:@"has_been_read"];
    int has_been_read = [(self.notificationInfoArray)[indexPath.row][indexOfhas_been_read] intValue];
    
    if(notificationUrl != nil){
        if(!([notificationUrl isEqualToString:@"(null)"] || [notificationUrl isEqualToString:@""])){
            
            WebViewController *wv = [WebViewController loadNowFromStoryboard:@"WebView"];
            wv.titleString = notificationTitle; //Title für Webview
            wv.url = [NSURL URLWithString:notificationUrl]; //insert url
            wv.urlGiven = YES; //yes if url is inserted above
            //wv.sharingText = @"";
            wv.hideSideBarButton = YES;
            //[wv prepareWebsite];
            [self.navigationController showViewController:wv sender:self];
            
        } else {
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        
    } else {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    // if the notification hasn't been read yet, mark it as read
    if(has_been_read != 1){
        // Prepare the query.
        NSString *query = [NSString stringWithFormat:@"UPDATE notifications SET has_been_read = 1 where notificationID=%i", notificationID];
        
        // Execute the query.
        [self.dbManager executeQuery:query];
        
        // Reload the table view.
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CAME_IN object:nil];
    }
    
}

-(void)editingInfoWasFinished{
    // Reload the data.
    [self loadData];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Force your tableview margins (this may be a bad idea)
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 24, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 24, 0, 0)];
    }
}

@end
