//
//  LoadingInformationViewController.h
//  Pickalbatros
//
//  Created by User on 15.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeModel.h"

@protocol loadingInformationProtocol <NSObject>

-(void)hideLoadingInformationView;


@end


@interface LoadingInformationViewController : UIViewController

@property (nonatomic) NSString *okButtonText, *cancelButtonText, *headLineText, *continueButtonText;

@property (nonatomic) BOOL skipUserInput, invisibleView;

@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UILabel *headLineLabel;

@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void)showAsOverlay;
-(void)showAsOverlayOnViewController:(UIViewController*)vc;

- (IBAction)okButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)continueButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (nonatomic) id<loadingInformationProtocol> delegate;


@property (nonatomic) int placeID;
@property (nonatomic) int pID;

@end
