//
//  StartStreamCollectionViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/25/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation
import GooglePlacePicker

protocol HashtagTapDelegate : class{
    func userTappedAtHashtag(hashtag: String, tag: NSInteger, index: NSInteger)
}

class StartStreamCollectionViewController: AdminViewController, TableCollectionViewLayoutDelegate, HashtagTapDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchControllerDelegate, UISearchBarDelegate, GMSAutocompleteViewControllerDelegate, UITextFieldDelegate, UICollectionViewDataSourcePrefetching, UIDocumentInteractionControllerDelegate, CommentsChangeDelegate {
    
    enum CollectionViewMode {
        case gridViewMode
        case tableViewMode
    }
    
    let documentViewController = UIDocumentInteractionController()
    let notifyingManager = NotifyingLabelManager()
    
    var scrollingPoint = CGPoint(x: 0, y: 0)
    var currentIndexPath: IndexPath?
    
    var hashtagsString: String = ""
    var placeMeta: [String:Any] = NSDictionary.init() as! [String : Any]
    var locationName: String? {
        get {
            if let locationString = UserDefaults.standard.object(forKey: "locationName") as? String {
                if locationString != "" {
                    return locationString
                }
            }
            
            return "Location".localized
        }
        
        set {
            UserDefaults.standard.setValue(newValue, forKey: "locationName")
        }
    }
    
    let imageDownloadOperationQueue = OperationQueue()
    var imageDownloadOperations = [IndexPath:ImageDownloadOperation]()
    
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewWidth: NSLayoutConstraint!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var scrollUpView: UIView!
    @IBOutlet weak var scrollUpImageView: UIImageView!
    
    @IBAction func scrollUpButtonTap(_ sender: Any) {
        self.collectionView.setContentOffset(CGPoint.init(x:0,y:-25), animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    var shouldShowSupplementary: Bool = false {
        didSet {
            tableLayout.shouldShowHeader = shouldShowSupplementary
        }
    }
    
    var filters:String = ""
    var imageMap:[String:UIImage] = [:]
    
    var gridLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    var tableLayout: ProfileCollectionViewLayout = ProfileCollectionViewLayout()
    
    var loadingUntilCount:Int = 100
    
    let loadingSteps:Int = 20
    
    @IBOutlet weak var foodTagsTextField: UITextField!
    
    var viewMode:CollectionViewMode = .gridViewMode {
        didSet {
            switch viewMode {
            case .gridViewMode:
                let offset = collectionView.contentOffset
                collectionView.collectionViewLayout = gridLayout
                collectionView.reloadData()
                collectionView.collectionViewLayout.invalidateLayout()
                collectionView.contentOffset = offset
                break;
            case .tableViewMode:
                let offset = collectionView.contentOffset
                collectionView.collectionViewLayout = tableLayout
                collectionView.reloadData()
                collectionView.collectionViewLayout.invalidateLayout()
                collectionView.contentOffset = offset
                break;
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.title == nil {
            self.title = "Home".localized
        }
        
        _ = LoggedUserCredentials.sharedInstance
        
        foodTagsTextField?.delegate = self
        
        gridLayout.minimumLineSpacing = 1
        gridLayout.minimumInteritemSpacing = 1
        let size = UIScreen.main.bounds.size.width / 3 - 1
        gridLayout.itemSize = CGSize(width: size, height: size)
        
        let cellNib = UINib(nibName: "StartStreamCollectionViewCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: "startStreamCollectionViewCell")
        
        let headerNib = UINib(nibName: "HashtagCollectionViewHeader", bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "hashtagCollectionViewHeader")
        collectionView.keyboardDismissMode = .onDrag
        
        if #available(iOS 10.0, *) {
            collectionView.prefetchDataSource = self
        }
        
        viewMode = .gridViewMode
        
        tableLayout.delegate = self
        
        gridLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 44)
        tableLayout.spacingsHeight = 170
        tableLayout.shouldShowHeader = false
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        }
        
        initNavigationItems()
        
        self.searchViewWidth?.constant = UIScreen.main.bounds.width
        self.searchView?.width = UIScreen.main.bounds.width
        
        if #available(iOS 11.0, *) {
            // iOS 11 section
            if searchView == nil {
                return
            }
            
            searchView.removeFromSuperview()
            let searchViewController = UISearchController(searchResultsController: nil)
            self.navigationItem.searchController = searchViewController
            searchViewController.searchBar.enablesReturnKeyAutomatically = false
            searchViewController.searchBar.placeholder = "#" + "search".localized
            searchViewController.delegate = self
            searchViewController.searchBar.delegate = self
            searchViewController.obscuresBackgroundDuringPresentation = false
            
            setupSearchView()
        }
        else {
            // iOS 10 and below section
            collectionView.contentInset = UIEdgeInsetsMake(-25, 0, 0, 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.locationButton?.setTitle(self.locationName, for: .normal)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = false
        }
        
        addObserverForNotifications()
        LoggedUserCredentials.sharedInstance.updateInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeObserverForNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.data.isEmpty {
            self.searchForFilters()
        }
        
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    func addObserverForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    func removeObserverForNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func searchForFilters() {
        self.filters = ""
        
        let locationName = self.locationName
        let locationHashtags = "location=\(locationName?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        
        let comaHashtags = self.hashtagsString.replacingOccurrences(of: " ", with: ",")
        let foodHashtagsSearch = "&tags=\(comaHashtags.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        
        if locationName != "" && locationName != "Location".localized {
            self.filters += locationHashtags
        }
        
        if self.hashtagsString != "" {
            self.filters += foodHashtagsSearch
        }
        
        self.loadData()
    }
    
    @IBAction func locationButtonTap(_ sender: Any) {
        NSLog(" Clicked location picker")
        let vc = GMSAutocompleteViewController()
        vc.delegate = self
        
        self.present(vc, animated: true, completion: {
            
        })
    }
    
    func setupSearchView() {
        self.searchViewHeight?.constant = 40
        self.searchView?.size.height = 40
    }
    
    func initNavigationItems(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(createImage))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageMethods.resize(UIImage(named:"ardndplus2"), for: CGSize(width: 33, height: 33)), style: .plain, target: self, action: #selector(openMyOffers))
    }
    
    @objc func createImage() {
        self.createPost(type: 1)
    }
    
    @objc func openMyOffers(){
        let vc = get(viewController: "MyOffersListViewController")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func createPost(type:Int) {
        if LoggedUserCredentials.sharedInstance.userId == "0" {
            CWAlert.showNotification(withTitle: "Error".localized, message: "To do this you should log in first.".localized, onViewController: self)
            return
        }
        
        let createController = get(viewController: "CreatePostViewController") as! CreatePostViewController
        
        createController.createdPost = {(post:[String:Any]) in
            self.data.insert(post, at: 0)
            self.collectionView.insertItems(at: [IndexPath(item: 0, section: 0)])
//            self.collectionView.reloadData()
//            self.collectionView.collectionViewLayout.invalidateLayout()
        }
        
        switch type {
        case 1:
            createController.postType = .food
            break
            
        case 2:
            createController.postType = .drinks
            break
            
        case 3:
            createController.postType = .foodAndDrinks
            break
            
        default:
            createController.postType = .food
        }
        
        let navigationController = UINavigationController(rootViewController: createController)
        
        self.present(navigationController, animated: true) {
            
        }
    }
    
    @objc func loadData() {
        DispatchQueue.global().async {
            let res = AdminAPI.get(fromUrl: "ardnd/post/0/20?\(self.filters)")
            
            if let resData = res as? [[String:Any]] {
                DispatchQueue.main.async {
                    self.data = resData
                    self.collectionView.reloadData()
                    self.collectionView.collectionViewLayout.invalidateLayout()
                    if #available(iOS 10.0, *) {
                        self.collectionView.refreshControl?.endRefreshing()
                    }
                    
                    if self.collectionView.alpha == 0 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.collectionView.alpha = 1
                            self.setTableViewMode()
                        }
                    }
                }
            }
        }
    }
    
    func getCommentsFrom(array: [[String:Any]]) -> [Comment] {
        var comments = [Comment]()
        for dictionary in array {
            guard (dictionary["id"] as? String) != nil else {
                continue;
            }
            
            let text = dictionary["text"] as? String ?? ""
            let authorName = dictionary["author_name"] as? String ?? ""
            
            let comment = Comment(text: text, author: authorName)
            comments.append(comment)
        }
        
        return comments
    }
    
    func addCommentDictionary(_ comment: [String:Any], index: Int) {
        if var comments = data[index]["comments"] as? [[String:Any]], comments.count != 0 {
            if comments.count >= 5 {
                comments.insert(comment, at: 5)
            } else {
                comments.append(comment)
            }
            
            data[index]["comments"] = comments
        } else {
            data[index]["comments"] = [comment]
        }
    }
    
    func hideGoUpButton() {
        if !self.scrollUpView.isHidden {
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollUpView.alpha = 0
            }, completion: { (completed:Bool) in
                if completed {
                    self.scrollUpView.isHidden = true
                }
            })
        }
    }
    
    func showGoUpButton() {
        if self.scrollUpView.isHidden {
            self.scrollUpView.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollUpView.alpha = 0.5
            })
        }
    }
    
    func loadUpdates() {
        
        if self.data.count + self.loadingSteps == self.loadingUntilCount {
            return
        }
        
        self.loadingUntilCount = self.data.count + self.loadingSteps
        
        DispatchQueue.global().async {
            let res = AdminAPI.get(fromUrl: "ardnd/post/\(self.data.count)/\(self.loadingUntilCount)?\(self.filters)")
            
            if let resData = res as? [[String:Any]] {
                DispatchQueue.main.async {
                    if resData.count > 0 {
                        var currentRows = self.data.count
                        
                        var indexPaths:[IndexPath] = []
                        
                        for _ in resData {
                            indexPaths.append(IndexPath(row:currentRows, section:0))
                            currentRows += 1;
                        }
                        
                        self.data.append(contentsOf: resData)
                        self.collectionView.insertItems(at: indexPaths)
                    } else {
                        self.loadingUntilCount = self.data.count
                    }
                }
            } else {
                self.loadingUntilCount = self.data.count
            }
        }
    }
    
    @objc  func setGridViewMode() {
        if viewMode != .gridViewMode {
            viewMode = .gridViewMode
             }
    } 
    @objc func setTableViewMode() {
        if viewMode != .tableViewMode {
            viewMode = .tableViewMode
        }
    }
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "hashtagCollectionViewHeader", for: indexPath) as! HashtagCollectionViewHeader
        
        header.gridViewButton.addTarget(self, action: #selector(setGridViewMode), for: .touchUpInside)
        header.tableViewButton.addTarget(self, action: #selector(setTableViewMode), for: .touchUpInside)
        
        if viewMode == .gridViewMode {
            header.gridViewButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
            header.tableViewButton.tintColor = UIColor.lightGray
        } else {
            header.gridViewButton.tintColor = UIColor.lightGray
            header.tableViewButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
        }
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch viewMode {
        case .gridViewMode:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "defaultCollectionViewCell", for: indexPath) as! DefaultProfileCollectionViewCell
            cell.tag = indexPath.row
            
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let image = self.imageMap[postImageString]
                    cell.imageView.image = image!
                }
//                    else if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
//                    imageMap[postImageString] = image
//                    cell.imageView?.image = image
//                }
//                else {
//                    let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
//                    imageDownloadOperation.completionHandler = { [weak self] (image) in
//                        guard let strongSelf = self else {
//                            return
//                        }
//
//                        DispatchQueue.main.async {
//                            cell.imageView?.image = image
//                        }
//
//                        strongSelf.imageMap[postImageString] = image
//                        strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
//                    }
//
//                    imageDownloadOperationQueue.addOperation(imageDownloadOperation)
//                    imageDownloadOperations[indexPath] = imageDownloadOperation
//                }
                else {
                    DispatchQueue.global().async {
                        let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
                        DispatchQueue.main.async {
                            if postImage != nil && cell.tag == indexPath.row {
                                self.imageMap[postImageString] = postImage
                                let image = self.imageMap[postImageString]

                                cell.imageView.image = image
                            }
                        }
                    }
                }
            }
            
            return cell
            
        case .tableViewMode:
            
            var cell: StartStreamCollectionViewCell
            
            if let offerId = data[indexPath.row]["entity_type"] as? String, offerId == "1" {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offerStartStreamCollectionViewCell", for: indexPath) as! StartStreamCollectionViewCell
            } else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "startStreamCollectionViewCell", for: indexPath) as! StartStreamCollectionViewCell
            }
            
            cell.tag = indexPath.item
            cell.delegate = self
            
            cell.immediateCommentTextField.delegate = self
            cell.immediateCommentTextField.returnKeyType = .send
            cell.immediateCommentTextField.tag = indexPath.item
            cell.immediateCommentImageView.image = LoggedUserCredentials.sharedInstance.userImage
            
            let headerText = self.data[indexPath.row]["user_name"] as? String ?? ""
            let postText = self.data[indexPath.row]["content"] as? String ?? ""
            let userId = data[indexPath.row]["user_id"] as? String ?? ""
            
            // handling user image
            
            if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
                if self.imageMap[userImageString] != nil {
                    cell.profileImageView.image = self.imageMap[userImageString]
                }
                else {
                    DispatchQueue.global().async {
                        var userImage:UIImage? = nil
                        
                        if(userImageString.hasPrefix("http")){
                            userImage = imageMethods.uiImage(withWebPath: userImageString)
                        }
                        else {
                            userImage = imageMethods.uiImage(withServerPathName: userImageString, width: 36)
                        }
                        
                        DispatchQueue.main.async {
                            if userImage != nil && cell.tag == indexPath.row {
                                self.imageMap[userImageString] = userImage
                                cell.profileImageView.image = userImage
                            }
                            else {
                                cell.profileImageView.image = UIImage(named: "emptyProfileImage")
                            }
                        }
                    }
                }
            }
            else {
                cell.profileImageView.image = UIImage(named: "emptyProfileImage")
            }
            
            if let allComments = data[indexPath.row]["comments"] as? [[String:Any]] {
                let comments = getCommentsFrom(array: allComments)
                cell.setComments(comments)
                cell.likeButton.setTitle(String(allComments.count), for: .normal)
            }
            
            cell.showAllCommentsButtonTapHandler = {
                let topComment = Comment(text: postText, author: headerText, authorImage: cell.profileImageView.image!)
                topComment.commentsId = self.data[indexPath.row]["id"] as! String
                self.currentIndexPath = indexPath
                self.getToCommentsViewController(topComment, focusOnComment: false)
            }
            
            cell.commentButtonTapHandler = {
                let topComment = Comment(text: postText, author: headerText, authorImage: cell.profileImageView.image!)
                topComment.commentsId = self.data[indexPath.row]["id"] as! String
                self.currentIndexPath = indexPath
                self.getToCommentsViewController(topComment, focusOnComment: true)
            }
            
            // handling post image
            
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let image = self.imageMap[postImageString]
                    cell.postImageView.image = image!
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
//                else if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
//                    imageMap[postImageString] = image
//                    cell.postImageView?.image = image
//                }
//                else {
//                    let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
//                    imageDownloadOperation.completionHandler = { [weak self] (image) in
//                        guard let strongSelf = self else {
//                            return
//                        }
//
//                        DispatchQueue.main.async {
//                            cell.postImageView?.image = image
//                        }
//
//                        strongSelf.imageMap[postImageString] = image
//                        strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
//                    }
//
//                    imageDownloadOperationQueue.addOperation(imageDownloadOperation)
//                    imageDownloadOperations[indexPath] = imageDownloadOperation
//                }
                else {
                    DispatchQueue.global().async {
                        let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
                        DispatchQueue.main.async {
                            if postImage != nil && cell.tag == indexPath.row {
                                self.imageMap[postImageString] = postImage
                                let image = self.imageMap[postImageString]

                                cell.postImageView.image = image
                                cell.setNeedsLayout()
                                cell.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
        
            cell.profileButtonTapHandler = {
                self.getProfileScreenHandler(indexPath: indexPath)
            }
            
            if LoggedUserCredentials.sharedInstance.userHasRights(userId: userId) {
                cell.optionsButton.isHidden = false
                
                cell.optionsButtonTapHandler = {
                    self.optionsButtonTapHandler(indexPath: indexPath)
                }
            } 
            
            cell.locationLabel.text = self.data[indexPath.row]["location_name"] as? String ?? ""
            cell.locationButtonTapHandler = {
                self.getLocationScreenHandler(indexPath: indexPath)
            }
        
            cell.offerButtonTapHandler = {
                self.offerButtonTapHandler(indexPath: indexPath)
            }
        
            // location bookmarking
            if self.data[indexPath.row]["isLiked"] as? String == "1" || self.data[indexPath.row]["isLiked"] as? Int == 1 {
                cell.bookmarkImageView.image = UIImage(named :"bookmark-full")
            } else {
                cell.bookmarkImageView.image = UIImage(named :"bookmark")
            }
            
            cell.bookmarkButtonTapHandler = {
                let placeVC = get(viewController: "LocationStreamViewController") as! LocationStreamViewController

                placeVC.place["name"] = self.data[indexPath.row]["location_name"] as? String ?? ""
                if let meta = self.data[indexPath.row]["meta"] as? [String:Any] {
                    if let placeId = meta["gmsPlaceId"] as? String {
                        placeVC.placeId = placeId
                    }
                    if let city = meta["city"] as? String {
                        placeVC.place["city"] = city
                    }
                }

                self.showLoading()

                if self.data[indexPath.row]["isLiked"] as? String == "1" || self.data[indexPath.row]["isLiked"] as? Int == 1 {
                    placeVC.unfollowLocation(whenDone:{
                        self.data[indexPath.row]["isLiked"] = 0
                        cell.bookmarkImageView.image = UIImage(named: "bookmark")
                        
                        self.notifyingManager.showWithText("Removed from your search list", view: self.view)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                            self.notifyingManager.removeLabel()
                        }
                        
                        self.endLoading()
                    })
                }
                else {
                    placeVC.followLocation(whenDone:{
                        self.data[indexPath.row]["isLiked"] = 1
                        cell.bookmarkImageView.image = UIImage(named: "bookmark-full")
                        
                        self.notifyingManager.showWithText("Saved in your search list", view: self.view)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                            self.notifyingManager.removeLabel()
                        }

                        self.endLoading()
                    })
                }
            }
            
            cell.headerLabel.text = headerText
            cell.postText.text = postText
            cell.postText.numberOfLines = 0
            cell.postText.lineBreakMode = .byWordWrapping
            cell.postText.font = UIFont.systemFont(ofSize: 17)
            
            if cell.postText.text != "" {
                cell.postText.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 13
                cell.postText.sizeToFit()
                cell.postLabelHeight.constant = cell.postText.height
            } else {
                cell.postLabelHeight.constant = 0
            }
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            if cell.hashtagsTextView.text != "" {
                cell.hashtagsTextView?.width = collectionView.width - 5
                cell.hashtagsTextView?.sizeToFit()
                cell.hashtagsTextViewHeight?.constant = (cell.hashtagsTextView?.height)!
            } else {
                cell.hashtagsTextViewHeight?.constant = 0
            }
            
            var likesCount = data[indexPath.row]["likes_number"] as? String ?? "0"
            cell.likeButton.setTitle(likesCount, for: .normal)
            
            if let likedPost = data[indexPath.row]["liked_by_me"] as? String {
                if likedPost == "1" {
                    cell.liked = true
                } else {
                    cell.liked = false
                }
            } else {
                cell.liked = false
            }
            
            cell.likeButtonTapHandler = {
                var likeCount = Int(likesCount)!
                if !cell.liked == true {
                    likeCount += 1
                    self.data[indexPath.row]["liked_by_me"] = "1"
                } else {
                    if likeCount != 0 {
                        likeCount -= 1
                    }
                    self.data[indexPath.row]["liked_by_me"] = "0"
                }
                
                likesCount = String(likeCount)
                self.data[indexPath.row]["likes_number"] = likeCount
                cell.likeButton.setTitle(likesCount, for: .normal)
                
                cell.liked = !cell.liked
            
                if let postId = self.data[indexPath.row]["id"] as? String {
                    let post = ["id":postId]
                    _ = AdminAPI.post(url: "ardnd/post/toggle-like/\(postId)", post: post)
                }
            }
            
            cell.shareButtonTapHandler = {
                let text = cell.postText.text ?? ""
                let image = cell.postImageView.image ?? UIImage()
                self.shareButtonHandler(text: text, image: image)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.data.count > 4 && indexPath.row >= self.data.count - 4 {
            self.loadUpdates()
        }
        if indexPath.row == 0 {
            self.hideGoUpButton()
        }
        if indexPath.row > 5 {
            self.showGoUpButton()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if #available(iOS 11.0, *) {
            if (navigationItem.searchController?.isActive) ?? false {
                navigationItem.searchController?.isActive = false
            }
        }
        
        let hashtagViewController = get(viewController: "HashtagCollectionViewController") as! HashtagCollectionViewController
        
        hashtagViewController.title = "Details"
        hashtagViewController.data = [data[indexPath.row]]
        hashtagViewController.shouldShowHeader = false
        
        navigationController?.pushViewController(hashtagViewController, animated: true)
    }
    
    // MARK: handlers
    
    func getToCommentsViewController(_ post: Comment, focusOnComment: Bool) {
        let res = AdminAPI.get(fromUrl: "ardnd/post/comments/get/\(post.commentsId)?for=post&withSubComments=1")
        
        runOnMainQueueAvoidDeadlock {
            var comments:[Comment] = []
            
            if let resData = res as? [[String:Any]] {
                for item in resData {
                    guard let commentId = item["id"] as? String else {
                        continue;
                    }
                    
                    let comment = Comment(text: item["text"] as! String, author: "")
                    comment.commentID = commentId
                    
                    if let profileImageString = item["profile_image"] as? String {
                        comment.authorImageUrl = profileImageString
                    }
                    
                    if let authorId = item["author_id"] as? String {
                        comment.profileId = authorId
                    }
                    
                    if let author = item["author_name"] as? String {
                        comment.author = author
                    }
                    
                    if let discussion = item["comments"] as? [[String:Any]], discussion.count != 0 {
                        for replyComment in discussion {
                            guard let replyId = replyComment["id"] as? String else {
                                continue;
                            }
                            
                            let discussionComment = Comment(text: replyComment["text"] as! String, author: "")
                            discussionComment.isReply = true
                            
                            if let authorId = replyComment["author_id"] as? String {
                                discussionComment.profileId = authorId
                            }
                            
                            if let author = replyComment["author_name"] as? String {
                                discussionComment.author = author
                            }
                            
                            discussionComment.commentID = replyId
                            if let profileImageString = replyComment["profile_image"] as? String {
                                discussionComment.authorImageUrl = profileImageString
                            }
                            
                            comment.discussion.append(discussionComment)
                        }
                    }
                    
                    comments.append(comment)
                }
            }
            
            let vc = get(viewController: "CommentsViewController") as! CommentsViewController
            
            vc.title = "Comments"
            vc.wantsToAddComment = focusOnComment
            vc.comments = comments
            vc.postInfo = post
            vc.delegate = self
            
            vc.profileImage = LoggedUserCredentials.sharedInstance.userImage
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getLocationScreenHandler(indexPath: IndexPath) {
        let title = self.data[indexPath.row]["location_name"] as? String ?? ""
        var placeId = ""
        if let meta = self.data[indexPath.row]["meta"] as? [String:Any] {
            if let metaPlaceId = meta["gmsPlaceId"] as? String {
                placeId = metaPlaceId
            }
        }
        
        var presentingController: UIViewController
        if navigationController != nil {
            presentingController = navigationController!
        } else {
            presentingController = self
        }
        
        NavigationManager.openLocationScreen(presentingController, locationName: title, placeId: placeId)
    }
    
    func getProfileScreenHandler(indexPath: IndexPath) {
        let presentingController: UIViewController
        if self.navigationController != nil {
            presentingController = self.navigationController!
        } else {
            presentingController = self
        }
        
        let title = self.data[indexPath.row]["user_name"] as? String ?? ""
        let userId = self.data[indexPath.row]["user_id"] as? String ?? ""
        
        var userImage = UIImage(named: "emptyProfileImage")
        if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
            if self.imageMap[userImageString] != nil {
                userImage = self.imageMap[userImageString]!
            }
        }
        
        NavigationManager.openProfileScreen(presentingController, title: title, userId: userId, imageMap: self.imageMap, userImage: userImage!)
    }
    
    func optionsButtonTapHandler(indexPath: IndexPath) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let editAction = UIAlertAction(title: "Edit post", style: .default, handler: { (action) in
            self.editPost(indexPath: indexPath)
        })
        let deleteAction = UIAlertAction(title: "Delete post", style: .destructive, handler: { (action) in
            self.deletePost(indexPath: indexPath)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            self.navigationController?.dismiss(animated: true, completion: {
                
            })
        })
        
        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.navigationController?.present(alertController, animated: true, completion: {
            
        })
    }
    
    func offerButtonTapHandler(indexPath: IndexPath) {
        let ovc = get(viewController: "OfferViewController") as! OfferViewController
        
        let offerId = self.data[indexPath.row]["entity_id"] as? String ?? "0"
        ovc.offerID = UInt(offerId)!
        ovc.title = self.data[indexPath.row]["offer_name"] as? String ?? ""
        
        self.navigationController?.pushViewController(ovc, animated: true)
    }
    
    func shareButtonHandler(text: String, image: UIImage) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = text
        
        let image = image
        let instagramURL = NSURL(string: "instagram://app")
        if (UIApplication.shared.canOpenURL(instagramURL! as URL)) {
            let writePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("sharingImage.ig")
            let fileURL = NSURL(fileURLWithPath: writePath) as URL
            if let testImage = UIImageJPEGRepresentation(image, 1.0) {
                try? FileManager.default.removeItem(atPath: writePath)
                try? testImage.write(to: fileURL, options: .atomicWrite)
            }
            
            self.documentViewController.url = fileURL
            self.documentViewController.uti = "com.instagram.photo"
            
            self.documentViewController.delegate = self
            
            DispatchQueue.main.async {
                self.notifyingManager.showWithText("Text was copyed to clipboard so you can paste it to sharing message.", view: self.view)
                
                self.documentViewController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
            }
        }
    }
    
    // MARK: document interaction controller delegate
    
    func documentInteractionControllerDidDismissOpenInMenu(_ controller: UIDocumentInteractionController) {
        notifyingManager.removeLabel()
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        notifyingManager.removeLabel()
    }
    
    func documentInteractionControllerDidDismissOptionsMenu(_ controller: UIDocumentInteractionController) {
        notifyingManager.removeLabel()
    }
    
    // MARK: post actions
    
    func editPost(indexPath: IndexPath) {
        let createController = get(viewController: "CreatePostViewController") as! CreatePostViewController
        createController.mode = .edit
        createController.post = ["id": data[indexPath.row]["id"]!, "location_name":data[indexPath.row]["location_name"] ?? "", "meta":data[indexPath.row]["meta"] ?? ""]
        
        weak var welf = self;
        createController.createdPost = {(post:[String:Any]) in
            welf?.loadData()
        }
        
        if !(self.data[indexPath.row]["drink_hashtags"] as? String ?? "").isEmpty {
            let drinkTags = self.data[indexPath.row]["drink_hashtags"] as! String
            createController.foodPlaceholderIsShown = false
            createController.postType = .drinks
            
            if !(self.data[indexPath.row]["food_hashtags"] as? String ?? "").isEmpty {
                let foodTags = self.data[indexPath.row]["food_hashtags"] as! String
                createController.drinkPlaceholderIsShown = false
                createController.postType = .foodAndDrinks
                createController.hashTags = foodTags
                createController.drinkTags = drinkTags
            }
            else {
                createController.hashTags = drinkTags
            }
        } else {
            if !(self.data[indexPath.row]["food_hashtags"] as? String ?? "").isEmpty {
                let foodTags = self.data[indexPath.row]["food_hashtags"] as! String
                createController.foodPlaceholderIsShown = false
                createController.postType = .food
                createController.hashTags = foodTags
            }
        }
        
        DispatchQueue.global().async {
            if let image = imageMethods.uiImage(withServerPathName: self.data[indexPath.row]["image"] as? String, width: 0) {
                createController.image = image
            }
        }
        
        if let postDescription = self.data[indexPath.row]["content"] as? String {
            createController.placeholderIsShown = false
            createController.postDescription = postDescription
        }
        
        createController.locationString = self.data[indexPath.row]["location_name"] as? String ?? ""
        
        let navigationController = UINavigationController(rootViewController: createController)
        
        self.present(navigationController, animated: true) {
            
        }
    }
    
    func deletePost(indexPath: IndexPath) {
        DispatchQueue.global().async {
            var modString = ""
            
            if LoggedUserCredentials.sharedInstance.isModerator() {
                modString = "?isMod=1"
            }
            
            let response = AdminAPI.delete(url: "ardnd/post/delete/\(self.data[indexPath.row]["id"] as? String ?? "0")\(modString)")
            
            DispatchQueue.main.async {
                if let res = response as? [String:Any] {
                    if res["success"] as? Int == 1 || res["success"] as? String == "1" {
                        self.endLoading()
                        
                        self.data.remove(at: indexPath.row)
                        self.collectionView.deleteItems(at: [indexPath])
                    }
                    else {
                        self.endLoading()
                    }
                }
            }
        }
    }
    
    // MARK: hashtags
    
    func createHashtagsText(foodString: String, drinksString: String) -> NSAttributedString {
        var foodAttributedString = NSMutableAttributedString(string: foodString)
        if foodString.count != 0 {
            foodAttributedString = self.createHashtagLink(hashtags: foodString) as! NSMutableAttributedString
            foodAttributedString.insert(NSAttributedString(string: "Food tags: ", attributes: [NSAttributedStringKey.font : UIFont.init(name: "Helvetica", size: 17) as Any]), at: 0)
        }
        
        var drinksAttributedString = NSMutableAttributedString(string: drinksString)
        if drinksString.count != 0 {
            drinksAttributedString = self.createHashtagLink(hashtags: drinksString) as! NSMutableAttributedString
            drinksAttributedString.insert(NSAttributedString(string:"Drink tags: ", attributes: [NSAttributedStringKey.font : UIFont.init(name: "Helvetica", size: 17) as Any]), at: 0)
        }
        
        foodAttributedString.append(drinksAttributedString)
        return foodAttributedString
    }
    
    func createHashtagLink(hashtags: String) -> NSAttributedString{
        let hashtagsArray = hashtags.components(separatedBy: " ")
        let attributedHashtags = NSMutableAttributedString.init()
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedString.Key(rawValue: NSAttributedStringKey.font.rawValue) : UIFont(name: "Helvetica", size: 16)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 0.05, green: 0.4, blue: 0.65, alpha: 1.0)]
        
        for item in hashtagsArray {
            let attributedItem = NSAttributedString(string: item, attributes: attributes)
            attributedHashtags.append(attributedItem)
            attributedHashtags.append(NSAttributedString.init(string: " "))
        }
        
        return attributedHashtags
    }
    
    func getHashtagSection(hashtag: String, tag: NSInteger, index: NSInteger) -> String {
        switch index {
        case 0:
            return ""
        case 1:
            let foodHashtags = self.data[tag]["food_hashtags"] as! String
            
            if foodHashtags.contains(hashtag) {
                return "food_hashtags"
            }
            
            return "drink_hashtags"
        case 2:
            return "food_hashtags"
        case 3:
            return "drink_hashtags"
        default:
            break;
        }
        
        return ""
    }
    
    // MARK: ProfileCollectionViewLayoutDelegate
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        var imageSize = CGSize.zero
        if let imageName = data[indexPath.item]["image"] as? String {
            if let image = imageMap[imageName] {
                imageSize = image.size
                
                let aspect = imageSize.width / imageSize.height
                return collectionView.width / aspect
            }
        }
        
        if imageSize == CGSize.zero {
            if let imageSizeString = data[indexPath.item]["image_size"] as? String {
                let size = imageSizeString.split(separator: "x")
                
                if let width = (size.first as NSString?)?.floatValue {
                    imageSize.width = CGFloat(width)
                } else {
                    return 400
                }
                
                if let height = (size.last as NSString?)?.floatValue {
                    imageSize.height = CGFloat(height)
                } else {
                    return 400
                }
            } else {
                return 400
            }
        }
        
        let aspect = imageSize.width / imageSize.height
        return collectionView.width / aspect
    }
    
    func collectionView(_ collectionView: UICollectionView, heightForTextsAtIndexPath indexPath:IndexPath) -> CGFloat {
        var offerHeight: CGFloat = 0
        if let offerId = data[indexPath.row]["entity_type"] as? String, offerId == "1" {
            offerHeight = 45
        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 13, height: 0))
        label.text = data[indexPath.item]["content"] as? String ?? ""
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 17)
        
        if label.text != "" {
            label.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 13
            label.sizeToFit()
        } else {
            label.height = 0
        }
        
        let hashtagsLabel = UITextView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        let foodHashtags = self.data[indexPath.item]["food_hashtags"] as? String ?? ""
        let drinkHashtags = self.data[indexPath.item]["drink_hashtags"] as? String ?? ""
        hashtagsLabel.attributedText = createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
        
        if hashtagsLabel.text != "" {
            hashtagsLabel.width = collectionView.width - 5
            hashtagsLabel.sizeToFit()
        } else {
            hashtagsLabel.height = 0
        }
        
        var height: CGFloat = 0
        guard let comments = data[indexPath.row]["comments"] as? [[String:Any]], comments.count != 0 else {
            return label.height + hashtagsLabel.height + offerHeight
        }
        
        for comment in comments {
            let label = UILabel(frame: CGRect(x: inset, y: height + inset, width: UIScreen.main.bounds.width, height: 0))
            let commentString = NSMutableAttributedString(string: comment["author_name"] as? String ?? "" + " ", attributes: nameFontAttribute)
            let commentText = NSMutableAttributedString(string: comment["text"] as? String ?? "", attributes: commentFontAttribute)
            commentString.append(commentText)
            label.attributedText = commentString
            
            label.sizeToFit()
            height += label.height
        }
        
        return label.height + hashtagsLabel.height + height + offerHeight
    }
    
    // MARK: HashtagsDelegate
    
    func userTappedAtHashtag(hashtag: String, tag: NSInteger, index: NSInteger) {
        let hashtagSection = self.getHashtagSection(hashtag:hashtag, tag: tag, index: index)
        
        var presentingViewController: UIViewController
        if self.navigationController != nil {
            presentingViewController = self.navigationController!
        } else {
            presentingViewController = self
        }
        
        NavigationManager.openHashtagScreen(presentingViewController, hashtag: hashtag, hashtagSection: hashtagSection)
    }
    
    // MARK: GSMAutocomplele delegate
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true) {
            self.locationButton?.setTitle(place.name, for: .normal)
            self.locationButton?.sizeToFit()
            self.locationName = place.name
            var meta:[String:Any] = ["gmsPlaceId":place.placeID]
            
            if place.addressComponents != nil {
                var gmsAdressParts:[String:Any] = [:]
                for component:GMSAddressComponent in place.addressComponents! {
                    if component.type == kGMSPlaceTypeLocality {
                        meta["city"] = component.name
                    }
                    gmsAdressParts[component.type] = component.name
                }
                meta["gmsAdressParts"] = gmsAdressParts
            }
            
            self.placeMeta = meta
            
            let locationName = place.name
            let locationHashtags = "location=\(locationName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            let foodHashtagsSearch = "&tags=\(self.hashtagsString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            self.filters = locationHashtags + foodHashtagsSearch
            self.loadData()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true) {
            
        }
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true) {
            self.placeMeta = [String:Any]()
            self.locationName = ""
            self.locationButton?.setTitle(self.locationName, for: .normal)
            self.locationButton?.sizeToFit()
            self.searchForFilters()
        }
    }
    
    // MARK: textfield delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == foodTagsTextField {
            setGridViewMode()
            shouldShowSupplementary = true
            return true
        } else if LoggedUserCredentials.sharedInstance.hasLoggedUser() == false {
            CWAlert.showNotification(withTitle: "Error".localized, message: "To do this you should log in first.".localized, onViewController: self)
            return false
        }

        var textFieldCoordinates = textField.convert(textField.frame.origin, to: collectionView)
        textFieldCoordinates.y += 50
        scrollingPoint = textFieldCoordinates
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField == foodTagsTextField {
            self.hashtagsString = textField.text!
            self.searchForFilters()
        } else {
            guard textField.text != "" else {
                return true
            }
            
            let index = textField.tag
            guard let postId = data[index]["id"] else {
                textField.text = ""
                return true
            }
            
            let userCredentials = LoggedUserCredentials.sharedInstance
            var newComment: [String:Any] = [:]
            newComment["text"] = textField.text
            newComment["for"] = "post"
            newComment["id"] = postId
            
            DispatchQueue.global().async {
                _ = AdminAPI.saveComment(url: "ardnd/post/comments/save", post: newComment)
            }
            
            var localComment = newComment
            localComment["author_name"] = userCredentials.userName
            addCommentDictionary(localComment, index: index)
            
            collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
            collectionView.collectionViewLayout.invalidateLayout()
        }
        
        textField.text = ""
        return true
    }
    
    // MARK: search controller delegate
    func willPresentSearchController(_ searchController: UISearchController) {
        setGridViewMode()
        shouldShowSupplementary = true
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        if #available(iOS 11.0, *) {
            navigationItem.searchController?.searchBar.text = self.hashtagsString
        }
    }
    
    // MARK: search bar delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let hashtags = searchBar.text {
            self.hashtagsString = hashtags
            self.searchForFilters()
        }
        else {
            self.hashtagsString = ""
            self.searchForFilters()
        }
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController?.isActive = false
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.hashtagsString = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.hashtagsString = ""
        self.searchForFilters()
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if #available(iOS 11.0, *) {
            if navigationItem.searchController != nil && (navigationItem.searchController?.isActive)! {
                navigationItem.searchController?.isActive = false
            }
        }
    }
    
    // MARK: comments changed delegate
    
    func didAddComment(_ comment: [String : Any], postId: String) {
        guard currentIndexPath != nil else {
            return
        }
        
        addCommentDictionary(comment, index: currentIndexPath!.row)
        collectionView.reloadItems(at: [currentIndexPath!])
    }
    
    func didDeleteComment(postId: String) {
        guard currentIndexPath != nil else {
            return
        }
        
        var deletedIndex = 0
        if var comments = data[currentIndexPath!.row]["comments"] as? [[String:Any]], comments.count != 0 {
            for (index, comment) in comments.enumerated() {
                if comment["id"] as? String == postId {
                    deletedIndex = index
                }
            }
            
            comments.remove(at: deletedIndex)
            data[currentIndexPath!.row]["comments"] = comments
        }
        
        collectionView.reloadItems(at: [currentIndexPath!])
    }
    
    // MARK: collection view prefetching
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if imageDownloadOperations[indexPath] != nil {
                return
            }
            
            guard let urlString = self.data[indexPath.row]["image"] as? String else {
                return
            }
            
            let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: urlString)
            imageDownloadOperationQueue.addOperation(imageDownloadOperation)
            imageDownloadOperations[indexPath] = imageDownloadOperation
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            guard let imageDownloadOperation = imageDownloadOperations[indexPath] else {
                return
            }
            
            imageDownloadOperation.cancel()
            imageDownloadOperations.removeValue(forKey: indexPath)
        }
    }
    @objc
    // MARK: keyboard notifications
    func handleKeyboardShow(_ notification: NSNotification) {
        guard self.scrollingPoint != CGPoint.zero else {
            return
        }
        
        let keyboardFrame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect
        let keyboardPoint = self.view.convert(keyboardFrame.origin, to: collectionView)
        let y = collectionView.contentOffset.y + self.scrollingPoint.y - keyboardPoint.y
        
        let scrollingPoint = CGPoint(x: 0, y: y)

        collectionView.contentOffset = scrollingPoint
        self.scrollingPoint = CGPoint.zero
    }
}
