//
//  IF3UserSettingsManager.swift
//  if_framework
//
//  Created by Christopher on 10/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class IF3UserSettingsManager {
    static let shared = IF3UserSettingsManager()
    
    let dynamicSettingsStorage = localStorage(filename: "settings")
    
    let oldSettingsDict:[[String:Any]]
    
    init() {
        self.oldSettingsDict = dynamicSettingsStorage?.object(forKey: "dynamicUserSettingsValues") as! [[String : Any]]
    }
}
