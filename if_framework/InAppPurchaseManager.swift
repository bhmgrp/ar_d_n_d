//
//  InAppPurchaseManager.swift
//  if_framework
//
//  Created by Christopher on 10/26/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation
import StoreKit

class InAppPurchaseManager:UIAlertController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    var productIdentifer:String = ""
    var purchased:()->Void = {}
    var purchaseFailed:()->Void = {}
    
    func unlockRequest(withProductIdentifer:String?,purchased:(()->Void)?,purchaseFailed:(()->Void)?){
        if withProductIdentifer != nil {
            self.productIdentifer = withProductIdentifer!
        }
        
        if purchased != nil {
            self.purchased = purchased!
        }
        
        if purchased != nil {
            self.purchaseFailed = purchased!
        }
        
        self.unlockRequest()
    }
    
    func success(){
        self.purchased()
        self.dismiss(animated: true) {
        }
    }
    
    func failed(){
        self.purchaseFailed()
        self.dismiss(animated: true) {
        }
    }
    
    func unlockRequest(){
        SKPaymentQueue.default().add(self)
        if (SKPaymentQueue.canMakePayments())
        {
            let productIDs:Set<String> = [self.productIdentifer]
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productIDs)
            productsRequest.delegate = self
            productsRequest.start()
            NSLog(" IAP-Manager | Fetching Products")
        }else{
            NSLog(" IAP-Manager | can not make purchases")
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        NSLog(" IAP-Manager | got the request from Apple")
        if (response.products.count>0) {
            //var validProducts = response.products
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == self.productIdentifer) {
                NSLog(" IAP-Manager | \(validProduct.localizedTitle)")
                NSLog(" IAP-Manager | \(validProduct.localizedDescription)")
                NSLog(" IAP-Manager | \(validProduct.price)")
                buy(product:validProduct);
            } else {
                NSLog(" IAP-Manager | wrong product found:\(validProduct.productIdentifier)")
            }
        } else {
            NSLog(" IAP-Manager | nothing")
            self.failed()
        }
    }
    
    func buy(product: SKProduct){
        NSLog(" IAP-Manager | Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        self.failed()
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        NSLog(" IAP-Manager | updating transactions")
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                NSLog(" IAP-Manager | purchased")
                SKPaymentQueue.default().finishTransaction(transaction)
                self.success()
                break
            case .failed:
                NSLog(" IAP-Manager | failed")
                SKPaymentQueue.default().finishTransaction(transaction)
                self.failed()
                break
            case .restored:
                NSLog(" IAP-Manager | restored")
                SKPaymentQueue.default().finishTransaction(transaction)
                self.success()
                break
            case .deferred:
                NSLog(" IAP-Manager | deffered")
                break
            case .purchasing:
                NSLog(" IAP-Manager | purchasing")
                break;
            }
        }
    }
}
