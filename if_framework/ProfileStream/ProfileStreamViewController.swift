//
//  ProfileStreamViewController.swift
//  if_framework
//
//  Created by Christopher on 4/3/17.
//  Copyright © 2017 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class ProfileStreamViewController: StartStreamViewController {
    
    @IBOutlet var profileInfoView: UIView!
    
    @IBOutlet weak var profileInfoImage: UIImageView!
    
    @IBOutlet weak var profileInfoLabel1: UILabel!
    @IBOutlet weak var profileInfoLabel2: UILabel!
    
    @IBOutlet weak var profileInfoButton: UIButton!
    
    @IBOutlet weak var profileInfoOffersView: UIView!

    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
@objc    var userImage:UIImage? = nil
@objc    var profileId:String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileInfoView.height = 115
        self.profileInfoView.width = self.view.width
        
        self.tableView.tableHeaderView = self.profileInfoView
        self.tableView.tableHeaderView?.addSubview(self.refreshControl)
        self.tableView.contentInset = .zero
        self.scrollView.delegate = self
        self.loadProfileInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let deadline = DispatchTime.now() + .seconds(3)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.tableView.backgroundColor = UIColor.red
            self.tableViewHeight.constant = self.tableView.contentSize.height
            
            let width = self.tableView.size.width
            self.contentView.size = CGSize.init(width: width, height: self.tableView.contentSize.height + 150)
            self.scrollView.contentSize = CGSize.init(width: width, height: self.tableView.contentSize.height + 150)
            
            self.view.setNeedsDisplay()
            self.view.setNeedsLayout()
        }
    }
    
    override func searchForFilters() {
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func initNavigationItems() {
        return
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 60
        
        if self.data[indexPath.row]["tmp_height"] != nil && self.data[indexPath.row]["tmp_height"] is CGFloat {
            return self.data[indexPath.row]["tmp_height"] as! CGFloat
        }
        else {
            // safety
            print("", separator:"", terminator:"")
        }
        
        if self.data[indexPath.row]["image"] == nil || !(self.data[indexPath.row]["image"] is String) || (self.data[indexPath.row]["image"] is String && self.data[indexPath.row]["image"] as? String == "") {
            let cell:AdminTableViewCell = tableView.dequeueReusableCell(withIdentifier: "textCell") as? AdminTableViewCell ?? AdminTableViewCell()
            cell.label2.text = self.data[indexPath.row]["content"] as? String ?? ""
            cell.label2.width = UIScreen.main.bounds.size.width - 16
            cell.label2.sizeToFit()
            
            height = 4 + 20 + 8 + cell.label2.height + 8;
        }
        else {
            let cell:AdminTableViewCell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as? AdminTableViewCell ?? AdminTableViewCell()
            cell.label2.text = self.data[indexPath.row]["content"] as? String ?? ""
            cell.label2.width = UIScreen.main.bounds.size.width - 16
            cell.label2.sizeToFit()
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = self.createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            cell.hashtagsTextView?.width = UIScreen.main.bounds.size.width - 5
            cell.hashtagsTextView?.sizeToFit()
            
            let cellImage:UIImage? = self.imageMap[self.data[indexPath.row]["image"] as? String ?? "nil"]
            
            var imageHeight:CGFloat = 300
            
            if cellImage != nil {
                let aspect = cellImage!.size.height / cellImage!.size.width
                
                imageHeight = (UIScreen.main.bounds.width) * aspect
                
                if  self.data[indexPath.row]["_tmp_identifier"] as? String == "imageOfferCell" {
                    imageHeight += 45 + 10
                }
            }
            else {
                return 4 + 20 + 4 + imageHeight + 8 + cell.label2!.height + 24 + cell.hashtagsTextView!.height
            }
            
            height = 4 + 20 + 4 + imageHeight + 8 + cell.label2!.height + 24 + cell.hashtagsTextView!.height
        }
        
        self.data[indexPath.row]["tmp_height"] = height
        
        return height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func loadProfileInfo() {
        self.profileInfoImage.image = self.userImage
        
        DispatchQueue.global().async {
            if let data = AdminAPI.get(fromUrl: "ardnd/user/profile-info/\(self.profileId)") as? [String:Any] {
                DispatchQueue.main.async {
                    self.profileInfoLabel1.text = data["posts_count"] as? String ?? ""
                    self.profileInfoLabel2.text = data["offers_count"] as? String ?? ""
                    
                    if (Int(data["offers_count"] as? String ?? "0") ?? 0 > 0) {
                        self.profileInfoButton.setTitle("View offers".localized, for: .normal)
                        self.profileInfoButton.addTarget(self, action: #selector(self.openOffers), for: .touchUpInside)
                        self.profileInfoOffersView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openOffers)))
                    }
                    else {
                        self.profileInfoButton.setTitle("No offers".localized, for: .normal)
                    }
                }
            }
        }
    }
    
    @objc func openOffers() {
        let vc = get(viewController: "OfferViewController") as! OfferViewController
        
        vc.userID = UInt(self.profileId) ?? 0
        vc.title = self.title
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
