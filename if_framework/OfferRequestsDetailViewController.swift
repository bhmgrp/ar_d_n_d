//
//  OfferRequestsDetailViewController.swift
//  if_framework
//
//  Created by Christopher on 10/12/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class OfferRequestsDetailViewController: AdminViewController {

    @IBOutlet weak var labelDear: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var requestTextView: UITextView!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var buttonDecline: UIButton!
    @IBOutlet weak var buttonProfile: UIButton!
    
    var offer:[String:Any] = [:]
    var request:[String:Any] = [:]
    var completed:()->Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTexts()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        self.initStyles()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initTexts(){
        title = offer["name"] as! String?
        
        let dynamicSettings = localStorage(filename: "dynamicUserSettingsValues")
        
        let firstName = dynamicSettings?.object(forKey: "firstName") as? String
        let lastName = dynamicSettings?.object(forKey: "lastName") as? String
        
        var userName = ""
        
        if(firstName != nil && !(firstName?.isEmpty)! && lastName != nil && !(lastName?.isEmpty)!){
            userName = firstName! + " " + lastName!;
        }
        else if(lastName != nil && !(lastName?.isEmpty)!){
            userName = lastName!;
        }
        else if(firstName != nil && !(firstName?.isEmpty)!){
            userName = firstName!;
        }
        
        labelDear.text = String(format:"Dear %@,".localized, userName)
        
        labelDescription.text = String(format:"You got a request for your offer \"%@\".".localized,offer["name"] as! String) + "\n\(String(format:"%@ has written the following".localized, request["requester_name"] as? String ?? "")):"
        
        requestTextView.text = request["text"] as? String ?? ""
        requestTextView.font = UIFont(name: "HelveticaNeue-Italic", size: 16)
        requestTextView.isEditable = false
        requestTextView.isSelectable = false
        
        
        buttonAccept.setTitle("Accept".localized, for: .normal)
        buttonDecline.setTitle("Decline".localized, for: .normal)
        buttonProfile.setTitle("Show profile".localized, for: .normal)
    }

    func initStyles(){
        buttonDecline.layer.cornerRadius = buttonDecline.height / 2
        buttonDecline.layer.borderWidth = 1
        buttonDecline.layer.borderColor = UIColor.red.cgColor
        buttonDecline.tintColor = .red
        buttonDecline.backgroundColor = .white
    
        buttonAccept.layer.cornerRadius = buttonAccept.height / 2
        buttonAccept.layer.borderWidth = 1
        buttonAccept.layer.borderColor = UIColor.green.cgColor
        buttonAccept.tintColor = .green
        buttonAccept.backgroundColor = .white
        
        buttonProfile.layer.cornerRadius = buttonProfile.height / 2
        buttonProfile.layer.borderWidth = 1
        buttonProfile.layer.borderColor = UIColor.black.cgColor
        buttonProfile.tintColor = .black
        buttonProfile.backgroundColor = .white
    }
    
    @IBAction func declinePressed(_ sender: UIButton) {
        self.showLoading()
        DispatchQueue.global().async {
            
            let response = API.post(toURL: SECURED_API_URL + "offer/my-requests", withPost: [
                "update":true,
                "offer_id":self.request["offer_id"]!,
                "user_id":UserDefaults.standard.integer(forKey: "loggedInUserID"),
                "request_id":self.request["id"]!,
                "set":["status":2]
            ]) as? [String:Any]
            
            DispatchQueue.main.async {
                if(response?["success"] != nil || response?["success"] as! Int == 1){
                    self.endLoading()
                    self.completed()
                }
                else {
                    self.endLoading()
                }
            }
        }
    }
    
    @IBAction func acceptPressed(_ sender: UIButton) {
        self.showLoading()
        DispatchQueue.global().async {
            
            let response = API.post(toURL: SECURED_API_URL + "offer/my-requests", withPost: [
                "update":true,
                "offer_id":self.request["offer_id"]!,
                "user_id":UserDefaults.standard.integer(forKey: "loggedInUserID"),
                "request_id":self.request["id"]!,
                "set":["status":1]
                ]) as? [String:Any]
            
            DispatchQueue.main.async {
                switch response?["success"]{
                case 1 as Double:
                    self.endLoading()
                    self.completed()
                    break
                case "1" as String:
                    self.endLoading()
                    self.completed()
                    break
                case true as Bool:
                    self.endLoading()
                    self.completed()
                    break
                default:
                    self.endLoading()
                }
            }
        }
    }
    
    @IBAction func showProfilePressed(_ sender: UIButton) {
        let profileId:Int = Int("\(self.request["user_id"] as? String ?? "")")!
        
        let vc = get(viewController: "ProfileDetailViewController") as! ProfileDetailViewController
        
        vc.profileId = profileId

        self.navigationController?.pushViewController(vc, animated: true)
        
//        
//        vc.view.height = UIScreen.main.bounds.size.height
//        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        
//        alert.view.height = UIScreen.main.bounds.size.height * 0.8
//        alert.setValue(vc, forKey: "contentViewController")
//        
//        if let popPresentationController : UIPopoverPresentationController = alert.popoverPresentationController {
//            popPresentationController.sourceView = sender
//            popPresentationController.sourceRect = CGRect(x: sender.width/2, y: sender.height/2, width: 1, height: 1)
//        }
//        
//        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
//        
//        self.present(alert, animated: true) {
//            
//        }
    }

}
