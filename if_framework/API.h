//
//  API.h
//  if_framework
//
//  Created by Julian Böhnke on 16.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface API : NSObject

@property NSString* status;
@property NSInteger code;

- (id) request: (NSString*)url : (NSDictionary*)params;

+ (id)postToURL:(NSString*)urlString WithPost:(NSDictionary*)postDict;

+ (id)getFrom:(NSString*)urlString;

+ (id)jsonObject:(NSData*)data;

@end
