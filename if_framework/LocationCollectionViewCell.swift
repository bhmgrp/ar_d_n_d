//
//  LocationCollectionViewCell.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/25/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class LocationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var topLeftImageView:        UIImageView!
    @IBOutlet weak var headerLabel:             UILabel!
    @IBOutlet weak var mainImageView:           UIImageView!
    @IBOutlet weak var postTextLabel:           UILabel!
    @IBOutlet weak var postTextLabelHeight:     NSLayoutConstraint!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var hashtagsTextView:        UITextView!
    @IBOutlet weak var hashtagsTextViewHeight:  NSLayoutConstraint!
    
    @IBOutlet weak var commentProfileImageView: UIImageView!
    @IBOutlet weak var immediateCommentTextField: UITextField!
    
    var liked: Bool = false {
        didSet {
            if liked == true {
                likeButton.setImage(UIImage(named: "filledLikeImage"), for: .normal)
            } else {
                likeButton.setImage(UIImage(named: "emptyLikeImage"), for: .normal)
            }
        }
    }
    
    @IBOutlet weak var commentsContainer: UIView!
    @IBOutlet weak var commentsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var commentsViewHeight: NSLayoutConstraint!
    @IBAction func showAllCommentsButtonTap(_ sender: Any) {
        showAllCommentsTapHandler()
    }
    
    var showAllCommentsTapHandler:()->Void = {}
    var likeButtonTapHandler:()->Void = {}
    var commentButtonTapHandler:()->Void = {}
    var shareButtonTapHandler:()->Void = {}
    var offerButtonTapHandler:()->Void = {}
    
    weak var delegate: HashtagTapDelegate?
    
    var profilePictureTapHandler :()->Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commentProfileImageView.layer.cornerRadius = 18.0
        commentProfileImageView.layer.masksToBounds = true
        
        topLeftImageView.layer.cornerRadius = 18.0
        topLeftImageView.layer.masksToBounds = true
        
        self.hashtagsTextView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector((onHashtagLinkTap))))
    }
    
    @IBAction func showAllCommentsTap(_ sender: Any) {
        showAllCommentsTapHandler()
    }
    
    @IBAction func likeButtonTap(_ sender: Any) {
        likeButtonTapHandler()
    }
    
    @IBAction func commentButtonTap(_ sender: Any) {
        commentButtonTapHandler()
    }
    
    @IBAction func shareButtonTap(_ sender: Any) {
        shareButtonTapHandler()
    }
    
    @IBAction func offerButtonTap(_ sender: Any) {
        offerButtonTapHandler()
    }
    
    func setComments(_ comments:[Comment]) {
        var commentsArray:[Comment]
        if comments.count > maximumCommentsNumber {
            commentsArray = Array(comments.prefix(maximumCommentsNumber))
        } else {
            commentsArray = comments
        }
        
        clearComments()
        
        for comment in commentsArray {
            let initialHeight = commentsView.height
            let label = UILabel(frame: CGRect(x: inset, y: initialHeight + inset, width: UIScreen.main.bounds.width, height: 0))
            let commentString = NSMutableAttributedString(string: comment.author + " ", attributes: nameFontAttribute)
            let commentText = NSMutableAttributedString(string: comment.text, attributes: commentFontAttribute)
            commentString.append(commentText)
            label.attributedText = commentString
            label.numberOfLines = 0
            
            label.sizeToFit()
            commentsView.addSubview(label)
            commentsViewHeight.constant += label.height
            commentsView.height += label.height
            commentsContainerHeight.constant += label.height
        }
    }
    
    func clearComments() {
        for comment in commentsView.subviews {
            comment.removeFromSuperview()
        }
        
        commentsView.height = 0
        commentsViewHeight.constant = 0
        commentsContainerHeight.constant = 38
    }
    
    @IBAction func profilePictureButtonTap(_ sender: Any) {
        profilePictureTapHandler()
    }
    
    @objc func onHashtagLinkTap(sender: UIGestureRecognizer){
        let textView = self.hashtagsTextView
        let locationOfTouch = sender.location(in: sender.view)
        
        let tapPosition : UITextPosition? = self.hashtagsTextView?.closestPosition(to: locationOfTouch)
        if let textRange = self.hashtagsTextView?.tokenizer.rangeEnclosingPosition(tapPosition!, with: .word, inDirection: 1) {
            if var tappedWord = textView?.text(in: textRange) {
                let newRange = textView?.textRange(from: tapPosition!, to: tapPosition!)
                textView?.selectedTextRange = newRange
                let characterIndex = textView?.offset(from: (textView?.beginningOfDocument)!, to: (textView?.selectedTextRange!.start)!)
                tappedWord.insert("#", at: tappedWord.startIndex)
                tappedWord.insert(" ", at: tappedWord.endIndex)
                let index = self.getWordIndex(word: tappedWord, characterIndex: characterIndex!, text: (textView?.text)!)
                
                self.delegate?.userTappedAtHashtag(hashtag: tappedWord, tag: self.tag, index: index)
                print("selected word :\(tappedWord)")
            }
        }
    }
    
    func getWordIndex(word: String, characterIndex: NSInteger, text: String) -> NSInteger {
        let string = text.components(separatedBy: word)
        
        if string.count - 1 <= 1 {
            return 1
        }
        
        if string.count > 1 {
            let range = text.range(of: word)
            let startIndex = range?.lowerBound.samePosition(in: text.utf16)
            let endIndex = range?.upperBound.samePosition(in: text.utf16)
            let location = text.distance(from: text.startIndex,
                                         to: (range?.lowerBound)!)
            
//            let length = startIndex?.distance(to: endIndex)
//
//            if location + length! > characterIndex {
//                return 2
//            }
//            else {
//                return 3
//            }
        }
        
        return 0
    }
}
