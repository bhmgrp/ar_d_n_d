//
//  HashtagCollectionViewHeader.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/25/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class HashtagCollectionViewHeader: UICollectionReusableView {
    
    @IBOutlet weak var gridViewButton: UIButton!
    @IBOutlet weak var tableViewButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var image = UIImage(named: "grid")
        gridViewButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        image = UIImage(named: "drag")
        tableViewButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
}
