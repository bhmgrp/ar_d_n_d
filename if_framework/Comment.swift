//
//  Comments.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/30/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class Comment: NSObject {
    var commentID: String = ""
    var profileId: String = ""
    
    var commentsId: String = ""
    
    var text: String = ""
    var author: String = ""
    var authorImageUrl: String = ""
    var authorImage: UIImage?
    
    var discussion: [Comment] = [Comment]()
    var isReply: Bool = false
    
    init(text: String, author: String) {
        self.text = text
        self.author = author
    }
    
    init(text: String, author: String, authorImage: UIImage) {
        self.text = text
        self.author = author
        self.authorImage = authorImage
    }
    
    init(text: String, author: String, authorImage: UIImage, commentId: String) {
        self.text = text
        self.author = author
        self.authorImage = authorImage
        self.commentID = commentId
    }
}
