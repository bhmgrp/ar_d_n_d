/*
 The MIT License (MIT)

 Copyright (c) 2015-present Badoo Trading Limited.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

import Foundation
import Chatto
import ChattoAdditions

public protocol DemoMessageModelProtocol: MessageModelProtocol {
    var status: MessageStatus { get set }
}

public class MessageSender {
    
    var from:Int
    var to:Int
    
    init(from: Int, to: Int){
        if(from==0){
            self.from = UserDefaults.standard.integer(forKey: "loggedInUserID")
        }
        else {
            self.from = from
        }
        self.to = to
    }
    
    public var onMessageChanged: ((_ message: DemoMessageModelProtocol) -> Void)?

    public func sendMessages(_ messages: [DemoMessageModelProtocol]) {
        for message in messages {
            self.sendMessage(message)
        }
    }

    public func sendMessage(_ message: DemoMessageModelProtocol) {
        message.status = .sending
        
        if(message.type == TextMessageModel<MessageModel>.chatItemType) {
            self.sendTextMessage(message: message as! TextMessageModel<MessageModel>)
        }
    }
    
    private func sendTextMessage(message: TextMessageModel<MessageModel>) {
        
        let now = Date().timeIntervalSince1970
        
        DispatchQueue.global().async {
            let response = API.post(toURL: SECURED_API_URL + "my-messages/create/\(self.from)/\(self.to)/\(now)", withPost: ["content":message.text])
            switch response {
            case is [String:Any]:
                let response = response as! [String:Any]
                
                switch response["success"] {
                case (1 as Int), (true as Bool), ("1" as String):
                    DispatchQueue.main.async {
                        (message as! DemoMessageModelProtocol).status = .success
                        
                        let userMessage = UserMessage.create()
                        userMessage.from_user_id = Int32(self.from)
                        userMessage.to_user_id = Int32(self.to)
                        userMessage.content = message.text
                        userMessage.tstamp = now
                        
                        CoreDataStack.sharedStack.saveContext()
                        
                        self.onMessageChanged?(message as! DemoMessageModelProtocol)
                    }
                    break
                default:
                    switch response["error"] {
                    case is String:
                        DispatchQueue.main.async {
                            (message as! DemoMessageModelProtocol).status = .failed
                            self.onMessageChanged?(message as! DemoMessageModelProtocol)
                        }
                        break
                    default:
                        DispatchQueue.main.async {
                            (message as! DemoMessageModelProtocol).status = .failed
                            self.onMessageChanged?(message as! DemoMessageModelProtocol)
                        }
                        break
                    }
                }
                break
            default:
                DispatchQueue.main.async {
                    (message as! DemoMessageModelProtocol).status = .failed
                    self.onMessageChanged?(message as! DemoMessageModelProtocol)
                }
                break
            }
        }
    }
    
    private func fakeMessageStatus(_ message: DemoMessageModelProtocol) {
        switch message.status {
        case .success:
            break
        case .failed:
            self.updateMessage(message, status: .sending)
            self.fakeMessageStatus(message)
        case .sending:
            switch arc4random_uniform(100) % 5 {
            case 0:
                if arc4random_uniform(100) % 2 == 0 {
                    self.updateMessage(message, status: .failed)
                } else {
                    self.updateMessage(message, status: .success)
                }
            default:
                let delaySeconds: Double = Double(arc4random_uniform(1200)) / 1000.0
                let delayTime = DispatchTime.now() + Double(Int64(delaySeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.fakeMessageStatus(message)
                }
            }
        }
    }

    private func updateMessage(_ message: DemoMessageModelProtocol, status: MessageStatus) {
        if message.status != status {
            message.status = status
            self.notifyMessageChanged(message)
        }
    }

    private func notifyMessageChanged(_ message: DemoMessageModelProtocol) {
        self.onMessageChanged?(message)
    }
}
