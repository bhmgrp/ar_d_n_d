//
//  RunOnMainQueueAvoidDeadlock.h
//  if_framework
//
//  Created by Vadym Patalakh on 4/17/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

void runOnMainQueueAvoidDeadlock(void (^block)(void));
