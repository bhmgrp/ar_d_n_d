//
//  ImageViewController.h
//  if_framework
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (nonatomic) NSString *imageName;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
