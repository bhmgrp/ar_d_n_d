//
//  dynamicFields.h
//  if_framework
//
//  Created by User on 11/27/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dynamicFields : NSObject

@property (nonatomic) NSArray *dynamicFieldTypes;


+(int)getFieldNumberForTypeString:(NSString*)fieldTypeString;

+(NSString*)getFieldStringForTypeNumber:(int)fieldTypeNumber;

+(NSDictionary*)getDynamicFieldTypeNumbersForStrings;

enum {
    DYNAMICFIELD_TEXT = 0,
    DYNAMICFIELD_DROPDOWN = 1,
    DYNAMICFIELD_CHECKBOX = 2,
    DYNAMICFIELD_CALLBACKCHECKBOX = 3,
    DYNAMICFIELD_EMAILINPUT = 4,
    DYNAMICFIELD_HTML = 5,
    DYNAMICFIELD_NEWSLETTERCHECKBOX = 6,
    DYNAMICFIELD_LOTTERYCHECKBOX = 7,
    DYNAMICFIELD_TEXTAREA = 8,
    DYNAMICFIELD_STARRATING = 9,
    DYNAMICFIELD_COMBINEDCHECKBOXES = 10,
    DYNAMICFIELD_PHOTOUPLOAD = 11,
    DYNAMICFIELD_PAGE = 12,
    DYNAMICFIELD_NAME = 13,
    DYNAMICFIELD_REGISTRATION = 14,
    DYNAMICFIELD_DATE = 15,
    DYNAMICFIELD_APP_DIVIDER = 16,
    DYNAMICFIELD_APP_IMAGE = 17,
    DYNAMICFIELD_APP_PROFILE_IMAGE = 18,
    DYNAMICFIELD_APP_SETTINGS_CONFIRMED = 19,
};

@end
