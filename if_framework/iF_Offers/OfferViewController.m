//
//  OfferSliderViewController.m
//  if_framework
//
//  Created by User on 10/23/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "MBProgressHUD.h"

#import "API.h"

#import "PlaceOverViewController.h"

#import "OfferViewController.h"
#import "OfferDetailViewController.h"
#import "StructureOverViewController.h"

#import "NavigationViewController.h"

#import "SettingsViewController.h"

#import "OnboardingViewController.h"


#import "AppDelegate.h"

#import "gestgid-Swift.h"


#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]

@interface OfferViewController (){
    float startingViewHeight;
    NSString *bottomBarTransitionStyle;
    BOOL disableBounce;
}

@property (nonatomic) BOOL requesting;

@property(strong, nonatomic) NSMutableArray* detailViewControllers;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *locationBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *locationButtonImage;
- (IBAction)locationButtonClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *settingsButtonImage;
- (IBAction)settingsButtonClicked:(id)sender;

@property (nonatomic) localStorage *offerStorage;
@property (nonatomic) NSString *currentlyLoading;
@property (nonatomic) UIAlertView *updateOffersAlertView;

@property (strong,nonatomic) OfferDetailViewController *currentDetailViewController;

@end

@implementation OfferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _offerStorage = [localStorage storageWithFilename:@"offers"];
    
    
    _userIsLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"userLoggedIn"];
    
    if(_offerCampaignID==0){
        _offerCampaignID = [globals sharedInstance].offerCampaignID;;
    }
    
    _placeID = [globals sharedInstance].placeID;
    _pID = [globals sharedInstance].pID;
    
    _currentlyLoading = @"offers";
    
    bottomBarTransitionStyle = @"fade";
    
    
//    _acceptOfferButtonText.textColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OfferSliderViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    for (UIView *view in self.pageViewController.view.subviews ) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scroll = (UIScrollView *)view;
            scroll.delegate = self;
        }
    }
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.contentView.width, self.contentView.height);
    
    [self addChildViewController:_pageViewController];
    [self.contentView insertSubview:_pageViewController.view belowSubview:_contentSeparationView];
    [self.pageViewController didMoveToParentViewController:self];
    
    
    [self changeSettingsConfirmationStatus:_settingsConfirmed];
    [self registerForKeyboardNotifications];
    
    [self load];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(load) name:TERMINAL_MODE_UPDATE object:nil];
    
    if(IDIOM==IPAD){
        for(NSLayoutConstraint *c in _arrowVerticalConstraints){
            c.constant = 100;
        }
    }
    
    if(_disableAccept)
        _acceptOfferButtonView.alpha = 0.5;
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(self.revealViewController.panGestureRecognizer!=nil){
        [self.contentView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)viewDidLayoutSubviews{
    if(startingViewHeight == 0.0){
        startingViewHeight = self.contentView.height;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)shareButtonPressed:(UIBarButtonItem*)sharebutton {
    
    localStorage *userSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    NSString *userName = [[NSString stringWithFormat:@"%@ %@", [userSettingsStorage objectForKey:@"firstName"],[userSettingsStorage objectForKey:@"lastName"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *shareString = [NSString stringWithFormat:@"Let's meet and enjoy the AR D'N'D offer \"%@\" together. Just download the AR D'N'D App.\nhttp://www.ardnd.com\nYour %@", _offerData[0][@"name"], userName];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[shareString] applicationActivities:nil];
   
    UIPopoverPresentationController *popPresentationController;
    if  ((popPresentationController = activityViewController.popoverPresentationController)) {
        popPresentationController.sourceView = self.navigationController.view;
        popPresentationController.sourceRect = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, 20, 1, 1);
    }
    
    [self presentViewController:activityViewController animated:YES completion:^{
        
    }];
}

- (void)load{
    
    [self initNavigationBar];
    
    [self initHeaderView];
    
    [self loadData];
}

- (void)loadData{
    [self loadDataAndForceDownload:NO];
}

- (void)loadDataAndForceDownload:(BOOL)forceDownload{
    if(_offerCampaignID!=0){
        NSLog(@"current offerKey: %@",[NSString stringWithFormat:@"offersForCampaign%li",(long)_offerCampaignID]);
        
        NSDictionary *tempData = [_offerStorage objectForKey:[NSString stringWithFormat:@"offersForCampaign%li",(long)_offerCampaignID]];
        
        NSArray *tempArray = tempData[@"offers"];
        
        if(tempArray.count==0||tempArray==nil||forceDownload){
            
            _currentlyLoading = @"offers";
            
            // defining the URL
            NSString *urlString = [NSString stringWithFormat:@"%@gestgid/campaign/%li/offers/language=%i/is_host=%li", API_URL,(long)_offerCampaignID,[languages currentLanguageID],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"hostModeActivated"]];
            // Set the URL where we load from
            
            NSLog(@" loadDataAndForceDownload %@", urlString);
            
            // Create the request
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
            
            // Create the NSURLConnection
            [NSURLConnection connectionWithRequest:request delegate:self];
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = NSLocalizedString(@"Loading",@"");
            
        }
        else {
            _offerData = [tempArray mutableCopy];
            [self initPageViewController];
            
            [self checkForUpdatedOffers];
            
        }
    }
    else if(_offerID!=0){
        _currentlyLoading = @"offers";
        
        // defining the URL
        NSString *urlString = [NSString stringWithFormat:@"%@gestgid/offers/%li/language=%i/userID=%li/is_host=%li", API_URL,(long)_offerID,[languages currentLanguageID],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"hostModeActivated"]];
        // Set the URL where we load from
        
        NSLog(@" loadDataAndForceDownload %@", urlString);
        
        // Create the request
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        // Create the NSURLConnection
        [NSURLConnection connectionWithRequest:request delegate:self];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Loading",@"");
    }
    else if(_userID!=0){
        _currentlyLoading = @"offers";
        
        // defining the URL
        NSString *urlString = [NSString stringWithFormat:@"%@gestgid/offers/user/%li/language=%i/is_host=%li", API_URL,(long)_userID,[languages currentLanguageID],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"hostModeActivated"]];
        // Set the URL where we load from
        
        NSLog(@" loadDataAndForceDownload %@", urlString);
        
        // Create the request
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        // Create the NSURLConnection
        [NSURLConnection connectionWithRequest:request delegate:self];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Loading",@"");
    }
    else {
        _offerData = [@[] mutableCopy];
    }
    
}

- (void)adminMenu:(UIBarButtonItem*)sharebutton{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    if ([self.offerData[0][@"hidden"] integerValue] == 0) {
        [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Deactivate this offer", @"admin", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@" offers - hiding offer");
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                id response = [AdminAPI updateWithUrl:@"admin/offers/update" post:@{@"hidden":@1,@"uid":@(self.offerID)}];
                NSLog(@" offers - response: %@", response);
                
                if ([response isKindOfClass:[NSDictionary class]]) {
                    if ([[((NSDictionary*)response) valueForKey:@"success"] integerValue] == 1) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:true];
                        });
                    }
                }
            });
        }]];
    }
    else {
        [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Activate this offer", @"admin", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@" offers - showing offer");
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                id response = [AdminAPI updateWithUrl:@"admin/offers/update" post:@{@"hidden":@0,@"uid":@(self.offerID)}];
                NSLog(@" offers - response: %@", response);
                
                if ([response isKindOfClass:[NSDictionary class]]) {
                    if ([[((NSDictionary*)response) valueForKey:@"success"] integerValue] == 1) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:true];
                        });
                    }
                }
            });
            
        }]];
    }
    
    [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    ac.popoverPresentationController.sourceView = (UIView*)sharebutton;
    ac.popoverPresentationController.sourceRect = CGRectMake(33 / 2, 33/ 2, 0, 0);
    
    [self presentViewController:ac animated:true completion:nil];
}

- (void)dataLoaded{
    
    // if we checked for an update
    if([_currentlyLoading isEqualToString:@"offersMD5"]){
        if(_offerCampaignID==0)
            return;
        
        NSString *responseMD5 = [[NSString alloc] initWithData:_downloadedData encoding:NSUTF8StringEncoding];
        NSString *currentMD5 = [_offerStorage objectForKey:[NSString stringWithFormat:@"offersForCampaign%li",(long)_offerCampaignID]][@"offerJsonStringMD5"];
        NSLog(@"responseMD5: %@; currentMD5: %@",responseMD5,currentMD5);
        if(![responseMD5 isEqualToString:currentMD5]){
            
            [self updateAvailable];
        }
        
        return;
    }
    
    if(_downloadedData!=nil){
        NSString *jsonString = [[NSString alloc] initWithData:_downloadedData encoding:NSUTF8StringEncoding];
        NSString *jsonStringMD5 = [[jsonString MD5String] lowercaseString];
        
        if(_offerCampaignID==0 && _offerID!=0){
            NSError *error;
            NSDictionary *offer = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
            _offerData = [@[offer] mutableCopy];
            
            if(_offerData.count == 1){
                UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithImage:[[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-personadd"] forSize:CGSizeMake(33, 33)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonPressed:)];
                shareItem.tintColor = [UIColor blackColor];
                
                NSMutableArray *items = [@[shareItem] mutableCopy];
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hostModeActivated"]) {
                    UIBarButtonItem *adminItem = [[UIBarButtonItem alloc] initWithImage:[[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-more"] forSize:CGSizeMake(33, 33)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(adminMenu:)];
                    adminItem.tintColor = [UIColor blackColor];
                    
                    [items addObject:adminItem];
                }
                
                
                self.navigationItem.rightBarButtonItems = items;
            }
            
            [self initPageViewController];
            return;
        }
        else if(_offerCampaignID==0 && _userID!=0){
            
            // Parse the JSON that came in
            NSError *error;
            NSArray *offersArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
            
            
            if(offersArray.count>0){
                _offerData = [offersArray mutableCopy];
                if(_offerData.count == 1){
                    
                    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithImage:[[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-personadd"] forSize:CGSizeMake(33, 33)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonPressed:)];
                    shareItem.tintColor = [UIColor blackColor];
                    
                    NSMutableArray *items = [@[shareItem] mutableCopy];
                    
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hostModeActivated"]) {
                        UIBarButtonItem *adminItem = [[UIBarButtonItem alloc] initWithImage:[[imageMethods resizeImage:[UIImage imageNamed:@"iosicons/ios7-more"] forSize:CGSizeMake(33, 33)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(adminMenu:)];
                        adminItem.tintColor = [UIColor blackColor];
                        
                        [items addObject:adminItem];
                    }
                    
                    
                    self.navigationItem.rightBarButtonItems = items;
                }
            }
            else {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                _offerData = [@[] mutableCopy];
            }
            [self initPageViewController];
            return;
        }
        
        
        // Parse the JSON that came in
        NSError *error;
        NSArray *offersArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
        
        
        if(offersArray.count>0){
            NSDictionary *offers = @{
                                     @"offerJsonStringMD5":jsonStringMD5,
                                     @"offers":offersArray
                                     };
            
            
            [_offerStorage setObject:offers forKey:[NSString stringWithFormat:@"offersForCampaign%li",(long)_offerCampaignID]];
            
            _offerData = [offersArray mutableCopy];
        }
        else {
            [_offerStorage removeObjectForKey:[NSString stringWithFormat:@"offersForCampaign%li",(long)_offerCampaignID]];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            _offerData = [@[] mutableCopy];
        }
    }
    else {
        [_offerStorage removeObjectForKey:[NSString stringWithFormat:@"offersForCampaign%li",(long)_offerCampaignID]];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        _offerData = [@[] mutableCopy];
    }
    
    
    [self initPageViewController];
    
}

- (void)initHeaderView{
    
    _headerView.hidden = NO;
    // arrows for navigation
    // left arrow
    _leftArrowImageView.image = [_leftArrowImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_leftArrowImageView setTintColor:[UIColor whiteColor]];
    _leftArrowImageView.hidden = YES; // initially hidden
    
    // right arrow
    _rightArrowImageView.image = [_rightArrowImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_rightArrowImageView setTintColor:[UIColor whiteColor]];
    _rightArrowImageView.hidden = YES; // initially hidden
}

- (void)initPageViews{
    
    _detailViewControllers = [@[] mutableCopy];
    
    NSArray *tempArray = @[];
    
    if(_offerData.count>0){
        tempArray = _offerData;
        if(_offerData.count>1)_headerView.hidden = NO;
        _pageViewController.view.hidden = NO;
    }
    
    _offerData = [@[
                     //add a first object (which will be the structure overview page)
//                    @{
//                        @"structureOverView":@YES,
//                        @"header_color":@"#ffffff"
//                        }
                    ] mutableCopy];
    
    NSUInteger index = 0;
    
    //[_detailViewControllers addObject:[self viewControllerAtIndex:index]];
    index++;
    for(NSDictionary* dict in tempArray){
//        NSLog(@"dict endtime %@, Tstamp: %i",dict[@"endtime"],[TimeStamp intValue]);
//        if((dict[@"endtime"]==nil || [dict[@"endtime"] intValue]==0 || [dict[@"endtime"] intValue]>[TimeStamp intValue]) &&
//           (dict[@"starttime"]==nil || [dict[@"starttime"] intValue]==0 || [dict[@"starttime"] intValue]<[TimeStamp intValue])){
            [_offerData addObject:dict];
            
            OfferDetailViewController *odvc = [self viewControllerAtIndex:index];
            odvc.offerDictionary = [dict mutableCopy];
            [odvc initImages];
            //[_detailViewControllers addObject:odvc];
            index++;
            
//        }
    }
    
    NSUInteger numberOfPages = [_offerData count];
    
    [self.pageControll setNumberOfPages:numberOfPages];
    
    // check if we have more than one page
    if(numberOfPages>1){
        // if yes unhide the right arrow
        _rightArrowImageView.hidden = NO;
    }
    
    disableBounce = NO;
    
    // get the first viewController
    if(_offerData.count<=_indexToOpen)_indexToOpen=0;
    _currentDetailViewController = [self viewControllerAtIndex:_indexToOpen];
    _currentDetailViewController.pageIndex = _indexToOpen;
    
    if([_currentDetailViewController isKindOfClass:[StructureOverViewController class]]){
        [self hideBottomButtonBar:YES];
        if(IDIOM==IPAD)self.headerView.hidden = YES;
    }
    
    [self updateHeaderTintColor];
    
    [self.pageViewController setViewControllers:@[_currentDetailViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    [self updateHeaderInputFieldsForIndex:_indexToOpen];
}

- (void)initPageViewController{
    [self initHeaderView];
    [self initPageViews];
}

- (void)checkForUpdatedOffers{
    _currentlyLoading = @"offersMD5";
    
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@gestgid/campaign/%li/offers/md5/language=%i/", API_URL,(long)_offerCampaignID,[languages currentLanguageID]];
    // Set the URL where we load from
    
    NSLog(@"%@", urlString);
    
    // Create the request
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)updateAvailable{
    _updateOffersAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update available",@"") message:NSLocalizedString(@"New offers are available. Please update your app.",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
    [_updateOffersAlertView show];
}


- (void)hideBottomButtonBar:(BOOL)hide{
    // always hide
    _acceptOfferButtonView.hidden = YES;
    return;
/*    if([bottomBarTransitionStyle isEqualToString:@"fade"]){
        if(hide){
            [UIView animateWithDuration:0.5f animations:^(void){
                _acceptOfferButtonView.alpha = 0.0f;
                _acceptOfferButtonVisualEffectView.alpha = 0.0f;
            } completion:^(BOOL finished){
                if(finished){
                    _acceptOfferButtonView.hidden = YES;
                    _acceptOfferButtonVisualEffectView.hidden = YES;
                }
            }];
        }
        else {
            _acceptOfferButtonView.hidden = NO;
            _acceptOfferButtonVisualEffectView.hidden = NO;
            [UIView animateWithDuration:0.5f animations:^(void){
                _acceptOfferButtonView.alpha = 1.0f;
                _acceptOfferButtonVisualEffectView.alpha = 1.0f;
            } completion:^(BOOL finished){
                if(finished){
                }
            }];
        }
        return;
    }
    
    if([bottomBarTransitionStyle isEqualToString:@"moveDown"]){
        if(hide){
            if(!_acceptOfferButtonView.hidden){
                [UIView animateWithDuration:0.5 animations:^(void){
                    CGRect tempFrame = _acceptOfferButtonView.frame;
                    tempFrame = CGRectMake(tempFrame.origin.x, [UIScreen mainScreen].bounds.size.height, tempFrame.size.width, tempFrame.size.height);
                    
                    _acceptOfferButtonView.frame = tempFrame;
                    _acceptOfferButtonViewBottomSpace.constant = -_acceptOfferButtonView.height;
                    _acceptOfferButtonVisualEffectView.frame = tempFrame;
                    _acceptOfferButtonVisualEffectViewBottomSpace.constant = -_acceptOfferButtonVisualEffectView.height;
                }];
            }
        }
        else {
            if(_acceptOfferButtonView.hidden){
                [UIView animateWithDuration:0.5 animations:^(void){
                    CGRect tempFrame = _acceptOfferButtonView.frame;
                    tempFrame = CGRectMake(tempFrame.origin.x, [UIScreen mainScreen].bounds.size.height-tempFrame.size.height, tempFrame.size.width, tempFrame.size.height);
                    
                    _acceptOfferButtonView.frame = tempFrame;
                    _acceptOfferButtonViewBottomSpace.constant = 0;
                    _acceptOfferButtonVisualEffectView.frame = tempFrame;
                    _acceptOfferButtonVisualEffectViewBottomSpace.constant = 0;
                }];
            }
        }
    }
    
    _acceptOfferButtonView.hidden = hide;*/
}

- (void)openDetailViewController{
    // following code will be executed if we click on the currently presented view controller
}

- (void)updateHeaderTintColor{
    if(_offerData.count<=_currentDetailViewController.pageIndex)
        return;
    
    UIColor *headerColor = [UIColor colorFromHexString:_offerData[_currentDetailViewController.pageIndex][@"header_color"]];
    
    self.pageControll.currentPageIndicatorTintColor = headerColor;
    [_leftArrowImageView setTintColor:headerColor];
    
    [_rightArrowImageView setTintColor:headerColor];
}

- (void)updateHeaderInputFieldsForIndex:(NSUInteger)index{
    NSUInteger currentPage = index;
    [self.pageControll setCurrentPage:currentPage];
    
    if([_currentDetailViewController isKindOfClass:[StructureOverViewController class]]){
        [self hideBottomButtonBar:YES];
        if(IDIOM==IPAD)_headerView.hidden = YES;
    }
    else {
        [self hideBottomButtonBar:NO];
        if(IDIOM==IPAD)_headerView.hidden = NO;
    }
    
    if(currentPage==0){
        _leftArrowImageView.hidden = YES;
        if(self.revealViewController.panGestureRecognizer!=nil)[_currentDetailViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    } else {
        _leftArrowImageView.hidden = NO;
    }
    
    if(currentPage==_offerData.count-1){
        _rightArrowImageView.hidden = YES;
    } else {
        _rightArrowImageView.hidden = NO;
    }
}

- (void)changeSettingsConfirmationStatus:(BOOL)status acceptText:(NSString*)acceptText {
    
    // if user is not logged in but we have terminal mode:
    if(!_userIsLoggedIn && [globals sharedInstance].terminalMode){
        // hide the "register" text but don't set settings confirmed
        _acceptOfferButtonText.text = @"";
        _settingsConfirmed = NO;
        return;
    }
    
    // if user is not logged in:
    if(!_userIsLoggedIn){
        // show "register" text and set settings to not confirmed
        _acceptOfferButtonText.text = NSLocalizedString(@"REGISTER",@"");
        _settingsConfirmed = NO;
        return;
    }
    
    _settingsConfirmed = status;
    if(status){
        
        if(acceptText!=nil && ![acceptText isEqualToString:@""]){
            _acceptOfferButtonText.text = acceptText;
        }
        else {
            _acceptOfferButtonText.text = NSLocalizedString(@"Order now", @"");
        }
    }
    else {
        _acceptOfferButtonText.text = NSLocalizedString(@"Settings", @"");
    }
}

- (void)changeSettingsConfirmationStatus:(BOOL)status{
    
    // if user is logged in and we have terminal mode:
    if(_userIsLoggedIn && [globals sharedInstance].terminalMode){
        // set settings as confirmed
        [self changeSettingsConfirmationStatus:YES acceptText:nil];
        return;
    }
    
    
    [self changeSettingsConfirmationStatus:status acceptText:nil];
}

- (void)pageViewController:(UIPageViewController *)pageViewController finishedAnimating:(BOOL)finished{
    _currentDetailViewController = (OfferDetailViewController*)pageViewController.viewControllers[0];
    
    [self updateHeaderInputFieldsForIndex:_currentDetailViewController.pageIndex];
    
    _userIsLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"userLoggedIn"];
    [self changeSettingsConfirmationStatus:_currentDetailViewController.confirmSettingsCheckboxChecked];
    [self updateHeaderTintColor];
}


#pragma mark - Page View Controller Delegate and Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSUInteger index = ((OfferDetailViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSUInteger index = ((OfferDetailViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [_offerData count] || _offerData==nil) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{
    return 0;//return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return 0;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if(completed && finished){
        [self pageViewController:pageViewController finishedAnimating:YES];
    }
}

- (OfferDetailViewController *)viewControllerAtIndex:(NSUInteger)index{
    if (([_offerData count] == 0) || (index >= [_offerData count])) {
        return nil;
    }
    
    if(_offerData[index][@"structureOverView"]!=nil){
        StructureOverViewController *stovc = [StructureOverViewController loadNowFromStoryboard:@"StructureOverViewInline"];
        if(IDIOM==IPAD){
            stovc = [StructureOverViewController loadNowFromStoryboard:@"StructureOverViewInline-iPad"];
        }
        stovc.offerViewController = self;
        return stovc;
    }
    
    // Create a new view controller and pass suitable data.
    OfferDetailViewController *offerDetailViewController = [OfferDetailViewController loadNowFromStoryboard:@"OfferDetailViewController"];
    
    offerDetailViewController.pageIndex = index;
    offerDetailViewController.offerDictionary = _offerData[index];
    offerDetailViewController.offerViewController = self;
    
    return offerDetailViewController;
}


#pragma mark - Scroll View Controller Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //NSLog(@"x: %f, y: %f",scrollView.contentOffset.x, scrollView.contentOffset.y);
    if(_currentDetailViewController.editing){
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if(disableBounce){
        if (_currentDetailViewController.pageIndex==0 && scrollView.contentOffset.x < scrollView.bounds.size.width) {
            scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
        }
        if (_currentDetailViewController.pageIndex==_offerData.count-1 && scrollView.contentOffset.x > scrollView.bounds.size.width) {
            scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
        }
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{

}


#pragma mark - arrows for navigation

- (IBAction)leftArrowClicked:(id)sender {
    NSInteger currentPage = _currentDetailViewController.pageIndex;
    if(currentPage!=0){
        currentPage--;
        OfferDetailViewController *vc = [self viewControllerAtIndex:currentPage];

        [_pageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL finished){}];

        [self pageViewController:_pageViewController finishedAnimating:YES];
    }
}

- (IBAction)rightArrowClicked:(id)sender {
    NSInteger currentPage = _currentDetailViewController.pageIndex;
    if(currentPage!=_offerData.count-1){
        currentPage++;
        OfferDetailViewController *vc = (OfferDetailViewController*)[self pageViewController:_pageViewController viewControllerAfterViewController:_currentDetailViewController];
        
        [_pageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished){}];
        
        [self pageViewController:_pageViewController finishedAnimating:YES];
    }
}


#pragma mark - NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self dataLoaded];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self dataLoaded];
}


#pragma mark - UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView == _updateOffersAlertView){
        [self loadDataAndForceDownload:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    
}


#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, -(64+_headerView.height), [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) +_headerView.height+64);
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{

}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}


#pragma mark - accept offer process

- (IBAction)acceptOfferButtonPressed:(id)sender {
    
    if(_requesting || _disableAccept){
        return;
    }
    
    // if user is not logged in but we have terminal mode:
    if(!_userIsLoggedIn && [globals sharedInstance].terminalMode){
        // don't do anything
        return;
    }
    
    if(!_userIsLoggedIn){
        
        OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
        
        [self.revealViewController pushFrontViewController:ovc animated:YES];
        return;
    }
    
    if(!_settingsConfirmed){
        
        SettingsViewController *svc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
        
        svc.settingType.text = @"generalSettings";
        
        [self.navigationController pushViewController:svc animated:YES];
        return;
    }
    
    if(![self allInputFieldsFilledOut]){
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"This field is required",@"") message:NSLocalizedString(@"Please add all necessary information.",@"") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",@""), nil] show ];
        [_currentDetailViewController highlightRequiredFields];
        return;
    }
    
    _requesting = YES;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @try{
            NSInteger success = 0;
            
            NSDictionary *dict = [API postToURL:[NSString stringWithFormat:@"%@offer/requests",SECURED_API_URL]
                                       WithPost:@{@"save":@(1),@"user_id":@([[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"]),@"request_text":_currentDetailViewController.requestText != nil ? _currentDetailViewController.requestText : @"",@"offer_id":_currentDetailViewController.offerDictionary[@"uid"], @"language":@([languages currentLanguageID])}];
            
            success = [dict[@"success"] integerValue];
            
            if(success){
                _requesting = NO;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Your request", @"")
                                                message:NSLocalizedString(@"Your request has been forwarded successfully. We will contact you as soon as possible.", @"")
                                               delegate:nil
                                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                      otherButtonTitles: nil
                      ] show];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                });
            } else {
                _requesting = NO;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                message:NSLocalizedString(dict[@"message"]?dict[@"message"]:dict[@"error"], @"")
                                               delegate:nil
                                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                      otherButtonTitles:nil
                      ] show];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                });
            }
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            
            _requesting = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
        }
    });


    //[self.navigationController popToRootViewControllerAnimated:YES];

}

- (BOOL)allInputFieldsFilledOut{
    NSMutableArray *inputArray = _currentDetailViewController.inputArray;
    
    BOOL everythingFilledOut = YES;
    
    for(NSDictionary *dict in inputArray){
        if([dict[@"required"] intValue]==1){
            if(dict[@"value"]==nil){
                everythingFilledOut = NO;
            }
            if([dict[@"value"] isKindOfClass:[NSString class]]){
                if([dict[@"value"] isEqualToString:@""]){
                    everythingFilledOut = NO;
                }
            }
            if([dict[@"value"] isKindOfClass:[NSNumber class]]){
                if(dict[@"value"]==0){
                    everythingFilledOut = NO;
                }
            }
        }
    }
    
    return everythingFilledOut;
}

- (void)showErrorViewWithTitle:(NSString*)title description:(NSString*)description{
    _errorView.hidden = NO;
    _errorHeader.text = title;
    _errorDescription.text = description;
    
    [self hideBottomButtonBar:YES];
}


#pragma mark - navigation bar

- (void)initNavigationBarRightButton{
    
    // no right bar if terminal mode is active
    if([globals sharedInstance].terminalMode){
        self.navigationItem.rightBarButtonItem = nil;
        return;
    }
    
    if(SINGLEPLACEID==0){
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        
        UIImage *locImage = [self.locationButtonImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.locationButtonImage.image = nil;
        [self.locationButtonImage setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        self.locationButtonImage.image = locImage;
        
        self.navigationItem.rightBarButtonItem = self.locationBarButton;
    }
    else {
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        
        UIImage *locImage = [self.settingsButtonImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.settingsButtonImage.image = nil;
        [self.settingsButtonImage setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        self.settingsButtonImage.image = locImage;
        
        self.navigationItem.rightBarButtonItem = self.settingsBarButton;
    }
}

- (void)initNavigationBarLeftButton{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
}

- (void)initNavigationBarButtons{
    //[self initNavigationBarLeftButton];
    //[self initNavigationBarRightButton];
}

- (void)initNavigationBar{
    [self initNavigationBarButtons];
    
    if([self.title isEqualToString:@""] || self.title == nil){
        self.title = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"];
    }
    
    if(self.navigationItem.titleView == nil) {
        
        //NSLog(@" test titleView offer => %@ ", @"set new titleView");
        
        //self.navigationItem.titleView = [NavigationTitle createNavTitle:NSLocalizedString(@"Offers", @"Angebote") SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];
}

- (IBAction)settingsButtonClicked:(id)sender {
    if([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1){
        SettingsViewController *svc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
        //NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:svc];
        
        svc.settingType.text = @"generalSettings";
        [self.navigationController pushViewController:svc animated:YES];
        //[self.revealViewController pushFrontViewController:nvc animated:YES];
    } else {
        OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
        ovc.modalPresentationStyle = UIModalPresentationFullScreen;
        ovc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self.revealViewController pushFrontViewController:ovc animated:YES];
    }
}

- (IBAction)locationButtonClicked:(id)sender {
    UIViewController *vc = [AppDelegate getStartViewController];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark trash

-(void)trash{
    [@[@{
                        @"icon":@"android_star",
                        @"header_image":@"silvester.jpg",
                        @"header_title":@"SILVERSTER IM DOME",
                        @"header_sub_title":@"39 EUR PRO PERSON",
                        @"header_text_color":@"#ffffff",
                        @"title":@"Silvester im Dome",
                        @"description":@"Die gläserne Eventhalle Kameha Dome verwandelt sich in der Silvesternacht in eine schillernde Partykulisse mit DJ und Live-Acts. Partyticket inklusive Welcome Drink bis 23:30 Uhr.",
                        @"price":@"39 EUR PRO PERSON",
                        @"input_fields":@[
                                @{
                                    @"title":@"Anzahl Personen",
                                    @"type":@"text",
                                    },
                                ],
                        },
                    @{
                        @"icon":@"coffee",
                        @"header_image":@"coffee_bg.jpg",
                        @"header_title":@"BONNER KUCHEN",
                        @"header_sub_title":@"HEUTE 20% RABATT",
                        @"header_text_color":@"#ffffff",
                        @"title":@"Bonner Kuchen",
                        @"description":@"Bonner Kuchen aus eigener Herstellung zum Vorzugspreis.",
                        @"price":@"3 EUR PRO STÜCK",
                        @"input_fields":@[
                                @{
                                    @"title":@"Zimmernummer",
                                    @"type":@"text",
                                    },
                                ],
                        },
                    @{
                        @"icon":@"female",
                        @"header_image":@"diva.jpg",
                        @"header_title":@"DIVA & GLAMOUR LOVERS",
                        @"header_sub_title":@"PER APP NUR 199 EUR",
                        @"header_text_color":@"#000000",
                        @"title":@"Diva & Glamour Lovers",
                        @"description":@"- Übernachtung in der Diva Suite\n- 1 Flasche Champagner\n- Lifestyle Frauenmagazine\n- 30 Minuten Kosmetik\n- Essen in der Brasserie Next Level",
                        @"price":@"199 EUR STATT 269 EUR",
                        @"input_fields":@[
                                @{
                                    @"title":@"Wunschtermin",
                                    @"type":@"text",
                                    },
                                ],
                        },
                    ] mutableCopy];
    
    
}



@end
