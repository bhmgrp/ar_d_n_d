//
//  OfferDetailViewController.m
//  if_framework
//
//  Created by User on 10/23/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "OfferDetailViewController.h"
#import "RegisterHeaderViewController.h"

#import "dynamicFields.h"
#import "gestgid-Swift.h"

#define LIKE(str1,str2) if([str1 isEqualToString:str2])

@interface OfferDetailViewController () <MKMapViewDelegate>{
    float screenMultiplicator;
    BOOL showRequiredFields;
    CGRect initialHeaderFrame;
}

@property (strong,nonatomic) OfferDetailViewController* textFieldDelegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeight;
@property (nonatomic) BOOL showWeekdays, showTimerange, showSpecial;
@end

@implementation OfferDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenMultiplicator = [UIScreen mainScreen].bounds.size.width / 320.0f;
    
    UIColor *headerTintColor = [UIColor colorFromHexString:_offerDictionary[@"header_color"]];
    
    self.headerLabel.text = [_offerDictionary[@"name"] uppercaseString];
    self.headerLabel.textColor = headerTintColor;
    if(IDIOM==IPAD)self.headerLabel.font = [UIFont fontWithName:@"Helvetica-BOLd" size:21];
    
    self.headerSubtitleLabel.text = [_offerDictionary[@"saving"] uppercaseString];
    self.headerSubtitleLabel.textColor = headerTintColor;
    
    
    self.headerIconWrapper.layer.borderColor = [headerTintColor CGColor];
    self.headerIconWrapper.layer.borderWidth = 2.0f;
    self.headerIconWrapper.layer.cornerRadius = 7.5f;
    
    float width = self.headerIcon.width*screenMultiplicator;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *image = [self loadImageForItem:_offerDictionary useFor:@"icon" atPath:_offerDictionary[@"icon"] width:width];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.headerIcon.image = image;
            self.headerIcon.image = [self.headerIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        });
    });
    
    self.headerIcon.tintColor = headerTintColor;
    self.headerIcon.translatesAutoresizingMaskIntoConstraints = NO;
    self.headerIconWidth.constant = self.headerIconWidth.constant * screenMultiplicator;
    self.headerIconHeight.constant = self.headerIconHeight.constant * screenMultiplicator;
    
    if(IDIOM==IPAD){
        self.headerIconWrapperTopSpace.constant = 60;
    
        self.headerSubtitleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
    
        self.headerLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:28];
        
        self.headerIconHeight.constant = 105;
        self.headerIconWidth.constant = 105;
        
        self.headerSubtitleBottomSpace.constant = 105;
    }
    
    
    [self initImages];
    
    
    self.textFieldDelegate = self;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self initInputArray];
    
    //[self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.0];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view endEditing:YES];
    [self.view resignFirstResponder];
    
}

-(void)viewDidLayoutSubviews{
    if(initialHeaderFrame.size.height==0)
        initialHeaderFrame = self.headerWrapper.frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if([overlay isKindOfClass:[MKCircle class]]){
        MKCircleRenderer *renderer = [[MKCircleRenderer alloc] initWithCircle:overlay];
        renderer.fillColor = [UIColor colorWithRGBHex:0x007AFF];
        renderer.strokeColor = [UIColor whiteColor];
        renderer.lineWidth = 3;
        renderer.alpha = 0.2;
        return renderer;
    }
    
    return nil;
}

-(void)initImages{
    self.headerBackgroundImageView.image = [self loadImageForItem:_offerDictionary useFor:@"header" atPath:_offerDictionary[@"image_header"] width:[UIScreen mainScreen].bounds.size.width];
    //self.backgroundImageView.image = [self loadImageForItem:_offerDictionary useFor:@"background" atPath:_offerDictionary[@"image_background"] width:[UIScreen mainScreen].bounds.size.width];
}

-(void)initInputArray{
    // initialize and mutable-ize the array and containing dictionaries
    if(_offerDictionary[@"dynamic_fields"]!=nil && ![_offerDictionary[@"dynamic_fields"] isKindOfClass:[NSNull class]]){
        _inputArray = [_offerDictionary[@"dynamic_fields"] mutableCopy];
    }
    
    _dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    NSMutableArray *tempInputArray = [@[] mutableCopy];
    for(NSDictionary *dict in _inputArray){
        NSMutableDictionary *newDict = [dict mutableCopy];
        [self preFillInputDict:newDict];
        [tempInputArray addObject:newDict];
        
        int inputType = [dict[@"field_type"] intValue];
        
        switch (inputType) {
            case DYNAMICFIELD_DROPDOWN:{
                newDict[@"cellType"] = @"dropdownCell";
                
                newDict[@"options"] = [dict[@"options"] componentsSeparatedByString:@";"];
                
                break;
            }
            case DYNAMICFIELD_DATE:{
                newDict[@"cellType"] = @"dateCell";
                break;
            }
            default:{
                newDict[@"cellType"] = @"inputCell";
                break;
            }
        }
        
    }
    _inputArray = tempInputArray;
    
    _dropdownArray = [@[] mutableCopy];
    
}


#pragma mark table view delegate and datasource methods

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if(section==0){
        return 0;
    }
    
    if(section==1){
        return 30;
    }
    
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==1){
        if([globals sharedInstance].terminalMode){
            return _inputArray.count;
        }
        return _inputArray.count+3;
    }
    int num = 6;
    if([self.offerDictionary[@"dynamic_content"] isKindOfClass:[NSDictionary class]] &&
       [self.offerDictionary[@"dynamic_content"][@"days"] isKindOfClass:[NSArray class]] &&
       ((NSArray*)self.offerDictionary[@"dynamic_content"][@"days"]).count > 0){
        num += 1;
        self.showWeekdays = true;
    }
    if([self.offerDictionary[@"dynamic_content"] isKindOfClass:[NSDictionary class]] &&
       [self.offerDictionary[@"dynamic_content"][@"timerange"] isKindOfClass:[NSDictionary class]] &&
       ![self.offerDictionary[@"dynamic_content"][@"timerange"][@"from"] isEmptyString] &&
       ![self.offerDictionary[@"dynamic_content"][@"timerange"][@"to"] isEmptyString]){
        num += 1;
        self.showTimerange = true;
    }
    //if([self.offerDictionary[@"dynamic_content"] isKindOfClass:[NSDictionary class]] &&
    //   [self.offerDictionary[@"dynamic_content"][@"special"] isKindOfClass:[NSString class]] &&
    //   ![self.offerDictionary[@"dynamic_content"][@"special"] isEmptyString]){
    //    num += 1;
    //    self.showSpecial = true;
    //}
    return num;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section==0){
        return nil;
    }
    RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
    //rhvc.labelTitle.text = sectionDict[@"title"];
    
    rhvc.view.tag = 0;
    
    rhvc.labelHeaderTitle.text = @"";
    
    return rhvc.view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section==1){
        if(indexPath.row==_inputArray.count+2){
            return 70;
        }
        if(indexPath.row==_inputArray.count+1){
            return 90;
        }
        if(indexPath.row==_inputArray.count){
            OfferDetailTableViewCell *c = [self getSettingsConfirmedCell];
            
            CGSize labelWidth = CGSizeMake([UIScreen mainScreen].bounds.size.width-15-65, CGFLOAT_MAX); // 300 is fixed width of label. You can change this value
            CGRect textRect = [c.offerSettingsConfirmedLabel.text boundingRectWithSize:labelWidth options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:17.0]} context:nil];
            
            return textRect.size.height+16;
        }
        if(_inputArray.count>indexPath.row){
            NSMutableDictionary *inputDict = _inputArray[indexPath.row];
            if([self inputDictShouldBePreFilled:inputDict]){
                return 0;
            }
            
            if([inputDict[@"cellType"] isEqualToString:@"dropdownCell"]){
                OfferDetailTableViewCell *c = [self getDropdownCellForInputDict:inputDict forIndexPath:indexPath];
                c.offerInputLabel.width = [UIScreen mainScreen].bounds.size.width/3*2 - c.accessoryView.width - 15;
                [c.offerInputLabel sizeToFit];
                
                return c.offerInputLabel.height+24;
            }
        }
        return 45;
    }
    if(indexPath.section==0){
        if(indexPath.row==1){
            OfferDetailTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
            
            cell.offerAvailabilityLabel.text = NSLocalizedStringFromTable(@"AR D'N'D Special", @"admin", @"");
            
            cell.offerAvailability.text = self.offerDictionary[@"dynamic_content"][@"special"];
            
            cell.width = [UIScreen mainScreen].bounds.size.width;
            
            [cell.offerAvailability sizeToFit];
            
            [cell.offerAvailabilityLabel sizeToFit];
            
            return MAX(60, cell.offerAvailability.height + 16);
        }
        else if(indexPath.row==2){
            OfferDetailTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
            
            NSString *description = _offerDictionary[@"description"];
        
            cell.offerDescriptionLabel.text = description;
            
            CGSize labelWidth = CGSizeMake([UIScreen mainScreen].bounds.size.width-30, CGFLOAT_MAX); // 300 is fixed width of label. You can change this value
            CGRect textRect = [cell.offerDescriptionLabel.text boundingRectWithSize:labelWidth options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:17.0]} context:nil];
            
            return textRect.size.height+16;
        }
        else if(
                (indexPath.row==5 && self.showWeekdays) ||
                (indexPath.row==5 && self.showTimerange) ||
                //(indexPath.row==4 && self.showSpecial) ||
                (indexPath.row==6 && self.showTimerange && self.showWeekdays) //||
                //(indexPath.row==5 && self.showTimerange && self.showSpecial) ||
                //(indexPath.row==5 && self.showWeekdays && self.showSpecial) ||
                //(indexPath.row==6 && self.showTimerange && self.showWeekdays && self.showSpecial)
                ){
            return 60;
        }
        else if(indexPath.row==5 || indexPath.row==6 || indexPath.row==7){
            return 130;
        }
        else return 60;
        //OfferDetailTableViewCell* c = [self getInformationTableViewCell];
        //return 166 - 47 + [self getHeightForLabel:c.offerDescriptionLabel];
    }
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OfferDetailTableViewCell *c = [self getTextInputCellForIndexPath:indexPath];
    
    if(indexPath.section==0){
        if(indexPath.row==0){
            OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
            
            if (cell.tag != indexPath.row+1000) {
                cell.tag = indexPath.row+1000;
                cell.offerTitleLabel.text = [NSString stringWithFormat:@"%@ | %@",_offerDictionary[@"host_name"],_offerDictionary[@"name"]];
                
                cell.tag = indexPath.row;
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSString *hostImageString = _offerDictionary[@"host_image"];
                    
                    if (hostImageString != nil && !hostImageString.isEmptyString){
                        NSLog(@"host image string: %@", hostImageString);
                        UIImage *img = nil;
                        if([hostImageString hasPrefix:@"http"]){
                            img = [imageMethods UIImageWithWebPath:hostImageString versionIdentifier:@""];
                        }
                        else {
                            img = [imageMethods UIImageWithServerPathName:hostImageString versionIdentifier:@"" width:52.0];
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(img != nil && cell.tag == indexPath.row){
                                NSLog(@"image: %@", img);
                                cell.hostImage.image = img;
                                cell.hostImage.layer.cornerRadius = 26.0;
                                [cell setNeedsLayout];
                            }
                        });
                    }
                });
            }
            
            return cell;
        }
        else if(indexPath.row==1){
            OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
            
            cell.offerAvailabilityLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"AR D'N'D Special", @"admin", @"")];
            
            cell.offerAvailability.text = self.offerDictionary[@"dynamic_content"][@"special"];
            
            return cell;
        }
        else if(indexPath.row==2){
            OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
            
            NSString *description = _offerDictionary[@"description"];
            
            cell.offerDescriptionLabel.text = description;
            
            return cell;
        }
        else if(indexPath.row==3){
            OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
            
            cell.offerAvailabilityLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"Price per person", @"admin", @"")];
            
            cell.offerAvailability.text = _offerDictionary[@"saving"];
        
            return cell;
        }
        else if(indexPath.row==4){
            OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
            
            cell.offerAvailabilityLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"Availability", @"")];
            cell.offerAvailability.text = [NSString stringWithFormat:@"%@ %@", _offerDictionary[@"availability"], NSLocalizedString(@"Seats", @"")];
            
            return cell;
        }
        else if((indexPath.row==5 && self.showWeekdays) ||
                (indexPath.row==5 && self.showTimerange) ||
                (indexPath.row==5 && self.showSpecial) ||
                (indexPath.row==6 && self.showTimerange && self.showWeekdays) ||
                (indexPath.row==6 && self.showTimerange && self.showSpecial) ||
                (indexPath.row==6 && self.showWeekdays && self.showSpecial) ||
                (indexPath.row==7 && self.showTimerange && self.showWeekdays && self.showSpecial)
                
                ){
            if(self.showWeekdays && indexPath.row==5){
                OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
                
                cell.offerAvailabilityLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"Weekdays", @"admin", @"")];
                
                NSArray *weekDayArray = @[
                                          NSLocalizedStringFromTable(@"Mon", @"admin", @""),
                                          NSLocalizedStringFromTable(@"Tue", @"admin", @""),
                                          NSLocalizedStringFromTable(@"Wed", @"admin", @""),
                                          NSLocalizedStringFromTable(@"Thu", @"admin", @""),
                                          NSLocalizedStringFromTable(@"Fri", @"admin", @""),
                                          NSLocalizedStringFromTable(@"Sat", @"admin", @""),
                                          NSLocalizedStringFromTable(@"Sun", @"admin", @""),
                                          ];
                
                NSMutableString *str = [@"" mutableCopy];
                NSString *separator = @"";
                
                for (int i=0;i<weekDayArray.count;i++){
                    if([self.offerDictionary[@"dynamic_content"][@"days"] containsObject:[NSString stringWithFormat:@"%i",i]] || [self.offerDictionary[@"dynamic_content"][@"days"] containsObject:@(i)]){
                        [str appendString:separator];
                        [str appendString:weekDayArray[i]];
                        
                        separator = @", ";
                    }
                }
                
                int startTime = [self.offerDictionary[@"starttime"] intValue];
                
                cell.offerAvailability.text = [str stringByAppendingString:startTime > 0 ? [NSString stringWithFormat:@" / %@ %@", NSLocalizedStringFromTable(@"Since", @"admin", @"") ,[miscFunctions localizedStringFromDate:[NSDate dateWithTimeIntervalSince1970:startTime] onlyDate:true]] : @""];
                
                return cell;
            }
            else if(self.showTimerange && (indexPath.row==5 || (indexPath.row==6 && self.showWeekdays))){
                OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
                
                cell.offerAvailabilityLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"Timerange", @"admin", @"")];
                
                cell.offerAvailability.text = [NSString stringWithFormat:@"%@ - %@", self.offerDictionary[@"dynamic_content"][@"timerange"][@"from"], self.offerDictionary[@"dynamic_content"][@"timerange"][@"to"]];
                
                return cell;
            }
            else {
                OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availabilityCell"];
                
                cell.offerAvailabilityLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"AR D'N'D Special", @"admin", @"")];
                
                cell.offerAvailability.text = self.offerDictionary[@"dynamic_content"][@"special"];
                
                return cell;
            }
        }
        else if(indexPath.row==4 || indexPath.row==5 || indexPath.row==6 || indexPath.row==7){
            OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell"];
            
            if (cell.tag != indexPath.row) {
                cell.tag = indexPath.row;
                
                NSString *location = @"";
                
                id address = _offerDictionary[@"offer_address"];
                
                if([address isKindOfClass:[NSDictionary class]]){
                    location = [NSString stringWithFormat:@"%@ %@, %@ %@", address[@"street"], address[@"street_number"], address[@"zip"], address[@"city"]];
                }
                else if([address isKindOfClass:[NSString class]]){
                    location = address;
                }
                
                NSLog(@"location: %@, address: %@", location, address);
                
                CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                [geocoder geocodeAddressString:location
                             completionHandler:^(NSArray* placemarks, NSError* error){
                                 if (placemarks && placemarks.count > 0) {
                                     CLPlacemark *topResult = [placemarks objectAtIndex:0];
                                     MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                                     
                                     MKCoordinateRegion region = cell.offerLocationMapView.region;
                                     region.center = placemark.coordinate;
                                     region.span.longitudeDelta = 0.005;
                                     region.span.latitudeDelta = 0.005;
                                     
                                     
                                     cell.offerLocationMapView.delegate = self;
                                     
                                     
                                     double radius = 200;
                                     MKCircle *circle = [MKCircle circleWithCenterCoordinate:placemark.coordinate radius:radius];
                                     [circle setTitle:@"background"];
                                     [cell.offerLocationMapView addOverlay:circle];
                                     
                                     
                                     [cell.offerLocationMapView setRegion:region animated:YES];
                                     //[cell.offerLocationMapView addSubview:view];
                                     //view.center = cell.offerLocationMapView.center;
                                     //[cell.offerLocationMapView addAnnotation:placemark];
                                 }
                             }
                 ];
            }
            
            return cell;
        }
        OfferDetailTableViewCell *cell = [self getInformationTableViewCell];
        cell.offerDescriptionLabel.height = [self getHeightForLabel:c.offerDescriptionLabel];
        c = cell;
        
        return c;
    }
    else {
        
    }
    
    if(indexPath.row==_inputArray.count){
        OfferDetailTableViewCell *cell = [self getSettingsConfirmedCell];
        cell.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
        c = cell;
    }
    if(indexPath.row==_inputArray.count+1){
        OfferDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"inputCell"];
        cell.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
        cell.offerInputTextField.tag=1999;
        cell.offerInputLabel.text = NSLocalizedStringFromTable(@"Message", @"admin", @"");
        cell.offerInputTextField.placeholder = NSLocalizedStringFromTable(@"Message to the host", @"admin", @"");
        cell.offerInputTextField.text = self.requestText;
        c = cell;
    }
    if(indexPath.row==_inputArray.count+2) {
        OfferDetailTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"SEND"];
        c.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
        c.sendButtonLabel.text = _offerViewController.acceptOfferButtonText.text;
        c.sendButtonLabel = _offerViewController.acceptOfferButtonText;
        if(self.offerViewController.disableAccept)
            c.sendButton.alpha = 0.5;
        else
            c.sendButton.alpha = 1;
        
        [c.sendButton addTarget:_offerViewController action:@selector(acceptOfferButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return c;
    }
    
    NSMutableDictionary *inputDict = (indexPath.row<_inputArray.count)?_inputArray[indexPath.row]:@{@"cellType":@"inputCell"};
    
    if([self inputDictShouldBePreFilled:inputDict]){
        c.offerInputTextField.userInteractionEnabled = false;
    }
    
    if(showRequiredFields){
        if([inputDict[@"required"] intValue]==1){
            c.offerInputLabel.textColor = [UIColor redColor];
        }
    }
    if([inputDict[@"cellType"] isEqualToString:@"dropdownCell"]){
        c.offerDropdownSelectedValueLabel.text = inputDict[@"options"][[inputDict[@"value"] intValue]];
    }
    
    if([inputDict[@"cellType"] isEqualToString:@"dateCell"]){
        c.offerDropdownSelectedValueLabel.text = [miscFunctions localizedStringFromDate:[NSDate dateWithTimeIntervalSince1970:[inputDict[@"value"] intValue]] onlyDate:YES];
        
        if(inputDict[@"value"] == nil){
            c.offerDropdownSelectedValueLabel.text = inputDict[@"placeholder"];
        }
    }
    
    return c;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section==1 && indexPath.row==_inputArray.count+1){
        OfferDetailTableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
        if(!c.offerInputTextField.isFirstResponder && c.offerInputTextField.placeholder!=nil){
            [c.offerInputTextField becomeFirstResponder];
            c.offerInputTextField.delegate = self;
            self.editing = YES;
        }
        else {
            [self.view endEditing:YES];
            self.editing = NO;
        }
        return;
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSLog(@"%@", _offerDictionary);
        
        ProfileStreamViewController* psvc = [ProfileStreamViewController load:@"ProfileStreamViewController" fromStoryboard:@"ProfileStreamViewController"];
        
        psvc.title = _offerDictionary[@"host_name"];
        
        psvc.filters = [NSString stringWithFormat:@"filters[if3_users.id]=%@", _offerDictionary[@"if3_user_id"]];
        
        psvc.profileId = [NSString stringWithFormat:@"%@",_offerDictionary[@"if3_user_id"]];
        
            NSString *hostImageString = _offerDictionary[@"host_image"];
            
            if (hostImageString != nil && !hostImageString.isEmptyString){
                NSLog(@"host image string: %@", hostImageString);
                UIImage *img = nil;
                if([hostImageString hasPrefix:@"http"]){
                    img = [imageMethods UIImageWithWebPath:hostImageString versionIdentifier:@""];
                }
                else {
                    img = [imageMethods UIImageWithServerPathName:hostImageString versionIdentifier:@"" width:52.0];
                }
                if(img != nil){
                    NSLog(@"image: %@", img);
                    psvc.userImage = img;
                }
            }
        
        [self.navigationController pushViewController:psvc animated:true];
        return;
    }
    
    if(_inputArray.count>indexPath.row){
        NSMutableDictionary *inputDict = _inputArray[indexPath.row];
        if([inputDict[@"cellType"] isEqualToString:@"dropdownCell"]){
            [self.view endEditing:YES];
            _pickerSelectedInputRow = indexPath.row;
            [self showDropdownPickerWithOptionsArray:inputDict[@"options"]];
            [self.view endEditing:YES];[self.view endEditing:YES];
            self.editing = NO;
            return;
        }
        
        if([inputDict[@"cellType"] isEqualToString:@"dateCell"]){
            [self.view endEditing:YES];
            _pickerSelectedInputRow = indexPath.row;
            [self showDatePicker];
            [self.view endEditing:YES];[self.view endEditing:YES];
            self.editing = NO;
            return;
        }
//
        if([inputDict[@"cellType"] isEqualToString:@"inputCell"]){
            OfferDetailTableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
            if(!c.offerInputTextField.isFirstResponder && c.offerInputTextField.placeholder!=nil){
                [c.offerInputTextField becomeFirstResponder];
                c.offerInputTextField.delegate = self;
                self.editing = YES;
            }
            else {
                [self.view endEditing:YES];
                self.editing = NO;
            }
        }
        
//        OfferDetailTableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
//        if([c isKindOfClass:[OfferDetailTableViewCell class]]){

//        }
//        else {
//            [self.view endEditing:YES];
//        }
    }
    else {
        [self.view endEditing:YES];
        self.editing = NO;
    }
}


#pragma mark scrollview delegate
-(void)scrollViewDidScroll:(UIScrollView*)scrollView {
    
}

#pragma mark other methods

-(void)assignFirstResponderToTextField:(UITextField*)textField{
    [textField becomeFirstResponder];
}

-(BOOL)inputDictShouldBePreFilled:(NSMutableDictionary*)inputDict{
    int inputType = [inputDict[@"field_type"] intValue];
    if(inputType==DYNAMICFIELD_EMAILINPUT){ // email
        return YES;
    }
    if(inputType==DYNAMICFIELD_NAME){ // name
        return YES;
    }
    if(inputType==DYNAMICFIELD_REGISTRATION){ // registration
        return YES;
    }
    return NO;
}

-(NSMutableDictionary*)preFillInputDict:(NSMutableDictionary*)inputDict{
    int inputType = [inputDict[@"field_type"] intValue];
    if(inputType==DYNAMICFIELD_EMAILINPUT){
        inputDict[@"value"]=[_dynamicUserSettingsStorage objectForKey:@"email"];
    }
    if(inputType==DYNAMICFIELD_NAME){
        inputDict[@"value"]=[[NSString stringWithFormat:@"%@ %@",[_dynamicUserSettingsStorage objectForKey:@"firstName"],[_dynamicUserSettingsStorage objectForKey:@"lastName"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    if(inputType==DYNAMICFIELD_REGISTRATION){
        NSDictionary *dynamicUserSettingsValues = [_dynamicUserSettingsStorage objectForKey:@"dynamicUserSettingsKeyValues"];
        
        inputDict[@"value"]=dynamicUserSettingsValues;
        
    }
    
    return inputDict;
}

-(float)getHeightForLabel:(UILabel*)label{
//    CGFloat width = label.frame.size.width;  //tableView width - left border width - accessory indicator - right border width
//    
//    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:label.text attributes:@{NSFontAttributeName: label.font}];
//    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
//                                               options:NSStringDrawingUsesLineFragmentOrigin
//                                               context:nil];
//    
//    CGSize size = rect.size;
//    size.height = ceilf(size.height);
//    size.width  = ceilf(size.width);
//
//    return size.height;
    
    [label sizeToFit];
    
    return label.height;
    
    //return 0.0f;
}

-(OfferDetailTableViewCell*)getInformationTableViewCell{
    OfferDetailTableViewCell* c = [self.tableView dequeueReusableCellWithIdentifier:@"informationCell"];
    c.offerTitleLabel.text = _offerDictionary[@"name"];
    c.offerDescriptionLabel.text = _offerDictionary[@"description"];
    c.offerPriceLabel.text = _offerDictionary[@"saving"];
    
    return c;
}

-(OfferDetailTableViewCell*)getSettingsConfirmedCell{
    OfferDetailTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:@"confirmSettingsCell"];
    c.offerSettingsConfirmedLabel.text = NSLocalizedString(@"Hereby I confirm, that my data are up to date.",@"");
    [c.offerSettingsConfirmedSwitch setOn:_confirmSettingsCheckboxChecked animated:YES];
    c.delegate = self;
    
    return c;
}

-(OfferDetailTableViewCell*)getDropdownCellForInputDict:(NSDictionary*)inputDict forIndexPath:(NSIndexPath*)indexPath{
    OfferDetailTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:@"dropdownCell"];
    c.offerInputLabel.text = inputDict[@"label"];
    c.offerDropdownSelectedValueLabel.text = inputDict[@"options"][[inputDict[@"value"] intValue]];
    c.delegate = self;
    
    return c;
}

-(OfferDetailTableViewCell*)getTextInputCellForIndexPath:(NSIndexPath*)indexPath{
    //@"inputCell"
    NSMutableDictionary *inputDict = (indexPath.row<_inputArray.count)?_inputArray[indexPath.row]:@{@"cellType":@"inputCell"};
    
    OfferDetailTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:inputDict[@"cellType"]];
    c.offerInputLabel.text = inputDict[@"label"];
    
    
    c.offerInputTextField.placeholder = inputDict[@"placeholder"];
    c.offerInputTextField.tag = indexPath.row;
    
    if(c.offerInputTextField.placeholder!=nil && ![c.offerInputTextField.placeholder isEqualToString:@""]){
        //c.offerInputTextField.delegate = self;
        c.delegate = self;
        NSLog(@"%@",c.offerInputTextField.delegate);
        
        //UIView *textInputOverlay = [[UIView alloc] initWithFrame:CGRectMake(
//                                                                            0,
//                                                                            0,
//                                                                            c.width,
//                                                                            c.height
//                                                                            )
//                                    ];
        //[c addSubview:textInputOverlay];
    }
    else c.offerInputTextField.userInteractionEnabled = NO;
    
    
    return c;
}

-(void)highlightRequiredFields{
    showRequiredFields = YES;
    [_tableView reloadRowsAtIndexPaths:_tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationFade];
}

-(void)sendButtonPressed{
    NSLog(@"send button pressed");
    
    NSLog(@"currently existing information: %@", _inputArray);
}

-(void)switchSwitched:(BOOL)selected{
    [_offerViewController changeSettingsConfirmationStatus:selected acceptText:_offerDictionary[@"accept_button_text"]];
    _confirmSettingsCheckboxChecked = selected;
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:_inputArray.count+2 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
}

-(UIImage*)loadImageForItem:(NSDictionary*)item useFor:(NSString*)usage atPath:(NSString*)path width:(float)width{
    
    NSString *fileName = [[path lastPathComponent] stringByDeletingPathExtension];
    
    imageMethods *images = [[imageMethods alloc] init];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@-%@-%@",fileName, item[@"tstamp"],item[@"uid"],usage];
    
    UIImage *cellImage = nil;
    NSString *structureImageSm = path;
    if(![structureImageSm isEqualToString:@""] && structureImageSm != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else {
            //Get Image From URL
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm,width*2];
            
            UIImage *imageFromURL = [images getImageFromURL:urlString];
            
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
            return cellImage;
        }
        
    }
    
    return cellImage;
}


#pragma mark textfield delegate methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(textField.tag==1999){
        self.requestText = newString;
        return YES;
    }
    
    _inputArray[textField.tag][@"value"] = newString;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    self.editing = NO;
    return NO;
}


#pragma mark pickerView delegate & datasource methods

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView==_dropdownPicker)return _dropdownArray.count;
    return 0;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30.0f;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView==_dropdownPicker)return _dropdownArray[row];
    return @"";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView==_dropdownPicker)_dropdownSelectedRow = (int)row;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


#pragma mark general picker methods

-(void)hidePickerView{
    [UIView animateWithDuration:0.5f animations:^(void){
        _pickerView.frame = CGRectMake(0, self.view.height, self.view.width, 0);
    } completion:^(BOOL finished){
        [_pickerView removeFromSuperview];
    }];
}


#pragma mark dropdown picker methods

-(void)showDropdownPickerWithOptionsArray:(NSArray*)optionsArray{
    _dropdownSelectedRow = 0;
    _dropdownArray = [optionsArray mutableCopy];
    
    // init dropdownView
    float dropdownViewHeight = 225;
    _pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height, self.view.width, dropdownViewHeight)];
    
    
    
    // add background first of all
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *pickerviewBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [pickerviewBackground setFrame:CGRectMake(0, 0, _pickerView.width, _pickerView.height)];
    
    [_pickerView addSubview:pickerviewBackground];
    
    
    
    // init picker for dropdown and add it to our view
    float dropdownHeight = 225-44;
    _dropdownPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, _pickerView.width, dropdownHeight)];
    
    _dropdownPicker.dataSource = self;
    _dropdownPicker.delegate = self;
    [_dropdownPicker reloadAllComponents];
    
    [_pickerView addSubview:_dropdownPicker];
    
    
    
    // add toolbar with flexible space and "Done"-Button
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,_dropdownPicker.width,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dropdownPickerDoneButtonClicked)];
    
    toolBar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    [_pickerView addSubview:toolBar];
    


    
    // add view to our controllers view and animate
    [self.view addSubview:_pickerView];
    [UIView animateWithDuration:0.5 animations:^(void){
        _pickerView.frame = CGRectMake(0, self.view.height-dropdownViewHeight-60, self.view.width, dropdownViewHeight);
    }];
}

-(void)dropdownPickerDoneButtonClicked{
    _inputArray[_pickerSelectedInputRow][@"value"] = [NSNumber numberWithInt:_dropdownSelectedRow];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_pickerSelectedInputRow inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    [self hidePickerView];
}


#pragma mark date picker methods

-(void)showDatePicker{
    
    // init dropdownView
    float dropdownViewHeight = 225;
    _pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height, self.view.width, dropdownViewHeight)];
    
    
    // add background first of all
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *pickerviewBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [pickerviewBackground setFrame:CGRectMake(0, 0, _pickerView.width, _pickerView.height)];
    
    [_pickerView addSubview:pickerviewBackground];
    
    
    // init picker for dropdown and add it to our view
    float dropdownHeight = 225-44;
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, _pickerView.width, dropdownHeight)];
    [_datePicker setDate:[NSDate dateWithTimeIntervalSinceNow:0]];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    if([_inputArray[_pickerSelectedInputRow][@"options"] rangeOfString:@"show_time"].location != NSNotFound){
        _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
    
    [_pickerView addSubview:_datePicker];
    
    
    // add toolbar with flexible space and "Done"-Button
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,_datePicker.width,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDoneButtonClicked)];
    
    toolBar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    [_pickerView addSubview:toolBar];
    
    
    // add view to our controllers view and animate
    [self.view addSubview:_pickerView];
    [UIView animateWithDuration:0.5 animations:^(void){
        _pickerView.frame = CGRectMake(0, self.view.height-dropdownViewHeight-60, self.view.width, dropdownViewHeight);
    }];
}

-(void)datePickerDoneButtonClicked{
    _inputArray[_pickerSelectedInputRow][@"value"] = [NSNumber numberWithDouble:_datePicker.date.timeIntervalSince1970];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_pickerSelectedInputRow inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    [self hidePickerView];
}

@end
