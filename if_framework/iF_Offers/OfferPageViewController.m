//
//  OfferPageViewController.m
//  if_framework
//
//  Created by User on 12/1/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "OfferPageViewController.h"

@interface OfferPageViewController ()

@end

@implementation OfferPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setViewControllers:(NSArray<UIViewController *> *)viewControllers direction:(UIPageViewControllerNavigationDirection)direction animated:(BOOL)animated completion:(void (^)(BOOL))completion{
    NSLog(@"updating page view controllers");
    [super setViewControllers:viewControllers direction:direction animated:animated completion:completion];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
