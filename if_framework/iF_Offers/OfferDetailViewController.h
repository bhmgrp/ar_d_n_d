//
//  OfferDetailViewController.h
//  if_framework
//
//  Created by User on 10/23/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferViewController.h"
#import "OfferDetailTableViewCell.h"

@interface OfferDetailViewController : UIViewController <
    UITableViewDataSource,
    UITableViewDelegate,
    UITextFieldDelegate,
    OfferDetailTableViewCellDelegate,
    UIPickerViewDelegate,
    UIPickerViewDataSource
>


@property (nonatomic) localStorage *dynamicUserSettingsStorage;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *headerWrapper;
@property (weak, nonatomic) IBOutlet UIImageView *headerBackgroundImageView;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubtitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerIconWrapper;


@property (weak, nonatomic) IBOutlet UIImageView *headerIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerIconHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerIconWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerSubtitleBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerIconWrapperTopSpace;

@property (nonatomic) NSMutableDictionary *offerDictionary;
@property (nonatomic) NSMutableArray *inputArray;


@property (nonatomic) NSUInteger pickerSelectedInputRow;

@property (nonatomic) UIView *pickerView;

@property (nonatomic) UIDatePicker *datePicker;

@property (nonatomic) UIPickerView *dropdownPicker;
@property (nonatomic) NSMutableArray *dropdownArray;
@property (nonatomic) int dropdownSelectedRow;


@property (nonatomic) BOOL confirmSettingsCheckboxChecked;
@property (nonatomic) BOOL userIsLoggedIn;

@property (strong,nonatomic) OfferViewController *offerViewController;

@property (nonatomic) NSString* requestText;

// needed for paging logic
@property (nonatomic) NSUInteger pageIndex;

-(void)sendButtonPressed;

-(void)initImages;

-(void)highlightRequiredFields;

-(void)initInputArray;

@end
