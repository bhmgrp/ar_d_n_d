//
//  OfferSliderViewController.h
//  if_framework
//
//  Created by User on 10/23/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferPageViewController.h"

@interface OfferViewController : UIViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate,NSURLConnectionDelegate,UIAlertViewDelegate, UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentSeparationView;

@property (strong, nonatomic) OfferPageViewController *pageViewController;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;

@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorHeader;
@property (weak, nonatomic) IBOutlet UILabel *errorDescription;

@property (nonatomic) NSUInteger placeID;
@property (nonatomic) NSUInteger offerCampaignID;
@property (nonatomic) NSUInteger userID;
@property (nonatomic) NSUInteger offerID;
@property (nonatomic) NSUInteger pID;

@property (nonatomic) BOOL disableAccept;

@property (nonatomic) NSMutableData *downloadedData;
@property (nonatomic) NSMutableArray *offerData;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *acceptOfferButtonVisualEffectView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceptOfferButtonVisualEffectViewBottomSpace;
@property (weak, nonatomic) IBOutlet UIView *acceptOfferButtonView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceptOfferButtonViewBottomSpace;
@property (weak, nonatomic) IBOutlet UILabel *acceptOfferButtonText;
@property (weak, nonatomic) IBOutlet UIButton *acceptOfferButton;
- (IBAction)acceptOfferButtonPressed:(id)sender;

@property (nonatomic) BOOL settingsConfirmed;
@property (nonatomic) BOOL userIsLoggedIn;

-(void)changeSettingsConfirmationStatus:(BOOL)status;
-(void)changeSettingsConfirmationStatus:(BOOL)status acceptText:(NSString*)acceptText;

@property (nonatomic) NSUInteger indexToOpen;

#pragma mark arrows for navigation

@property (weak, nonatomic) IBOutlet UIImageView *leftArrowImageView;
- (IBAction)leftArrowClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImageView;
- (IBAction)rightArrowClicked:(id)sender;


@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *arrowVerticalConstraints;


-(void)loadData;
-(void)loadDataAndForceDownload:(BOOL)forceDownload;



@end
