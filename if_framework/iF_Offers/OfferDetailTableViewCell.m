//
//  OfferDetailTableViewCell.m
//  if_framework
//
//  Created by User on 10/26/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "OfferDetailTableViewCell.h"

@implementation OfferDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    //self.offerInputTextField.delegate = self.delegate;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)switchSwitched:(UISwitch *)sender {
    if([self.delegate respondsToSelector:@selector(switchSwitched:)]){
        [self.delegate switchSwitched:sender.on];
    }
}

//
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
//    return YES;
//}
//
//-(BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return NO;
//}


@end
