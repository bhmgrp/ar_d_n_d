//
//  OfferDetailTableViewCell.h
//  if_framework
//
//  Created by User on 10/26/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapKit/MapKit.h"

@protocol OfferDetailTableViewCellDelegate <NSObject,UITextFieldDelegate>

-(void)switchSwitched:(BOOL)selected;

@end

@interface OfferDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *offerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerAvailabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerAvailability;

@property (weak, nonatomic) IBOutlet MKMapView *offerLocationMapView;

@property (weak, nonatomic) IBOutlet UIImageView *hostImage;


@property (weak, nonatomic) IBOutlet UILabel *offerInputLabel;
@property (strong, nonatomic) IBOutlet UITextField *offerInputTextField;

@property (weak, nonatomic) IBOutlet UILabel *offerDropdownSelectedValueLabel;


@property (weak, nonatomic) IBOutlet UISwitch *offerSettingsConfirmedSwitch;
@property (weak, nonatomic) IBOutlet UILabel *offerSettingsConfirmedLabel;

@property (strong, nonatomic) id<OfferDetailTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *overlayView;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UILabel *sendButtonLabel;

- (IBAction)switchSwitched:(UISwitch *)sender;

@end
