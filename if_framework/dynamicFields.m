//
//  dynamicFields.m
//  if_framework
//
//  Created by User on 11/27/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "dynamicFields.h"

@implementation dynamicFields

+(int)getFieldNumberForTypeString:(NSString*)fieldTypeString{
    NSDictionary* dynamicFieldTypes = [dynamicFields getDynamicFieldTypeNumbersForStrings];
    if(dynamicFieldTypes[fieldTypeString]!=nil){
        return [dynamicFieldTypes[fieldTypeString] intValue];
    }
    
    return 0;
}



+(NSString*)getFieldStringForTypeNumber:(int)fieldTypeNumber{
    switch (fieldTypeNumber) {
        case  0: return @"DYNAMICFIELD_TEXT";
        case  1: return @"DYNAMICFIELD_DROPDOWN";
        case  2: return @"DYNAMICFIELD_CHECKBOX";
        case  3: return @"DYNAMICFIELD_CALLBACKCHECKBOX";
        case  4: return @"DYNAMICFIELD_EMAILINPUT";
        case  5: return @"DYNAMICFIELD_HTML";
        case  6: return @"DYNAMICFIELD_NEWSLETTERCHECKBOX";
        case  7: return @"DYNAMICFIELD_LOTTERYCHECKBOX";
        case  8: return @"DYNAMICFIELD_TEXTAREA";
        case  9: return @"DYNAMICFIELD_STARRATING";
        case 10: return @"DYNAMICFIELD_COMBINEDCHECKBOXES";
        case 11: return @"DYNAMICFIELD_PHOTOUPLOAD";
        case 12: return @"DYNAMICFIELD_PAGE";
        case 13: return @"DYNAMICFIELD_NAME";
        case 14: return @"DYNAMICFIELD_REGISTRATION";
        case 15: return @"DYNAMICFIELD_DATE";
        case 16: return @"DYNAMICFIELD_APP_DIVIDER";
        case 17: return @"DYNAMICFIELD_APP_IMAGE";
        case 18: return @"DYNAMICFIELD_APP_PROFILE_IMAGE";
        case 19: return @"DYNAMICFIELD_APP_SETTINGS_CONFIRMED";
        default: return @"DYNAMICFIELD_TEXT";
    }
}

+(NSDictionary*)getDynamicFieldTypeNumbersForStrings{
    return @{
             @"DYNAMICFIELD_TEXT":@0,
             @"DYNAMICFIELD_DROPDOWN":@1,
             @"DYNAMICFIELD_CHECKBOX":@2,
             @"DYNAMICFIELD_CALLBACKCHECKBOX":@3,
             @"DYNAMICFIELD_EMAILINPUT":@4,
             @"DYNAMICFIELD_HTML":@5,
             @"DYNAMICFIELD_NEWSLETTERCHECKBOX":@6,
             @"DYNAMICFIELD_LOTTERYCHECKBOX":@7,
             @"DYNAMICFIELD_TEXTAREA":@8,
             @"DYNAMICFIELD_STARRATING":@9,
             @"DYNAMICFIELD_COMBINEDCHECKBOXES":@10,
             @"DYNAMICFIELD_PHOTOUPLOAD":@11,
             @"DYNAMICFIELD_PAGE":@12,
             @"DYNAMICFIELD_NAME":@13,
             @"DYNAMICFIELD_REGISTRATION":@14,
             @"DYNAMICFIELD_DATE":@15,
             @"DYNAMICFIELD_APP_DIVIDER":@16,
             @"DYNAMICFIELD_APP_IMAGE":@17,
             @"DYNAMICFIELD_APP_PROFILE_IMAGE":@18,
             @"DYNAMICFIELD_APP_SETTINGS_CONFIRMED":@19,
             };
}

@end
