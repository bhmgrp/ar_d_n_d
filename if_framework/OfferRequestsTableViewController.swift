//
//  OfferREquestsTableViewController.swift
//  if_framework
//
//  Created by Christopher on 10/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit
import StoreKit

class OfferRequestsTableViewController: AdminViewController, UITabBarDelegate {
    
    // MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRequestsView: UIView!
    @IBOutlet weak var sadSmiley: UILabel!
    
    
    var tabBar:CWTabBar = CWTabBar()
    
    func getBar() -> CWTabBar {
        return CWTabBar(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.height ?? 0) + UIApplication.shared.statusBarFrame.size.height, width: UIScreen.main.bounds.size.width, height: 49))
    }
    
    // MARK: variables
    var requests:[[String:Any]] = []
    
    
    // MARK: functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar = self.getBar()
        self.title = "Requests".localized
        self.initStyle()
        self.refreshControl.addTarget(self, action: #selector(loadUpdates), for: .valueChanged)
        
        self.initTabs()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadUpdates()
    }
    
    func initTabs() {
        let myRequestsItem = UITabBarItem(title: "My requests".localized, image: nil, tag: 0)
        myRequestsItem.titlePositionAdjustment = UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))
        myRequestsItem.setTitleTextAttributes([NSAttributedStringKey.font:UIFont(name:"HelveticaNeue", size:16.0)!], for: .normal)
        
        
        let requestsItem = UITabBarItem(title: "Requests".localized, image: nil, tag: 1)
        requestsItem.titlePositionAdjustment = UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))
        requestsItem.setTitleTextAttributes([NSAttributedStringKey.font:UIFont(name:"HelveticaNeue", size:16.0)!], for: .normal)
        
        
        self.tabBar.items = [
            myRequestsItem,
            requestsItem
        ]
        
        self.tabBar.delegate = self
        
        if UserDefaults.standard.bool(forKey: "hostMode") {
            //self.tabBar.setSelected(item:self.tabBar.items[1])
            self.tabBar.selectedItem = self.tabBar.items![1]
            self.tabBar.highLightLine.x = (UIScreen.main.bounds.size.width / CGFloat(2)) * CGFloat(1)
        }
        else {
            //self.tabBar.setSelected(item:self.tabBar.items[0])
            self.tabBar.selectedItem = self.tabBar.items![0]
        }
            
        self.view.addSubview(self.tabBar)
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 0 && UserDefaults.standard.bool(forKey: "hostMode") {
            UserDefaults.standard.set(false, forKey: "hostMode")
            self.loadUpdates()
        }
        
        if item.tag == 1 && !UserDefaults.standard.bool(forKey: "hostMode") {
            UserDefaults.standard.set(true, forKey: "hostMode")
            self.loadUpdates()
        }
        
        UIView.animate(withDuration: 0.5) {
            self.tabBar.highLightLine.x = (UIScreen.main.bounds.size.width / CGFloat(self.tabBar.items?.count ?? 1)) * CGFloat(item.tag)
        }
    }
    
    func initStyle() {
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsetsMake(-11, 0, 0, 0)
        self.refreshControl = UIRefreshControl()
        self.tableView.tableHeaderView = self.refreshControl
    }
    
    @objc func loadUpdates() {
        self.showLoading()
        DispatchQueue.global().async {
            var url = SECURED_API_URL + "offer/requests/\(UserDefaults.standard.integer(forKey: "loggedInUserID"))"
            if UserDefaults.standard.bool(forKey: "hostMode") {
                url = SECURED_API_URL + "offer/my-requests/\(UserDefaults.standard.integer(forKey: "loggedInUserID"))"
            }
            
            let response = API.getFrom(url)
            if response is [[String:Any]]{
                self.requests = response as! [[String:Any]]
                DispatchQueue.main.async {
                    if self.requests.count == 0 {
                        self.noRequestsView.alpha = 1
                        self.noRequestsView.isHidden = false
                        self.noRequestsView.isUserInteractionEnabled = false
                        self.sadSmiley.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
                    }
                    else {
                        self.noRequestsView.alpha = 0
                        self.noRequestsView.isHidden = true
                        self.noRequestsView.isUserInteractionEnabled = false
                    }
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    self.endLoading()
                }
            }
            else {
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                }
            }
        }
        
    }
    
    func acccessorySelected(_ accessory: UIView) {
        
    }
    
    func guestSelectedAcceptedRequest(indexPath:IndexPath) {
        let buyedRequest = (localStorage(filename: "buyedOfferRequests").object(forKey: "buyed\(self.requests[indexPath.row]["id"] ?? "NONE")") as? Bool ?? false) || self.requests[indexPath.row]["status"] as? String ?? "0" == "4"
        
        var attributedMessage = NSAttributedString(string: "In order to take part in this event, you need to pay a deposit of 2.99€ of the full price for this offer, to make sure that you will take part.".localized, attributes: [
            NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 16)!,
            NSAttributedStringKey.foregroundColor : UIColor.darkGray
            ])
        
        if buyedRequest {
            attributedMessage = NSAttributedString(string: String(format:"You already payed the deposit for this request.\n\nIt will take place at the following location:\n%@".localized, ((self.requests[indexPath.row]["offer"] as? [String:Any] ?? [:])["address"] as? [String:String] ?? [:])["formated_address"] ?? ""), attributes: [
                NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 16)!,
                NSAttributedStringKey.foregroundColor : UIColor.darkGray
                ])
        }
        
        let alert = UIAlertController(title: "Take part now!".localized, message: "", preferredStyle: .actionSheet)
        
        alert.setValue(attributedMessage, forKey: "attributedMessage")
        
        if let popPresentationController : UIPopoverPresentationController = alert.popoverPresentationController {
            let cell = self.tableView.cellForRow(at: indexPath)
            popPresentationController.sourceView = cell
            popPresentationController.sourceRect = CGRect(x: (cell?.width)! - 80, y: (cell?.height)!/2, width: 1, height: 1)
        }
        
        alert.addAction(UIAlertAction(title: "View offer again".localized, style: .default, handler: {(action: UIAlertAction) -> Void in
            
            let ovc = OfferViewController.loadNow(fromStoryboard: "OfferViewController") as! OfferViewController
            ovc.offerID = UInt((self.requests as [[String:Any]])[indexPath.row]["uid"] as! String)!
            ovc.disableAccept = true
            ovc.title = (self.requests as [[String:Any]])[indexPath.row]["name"] as? String
            self.navigationController?.pushViewController(ovc, animated: true)
        
        }))
        
        if buyedRequest {
            alert.addAction(UIAlertAction(title: "Chat with host".localized, style: .default, handler: {(action: UIAlertAction) -> Void in
                
                if let offer = self.requests[indexPath.row]["offer"] as? [String:Any]? ?? [:] {
                    let offerUserID = "\(offer["if3_user_id"] as? String ?? "")"
                    let chatController = UserChatViewController()
                    chatController.to = Int(offerUserID)!
                    chatController.view.frame = UIScreen.main.bounds
                    chatController.title = self.requests[indexPath.row]["offerer_name"] as? String ?? "Chat".localized
                    self.navigationController?.pushViewController(chatController, animated: true)
                }
                
            }))
        }
        else {
            alert.addAction(UIAlertAction(title: "Pay deposit".localized, style: .default, handler: {(action: UIAlertAction) -> Void in
                self.purchase(requestId: self.requests[indexPath.row]["id"] as? String ?? "0", indexPath: indexPath)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .destructive, handler: {(action: UIAlertAction) -> Void in
        
        }))
        
        self.present(alert, animated: true, completion: {() -> Void in
        
        })
        
    }
    
    func purchase(requestId:String, indexPath:IndexPath){
        //CWAlert.showNotification(withTitle: "Buyed (SIMULATED)", message: nil, onViewController: self);
        self.showLoading()
        
        let purchaser = InAppPurchaseManager(title: "Loading".localized, message: nil, preferredStyle: .alert)
        
        self.present(purchaser, animated: true) {
            purchaser.unlockRequest(withProductIdentifer: "com.bhmms.ar_dnd.deposit2",
                                    purchased: {self.endLoading();self.markAsBuyd(requestId: requestId, indexPath: indexPath)},
                          purchaseFailed: {self.endLoading()})
        }
    }
    
    func markAsBuyd(requestId:String, indexPath: IndexPath){
        localStorage(filename: "buyedOfferRequests").setObject(true, forKey: "buyed\(requestId)" as NSCopying!)
        
        self.showLoading()
        DispatchQueue.global().async {
            let response = API.post(
                toURL: "\(SECURED_API_URL)offer/requests",
                withPost: [
                    "update": true,
                    "offer_id": self.requests[indexPath.row]["offer_id"]!,
                    "user_id": (UserDefaults.standard.integer(forKey: "loggedInUserID")),
                    "request_id": requestId,
                    "set": ["status": 4]
                ]) as! [String:Any]
            switch response["success"] {
            case (1 as Int), (true as Bool), ("1" as String):
                DispatchQueue.main.async {
                    self.requests[indexPath.row]["status"] = "4"
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                    self.endLoading()
                }
                break
            default:
                switch response["error"] {
                case is String:
                    DispatchQueue.main.async {
                        CWAlert.showNotification(withTitle: "Error".localized, message: response["error"] as? String, onViewController: self)
                    }
                default:
                    DispatchQueue.main.async {
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        self.endLoading()
                    }
                }
            }
        }

    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell") as! AdminTableViewCell
        cell.label1?.text = requests[indexPath.row]["name"] as? String
        
        cell.label2?.textColor = UIColor.darkGray
        
        cell.imageView1.isHidden = true
        
        var statusString:String? = nil
        switch (self.requests as [[String:Any]])[indexPath.row]["status"] {
        case 0 as Int, "0" as String:
            statusString = "Requested".localized
            break
        case 1 as Int, "1" as String:
            statusString = "Accepted".localized
            cell.label2?.textColor = UIColor.green
            if(localStorage(filename: "buyedOfferRequests").object(forKey: "buyed\(self.requests[indexPath.row]["id"] ?? "NONE")") as? Bool ?? false) {
                cell.imageView1.isHidden = false
                cell.imageView1.image = imageMethods.resize(UIImage(named: "iosicons/ios7-chatbubble-outline.png"), for: CGSize(width: 36, height: 36))
            }
            break
        case 2 as Int, "2" as String:
            statusString = "Denied".localized
            cell.label2?.textColor = UIColor.red
            break
        case 4 as Int, "4" as String:
            statusString = "Deposit payed".localized
            cell.imageView1.isHidden = false
            cell.imageView1.image = imageMethods.resize(UIImage(named: "iosicons/ios7-chatbubble-outline.png"), for: CGSize(width: 36, height: 36))
            cell.label2?.textColor = UIColor.blue
            break
        default:
            break
        }
        
        cell.label2?.text = statusString
        cell.label3?.text! = "\("Date".localized): \(requests[indexPath.row]["create_time"]!)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if UserDefaults.standard.bool(forKey: "hostMode") {
            if let statusString = requests[indexPath.row]["status"] as? String {
                if Int(statusString) == 0 {
                    weak var ordvc = OfferRequestsDetailViewController.load("OfferRequestsDetailViewController", fromStoryboard: "OfferRequestsDetailViewController") as? OfferRequestsDetailViewController
                    ordvc?.request = requests[indexPath.row]
                    ordvc?.offer = ["name": unwrap(object: requests[indexPath.row]["name"], defaultValue: "")]
                    ordvc?.completed = {() -> Void in
                        _ = ordvc?.navigationController?.popViewController(animated: true)
                    }
                    self.navigationController?.pushViewController(ordvc!, animated: true)
                }
                if Int(statusString) == 1 || Int(statusString) == 4{
                    if let requesterId = self.requests[indexPath.row]["user_id"] as? String? ?? "0" {
                        let offerUserID = "\(requesterId)"
                        let chatController = UserChatViewController()
                        chatController.to = Int(offerUserID)!
                        chatController.view.frame = UIScreen.main.bounds
                        chatController.title = self.requests[indexPath.row]["requester_name"] as? String ?? "Chat".localized
                        self.navigationController?.pushViewController(chatController, animated: true)
                    }
                }
                return
            }
        }
        
        if let statusString = requests[indexPath.row]["status"] as? String {
            if Int(statusString) == 1 || Int(statusString) == 4 {
                self.guestSelectedAcceptedRequest(indexPath: indexPath)
                return
            }
        }
        
        let ovc = OfferViewController.loadNow(fromStoryboard: "OfferViewController") as! OfferViewController
        ovc.offerID = UInt((self.requests as [[String:Any]])[indexPath.row]["uid"] as! String)!
        ovc.disableAccept = true
        ovc.title = (self.requests as [[String:Any]])[indexPath.row]["name"] as? String
        self.navigationController?.pushViewController(ovc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions:[UITableViewRowAction] = []
        
        if !UserDefaults.standard.bool(forKey: "hostMode") {
            let deleteAction = UITableViewRowAction(style: .destructive, title: "Revoke".localized, handler: {(action: UITableViewRowAction, indexPath: IndexPath) -> Void in
                self.showLoading()
                DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                    var response = API.post(
                        toURL: "\(SECURED_API_URL)offer/requests",
                        withPost: ["update": true,
                                   "offer_id": self.requests[indexPath.row]["offer_id"]!,
                                   "user_id": (UserDefaults.standard.integer(forKey: "loggedInUserID")),
                                   "request_id": self.requests[indexPath.row]["id"]!,
                                   "set": ["status": 3]
                        ]) as! [String:Any]
                    switch response["success"] {
                    case (1 as Int), (true as Bool), ("1" as String):
                        DispatchQueue.main.async {
                            self.requests.remove(at: indexPath.row)
                            self.tableView.reloadData()
                            self.endLoading()
                        }
                        break
                    default:
                        switch response["error"] {
                        case is String:
                            DispatchQueue.main.async {
                                CWAlert.showNotification(withTitle: "Error".localized, message: response["error"] as? String, onViewController: self)
                            }
                        default:
                            DispatchQueue.main.async {
                                self.tableView.reloadRows(at: [indexPath], with: .fade)
                                self.endLoading()
                            }
                        }
                    }
                })
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            })
            actions.append(deleteAction)
            if Int(requests[indexPath.row]["status"] as! String) == 2 {
                deleteAction.title = "Delete".localized
            }
            if Int(requests[indexPath.row]["status"] as! String) == 1 {
                let acceptAction = UITableViewRowAction(style: .default, title: "Take part".localized, handler: {(action: UITableViewRowAction, indexPath: IndexPath) -> Void in
                    self.tableView.reloadRows(at: [indexPath], with: .right)
                    self.tableView(tableView, didSelectRowAt: indexPath)
                })
                acceptAction.backgroundColor = UIColor.green
                actions.append(acceptAction)
            }
        }
        else {
            // decline the request
            let deleteAction = UITableViewRowAction(style: .destructive, title: "Decline".localized, handler: {(action: UITableViewRowAction, indexPath: IndexPath) -> Void in
                self.showLoading()
                
                DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                    let response = API.post(
                        toURL: "\(SECURED_API_URL)offer/my-requests",
                        withPost: [
                            "update": true,
                            "offer_id": self.requests[indexPath.row]["offer_id"]!,
                            "user_id": (UserDefaults.standard.integer(forKey: "loggedInUserID")),
                            "request_id": self.requests[indexPath.row]["id"]!,
                            "set": ["status": 2]
                        ]) as! [String:Any]
                    switch response["success"] {
                    case (1 as Int), (true as Bool), ("1" as String):
                        DispatchQueue.main.async {
                            self.requests.remove(at: indexPath.row)
                            self.tableView.reloadData()
                            self.endLoading()
                        }
                        break
                    default:
                        switch response["error"] {
                        case is String:
                            DispatchQueue.main.async {
                                CWAlert.showNotification(withTitle: "Error".localized, message: response["error"] as? String, onViewController: self)
                            }
                        default:
                            DispatchQueue.main.async {
                                self.tableView.reloadRows(at: [indexPath], with: .fade)
                                self.endLoading()
                            }
                        }
                    }
                })
            })
            if(Int(requests[indexPath.row]["status"] as! String) == 1){
                deleteAction.title = "Delete".localized
            }
            actions.append(deleteAction)
            if(Int(requests[indexPath.row]["status"] as! String) != 1 && Int(requests[indexPath.row]["status"] as! String) != 4){
                // accept the request
                let acceptAction = UITableViewRowAction(style: .default, title: "Accept".localized, handler: {(action: UITableViewRowAction, indexPath: IndexPath) -> Void in
                    DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                        let response = API.post(
                            toURL: "\(SECURED_API_URL)offer/my-requests",
                            withPost: [
                                "update": true,
                                "offer_id": self.requests[indexPath.row]["offer_id"]!,
                                "user_id": (UserDefaults.standard.integer(forKey: "loggedInUserID")),
                                "request_id": self.requests[indexPath.row]["id"]!,
                                "set": ["status": 1]
                            ]) as! [String:Any]
                        switch response["success"] {
                        case (1 as Int), (true as Bool), ("1" as String):
                            DispatchQueue.main.async {
                                self.loadUpdates()
                            }
                            break
                        default:
                            switch response["error"] {
                            case is String:
                                DispatchQueue.main.async {
                                    CWAlert.showNotification(withTitle: "Error".localized, message: response["error"] as? String, onViewController: self)
                                }
                            default:
                                DispatchQueue.main.async {
                                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                                    self.endLoading()
                                }
                            }
                        }
                        
                    })
                })
                acceptAction.backgroundColor = UIColor.green
                actions.append(acceptAction)
            }
            
        }
        return actions
    }
}
