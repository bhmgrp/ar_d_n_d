//
//  WebViewController.m
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "WebViewController.h"
#import "SSKeychain.h"

@interface WebViewController ()

@property BOOL didLoad;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewBottomSpace;

@property (nonatomic) NSMutableURLRequest *request;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewTopConstraint;
@property (nonatomic) BOOL  shouldNotUseInset;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation WebViewController

+(WebViewController*)getWebViewController{
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    
    return wvc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self loadCookies];
    
//    if(![globals sharedInstance].webProcessPool)
//        [globals sharedInstance].webProcessPool = [WKProcessPool new];
//    
//    
//    WKWebViewConfiguration *config = [WKWebViewConfiguration new];
//    config.processPool = [globals sharedInstance].webProcessPool;
//    
//    _webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:config];
    //_webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    //_webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+64, self.view.frame.size.width, self.view.frame.size.height);
    
    
    if(!self.urlGiven){
        self.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]; //if it's not given use
    }
    
    //prepare request for the webView
    _request = [NSMutableURLRequest requestWithURL:self.url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:30.0];
    
    if (_request.URL) {
        NSDictionary *cookies = [NSHTTPCookie requestHeaderFieldsWithCookies:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:_request.URL]];
        if ([cookies objectForKey:@"Cookie"]) {
            NSMutableURLRequest *mutableRequest = _request.mutableCopy;
            [mutableRequest addValue:cookies[@"Cookie"] forHTTPHeaderField:@"Cookie"];
            _request = mutableRequest;
        }
    }
    
    NSString *validDomain = _request.URL.host;
    const BOOL requestIsSecure = [_request.URL.scheme isEqualToString:@"https"];
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        // Don't even bother with values containing a `'`
        if ([cookie.name rangeOfString:@"'"].location != NSNotFound) {
            NSLog(@"Skipping %@ because it contains a '", cookie.properties);
            continue;
        }
        
        // Is the cookie for current domain?
        if (![cookie.domain hasSuffix:validDomain]) {
            NSLog(@"Skipping %@ (because not %@)", cookie.properties, validDomain);
            continue;
        }
        
        // Are we secure only?
        if (cookie.secure && !requestIsSecure) {
            NSLog(@"Skipping %@ (because %@ not secure)", cookie.properties, _request.URL.absoluteString);
            continue;
        }
        
        NSString *value = [NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value];
        [array addObject:value];
    }
    
    NSString *header = [array componentsJoinedByString:@";"];
    [_request setValue:header forHTTPHeaderField:@"Cookie"];
    
    NSMutableString *script = [NSMutableString new];
    
    // Get the currently set cookie names in javascriptland
    [script appendString:@"var cookieNames = document.cookie.split('; ').map(function(cookie) { return cookie.split('=')[0] } );\n"];
    
    for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        // Skip cookies that will break our script
        if ([cookie.value rangeOfString:@"'"].location != NSNotFound) {
            continue;
        }
        
        // Create a line that appends this cookie to the web view's document's cookies
        [script appendFormat:@"if (cookieNames.indexOf('%@') == -1) { document.cookie='%@'; };\n", cookie.name, cookie.wn_javascriptString];
    }
    
    WKUserContentController *userContentController = [[WKUserContentController alloc] init];
    WKUserScript *cookieInScript = [[WKUserScript alloc] initWithSource:script
                                                          injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                       forMainFrameOnly:NO];
    [userContentController addUserScript:cookieInScript];
    
    // Create a config out of that userContentController and specify it when we create our web view.
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.userContentController = userContentController;
    
    self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:config];
    self.webView.navigationDelegate = self;
    self.webView.hidden = YES;
    if (!self.shouldNotUseInset)
        self.webView.scrollView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    
    WKUserScript *cookieOutScript = [[WKUserScript alloc] initWithSource:@"window.webkit.messageHandlers.updateCookies.postMessage(document.cookie);"
                                                           injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                        forMainFrameOnly:NO];
    [userContentController addUserScript:cookieOutScript];
    
    [userContentController addScriptMessageHandler:self
                                              name:@"updateCookies"];
    
    
    [self.view addSubview:_webView];
    if(self.useHTML){
        // workaround but the only possible solution:
        // wrapping the content in a span, this sets a "default" font, if nothing is given in the html
        // we dont have access to modify the default font of the webkit
        _htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@;\">%@</span>",
                       @"Helvetica",
                       _htmlString];
        [self.webView loadHTMLString:self.htmlString baseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]];
    } else if(self.url!=nil){
        //load the request
        
        NSLog(@"self.url: %@", self.url);
        
        [self.webView loadRequest:_request];
        NSLog(@"%@",self.url);
        
        self.isLoading = YES;
    }
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSLog(@"message.body: %@", message.body);
    NSArray<NSString *> *cookies = [message.body componentsSeparatedByString:@"; "];
    for (NSString *cookie in cookies) {
        // Get this cookie's name and value
        NSArray<NSString *> *comps = [cookie componentsSeparatedByString:@"="];
        if (comps.count < 2) {
            continue;
        }
        
        // Get the cookie in shared storage with that name
        NSHTTPCookie *localCookie = nil;
        for (NSHTTPCookie *c in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:self.webView.URL]) {
            if ([c.name isEqualToString:comps[0]]) {
                localCookie = c;
                break;
            }
        }
        
        // If there is a cookie with a stale value, update it now.
        if (localCookie) {
            NSMutableDictionary *props = [localCookie.properties mutableCopy];
            props[NSHTTPCookieValue] = comps[1];
            NSHTTPCookie *updatedCookie = [NSHTTPCookie cookieWithProperties:props];
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:updatedCookie];
        }
        
        else {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties:@{NSHTTPCookieDomain:_request.URL.host,NSHTTPCookieName:comps[0],NSHTTPCookieValue:comps[1]}]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSLog(@"bottom bar space: %f",  self.view.height - self.tabBarController.tabBar.y);
    
    _webViewBottomSpace.constant = -49+(self.view.height - self.tabBarController.tabBar.y);
    
    NSLog(@"self.webView.height: %f", self.webView.height);
}

- (void)setUrl:(NSURL *)url {
    _url = url;
    self.urlGiven = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSLog(@" web title 1 => %@ ", self.titleString);
    NSLog(@" web => %@ ", self.navigationItem.titleView);
    
    self.title = self.titleString;
    //if(self.navigationItem.titleView == nil) {
    //    self.navigationItem.titleView = [NavigationTitle createNavTitle:self.titleString SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    //}
}

- (void)setContentOffsetToZero {
    self.webViewTopConstraint.constant = 0;
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.shouldNotUseInset = true;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
//    if([self.url.absoluteString isEqualToString:@"https://ifbck.com/admin/login"]){
//        NSString *password = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#loginform-password').val()"];
//        NSString *username = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#loginform-username').val()"];
//        
//        if(username != nil && ![username isEmptyString]){
//            [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"adminLoginUsername"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            if(password != nil && ![password isEmptyString]){
//                [SSKeychain setPassword:password forService:@"adminLogin" account:username];
//            }
//        }
//        
//    }
    
    return YES;
}


-(void)webViewTouchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}


- (void) webViewDidStartLoad:(UIWebView *)webView{
    
    self.isLoading = YES;
    
    [miscFunctions showLoadingAnimationForView:self.view];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    self.isLoading = NO;
    
//    self.currentURL = self.webView.request.URL.absoluteString;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
//    
//    [self.webView setBackgroundColor:[UIColor whiteColor]];
//    [self.webView setOpaque:YES];
//    
//    if([self.url.absoluteString isEqualToString:@"https://ifbck.com/admin/login"]){
//        NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"adminLoginUsername"];
//        NSString *password = [SSKeychain passwordForService:@"adminLogin" account:username];
//        
//        NSString *jquery;
//        
//        NSLog(@"username: %@, password: %@", username, password);
//        
//        if(username!=nil && ![username isEmptyString]){
//            jquery = [NSString stringWithFormat:@"$('#loginform-username').val('%@');",username];
//            [self.webView stringByEvaluatingJavaScriptFromString:jquery];
//            
//            if(password != nil && ![password isEmptyString]){
//                jquery = [NSString stringWithFormat:@"$('#loginform-password').val('%@');",password];
//                [self.webView stringByEvaluatingJavaScriptFromString:jquery];
//            }
//        }
//        
//        
//    }
    
    _didLoad = YES;
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if(error.code==NSURLErrorCancelled){
        return;
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertView *webError = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error",@"") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles: nil];
    
    [webError show];
    
    NSLog(@"%@", error);
}

- (void)hideModalWebView{
    if(self.navigationController!=nil){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)saveCookies {
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:cookiesData forKey:[NSString stringWithFormat:@"cookiesForUrl%@",self.url.absoluteString]];
    [defaults synchronize];
}

- (void)loadCookies {
    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"cookiesForUrl%@",self.url.absoluteString]]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in cookies) {
        [cookieStorage setCookie: cookie];
    }
}

#pragma mark - wkwebview navigation delegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self.activityIndicator stopAnimating];
    self.webView.hidden = NO;
}

@end

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return NO;
}

@end

@implementation NSHTTPCookie(jsstring)

- (NSString *)wn_javascriptString {
    NSString *string = [NSString stringWithFormat:@"%@=%@;domain=%@;path=%@",
                        self.name,
                        self.value,
                        self.domain,
                        self.path ?: @"/"];
    
    if (self.secure) {
        string = [string stringByAppendingString:@";secure=true"];
    }
    
    return string;
}

@end
