//
//  UIViewController+TabBarTintColor.m
//  if_framework
//
//  Created by Christopher on 10/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "UIViewController+TabBarTintColor.h"
#import <objc/runtime.h>

@interface UIViewController ()

@end

static void const *key;

@implementation UIViewController (TabBarTintColor)


//-(void)swizzled_viewDidAppear:(BOOL)animated
//{
//    [self swizzled_viewDidAppear:animated];
//        
//    if(self.tintColor == nil){
//        self.tintColor = [UIColor blackColor];
//    }
//    
//    [UITabBar appearance].tintColor = self.tintColor;
//}
//
//+ (void)load
//{
//    if(self != UIViewController.self){
//        return;
//    }
//    
//    Method original, swizzled;
//    
//    original = class_getInstanceMethod(self, @selector(viewDidAppear:));
//    swizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidAppear:));
//    
//    method_exchangeImplementations(original, swizzled);
//    
//}

- (UIColor*)tintColor {
    return objc_getAssociatedObject(self, key);
}

- (void)setTintColor:(UIColor*)value {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_RETAIN);
}

@end
