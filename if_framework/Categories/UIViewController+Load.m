//
// Created by Nicolas Tichy on 12/1/13.
// Copyright (c) 2014 Cassida Custom Solutions. All rights reserved.
//

#import "UIViewController+Load.h"
//#import "BaseViewController.h"

@implementation UIViewController (Load)

+(id)load:(NSString*)storyboardId fromStoryboard:(NSString*)storyboard
{
    UIStoryboard* sb = [UIStoryboard storyboardWithName:storyboard bundle:nil];
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:storyboardId];
    return vc;
}

/*+(id)loadFromBaseViewController
{
    [BaseViewController setConcreteSubclassToInstantiate:[self class]];
    UIStoryboard* sb = [UIStoryboard storyboardWithName:@"BaseViewController" bundle:nil];
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:@"BaseViewController"];
    return vc;
}
*/
+(id)load:(NSString*)storyboardId
{
    UIStoryboard* sb = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:storyboardId];
    return vc;
}

+(id)loadNow
{
    UIStoryboard* sb = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];

    return vc;
}

+(id)loadNowFromStoryboard:(NSString*)storyboard
{
    UIStoryboard* sb = [UIStoryboard storyboardWithName:storyboard bundle:nil];
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];

    return vc;
}

+(id)loadView
{
    return ((UIViewController *)[self loadNow]).view;
}


//-(UIViewController*)topViewController
//{
//    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
//}

+(UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    }
    else if ([rootViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    }
    else if (rootViewController.presentedViewController)
    {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    }

    return rootViewController;
}

-(void)hideView:(UIView *)view1 showView:(UIView *)view2
{
    [UIView animateWithDuration:0.3f animations:^
    {
        view1.alpha = 0.0f;
    }
    completion:^(BOOL finished)
    {
        [UIView animateWithDuration:0.3f animations:^
        {
            view2.alpha = 1.0f;
        }
        completion:nil];
    }];
}

-(void)updateTabBarTintColor{
    // prototype
}

-(void)setSidebarButton{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
}

@end