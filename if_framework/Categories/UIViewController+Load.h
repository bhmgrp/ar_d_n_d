//
// Created by Nicolas Tichy on 12/1/13.
// Copyright (c) 2014 Cassida Custom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController (Load)

+(id)load:(NSString*)storyboardId fromStoryboard:(NSString*)storyboard;
+(id)load:(NSString*)storyboardId;

+(id)loadNow;
+(id)loadView;
//-(UIViewController*)topViewController;
+(UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController;

-(void)hideView:(UIView *)view1 showView:(UIView *)view2;
+(id)loadNowFromStoryboard:(NSString*)storyboard;
//+(id)loadFromBaseViewController;
-(void)updateTabBarTintColor;

-(void)setSidebarButton;
@end