//
//  NSDictionary+URLEncodedString.m
//  if_framework_terminal
//
//  Created by User on 4/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "NSDictionary+URLEncodedString.h"

@implementation NSDictionary (URLEncodedString)

-(NSString*)urlEncodedString{
    __block NSString *str = @"";
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSArray class]]){
            obj = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:obj options:0 error:nil] encoding:NSUTF8StringEncoding];
        }
        str = [str stringByAppendingString:[NSString stringWithFormat:@"&%@=%@",key,obj]];
    }];
    
    return str;
}

@end
