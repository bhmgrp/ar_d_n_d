//
// Created by Nicolas Tichy on 11/15/13.
// Copyright (c) 2014 Cassida Custom Solutions. All rights reserved.
//

#import "UIColor+Hex.h"

@implementation UIColor (Hex)

+(UIColor *)colorWithRGBHex:(UInt32)hex
{
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;

    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0f];
}

+(UIColor *)colorWithRGBHex:(UInt32)hex withAlpha:(CGFloat)alpha
{
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;

    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:alpha];
}

- (int64_t)colorCode
{
    CGFloat red, green, blue;
    if ([self getRed:&red green:&green blue:&blue alpha:NULL])
    {
        int64_t redInt = (int64_t)(red * 255 + 0.5);
        int64_t greenInt = (int64_t)(green * 255 + 0.5);
        int64_t blueInt = (int64_t)(blue * 255 + 0.5);

        return (redInt << 16) | (greenInt << 8) | blueInt;
    }

    return 0;
}

+(UIColor*)colorFromInt64:(int64_t)val
{
    long tempLong = (long)((val >> 32) << 32); //shift it right then left 32 bits, which zeroes the lower half of the long
    int yourInt = (int)(val - tempLong);
    return [UIColor colorWithRGBHex:(UInt32)yourInt];
}

+ (UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if(cleanString.length == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if(cleanString.length == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

-(BOOL)colorIsDark{
    {
        const CGFloat *componentColors = CGColorGetComponents(self.CGColor);
        
        CGFloat colorBrightness = ((componentColors[0] * 299) + (componentColors[1] * 587) + (componentColors[2] * 114)) / 1000;
        if (colorBrightness < 0.5) {
            return YES;
        }
        else {
            return NO;
        }
    }
}

-(BOOL)colorIsLight{
    return ![self colorIsDark];
}

+ (NSString *)hexStringForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"#%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}

- (NSString *)hexString{
    return [UIColor hexStringForColor:self];
}

@end
