//
//  UIViewController+TabBarTintColor.h
//  if_framework
//
//  Created by Christopher on 10/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TabBarTintColor)

@property (nonatomic) UIColor *tintColor;

@end
