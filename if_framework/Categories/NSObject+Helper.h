//
//  NSObject+IsEmptyString.h
//  if_framework
//
//  Created by User on 3/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Helper)

-(BOOL) isEmptyString;

-(BOOL) isEmptyObject;

@property (NS_NONATOMIC_IOSONLY, getter=isString, readonly) BOOL string;

@end
