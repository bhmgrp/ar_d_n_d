//
// Created by Nicolas Tichy on 11/14/13.
// Copyright (c) 2014 Cassida Custom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Solid)
+ (UIImage *)squareImageWithColor:(UIColor *)color dimension:(int)dimension;
+ (UIImage *)rectImageWithColor:(UIColor *)color withSize:(CGSize)size;
@end

