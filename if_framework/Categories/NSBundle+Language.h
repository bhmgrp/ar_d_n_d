@interface NSBundle (Language)
+(void)setLanguage:(NSString*)language;
@end