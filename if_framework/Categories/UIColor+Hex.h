//
// Created by Nicolas Tichy on 11/15/13.
// Copyright (c) 2014 Cassida Custom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (Hex)

+(UIColor *)colorWithRGBHex:(UInt32)hex;
+(UIColor *)colorWithRGBHex:(UInt32)hex withAlpha:(CGFloat)alpha;

@property (NS_NONATOMIC_IOSONLY, readonly) int64_t colorCode;
+(UIColor*)colorFromInt64:(int64_t)val;

+(UIColor *) colorFromHexString:(NSString *)hexString;

+(NSString *)hexStringForColor:(UIColor *)color;

-(NSString *)hexString;

-(BOOL)colorIsDark;

-(BOOL)colorIsLight;

@end
