//
//  LoggedUserCredentials.swift
//  gestgid
//
//  Created by Vadym Patalakh on 8/20/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class LoggedUserCredentials: NSObject {
    static let sharedInstance = LoggedUserCredentials()
    
    var userId: String = "0"
    var userImage: UIImage = UIImage(named: "emptyProfileImage")!
    var userName: String = ""
    
    fileprivate override init() {
        super.init()
        updateInfo()
    }
    
    func updateInfo() {
        self.userId = "\(UserDefaults.standard.integer(forKey: "loggedInUserID"))"
        
        guard let dynamicStorage = localStorage(filename: "dynamicUserSettingsValues") else {
            return
        }
        
        if let firstName = dynamicStorage.getObjectForKey("firstName") as? String {
            if let lastName = dynamicStorage.getObjectForKey("lastName") as? String {
                self.userName = firstName + lastName
            }
        }
        
        if let imageName = dynamicStorage.getObjectForKey("profileImageName") as? String {
            if let imagePath = dynamicStorage.getObjectForKey("profileImagePath") as? String {
                let imageUrlPath = imagePath + imageName
                DispatchQueue.global().async {
                    var userImage:UIImage?
                    
                    if imageUrlPath.hasPrefix("http") {
                        userImage = imageMethods.uiImage(withWebPath: imageUrlPath)
                    } else {
                        userImage = imageMethods.uiImage(withServerPathName: imageUrlPath, width:30)
                    }
                    
                    DispatchQueue.main.async {
                        if userImage != nil {
                            self.userImage = userImage!
                        }
                    }
                }
            }
        }
    }
    
    func hasLoggedUser() -> Bool {
        if userId != "0" {
            return true
        }
        
        return false
    }
    
    func userHasRights(userId: String) -> Bool {
        if self.isModerator() || userId == self.userId {
            return true
        }
        
        return false
    }
    
    func isModerator() -> Bool {
        return UserDefaults.standard.bool(forKey: "hostModeActivated") ? UserDefaults.standard.bool(forKey: "hostModeActivated") : false
    }
}
