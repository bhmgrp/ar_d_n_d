//
//  NavigationViewController.m
//  pickalbatros
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "NavigationViewController.h"
#import "SWRevealViewController.h"

@interface NavigationViewController (){
    SWRevealViewController *swrvc;
}

@property (nonatomic) NSMutableArray *navigationBarBackgroundViews;

@end

@implementation NavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self updateNavigationBarStyle];
    
    //[self.view addGestureRecognizer:[[SWRevealViewController alloc] init].panGestureRecognizer];
    
    _navigationBarBackgroundViews = [@[] mutableCopy];
}

- (UIView*)getNavitationBarBackground{

//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIView *navigationBarBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    UIView *navigationBarBackground = [[UIView alloc] init];
    navigationBarBackground.backgroundColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];
    [navigationBarBackground setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationBar.height)];
    
    navigationBarBackground.clipsToBounds = true;
    // navigation bar border
    UIView *bar2 = [[UIView alloc] init];
    [bar2 setBackgroundColor:[UIColor grayColor]];
    [bar2 setFrame:CGRectMake(0, [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationBar.height - 1, [UIScreen mainScreen].bounds.size.width, 0.5)];
    
    [navigationBarBackground addSubview:bar2];
    
    return navigationBarBackground;
}

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    NavigationViewController *nvc = [super initWithRootViewController:rootViewController];
    
    UIView *navigationBarBackground = [self getNavitationBarBackground];
    
    [rootViewController.view addSubview:navigationBarBackground];
    
    [self.navigationBarBackgroundViews addObject:navigationBarBackground];
    
    return nvc;
}

-(void)deleteSettings{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedPlaceID"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateNavigationBarStyle{
    
    UIColor *backGroundColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];
    UIColor *textColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
    
    NSString *bgColorString = [[NSUserDefaults standardUserDefaults] valueForKey:@"placeBackgroundColor"];
    if(bgColorString != nil && ![bgColorString isEqualToString:@""]){
        backGroundColor = [self.view getColorFromHexString:bgColorString];
    }
    
    NSString *textColorString = [[NSUserDefaults standardUserDefaults] valueForKey:@"placeTextColor"];
    if(textColorString != nil && ![textColorString isEqualToString:@""]){
        textColor = [self.view getColorFromHexString:textColorString];
    }
    
    //NSString *bgColorString = [[NSUserDefaults standardUserDefaults] valueForKey:@"placeBackgroundColor"];
    
    
    //navigation Bar style
    [[UINavigationBar appearance]setBarTintColor:backGroundColor];
    
//    self.navigationBar.barStyle = UIBarStyleBlack;
    [self setNeedsStatusBarAppearanceUpdate];
    [self.navigationBar setBackgroundColor:backGroundColor];
    [self.navigationBar setTintColor:textColor];
    
    [self.navigationBar setTitleTextAttributes: @{
                                                  NSForegroundColorAttributeName: textColor,
                                                  NSFontAttributeName: [UIFont fontWithName:@"Arial" size:17.0f]
                                                  }];
    [self.navigationBar setTranslucent:NO];
    
    // blurry effect on navigation bar
    
    //self.navigationBar.barStyle = UIBarStyleDefault;
    
    self.navigationBar.translucent = YES;
    
    [self.navigationBar setShadowImage:[[UIImage alloc]init]];
//    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//
    [self.navigationBar setBackgroundColor:[UIColor clearColor]];
    //[self.navigationBar setTintColor:[UIColor blackColor]];
    
    
    /*
    */
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{

    
    [_navigationBarBackgroundViews addObject:[self getNavitationBarBackground]];
    
    if(self.viewControllers.count>0 && viewController.view.tag != 1){
        viewController.view.tag = 1;
        [viewController.view addSubview:[self getNavitationBarBackground]];
    }
    
    //[self.view insertSubview:bluredEffectView belowSubview:self.navigationBar];
    [super pushViewController:viewController animated:animated];
    
    swrvc = (SWRevealViewController*)[UIApplication sharedApplication].keyWindow.rootViewController;
    if([swrvc respondsToSelector:@selector(panGestureRecognizer)]){
        [viewController.view addGestureRecognizer:swrvc.panGestureRecognizer];
    }
}

-(void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated{
    [super setNavigationBarHidden:hidden animated:animated];
    [self hideNavigationBarBackground:hidden animated:animated];
}

-(void)hideNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated{
    if(hidden){
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, 64);
                }];
            }
            else {
                navigationBarBackground.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, 64);
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
                }];
            }
            else {
                navigationBarBackground.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
            }
        }
    }
}


-(void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated{
    if(hidden){
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 0.0;
                }];
            }
            else {
                navigationBarBackground.alpha = 0.0;
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 1.0;
                }];
            }
            else {
                navigationBarBackground.alpha = 1.0;
            }
        }
    }
}

-(void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated finished:(void(^)(void))finished{
    void(^finishedfinishedFunction)() = finished;
    
    if(hidden){
        NSLog(@"hide navigation background");
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 0.0;
                } completion:^(BOOL finished){
                    if(finished){
                        finishedfinishedFunction();
                    }
                }];
            }
            else {
                navigationBarBackground.alpha = 0.0;
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 1.0;
                } completion:^(BOOL finished){
                    finishedfinishedFunction();
                }];
            }
            else {
                navigationBarBackground.alpha = 1.0;
                [self setNavigationBarHidden:NO animated:NO];
            }
        }
    }
}

- (void)addNavigationBarBackgroundToViewController:(UIViewController*)viewController {
    [viewController.view addSubview:[self getNavitationBarBackground]];
}

@end
