//
//  MiscFunctions.h
//  if_framework
//
//  Created by User on 02.07.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface miscFunctions : NSObject

+(void)showLoadingAnimationForView:(UIView*)view;

+(NSString*)stringByParsingHtmlString:(NSString*)s;

+(NSString*)localizedStringFromDate:(NSDate*)myNSDateInstance onlyDate:(BOOL)onlyDate;

+(NSString*)localizedStringFromDate:(NSDate *)myNSDateInstance onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime;

+(NSString*)localizedStringFromDate:(NSDate *)myNSDateInstance :(BOOL)onlyDate :(BOOL)onlyTime :(BOOL)onlyMonthAndYear;

+(NSString*)enStringFromDate:(NSDate *)myNSDateInstance onlyDate:(BOOL)onlyDate;

+(NSString*)enStringFromDate:(NSDate *)myNSDateInstance onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime;

+(NSDate*)dateFromLocalizedString:(NSString*)string onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime;

+(NSDate*)dateFromEnString:(NSString*)string onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime;

@end
