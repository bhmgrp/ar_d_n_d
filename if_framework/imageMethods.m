//
//  ImageMethods.m
//  pickalbatros
//
//  Created by User on 10.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "imageMethods.h"

@interface imageMethods()

@property (nonatomic) NSMutableData *uploadingImageData;

@property (nonatomic) uploadCompleted doneUploadingImage;

@end

@implementation imageMethods

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    return result;
}

- (UIImage *)getImageFromLocalFolder:(NSString *)imageName {
    UIImage *cellImage = nil;
    
    imageMethods *images = [[imageMethods alloc] init];
    
    imageName = imageName.lastPathComponent.stringByDeletingPathExtension;
    
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    // if png image exists:
    if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageName]]]){
        //Load Image From Directory
        cellImage = [images loadImage:imageName ofType:@"png" inDirectory:documentsPath];
    }
    // if jpg image exists:
    else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]]]){
        //Load Image From Directory
        cellImage = [images loadImage:imageName ofType:@"jpg" inDirectory:documentsPath];
    }
    
    return cellImage;
}

-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    if ([extension.lowercaseString isEqualToString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    } else if ([extension.lowercaseString isEqualToString:@"jpg"] || [extension.lowercaseString isEqualToString:@"jpeg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
    } else {
        NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
    }
}

-(UIImage *) loadImage:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.%@", directoryPath, fileName, extension]];
    
    return result;
}

+(UIImage*)UIImageWithWebPath:(NSString*)name{
    return [self UIImageWithWebPath:name versionIdentifier:@"" width:@""];
}

+(UIImage*)UIImageWithWebPath:(NSString*)name versionIdentifier:(NSString*)version{
    return [self UIImageWithWebPath:name versionIdentifier:version width:@""];
}

+(UIImage*)UIImageWithWebPath:(NSString*)name versionIdentifier:(NSString*)version width:(NSString*)width{
    if(version==nil || [version isEqualToString:@""]){
        version = @"";
    }
    
    NSString *fileName = [name.lastPathComponent.stringByDeletingPathExtension stringByAppendingString:version];
    
    imageMethods *images = [[self alloc] init];
    
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageNameFromTstamp = fileName;
    
    UIImage *cellImage = nil;
    if(![name isEqualToString:@""] && name != nil){
        
        // if png image exists:
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            return cellImage;
        }
        // if jpg image exists:
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            return cellImage;
        }
        else {
            //Get Image From URL
            NSString *urlString = [NSString stringWithFormat:@"%@%@",name,width];
            
            UIImage *imageFromURL = [images getImageFromURL:urlString];
            
            NSString *imageType = @"";
            if([name hasSuffix:@".png"] ||
               [name hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([name hasSuffix:@".jpg"] ||
                    [name hasSuffix:@".jpeg"] ||
                    [name hasSuffix:@".JPEG"] ||
                    [name hasSuffix:@".JPG"]){
                imageType = @"jpg";
            } else {
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            
            return cellImage;
        }
        
    }
    
    return cellImage;
}

+(UIImage*)UIImageWithServerPathName:(NSString*)name versionIdentifier:(NSString*)version width:(float)width{
    UIImage *cellImage = [self UIImageWithWebPath:[NSString stringWithFormat:@"%@/%@",SYSTEM_DOMAIN,name] versionIdentifier:version width:[NSString stringWithFormat:@"?w=%f", width]];
    
    return cellImage;
}

+(UIImage*)UIImageWithServerPathName:(NSString*)name width:(float)width{
    return [self UIImageWithServerPathName:name versionIdentifier:@"" width:width];
}

+(void)deleteAllImages{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
        if(
           [file hasSuffix:@"png"] ||
           [file hasSuffix:@"PNG"] ||
           [file hasSuffix:@"jpg"] ||
           [file hasSuffix:@"JPG"] ||
           [file hasSuffix:@"JPEG"] ||
           [file hasSuffix:@"jpeg"]){
            NSLog(@"deleting: %@", file);
            BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
            if (!success || error) {
                // it failed.
            }
        }
    }
}

-(void)uploadImage:(UIImage*)image withParameters:(NSDictionary *)parameters completed:(uploadCompleted)completed {
    _doneUploadingImage = completed;
    [self uploadImage:image withParameters:parameters];
}

-(void)uploadImage:(UIImage*)image withParameters:(NSDictionary*)parameters{
    NSURL *uploadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@image_upload.php",SECURED_API_URL]];
    [self uploadFile:image forField:[NSString stringWithFormat:@"file"] URL:uploadURL parameters:parameters];
}

-(void)uploadImage:(UIImage*)image{
    [self uploadImage:image withParameters:nil];
}

- (void)uploadFile:(UIImage *)image
          forField:(NSString *)fieldName
               URL:(NSURL*)url
        parameters:(NSDictionary *)parameters{

    NSString *filename = @"profile_image.jpg";
    NSData *imageData = UIImageJPEGRepresentation(image, 85);

    NSMutableData *httpBody = [NSMutableData data];
    NSString *boundary = [self generateBoundaryString];
    NSString *mimetype = [self contentTypeForImageData:imageData];

    // configure the request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];

    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];

    // add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];

    // add image data
    if (imageData) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:imageData];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }

    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];

    // setting the body of the post to the reqeust
    [request setHTTPBody:httpBody];

    [NSURLConnection connectionWithRequest:request delegate:self];
}

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    float uploadingProgress = ((float)totalBytesWritten / totalBytesExpectedToWrite);

    NSLog(@" im - upload progress: %f",uploadingProgress);

    if([self.uploadDelegate respondsToSelector:@selector(uploadingWithProgress:)])
        [self.uploadDelegate uploadingWithProgress:uploadingProgress];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    _uploadingImageData = [[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_uploadingImageData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSError *error;
    NSDictionary *jsonData = [NSJSONSerialization
                              JSONObjectWithData:_uploadingImageData
                              options:NSJSONReadingMutableContainers
                              error:&error];

    if([self.uploadDelegate respondsToSelector:@selector(uploadFinishedWithResponse:)])
        [self.uploadDelegate uploadFinishedWithResponse:jsonData];

    if(_doneUploadingImage != nil){
        _doneUploadingImage(jsonData);
    }
    
    NSLog(@" im - response: %@", [[NSString alloc] initWithData:_uploadingImageData encoding:NSUTF8StringEncoding]);
}

- (NSString *)generateBoundaryString{
    // generate boundary string
    //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections

    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}
- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];

    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}


+ (UIImage *)resizeImage:(UIImage *)image forSize:(CGSize)size{
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
