//
//  CWDatePicker.swift
//  if_framework
//
//  Created by Christopher on 10/10/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class CWDatePicker: UIView {
    
    @objc   var selected:(Date) -> Void = {date in}
    @objc   var updated:(Date) -> Void = {date in}
    @objc   var date:Date = Date(timeIntervalSinceNow: 0)
    @objc   var datePicker:UIDatePicker = UIDatePicker()
    @objc   var toolBar:UIToolbar? = nil
    @objc    var timeSwitchView:UIView? = nil
    @objc   var onlyTime:Bool = false {
        didSet {
            if self.onlyTime {
                self.datePicker.datePickerMode = .time
            }
            else {
                self.datePicker.datePickerMode = .date
            }
        }
    }
    @objc    var showTime:Bool = false {
        didSet {
            if self.showTime {
                self.datePicker.datePickerMode = .dateAndTime
            }
            else {
                self.datePicker.datePickerMode = .date
            }
        }
    }
    
    @objc    var fromView:UIView? = nil
    
    @objc    init(withDate: Date, fromView: UIView?){
        super.init(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height / 2))
        
        self.initWithDate(date: withDate)
        
        self.datePicker.frame = CGRect(x: 0, y: 44, width: self.width, height: self.height-44)
        
        self.fromView = fromView
    }
    
    @objc    init(withDate: Date){
        super.init(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height / 2))
        
        self.initWithDate(date: withDate)
    }
    
    @objc    func initWithDate(date: Date){
        
        datePicker.date = date
        datePicker.addTarget(self, action: #selector(changedDate(datePicker:)), for: .valueChanged)
        
        toolBar = UIToolbar(frame: CGRect(x: 8, y: 0, width: self.width-16, height: 44))
        
        toolBar?.items = [
            UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePressed))
        ]
        
        toolBar?.barStyle = .default
        toolBar?.backgroundColor = UIColor.clear
        toolBar?.setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default)
        toolBar?.setShadowImage(UIImage(), forToolbarPosition: .any)
        
        let effect = UIBlurEffect(style: .light) as UIVisualEffect
        let effectView = UIVisualEffectView(effect: effect)
        effectView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        
        
        self.backgroundColor = UIColor.clear
        
        let bar = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: 0.5))
        bar.backgroundColor = UIColor.gray
        
        self.addSubview(effectView)
        self.addSubview(toolBar!)
        self.addSubview(datePicker)
        self.addSubview(bar)
        
        self.showTimeSwitcher()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc    func show(){
        if(self.fromView != nil){
            show(on: self.fromView!)
        }
        else {
            show(on: UIApplication.shared.keyWindow!)
        }
    }
    
    @objc    func show(on: UIView){
        on.addSubview(self)
        
        UIView.animate(withDuration: 0.3) {
            self.y = UIScreen.main.bounds.size.height / 2;
        }
    }
    
    @objc    func showTimeSwitcher(){
        if(self.timeSwitchView == nil) {
            self.timeSwitchView = UIView(frame: CGRect(x: 0, y: 44, width: self.width, height: 44))
            
            self.addSubview(timeSwitchView!)
            
            
            // switcher
            let timeSwitch = UISwitch()
            timeSwitch.isOn = true
            timeSwitch.addTarget(self, action: #selector(timeSwitched), for: .valueChanged)
            timeSwitch.center = CGPoint(x: 0, y: timeSwitchView!.height / 2)
            timeSwitch.x = (timeSwitchView?.width)! - 8 - timeSwitch.width
            
            timeSwitchView?.addSubview(timeSwitch)
            
            
            // label
            let label = UILabel()
            label.text = "Time".localized
            label.width = self.width - 24 - timeSwitch.width
            label.height = (timeSwitchView?.height)!
            label.center = CGPoint(x: 0, y: timeSwitchView!.height / 2)
            label.x = 8
            label.textAlignment = .right
            
            timeSwitchView?.addSubview(label)
            
        }
        self.timeSwitchView?.isHidden = false
        
        self.datePicker.y = (timeSwitchView?.y)! + (timeSwitchView?.height)!
        self.datePicker.height = (self.datePicker.height) - (timeSwitchView?.height)!
    }
    
    @objc   func hideTimeSwitcher(){
        self.timeSwitchView?.isHidden = true
        
        self.datePicker.y = 44
        self.datePicker.height = self.height - 44
    }
    
    @objc    func hideToolBar(){
        self.toolBar?.isHidden = true
        self.datePicker.y = self.datePicker.y - 49
        self.datePicker.height = self.datePicker.height + 49
    }
    
    @objc func cancelPressed(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.y = UIScreen.main.bounds.size.height;
            
        }) { (completed) in
            
            if completed {
                self.removeFromSuperview()
            }
        }
    }
    
    @objc func donePressed(){
        UIView.animate(withDuration: 0.3, animations: {
            
            self.y = UIScreen.main.bounds.size.height;
            
        }) { (completed) in
            
            if completed {
                self.removeFromSuperview()
                
                self.date = (self.datePicker.date)
                self.selected(self.date)
            }
        }
    }
    
    @objc func changedDate(datePicker:UIDatePicker){
        self.updated(datePicker.date)
    }
    
    @objc func timeSwitched(switcher:UISwitch){
        if(switcher.isOn){
            self.datePicker.datePickerMode = .dateAndTime
        }
        else {
            self.datePicker.datePickerMode = .date
        }
    }
}

