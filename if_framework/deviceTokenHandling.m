//
//  deviceTokenHandling.m
//  Freese Gruppe
//
//  Created by User on 04.12.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import "deviceTokenHandling.h"
#import "AppDelegate.h"

@implementation deviceTokenHandling

// update token and set user id
+(void)updateDeviceToken:(NSString*) token forUserID:(long) userID{
    
    
    //NSLog(@"token update called");
    
    // get a new token from the app delegate
    if([token isEqualToString:@""]){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *updatedToken = [NSString stringWithFormat:@"%@", appDelegate.deviceToken];
        
        token = updatedToken;
    }
    
    //format token
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    
    //NSLog(@"token: %@", token);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        long success = 0;
        
        NSString *post =[[NSString alloc] initWithFormat:@"token=%@&userID=%li", token, userID];
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@device_token_update.php?jsonUpdate&customerID=%i",SECURED_API_URL, CUSTOMERID]];
        
        NSLog(@"URL => %@", url);
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSError *error;
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        //NSLog(@"Response code: %ld", (long)[response statusCode]);
        
        if ([response statusCode] >= 200 && [response statusCode] < 300)
        {
            NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
            NSLog(@"Response (deviceTokenHandling) ==> %@", responseData);
            
            NSError *error = nil;
            NSDictionary *jsonData = [NSJSONSerialization
                                      JSONObjectWithData:urlData
                                      options:NSJSONReadingMutableContainers
                                      error:&error];
            
            success = [jsonData[@"success"] integerValue];
            NSLog(@"%li", success);
            NSLog(@"Success: %ld",(long)success);NSLog(@"error: %@", error);
            
            if(success == 1){
                NSLog(@"token sucessfully updated");
            } else {
                NSLog(@"something went wrong");
            }
            
        } else {
            NSLog(@"connection failed");
        }
        
        
    });

}

+(void)userLogoutForToken:(NSString*) token forUserID:(long) userID{
    long success = 0;
    
    // get a new token from the app delegate
    if([token isEqualToString:@""]){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *updatedToken = [NSString stringWithFormat:@"%@", appDelegate.deviceToken];
        
        token = updatedToken;
    }
    
    //format token
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    
    NSString *post =[[NSString alloc] initWithFormat:@"token=%@&userID=%li", token, userID];
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@device_token_update.php?jsonUpdate&customerID=%i&userLogout=1",SECURED_API_URL,CUSTOMERID]];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    //NSLog(@"Response code: %ld", (long)[response statusCode]);
    
    if ([response statusCode] >= 200 && [response statusCode] < 300)
    {
        NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSLog(@"Response (deviceTokenHandling) ==> %@", responseData);
        
        NSError *error = nil;
        NSDictionary *jsonData = [NSJSONSerialization
                                  JSONObjectWithData:urlData
                                  options:NSJSONReadingMutableContainers
                                  error:&error];
        
        success = [jsonData[@"success"] integerValue];
        NSLog(@"Success: %ld",(long)success);
        
        if(success == 1){
            NSLog(@"token->user connection deleted");
        } else {
            NSLog(@"something went wrong");
        }
        
    } else {
        NSLog(@"connection failed");
    }

}

+(void)registerDeviceToken:(NSString*) token{
    
    int debug = 0;
    #ifdef DEBUG
    debug = 1;
    #endif
    
    
    // register the token
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@device_token_registration.php?appID=%i&registerToken=%@&appInDebugMode=%i",SECURED_API_URL, APPID, token, debug]];
    
    //NSLog(@"token registration url: %@", url);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
    
    NSError *error;
    NSHTTPURLResponse *response = nil;
    
    //NSData *urlData=
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //NSLog(@"Token Registration response:\n%@", response);
    
}

@end
