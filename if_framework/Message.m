//
//  Message.m
//  if_framework
//
//  Created by Julian Böhnke on 17.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "Message.h"

@implementation Message

+ (void) alert:(NSString *)msg Title:(NSString *)title Tag:(int)tag {
  
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil, nil];
        alertView.tag = tag;
        [alertView show];
}

@end
