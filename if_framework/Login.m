//
//  Login&Signup.m
//  if_framework
//
//  Created by Julian Böhnke on 18.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "Login.h"
#import "DBManager.h"
#import "API.h"

@interface Login ()
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation Login

- (UNIHTTPJsonResponse*) callAPI {
    
    API* api = [API new];
    
    UNIHTTPJsonResponse* response = [api request:self.url :self.postData];
    
    return response;
}

-(id)init {
    if ( self = [super init] ) {
        self.overSocial = NO;
    }
    return self;
}

- (void) markUserAsLoggedIn:(NSDictionary*) userData {
    
    NSLog(@" markUserAsLoggedIn %@ ", userData);
    
    // initialising the db manager
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    
    int userID = [userData[@"user_id"] intValue];
    NSString *email = userData[@"username"];
    NSString *first_name = userData[@"first_name"];
    NSString *last_name = userData[@"last_name"];
    NSString *profileImage = nil;
    
    if (userData[@"token"] != nil) {
        [SSKeychain setPassword:userData[@"token"] forService:@"com.bhmms.dashboard.if3_auth_token" account:@"main_acc"];
    }
    
    if (userData[@"profile_image"] != [NSNull null]) {
        profileImage = userData[@"profile_image"];
    }
    
    // update token
    [deviceTokenHandling updateDeviceToken:@"" forUserID:userID];
    
    [[NSUserDefaults standardUserDefaults] setInteger:userID forKey:@"loggedInUserID"];
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"loggedInUserName"];
    [[NSUserDefaults standardUserDefaults] setBool:self.overSocial forKey:@"overSocial"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // save dynamic settings in an extra dictionary
    NSMutableDictionary *tmpDict = [@{} mutableCopy];
    NSArray *dynamicJsonUserSettings = userData[@"dynamicUserSettings"];
    
    for (NSDictionary *setting in dynamicJsonUserSettings) {
        tmpDict[[NSString stringWithFormat:@"%@",setting[@"attribute_id"]]] = setting[@"value"];
    }
    
    localStorage *dynamicSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    [dynamicSettingsStorage setObject:tmpDict forKey:@"dynamicUserSettingsKeyValues"];
    [dynamicSettingsStorage setObject:first_name forKey:@"firstName"];
    [dynamicSettingsStorage setObject:last_name forKey:@"lastName"];
    [dynamicSettingsStorage setObject:email forKey:@"email"];
    [dynamicSettingsStorage setObject:@(self.overSocial) forKey:@"overSocial"];
    
    NSString *imageName = [profileImage lastPathComponent];
    NSString *imagePath = [profileImage stringByReplacingOccurrencesOfString:imageName withString:@""];
    
    [dynamicSettingsStorage setObject:imageName forKey:@"profileImageName"];
    [dynamicSettingsStorage setObject:imagePath forKey:@"profileImagePath"];
    [dynamicSettingsStorage setObject:profileImage forKey:@"profileImage"];
    
    // get amount of users with that ID
    NSString *registeredUserQuery = [NSString stringWithFormat:@"select * from users where userID = %li;",(long) userID];
    
    NSArray *tempArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:registeredUserQuery]];
    
    if (tempArray.count > 0) {
        // mark user as logged in and update information
        NSString *logInUserQuery = [NSString stringWithFormat:@"UPDATE users SET is_logged_in = 1, login = '%@', first_name = '%@', last_name = '%@' WHERE userID = %li;", email, first_name, last_name, (long)userID];
        
        // Execute the query.
        [self.dbManager executeQuery:logInUserQuery];
        
    } else {
        // register new user locally and mark as logged in
        NSString *query = [NSString stringWithFormat:@"insert into users (userID, login, first_name, last_name, is_logged_in, wants_check_in) values(%li, '%@', '%@', '%@', %i, %i);", (long)userID, email, first_name, last_name, 1, 1];        
        // Execute the query.
        [self.dbManager executeQuery:query];
    }
}

@end
