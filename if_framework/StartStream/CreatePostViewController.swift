//
//  CreatePostViewController.swift
//  if_framework
//
//  Created by Christopher on 2/20/17.
//  Copyright © 2017 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

import GooglePlacePicker

enum PostType {
    case food
    case drinks
    case foodAndDrinks
}

enum Mode {
    case post
    case edit
}

class CreatePostViewController: AdminViewController, UITextViewDelegate, GMSAutocompleteViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImageUploadDelegate {
    @IBOutlet weak var privacySwitchTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var drinkHashtagsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!

    @IBOutlet weak var widthImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var hashtagTextView: UITextView!
    @IBOutlet weak var drinkHashtagView: UITextView!
   
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var privacySwitch: UISwitch!
    @IBOutlet weak var foodHashtagSeparator: UIView!
    @IBOutlet weak var drinkHashtagSeparator: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var imageUploadButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    
    @IBOutlet weak var foodButton: UIButton!
    @IBOutlet weak var drinksButton: UIButton!
    @IBOutlet weak var foodAndDrinksButton: UIButton!
    
    let placeHolder: NSAttributedString = NSAttributedString(string: "Add description ...".localized, attributes: [NSAttributedStringKey.font : UIFont(name: "Helvetica", size: 17) as Any, NSAttributedStringKey.foregroundColor : UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1)])
    let foodPlaceholder: NSAttributedString = NSAttributedString(string: "Add food hashtags ...".localized, attributes: [NSAttributedStringKey.font : UIFont(name: "Helvetica", size: 17) as Any, NSAttributedStringKey.foregroundColor : UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1)])
    let drinkPlaceholder: NSAttributedString = NSAttributedString(string: "Add drink hashtags ...".localized, attributes: [NSAttributedStringKey.font : UIFont(name: "Helvetica", size: 17) as Any, NSAttributedStringKey.foregroundColor : UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1)])
    
    var image: UIImage?
    var postDescription: String = ""
    var locationString: String = ""
    var hashTags: String = ""
    var drinkTags: String = ""
    
    var placeholderIsShown: Bool = true
    var foodPlaceholderIsShown: Bool = true
    var drinkPlaceholderIsShown: Bool = true
    
    var createdPost:(([String:Any])->Void)?
    
    var placePicker:GMSPlacePicker?
    
    var mode:Mode = .post
    
    var post:[String:Any] = [:]
    
    var postType:PostType = .food {
        didSet {
            switch postType {
            case .food:
                self.hideFoodHashtagSection(hide: false)
                self.hideDrinkHashtagSection(hide: true)
                
                if self.foodPlaceholderIsShown {
                    self.hashtagTextView?.attributedText = self.foodPlaceholderIsShown ? self.foodPlaceholder : NSAttributedString(string: "")
                }
                
                self.foodButton?.setTitleColor(UIColor.black, for: .normal)
                self.drinksButton?.setTitleColor(UIColor.init(red: 191/255, green: 191/255, blue: 191/255, alpha: 1), for: .normal)
                self.foodAndDrinksButton?.setTitleColor(UIColor.init(red: 191/255, green: 191/255, blue: 191/255, alpha: 1), for: .normal)
                
                privacySwitchTopConstraint?.constant = 10
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }

                break
                
            case .drinks:
                self.hideFoodHashtagSection(hide: false)
                self.hideDrinkHashtagSection(hide: true)
                
                if self.foodPlaceholderIsShown {
                    self.hashtagTextView?.attributedText = self.foodPlaceholderIsShown ? self.drinkPlaceholder : NSAttributedString(string: "")
                }
                
                self.foodButton?.setTitleColor(UIColor.init(red: 191/255, green: 191/255, blue: 191/255, alpha: 1), for: .normal)
                self.drinksButton?.setTitleColor(UIColor.black, for: .normal)
                self.foodAndDrinksButton?.setTitleColor(UIColor.init(red: 191/255, green: 191/255, blue: 191/255, alpha: 1), for: .normal)
                
                privacySwitchTopConstraint?.constant = 10
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
                
                break
                
            case .foodAndDrinks:
                self.hideFoodHashtagSection(hide: false)
                self.hideDrinkHashtagSection(hide: false)
                
                if self.foodPlaceholderIsShown {
                    self.hashtagTextView?.attributedText = self.foodPlaceholderIsShown ? self.foodPlaceholder : NSAttributedString(string: "")
                }
                
                if self.drinkPlaceholderIsShown {
                    self.drinkHashtagView?.attributedText = self.drinkPlaceholderIsShown ? self.drinkPlaceholder : NSAttributedString(string: "")
                }
                
                self.foodButton?.setTitleColor(UIColor.init(red: 191/255, green: 191/255, blue: 191/255, alpha: 1), for: .normal)
                self.drinksButton?.setTitleColor(UIColor.init(red: 191/255, green: 191/255, blue: 191/255, alpha: 1), for: .normal)
                self.foodAndDrinksButton?.setTitleColor(UIColor.black, for: .normal)
                
                privacySwitchTopConstraint?.constant = 30 + drinkHashtagsHeight.constant
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
                
                break
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create post".localized
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(cancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: .plain, target: self, action: #selector(save))
        
        self.keyboardNotifications()
        
        self.textView.returnKeyType = UIReturnKeyType.done
        self.hashtagTextView.returnKeyType = UIReturnKeyType.done
        self.drinkHashtagView.returnKeyType = UIReturnKeyType.done
        
        self.locationButton.addTarget(self, action: #selector(setLocation), for: .touchUpInside)
        
        self.hashtagTextView?.attributedText = self.foodPlaceholderIsShown ? self.foodPlaceholder : NSAttributedString(string: "")
        self.hideDrinkHashtagSection(hide: true)
        
        self.backgroundImage.autoresizingMask = UIViewAutoresizing.flexibleRightMargin
        self.imageUploadButton.addTarget(self, action: #selector(openImageUploader), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if mode == .post {
            if placeholderIsShown == true {
                textView.attributedText = placeHolder
            }
            return
        }
        
        if image != nil {
            self.setImage(image: image!)
        }
        
        if placeholderIsShown {
            textView.attributedText = self.placeHolder
        } else {
            textView.text = postDescription
        }
        
        if locationString != "" {
            self.locationButton.setAttributedTitle(NSAttributedString(string: "\("Location".localized): \(locationString)", attributes: [NSAttributedStringKey.font : UIFont(name: "Helvetica", size: 17) as Any, NSAttributedStringKey.foregroundColor : UIColor.black]), for: .normal)
        }
        
        hashtagTextView.text = hashTags
        drinkHashtagView.text = drinkTags
        
        let postType1 = postType
        postType = postType1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func save() {
        if self.post["location_name"] == nil || (self.post["location_name"] != nil && self.post["location_name"] as! String == "") {
            CWAlert.showNotification(withTitle: "Error".localized, message: "You must select a location".localized, onViewController: self)
            
            return
        }
        var food = ""
        var drinks = ""
        switch self.postType {
        case .food:
            if !self.foodPlaceholderIsShown {
                food = self.hashtagTextView.text
            }
            break
            
        case .drinks:
            if !self.foodPlaceholderIsShown {
                drinks = self.hashtagTextView.text
            }
            break
            
        case .foodAndDrinks:
            if !self.foodPlaceholderIsShown {
                food = self.hashtagTextView.text
            }
            
            if !self.drinkPlaceholderIsShown {
                drinks = self.drinkHashtagView.text
            }
        }
        
        food = addMissingSpaces(string: food)
        food = self.addMissingHashtags(string: food)
        drinks = addMissingSpaces(string: drinks)
        drinks = self.addMissingHashtags(string: drinks)
        self.appendStringWithSpaceIfNeeded(string: &food)
        self.appendStringWithSpaceIfNeeded(string: &drinks)
        
        self.post["content"] = self.textView.attributedText.string != self.placeHolder.string ? self.textView.attributedText.string : ""
        self.post["user_id"] = UserDefaults.standard.integer(forKey: "loggedInUserID")
        self.post["food_hashtags"] = food
        self.post["drink_hashtags"] = drinks
        self.post["privacy_mode"] = privacySwitch.isOn ? 1 : 0
        
        NSLog(" CreatePost - creating: \(self.post)")
        
        var res: Any
        switch mode {
        case .post:
            res = AdminAPI.create(url: "ardnd/post/create", post: self.post)
        case .edit:
            let id = post["id"] as! String
            res = AdminAPI.create(url: "ardnd/post/update/" + id, post: self.post)
        }
        
        if let resDict = res as? [String:Any] {
            if resDict["success"] as? Int == 1  || resDict["success"] as? String == "1" {
                if let resData = resDict["data"] as? [String:Any] {
                    if self.createdPost != nil {
                        self.createdPost!(resData)
                    }
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            NSLog(" CreatePost - Response: \(resDict)")
        }
    }
    
    func appendStringWithSpaceIfNeeded(string: inout String)
    {
        if string.last == " " || string.count == 0 {
            return
        }
        
        string.append(" ")
    }
    
    func addMissingHashtags(string: String) -> String {
        if string.count == 0 {
            return string
        }
        
        var hashtagsString = ""
        
        let words = string.components(separatedBy: " ")
        for var word in words {
            if !self.validateIFSC(code: word) {
                continue
            }
            
            if word.substring(to: word.index(after: word.startIndex)) != "#" {
                word.insert("#", at: word.startIndex)
                word.insert(" ", at: word.endIndex)
            }
            hashtagsString += word
        }
        
        return hashtagsString
    }
    
    func addMissingSpaces(string: String) ->String {
        guard string.count != 0 else {
            return string
        }
        
        var hashtagsString = ""
        
        let words = string.components(separatedBy: "#")
        for var word in words {
            guard word.count != 0 else {
                continue
            }
            
            if word.substring(to: word.index(after: word.startIndex)) != " " {
                word.insert(" ", at: word.startIndex)
            }
            hashtagsString += word
        }
        
        return hashtagsString
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if LoggedUserCredentials.sharedInstance.isModerator() {
            sender.isOn = !sender.isOn
        } else {
            let alertController = UIAlertController.init(title: "", message: "To do this you need to become an influencer".localized, preferredStyle: .alert)

            let okAction = UIAlertAction.init(title: "I want to become one".localized, style: .default) { (nil) in

            }

            let dontShowAction = UIAlertAction.init(title: "Cancel".localized, style: .default) { (nil) in
//                UserDefaults.standard.setValue(false, forKey: "showPrivatePostPopUpMessage")
                sender.isOn = true
            }

            alertController.addAction(okAction)
            alertController.addAction(dontShowAction)

            self.present(alertController, animated: true) {

            }
        }
        
//        if !sender.isOn {
//            return
//        }
//
//        if UserDefaults.standard.value(forKey: "showPrivatePostPopUpMessage") == nil || (UserDefaults.standard.value(forKey: "showPrivatePostPopUpMessage") as? Bool) == true {
//            let alertController = UIAlertController.init(title: "", message: "Only you can see and save this post in your favorite „food-spot“. Others will not be able to discover this amazing location.".localized, preferredStyle: .alert)
//
//            let okAction = UIAlertAction.init(title: "OK".localized, style: .default) { (nil) in
//
//            }
//
//            let dontShowAction = UIAlertAction.init(title: "Don`t show again".localized, style: .default) { (nil) in
//                UserDefaults.standard.setValue(false, forKey: "showPrivatePostPopUpMessage")
//                sender.isOn = false
//            }
//
//            alertController.addAction(okAction)
//            alertController.addAction(dontShowAction)
//
//            self.present(alertController, animated: true) {
//
//            }
//        }
    }
    
    func validateIFSC(code : String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "[A-Za-z]")
        return regex.numberOfMatches(in: code, range: NSRange(location:0, length: code.count)) >= 1
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.attributedText = NSAttributedString(string: textView.text, attributes: [NSAttributedStringKey.font : UIFont(name: "Helvetica", size: 17) as Any, NSAttributedStringKey.foregroundColor : UIColor.black])
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        switch textView.tag {
        case 0:
            textView.text = self.placeholderIsShown ? "" : textView.text
            self.placeholderIsShown = false
            break
            
        case 1:
            textView.text = self.foodPlaceholderIsShown ? "" : textView.text
            self.foodPlaceholderIsShown = false
            break
            
        case 2:
            textView.text = self.drinkPlaceholderIsShown ? "" : textView.text
            self.drinkPlaceholderIsShown = false
            break

        default:
            break
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != "" {
            return
        }
        
        if textView.tag == 0 {
            textView.attributedText = placeHolder
            placeholderIsShown = true
            return
        }
        
        if textView.tag == 2 {
            textView.attributedText = self.drinkPlaceholder
            self.drinkPlaceholderIsShown = true
            return
        }
        
        textView.attributedText = self.postType == .drinks ? self.drinkPlaceholder : self.foodPlaceholder
        self.foodPlaceholderIsShown = true
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        let info = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        bottomConstraint.constant = keyboardSize - bottomLayoutGuide.length + 8
        imageUploadButton.isHidden = true
        backgroundImage.isHidden = true
        privacyLabel.isHidden = true
        privacySwitch.isHidden = true
        
        let duration: TimeInterval = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        UIView.animate(withDuration: duration) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let info = sender.userInfo!
        let duration: TimeInterval = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        bottomConstraint.constant = self.backgroundImage.height
        imageUploadButton.isHidden = false
        backgroundImage.isHidden = false
        privacyLabel.isHidden = false
        privacySwitch.isHidden = false
        
        UIView.animate(withDuration: duration) { self.view.layoutIfNeeded() }
    }
    
    func keyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @objc func setLocation() {
        let vc = GMSAutocompleteViewController()
        vc.delegate = self;
        
        self.present(vc, animated: true, completion: {
            
        })
    }
    
    func selected(place: GMSPlace, on: UIViewController, okPressed:(()->Void)?) -> Bool{
        self.post["location_name"] = place.name
        
        var meta:[String:Any] = ["gmsPlaceId":place.placeID]
        
        if place.addressComponents != nil {
            var gmsAdressParts:[String:Any] = [:]
            for component:GMSAddressComponent in place.addressComponents! {
                if component.type == kGMSPlaceTypeLocality {
                    meta["city"] = component.name
                }
                gmsAdressParts[component.type] = component.name
            }
            meta["gmsAdressParts"] = gmsAdressParts
        }
        
        self.post["meta"] = meta
        
        self.locationButton.setAttributedTitle(NSAttributedString(string: "\("Location".localized): \(place.name)", attributes: [NSAttributedStringKey.font : UIFont(name: "Helvetica", size: 17) as Any, NSAttributedStringKey.foregroundColor : UIColor.black]), for: .normal)
        
        return true
    }
    
    // MARK: GMSAutoCompleteViewController delegate
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if(self.selected(place: place, on: viewController)){
            viewController.dismiss(animated: true) {
                
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true) {
            
        }
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true) {
            
        }
    }
    
    func selected(place: GMSPlace, okPressed:(()->Void)?) -> Bool{
        return selected(place: place, on: self, okPressed: okPressed)
    }
    
    func selected(place: GMSPlace, on: UIViewController) -> Bool{
        return selected(place: place, on: on, okPressed: nil)
    }
    
    func selected(place: GMSPlace) -> Bool{
        return selected(place: place, on: self, okPressed: nil)
    }
    
    
    // MARK: image uploader methods
    @objc func openImageUploader(){
        let imageSelectorAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        imageSelectorAlert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { (nil) in
            let picker = UIImagePickerController()
            
            picker.delegate = self;
            picker.allowsEditing = true;
            picker.sourceType = .camera;
            
            self.present(picker, animated: true, completion: {
                
            })
        }))
        
        imageSelectorAlert.addAction(UIAlertAction(title: "Library".localized, style: .default, handler: { (nil) in
            let picker = UIImagePickerController()
            
            picker.delegate = self;
            picker.allowsEditing = true;
            picker.sourceType = .photoLibrary;
            
            self.present(picker, animated: true, completion: {
                
            })
        }))
        
        imageSelectorAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (nil) in
            
        }))
        
        self.present(imageSelectorAlert, animated: true) {
            
        }
    }
    
    // MARK: image picking
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerEditedImage]
        
        self.showLoading()
        
        
        let imagemethods = imageMethods()
        imagemethods.uploadDelegate = self
        
        imagemethods.uploadImage(image as! UIImage!, withParameters: ["uploadType":"elementImage"])
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func uploadFinished(withResponse response: [AnyHashable : Any]!) {
        
        DispatchQueue.main.async {
            self.endLoading()
        }
        
        if response != nil {
            if response["success"] as? Int == 1 {
                self.post["image"] = (response["filePath"] as! String) + (response["fileName"] as! String)
                
                if(self.post["image"] != nil){
                    DispatchQueue.global().async {
                        let image:UIImage? = imageMethods.uiImage(withServerPathName: self.post["image"] as? String, width: 90)
                        
                        DispatchQueue.main.async {
                            if(image != nil){
                                self.setImage(image: image!)
                            }
                            else {
                                self.backgroundImage.image = nil
                                self.imageUploadButton.setTitle("Image", for: .normal)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setImage(image: UIImage) {
        self.setImageViewSizeToImage(image: image)
        self.backgroundImage.image = image
        self.imageUploadButton.setTitle("", for: .normal)
    }
    
    func setImageViewSizeToImage(image: UIImage) {
        let imageWidth = image.size.width
        let imageHeight = image.size.height
        
        let aspect = imageWidth / imageHeight
        
        if (imageHeight > self.backgroundImage.height) {
            self.widthImageConstraint.constant = aspect * self.backgroundImage.height
        }
    }
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.hashtagTextView.text = "#food"
            break
            
        case 1:
            self.hashtagTextView.text = "#drink"
            break
            
        case 2:
            self.hashtagTextView.text = "#food #drink"
            break
            
        default:
            break;
        }
    }
    
    // MARK: type changing events
    
    @IBAction func foodButtonTapped(_ sender: Any) {
         self.postType = .food
    }
    
    @IBAction func drinksButtonTapped(_ sender: Any) {
        self.postType = .drinks
    }
    
    @IBAction func foodAndDrinksButtonTapped(_ sender: Any) {
        self.postType = .foodAndDrinks
    }
    
    func hideFoodHashtagSection(hide: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.hashtagTextView?.alpha = hide ? 0 : 1
            self.foodHashtagSeparator?.alpha = hide ? 0 : 1
        }
    }
    
    func hideDrinkHashtagSection(hide: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.drinkHashtagView?.alpha = hide ? 0 : 1
            self.drinkHashtagSeparator?.alpha = hide ? 0 : 1
        }
    }
}
