//
//  StartStreamViewController.swift
//  if_framework
//
//  Created by Christopher on 2/20/17.
//  Copyright © 2017 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit
import AVFoundation
import GooglePlacePicker

class StartStreamViewController: AdminViewController, AdminTableViewCellDelegate, GMSAutocompleteViewControllerDelegate, UITextFieldDelegate, UISearchControllerDelegate, UISearchBarDelegate, UITableViewDataSourcePrefetching {
    
    fileprivate let imageDownloadOperationQueue = OperationQueue()
    fileprivate var imageDownloadOperations = [IndexPath:ImageDownloadOperation]()

    @IBOutlet weak var locationButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var foodTagsTextField: UITextField!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bottonSeparatorView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchViewWidth: NSLayoutConstraint!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    
@objc   var locationName: String? {
        get {
            if let locationString = UserDefaults.standard.object(forKey: "locationName") as? String {
                if locationString != "" {
                    return locationString
                }
            }
            
            return "Location".localized
        }
        
        set {
            UserDefaults.standard.setValue(newValue, forKey: "locationName")
        }
    }
    
    @IBOutlet var searchView: UIView!
    
    @IBOutlet weak var scrollUpView: UIView!
    @IBOutlet weak var scrollUpImage: UIImageView!
    
@objc   var hashtagsString: String = ""
@objc   var placeMeta: [String:Any] = NSDictionary.init() as! [String : Any]
    
    @IBAction func scrollUpButtonPressed(_ sender: UIButton) {
        self.tableView.setContentOffset(CGPoint.init(x:0,y:0), animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
@objc   var filters:String = ""
    
@objc   var locationImage:UIImage = imageMethods.resize(UIImage(named: "ios7-location")!, for: CGSize(width: 20, height: 20)).withRenderingMode(.alwaysTemplate)
    
@objc   var userId:String = "0"
    
@objc   var imageMap:[String:UIImage] = [:]
    let footerView:UIView = {
        let v = UIView()
        let a = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        v.backgroundColor = .groupTableViewBackground
        v.height = 60
        v.width = UIScreen.main.bounds.size.width
        v.addSubview(a)
        
        a.center = v.center
        a.startAnimating()
        
        return v
    }()
    
@objc   var loadingUntilCount:Int = 0
    
    let loadingSteps:Int = 15
    
    let isModerator:Bool = UserDefaults.standard.bool(forKey: "hostModeActivated") ? UserDefaults.standard.bool(forKey: "hostModeActivated") : false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorInset = .zero
        
        //if self.navigationController != nil {
        //    self.tableView.addGestureRecognizer(self.navigationController!.barHideOnSwipeGestureRecognizer)
        //}
        
        if self.title == nil {
            self.title = "Home".localized
        }
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tableView.backgroundColor = .groupTableViewBackground
        self.refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        self.scrollUpView?.isHidden = true
        self.scrollUpImage?.image = UIImage(named:"ios7-arrow-up.png")?.withRenderingMode(.alwaysTemplate)
        self.scrollUpImage?.tintColor = .white
        
        self.foodTagsTextField?.delegate = self
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorColor = .white
        self.initNavigationItems()
        
        // self.locationButton?.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        self.searchViewWidth?.constant = UIScreen.main.bounds.width
        self.searchView?.width = UIScreen.main.bounds.width
        
        
        self.userId = "\(UserDefaults.standard.integer(forKey: "loggedInUserID"))"
        
        if #available(iOS 10.0, *) {
            tableView.prefetchDataSource = self
        }
        
        if #available(iOS 11.0, *) {
            // iOS 11 section
            if searchView == nil {
                return
            }
            
            self.tableView.tableHeaderView = nil
            let searchViewController = UISearchController(searchResultsController: nil)
            self.navigationItem.searchController = searchViewController
            searchViewController.searchBar.enablesReturnKeyAutomatically = false
            searchViewController.searchBar.placeholder = "#" + "search".localized
            searchViewController.delegate = self
            searchViewController.searchBar.delegate = self
            searchViewController.obscuresBackgroundDuringPresentation = false
            
            //self.tableView.tableHeaderView = self.searchView
            //self.tableView.tableHeaderView?.addSubview(self.refreshControl)
//            self.tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
//            self.tableView.tableHeaderView = self.refreshControl
            self.tableView.addSubview(self.refreshControl)
            self.setupSearchView()
        }
        else {
            // iOS 10 and below section
            self.tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
            self.tableView.tableHeaderView?.addSubview(self.refreshControl)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
        self.locationButton?.setTitle(self.locationName, for: .normal)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    func setupSearchView() {
        self.searchViewHeight?.constant = 40
        self.searchView?.size.height = 40
        self.locationButtonTopConstraint?.constant = 5
        self.searchButton?.isHidden = true
        self.bottonSeparatorView?.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.data.isEmpty {
            self.searchForFilters()
        }
        
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    func createHashtagLink(hashtags: String) -> NSAttributedString{
        let hashtagsArray = hashtags.components(separatedBy: " ")
        let attributedHashtags = NSMutableAttributedString.init()
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedString.Key(rawValue: NSAttributedStringKey.font.rawValue) : UIFont(name: "Helvetica", size: 16)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 0.05, green: 0.4, blue: 0.65, alpha: 1.0)]
        
        for item in hashtagsArray {
            let attributedItem = NSAttributedString(string: item, attributes: attributes)
            attributedHashtags.append(attributedItem)
            attributedHashtags.append(NSAttributedString.init(string: " "))
        }

        return attributedHashtags
    }
    
    func initNavigationItems(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(createImage))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageMethods.resize(UIImage(named:"ardndplus2"), for: CGSize(width: 33, height: 33)), style: .plain, target: self, action: #selector(openMyOffers))
    }

    @IBAction func offersButtonPressed(_ sender: UIButton) {
        self.openMyOffers()
    }
    
    @IBAction func searchButtonTap(_ sender: Any) {
        if let hashtags = self.foodTagsTextField?.text {
            self.hashtagsString = hashtags
            self.searchForFilters()
        }
        else {
            self.hashtagsString = ""
            self.searchForFilters()
        }
    }
    
    @IBAction func locationTextFieldTouchDown(_ sender: Any) {
        NSLog(" Clicked location picker")
        let vc = GMSAutocompleteViewController()
        vc.delegate = self;
        
        self.present(vc, animated: true, completion: {
            
        })
    }
    
    @objc func openMyOffers(){
        let vc = get(viewController: "MyOffersListViewController")

        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func loadData() {
        self.tableView.tableFooterView = self.footerView
        DispatchQueue.global().async {
            let res = AdminAPI.get(fromUrl: "ardnd/post/0/10?\(self.filters)")
            
            if let resData = res as? [[String:Any]] {
                DispatchQueue.main.async {
                    self.data = resData
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    self.tableView.tableFooterView = nil
                }
            } else {
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.tableView.tableFooterView = nil
                }
            }
        }
    }
    
    func loadUpdates() {
        
        if self.data.count + self.loadingSteps == self.loadingUntilCount {
            return
        }
        
        self.tableView.tableFooterView = self.footerView
        
        self.loadingUntilCount = self.data.count + self.loadingSteps
        
        DispatchQueue.global().async {
            let res = AdminAPI.get(fromUrl: "ardnd/post/\(self.data.count)/\(self.loadingUntilCount)?\(self.filters)")
            
            if let resData = res as? [[String:Any]] {
                DispatchQueue.main.async {
                    if resData.count > 0 {
                        var currentRows = self.data.count
                        
                        var indexPaths:[IndexPath] = []
                        
                        for _ in resData {
                            indexPaths.append(IndexPath(row:currentRows, section:0))
                            currentRows += 1;
                        }
                        
                        self.data.append(contentsOf: resData)
                        
                        self.tableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.bottom)
                    }
                    
                    self.loadingUntilCount = self.data.count
                }
            }
            
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.tableView.tableFooterView = nil
                    self.tableView.setNeedsLayout()
                })
            }
        }
    }
    
    func createText() {
        self.createPost(type: 0)
    }
    
    @objc func createImage() {
        self.createPost(type: 1)
    }
    
    func createPost(type:Int) {
        let createController = get(viewController: "CreatePostViewController") as! CreatePostViewController
        
        createController.createdPost = {(post:[String:Any]) in
            self.data.insert(post, at: 0)
            self.tableView.insertRows(at: [IndexPath(row:0, section:0)], with: .fade)
        }
        
        switch type {
        case 1:
            createController.postType = .food
            break
            
        case 2:
            createController.postType = .drinks
            break
            
        case 3:
            createController.postType = .foodAndDrinks
            break
            
        default:
            createController.postType = .food
        }
        
        let navigationController = UINavigationController(rootViewController: createController)
        
        self.present(navigationController, animated: true) { 
            
        }
    }
    
    func editPost(indexPath: NSIndexPath) {
        let createController = get(viewController: "CreatePostViewController") as! CreatePostViewController
        createController.mode = .edit
        createController.post = ["id": data[indexPath.row]["id"]!, "location_name":data[indexPath.row]["location_name"] ?? "", "meta":data[indexPath.row]["meta"] ?? ""]
        
        weak var welf = self;
        createController.createdPost = {(post:[String:Any]) in
            welf?.loadData()
        }
        
        if !(self.data[indexPath.row]["drink_hashtags"] as? String ?? "").isEmpty {
            let drinkTags = self.data[indexPath.row]["drink_hashtags"] as! String
            createController.foodPlaceholderIsShown = false
            createController.postType = .drinks
            
            if !(self.data[indexPath.row]["food_hashtags"] as? String ?? "").isEmpty {
                let foodTags = self.data[indexPath.row]["food_hashtags"] as! String
                createController.drinkPlaceholderIsShown = false
                createController.postType = .foodAndDrinks
                createController.hashTags = foodTags
                createController.drinkTags = drinkTags
            }
            else {
                createController.hashTags = drinkTags
            }
        } else {
            if !(self.data[indexPath.row]["food_hashtags"] as? String ?? "").isEmpty {
                let foodTags = self.data[indexPath.row]["food_hashtags"] as! String
                createController.foodPlaceholderIsShown = false
                createController.postType = .food
                createController.hashTags = foodTags
            }
        }
        
        let tableViewCell = tableView.cellForRow(at: indexPath as IndexPath) as? AdminTableViewCell
        createController.image = (tableViewCell?.imageView2.image)!
        
        if let postDescription = self.data[indexPath.row]["content"] as? String {
            createController.placeholderIsShown = false
            createController.postDescription = postDescription
        }
        
        createController.locationString = self.data[indexPath.row]["location_name"] as? String ?? ""
        
        let navigationController = UINavigationController(rootViewController: createController)
        
        self.present(navigationController, animated: true) {
            
        }
    }
    
    //MARK: table view data source delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier:String = "textCell"
        var isImage:Bool = false
    
        if let tmpIdentifier = self.data[indexPath.row]["_tmp_identifier"] as? String {
            identifier = tmpIdentifier
            
            if tmpIdentifier == "imageCell" || tmpIdentifier == "imageOfferCell" {
                isImage = true
            }
        }
        else {
            if self.data[indexPath.row]["image"] != nil && self.data[indexPath.row]["image"] is String  && self.data[indexPath.row]["image"] as! String != "" {
                if self.data[indexPath.row]["entity_type"] != nil &&
                    (self.data[indexPath.row]["entity_type"] as? String == "1" || self.data[indexPath.row]["entity_type"] as? Int == 1) &&
                    self.data[indexPath.row]["offer_id"] != nil &&
                    (self.data[indexPath.row]["offer_id"] as? Int != nil && self.data[indexPath.row]["offer_id"] as? Int != 0 || (self.data[indexPath.row]["offer_id"] as? String != nil && self.data[indexPath.row]["offer_id"] as? String != "0" && self.data[indexPath.row]["offer_id"] as? String != "")) {
                    self.data[indexPath.row]["_tmp_identifier"] = identifier = "imageOfferCell"
                }
                else {
                    self.data[indexPath.row]["_tmp_identifier"] = identifier = "imageCell"
                }
                isImage = true
            }
            else {
                self.data[indexPath.row]["_tmp_identifier"] = identifier = "textCell"
                isImage = false
            }
        }
        
        let cell:AdminTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AdminTableViewCell ?? AdminTableViewCell()
        
        cell.tag = indexPath.row
        cell.delegate = self

        if isImage {
            if self.getLinesForDescriptionLabel(text: self.data[indexPath.row]["content"] as? String ?? "") < 3 {
                cell.button1?.removeFromSuperview()
            }
            else {
                print("", separator:"", terminator:"")
            }
        
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let postImage = self.imageMap[postImageString]
                    DispatchQueue.main.async {
                        if postImage != nil && cell.tag == indexPath.row {
                            self.imageMap[postImageString] = postImage
                            cell.imageView2?.image = postImage
                        }
                    }
                }
                else {
                    if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
                        imageMap[postImageString] = image
                        cell.imageView2?.image = image

                        tableView.beginUpdates()
                        tableView.endUpdates()
                    }
                    else {
//                        DispatchQueue.global().async {
//                            let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
//                                DispatchQueue.main.async {
//                                    if postImage != nil && cell.tag == indexPath.row {
//                                        self.imageMap[postImageString] = postImage
//                                        cell.imageView2?.image = postImage
//
//                                        tableView.beginUpdates()
//                                        tableView.endUpdates()
//                                    }
//                                    else {
//                                        print("", separator:"", terminator:"")
//                                    }
//                                }
//                            }
                        let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
                        imageDownloadOperation.completionHandler = { [weak self] (image) in
                            guard let strongSelf = self else {
                                return
                            }
                            
                            strongSelf.imageMap[postImageString] = image
                            strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
                            
                            DispatchQueue.main.async {
                                cell.imageView2?.image = image
                                
                                tableView.beginUpdates()
                                tableView.endUpdates()
                            }
                            
                        }
                        
                        imageDownloadOperationQueue.addOperation(imageDownloadOperation)
                        imageDownloadOperations[indexPath] = imageDownloadOperation
                    }
                }
            }
        }
//        else {
//            print("", separator:"", terminator:"")
//        }
    
        if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
            if self.imageMap[userImageString] != nil {
                cell.imageView1?.image = self.imageMap[userImageString]
            }
            else {
                DispatchQueue.global().async {
                    var userImage:UIImage? = nil
                    
                    if(userImageString.hasPrefix("http")){
                        userImage = imageMethods.uiImage(withWebPath: userImageString)
                    }
                    else {
                        userImage = imageMethods.uiImage(withServerPathName: userImageString, width: 36)
                    }
                    
                        DispatchQueue.main.async {
                            if userImage != nil && cell.tag == indexPath.row {
                                self.imageMap[userImageString] = userImage
                                cell.imageView1?.image = userImage
                            }
                            else {
                                self.imageMap[userImageString] = UIImage()
                                cell.imageView1?.image = nil
                            }
                        }
                }
            }
        }
        else {
            cell.imageView1?.image = nil
        }
        
        if cell.imageView3 != nil {
            if let locatonName = self.data[indexPath.row]["location_name"] as? String, locatonName != "" {
                cell.imageView3.image = self.locationImage
                cell.imageView3.tintColor = .lightGray
            }
            else {
                cell.imageView3.image = nil
            }
        }
        else {
            print("", separator:"", terminator:"")
        }
        
        cell.label1?.text = self.data[indexPath.row]["user_name"] as? String ?? ""
        cell.label2?.text = self.data[indexPath.row]["content"] as? String ?? ""
        cell.label3?.text = self.data[indexPath.row]["location_name"] as? String ?? ""
        
        let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
        let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
        cell.hashtagsTextView?.attributedText = self.createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
        cell.hashtagsTextView?.width = UIScreen.main.bounds.width - 5
        cell.hashtagsTextView?.sizeToFit()
        cell.hashtagsTextViewHeight?.constant = (cell.hashtagsTextView?.height)!
        
        cell.button1pressed = {
            let ovc = get(viewController: "OfferViewController") as! OfferViewController
            
            ovc.offerID = UInt(self.data[indexPath.row]["entity_id"] as? String ?? "0") ?? UInt(0)
            
            ovc.title = self.data[indexPath.row]["offer_name"] as? String ?? ""
            
            self.navigationController?.pushViewController(ovc, animated: true)
        }
        
        // go to location stream
        cell.button2pressed = {
            let vc = get(viewController: "LocationStreamViewController") as! LocationStreamViewController
            
            vc.title = self.data[indexPath.row]["location_name"] as? String ?? ""
            vc.filters = "location=\((self.data[indexPath.row]["location_name"] as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            
            if let meta = self.data[indexPath.row]["meta"] as? [String:Any] {
                if let placeId = meta["gmsPlaceId"] as? String {
                    vc.placeId = placeId
                }
            }
            
            let vcc = get(viewController: "LocationCollectionViewController") as! LocationCollectionViewController
            vcc.title = self.data[indexPath.row]["location_name"] as? String ?? ""
            vcc.filters = "location=\((self.data[indexPath.row]["location_name"] as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            
            if let meta = self.data[indexPath.row]["meta"] as? [String:Any] {
                if let placeId = meta["gmsPlaceId"] as? String {
                    vcc.placeId = placeId
                }
            }
            
            self.navigationController?.pushViewController(vcc, animated: true)
        }
        
        
        // location bookmarking
        if self.data[indexPath.row]["isLiked"] as? String == "1" || self.data[indexPath.row]["isLiked"] as? Int == 1 {
            let imgName = "bookmark-full.png"
            
            if let img = self.imageMap[imgName] {
                cell.imageView4?.image = img
            }
            else {
                DispatchQueue.global().async {
                    let img = imageMethods.resize(UIImage(named:imgName), for: CGSize(width: 37, height: 37))
                    
                    DispatchQueue.main.async {
                        if img != nil && cell.tag == indexPath.row {
                            self.imageMap[imgName] = img
                            
                            cell.imageView4?.image = img
                            
                            cell.setNeedsLayout()
                        }
                    }
                }
            }
        }
        else {
            let imgName = "bookmark.png"
            
            if let img = self.imageMap[imgName] {
                cell.imageView4?.image = img
            }
            else {
                DispatchQueue.global().async {
                    let img = imageMethods.resize(UIImage(named:imgName), for: CGSize(width: 37, height: 37))
                    
                    DispatchQueue.main.async {
                        if img != nil && cell.tag == indexPath.row {
                            self.imageMap[imgName] = img
                            
                            cell.imageView4?.image = img
                            
                            cell.setNeedsLayout()
                        }
                    }
                }
            }
        }
        
        cell.button4pressed = {
            let placeVC = get(viewController: "LocationStreamViewController") as! LocationStreamViewController
            
            placeVC.place["name"] = self.data[indexPath.row]["location_name"] as? String ?? ""
            
            if let meta = self.data[indexPath.row]["meta"] as? [String:Any] {
                if let placeId = meta["gmsPlaceId"] as? String {
                    placeVC.placeId = placeId
                }
                if let city = meta["city"] as? String {
                    placeVC.place["city"] = city
                }
            }
            
            self.showLoading()
            
            if self.data[indexPath.row]["isLiked"] as? String == "1" || self.data[indexPath.row]["isLiked"] as? Int == 1 {
                placeVC.unfollowLocation(whenDone:{
                    self.data[indexPath.row]["isLiked"] = 0
                    cell.imageView4?.image = self.imageMap["bookmark.png"]
                    cell.setNeedsLayout()
                    self.endLoading()
                })
            }
            else {
                placeVC.followLocation(whenDone:{
                    self.data[indexPath.row]["isLiked"] = 1
                    cell.imageView4?.image = self.imageMap["bookmark-full.png"]
                    cell.setNeedsLayout()
                    self.endLoading()
                })
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions:[UITableViewRowAction] = []
        
        if self.isModerator ||  self.data[indexPath.row]["user_id"] as? String == self.userId || self.data[indexPath.row]["user_id"] as? Int == Int.init(self.userId) {
        
            actions.append(UITableViewRowAction(style: .destructive, title: "Delete".localized, handler: { (nil, indexPath) in
                let ac = UIAlertController(title: "Delete?".localized, message: "Are you sure, that you want to delete this post?".localized, preferredStyle: .alert)
                
                ac.addAction(UIAlertAction(title: "Delete".localized, style: .destructive, handler: { (action) in
                    self.showLoading()
                    
                    DispatchQueue.global().async {
                        var modString = ""
                        
                        if self.isModerator {
                            modString = "?isMod=1"
                        }
                        
                        let response = AdminAPI.delete(url: "ardnd/post/delete/\(self.data[indexPath.row]["id"] as? String ?? "0")\(modString)")
                        
                        DispatchQueue.main.async {
                            if let res = response as? [String:Any] {
                                if res["success"] as? Int == 1 || res["success"] as? String == "1" {
                                    self.endLoading()
                                    
                                    self.data.remove(at: indexPath.row)
                                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                                }
                                else {
                                    self.endLoading()
                                }
                            }
                        }
                    }
                }))
                
                ac.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (action) in
                    
                }))
                
                self.present(ac, animated: true, completion: nil)
            }))
        
            actions.append(UITableViewRowAction(style: .normal, title: "Edit", handler: { (nil, indexPath) in
                self.editPost(indexPath: indexPath as NSIndexPath)
            }))
        }
        
        return actions
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 266
        
        if self.data[indexPath.row]["tmp_height"] != nil && self.data[indexPath.row]["tmp_height"] is CGFloat {
            return self.data[indexPath.row]["tmp_height"] as! CGFloat
        }
        else {
            // safety
            print("", separator:"", terminator:"")
        }
        
        if self.data[indexPath.row]["image"] == nil || !(self.data[indexPath.row]["image"] is String) || (self.data[indexPath.row]["image"] is String && self.data[indexPath.row]["image"] as? String == "") {
            let cell:AdminTableViewCell = tableView.dequeueReusableCell(withIdentifier: "textCell") as? AdminTableViewCell ?? AdminTableViewCell()
            cell.label2?.text = self.data[indexPath.row]["content"] as? String ?? ""
            cell.label2?.width = UIScreen.main.bounds.size.width - 16
            cell.label2?.sizeToFit()
            
            height = 4 + 45 + 8 + cell.label2.height + 8;
        }
        else {
            let cell:AdminTableViewCell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as? AdminTableViewCell ?? AdminTableViewCell()
            cell.label2?.text = self.data[indexPath.row]["content"] as? String ?? ""
            cell.label2?.width = UIScreen.main.bounds.size.width - 16
            cell.label2?.sizeToFit()
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = self.createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            cell.hashtagsTextView?.width = UIScreen.main.bounds.size.width - 5
            cell.hashtagsTextView?.sizeToFit()
            
            let cellImage:UIImage? = self.imageMap[self.data[indexPath.row]["image"] as? String ?? "nil"]
            
            var imageHeight:CGFloat = 60
            
            if cellImage != nil {
                let aspect = cellImage!.size.height / cellImage!.size.width
                
                imageHeight = (UIScreen.main.bounds.width) * aspect

                if  self.data[indexPath.row]["_tmp_identifier"] as? String == "imageOfferCell" {
                    imageHeight += 45 + 10
                }
            }
            else {
                return 4 + 45 + 4 + imageHeight + 8 + cell.label2!.height + 24 + cell.hashtagsTextView!.height
            }
            
            height = 4 + 45 + 4 + imageHeight + 8 + cell.label2!.height + 24 + cell.hashtagsTextView!.height
        }
        
        self.data[indexPath.row]["tmp_height"] = height
        
        return height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        if #available(iOS 11.0, *) {
            if (navigationItem.searchController?.isActive) ?? false {
                navigationItem.searchController?.isActive = false
            }
        }
        
        let vc = get(viewController: "ProfileStreamViewController") as! ProfileStreamViewController
        
        vc.title = self.data[indexPath.row]["user_name"] as? String ?? ""
        vc.filters = "filters[if3_users.id]=\(self.data[indexPath.row]["user_id"] as? String ?? "")"
        
        if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
            if self.imageMap[userImageString] != nil {
                vc.userImage = self.imageMap[userImageString]
            }
        }
        
        vc.profileId = self.data[indexPath.row]["user_id"] as? String ?? ""

        let vcc = get(viewController: "ProfileCollectionViewController") as! ProfileCollectionViewController
        vcc.title = self.data[indexPath.row]["user_name"] as? String ?? ""
        vcc.filters = "filters[if3_users.id]=\(self.data[indexPath.row]["user_id"] as? String ?? "")"
        vcc.imageMap = imageMap
        
        if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
            if let image = self.imageMap[userImageString] as? UIImage {
                vcc.userImage = image
            }
        }
        
        vcc.profileId = self.data[indexPath.row]["user_id"] as? String ?? ""
        
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.data.count > 4 && indexPath.row >= self.data.count - 4 {
            self.loadUpdates()
        }
        if indexPath.row == 0 {
            self.hideGoUpButton()
        }
        if indexPath.row > 5 {
            self.showGoUpButton()
        }
    }
    
    //MARK: table view data source prefetching delegate
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if imageDownloadOperations[indexPath] != nil {
                return
            }
            
            guard let urlString = self.data[indexPath.row]["image"] as? String else {
                return
            }
            
            let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: urlString)
            imageDownloadOperationQueue.addOperation(imageDownloadOperation)
            imageDownloadOperations[indexPath] = imageDownloadOperation
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            guard let imageDownloadOperation = imageDownloadOperations[indexPath] else {
                return
            }
            
            imageDownloadOperation.cancel()
            imageDownloadOperations.removeValue(forKey: indexPath)
        }
    }
    
    func hideGoUpButton() {
        if !self.scrollUpView.isHidden {
            UIView.animate(withDuration: 0.3, animations: { 
                self.scrollUpView.alpha = 0
            }, completion: { (completed:Bool) in
                if completed {
                    self.scrollUpView.isHidden = true
                }
            })
        }
    }
    
    func showGoUpButton() {
        if self.scrollUpView.isHidden {
            self.scrollUpView.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollUpView.alpha = 0.5
            })
        }
    }
    
    
    func getLinesForDescriptionLabel(text:String) -> Int {
        let label = UILabel()
        
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(name: "Helveticaneue", size: 16)!
        label.width = UIScreen.main.bounds.size.width - 16
        label.text = text
        
        label.sizeToFit()
        
        return label.numberOfLines
    }
    
    func getHeightForDescriptionLabel(text:String) -> CGFloat {
        
        let label = UILabel()
        
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(name: "HelveticaNeue", size: 16)!
        label.width = UIScreen.main.bounds.size.width - 16
        label.text = text
        
        label.sizeToFit()
        
        return label.height
    }
    
    func createHashtagsText(foodString: String, drinksString: String) -> NSAttributedString {
        var foodAttributedString = NSMutableAttributedString(string: foodString)
        if foodString.count != 0 {
            foodAttributedString = self.createHashtagLink(hashtags: foodString) as! NSMutableAttributedString
            foodAttributedString.insert(NSAttributedString(string: "Food tags: ", attributes: [NSAttributedStringKey.font : UIFont.init(name: "Helvetica", size: 17) as Any]), at: 0)
        }
        
        var drinksAttributedString = NSMutableAttributedString(string: drinksString)
        if drinksString.count != 0 {
            drinksAttributedString = self.createHashtagLink(hashtags: drinksString) as! NSMutableAttributedString
            drinksAttributedString.insert(NSAttributedString(string:"Drink tags: ", attributes: [NSAttributedStringKey.font : UIFont.init(name: "Helvetica", size: 17) as Any]), at: 0)
        }
        
        foodAttributedString.append(drinksAttributedString)
        return foodAttributedString
    }
    
    func searchForFilters() {
        self.filters = ""
        
        let locationName = self.locationName
        let locationHashtags = "location=\(locationName?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        
        let comaHashtags = self.hashtagsString.replacingOccurrences(of: " ", with: ",")
        let foodHashtagsSearch = "&tags=\(comaHashtags.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        
        if locationName != "" && locationName != "Location".localized {
            self.filters += locationHashtags
        }
        
        if self.hashtagsString != "" {
            self.filters += foodHashtagsSearch
        }
        
        self.loadData()
    }
    
    // MARK: admin table view cell delegate
    func userTappedAtHashtag(hashtag: String, tag: NSInteger, index: NSInteger) {
        let hashtagSection = self.getHashtagSection(hashtag:hashtag, tag: tag, index: index)
        let hashtagString = hashtag.replacingOccurrences(of: "#", with: "")
        
        let vc = get(viewController: "HashtagStreamViewController") as! HashtagStreamViewController
        
        vc.title = "Posts with " + hashtag
        vc.filters = "filters[" + hashtagSection + "][value]=\((hashtagString).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")&filters[" + hashtagSection + "][compare]=LIKE"
        
        let vcc = get(viewController: "HashtagCollectionViewController") as! HashtagCollectionViewController
        
        vcc.title = "Posts with " + hashtag
        vcc.filters = "filters[" + hashtagSection + "][value]=\((hashtagString).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")&filters[" + hashtagSection + "][compare]=LIKE"
        
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    func getHashtagSection(hashtag: String, tag: NSInteger, index: NSInteger) -> String {
        switch index {
        case 0:
            return ""
        case 1:
            let foodHashtags = self.data[tag]["food_hashtags"] as! String
            
            if foodHashtags.contains(hashtag) {
                return "food_hashtags"
            }
            
            return "drink_hashtags"
        case 2:
            return "food_hashtags"
        case 3:
            return "drink_hashtags"
        default:
            break;
        }
        
        return ""
    }
    
    // MARK: GMSAutoCompleteViewController delegate
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true) {
            self.locationButton?.setTitle(place.name, for: .normal)
            self.locationButton?.sizeToFit()
            self.locationName = place.name
            var meta:[String:Any] = ["gmsPlaceId":place.placeID]
            
            if place.addressComponents != nil {
                var gmsAdressParts:[String:Any] = [:]
                for component:GMSAddressComponent in place.addressComponents! {
                    if component.type == kGMSPlaceTypeLocality {
                        meta["city"] = component.name
                    }
                    gmsAdressParts[component.type] = component.name
                }
                meta["gmsAdressParts"] = gmsAdressParts
            }
            
            self.placeMeta = meta
            
            let locationName = place.name
            let locationHashtags = "location=\(locationName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            let foodHashtagsSearch = "&tags=\(self.hashtagsString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
            self.filters = locationHashtags + foodHashtagsSearch
            self.loadData()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true) {
            
        }
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true) {
            self.placeMeta = [String:Any]()
            self.locationName = ""
            self.locationButton?.setTitle(self.locationName, for: .normal)
            self.locationButton?.sizeToFit()
            self.searchForFilters()
        }
    }
    
    // MARK: textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField == self.foodTagsTextField {
            self.hashtagsString = textField.text!
            self.searchForFilters()
        }
        
        return true
    }
    
    // MARK: search controller delegate
    func didDismissSearchController(_ searchController: UISearchController) {
        if #available(iOS 11.0, *) {
            navigationItem.searchController?.searchBar.text = self.hashtagsString
        }
    }
    
    // MARK: search bar delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let hashtags = searchBar.text {
            self.hashtagsString = hashtags
            self.searchForFilters()
        }
        else {
            self.hashtagsString = ""
            self.searchForFilters()
        }
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController?.isActive = false
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.hashtagsString = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.hashtagsString = ""
        self.searchForFilters()
        self.loadData()
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if #available(iOS 11.0, *) {
            if navigationItem.searchController != nil && (navigationItem.searchController?.isActive)! {
                navigationItem.searchController?.isActive = false
            }
        }
    }
}
