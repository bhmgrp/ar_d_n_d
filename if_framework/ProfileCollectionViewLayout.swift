//
//  ProfileCollectionViewLayout.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/23/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

protocol TableCollectionViewLayoutDelegate: class {
    func collectionView(_ collectionView:UICollectionView, heightForPhotoAtIndexPath:IndexPath) -> CGFloat
    
    func collectionView(_ collectionView:UICollectionView, heightForTextsAtIndexPath:IndexPath) -> CGFloat
}

class ProfileCollectionViewLayout: UICollectionViewFlowLayout {
    var spacingsHeight : Int = 0
    
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    
    weak var delegate: TableCollectionViewLayoutDelegate?
    var shouldShowHeader: Bool = true
    
    fileprivate var contentHeight: CGFloat = 0
    
    fileprivate var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        guard let collectionView = collectionView else {
            return
        }
        
        cache.removeAll()
        contentHeight = 0
        
        let heightForSupplementary = headerReferenceSize.height
        var yOffset = heightForSupplementary
        let frame = CGRect(x: 0, y: 0, width: Int(collectionView.width), height: Int(heightForSupplementary))
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, with: IndexPath(row: 0, section: 0))
        attributes.frame = frame
        cache.append(attributes)
        
        contentHeight = max(contentHeight, frame.maxY)
        
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath.init(row: item, section: 0)
            
            let photoHeight = delegate?.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
            let textsHeight = delegate?.collectionView(collectionView, heightForTextsAtIndexPath: indexPath)
            let cellHeight = spacingsHeight + Int(photoHeight!) + Int(textsHeight!)
            let frame = CGRect(x: 0, y: Int(yOffset), width: Int(collectionView.width), height: cellHeight)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = frame
            cache.append(attributes)
            
            contentHeight = max(contentHeight, frame.maxY)
            yOffset += CGFloat(cellHeight)
        }
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let collectionView = collectionView else {
            return UICollectionViewLayoutAttributes()
        }
        
        if shouldShowHeader == false {
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
            attributes.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            return attributes
        }

        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
        attributes.frame = CGRect(x: 0, y: 0, width: collectionView.width, height: 184)
        return attributes
    }
    
    override func indexPathsToInsertForSupplementaryView(ofKind elementKind: String) -> [IndexPath] {
        return [IndexPath(row: 0, section: 0)]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard cache.count > indexPath.item else {
            return nil
        }
        
        return cache[indexPath.item]
    }
}
