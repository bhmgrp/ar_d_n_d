//
//  ProfileDetailViewController.swift
//  if_framework
//
//  Created by Christopher on 10/27/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class ProfileDetailViewController: AdminViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var noImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var profileId:Int = 0
    var profileData:[String:Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadData()
        
        self.initStyle(forTableView: self.tableView)
        self.tableView.tableHeaderView = nil
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        DispatchQueue.global().async {
            let data = API.getFrom(SECURED_API_URL + "gestgid/profile/\(self.profileId)")
            
            if data is [String:Any] {
                self.profileData = data as! [String:Any]
                
                if self.profileData["attributes"] is [[String:Any]] {
                    DispatchQueue.main.async {
                        self.data = self.profileData["attributes"] as! [[String:Any]]
                        NSLog("self.data: \(self.data)")
                        self.initProfileData()
                        self.tableView.reloadData()
                    }
                }
                
            }
        }
    }
    
    func initProfileData(){
        self.title = ("\(self.profileData["first_name"] as? String ?? "") \(self.profileData["last_name"] as? String ?? "")").trimmingCharacters(in: .whitespacesAndNewlines)
        
        self.noImageView.image = imageMethods.resize(UIImage(named:"iosicons/ios7-contact-outline.png"), for: CGSize(width: 64, height: 64))
        
        let imagePath = self.profileData["profile_image"] as? String ?? ""
        DispatchQueue.global().async {
            var image:UIImage? = nil
            if(imagePath.hasPrefix("http")){
                image = imageMethods.uiImage(withWebPath: imagePath)
            }
            else {
                image = imageMethods.uiImage(withServerPathName: imagePath, width: 375)
            }
            
            if(image != nil){
                DispatchQueue.main.async {
                    self.profileImageView.image = image
                }
            }
            else {
                DispatchQueue.main.async {
                    
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AdminTableViewCell
        
        cell.label1.text = (self.data[indexPath.row]["attribute"] as? String ?? "").localized
        
        cell.label2.text = (self.data[indexPath.row]["value"] as? String ?? "")
        
        if self.data[indexPath.row]["attribute"] as? String ?? "" == "Birthdate" {
            let birthDateString = self.data[indexPath.row]["value"] as? String ?? ""
            let birthDate = miscFunctions.date(fromEnString: birthDateString, onlyDate: true, onlyTime: false)
            
            cell.label2.text = "\((birthDate ?? Date()).age)"
            cell.label1.text = "Age".localized
        }
        
        return cell
    }
}
