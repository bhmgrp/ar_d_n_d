//
//  CWBadgeView.swift
//  if_framework
//
//  Created by Christopher on 10/19/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class CWBadgeView: UIView {
    
    
    var backgroundView: UIView!
    //@IBOutlet weak var label: UILabel!
    var label: UILabel!
    
    var text:String? {
        didSet {
            self.updatedText()
        }
    }
    
    func setup(){
        self.backgroundView = UIView()
        
        self.backgroundView.frame = bounds
        
        self.backgroundView.backgroundColor = .red
        
        self.backgroundView.layer.cornerRadius = self.backgroundView.height / 2
        
        self.label = UILabel()
        
        self.label.text = "1"
        
        self.label.textAlignment = .center
        
        self.label.font = UIFont(name: "HelveticaNeue-Bold", size: 13)!
        
        self.label.textColor = .white
        
        
        self.adjustFrames()
        
        self.backgroundView.addSubview(label)
        
        self.addSubview(self.backgroundView)
        
        self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .top, relatedBy: .equal, toItem: self.label, attribute: .top, multiplier: 1, constant: 4))
        self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .left, relatedBy: .equal, toItem: self.label, attribute: .left, multiplier: 1, constant: 4))
        //self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .right, relatedBy: .equal, toItem: self.label, attribute: .right, multiplier: 1, constant: 4))
        //self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .bottom, relatedBy: .equal, toItem: self.label, attribute: .bottom, multiplier: 1, constant: 4))
        
        self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.backgroundView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    func adjustFrames() {
        self.label.sizeToFit()
        
        if(self.label.width<self.label.height){
            self.label.width = self.label.height
        }
        
        self.backgroundView.height = self.label.height + 8
        self.backgroundView.width = self.label.width + 8
        
        self.label.x = 4
        self.label.y = 4
        
        self.backgroundView.layer.cornerRadius = self.backgroundView.bounds.size.height / 2
    }
    
    func updatedText() {
        if(self.text == nil || self.text == "" || self.text == "0"){
            self.isHidden = true
        }
        else {
            self.isHidden = false
            self.label.text = self.text
            self.adjustFrames()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
        
        //fatalError("init(coder:) has not been implemented")
    }
}
