//
//  languages.m
//  pickalbatros
//
//  Created by User on 02.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "languages.h"

@implementation languages

+(int)currentLanguageID{
    return [self getCurrentLanguageID];
}

+(int)getCurrentLanguageID{
    NSString *userLanguage = [ifbckGlobals sharedInstance].currentLanguageKey;
    
    //NSLog(@"prefered language: %@", userLanguage);
    
    return [self languageIDForIsoCode:userLanguage];
}

+(int)languageIDForIsoCode:(NSString*)languageIsoCode{
    
    NSDictionary *mapDict = [self languageKeyIDMapping];
    
    if(mapDict[languageIsoCode] != nil){
        return [mapDict[languageIsoCode] intValue];
    }
    
    return 1; // default: english
}


+(NSString*)currentLanguageKey{
    return [self getCurentLanguageKey];
}

+(NSString*)getCurentLanguageKey{
    return [ifbckGlobals sharedInstance].currentLanguageKey;
}

+(NSString*)getLanguageKeyForID:(NSInteger)languageID{
    NSString *languageKey = @"en";
    
    if([self languageIDKeyMapping][[NSNumber numberWithInteger:languageID]]!=nil){
        languageKey = [self languageIDKeyMapping][[NSNumber numberWithInteger:languageID]];
    }
    
    return languageKey;
}

+(NSDictionary*)languageIDKeyMapping{
    return @{
             @1:@"en",
             @0:@"de",
             @2:@"fr",
             @3:@"nl",
             @4:@"cr",
             @5:@"it",
             @6:@"cz",
             @7:@"es",
             @8:@"no",
             @9:@"cn",
             @10:@"ru",
             @11:@"eg",
             @12:@"pl",
             @13:@"gr",
             @14:@"id",
             @15:@"th",
             @16:@"pt",
             @17:@"br",
             @18:@"ro",
             @19:@"tr",
             @20:@"se",
             };
}

+(NSDictionary*)languageKeyIDMapping{
    return @{
             @"en":@1,
             @"de":@0,
             @"fr":@2,
             @"nl":@3,
             @"cr":@4,
             @"it":@5,
             @"cz":@6,
             @"es":@7,
             @"no":@8,
             @"cn":@9,
             @"ru":@10,
             @"eg":@11,
             @"pl":@12,
             @"gr":@13,
             @"id":@14,
             @"th":@15,
             @"pt":@16,
             @"br":@17,
             @"ro":@18,
             @"tr":@19,
             @"se":@20,
             };
}

+(NSDictionary*)languageKeyNameMapping{
    return @{
             @"en":@"English",
             @"de":@"Deutsch",
             @"fr":@"Français",
             @"nl":@"Nederlands",
             @"it":@"Italiano",
             @"hr":@"Hrvatski",
             @"cz":@"Ceški",
             @"es":@"Español",
             @"no":@"Norsk",
             @"cn":@"中国的",
             @"ru":@"русский",
             @"pl":@"Polski",
             @"eg":@"العربي",
             @"pl":@"Polski",
             @"el":@"Ελληνικά",
             @"id":@"Indonesia",
             @"th":@"Thai",
             @"pt":@"Português",
             @"tr":@"Türkçe",
             @"ro":@"Română",
             @"br":@"Brazilian Português",
             @"se":@"Swedish",
             };
}

+(void)setLanguage:(NSString*)languageKey{
    [ifbckGlobals sharedInstance].currentLanguageKey = languageKey;
    [NSBundle setLanguage:languageKey];
}

+(NSString *)getLanguageNameByKey:(NSString *)languageKey{
    return [self languageKeyNameMapping][languageKey];
}


@end
