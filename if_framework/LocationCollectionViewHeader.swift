//
//  LocationCollectionViewHeader.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/25/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation
import MapKit
import GooglePlaces

protocol LocationHeaderDelegate: class {
    func locationHeaderWantsToOpenUrl(url :NSURL)
}

class LocationCollectionViewHeader: UICollectionReusableView, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableButton: UIButton!
    @IBOutlet weak var gridButton: UIButton!
    @IBOutlet weak var viewForBorder: UIView!
    
    weak var delegate: LocationHeaderDelegate?
    
    var place:GMSPlace?
    let locationManager = CLLocationManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mapView.delegate = self
        
        var image = UIImage(named: "grid")
        gridButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        image = UIImage(named: "drag")
        tableButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    func showAlert() {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        let delegate = UIApplication.shared.delegate
        
        if delegate?.responds(to: #selector(getter: window)) == true {
            alertWindow.tintColor = delegate?.window??.tintColor
        }

        let topWindow = UIApplication.shared.windows.last
        alertWindow.windowLevel = (topWindow?.windowLevel)! + 1
        alertWindow.makeKeyAndVisible()
        
        let alertController = UIAlertController(title: "Oops", message: "We couldn`t get a webpage for this place.", preferredStyle: .actionSheet)
        
        let alertAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
            alertWindow.rootViewController?.dismiss(animated: true, completion: {
                
            })
        }
        
        alertController.addAction(alertAction)
        
        alertWindow.rootViewController?.present(alertController, animated: true, completion: {
            
        })
    }
    
    @IBAction fileprivate func webpageButtonTap(_ sender: Any) {
        guard let placeWebpage = place?.website, placeWebpage.absoluteString != "" else {
            self.showAlert()
            return
        }
        
        delegate?.locationHeaderWantsToOpenUrl(url: placeWebpage as NSURL)
//        UIApplication.shared.openURL(placeWebpage)
    }
    
    @IBAction fileprivate func routeButtonTap(_ sender: Any) {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
            
        case .restricted:
            break
            
        case .denied:
            break
            
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        }
    }
    
    // MARK: location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.coordinate = (locations.first?.coordinate)!
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.coordinate = (place?.coordinate)!
        
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        let sourceLocation = sourceAnnotation.coordinate
        let destinationLocation = destinationAnnotation.coordinate
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
}
