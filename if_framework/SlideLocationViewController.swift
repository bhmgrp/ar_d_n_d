//
//  SlideLocationViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 8/27/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import MapKit
import GooglePlaces

enum SlideViewPositionMode {
    case minimal
    case middle
    case maximum
}

fileprivate let navigationBarHeight: CGFloat = 64

class SlideLocationViewController : StartStreamCollectionViewController, LocationHeaderDelegate {

    // MARK: private properties
    @IBOutlet fileprivate weak var mapView: MKMapView!
    
    @IBOutlet fileprivate weak var slideViewTopConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var slideView: UIView!
    @IBOutlet fileprivate weak var slideIndicatorImageView: UIImageView!
    
    fileprivate let followedLocationsStorage = localStorage(filename: "followedLocations")
    
    fileprivate let slideTopPositionCoordinate: CGFloat = 15
    fileprivate let slideMiddlePositionCoordinate: CGFloat = 200
    fileprivate var slideBottomPositionCoordinate: CGFloat {
        get {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            let identifier = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else {
                    return identifier
                }
                
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
            
            if identifier.contains("iPhone10,3") || identifier.contains("iPhone10,6") {
                return UIScreen.main.bounds.height - 200
            }
            
            return UIScreen.main.bounds.height - 124 - 20
        }
    }
    
    // MARK: public properties
    var place: [String:Any] = [:]
    var placeId: String = "" {
        didSet {
            self.place["gmsPlaceId"] = self.placeId
        }
    }
    
    var placeClient: GMSPlacesClient?
    var placeRepresentation: GMSPlace? {
        didSet {
            let center = placeRepresentation!.coordinate

            var region = MKCoordinateRegionMakeWithDistance(center, 250, 250)
            region.center.latitude -= region.span.latitudeDelta * 0.4
            let adjustedRegion = mapView.regionThatFits(region)
            mapView.setRegion(adjustedRegion, animated: true)
            mapView.addAnnotation({
                let annotation = MKPointAnnotation();
                annotation.coordinate = placeRepresentation!.coordinate;
                return annotation
                }())
            
            if placeRepresentation?.addressComponents != nil {
                var gmsAdressParts:[String:Any] = [:]
                for component:GMSAddressComponent in (placeRepresentation?.addressComponents!)! {
                    if component.type == kGMSPlaceTypeLocality {
                        self.place["city"] = component.name
                    }
                    gmsAdressParts[component.type] = component.name
                }
                self.place["gmsAdressParts"] = gmsAdressParts
                self.place["gmsPlaceId"] = placeRepresentation?.placeID
            }
        }
    }
    
    var slideViewPositionMode: SlideViewPositionMode = .minimal {
        didSet {
            switch slideViewPositionMode {
            case .minimal:
                slideViewTopConstraint.constant = slideBottomPositionCoordinate
                slideIndicatorImageView.image = UIImage(named: "bottomIndicator")
                break;
                
            case .middle:
                slideViewTopConstraint.constant = slideMiddlePositionCoordinate
                slideIndicatorImageView.image = UIImage(named: "middleIndicator")
                UIView.animate(withDuration: 0.3) {
                    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, self.slideMiddlePositionCoordinate + 50, 0)
                }
                break;
                
            case .maximum:
                slideViewTopConstraint.constant = slideTopPositionCoordinate
                slideIndicatorImageView.image = UIImage(named: "topIndicator")
                UIView.animate(withDuration: 0.3) {
                    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, self.slideTopPositionCoordinate + 50, 0)
                }
                break;
            }
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableLayout.delegate = self
        
        let cellNib = UINib(nibName: "LocationCollectionViewCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: "locationCollectionViewCell")
        
        let headerNib = UINib(nibName: "LocationCollectionViewHeader", bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "locationCollectionViewHeader")
        
        gridLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 104)
        tableLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 104)
        tableLayout.spacingsHeight = 183
        
        viewMode = .gridViewMode
        
        initClient()
        loadData()
        setupSlideGestures()
        
        if #available(iOS 11.0, *){
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, slideMiddlePositionCoordinate + 50, 0)
        } else {
            collectionView.contentInset = UIEdgeInsetsMake(-65, 0, 0, 0)
        }
    }
    
    // MARK: child methods
    override func initNavigationItems() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Follow".localized, style: .plain, target: self, action: #selector(LocationStreamViewController.followLocation as (LocationStreamViewController) -> () -> ()))
        self.ifIndexOfPlace(place: self.place, then: { (_, _) in
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Unfollow".localized, style: .plain, target: self, action: #selector(LocationStreamViewController.unfollowLocation as (LocationStreamViewController) -> () -> ()))
        }, else: nil)
    }
    
    override func searchForFilters() {
        
    }
    
    // MARK: collection view delegate
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "locationCollectionViewHeader", for: indexPath) as! LocationCollectionViewHeader
        
        header.delegate = self
        header.place = placeRepresentation
        
        header.gridButton.addTarget(self, action: #selector(setGridViewMode), for: .touchUpInside)
        header.tableButton.addTarget(self, action: #selector(setTableViewMode), for: .touchUpInside)
        
        if viewMode == .gridViewMode {
            header.gridButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
            header.tableButton.tintColor = UIColor.lightGray
        } else {
            header.gridButton.tintColor = UIColor.lightGray
            header.tableButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
        }
        
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.data.count > 4 && indexPath.row >= self.data.count - 4 {
            self.loadUpdates()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch viewMode {
        case .gridViewMode:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
            
        case .tableViewMode:
            
            var cell: LocationCollectionViewCell
            if let offerId = data[indexPath.item]["entity_type"] as? String, offerId == "1" {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offerLocationCollectionViewCell", for: indexPath) as! LocationCollectionViewCell
            } else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "locationCollectionViewCell", for: indexPath) as! LocationCollectionViewCell
            }
            
            cell.tag = indexPath.item
            cell.delegate = self
            
            cell.immediateCommentTextField.delegate = self
            cell.immediateCommentTextField.tag = indexPath.row
            cell.immediateCommentTextField.returnKeyType = .send
            
            let headerText = self.data[indexPath.row]["user_name"] as? String ?? ""
            let postText = self.data[indexPath.row]["content"] as? String ?? ""
            
            cell.commentProfileImageView.image = LoggedUserCredentials.sharedInstance.userImage
            
            // take only top 5 comments which is not replies
            if let allComments = data[indexPath.row]["comments"] as? [[String:Any]] {
                let comments = getCommentsFrom(array: allComments)
                cell.setComments(comments)
                
                cell.showAllCommentsTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.topLeftImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: false)
                }
                
                cell.commentButtonTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.topLeftImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: true)
                }
            }
            
            // handling user image
            
            if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
                if self.imageMap[userImageString] != nil {
                    cell.topLeftImageView.image = self.imageMap[userImageString]
                }
                else {
                    DispatchQueue.global().async {
                        var userImage:UIImage? = nil
                        
                        if(userImageString.hasPrefix("http")){
                            userImage = imageMethods.uiImage(withWebPath: userImageString)
                        }
                        else {
                            userImage = imageMethods.uiImage(withServerPathName: userImageString, width: 36)
                        }
                        
                        DispatchQueue.main.async {
                            if userImage != nil && cell.tag == indexPath.row {
                                self.imageMap[userImageString] = userImage
                                cell.topLeftImageView.image = userImage
                            }
                            else {
                                self.imageMap[userImageString] = UIImage()
                                cell.topLeftImageView.image = nil
                            }
                        }
                    }
                }
            }
            else {
                cell.topLeftImageView.image = nil
            }
            
            // handling post image
            
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let image = self.imageMap[postImageString]
                    
                    cell.mainImageView.image = image
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
                    //                    else if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
                    //                    imageMap[postImageString] = image
                    //                    cell.mainImageView?.image = image
                    //                }
                    //                else {
                    //                    let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
                    //                    imageDownloadOperation.completionHandler = { [weak self] (image) in
                    //                        guard let strongSelf = self else {
                    //                            return
                    //                        }
                    //
                    //                        DispatchQueue.main.async {
                    //                            cell.mainImageView?.image = image
                    //                        }
                    //
                    //                        strongSelf.imageMap[postImageString] = image
                    //                        strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
                    //                    }
                    //
                    //                    imageDownloadOperationQueue.addOperation(imageDownloadOperation)
                    //                    imageDownloadOperations[indexPath] = imageDownloadOperation
                    //                }
                else {
                    DispatchQueue.global().async {
                        let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
                        DispatchQueue.main.async {
                            if postImage != nil && cell.tag == indexPath.row {
                                self.imageMap[postImageString] = postImage
                                let image = self.imageMap[postImageString]
                                
                                cell.mainImageView?.image = image
                                cell.setNeedsLayout()
                                cell.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
            
            cell.profilePictureTapHandler = {
                self.getProfileScreenHandler(indexPath: indexPath)
            }
            
            cell.headerLabel.text = headerText
            cell.postTextLabel.text = postText
            cell.postTextLabel.numberOfLines = 0
            cell.postTextLabel.lineBreakMode = .byWordWrapping
            cell.postTextLabel.font = UIFont.systemFont(ofSize: 17)
            
            if cell.postTextLabel.text != "" {
                cell.postTextLabel.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 13
                cell.postTextLabel.sizeToFit()
                cell.postTextLabelHeight.constant = cell.postTextLabel.height
            } else {
                cell.postTextLabelHeight.constant = 0
            }
            
            cell.offerButtonTapHandler = {
                self.offerButtonTapHandler(indexPath: indexPath)
            }
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            if cell.hashtagsTextView.text != "" {
                cell.hashtagsTextView?.width = collectionView.width - 5
                cell.hashtagsTextView?.sizeToFit()
                cell.hashtagsTextViewHeight?.constant = (cell.hashtagsTextView?.height)!
            } else {
                cell.hashtagsTextViewHeight?.constant = 0
            }
            
            var likesCount = data[indexPath.row]["likes_number"] as? String ?? "0"
            cell.likeButton.setTitle(likesCount, for: .normal)
            
            if let likedPost = data[indexPath.row]["liked_by_me"] as? String {
                if likedPost == "1" {
                    cell.liked = true
                } else {
                    cell.liked = false
                }
            }
            
            cell.likeButtonTapHandler = {
                var likeCount = Int(likesCount)!
                if !cell.liked == true {
                    likeCount += 1
                    self.data[indexPath.row]["liked_by_me"] = "1"
                } else {
                    if likeCount != 0 {
                        likeCount -= 1
                    }
                    self.data[indexPath.row]["liked_by_me"] = "0"
                }
                
                likesCount = String(likeCount)
                self.data[indexPath.row]["likes_number"] = likeCount
                cell.likeButton.setTitle(likesCount, for: .normal)
                
                cell.liked = !cell.liked
                
                if let postId = self.data[indexPath.row]["id"] as? String {
                    let post = ["id":postId]
                    _ = AdminAPI.post(url: "ardnd/post/toggle-like/\(postId)", post: post)
                }
            }
            
            cell.shareButtonTapHandler = {
                let text = cell.postTextLabel.text ?? ""
                let image = cell.mainImageView.image ?? UIImage()
                self.shareButtonHandler(text: text, image: image)
            }
            
            return cell
        }
    }
    
    // MARK: handling map
    func initMapviewFor(gmsPlaceId:String){
        self.placeClient?.lookUpPlaceID(gmsPlaceId, callback: { (gmsPlace, err) in
            guard gmsPlace != nil else {
                return
            }
            
            self.placeRepresentation = gmsPlace
            self.collectionView.reloadData()
        })
    }
    
    // MARK: location header delegate
    func locationHeaderWantsToOpenUrl(url: NSURL) {
        let webViewController = WebViewController.getWebViewController()
        webViewController?.titleString = title
        webViewController?.url = url as URL?
        webViewController?.setContentOffsetToZero()
        
        self.navigationController?.pushViewController(webViewController!, animated: true)
    }
    
    // MARK: gesture recognizers
    @objc func panRecognizerHandler(_ panRecognizer: UIPanGestureRecognizer) {
        switch panRecognizer.state {
        case .began:
            break;
            
        case .changed:
            moveSlideViewToPoint(panRecognizer.location(in: mapView))
            break;
            
        case .possible:
            break;
            
        case .failed:
            break;
            
        case .cancelled:
            break;
            
        case .ended:
            let slideY = slideView.frame.origin.y - navigationBarHeight
            
            let minimumCoordinateDifference = abs(slideBottomPositionCoordinate - slideY)
            let middleCoordinateDifference = abs(slideMiddlePositionCoordinate - slideY)
            let maximumCoordinateDifference = abs(slideTopPositionCoordinate - slideY)
            
            let differencesArray = [minimumCoordinateDifference, middleCoordinateDifference, maximumCoordinateDifference]
            
            switch differencesArray.min() {
            case minimumCoordinateDifference:
                slideViewPositionMode = .minimal
                break;
                
            case middleCoordinateDifference:
                slideViewPositionMode = .middle
                break;
            
            case maximumCoordinateDifference:
                slideViewPositionMode = .maximum
                break;
                
            default:
                break;
            }
        }
    }
}

fileprivate extension SlideLocationViewController {
    func initClient(){
        self.placeClient = GMSPlacesClient.init()
        
        if self.placeClient != nil{
            if self.placeId != "" {
                NSLog(" LocationStreamVC - showing place for id")
                self.initMapviewFor(gmsPlaceId: self.placeId)
            }
                
            else {
                NSLog(" LocationStreamVC - showing place by name")
                self.placeClient?.autocompleteQuery(self.title ?? "NO LOCATION FOUND", bounds: nil, filter: nil, callback: { (prediction, error) in
                    if prediction != nil {
                        if let placeID = prediction?.first?.placeID {
                            self.initMapviewFor(gmsPlaceId: placeID)
                        }
                    }
                })
            }
        }
    }
    
    func ifIndexOfPlace(place:[String:Any],then doThen:((Int, inout [[String:Any]])->Void)?,else doElse:((inout [[String:Any]])->Void)?){
        
        if var places = self.followedLocationsStorage?.getObjectForKey("followedLocations") as? [[String:Any]] {
            if let index = places.index(where: { (element) -> Bool in
                return element["name"] as? String == place["name"] as? String
            }) {
                NSLog(" LocationStreamVC - having this place")
                if doThen != nil {
                    doThen!(index, &places)
                }
            }
            else {
                if doElse != nil {
                    doElse!(&places)
                }
            }
        }
        else {
            NSLog(" LocationStreamVC - dont have any places yet")
            var places = [] as! [[String:Any]]
            if doElse != nil {
                doElse!(&places)
            }
        }
    }

    func setupSlideGestures() {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panRecognizerHandler(_:)))
        
        slideIndicatorImageView.addGestureRecognizer(panRecognizer)
    }
    
    func moveSlideViewToPoint(_ point: CGPoint) {
        let y = point.y
        guard y >= slideTopPositionCoordinate else {
            return;
        }
        
        guard y <= slideBottomPositionCoordinate else {
            return;
        }
        
        slideViewTopConstraint.constant = y
        view.layoutIfNeeded()
    }
    
    func getCoordinatesDifference(_ firstCoordinate: CGFloat, _ secondCoordinate: CGFloat) -> CGFloat {
        let minCoord = min(firstCoordinate, secondCoordinate)
        let maxCoord = max(firstCoordinate, secondCoordinate)
        
        return maxCoord - minCoord
    }
}
