//
//  StructureOverViewController.m
//  pickalbatros
//
//  Created by User on 10.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "StructureOverViewController.h"
#import "StructureOverViewCollectionViewCell.h"
#import "iFeedBackViewCell.h"
#import "MasterViewControllerNew.h"
#import "imageMethods.h"
#import "WebViewController.h"
#import "MBProgressHUD.h"
#import "PlaceOverViewController.h"
#import "SettingsViewController.h"
#import "NavigationViewController.h"
#import "OnboardingViewController.h"
#import "ImageViewController.h"

#import "BookingViewTypeViewController.h"

#import "DynamicFormViewController.h"

@interface StructureOverViewController (){
    imageMethods *images;
    NSString *documentsPath;
    
    BOOL sendFeedbackButtonPressed;
    int numbersOfItemsPerRow;
    
    int numOfSectionsInTab;
}

@property (nonatomic) NSMutableDictionary *MasterViewControllers;

@property (nonatomic) localStorage* structureStorage;

@property (nonatomic) NSDictionary* selectedItem;

@end

@implementation StructureOverViewController

- (void)viewDidLoad {
    
    numOfSectionsInTab = NUMBEROFTABSINTABVIEW;
    
    [super viewDidLoad];
    
    self.structureStorage = [localStorage storageWithFilename:@"structures"];
    
    self.iFeedbackButtonView.backgroundColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];
    self.iFeedbackButtonView.backgroundColor = [UIColor clearColor];
    
    self.iFeedbackButtonLabel.textColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
    self.iFeedbackButtonArrow.image = [self.iFeedbackButtonArrow.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_iFeedbackButtonArrow setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    
    if(IDIOM==IPAD){
        //        UIImageView *headerImageShadow = [[UIImageView alloc] initWithImage:_headerImageView.image];
        //
        //        headerImageShadow.frame = _headerImageView.frame;
        //        headerImageShadow.center = CGPointMake(headerImageShadow.center.x+2, headerImageShadow.center.y+2);
        //
        //        headerImageShadow.tintColor = [UIColor darkGrayColor];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadStructures) name:@"updateStructures" object:nil];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];
    if([self.title isEqualToString:@""] || self.title == nil){
        self.title = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"];
    }
    
    NSLog(@" Structur over view title => %@ ", self.title);
    
    self.navigationItem.title = self.title;
    
    if(true){
        self.navigationItem.leftBarButtonItem = self.sideBarButton;
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.navigationItem.leftBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    }
    
    [self.headerImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.headerImageView setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    
    [self initNavigationBarButtons];
    
    images = [[imageMethods alloc] init];
    documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    numbersOfItemsPerRow = 2;
    
    if(_placeID == 0){
        _placeID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceID"]intValue];
    }
    
    self.stovcBackgroundImageView.image = [self getBackgroundImage:self.sliderImagesString];
    
    [self loadData];
    
    self.headerSubheadlineLabel.text = NSLocalizedString(@"Discover hotel ameneties",@"Subheadline for main page");
    self.headerSubSubHeadlineLabel.text = NSLocalizedString(@"Enjoy your stay with\nour digital services", @"");
    
    self.exploreMoreLabel.text = NSLocalizedString(@"EXPLORE MORE", @"");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:TERMINAL_MODE_UPDATE object:nil];
    
    if([UIScreen mainScreen].bounds.size.height < 568){
        [_descriptionView removeFromSuperview];
    }
    
    for(UIImageView* img in _arrowImages){
        [img setTintColor:[UIColor whiteColor]];
    }
    
    _pageControll.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    [self translateOutlets];
    
    NSDictionary *placeDict = [[localStorage storageWithFilename:@"currentPlace"] objectForKey:@"selectedPlaceDictionary"];
    _placeHeaderDescription.text = placeDict[@"header"];
    _placeDescription.text = placeDict[@"description"];
    
    _MasterViewControllers = [@{} mutableCopy];
}

-(void)translateOutlets{
    _iFeedbackButtonLabel.text = NSLocalizedString(@"Give iFeedback®", @"");
}

-(void)initNavigationBarRightButton{
    if(SINGLEPLACEID==0){
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        
        UIImage *locImage = [self.locationButtonImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.locationButtonImage.image = nil;
        [self.locationButtonImage setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        self.locationButtonImage.image = locImage;
        
        self.navigationItem.rightBarButtonItem = self.locationBarButton;
    }
    else {
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        
        UIImage *locImage = [self.settingsButtonImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.settingsButtonImage.image = nil;
        [self.settingsButtonImage setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
        self.settingsButtonImage.image = locImage;
        
        self.navigationItem.rightBarButtonItem = self.settingsBarButton;
    }
}

-(void)initNavigationBarLeftButton{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
}

-(void)initNavigationBarButtons{
    [self initNavigationBarLeftButton];
    [self initNavigationBarRightButton];
}

- (IBAction)settingsButtonClicked:(id)sender {
    [self settingsClick];
}

- (void) settingsClick {
    if([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1){
        SettingsViewController *svc = [SettingsViewController loadNowFromStoryboard:@"Settings"];
        
        svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:svc.revealViewController action:@selector(revealToggle:)];
        svc.settingType.text = @"generalSettings";
        [self.navigationController pushViewController:svc animated:YES];
    } else {
        OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
        ovc.modalPresentationStyle = UIModalPresentationFullScreen;
        ovc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self.revealViewController pushFrontViewController:ovc animated:YES];
    }
}

- (IBAction)locationButtonClicked:(id)sender {
    [self switchToPlace];
}

- (void) switchToPlace {
    PlaceOverViewController *povc = [PlaceOverViewController loadNowFromStoryboard:@"PlaceOverView"];
    povc.placeID = 0;
    povc.showSidebarButton = NO;
    [self.navigationController pushViewController:povc animated:YES];
}

-(void)loadData{
    localStorage *currentPlaceStorage = [localStorage storageWithFilename:@"currentPlace"];
    NSDictionary *place = [currentPlaceStorage objectForKey:@"selectedPlaceDictionary"];
    
    NSArray *tempArray = place[@"treestructures"];
        
    NSLog(@"loading data for placeID: %i", _placeID);
    
    
    if(tempArray == nil || tempArray.count == 0){
        NSLog(@"loading from url");
        // defining the URL
        NSString *urlString = [NSString stringWithFormat:@"%@get/gestgid/place/%i/treestructures/%i", API_URL, _placeID,[languages currentLanguageID]];
        // Set the URL where we load from
        
        NSLog(@"%@", urlString);
        
        _jsonFileUrl = [NSURL URLWithString:urlString];
        
        [self downloadItems];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Loading", @"");
        
    } else {
        
        self.dataArray = [@[] mutableCopy];
        self.data = [@[] mutableCopy];
        
        int i = 0;
        
        for(NSDictionary *dict in tempArray){
            
            if(!([dict[@"hide_in_terminal_mode"] boolValue] && [globals sharedInstance].terminalMode)){
                [_data addObject:dict];
                if(i>numOfSectionsInTab-1+((int)[globals sharedInstance].terminalMode)){
                    [_dataArray addObject:dict];
                }
                i++;
            }
        }
        
        NSLog(@"data collected: %@", _data);
        
        [self.collectionView reloadData];
    }
}

-(void)deleteSettings{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedPlaceID"];
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==_dataArray.count-1){
        _pageControll.currentPage = 1;
    }
    if(indexPath.row==0){
        _pageControll.currentPage = 0;
    }
}

-(void)viewDidLayoutSubviews{
    
    [self.headerImageBorderView.layer setBorderColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR].CGColor];
    [self.headerImageBorderView.layer setCornerRadius:15.0f];
    [self.headerImageBorderView.layer setBorderWidth:1.0f];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if(self.tabBarController.tabBar.hidden){
        [UIView animateWithDuration:0.5 animations:^{
            self.tabBarController.tabBar.alpha = 1.0f;
        } completion:^(BOOL finished) {
            if(finished)
                self.tabBarController.tabBar.hidden = NO;
        }];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.view.height = [UIScreen mainScreen].bounds.size.height;
    
    if(self.revealViewController.panGestureRecognizer!=nil){
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_dataArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == _dataArray.count){
        StructureOverViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"iFCell" forIndexPath:indexPath];
        
        return cell;
    } else {
        StructureOverViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"structureCell" forIndexPath:indexPath];
        cell.textAndImageColor = [UIColor blackColor];
        [cell updateColors];
        cell.structureNameLabel.text = [_dataArray[indexPath.row][@"name"] uppercaseString];
        cell.structureSubtitleLabel.text = [_dataArray[indexPath.row][@"subtitle"] isEqualToString:@""]?@" ":_dataArray[indexPath.row][@"subtitle"];
        cell.structureImageView.image = [self loadImageForItem:_dataArray[indexPath.row]];
        
        [cell.structureImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [cell.layer setCornerRadius:5.0f];
        
        return cell;
    }
}

// cell sizes
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float multi = [UIScreen mainScreen].bounds.size.height / 568.0f;
    if(IDIOM == IPAD){
        return CGSizeMake(162,152);
    }
    
    float width;
    
    width = ([UIScreen mainScreen].bounds.size.width - (3*15)) / 2;
    
    
    return CGSizeMake(width,113*multi);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"element clicked");
    
    if(_MasterViewControllers[[NSString stringWithFormat:@"%li", (long)indexPath.row]]!=nil){
        [self.navigationController pushViewController:_MasterViewControllers[[NSString stringWithFormat:@"%li", (long)indexPath.row]] animated:YES];
        return;
    }
    
    UIViewController *vc = [self getViewControllerForIndex:indexPath.row delegate:self];
    
    [miscFunctions showLoadingAnimationForView:self.view];
    
    if(vc!=nil){
        NSLog(@"pushing view controller");
        _MasterViewControllers[[NSString stringWithFormat:@"%li", (long)indexPath.row]] = vc;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (void)downloadItems
{
    // Download the json file
    
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_jsonFileUrl];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _dataArray = [@[] mutableCopy];
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
    int i = 0;
    for(NSDictionary *dict in jsonArray){
        if(!([dict[@"hide_in_terminal_mode"] boolValue] && [globals sharedInstance].terminalMode)){
            [_data addObject:dict];
            if(!(i==0||i==1)){
                [_dataArray addObject:dict];
            }
            i++;
        }
    }
    
    
    int numOfItems = (int)[jsonArray count];
    i = 0;
    for(NSDictionary *item in jsonArray){
        if([self loadImageForItem:item] != nil){
            
        }
        i++;
        if(i >= numOfItems){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [_collectionView reloadData];
        }
    }
    
    [self.structureStorage setObject:jsonArray forKey:[NSString stringWithFormat:@"structureListForPlace%i",_placeID ]];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"Connection failed, Error: %@" ,error);
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"") message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil, nil] show];
}


- (IBAction)iFeedbackButtonClicked:(UIButton *)sender {
    if(sendFeedbackButtonPressed)
        return;
    sendFeedbackButtonPressed = YES;
    [self loadIFeedbackModelWithUrl:[globals sharedInstance].iFeedbackLink];
}

- (void)reloadStructures{
    
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get/gestgid/place/%i/treestructures/%i", API_URL, _placeID, [languages currentLanguageID]];
    // Set the URL where we load from
    
    NSLog(@"%@", urlString);
    
    _jsonFileUrl = [NSURL URLWithString:urlString];
    
    [self downloadItems];
}


-(UIImage*)getBackgroundImage:(NSString*)sliderImagesString{
    
    NSDictionary *selectedPlaceDictionary = [[localStorage storageWithFilename:@"currentPlace"] objectForKey:@"selectedPlaceDictionary"];
    
    UIImage *image = nil;
    NSString *sliderImagesStringtmp = selectedPlaceDictionary[@"app_images_slider"];
    //NSLog(@"sliderImagesStrin: %@",sliderImagesString);
    if(sliderImagesString==nil)sliderImagesString = sliderImagesStringtmp;
    
    NSArray *imagePaths = [sliderImagesString componentsSeparatedByString:@","];
    
    NSString *structureImageSm = imagePaths[0];
    NSLog(@"imagePaths[0]: %@", imagePaths[0]);
    //NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@",selectedPlaceDictionary[@"tstamp"],selectedPlaceDictionary[@"uid"]];
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@",[[structureImageSm lastPathComponent]stringByDeletingPathExtension],selectedPlaceDictionary[@"uid"]];
    
    NSLog(@"imageNameFromTstamp: %@", [NSString stringWithFormat:@"%@-%@",[[structureImageSm lastPathComponent]stringByDeletingPathExtension],selectedPlaceDictionary[@"uid"]]);
    
    if(![structureImageSm isEqualToString:@""] && structureImageSm != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            image = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            image = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
        }
        else {
            //Get Image From URL
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@",SYSTEM_DOMAIN,structureImageSm]];
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            image = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
        }
        
    }
    
    if(image==nil)image=self.stovcBackgroundImageView.image;
    
    return image;
}



-(UIImage*)loadImageForItem:(NSDictionary*)item{
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@",item[@"tstamp"],item[@"uid"]];
    UIImage *cellImage = nil;
    NSString *structureImageSm = item[@"image_sm"];
    if(![structureImageSm isEqualToString:@""] && structureImageSm != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else {
            //Get Image From URL
            //          40 Pixel Icons for iPhone 5
            float imageDevicePoints = 40;
            float mainScreenHeight = [UIScreen mainScreen].bounds.size.height;
            if(mainScreenHeight>=568){
                imageDevicePoints = 40; //iPhone 5
            }
            if(mainScreenHeight>=667){
                imageDevicePoints = 63; //iPhone 6
            }
            if(mainScreenHeight>=736){
                imageDevicePoints = 80; // iPhone 6+
            }
            
            
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm,imageDevicePoints*3]];
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            return cellImage;
        }
        
    }
    
    return cellImage;
}

-(void)setSideBarGesture{
    if(_swrvc!=nil){
        [self.view addGestureRecognizer:_swrvc.panGestureRecognizer];
        NSLog(@"setting sidebar gesture");
    }
}

-(void)setSideBarGestureFor:(SWRevealViewController*)revealViewController{
    if(revealViewController!=nil){
        [self.view addGestureRecognizer:revealViewController.panGestureRecognizer];
        NSLog(@"setting sidebar gesture");
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
}

-(void)openViewForUrl:(NSString*)viewUrl{
    
    NSString *classString = [viewUrl stringByReplacingOccurrencesOfString:@"ifbck://" withString:@""];
    
    
    Class theClass = NSClassFromString(classString);
    UIViewController *vc = [theClass loadNowFromStoryboard:classString];
    
    if(vc!=nil){
        
        if([vc isKindOfClass:[ImageViewController class]]){
            if([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1){
                
                ImageViewController *ivc = (ImageViewController*)vc;
                
                ivc.imageName = _selectedItem[@"image_big"];
                
                [self.navigationController pushViewController:ivc animated:YES];
            } else {
                OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
                //            [self.revealViewController.frontViewController.navigationController pushViewController:rvc animated:YES];
                
                ovc.modalPresentationStyle = UIModalPresentationFullScreen;
                ovc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self.navigationController presentViewController:ovc animated:YES completion:^{
                    [UIApplication sharedApplication].keyWindow.rootViewController = ovc;
                }];
                
            }
            return;
        }
        
        if([vc isKindOfClass:[OfferViewController class]]){
            
            int selectedOfferCampaignID = [_selectedItem[@"campaign_id"] intValue];
            
            OfferViewController *ovc = (OfferViewController*)vc;
            
            ovc.offerCampaignID = selectedOfferCampaignID;
            
            [self.navigationController pushViewController:ovc animated:YES];

            NSLog(@"opening offer view controller");
            return;
        }
        
        if([vc isKindOfClass:[DynamicFormViewController class]]){
            DynamicFormViewController* dvc = (DynamicFormViewController*) vc;
            
            vc = dvc;
        }
        
        NSLog(@" stov title 4 => %@ ", _selectedItem[@"name"]);
        
        if(vc.navigationItem.titleView == nil) {
            vc.navigationItem.titleView = [NavigationTitle createNavTitle:_selectedItem[@"name"] SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        }
        
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        NSString *versionString = [NSString stringWithFormat:@"v.%@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        
        [[[UIAlertView alloc] initWithTitle:@"Could not open" message:[NSString stringWithFormat:@"Maybe the app is not up to date?\n%@",versionString] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
    }
}

-(UIViewController*)getViewControllerForUrl:(NSString*)viewUrl{
    
    NSString *classString = [viewUrl stringByReplacingOccurrencesOfString:@"ifbck://" withString:@""];
    
    Class theClass = NSClassFromString(classString);
    UIViewController *vc = [theClass loadNowFromStoryboard:classString];
    
    if(vc!=nil){
        
        NSLog(@" stov title 5 => %@ ", _selectedItem[@"name"]);
        NSLog(@" stov 5 => %@ ", vc.navigationItem.titleView);
        
        if(vc.navigationItem.titleView == nil) {
            vc.navigationItem.titleView = [NavigationTitle createNavTitle:_selectedItem[@"name"] SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        }
        
        if([vc isKindOfClass:[ImageViewController class]]){
            if([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1){
                
                ImageViewController *ivc = (ImageViewController*)vc;
                
                ivc.imageName = _selectedItem[@"image_big"];
                
                return ivc;
            } else {
                OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
                return ovc;
            }
            return nil;
        }
        
        if([vc isKindOfClass:[OfferViewController class]]){
            
            int selectedOfferCampaignID = [_selectedItem[@"campaign_id"] intValue];
            
            OfferViewController *ovc = (OfferViewController*)vc;
            
            ovc.offerCampaignID = selectedOfferCampaignID;
            
            return ovc;
        }
        
        if([vc isKindOfClass:[DynamicFormViewController class]]){
            DynamicFormViewController* dvc = (DynamicFormViewController*) vc;
            
            return dvc;
        }
        
        return vc;
    }
    else {
        // error when the class couldn't be found
        //        NSString *versionString = [NSString stringWithFormat:@"v.%@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        //
        //        [[[UIAlertView alloc] initWithTitle:@"Could not open" message:[NSString stringWithFormat:@"Maybe the app is not up to date?\n%@",versionString] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
    }
    return nil;
}


-(void)loadIFeedbackModelWithUrl:(NSString*)url{
    [self loadIfeedbackModelWithUrl:url title:nil];
}

-(void)loadIfeedbackModelWithUrl:(NSString*)url title:(NSString*)title{
    [self loadIfeedbackModelWithUrl:url title:title delegate:nil];
}

-(void)loadIfeedbackModelWithUrl:(NSString*)url title:(NSString*)title delegate:(id<ifbckDelegate>)delegate{
    [self loadIfeedbackModelWithUrl:url title:title delegate:delegate index:0];
}

-(void)loadIfeedbackModelWithUrl:(NSString*)url title:(NSString*)title delegate:(id<ifbckDelegate>)delegate index:(NSUInteger)index{
    
    if(delegate==nil){
        delegate = self;
    }

    ifbck *i = [ifbck new];
    i.delegate = delegate;
    i.navigationBarHeight = 44;
    [i loadViewControllerForURL:url atIndex:index];
}

-(UIViewController*)getViewControllerForIndex:(NSInteger)index delegate:(id<ifbckDelegate>)delegate{
    if(delegate==self || delegate==nil)index=index+numOfSectionsInTab;
    
    if([_data[index][@"viewtype"] intValue] == 2){
        NSString *bookingViewTypeStoryboardName = @"BookingViewTypeViewController";
        
        if(IDIOM==IPAD){
            bookingViewTypeStoryboardName = @"BookingViewTypeViewController-iPad";
        }
        
        BookingViewTypeViewController *vc = [BookingViewTypeViewController loadNowFromStoryboard:bookingViewTypeStoryboardName];
        
        NSLog(@" stov title 1 => %@ ", _data[index][@"name"]);
        
        vc.navigationItem.titleView = [NavigationTitle createNavTitle:_data[index][@"name"] SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        vc.bookingDetail = _data[index][@"first_child_element"];
        
        return vc;
    }
    else if([_data[index][@"description"] hasPrefix:@"http"]){
        NSString *url = _data[index][@"description"];
        
        // check if it's an ifeedback url
        NSString *regex = @"^http(s)?://([a-zA-Z0-9]+\\.)?ifbck.com/[a-zA-Z0-9-_]+$";
        
        NSPredicate *testString = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        
        if([testString evaluateWithObject:url]) {
            if (delegate == self || delegate == nil) {
                [self loadIfeedbackModelWithUrl:url title:_data[index][@"name"] delegate:delegate index:index];
                return nil;
            }
            else {
                ifbck *i = [ifbck new];
                i.delegate = delegate;
                [i loadViewControllerForURL:url atIndex:index];
            }
            return [UIViewController load:@"LoadingViewController" fromStoryboard:@"Loading"];;
        }
        
        WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
        wvc.url = [NSURL URLWithString:url];
        wvc.urlGiven = YES;
        
        wvc.hideSideBarButton = YES;
        wvc.titleString = _data[index][@"name"];
        
        return wvc;
    }
    else if([_data[index][@"description"] hasPrefix:@"ifbck://"]){
        _selectedItem = _data[index];
        if(delegate==self || delegate==nil){
            [self openViewForUrl:_data[index][@"description"]];
        }
        else {
            return [self getViewControllerForUrl:_data[index][@"description"]];
        }
    }
    else {
        NSLog(@"init master view controller");
        MasterViewControllerNew *vc = [MasterViewControllerNew loadNowFromStoryboard:@"TreeStructure"];
        
        vc.elementID = [_data[index][@"root_element_id"] intValue];
        
        NSLog(@" stov title 2 => %@ ", _data[index][@"name"]);
        
        vc.navigationItem.titleView = [NavigationTitle createNavTitle:_data[index][@"name"] SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        vc.textAboveTheTable = @"";
        vc.cameFromMaster = @"YES";
        
        return vc;
    }
    return nil;
}

- (void)ifbckDidLoadViewController:(UIViewController *)viewController {
    if([viewController isKindOfClass:[SWRevealViewController class]]){
        UIViewController *vc = ((SWRevealViewController *)viewController).frontViewController;

        if([vc isKindOfClass:[UINavigationController class]]){
            CGRect frame;

            frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
            UIViewController *vcWrapper = [UIViewController new];
            vcWrapper.title = @"iFeedback®";
            vcWrapper.view.frame = frame;
            vcWrapper.view.backgroundColor = [UIColor whiteColor];
            [vcWrapper.view addSubview:viewController.view];
            [vcWrapper addChildViewController:viewController];
            [viewController didMoveToParentViewController:vcWrapper];
            frame = CGRectMake(0,64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64);
            viewController.view.frame = frame;
            frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64);
            vc.view.frame = frame;

            if(self.navigationController.topViewController==self)
                [self.navigationController pushViewController:vcWrapper animated:YES];
            else {
                [((NavigationViewController *) self.navigationController) addNavigationBarBackgroundToViewController:vcWrapper];
                [self.navigationController setViewControllers:@[self, vcWrapper]];
            }

            [UIView animateWithDuration:0.5 animations:^{
                self.tabBarController.tabBar.alpha = 0.0f;
            } completion:^(BOOL finished) {
                self.tabBarController.tabBar.hidden = YES;
            }];
        }
    }
}

@end
