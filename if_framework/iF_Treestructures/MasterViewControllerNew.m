//
//  MasterViewControllerNew.m
//  KAMEHA
//
//  Created by User on 08.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "MasterViewControllerNew.h"
#import "Structure.h"
#import "StructureTableViewCell.h"
#import "DetailViewControllerNew.h"
#import "SWRevealViewController.h"
#import "StructureTableViewCell.h"
#import "MasterDescriptionTableViewCell.h"
#import "RegisterHeaderViewController.h"
#import "MBProgressHUD.h"
#import "shareMenu.h"

#import "StructureImageViewController.h"

#define getPlaceParameter(s) ([[self.selectedPlaceStorage objectForKey:@"selectedPlaceDictionary"][@"parameters"] isKindOfClass:[NSDictionary class]]?[self.selectedPlaceStorage objectForKey:@"selectedPlaceDictionary"][@"parameters"][s]:nil)
#define placeParameters [self.selectedPlaceStorage objectForKey:@"selectedPlaceDictionary"][@"parameters"]

//#import "MasterResultsTableViewController.h"

@interface MasterViewControllerNew ()<UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

{
    homeModel *_homeModel;
    NSMutableArray *_feedItems;
    NSMutableArray *_searchResults;
    NSMutableArray *_filteredResults;
    BOOL alreadyReloaded;
    BOOL canResetSearch;
    BOOL imageExisting;
    
    NSArray *_headerSectionCells;
    NSArray *_dataSections;
    
    imageMethods *images;
    NSString *documentsPath;
}

@property (nonatomic) localStorage *masterStorage, *selectedPlaceStorage;

@property (nonatomic, strong) UISearchController *searchController;

// our secondary search results table view
@property (nonatomic, strong) UITableViewController *resultsTableController;

@property (nonatomic) UIRefreshControl *refreshControll;

@property (nonatomic) UIColor *cellBackgroundColor, *cellTextColor, *dividerBackgroundColor, *dividerTextColor;

@end

@implementation MasterViewControllerNew

- (void)viewDidLoad {
    [super viewDidLoad];



    self.masterStorage = [localStorage storageWithFilename:@"homeModel"];
    self.selectedPlaceStorage = [localStorage storageWithFilename:[NSString stringWithFormat:@"place%li", (long)[[NSUserDefaults standardUserDefaults] integerForKey:@"selectedPlaceID"]]];

    // define colors
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.cellBackgroundColor = [UIColor clearColor];
    self.cellTextColor = [UIColor blackColor];
    self.dividerBackgroundColor = [UIColor groupTableViewBackgroundColor];
    self.dividerTextColor = [UIColor colorWithRGBHex:0x303030];
    
    NSLog(@"parameters: %@", placeParameters);
    
    if(getPlaceParameter(@"app_treestructure_list_background_color") != nil){
        self.view.backgroundColor = [UIColor colorFromHexString:getPlaceParameter(@"app_treestructure_list_background_color")];
        self.tableView.backgroundColor = [UIColor colorFromHexString:getPlaceParameter(@"app_treestructure_list_background_color")];
    }

    if(getPlaceParameter(@"app_treestructure_list_cell_background_color") != nil){
        self.cellBackgroundColor = [UIColor colorFromHexString:getPlaceParameter(@"app_treestructure_list_cell_background_color")];
    }

    if(getPlaceParameter(@"app_treestructure_list_cell_text_color") != nil){
        self.cellTextColor = [UIColor colorFromHexString:getPlaceParameter(@"app_treestructure_list_cell_text_color")];
    }

    if(getPlaceParameter(@"app_treestructure_list_divider_background_color") != nil){
        self.dividerBackgroundColor = [UIColor colorFromHexString:getPlaceParameter(@"app_treestructure_list_divider_background_color")];
    }

    if(getPlaceParameter(@"app_treestructure_list_divider_text_color") != nil){
        self.dividerTextColor = [UIColor colorFromHexString:getPlaceParameter(@"app_treestructure_list_divider_text_color")];
    }

    self.tableView.separatorColor = self.dividerBackgroundColor;

    images = [[imageMethods alloc] init];
    documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    _refreshControll = [[UIRefreshControl alloc] init];
    [_refreshControll addTarget:self action:@selector(refreshContent) forControlEvents:UIControlEventValueChanged];
    _refreshControll.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pull to refresh",@"") attributes:nil];
    _refreshControll.backgroundColor = [UIColor colorWithRed:247/255.0f green:247/255.0f blue:247/255.0f alpha:1.0f];
    
    [_tableView addSubview:_refreshControll];
    
    if(self.preloadImages && self.imageString != nil && ![[self.imageString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        NSLog(@"imageString: %@",self.imageString);
        
        [self initImage];
    }
    
    [self registerForKeyboardNotifications];
    
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self setNeedsStatusBarAppearanceUpdate];
    
    if(self.revealViewController.panGestureRecognizer!=nil){
        [self.navigationController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    // Sidebar Stuff
    if([self.cameFromMaster isEqualToString:@"YES"]){
        // show the back button, we already have it so do nothing
    } else {
        // show the sidebar Button
        //[self.sidebarButton setTintColor:[UIColor blackColor]];
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        self.navigationItem.leftBarButtonItem = self.sidebarButton;
    }
    
    // right bar button item (hidden because we have nothing to share)
    //self.navigationItem.rightBarButtonItem = _shareBarButtonItem;
    //[_shareButtonImage setImage:[_shareButtonImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    //[_shareButtonImage setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    
    // Set this view controller object as the delegate and data source for the table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // Create array object and assign it to _feedItems variable
    _feedItems = [@[] mutableCopy];
    _searchResults = [@[] mutableCopy];
    _filteredResults = [@[] mutableCopy];
    
    // Create new HomeModel object and assign it to _homeModel variable
    _homeModel = [[homeModel alloc] init];
    
    // set the root ID if not set
    if(!_rootID){
        self.rootID = self.elementID;
    }
    
    //NSLog(@"root id: %i", _rootID);
    NSString *getMasterDescriptionString = @"&getMasterDescription";
    /*
     if([self.textAboveTheTable isEqualToString:@""] || self.textAboveTheTable == nil){
     getMasterDescriptionString = @"&getMasterDescription=1";
     }
     */
    NSData *tempData = (NSData*)[self.masterStorage objectForKey:[NSString stringWithFormat:@"dataForStructureMasterViewID%i",_elementID]];
    
    if(tempData == nil || self.shouldReloadItems){
        // defining the URL
        NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonElementsFor=%i&getViewtypeForRootID=%i%@&languageID=%i", API_URL, IFBCKPID, _elementID, _rootID, getMasterDescriptionString, [languages currentLanguageID]];
        
        NSLog(@"%@", urlString);
        
        // Set the element id (for saving the json locally)
        _homeModel.elementID = self.elementID;
        
        // Set the URL where we load from
        _homeModel.jsonFileUrl = [NSURL URLWithString:urlString];
        
        // Set this view controller object as the delegate for the home model object
        _homeModel.delegate = self;
        
        // Call the download items method of the home model object
        [_homeModel downloadItems];
        
        // Loading Screen
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Loading",@"");
    } else {
        
        //NSError *error;
        //NSLog(@"%@",[NSJSONSerialization JSONObjectWithData:tempData options:NSJSONReadingAllowFragments error:&error]);
        // Set this view controller object as the delegate for the home model object
        _homeModel.delegate = self;
        [_homeModel parseDownloadedData:tempData];
        NSLog(@"master elements reused");
    }


}

- (void)initImage{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.cellImage = [self createImageFromWebPath:self.imageString];
    
    [self performSelectorOnMainThread:@selector(imageDownloaded) withObject:nil waitUntilDone:NO];
}

- (void)imageDownloaded{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];

}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)return indexPath;
    return indexPath;
}

-(void)itemsDownloaded:(NSMutableArray *)items
{
    // This delegate method will get called when the items are finished downloading
    if(items != nil){
        //[[NSUserDefaults standardUserDefaults] setObject:(NSArray*)items forKey:[NSString stringWithFormat:@"structureItemsForID%i",_elementID]];
        //[[NSUserDefaults standardUserDefaults] synchronize];
    }
    // Set the downloaded items to the array
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [_feedItems addObjectsFromArray:items];
    
    _feedItems = items;
    
    if(_feedItems.count > 0){
        
        Structure *item = _feedItems[0];
        
        NSMutableArray *sectionZeroCells = [@[] mutableCopy];
        
        if(self.imageString != nil){
            [sectionZeroCells addObject:@{@"cellIdentifier":@"imageCell"}];
        } else if(item.rootImage != nil && ![item.rootImage isEqualToString:@""]){
            NSLog(@"going through the elements, found an image");
                self.imageString = item.rootImage;
                [sectionZeroCells addObject:@{@"cellIdentifier":@"imageCell"}];
                
                [self performSelectorInBackground:@selector(initImage) withObject:nil];
                
                imageExisting = YES;
            //[self.tableView reloadData];
        }
        if(![item.rootDescription isKindOfClass:[NSNull class]]){
            if(![[item.rootDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && item.rootDescription != nil){
                [sectionZeroCells addObject:@{@"cellIdentifier":@"structureDescriptionTableViewCell",@"text":item.rootDescription}];
            }
        }
        
        
        if(item.globalviewtype == 1){
            NSMutableArray *otherSections =  [@[] mutableCopy];
            int createNewSection = 1;
            NSString* sectionTitle = @"";
            NSMutableArray *sectionCells = [@[] mutableCopy];
            int u = 0;
            while(u < items.count){
                Structure *i = items[u];
                //[tempSearchResults addObject:i];
                if(![i.cellIdentifier isEqualToString:@"deviderCell"]){
                    [sectionCells addObject:i];
                    [_searchResults addObject:i];
                }
                else {
                    if(createNewSection == 1){
                        sectionTitle = i.name;
                        createNewSection = 0;
                    }
                    else {
                        u--;
                        //createSection = 0;
                        createNewSection = 1;
                        
                        [otherSections addObject:@{@"sectionHeader":sectionTitle,@"sectionCells":sectionCells}];
                        sectionCells = [@[] mutableCopy];
                        //createSection = 1;
                    }
                }
                u++;
            }
            if(u>0){
                [otherSections addObject:@{@"sectionHeader":sectionTitle,@"sectionCells":sectionCells}];
                //sectionCells = [@[] mutableCopy];
                
                _dataSections = otherSections;
            }
        }
        else {
            
            NSMutableArray *otherSections =  [@[] mutableCopy];
            [otherSections addObject:@{@"sectionHeader":@"",@"sectionCells":items}];
            _dataSections = otherSections;
            [_searchResults addObjectsFromArray:items];
        }
        
        _headerSectionCells = sectionZeroCells;
        // else reload the table view
        
        if(item.globalviewtype == 1 && !self.isSubLevel){
            self.viewType = 1;
            //[sectionZeroCells addObject:@{@"cellIdentifier":@"searchBarTableViewCell"}];
        }
        if(self.viewType == 1){
            [self loadSearchBar];
        }
        
        
        [self.tableView reloadData];
        //NSLog(@"items: %@", items);
    }
    [_refreshControll endRefreshing];
    _refreshControll.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pull to refresh",@"") attributes:nil];
}

- (void)initData{
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView == self.tableView){
        return 1+[_dataSections count];
    }
    else {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.tableView){
        if(section == 0){
            return [_headerSectionCells count];
        } else {
            return [_dataSections[section-1][@"sectionCells"] count];
        }
    } else {
        return [_filteredResults count];
        //return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.tableView){
        if(indexPath.section == 0){
            NSString *cellIdentifier = _headerSectionCells[indexPath.row][@"cellIdentifier"];
            if([cellIdentifier isEqualToString:@"structureDescriptionTableViewCell"]){
                MasterDescriptionTableViewCell *c = (MasterDescriptionTableViewCell*)[self getDescriptionCellForIndexPath:indexPath];
                
                [c.descriptionTextArea setEditable:NO];
                [c.descriptionTextArea setSelectable:NO];
                [c.descriptionTextArea setScrollEnabled:NO];

                c.descriptionTextArea.textColor = self.cellTextColor;
                c.backgroundColor = self.cellBackgroundColor;
                return c;
            }
            else {
                StructureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                cell.cellImage.image = self.cellImage;

                cell.backgroundColor = self.cellBackgroundColor;
                return cell;
            }
        }
        else {
            StructureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elementTableViewCell"];
            Structure *item = (Structure*)_dataSections[indexPath.section-1][@"sectionCells"][indexPath.row];
            cell.nameLabel.text = item.name;
            //cell.nameLabel.text =
            
            if(item.childs == 0){
                if(item.text1 == nil || ([item.text1 isKindOfClass:[NSString class]] && [item.text1 isEqualToString:@""])){
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                else {
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            }

            cell.nameLabel.textColor = self.cellTextColor;
            cell.backgroundColor = self.cellBackgroundColor;
            return cell;
        }
    }
    else {
        StructureTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"elementTableViewCell"];
        Structure *item = (Structure*)_filteredResults[indexPath.row];
        cell.nameLabel.text = item.name;
        
        if(item.text1 == nil || ([item.text1 isKindOfClass:[NSString class]] && [item.text1 isEqualToString:@""])){
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }

        cell.nameLabel.textColor = self.cellTextColor;
        cell.backgroundColor = self.cellBackgroundColor;
        return cell;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView==self.tableView){
        if(indexPath.section == 0){
            NSString *cellIdentifier = _headerSectionCells[indexPath.row][@"cellIdentifier"];
            if([cellIdentifier isEqualToString:@"structureDescriptionTableViewCell"]){
                MasterDescriptionTableViewCell *c = (MasterDescriptionTableViewCell*)[self getDescriptionCellForIndexPath:indexPath];
                [c.descriptionTextArea sizeToFit];
                return c.descriptionTextArea.height+4;
            }
            else {
                if(self.imageString != nil && self.cellImage != nil){
                    //NSLog(@"imagestring not null, cellimage not null");
                    return 209 * ([UIScreen mainScreen].bounds.size.width / 320);
                } else return 0;
            }
        }
        else {
            return 45;
        }
    }
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 0;
    }
    else {
        if([_dataSections[section-1][@"sectionHeader"] isEqualToString:@""]){
            return 0;
        } else {
            return 30;
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(tableView == self.tableView){
        if(section == 0){
            UIView *emptyV = [[UIView alloc] init];
            emptyV.height = 0;
            return emptyV;
        }
        else {
            if([_dataSections[section-1][@"sectionHeader"] isEqualToString:@""]){
                UIView *emptyV = [[UIView alloc] init];
                emptyV.height = 0;
                return emptyV;
            }
            else {
                RegisterHeaderViewController *rhvc = [RegisterHeaderViewController loadNowFromStoryboard:@"LoginAndRegister"];
                
                rhvc.view.tag = 0;

                rhvc.view.backgroundColor = self.dividerBackgroundColor;
                
                rhvc.labelHeaderTitle.text = _dataSections[section-1][@"sectionHeader"];

                rhvc.labelHeaderTitle.textColor = self.dividerTextColor;
                
                return rhvc.view;
            }
        }
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView == self.tableView){
        if(indexPath.section != 0){
            Structure *item = _dataSections[indexPath.section-1][@"sectionCells"][indexPath.row];
            
            if(item.childs >= 1){
                
                MasterViewControllerNew *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MasterViewControllerNew"];
                
                NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@",(![item.image_big isKindOfClass:[NSNull class]])?[[item.image_big lastPathComponent]stringByDeletingPathExtension]:@""];
                
                // if we have a png with that filename load it
                if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
                    vc.preloadImages = YES;
                }
                // else if we have a jpg with that filename load it
                else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
                    vc.preloadImages = YES;
                }
                // else get it from the url
                else {
                    // dont preload
                }
                
                vc.shouldReloadItems = self.shouldReloadItems;
                vc.elementName = item.name;
                vc.elementID = item.uid;
                vc.rootID = self.rootID;
                vc.isSubLevel = YES;
                vc.viewType = self.viewType;
                vc.navigationItem.title = item.name;
                vc.imageString = item.image_big;
                
                vc.cameFromMaster = @"YES";
                
                [self.navigationController pushViewController:vc animated:YES];
                
            } else if(item.childs == 0){
                if(item.text1 ==nil || item.text1.class == [NSNull class] || [item.text1 isEqualToString:@""]){
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    return;
                }
                
                DetailViewControllerNew *dvn = [DetailViewControllerNew loadNowFromStoryboard:@"TreeStructure"];
                //if([self createImageForElement:item]!=nil){
                
                //} else {
                //dvn.ignoreImages = YES;
                //}
                
                
                dvn.navigationItem.title = item.name;
                
                dvn.headerText1 = item.name;
                if(self.viewType == 1){
                    dvn.headerText2 = _dataSections[indexPath.section-1][@"sectionHeader"];
                } else {
                    dvn.headerText2 = self.elementName;
                }
                if(![item.image_big isKindOfClass:[NSNull class]]){
                    if(item.image_big == nil ||
                       [[item.image_big stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
                        dvn.ignoreImages = YES;
                    } else {
                        NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%i-%i-%@",item.tstamp,item.uid,item.name];
                        
                        NSLog(@"imageNameFromTstamp: %@",imageNameFromTstamp);
                        if(item.name != nil && ![item.name isKindOfClass:[NSNull class]]){
                            if(![item.name isEqualToString:@""]){
                                if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
                                    // if png image exists:
                                    dvn.preLoadImage = YES;
                                }
                                else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
                                    // if jpg image exists:
                                    dvn.preLoadImage = YES;
                                }
                                else {
                                    //Get Image From URL
                                }
                            }
                        }
                    }
                } else dvn.ignoreImages = YES;
                
                
                dvn.priceText = item.text5;
                dvn.descriptionText = item.text1;
                dvn.imageString = item.image_big;
                
                dvn.viewType = self.viewType;
                //NSLog(@"%i",dvn.viewType);
                
                dvn.uid = item.uid;
                dvn.tstamp = item.tstamp;
                
                [self.navigationController pushViewController:dvn animated:YES];
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
            
        }
        else {
            NSString *cellIdentifier = _headerSectionCells[indexPath.row][@"cellIdentifier"];
            if([cellIdentifier isEqualToString:@"structureDescriptionTableViewCell"]){
            }
            else {
                StructureImageViewController *sivc = [StructureImageViewController loadNowFromStoryboard:@"StructureImageViewController"];
                
                //sivc.image = self.cellImage;
                
                sivc.imageServerPath = self.imageString;
                
                [self.navigationController presentViewController:sivc animated:YES completion:^(void){}];
            }
        }
    }
    else if(tableView == self.resultsTableController.tableView){
        Structure *item = _filteredResults[indexPath.row];
        
        if(item.text1 ==nil || item.text1.class == [NSNull class] || [item.text1 isEqualToString:@""]){
            return;
        }
        
        DetailViewControllerNew *dvn = [DetailViewControllerNew loadNowFromStoryboard:@"TreeStructure"];
        
        dvn.navigationItem.title = item.name;
        
        dvn.headerText1 = item.name;
        dvn.headerText2 = self.elementName;
        
        if(![item.image_big isKindOfClass:[NSNull class]]){
            if(item.image_big == nil ||
               [[item.image_big stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
                dvn.ignoreImages = YES;
            } else {
                
            }
        } else {
            dvn.ignoreImages = YES;
        }
        
        dvn.priceText = item.text5;
        dvn.descriptionText = item.text1;
        dvn.imageString = item.image_big;
        
        dvn.viewType = self.viewType;
        //NSLog(@"%i",dvn.viewType);
        
        dvn.uid = item.uid;
        dvn.tstamp = item.tstamp;
        
        [self.navigationController pushViewController:dvn animated:YES];
        
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }
}

//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
//
//    [self filterContentForSearchText:searchString
//                               scope:nil];
//    /*[[self.searchDisplayController.searchBar scopeButtonTitles]
//     objectAtIndex:[self.searchDisplayController.searchBar
//     selectedScopeButtonIndex]]*/
//
//    return YES;
//}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    
    return YES;
}


-(void)hideSearchBar{
    
}

- (void)reloadItems{
    self.shouldReloadItems = YES;
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonElementsFor=%i&getViewtypeForRootID=%i%@&languageID=%i", API_URL, IFBCKPID, _elementID, _rootID, @"",[languages currentLanguageID]];
    
    NSLog(@"refresh: %@", urlString);
    
    // Set the URL where we load from
    _homeModel.jsonFileUrl = [NSURL URLWithString:urlString];
    
    // Set this view controller object as the delegate for the home model object
    _homeModel.delegate = self;
    
    // Call the download items method of the home model object
    [_homeModel downloadItems];
    
    // Loading Screen
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
}

-(UIImage*)createImageForElement:(Structure*)item{
    UIImage *cellImage = nil;
    
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%i-%i-%@",item.tstamp,item.uid,item.name];
    NSString *imageName = [NSString stringWithFormat:@"%@",item.image_big];
    
    NSLog(@"imageNameFromTstamp: %@",imageNameFromTstamp);
    if(item.name != nil && ![item.name isKindOfClass:[NSNull class]]){
        if(![item.name isEqualToString:@""]){
            if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
                // if png image exists:
                //Load Image From Directory
                cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
                //NSLog(@"image reused: %@", imageNameFromTstamp);
            }
            else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
                // if jpg image exists:
                //Load Image From Directory
                cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
                //NSLog(@"image reused: %@", imageNameFromTstamp);
            }
            else {
                NSLog(@"imageName: %@", imageName);
                //Get Image From URL
                UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@",SYSTEM_DOMAIN,imageName]];
                NSString *imageType = @"";
                if([imageName hasSuffix:@".png"] ||
                   [imageName hasSuffix:@".PNG"]){
                    imageType = @"png";
                }
                else if([imageName hasSuffix:@".jpg"] ||
                        [imageName hasSuffix:@".jpeg"] ||
                        [imageName hasSuffix:@".JPEG"] ||
                        [imageName hasSuffix:@".JPG"]){
                    imageType = @"jpg";
                }
                
                //Save Image to Directory
                [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
                
                //Load Image From Directory
                cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
                
                //NSLog(@"new image saved: %@", imageNameFromTstamp);
            }
        }
    }
    return cellImage;
}

-(UIImage*)createImageFromWebPath:(NSString*)path{
    UIImage *cellImage = nil;
    
    NSString *structureImageSm = path;
    
    
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@",(![structureImageSm isKindOfClass:[NSNull class]])?[[structureImageSm lastPathComponent]stringByDeletingPathExtension]:@""];
    
    // if we have a png with that filename load it
    if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
        cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
        NSLog(@"png image reused: %@", imageNameFromTstamp);
    }
    // else if we have a jpg with that filename load it
    else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
        cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
        NSLog(@"jpg image reused: %@", imageNameFromTstamp);
    }
    // else get it from the url
    else {
        UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@&w=%f",SYSTEM_DOMAIN,structureImageSm,[UIScreen mainScreen].bounds.size.width*2]];
        NSString *imageType = @"";
        if([structureImageSm hasSuffix:@".png"] ||
           [structureImageSm hasSuffix:@".PNG"]){
            imageType = @"png";
        }
        else if([structureImageSm hasSuffix:@".jpg"] ||
                [structureImageSm hasSuffix:@".jpeg"] ||
                [structureImageSm hasSuffix:@".JPEG"] ||
                [structureImageSm hasSuffix:@".JPG"]){
            imageType = @"jpg";
        }
        //NSLog(@"image from url: http://www.ifbck.com/%@",structureImageSm);
        //NSLog(@"documents path: %@",documentsPath);
        //NSLog(@"imageFromURL: %@",imageFromURL);
        //NSLog(@"imageNameFromTstamp: %@",imageNameFromTstamp);
        //NSLog(@"imageType: %@", imageType);
        //Save Image to Directory
        [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
        
        //Load Image From Directory
        cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
        
        NSLog(@"new image saved: %@", imageNameFromTstamp);
    }
    
    //cell.imageViewPlaceNavigation.image = cellImage;
    return cellImage;
    
}

// to get rid of the warnings
-(void)rootItemsDownloaded:(NSArray *)items{}
-(void)downloadStarted{}

// search bar stuff

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UISearchControllerDelegate

// Called after the search controller's search bar has agreed to begin editing or when
// 'active' is set to YES.
// If you choose not to present the controller yourself or do not implement this method,
// a default presentation is performed on your behalf.
//
// Implement this method if the default presentation is not adequate for your purposes.
//
- (void)presentSearchController:(UISearchController *)searchController {
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    // do something after the search controller is presented
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    // do something before the search controller is dismissed
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    // do something after the search controller is dismissed
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [_searchResults mutableCopy];
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    // build all the "AND" expressions for each value in the searchString
    //
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
        //
        // example if searchItems contains "iphone 599 2007":
        //      name CONTAINS[c] "iphone"
        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
        //
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"name"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        lhs = [NSExpression expressionForKeyPath:@"text1"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // at this OR predicate to our master AND predicate
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }
    
    // match up the fields of the Product object
    NSCompoundPredicate *finalCompoundPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
    
    // hand over the filtered results to our search results table
    UITableViewController *tableController = (UITableViewController*)self.searchController.searchResultsController;
    //tableController.filteredProducts = searchResults;
    _filteredResults = searchResults;
    [tableController.tableView reloadData];
    
    
}

-(void)loadSearchBar{
    
    _resultsTableController = [[UITableViewController alloc]init];
    _resultsTableController.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    [self.searchController.searchBar setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleDefault;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = self;
    self.resultsTableController.tableView.dataSource = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES; // default is YES
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    //
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
}

- (IBAction)shareButtonPressed:(id)sender {
    UIBarButtonItem *_sender = (UIBarButtonItem*)sender;
    [shareMenu shareDialogueInViewController:self fromSourceView:(UIView*)_sender];
}

-(UITableViewCell*)getDescriptionCellForIndexPath:(NSIndexPath*)indexPath{
    NSString *cellIdentifier = _headerSectionCells[indexPath.row][@"cellIdentifier"];
    MasterDescriptionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *headerString =_headerSectionCells[indexPath.row][@"text"];
    headerString = [[[NSAttributedString alloc] initWithData:[headerString dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                          documentAttributes:nil error:nil] string];
    cell.descriptionTextArea.text = [headerString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    cell.descriptionTextArea.width = [UIScreen mainScreen].bounds.size.width - 24;
    return cell;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    @try {
        if(!imageExisting)
            if(![self.imageString isKindOfClass:[NSNull class]])
                if(!self.preloadImages &&
                   self.imageString != nil &&
                   ![[self.imageString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] &&
                   self.cellImage == nil){
                    NSLog(@"viewDidAppear, loading image");
                    
                    self.cellImage = [self createImageFromWebPath:self.imageString];
                    
                    NSMutableArray *indexPaths = [@[] mutableCopy];
                    
                    long count = [self.tableView numberOfRowsInSection:0];
                    
                    for (int i = 0; i < count; i++) {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                    imageExisting = YES;
                    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                }
    } @catch (NSException *e){
        
    }
}

-(void)refreshContent{
    _refreshControll.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Loading",@"") attributes:nil];
    
    self.shouldReloadItems = YES;
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonElementsFor=%i&getViewtypeForRootID=%i%@&languageID=%i", API_URL, IFBCKPID, _elementID, _rootID, @"", [languages currentLanguageID]];
    
    NSLog(@"refresh: %@", urlString);
    
    // Set the URL where we load from
    _homeModel.jsonFileUrl = [NSURL URLWithString:urlString];
    
    // Set this view controller object as the delegate for the home model object
    _homeModel.delegate = self;
    
    // Call the download items method of the home model object
    [_homeModel downloadItems];
    
}

-(void)homeModelDownloadFailedWithError:(NSString *)error{
    [_refreshControll endRefreshing];
    _refreshControll.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pull to refresh",@"") attributes:nil];
}


#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height));
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    //    NSDictionary* info = [aNotification userInfo];
    //
    //    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    //
    //    [UIView animateWithDuration:0.5 animations:^(void){
    //        self.navigationController.view.frame = CGRectMake(0, -(64+_headerView.height), [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) +_headerView.height+64);
    //    }];
    //
    //
    //    //self.headerView.height = 0;
    //    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}

@end
