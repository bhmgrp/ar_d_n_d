//
//  homeModel.m
//  Database
//
//  Created by User on 16.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import "homeModel.h"
//#import "Element.h"
#import "Structure.h"

@interface homeModel ()


@end


@implementation homeModel


- (void)downloadItems
{
    // Download the json file
    if(self.delegate){
        if([self.delegate respondsToSelector:@selector(downloadStarted)]){
            [self.delegate downloadStarted];
        }
    }
    
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_jsonFileUrl];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [self parseDownloadedData:_downloadedData];
    
}

-(void)parseDownloadedData:(NSData*)downloadedData{
    
    // Create an array to store the locations
    NSMutableArray *_structures = [[NSMutableArray alloc] init];
    
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:downloadedData options:NSJSONReadingAllowFragments error:&error];
    
    // Loop through Json objects, create question objects and add them to our questions array
    int numOfElements = (int)[jsonArray count];
    int i = 0;
    
    int localviewType = 0;
    
    if(numOfElements > 0){
        
        [[localStorage storageWithFilename:@"homeModel"] setObject:downloadedData forKey:[NSString stringWithFormat:@"dataForStructureMasterViewID%i",_elementID]];
        
        NSDictionary *firstElement = jsonArray[0];
        
        if(firstElement[@"globalviewtype"] != nil){
            localviewType = ![firstElement[@"globalviewtype"] isKindOfClass:[NSNull class]]?[firstElement[@"globalviewtype"] intValue]:0;
        } else {
            localviewType = 0;
        }
        
        for (i = 0; i < numOfElements; i++){
            NSDictionary *jsonElement = jsonArray[i];
            
            // Create a new location object and set its props to JsonElement properties
            Structure *newStructure = [[Structure alloc] init];
            
            newStructure.name = [self stringByParsingHtmlString:jsonElement[@"name"]];
            newStructure.uid = [jsonElement[@"uid"] intValue];
            newStructure.globalviewtype = localviewType;
            
            int flags = [jsonElement[@"flags"] intValue];
            int childs = [jsonElement[@"children"] intValue];
            
            newStructure.flags = flags;
            newStructure.tstamp = [jsonElement[@"tstamp"] intValue];
            
            // translating flags
            if(flags == 2  && childs == 0){
                newStructure.backgroundColor = [UIColor blackColor];
                newStructure.fontColor = [UIColor whiteColor];
                newStructure.cellShouldBeDisabled = YES;
                newStructure.align = NSTextAlignmentCenter;
                newStructure.align = NSTextAlignmentLeft;
                newStructure.cellShouldBeHidden = NO;
                newStructure.accessory = UITableViewCellAccessoryNone;
                newStructure.cellHeight = 22;
                newStructure.cellIdentifier = @"deviderCell";
            } else if(flags == 1){
                newStructure.cellShouldBeHidden = YES;
            } else {
                newStructure.backgroundColor = [UIColor whiteColor];
                newStructure.fontColor = [UIColor blackColor];
                newStructure.cellShouldBeDisabled = NO;
                newStructure.align = NSTextAlignmentLeft;
                newStructure.cellShouldBeHidden = NO;
                newStructure.accessory = UITableViewCellAccessoryDisclosureIndicator;
                newStructure.cellIdentifier = @"TableCell";
                if(jsonElement[@"text5"]!=nil && ![jsonElement[@"text5"] isKindOfClass:[NSNull class]] && ![jsonElement[@"text5"] isEqualToString:@""]){
                    newStructure.cellHeight = 69;
                } else {
                    newStructure.cellHeight = 45;
                }
            }
            
            // "workaround" if thats a usual Tree Element
            if(jsonElement[@"text1"]){
                newStructure.text1 = jsonElement[@"text1"];
                newStructure.text2 = jsonElement[@"text2"];
                newStructure.text3 = jsonElement[@"text3"];
                newStructure.text4 = jsonElement[@"text4"];
                newStructure.text5 = jsonElement[@"text5"];
                newStructure.image_sm = jsonElement[@"img_sm"];
                newStructure.image_big = jsonElement[@"img_big"];
                newStructure.childs = [jsonElement[@"childs"] intValue];
            }
            
            if(jsonElement[@"root_description"]){
                newStructure.rootDescription = jsonElement[@"root_description"];
            }
            
            if(jsonElement[@"root_image"]){
                newStructure.rootImage = jsonElement[@"root_image"];
            }
            // else if it is a Tree
            if(jsonElement[@"root_element_id"]){
                newStructure.sorting = ![jsonElement[@"sorting"] isKindOfClass:[NSNull class]]?[jsonElement[@"sorting"] intValue]:0;
                newStructure.root_element_id = ![jsonElement[@"root_element_id"] isKindOfClass:[NSNull class]]?[jsonElement[@"root_element_id"] intValue]:0;
                newStructure.viewtype = ![jsonElement[@"viewtype"] isKindOfClass:[NSNull class]]?[jsonElement[@"viewtype"] intValue]:0;
                newStructure.image_sm = jsonElement[@"image_sm"];
                if(jsonElement[@"root_description"]){
                    newStructure.rootDescription = jsonElement[@"root_description"];
                }
            }
            
            // Add this structure element to the array
            [_structures addObject:newStructure];
        }
        
    }
    
    // Ready to notify delegate that data is ready and pass back items
    if (self.delegate)
    {
        if(self.rootItems){
            [self.delegate rootItemsDownloaded:_structures];
        }
        [self.delegate itemsDownloaded:_structures];
        
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
    UIAlertView *webError = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error",@"") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles: nil];
    
    [webError show];

    // Ready to notify delegate that data is ready and pass back items
    if (self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(homeModelDownloadFailedWithError:)]){
            [self.delegate homeModelDownloadFailedWithError:error.localizedDescription];
        }
        
    }
    
}

-(NSString*)stringByParsingHtmlString:(NSString*)s{
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    s = [s stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    s = [s stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    s = [s stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    return s;
}


@end
