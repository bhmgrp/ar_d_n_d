//
//  DetailTableViewCell.h
//  pickalbatros
//
//  Created by User on 24.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelHeadline;
@property (weak, nonatomic) IBOutlet UILabel *labelSubHeadline;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imageCellImageView;

@property (weak, nonatomic) IBOutlet UIView *webViewWrapper;
@property (weak, nonatomic) IBOutlet UIWebView *descriptionWebView;


@end
