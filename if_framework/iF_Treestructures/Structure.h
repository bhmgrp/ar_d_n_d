//
//  Structures.h
//  Database
//
//  Created by User on 17.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Structure : NSObject

@property (nonatomic) int uid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) int root_element_id;
@property (nonatomic) int sorting;
@property (nonatomic) int viewtype;
@property (nonatomic) int globalviewtype;
@property (nonatomic) int flags;
@property (nonatomic) int childs;
@property (nonatomic, strong) NSString *image_sm, *image_big;
@property (nonatomic, strong) NSString *text1, *text2, *text3, *text4, *text5;
@property (nonatomic) NSString *rootDescription, *rootImage;
@property (nonatomic) int tstamp;

// style for the cells
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *fontColor;
@property (nonatomic) NSTextAlignment align;
@property (nonatomic) UITableViewCellAccessoryType accessory;
@property (nonatomic) BOOL cellShouldBeDisabled;
@property (nonatomic) BOOL cellShouldBeHidden;
@property (nonatomic) float cellHeight;
@property (nonatomic) NSString *cellIdentifier;

// for error
@property (nonatomic) BOOL hasAnError;

@end
