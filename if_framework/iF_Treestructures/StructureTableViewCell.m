//
//  StructureTableViewCell.m
//  Database
//
//  Created by User on 17.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import "StructureTableViewCell.h"

@implementation StructureTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
