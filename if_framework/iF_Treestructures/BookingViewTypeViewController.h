//
//  BookingViewTypeViewController.h
//  if_framework
//
//  Created by User on 3/8/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingViewTypeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *headerImageBorderView;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageIcon;

- (IBAction)bookingButtonClicked:(id)sender;

@property (nonatomic) NSDictionary *bookingDetail;

@property (weak, nonatomic) IBOutlet UILabel *text4Label;

@end
