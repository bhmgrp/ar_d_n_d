//
//  NavigationManager.swift
//  gestgid
//
//  Created by Vadym Patalakh on 8/20/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class NavigationManager: NSObject {
    fileprivate static func pushViewController(presentingViewController: UIViewController, presentedViewController: UIViewController) {
        if let navigationController = presentingViewController as? UINavigationController {
            navigationController.pushViewController(presentedViewController, animated: true)
        } else {
            presentingViewController.show(presentedViewController, sender: presentingViewController)
        }
    }
    
    static func openLocationScreen(_ presentingController: UIViewController, locationName: String, placeId: String) {
        let locationViewController = get(viewController: "SlideLocationViewController") as! SlideLocationViewController
        
        locationViewController.title = locationName
        locationViewController.filters = "location=\(locationName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        
        locationViewController.placeId = placeId
        
        NavigationManager.pushViewController(presentingViewController: presentingController, presentedViewController: locationViewController)
    }
    
    static func openProfileScreen(_ presentingController: UIViewController, title: String, userId: String, imageMap: [String:UIImage], userImage: UIImage) {
        let profileViewController = get(viewController: "ProfileCollectionViewController") as! ProfileCollectionViewController
        profileViewController.title = title
        profileViewController.filters = "filters[if3_users.id]=\(userId)"
        profileViewController.imageMap = imageMap
        profileViewController.userImage = userImage
        profileViewController.profileId = userId
        
        NavigationManager.pushViewController(presentingViewController: presentingController, presentedViewController: profileViewController)
    }
    
    static func openHashtagScreen(_ presentingController: UIViewController, hashtag: String, hashtagSection: String) {
        let hashtagViewController = get(viewController: "HashtagCollectionViewController") as! HashtagCollectionViewController
        
        hashtagViewController.title = "Posts with " + hashtag
        let hashtagString = hashtag.replacingOccurrences(of: "#", with: "")
        hashtagViewController.filters = "filters[" + hashtagSection + "][value]=\((hashtagString).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")&filters[" + hashtagSection + "][compare]=LIKE"
        
        NavigationManager.pushViewController(presentingViewController: presentingController, presentedViewController: hashtagViewController)
    }
}
