//
//  MyOffersListViewController.swift
//  if_framework
//
//  Created by Christopher on 10/4/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class MyOffersListViewController: AdminViewController {

    // ooutlets
    @IBOutlet weak var tableView: UITableView!


    // constants
    let userSettingsStorage = localStorage(filename: "dynamicUserSettingsValues")
    
    // variables
    var selectedIndexPath:IndexPath? = nil
    var username:String = ""
    

    // methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initGoogleAPIS()
        
        title = "My Offers".localized
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "   ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(self.newOffer))
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 105, 0, 0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.contentInset = UIEdgeInsets(top: -60, left: 0, bottom: 0, right: 0)
        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
        
        self.tableView.tableHeaderView = refreshControl
        
        self.refreshControl.addTarget(self, action: #selector(initData), for: .valueChanged)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: CGFloat(18.0))!, NSAttributedStringKey.foregroundColor: UIColor.black]
        
        self.initUsername()
        self.initData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(selectedIndexPath != nil){
            self.tableView.reloadRows(at: [selectedIndexPath!], with: .fade)
        }
    
    }
    
    func initUsername(){
        self.username = "\(self.userSettingsStorage?.object(forKey: "firstName") as? String ?? "") \(self.userSettingsStorage?.object(forKey: "lastName") as? String ?? "")".trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    @objc func initData(){
        self.showLoading()
        
        // get data from server
        DispatchQueue.global().async {
            if let webData = API.getFrom(SECURED_API_URL+"get/offer/my-offers/ardnd") as? [NSDictionary] {
                
                self.data = []
                
                for dictionary in webData {
                    self.data.append(dictionary.mutableCopy() as! [String:AnyObject])
                }
                
                DispatchQueue.main.async {
                    self.endLoading()
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            }
            else {
                DispatchQueue.main.async {
                    self.endLoading()
                    self.refreshControl.endRefreshing()
                }
            }
        }
    }

    @objc func newOffer(){
        if self.username.isEmpty {
            CWAlert.showNotification(withTitle: "Error".localized, message: "Your username cannot be empty, if you want to create an offer.".localized, onViewController: self)
            return
        }
        
        let vc = MyOfferEditViewController.load("MyOfferEditViewController", fromStoryboard: "MyOfferEditViewController") as! MyOfferEditViewController
        
        vc.completed = {offer in
            self.data.append(offer)
            self.tableView.reloadData()
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteOffer(id:Int!, atIndex:IndexPath?){
        if(id != 0){
            self.showLoading()
            
            DispatchQueue.global().async {
                let response = API.post(toURL: SECURED_API_URL + "offer/delete", withPost: ["save":true,"id":id]) as? NSDictionary
                
                DispatchQueue.main.async {
                    if((response?["success"]) != nil && (response?["success"] as! Int) == 1){
                        self.data[(atIndex?.row)!]["deleted"] = 1
                        self.tableView.reloadRows(at: [atIndex!], with: .fade)
                    }
                    else if let error = response?["error"] as? NSError {
                        let alert = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
                        
                        self.present(alert, animated: true, completion: {
                            self.endLoading()
                        })
                    }
                    else {
                    }
                    
                    self.endLoading()
                }
            }
        }
    }
    
    @objc func shareButtonPressed(button:UIButton) {
        let shareString = String(format: "Let's meet and enjoy the AR D'N'D offer \"%@\" together. Just download the AR D'N'D App.\nhttp://www.ardnd.com\nYour %@".localized, self.data[button.tag]["name"] as? String ?? "", self.username)
        
        let objectsToShare = [shareString]
        
        let activityViewController = UIActivityViewController(activityItems: objectsToShare as [AnyObject], applicationActivities: nil)
        
        if let popPresentationController : UIPopoverPresentationController = activityViewController.popoverPresentationController {
            popPresentationController.sourceView = button
            popPresentationController.sourceRect = CGRect(x: button.width/2, y: button.height/2, width: 1, height: 1)
        }
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: TableViewDelegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AdminTableViewCell
        
        cell.label1?.text = self.data[indexPath.row]["name"] as? String
        cell.label2?.text = self.data[indexPath.row]["description"] as? String
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell.tag = indexPath.row
        cell.button1.tag = indexPath.row
        
        cell.button1.addTarget(self, action: #selector(shareButtonPressed(button:)), for: .touchUpInside)
        cell.button1.setTitle("Share +".localized, for: .normal)
        
        // get image from server
        if(!(self.data[indexPath.row]["image_header"] as AnyObject).isEmptyString()){
            DispatchQueue.global().async {
                let image:UIImage? = imageMethods.uiImage(withServerPathName: self.data[indexPath.row]["image_header"] as? String, width: 90)
                
                DispatchQueue.main.async {
                    if(indexPath.row==cell.tag){
                        cell.imageView1?.image = image
                        cell.setNeedsLayout()
                    }
                }
            }
        }
        else {
            cell.imageView1?.image = nil
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = MyOfferEditViewController.load("MyOfferEditViewController", fromStoryboard: "MyOfferEditViewController") as! MyOfferEditViewController
        
        vc.offer = self.data[indexPath.row]
        vc.completed = {offer in
            self.data[indexPath.row] = offer
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(data[indexPath.row]["deleted"] as? Int == 1){
            return 0
        }
        else {
            return 90.0
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete".localized) { (nil, indexPath) in
            
            let alertController = UIAlertController(title: "Delete?".localized, message: "Are you sure, that you want to delete this offer?".localized, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Yes".localized, style: .destructive, handler: { (nil) in
                
                if let uid = self.data[indexPath.row]["uid"] as AnyObject? {
                    if let id = Int(String(format:"%@", uid as! CVarArg)) {
                        self.deleteOffer(id: id as Int!, atIndex:indexPath)
                    }
                }
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (nil) in
                
            }))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        return [deleteAction]
    }
}
