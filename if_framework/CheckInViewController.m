//
//  CheckInViewController.m
//  KAMEHA
//
//  Created by User on 20.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "CheckInViewController.h"
#import "DatePickerViewController.h"
#import "SettingsViewController.h"
#import "OnboardingViewController.h"
#import "SWRevealViewController.h"
#import "DBManager.h"

#import "CheckInTableViewCell.h"

@interface CheckInViewController ()


@property (nonatomic) SettingsViewController *svc;
@property (nonatomic) OnboardingViewController *obvc;

@property (nonatomic) DBManager *dbManager;

@property (nonatomic) BOOL isLoggedIn;
@property (nonatomic) BOOL settingsConfirmed;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *tableData;

@property (nonatomic) NSMutableDictionary *valueDict;

@property (nonatomic) NSString *dateFromString, *dateToString, *wishesString, *nameString, *eMailString, *imagePathString;

@property (nonatomic) NSString *placeHolderText;

@end

@implementation CheckInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _placeHolderText = NSLocalizedString(@" Please insert here ...", @"");
    _arrivalLabel.text = NSLocalizedString(@"Arrival", @"");
    _departureLabel.text = NSLocalizedString(@"Departure", @"");
    _wishesLabel.text = NSLocalizedString(@"Preferences/Reason for your stay", @"");
    _settingsConfirmedLabel.text = NSLocalizedString(@"Hereby I confirm, that my contact data are up to date.",@"");
    _checkInDescriptionLabel.text = NSLocalizedString(@"Check in at least one day prior to your stay and get outstanding amenities during your stay.",@"");
    
    
    // Localization
    //self.settingsConfirmedLabel.text = NSLocalizedString(@"",@"");
    
    
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    
    UIImage *locImage = [self.settingsButtonImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.settingsButtonImage.image = nil;
    [self.settingsButtonImage setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    self.settingsButtonImage.image = locImage;
    
    self.navigationItem.rightBarButtonItem = self.settingsBarButton;
    
    
    // if there is one logged in user
    if([[NSUserDefaults standardUserDefaults] integerForKey:@"userLoggedIn"] == 1){
        self.isLoggedIn = 1;
    }
    else {
        self.isLoggedIn = 0;
    }
    
    [self registerForKeyboardNotifications];
    
    
    self.tableData = @[
                       @{@"title":@"",
                         @"sectionCells":@[
                                 @{@"type":@"headerCell",
                                   @"inputTag":@0,
                                   @"label":@"",
                                   @"placeHolder":@""
                                   },
                                 @{@"type":(self.isLoggedIn)?@"mainCell":@"mainCellNotLoggedIn",
                                   @"inputTag":@0,
                                   @"label":@"",
                                   @"placeHolder":@""
                                   }
                                 ]
                         }
                       ];
    [self.tableView reloadData];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    ////// main view //////
    UITapGestureRecognizer *mainViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mainViewClicked)];
    [self.mainView addGestureRecognizer:mainViewTap];
    ////// main view //////
    
    ////// date picker //////
    self.datePickerView.alpha = 0;
    self.datePickerView.height = 200;
    self.datePickerView.width = [UIScreen mainScreen].bounds.size.width;
    [self.view addSubview:self.datePickerView];
    self.datePickerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, self.datePickerView.width, self.datePickerView.height);
    ////// date picker //////
    
    ////// text view //////
    self.textViewWishes.layer.borderColor = [UIColor whiteColor].CGColor;
    self.textViewWishes.layer.borderWidth = 1.5f;
    self.textViewWishes.layer.cornerRadius = 5.5f;
    self.textViewWishes.backgroundColor = [UIColor clearColor];
    
    self.textViewWishes.delegate = self;
    
    self.textViewWishes.text = _placeHolderText;
    self.textViewWishes.textColor = [UIColor lightGrayColor];
    ////// text view //////
    
    
    ////// register & settings buttons //////
    [self settingsConfirmationChanged:NO];
    ////// register & settings buttons //////
    
    
    ////// date views //////
    self.dateFromView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.dateFromView.layer.borderWidth = 1.5f;
    self.dateFromView.layer.cornerRadius = 5.5f;
    self.dateFromView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *fromDateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fromDateClicked)];
    [self.dateFromView addGestureRecognizer:fromDateTap];
    
    
    self.dateToView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.dateToView.layer.borderWidth = 1.5f;
    self.dateToView.layer.cornerRadius = 5.5f;
    self.dateToView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *toDateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toDateClicked)];
    [self.dateToView addGestureRecognizer:toDateTap];
    ////// date views //////
    
    
    self.checkInButton.layer.cornerRadius = 5.5f;
    self.registerButton.layer.cornerRadius = 5.5f;
    
    
    ////// navigation controller & item //////
    //self.navigationItem.title = @"CheckIn";
    self.navigationItem.backBarButtonItem.title = @"";
    ////// navigation controller & item //////
    
    
    self.dateFrom = [NSDate dateWithTimeIntervalSinceNow:0];
    self.dateTo = [NSDate dateWithTimeIntervalSinceNow:0];
    
    [self fillToLabelsWithDate:self.dateFrom];
    [self fillFromLabelsWithDate:self.dateTo];
    
    _dateFromString = @"";
    _dateToString = @"";
    _wishesString = @"";
    _nameString = @"";
    _eMailString = @"";
    _valueDict = [@{
                    [NSNumber numberWithInt:CHECKINFIELDDATEFROM]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDDATEFROM],
                                                                      @"field_type":[NSNumber numberWithInt:0],
                                                                      @"value":_dateFromString
                                                                      } mutableCopy],
                    [NSNumber numberWithInt:CHECKINFIELDDATETO]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDDATETO],
                                                                    @"field_type":[NSNumber numberWithInt:0],
                                                                    @"value":_dateToString
                                                                    } mutableCopy],
                    [NSNumber numberWithInt:CHECKINFIELDWISHES]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDWISHES],
                                                                    @"field_type":[NSNumber numberWithInt:0],
                                                                    @"value":_wishesString
                                                                    } mutableCopy],
                    [NSNumber numberWithInt:CHECKINFIELDNAME]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDNAME],
                                                                  @"field_type":[NSNumber numberWithInt:0],
                                                                  @"value":_nameString
                                                                  } mutableCopy],
                    [NSNumber numberWithInt:CHECKINFIELDEMAIL]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDEMAIL],
                                                                   @"field_type":[NSNumber numberWithInt:0],
                                                                   @"value":_eMailString
                                                                   } mutableCopy],
                    } mutableCopy];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView reloadData];
}


-(void)fromDateClicked{
    self.datePicker.tag = 0;
    
    [UIView animateWithDuration:0.6 animations:^{
        self.datePickerView.y = [UIScreen mainScreen].bounds.size.height + self.datePickerView.height * (-1);
        self.datePickerView.alpha = self.datePickerView.alpha * (-1) + 1;
        self.datePickerView.backgroundColor = [UIColor whiteColor];
    } completion:^(BOOL finished){
        if(self.datePickerView.alpha == 0 && finished)self.datePickerView.y = [UIScreen mainScreen].bounds.size.height;
    }];
    
}

-(void)toDateClicked{
    self.datePicker.tag = 1;
    
    [UIView animateWithDuration:0.6 animations:^{
        self.datePickerView.y = [UIScreen mainScreen].bounds.size.height + self.datePickerView.height * (-1);
        self.datePickerView.alpha = self.datePickerView.alpha * (-1) + 1;
        self.datePickerView.backgroundColor = [UIColor whiteColor];
    } completion:^(BOOL finished){
        if(self.datePickerView.alpha == 0 && finished)self.datePickerView.y = [UIScreen mainScreen].bounds.size.height;
    }];
}

-(void)mainViewClicked{
    [self.textViewWishes resignFirstResponder];
}

-(void)saveDateFromPicker{
    if(self.datePicker.tag == 0){
        [self fillFromLabelsWithDate:self.dateFrom];
    }
    else {
        
        if([self.dateFrom compare:self.dateTo]==NSOrderedAscending){
            [self fillToLabelsWithDate:self.dateTo];
        } else {
            [self fillToLabelsWithDate:self.dateTo];
        }
    }
    
    [UIView animateWithDuration:0.6 animations:^{
        self.datePickerView.y = [UIScreen mainScreen].bounds.size.height;
        self.datePickerView.alpha = 0;
    } completion:^(BOOL finished){
    }];
}


-(void)fillFromLabelsWithDate:(NSDate*)date{
    self.dateFrom = date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    [formatter setDateFormat:@"dd"];
    self.labelFromDay.text = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"MMMM"];
    self.labelFromMonth.text = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"HH:mm"];
    self.labelFromTime.text = [NSString stringWithFormat:@"%@ Uhr", [formatter stringFromDate:date]];
}

-(void)fillToLabelsWithDate:(NSDate*)date{
    self.dateTo = date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    [formatter setDateFormat:@"dd"];
    self.labelToDay.text = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"MMMM"];
    self.labelToMonth.text = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"HH:mm"];
    self.labelToTime.text = [NSString stringWithFormat:@"%@ Uhr", [formatter stringFromDate:date]];
}


-(NSString*)dateTimeStringFromDate:(NSDate*)date
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:date
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    
    NSString *timeString = [NSDateFormatter localizedStringFromDate:date
                                                          dateStyle:NSDateFormatterNoStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    return [NSString stringWithFormat:@"%@, %@", dateString, timeString];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem.title = @"";
    [self.navigationItem.backBarButtonItem setTitle:@""];
    
    
    if(self.revealViewController.panGestureRecognizer!=nil){
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSDictionary *sectionDict = self.tableData[section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    return sectionCellArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *sectionDict = self.tableData[indexPath.section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSDictionary *cellDict = sectionCellArray[indexPath.row];
    
    CheckInTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellDict[@"type"]];
    
    if([cellDict[@"type"] isEqualToString:@"mainCell"]){
        [cell.contentView addSubview:self.mainView];
        self.mainView.center = cell.contentView.center;
        self.mainView.frame =CGRectMake(([UIScreen mainScreen].bounds.size.width - self.mainView.width) / 2, 0, self.mainView.width, self.mainView.height);
        cell.backgroundColor = self.mainView.backgroundColor;
    }
    if([cellDict[@"type"] isEqualToString:@"mainCellNotLoggedIn"]){
        [cell.contentView addSubview:self.mainViewNotLoggedIn];
        [self.mainViewNotLoggedIn setWidth:cell.contentView.width];
        self.mainViewNotLoggedIn.center = cell.contentView.center;
        self.mainViewNotLoggedIn.frame =CGRectMake(([UIScreen mainScreen].bounds.size.width - self.mainViewNotLoggedIn.width) / 2, 0, self.mainViewNotLoggedIn.width, self.mainViewNotLoggedIn.height);
        cell.backgroundColor = self.mainViewNotLoggedIn.backgroundColor;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *sectionDict = self.tableData[indexPath.section];
    NSArray *sectionCellArray = sectionDict[@"sectionCells"];
    NSDictionary *cellDict = sectionCellArray[indexPath.row];
    
    if([cellDict[@"type"] isEqualToString:@"headerCell"]) {
        return [UIScreen mainScreen].bounds.size.width * 0.56875;
    } else if([cellDict[@"type"] isEqualToString:@"mainCell"]){
        if(self.isLoggedIn){
            return self.mainView.height;
        } else {
            return self.mainViewNotLoggedIn.height;
        }
    } else if([cellDict[@"type"] isEqualToString:@"mainCellNotLoggedIn"]){
        return self.mainViewNotLoggedIn.height;
    } else {
        return 0;
    }
}

- (IBAction)pickerChanged:(UIDatePicker *)sender {
    if(sender.tag == 0){
        self.dateFrom = self.datePicker.date;
        
        [self fillFromLabelsWithDate:self.dateFrom];
    } else {
        
        self.dateTo = self.datePicker.date;
        
        if([self.dateFrom compare:self.dateTo]==NSOrderedAscending){
            [self fillToLabelsWithDate:self.dateTo];
        } else {
            [self fillToLabelsWithDate:self.dateTo];
        }
    }
}

- (IBAction)datePickerDone:(UIButton *)sender {
    [self saveDateFromPicker];
}

- (IBAction)settingsButtonClicked:(id)sender {
    
    // if there is one logged in user
    if(self.isLoggedIn == 1){
        // show the settings
        UIStoryboard *LoginAndregisterBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
        self.svc = (SettingsViewController *)[LoginAndregisterBoard instantiateInitialViewController];
        
        [self.navigationController pushViewController:self.svc animated:YES];
    }
    else
    {
        // else show the onboarding screen
        UIStoryboard *LoginAndregisterBoard = [UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil];
        self.obvc = (OnboardingViewController *)[LoginAndregisterBoard instantiateInitialViewController];
        
        self.obvc.modalPresentationStyle = UIModalPresentationFullScreen;
        self.obvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self.navigationController presentViewController:self.obvc animated:YES completion:^{
            [UIApplication sharedApplication].keyWindow.rootViewController = self.obvc;
        }];
        
    }
}

- (IBAction)switchSettingsConfirmation:(UISwitch *)sender {
    [self settingsConfirmationChanged:sender.isOn];
}

-(void)settingsConfirmationChanged:(BOOL)settingsConfirmed{
    
    _settingsConfirmed = settingsConfirmed;
    
    if(!_isLoggedIn){
        ////// register button text //////
        [_checkInButton setTitle:NSLocalizedString(@"REGISTER", @"") forState:UIControlStateNormal];
        [_checkInButton setTitle:NSLocalizedString(@"REGISTER", @"") forState:UIControlStateHighlighted];
        ////// register button text //////
        return;
    }
    
    ////// swttings switch //////
    if(_settingsConfirmed){
        [_checkInButton setTitle:NSLocalizedString(@"CHECK-IN", @"") forState:UIControlStateNormal];
        [_checkInButton setTitle:NSLocalizedString(@"CHECK-IN", @"") forState:UIControlStateHighlighted];
    }
    else {
        [_checkInButton setTitle:NSLocalizedString(@"SETTINGS", @"") forState:UIControlStateNormal];
        [_checkInButton setTitle:NSLocalizedString(@"SETTINGS", @"") forState:UIControlStateHighlighted];
    }
    ////// settings switch //////
    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if(textView.textColor == [UIColor lightGrayColor]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = _placeHolderText;
        [textView resignFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField*)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
}


// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    self.view.height = [UIScreen mainScreen].bounds.size.height - kbSize.height;
    self.tableView.height = [UIScreen mainScreen].bounds.size.height - kbSize.height;
    [self.tableView layoutIfNeeded];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    
    self.view.height = [UIScreen mainScreen].bounds.size.height;
    self.tableView.height = [UIScreen mainScreen].bounds.size.height;
    [self.tableView layoutIfNeeded];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


- (IBAction)checkInButtonClicked:(UIButton *)sender {
    
    if(!_settingsConfirmed){
        [self settingsButtonClicked:sender];
        return;
    }
    
    if([_wishesString isEqualToString:_placeHolderText]){
        _wishesString = @"";
    }
    
    localStorage *dynamicUserSettings = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    NSString *firstName, *lastName, *eMail, *imagePathString;
    
    firstName = [dynamicUserSettings objectForKey:@"firstName"];
    lastName = [dynamicUserSettings objectForKey:@"lastName"];
    eMail = [dynamicUserSettings objectForKey:@"email"];
    imagePathString = [NSString stringWithFormat:@"%@%@", [dynamicUserSettings objectForKey:@"profileImagePath"],[dynamicUserSettings objectForKey:@"profileImageName"]];
    NSLog(@"\nfirstName: %@\nlastName: %@",firstName,lastName);
    
    if(firstName == nil || lastName == nil || [firstName isEqualToString:@""] || [lastName isEqualToString:@""]){
        [self.settingsSwitch setOn:NO animated:YES];
        _settingsConfirmed = NO;
        [_checkInButton setTitle:NSLocalizedString(@"SETTINGS", @"") forState:UIControlStateNormal];
        [_checkInButton setTitle:NSLocalizedString(@"SETTINGS", @"") forState:UIControlStateHighlighted];
        [self.settingsSwitch setOn:NO animated:YES];
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"First and / or last name is empty, please check your settings.",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        
        [av show];
        
        return;
    }
    
    [self sendCheckInWithDateFrom:[self date2str:_dateFrom onlyDate:NO] dateTo:[self date2str:_dateTo onlyDate:NO] wishes:_textViewWishes.text name:[NSString stringWithFormat:@"%@ %@",firstName, lastName] eMail:eMail imageString:imagePathString];
    
}

-(void)sendCheckInWithDateFrom:(NSString*)dateFromString
                        dateTo:(NSString*)dateToString
                        wishes:(NSString*)wishesString
                          name:(NSString*)nameString
                         eMail:(NSString*)eMailString
                   imageString:(NSString*)imagePathString{
    _dateFromString = dateFromString;
    _dateToString = dateToString;
    _wishesString = wishesString;
    _nameString = nameString;
    _eMailString = eMailString;
    _imagePathString = imagePathString;
    
    _valueDict = [@{
                    [NSString stringWithFormat:@"%i",CHECKINFIELDDATEFROM]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDDATEFROM],
                                                                               @"field_type":[NSNumber numberWithInt:0],
                                                                               @"value":_dateFromString
                                                                               } mutableCopy],
                    [NSString stringWithFormat:@"%i",CHECKINFIELDDATETO]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDDATETO],
                                                                             @"field_type":[NSNumber numberWithInt:0],
                                                                             @"value":_dateToString
                                                                             } mutableCopy],
                    [NSString stringWithFormat:@"%i",CHECKINFIELDWISHES]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDWISHES],
                                                                             @"field_type":[NSNumber numberWithInt:0],
                                                                             @"value":_wishesString
                                                                             } mutableCopy],
                    [NSString stringWithFormat:@"%i",CHECKINFIELDNAME]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDNAME],
                                                                           @"field_type":[NSNumber numberWithInt:0],
                                                                           @"value":_nameString
                                                                           } mutableCopy],
                    [NSString stringWithFormat:@"%i",CHECKINFIELDEMAIL]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDEMAIL],
                                                                            @"field_type":[NSNumber numberWithInt:5],
                                                                            @"value":_eMailString
                                                                            } mutableCopy],
                    [NSString stringWithFormat:@"%i",CHECKINFIELDIMAGE]: [@{@"uid":[NSNumber numberWithInt:CHECKINFIELDIMAGE],
                                                                            @"field_type":[NSNumber numberWithInt:9],
                                                                            @"value":_imagePathString
                                                                            } mutableCopy],
                    } mutableCopy];
    
    NSLog(@"value array: %@",_valueDict);
    
    int success = 0;
    @try{
        NSError *error = [[NSError alloc] init];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_valueDict
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSString *post =[[NSString alloc] initWithFormat:@"dynamicFieldValues=%@", jsonString];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@save_feedback.php?place_id=%i&campaign_id=%i&entry_id=%i&ep_type=%i",SECURED_API_URL,CHECKINPLACEID,CHECKINCAMPAIGNID,APPENTRYPOINT,1]];
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
        
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSLog(@"Response (checkInViewController) ==> %@", responseData);
        
        if ([response statusCode] >= 200 && [response statusCode] < 300)
        {
            NSError *error = nil;
            NSDictionary *jsonData = [NSJSONSerialization
                                      JSONObjectWithData:urlData
                                      options:NSJSONReadingMutableContainers
                                      error:&error];
            
            success = [jsonData[@"success"] intValue];
            
            
        } else {
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    if(success){
        [self.navigationController popViewControllerAnimated:YES];
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Thank you very much for your check-in. Please visit the reception after your arrival.",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        
        [av show];
    }
}

-(NSString*)date2str:(NSDate*)myNSDateInstance onlyDate:(BOOL)onlyDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    if (onlyDate) {
        [formatter setDateFormat:@"dd.MM.yyyy"];
    }else{
        [formatter setDateFormat: @"dd.MM.yyyy HH:mm"];
    }
    
    NSString *stringFromDate = [formatter stringFromDate:myNSDateInstance];
    return stringFromDate;
}

@end
