//
//  ImageMethods.h
//  pickalbatros
//
//  Created by User on 10.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImageUploadDelegate <NSObject>

@optional -(void)uploadingWithProgress:(float)progress;

@optional -(void)uploadFinishedWithResponse:(NSDictionary*)response;

@end

@interface imageMethods : NSObject <NSURLConnectionDataDelegate>

typedef void(^uploadCompleted)(NSDictionary* nullable);

@property NSString *mimeType;

@property id<ImageUploadDelegate> uploadDelegate;

-(UIImage *) getImageFromURL:(NSString *)fileURL;
- (UIImage *)getImageFromLocalFolder:(NSString *)imageName;

-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;

-(UIImage*) loadImage:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;

+(UIImage*)UIImageWithWebPath:(NSString*)name;

+(UIImage*)UIImageWithWebPath:(NSString*)name versionIdentifier:(NSString*)version;

+(UIImage*)UIImageWithWebPath:(NSString*)name versionIdentifier:(NSString*)version width:(NSString*)width;

+(UIImage*)UIImageWithServerPathName:(NSString*)name width:(float)width;

+(UIImage*)UIImageWithServerPathName:(NSString*)name versionIdentifier:(NSString*)version width:(float)width;

+(void)deleteAllImages;

-(void)uploadImage:(UIImage*)image withParameters:(NSDictionary*)parameters;

-(void)uploadImage:(UIImage*)image;

+(UIImage*)resizeImage:(UIImage*)image forSize:(CGSize)size;

@end
