//
//  RunOnMainQueueAvoidDeadlock.m
//  if_framework
//
//  Created by Vadym Patalakh on 4/17/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

#import "RunOnMainQueueAvoidDeadlock.h"

void runOnMainQueueAvoidDeadlock(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}
