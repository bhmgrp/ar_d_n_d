//
//  AdminTableViewCell.swift
//  if_framework
//
//  Created by Christopher on 10/19/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit
import Foundation

protocol AdminTableViewCellDelegate : class{
    func userTappedAtHashtag(hashtag: String, tag: NSInteger, index: NSInteger)
}

class AdminTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var hashtagsTextView: UITextView!
    
    @IBOutlet weak var hashtagsTextViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var imageView4: UIImageView!
    
    @IBOutlet weak var badge1: CWBadgeView!
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    @IBAction func button1pressed(_ sender: UIButton) {
        self.button1pressed()
    }
    @IBAction func button2pressed(_ sender: UIButton) {
        self.button2pressed()
    }
    @IBAction func button4pressed(_ sender: UIButton) {
        self.button4pressed()
    }
    
    weak var delegate: AdminTableViewCellDelegate?
    
    var button1pressed:()->Void = {}
    var button2pressed:()->Void = {}
    var button3pressed:()->Void = {}
    var button4pressed:()->Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.hashtagsTextView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector((onHashtagLinkTap))))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func onHashtagLinkTap(sender: UIGestureRecognizer){
        let textView = self.hashtagsTextView
        let locationOfTouch = sender.location(in: sender.view)
        
        let tapPosition : UITextPosition? = self.hashtagsTextView?.closestPosition(to: locationOfTouch)
        if let textRange = self.hashtagsTextView?.tokenizer.rangeEnclosingPosition(tapPosition!, with: .word, inDirection: 1) {
            if var tappedWord = textView?.text(in: textRange) {
                let newRange = textView?.textRange(from: tapPosition!, to: tapPosition!)
                textView?.selectedTextRange = newRange
                let characterIndex = textView?.offset(from: (textView?.beginningOfDocument)!, to: (textView?.selectedTextRange!.start)!)
                tappedWord.insert("#", at: tappedWord.startIndex)
                tappedWord.insert(" ", at: tappedWord.endIndex)
                let index = self.getWordIndex(word: tappedWord, characterIndex: characterIndex!, text: (textView?.text)!)
                
                self.delegate?.userTappedAtHashtag(hashtag: tappedWord, tag: self.tag, index: index)
                print("selected word :\(tappedWord)")
            }
        }
    }
    
    func getWordIndex(word: String, characterIndex: NSInteger, text: String) -> NSInteger {
        let string = text.components(separatedBy: word)
        
        if string.count - 1 <= 1 {
            return 1
        }
        
        if string.count > 1 {
            let range = text.range(of: word)
            let startIndex = range?.lowerBound.samePosition(in: text.utf16)
            let endIndex = range?.upperBound.samePosition(in: text.utf16)
            let location = text.distance(from: text.startIndex,
                                         to: (range?.lowerBound)!)
            
//            let length =  startIndex?.//startIndex?.distance(to: endIndex)
//
//            if location + length! > characterIndex {
//                return 2
//            }
//            else {
//                return 3
//            }
        }
        
        return 0
    }
}
