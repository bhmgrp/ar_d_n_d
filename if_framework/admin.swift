//
//  admin.swift
//  if_framework
//
//  Created by Christopher on 10/6/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

import GooglePlaces
import GoogleMaps

class admin: NSObject {
    static func initGoogleAPIS() {
        initGoogleAPIS()
    }
}

func t(string:String) ->String{
    return NSLocalizedString(string, tableName: "admin", bundle: Bundle.main, value: "", comment: "")
}

func get(viewController:String) -> UIViewController {
    return UIViewController.load(viewController, fromStoryboard: viewController) as! UIViewController
}

func get(viewController:String, fromStoryboard: String) -> UIViewController {
    return UIViewController.load(viewController, fromStoryboard: fromStoryboard) as! UIViewController
}

func isIpad() -> Bool {
     return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
}

func unwrap(object:Any?,defaultValue:Any) -> Any{
    return object != nil ? object! : defaultValue
}

// MARK: extensions
extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: "admin", bundle: Bundle.main, value: "", comment: "")
    }
    
    var translated: String {
        return localized
    }
    
    func localized(fromTable:String) -> String{
        return NSLocalizedString(self, tableName: fromTable, bundle: Bundle.main, value: "", comment: "")
    }
    
    func localize(string:String) -> String{
        return NSLocalizedString(string, tableName: self, bundle: Bundle.main, value: "", comment: "")
    }
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}

extension UIView {
    convenience init (frame:CGRect,backgroundColor:UIColor) {
        self.init()
        self.frame = frame
        self.backgroundColor = backgroundColor
    }
}

extension Date {
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}

func initGoogleAPIS() {
    // Do a quick check to see if you've provided an API key, in a real app you wouldn't need this
    // but for the demo it means we can provide a better error message if you haven't.
    if kMapsAPIKey.isEmpty || kPlacesAPIKey.isEmpty {
        // Blow up if API keys have not yet been set.
        let bundleId = Bundle.main.bundleIdentifier!
        let msg = "Configure API keys inside SDKDemoAPIKey.swift for your  bundle `\(bundleId)`, " +
        "see README.GooglePlacePickerDemos for more information"
        fatalError(msg)
    }
    
    // Provide the Places API with your API key.
    GMSPlacesClient.provideAPIKey(kPlacesAPIKey)
    // Provide the Maps API with your API key. We need to provide this as well because the Place
    // Picker displays a Google Map.
    GMSServices.provideAPIKey(kMapsAPIKey)
    
    // Log the required open source licenses! Yes, just logging them is not enough but is good for
    // a demo.
    // print(GMSPlacesClient.openSourceLicenseInfo())
    // print(GMSServices.openSourceLicenseInfo())
}
