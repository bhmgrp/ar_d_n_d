//
//  MyOfferEditTableViewCell.swift
//  if_framework
//
//  Created by Christopher on 10/5/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class MyOfferEditTableViewCell: UITableViewCell {

    @IBOutlet public weak var textView: UITextView!
    @IBOutlet public weak var textInput: UITextField!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var colorIndicator: UIView!
    
    @IBOutlet weak var labelBackgroundImage: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var buttonBackgroundImage: UIButton!
    @IBOutlet weak var labelIcon: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var buttonIcon: UIButton!
    
    @IBOutlet weak var switch1: UISwitch!
    
    public var tableView:UITableView? = nil
    public var switchSwitched:(UISwitch)->Void = {_ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initHeaderLabels()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func initHeaderLabels() {
        self.labelBackgroundImage?.text = "Background image".localized
        self.labelIcon?.text = "Icon".localized
    }
    
    func initStyle(){
        if((textView) != nil){
            self.textView.layer.borderColor = UIColor(from: 0xD4D4D4).cgColor//tableView?.separatorColor?.cgColor
            self.textView.layer.borderWidth = 0.5
            self.textView.layer.cornerRadius = 7.0
        }
        if((label2) != nil) {
            self.label2.textColor = UIColor(from: 0xD4D4D4)
        }
    }
    
    public func configure(text: String?, placeholder: String?) {
        if((textInput) != nil){
            textInput.text = text
            textInput.placeholder = placeholder
            
            textInput.accessibilityValue = text
            textInput.accessibilityLabel = placeholder
            
            textInput.tag = self.tag
        }
        
        if((textView) != nil){
            textView.text = text
            
            textView.tag = self.tag
            
            if(text == nil || text == ""){
                textView.text = placeholder;
                textView.textColor = UIColor(rgbHex: 0xBBBAC2)
            }
        }
        
        if((label1) != nil){
            label1.text = placeholder
        }
        
        if((label2) != nil){
            label2.text = text
        }
        
        self.initStyle()
    }
    
    public func configure(text: String?, placeholder: String?, label: String?) {
        configure(text: text, placeholder: placeholder)
        
        if((label1) != nil && label != nil && !(label?.isEmpty)!){
            label1.text = label
        }
        
    }
    
    @IBAction func switchSwitched(_ sender: UISwitch) {
        self.switchSwitched(sender)
    }
    
    
}
