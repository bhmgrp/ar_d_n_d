//
//  HashtagCollectionViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/25/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class HashtagCollectionViewController: StartStreamCollectionViewController {
    var shouldShowHeader: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "LocationCollectionViewCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: "locationCollectionViewCell")
        
        let headerNib = UINib(nibName: "StartStreamCollectionViewCell", bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "startStreamCollectionViewCell")
        
        tableLayout.delegate = self
        
        gridLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 44)
        tableLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 44)
        tableLayout.spacingsHeight = 170
        
        if #available(iOS 11.0, *){
            
        } else {
            collectionView.contentInset = UIEdgeInsetsMake(-65, 0, 0, 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if shouldShowHeader == false {
            viewMode = .tableViewMode
            collectionView.contentInset = UIEdgeInsetsMake(-44, 0, 0, 0)
        } else {
            viewMode = .gridViewMode
            loadData()
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    override func searchForFilters() {
        
    }
    
    override func initNavigationItems() {
        return
    }
    
    // MARK: collection view delegate
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "hashtagCollectionViewHeader", for: indexPath) as! HashtagCollectionViewHeader
        
        header.gridViewButton.addTarget(self, action: #selector(setGridViewMode), for: .touchUpInside)
        header.tableViewButton.addTarget(self, action: #selector(setTableViewMode), for: .touchUpInside)
        
        if viewMode == .gridViewMode {
            header.gridViewButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
            header.tableViewButton.tintColor = UIColor.lightGray
        } else {
            header.gridViewButton.tintColor = UIColor.lightGray
            header.tableViewButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
        }
        
        if shouldShowHeader == false {
            header.isHidden = true
        }
        
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch viewMode {
        case .gridViewMode:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        
        case .tableViewMode:
            var cell: StartStreamCollectionViewCell
            
            if let offerId = data[indexPath.item]["entity_type"] as? String, offerId == "1" {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offerStartStreamCollectionViewCell", for: indexPath) as! StartStreamCollectionViewCell
            } else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "startStreamCollectionViewCell", for: indexPath) as! StartStreamCollectionViewCell
            }
            
            cell.tag = indexPath.item
            cell.delegate = self
            
            cell.immediateCommentTextField.delegate = self
            cell.immediateCommentTextField.tag = indexPath.row
            
            cell.immediateCommentImageView.image = LoggedUserCredentials.sharedInstance.userImage
            
            let headerText = self.data[indexPath.row]["user_name"] as? String ?? ""
            let postText = self.data[indexPath.row]["content"] as? String ?? ""
            
            // take only top 5 comments which is not replies
            if let allComments = data[indexPath.row]["comments"] as? [[String:Any]] {
                let comments = getCommentsFrom(array: allComments)
                cell.setComments(comments)
                
                cell.showAllCommentsButtonTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.profileImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: false)
                }
                
                cell.commentButtonTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.profileImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: true)
                }
            }
            
            // handling user image
            
            if let userImageString = self.data[indexPath.row]["user_image"] as? String, userImageString != "" {
                if self.imageMap[userImageString] != nil {
                    cell.profileImageView.image = self.imageMap[userImageString]
                }
                else {
                    DispatchQueue.global().async {
                        var userImage:UIImage? = nil
                        
                        if(userImageString.hasPrefix("http")){
                            userImage = imageMethods.uiImage(withWebPath: userImageString)
                        }
                        else {
                            userImage = imageMethods.uiImage(withServerPathName: userImageString, width: 36)
                        }
                        
                        DispatchQueue.main.async {
                            if userImage != nil && cell.tag == indexPath.row {
                                self.imageMap[userImageString] = userImage
                                cell.profileImageView.image = userImage
                            }
                            else {
                                self.imageMap[userImageString] = UIImage()
                                cell.profileImageView.image = nil
                            }
                        }
                    }
                }
            }
            else {
                cell.profileImageView.image = nil
            }
            
            // handling post image
            
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let image = self.imageMap[postImageString]
                    cell.postImageView.image = image
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
//                    else if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
//                    imageMap[postImageString] = image
//                    cell.postImageView?.image = image
//                }
//                else {
//                    let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
//                    imageDownloadOperation.completionHandler = { [weak self] (image) in
//                        guard let strongSelf = self else {
//                            return
//                        }
//
//                        DispatchQueue.main.async {
//                            cell.postImageView?.image = image
//                        }
//
//                        strongSelf.imageMap[postImageString] = image
//                        strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
//                    }
//
//                    imageDownloadOperationQueue.addOperation(imageDownloadOperation)
//                    imageDownloadOperations[indexPath] = imageDownloadOperation
//                }
                else {
                    DispatchQueue.global().async {
                        let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
                        DispatchQueue.main.async {
                            if postImage != nil && cell.tag == indexPath.row {
                                self.imageMap[postImageString] = postImage
                                let image = self.imageMap[postImageString]
                                cell.postImageView.image = image
                                cell.setNeedsLayout()
                                cell.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
            
            cell.headerLabel.text = headerText
            cell.postText.text = postText
            cell.postText.numberOfLines = 0
            cell.postText.lineBreakMode = .byWordWrapping
            cell.postText.font = UIFont.systemFont(ofSize: 17)
            
            if cell.postText.text != "" {
                cell.postText.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 13
                cell.postText.sizeToFit()
                cell.postLabelHeight.constant = cell.postText.height
            } else {
                cell.postLabelHeight.constant = 0
            }
            
            cell.locationLabel.text = self.data[indexPath.row]["location_name"] as? String ?? ""
            cell.locationButtonTapHandler = {
                self.getLocationScreenHandler(indexPath: indexPath)
            }
            
            cell.profileButtonTapHandler = {
                self.getProfileScreenHandler(indexPath: indexPath)
            }
            
            cell.offerButtonTapHandler = {
                self.offerButtonTapHandler(indexPath: indexPath)
            }
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            if cell.hashtagsTextView.text != "" {
                cell.hashtagsTextView?.width = collectionView.width - 5
                cell.hashtagsTextView?.sizeToFit()
                cell.hashtagsTextViewHeight?.constant = (cell.hashtagsTextView?.height)!
            } else {
                cell.hashtagsTextViewHeight?.constant = 0
            }
            
            var likesCount = data[indexPath.row]["likes_number"] as? String ?? "0"
            cell.likeButton.setTitle(likesCount, for: .normal)
            
            if let likedPost = data[indexPath.row]["liked_by_me"] as? String {
                if likedPost == "1" {
                    cell.liked = true
                } else {
                    cell.liked = false
                }
            }
            
            cell.likeButtonTapHandler = {
                var likeCount = Int(likesCount)!
                if !cell.liked == true {
                    likeCount += 1
                    self.data[indexPath.row]["liked_by_me"] = "1"
                } else {
                    if likeCount != 0 {
                        likeCount -= 1
                    }
                    self.data[indexPath.row]["liked_by_me"] = "0"
                }
                
                likesCount = String(likeCount)
                self.data[indexPath.row]["likes_number"] = likeCount
                cell.likeButton.setTitle(likesCount, for: .normal)
                
                cell.liked = !cell.liked
                
                if let postId = self.data[indexPath.row]["id"] as? String {
                    let post = ["id":postId]
                    _ = AdminAPI.post(url: "ardnd/post/toggle-like/\(postId)", post: post)
                }
            }
            
            cell.shareButtonTapHandler = {
                let text = cell.postText.text ?? ""
                let image = cell.postImageView.image ?? UIImage()
                self.shareButtonHandler(text: text, image: image)
            }
            
            return cell
        }
    }
}
