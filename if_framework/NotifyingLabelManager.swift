//
//  NotifyingLabel.swift
//  gestgid
//
//  Created by Vadym Patalakh on 8/23/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class NotifyingLabelManager: NSObject {
    fileprivate var notifyingLabel: UILabel?
    fileprivate var notifyingView: UIView?
    
    fileprivate func initViews() {
        let x = UIScreen.main.bounds.size.width / 2 - 125
        notifyingView = UIView(frame: CGRect(x: x, y: 100, width: 250, height: 0))
        notifyingLabel = UILabel(frame: CGRect(x: 20, y: 0, width: 250, height: 0))
        notifyingLabel?.textAlignment = .center
        notifyingLabel?.textColor = UIColor.white
        notifyingLabel?.numberOfLines = 0
        
        notifyingView?.addSubview(notifyingLabel!)
        
        notifyingView?.layer.cornerRadius = 20
        notifyingView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        notifyingView?.alpha = 0
    }
    
    func showWithText(_ text: String, view: UIView) {
        if notifyingLabel == nil || notifyingView == nil {
            initViews()
        }
        
        notifyingLabel?.frame.size = CGSize(width: 250, height: 0)
        notifyingLabel?.text = text
        notifyingLabel?.sizeToFit()
        let labelSize = notifyingLabel?.bounds.size
        var size = CGSize(width: labelSize!.width, height: labelSize!.height)
        size.width += 40
        notifyingView?.bounds.size = size
        view.addSubview(notifyingView!)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.notifyingView?.alpha = 1
        }, completion: nil)
    }
    
    func removeLabel() {
        UIView.animate(withDuration: 0.3, animations: {
            self.notifyingView?.alpha = 0
        }, completion: { (completed) in
            self.notifyingView?.removeFromSuperview()
        })
    }
}
