//
//  AdminViewController.swift
//  if_framework
//
//  Created by Christopher on 10/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class AdminViewController: UIViewController {
    
    var activityIndicatorView:UIActivityIndicatorView = UIActivityIndicatorView.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var refreshControl: UIRefreshControl! = UIRefreshControl()
@objc    var data:[[String:Any]] = []
    
    private var defaultBarbuttonItem:UIBarButtonItem? = nil
    private var loadingBarButtonItem:UIBarButtonItem? = nil
    private var doneEditingBarbuttonItem:UIBarButtonItem? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.activityIndicatorView.hidesWhenStopped = true
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Bold", size: CGFloat(18.0))!, NSAttributedStringKey.foregroundColor: UIColor.black]
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "   ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        self.loadingBarButtonItem = UIBarButtonItem(customView: activityIndicatorView)
        self.doneEditingBarbuttonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneEditingButtonPressed))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    
    func showLoading(){
        if(self.navigationItem.rightBarButtonItem != self.loadingBarButtonItem){
            self.defaultBarbuttonItem = self.navigationItem.rightBarButtonItem
            self.navigationItem.rightBarButtonItem = self.loadingBarButtonItem
            self.activityIndicatorView.startAnimating()
        }
    }
    
    func endLoading(){
        if self.defaultBarbuttonItem != nil {
            self.navigationItem.rightBarButtonItem = self.defaultBarbuttonItem
        }
        self.activityIndicatorView.stopAnimating()
        self.refreshControl.endRefreshing()
    }
    
    func initStyle(forTableView table: UITableView){
        table.tableFooterView = UIView(frame: .zero)
        table.addSubview(self.refreshControl)
        table.contentInset = UIEdgeInsetsMake(-60, 0, 0, 0)
        table.backgroundColor = .groupTableViewBackground
    }
    
    func showDoneEditingButton(){
        if(self.navigationItem.rightBarButtonItem != self.doneEditingBarbuttonItem){
            if self.navigationItem.rightBarButtonItem != nil && self.navigationItem.rightBarButtonItem != self.loadingBarButtonItem {
                self.defaultBarbuttonItem = self.navigationItem.rightBarButtonItem
            }
            self.navigationItem.rightBarButtonItem = self.doneEditingBarbuttonItem
        }
    }
    
    @objc func doneEditingButtonPressed(){
        self.isEditing = false
        self.view.endEditing(true)
        self.hideDoneEditingButton()
        self.navigationController?.view.endEditing(true)
        self.navigationController?.isEditing = false
    }
    
    func hideDoneEditingButton(){
        if self.defaultBarbuttonItem != nil {
            self.navigationItem.rightBarButtonItem = self.defaultBarbuttonItem
        }
    }
}


// MARK: TableViewDataSource and Delegate
extension AdminViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle().loadNibNamed("AdminTableViewCell", owner: nil, options: nil)?.first as! AdminTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .groupTableViewBackground
        
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .groupTableViewBackground
        
        return view
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return []
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
    }
}


// MARK: Sectons TextFieldDelegate 
class TextFieldSectionsDelegate: UIViewController, UITextFieldDelegate {
    var sections:[[[String:Any]]] = []
    
    // MARK: textField delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        
        
        if(textField.tag == 999){
            return true
        }
        
        if  self.sections[textField.tag / 1000][textField.tag % 1000]["onTextChanged"] != nil &&
            self.sections[textField.tag / 1000][textField.tag % 1000]["onTextChanged"] is ((String)->Void) {
            (self.sections[textField.tag / 1000][textField.tag % 1000]["onTextChanged"] as! (String)->Void)(newString!)
            return true
        }
    
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.tag == 999){
            return
        }
        if self.sections[textField.tag / 1000][textField.tag % 1000]["onStart"] is (UITextField,String)->Void {
            (self.sections[textField.tag / 1000][textField.tag % 1000]["onStart"] as! (UITextField,String)->Void)(textField,textField.text ?? "")
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag == 999){
            return
        }
        if self.sections[textField.tag / 1000][textField.tag % 1000]["onEnd"] is (UITextField,String)->Void {
            (self.sections[textField.tag / 1000][textField.tag % 1000]["onEnd"] as! (UITextField,String)->Void)(textField,textField.text ?? "")
            return
        }
    }
}

