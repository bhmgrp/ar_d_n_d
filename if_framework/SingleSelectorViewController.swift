//
//  SingleSelectorViewController.swift
//  if_framework
//
//  Created by Christopher on 10/26/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class SingleSelectorViewController: MultipleSelectorViewController {

    var selectedOption:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func getSingleSelectorWith(options:[String]) -> SingleSelectorViewController{
        let vc = get(viewController:"MultipleSelectorViewController") as! SingleSelectorViewController
        
        vc.options = options
        
        return vc
    }
    
    static func getSingleSelectorWith(optionsFromUrl:String) -> SingleSelectorViewController{
        let vc = get(viewController:"SingleSelectorViewController") as! SingleSelectorViewController
        
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: vc, action: #selector(hide))
        
        vc.showLoading()
        DispatchQueue.global().async {
            
            let options = API.getFrom(optionsFromUrl)
            
            if(options is [String]){
                vc.options = options as! [String]
            }
            
            DispatchQueue.main.async {
                vc.endLoading()
                vc.tableView.reloadData()
            }
        }
        
        return vc
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.selectedOption = self.options?[indexPath.row]
        
        _ = self.navigationController?.popViewController(animated: true)
    }

}
