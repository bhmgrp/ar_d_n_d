//
//  IconPickerCollectionViewCell.swift
//  if_framework
//
//  Created by Christopher on 10/6/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class IconPickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    
}
