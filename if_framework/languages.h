//
//  languages.h
//  pickalbatros
//
//  Created by User on 02.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface languages : NSObject

@property (nonatomic) NSDictionary *languagesIDMapping;

+(int)currentLanguageID;

+(int)getCurrentLanguageID;

+(int)languageIDForIsoCode:(NSString*)languageIsoCode;

+(NSString*)currentLanguageKey;

+(NSString*)getCurentLanguageKey;

+(NSString*)getLanguageKeyForID:(NSInteger)languageID;


+(NSDictionary*)languageIDKeyMapping;

+(void)setLanguage:(NSString*)languageKey;

+(NSString*)getLanguageNameByKey:(NSString*)languageKey;

typedef NS_ENUM(unsigned int, LANGUAGE_KEY_STRING) {LANGUAGE_KEY_EN, LANGUAGE_KEY_DE, LANGUAGE_KEY_FR};
#define LANGUAGE_KEY_STRING_FOR(enum) [@[@"en",@"de",@"fr"] objectAtIndex:enum]

@end;
