//
//  LoadingInformationViewController.m
//  Pickalbatros
//
//  Created by User on 15.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "LoadingInformationViewController.h"
#import "Structure.h"

@interface LoadingInformationViewController (){
    float loadingProgress;
}

@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic, strong) NSURL *jsonFileUrl;

@property (nonatomic,strong) NSArray *dataArray;

@property (nonatomic) int downloading;

@property (nonatomic) UIView *bgView;

@property (nonatomic) homeModel *homeModel;

@property (nonatomic) int rootID;

@property (nonatomic) int elementID;

@property (nonatomic) int numOfItems, currentItem;

@property (nonatomic) BOOL downloadFinished;

@end

@implementation LoadingInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _backgroundView.alpha = 0.0f;
    _popupView.alpha = 0.0f;
    _homeModel = [[homeModel alloc] init];
    loadingProgress = 0;
    [self setNeedsStatusBarAppearanceUpdate];
    if(!_skipUserInput){
        _headLineText = (_headLineText == nil)?_headLineText=NSLocalizedString(@"There is an update available for the selected place, do you want to download it now?",@""):_headLineText;
        
        _headLineLabel.text = _headLineText;
        [_okButton setTitle:(_okButtonText!=nil)?_okButtonText:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
        [_cancelButton setTitle:(_cancelButtonText!=nil)?_cancelButtonText:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
        [_continueButton setTitle:(_continueButtonText!=nil)?_continueButtonText:NSLocalizedString(@"Continue", @"") forState:UIControlStateNormal];
        
        _progressBar.hidden = YES;
    }
    
    
    if(_pID==0)
        _pID = (int)[globals sharedInstance].pID;
    
    if(_placeID==0)
        _placeID = (int)[globals sharedInstance].placeID;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!_invisibleView){
        [UIView animateWithDuration:0.5 animations:^{_backgroundView.alpha = 0.5f;_popupView.alpha = 1.0f;}];
    }
    else {
        [UIView animateWithDuration:0.5 animations:^{
            _backgroundView.backgroundColor = [UIColor clearColor];_backgroundView.alpha = 1.0f;}];
    }
    if(_skipUserInput){
        [self okButtonPressed:nil];
    }
}



-(void)viewDidLayoutSubviews{
    if(!_invisibleView){
        self.popupView.layer.cornerRadius = 10.0f;
        self.popupView.backgroundColor = [UIColor clearColor];
        [self.popupView addBlurBehindSelfInView:self.view];
    }
}


-(void)downloadPlaceInfo{
    
    localStorage *placeStorage = [localStorage storageWithFilename:[NSString stringWithFormat:@"place%i",_placeID]];
    localStorage *currentPlaceStorage = [localStorage storageWithFilename:@"currentPlace"];
    
    NSString *serverMD5String = [[NSString alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@gestgid/place/%li/md5",SECURED_API_URL,(long)_placeID]]] encoding:NSUTF8StringEncoding];
    
    if(![serverMD5String isEqualToString:[placeStorage objectForKey:@"md5"]]){
        NSLog(@"updating");
        NSData *placeData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@get/gestgid/place/%li",SECURED_API_URL,(long)_placeID]]];
        
        NSString *jsonStringMD5 = [[[[NSString alloc] initWithData:placeData encoding:NSUTF8StringEncoding] MD5String] lowercaseString];
        
        NSDictionary *place = [NSJSONSerialization JSONObjectWithData:placeData options:0 error:nil];
        
        [[NSUserDefaults standardUserDefaults] setInteger:[place[@"tstamp"] intValue] forKey:@"placeVersionTimestamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [placeStorage setObject:jsonStringMD5 forKey:@"md5"];
        [placeStorage setObject:place forKey:@"selectedPlaceDictionary"];
        [currentPlaceStorage setObject:place forKey:@"selectedPlaceDictionary"];
        [[localStorage storageWithFilename:@"checkout"] setObject:place[@"checkoutelements"] forKey:[NSString stringWithFormat:@"checkoutElementsForPlace%i",_placeID]];
    }
    else {
        NSLog(@"not updating");
        NSDictionary *place = [placeStorage objectForKey:@"selectedPlaceDictionary"];
        [[NSUserDefaults standardUserDefaults] setInteger:[place[@"tstamp"] intValue] forKey:@"placeVersionTimestamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [currentPlaceStorage setObject:place forKey:@"selectedPlaceDictionary"];
        [[localStorage storageWithFilename:@"checkout"] setObject:place[@"checkoutelements"] forKey:[NSString stringWithFormat:@"checkoutElementsForPlace%i",_placeID]];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"itemsDownloaded"];
    
    [self continueButtonClicked:nil];
}


-(void)showAsOverlay{
    
    CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainScreenSize.width, mainScreenSize.height)];
    _bgView.backgroundColor= [UIColor colorWithWhite:0.0f alpha:0.5f];
    _bgView.alpha = 0.0f;
    _popupView.alpha = 0.0f;
    
    [[UIApplication sharedApplication].keyWindow addSubview:_bgView];
    [[UIApplication sharedApplication].keyWindow addSubview:self.view];
    
    [UIView animateWithDuration:0.5 animations:^{
        _popupView.alpha = 1.0f;
        _bgView.alpha = 1.0f;
    }];
}


-(void)showAsOverlayOnViewController:(UIViewController*)vc{
    
    CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainScreenSize.width, mainScreenSize.height)];
    _bgView.backgroundColor= [UIColor colorWithWhite:0.0f alpha:0.5f];
    _bgView.alpha = 0.0f;
    _popupView.alpha = 0.0f;
    
    [vc.view addSubview:_bgView];
    [vc.view addSubview:self.view];
    
    [UIView animateWithDuration:0.5 animations:^{
        _popupView.alpha = 1.0f;
        _bgView.alpha = 1.0f;
    }];
}

- (IBAction)okButtonPressed:(id)sender {
    [_activityIndicator startAnimating];
    
    _okButton.hidden = YES;
    _cancelButton.hidden = YES;
    _okButton.userInteractionEnabled = NO;
    _cancelButton.userInteractionEnabled = NO;
    
    _progressBar.hidden = NO;
    
    _headLineLabel.text = NSLocalizedString(@"Loading contents", @"");
    
    _progressBar.hidden = YES;
    
    [self downloadPlaceInfo];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{self.backgroundView.alpha = 0.0f;self.view.alpha=0.0f;}
                     completion:^(BOOL finished){
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
}

- (IBAction)continueButtonClicked:(id)sender {
//    void (^onCompletion)(void);
//    if(self.delegate){
//        if([self.delegate respondsToSelector:@selector(hideLoadingInformationView)]){
//            onCompletion = ^{
//                [self.delegate hideLoadingInformationView];
//            };
//        }
//    }
//    [UIView animateWithDuration:0.2 animations:^{self.backgroundView.alpha = 0.0f;self.view.alpha=0.0f;}
//                     completion:^(BOOL finished){
//                         if(finished)
//                             [self dismissViewControllerAnimated:NO completion:onCompletion];
//                     }];
    
    [self dismissViewControllerAnimated:NO completion:^{
        [self.delegate hideLoadingInformationView];
    }];
}


-(void)homeModelDownloadFailedWithError:(NSString *)error{
    [self cancelButtonPressed:nil];
}

@end
