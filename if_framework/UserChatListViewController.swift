//
//  UserChatListViewController.swift
//  if_framework
//
//  Created by Christopher on 10/19/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit
import UserNotifications

class UserChatListViewController: AdminViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var unreadMessagesCount:Int = 0
    var selectedIndexPath:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Messages".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: CGFloat(18.0))!, NSAttributedStringKey.foregroundColor : UIColor.black]
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAfterUnreadUpdate), name: NSNotification.Name(rawValue: kUpdateChatsNotification), object: nil)
        
    
        self.initStyle()
        self.loadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(self.selectedIndexPath != nil){
            self.tableView.reloadRows(at: [self.selectedIndexPath!], with: .fade)
            self.selectedIndexPath = nil
        }
        
        self.checkNotificationPermission()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initStyle() {
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 15+50+15, 0, 0)
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsetsMake(-60, 0, 0, 0)
        self.tableView.tableHeaderView = self.refreshControl
    }
    
    @objc func loadData() {
        self.showLoading()
        DispatchQueue.global().async {
            let response = API.getFrom(SECURED_API_URL + "my-messages/chats/")
            
            DispatchQueue.main.async {
                self.endLoading()
                
                switch response {
                case is [[String:Any]]:
                    self.parseData(data: response as! [[String:Any]])
                    break
                default:
                    break
                }
            }
        }
    }
    
    @objc func reloadAfterUnreadUpdate() {
        self .parseData(data: self.data)
    }
    
    func parseData(data: [[String:Any]]) {
        self.unreadMessagesCount = 0
        
        var newData:[[String:Any]] = []
        for var messageInfo in data {
            let unreadString = (UserMessages.shared.unreadMessages[messageInfo["id"] as! String] ?? "0")!
            
            if(self.selectedIndexPath != nil){
                let idString = messageInfo["id"] as! String
                let row = selectedIndexPath!.row
                if(idString != ((self.data[row] as! [String:String])["id"])){
                    messageInfo["unread"] = unreadString
                    self.unreadMessagesCount += Int(unreadString) ?? 0
                }
            }
            else {
                messageInfo["unread"] = unreadString
                self.unreadMessagesCount += Int(unreadString) ?? 0
            }
            
            if !(UserMessages.shared.newestMessages[messageInfo["id"] as! String]?["content"] ?? "").isEmpty {
                messageInfo["last_message"] = UserMessages.shared.newestMessages[messageInfo["id"] as! String]?["content"]
            }
            
            if !(UserMessages.shared.newestMessages[messageInfo["id"] as! String]?["tstamp"] ?? "").isEmpty {
                messageInfo["tstamp"] = UserMessages.shared.newestMessages[messageInfo["id"] as! String]?["tstamp"]
            }
            
            newData.append(messageInfo)
            
        }
        
        self.updateBadge()
        
        self.data = newData.sorted(by: { (m1, m2) -> Bool in
            return m1["tstamp"] as? String ?? "0" > m2["tstamp"] as? String ?? "0"
        })
        self.tableView.reloadData()
    }
    
    func markRowAsRead(row: Int) {
        let unreadMessages = Int(self.data[row]["unread"] as? String ?? "0")!
        self.unreadMessagesCount -= unreadMessages
        self.updateBadge()
        
        UserMessages.shared.markAsRead(fromUser: self.data[row]["id"] as? String ?? "")
        
        self.data[row]["unread"] = "0"
    }
    
    func updateBadge() {
        if self.unreadMessagesCount > 0 {
            self.navigationController?.tabBarItem.badgeValue = "\(self.unreadMessagesCount)"
            UIApplication.shared.applicationIconBadgeNumber = self.unreadMessagesCount
        }
        else {
            self.navigationController?.tabBarItem.badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AdminTableViewCell
        
        cell.label1?.text = self.data[indexPath.row]["user_name"] as? String ?? ""
        cell.label2?.text = self.data[indexPath.row]["last_message"] as? String ?? ""
        
        cell.tag = indexPath.row
        
        let imagePath = self.data[indexPath.row]["profile_image"] as? String ?? ""
        
        if(imagePath == ""){
            cell.imageView1.image = imageMethods.resize(UIImage(named:"iosicons/ios7-contact.png"), for: CGSize(width: 50, height: 50)).withRenderingMode(.alwaysTemplate)
            cell.imageView1.tintColor = .groupTableViewBackground
            //cell.imageView1.backgroundColor = .groupTableViewBackground
        }
        else {
            DispatchQueue.global().async {
                var image:UIImage? = nil
                if(imagePath.hasPrefix("http")){
                    image = imageMethods.uiImage(withWebPath: imagePath)
                }
                else {
                    image = imageMethods.uiImage(withServerPathName: imagePath, width: 50)
                }
                
                    DispatchQueue.main.async {
                        if(image != nil && cell.tag == indexPath.row){
                        cell.imageView1.image = image
                        cell.setNeedsLayout()
                        }
                        else {
                        }
                    }
            }
        }
        
        if Int(self.data[indexPath.row]["unread"] as? String ?? "0")! > 0 {
            cell.badge1.text = self.data[indexPath.row]["unread"] as? String ?? "0"
            cell.label2.font = UIFont(name: "HelveticaNeue-Bold", size: 17)!
        }
        else {
            cell.badge1.text = "0"
            cell.label2.font = UIFont(name: "HelveticaNeue", size: 17)!
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        self.markRowAsRead(row: indexPath.row)
        
        self.selectedIndexPath = indexPath
        
        switch self.data[indexPath.row]["id"] {
        case is String:
            let cvc = UserChatViewController()
            cvc.to = Int(self.data[indexPath.row]["id"] as! String)!
            cvc.title = self.data[indexPath.row]["user_name"] as? String ?? ""
            _ = self.navigationController?.pushViewController(cvc, animated: true)
            break
        case is Int:
            let cvc = UserChatViewController()
            cvc.to = self.data[indexPath.row]["id"] as! Int
            cvc.title = self.data[indexPath.row]["user_name"] as? String ?? ""
            _ = self.navigationController?.pushViewController(cvc, animated: true)
            break
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func checkNotificationPermission() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings(){ (settings) in
                if settings.soundSetting == .disabled || settings.alertSetting == .disabled {
                    self.notPermittedPopup()
                }
            }
        } else {
            if(!UIApplication.shared.isRegisteredForRemoteNotifications){
                self.notPermittedPopup()
            }
        }
    }
    
    func notPermittedPopup() {
        //CWAlert.showNotification(withTitle: "Push notifications restricted".localized, message: "This app needs permission to send push notifications, in order to make chat work properly.".localized, onViewController: self)
        
        if(!UserDefaults.standard.bool(forKey: "dontShowNotificationPermissionPopup")){
            let alert = UIAlertController(title: "Push notifications restricted".localized, message: "This app needs permission to send push notifications, in order to make chat work properly.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (nil) in
                if #available(iOS 10.0, *) {
                    let center = UNUserNotificationCenter.current()
                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                        // Enable or disable features based on authorization.
                    }
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Don't show again".localized, style: .default, handler: { (nil) in
                UserDefaults.standard.set(true, forKey: "dontShowNotificationPermissionPopup")
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel".localized, style: .default, handler: { (nil) in
                
            }))
        
            self.present(alert, animated: true, completion: { 
                
            })
        }
    }
}
