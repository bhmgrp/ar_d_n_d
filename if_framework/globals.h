//
//  globals.h
//  if_framework
//
//  Created by User on 12/4/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface globals : NSObject

+(globals *)sharedInstance;

@property (nonatomic) WKWebView *wkwebview;

@property (nonatomic) NSUInteger pID;
@property (nonatomic) NSUInteger placeID;
@property (nonatomic) NSUInteger campaignID;
@property (nonatomic) NSUInteger offerCampaignID;

@property (nonatomic) NSString *iFeedbackLink;

@property (nonatomic) NSString *currentLanguageKey;

@property (nonatomic, readonly) BOOL terminalMode;

@property (nonatomic, readonly) BOOL userLoggedIn;

-(NSString*)getIFeedbackLink;

-(void)setPID:(NSUInteger)pID;
-(void)setPlaceID:(NSUInteger)placeID;
-(void)setCampaignID:(NSUInteger)campaignID;
-(void)setOfferCampaignID:(NSUInteger)offerCampaignID;

+(void)setPID:(NSUInteger)pID;
+(void)setPlaceID:(NSUInteger)placeID;
+(void)setCampaignID:(NSUInteger)campaignID;
+(void)setOfferCampaignID:(NSUInteger)offerCampaignID;

-(void)setCurrentLanguageKey:(NSString*)languageKey;

@end
