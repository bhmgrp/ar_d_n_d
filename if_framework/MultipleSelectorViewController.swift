//
//  MultipleSelectorViewController.swift
//  if_framework
//
//  Created by Christopher on 10/24/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

class MultipleSelectorViewController: AdminViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var options:[String?]?
@objc    var selected:[Int] = []
@objc    var selectedOptions:[String] = []
@objc    var onHide:()->Void = {}
    
@objc   static func getWith(options:[String]) -> MultipleSelectorViewController{
        let vc = get(viewController:"MultipleSelectorViewController") as! MultipleSelectorViewController
        
        vc.options = options
        
        return vc
    }
    
@objc   static func getWith(optionsFromUrl:String) -> MultipleSelectorViewController{
        let vc = get(viewController:"MultipleSelectorViewController") as! MultipleSelectorViewController
        
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: vc, action: #selector(hide))
        
        vc.showLoading()
        DispatchQueue.global().async {
            
            let options = API.getFrom(optionsFromUrl)
            
            if(options is [String]){
                vc.options = options as! [String]
            }
            
            DispatchQueue.main.async {
                vc.endLoading()
                vc.tableView.reloadData()
            }
        }
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.initStyle(forTableView: self.tableView)
        self.tableView.tableHeaderView = nil
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hide))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.onHide()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weekday") as! AdminTableViewCell
        
        if(self.options != nil){
            cell.label1.text = self.options![indexPath.row] ?? ""
        }
        else {
            cell.label1.text = self.data[indexPath.row]["name"] as? String ?? ""
        }
        
        
        if(self.selected.contains(indexPath.row) || self.selectedOptions.contains(self.options?[indexPath.row] ?? "NONE")){
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.options != nil){
            return self.options!.count
        }
        else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        if(self.selected.contains(indexPath.row)){
            self.selected.remove(at: self.selected.index(of: indexPath.row)!)
            if self.selectedOptions.contains(self.options?[indexPath.row] ?? "NONE") {
                self.selectedOptions.remove(at: self.selectedOptions.index(of: self.options?[indexPath.row] ?? "NONE")!)
            }
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
        else {
            self.selected.append(indexPath.row)
            self.selectedOptions.append(self.options?[indexPath.row] ?? "")
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func hide() {
        if(self.navigationController != nil){
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {
            self.dismiss(animated: true, completion: {
                
            })
        }
    }

}
