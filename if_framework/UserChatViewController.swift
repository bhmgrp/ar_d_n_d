/*
 The MIT License (MIT)
 
 Copyright (c) 2015-present Badoo Trading Limited.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

import UIKit
import Chatto
import ChattoAdditions

class UserChatViewController: BaseChatViewController {
    
    var from:Int = 0
    var to:Int = 0
    
    lazy var messageSender: MessageSender = {
        let sender = MessageSender(from: self.from, to: self.to)
        sender.onMessageChanged = { [weak self] (message) in
            guard let sSelf = self else { return }
            sSelf.chatDataSourceDidUpdate(sSelf.dataSource)
        }
        
        return sender
    }()
    
    var dataSource: ChatDataSource! {
        didSet {
            self.chatDataSource = self.dataSource
            self.chatDataSourceDidUpdate(self.dataSource)
        }
    }
    
    lazy private var baseMessageHandler: BaseMessageHandler = {
        return BaseMessageHandler(messageSender: self.messageSender)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.from = UserDefaults.standard.integer(forKey: "loggedInUserID")
        
        self.dataSource = ChatDataSource(from:from, to:to)
        
        self.view.backgroundColor = .white
        
        super.chatItemsDecorator = ChatItemsDemoDecorator()
        
        //let addIncomingMessageButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(UserChatViewController.addRandomIncomingMessage))
        //self.navigationItem.rightBarButtonItem = addIncomingMessageButton
    }
    
    @objc
    private func addRandomIncomingMessage() {
        //self.dataSource.addRandomIncomingMessage()// create a corresponding local notification
        NSLog("sending notification")
        
        let notification = UILocalNotification()
        
        notification.alertBody = "Test body"
        notification.alertAction = "open"
        notification.fireDate = Date(timeIntervalSinceNow: 15)
        notification.userInfo = ["url":"{\"update\":\"chats\"}"]
        
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    var chatInputPresenter: BasicChatInputBarPresenter!
    override func createChatInputView() -> UIView {
        let chatInputView = Bundle(for: UserChatViewController.self).loadNibNamed("CustomChatInputView", owner: nil, options: nil)!.first as! ChatInputBar
        chatInputView.translatesAutoresizingMaskIntoConstraints = false
        chatInputView.frame = CGRect.zero
        
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonAppearance.title = NSLocalizedString("Send", comment: "")
        appearance.sendButtonAppearance.font = UIFont(name: "HelveticaNeue", size: 17)!
        appearance.textInputAppearance.placeholderText = "Type a message".localized
        appearance.textInputAppearance.font = UIFont(name: "HelveticaNeue", size: 17)!
        appearance.textInputAppearance.placeholderFont = UIFont(name: "HelveticaNeue", size: 17)!
        self.chatInputPresenter = BasicChatInputBarPresenter(chatInputBar: chatInputView, chatInputItems: self.createChatInputItems(), chatInputBarAppearance: appearance)
        chatInputView.maxCharactersCount = 1000
        return chatInputView
    }
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        
        let textMessagePresenter = TextMessagePresenterBuilder(
            viewModelBuilder: DemoTextMessageViewModelBuilder(),
            interactionHandler: DemoTextMessageHandler(baseHandler: self.baseMessageHandler)
        )
        textMessagePresenter.baseMessageStyle = BaseMessageCollectionViewCellAvatarStyle()
        
        let photoMessagePresenter = PhotoMessagePresenterBuilder(
            viewModelBuilder: DemoPhotoMessageViewModelBuilder(),
            interactionHandler: DemoPhotoMessageHandler(baseHandler: self.baseMessageHandler)
        )
        photoMessagePresenter.baseCellStyle = BaseMessageCollectionViewCellAvatarStyle()
        
        return [
            DemoTextMessageModel.chatItemType: [
                textMessagePresenter
            ],
            DemoPhotoMessageModel.chatItemType: [
                photoMessagePresenter
            ],
            SendingStatusModel.chatItemType: [SendingStatusPresenterBuilder()],
            TimeSeparatorModel.chatItemType: [TimeSeparatorPresenterBuilder()]
        ]
    }

    
    func createChatInputItems() -> [ChatInputItemProtocol] {
        var items = [ChatInputItemProtocol]()
        items.append(self.createTextInputItem())
        //items.append(self.createPhotoInputItem())
        return items
    }
    
    private func createTextInputItem() -> TextChatInputItem {
        let item = TextChatInputItem()
        item.textInputHandler = { [weak self] text in
            let newText = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self?.dataSource.addTextMessage(newText)
        }
        return item
    }
    
    private func createPhotoInputItem() -> PhotosChatInputItem {
        let item = PhotosChatInputItem(presentingController: self)
        //item.photoInputHandler = { [weak self] image in
            //self?.dataSource.addPhotoMessage(image)
        //}
        return item
    }
}
