//
//  iFeedbackTableViewCell.m
//  if_framework
//
//  Created by User on 6/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackTableViewCell.h"

@implementation iFeedbackTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if(IDIOM!=IPAD || IFBCKINTEGRATED){
        for(NSLayoutConstraint *constraint in _constraintsToResizeBy1of2){
            constraint.constant = constraint.constant / 2 * 1;
        }
        
        for(NSLayoutConstraint *constraint in _constraintsToResizeBy3of4){
            constraint.constant = constraint.constant / 4 * 3;
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)resizeFontFor:(UIView *)item{
    if([item isKindOfClass:[UITextField class]]){
        ((UITextField*)item).font = [UIFont fontWithName:((UITextField*)item).font.fontName size:((UITextField*)item).font.pointSize/1.58888];
    }
    else if([item isKindOfClass:[UITextView class]]){
        ((UITextView*)item).font = [UIFont fontWithName:((UITextView*)item).font.fontName size:((UITextView*)item).font.pointSize/1.58888];
    }
    else if([item isKindOfClass:[UILabel class]]){
        ((UILabel*)item).font = [UIFont fontWithName:((UILabel*)item).font.fontName size:((UILabel*)item).font.pointSize/1.58888];
    }
}

@end
