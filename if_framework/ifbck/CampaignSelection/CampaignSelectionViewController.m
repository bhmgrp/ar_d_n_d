//
//  CampaignSelectionViewController.m
//  if_framework_terminal
//
//  Created by User on 5/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "CampaignSelectionViewController.h"
#import "CampaignSelectionTableViewCell.h"

#import "ifbckNavigationViewController.h"
#import "TopicListViewController.h"

@interface CampaignSelectionViewController (){
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) NSString *languageKey;

@end

@implementation CampaignSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    if(super.ifbckModel.place[@"header"] != nil && ![super.ifbckModel.place[@"header"] isEmptyString]){
        self.title = super.ifbckModel.place[@"header"];
    }
    else {
        self.title = [super.ifbckModel getTranslationForKey:@"choose_campaign"];
    }
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:nil action:nil]];

    [self initNavigationBarButtons];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    super.ifbckModel.parameters = [super.ifbckModel.place[@"parameters"] isKindOfClass:[NSDictionary class]]?super.ifbckModel.place[@"parameters"]:@{};
    super.ifbckModel.topicRatings = [@{} mutableCopy];
    
    if(_languageKey!=nil){
        if(![_languageKey isEqualToString:[languages currentLanguageKey]]){
            _languageKey = [languages currentLanguageKey];
            
            [super.ifbckModel loadClient];
            
            [self initNavigationBarButtons];
        }
    }
    else {
        _languageKey = [languages currentLanguageKey];
    }
}

- (void)viewDidLayoutSubviews {
    [self initFooter];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
    [((ifbckNavigationViewController*)self.navigationController) removeTopicFooter];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = super.tableViewBackgroundColor;
}

- (void)initFooter{
    
    // footer background
    if(super.ifbckModel.place[@"footer_bgcolor"]!=nil && ![super.ifbckModel.place[@"footer_bgcolor"] isEmptyString]){
        _footerBackgroundView.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.place[@"footer_bgcolor"]];
    }
    else {
        _footerBackgroundView.hidden = YES;
        _footerBackgroundView.backgroundColor = [UIColor clearColor];
    }
    
    // footer logo
    if(super.ifbckModel.place[@"footer_logo"]!=nil && ![super.ifbckModel.place[@"footer_logo"] isEmptyString]){
        // load image in background
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            UIImage *footerLogoImage = [imageMethods UIImageWithServerPathName:super.ifbckModel.place[@"footer_logo"]  versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)super.ifbckModel.pID,350] width:350];
            
            // update UI in main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                _footerImage.image = footerLogoImage;
            });
        });
    }
    else {
        _footerImage.hidden = YES;
    }
    
    //_tableView.contentInset = UIEdgeInsetsMake(0, 0, footerHeight, 0);
    //_tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, footerHeight, 0);
}

- (void)initNavigationBarButtons{
    // right bar button item
    NSArray *languagesArray = [(NSString*)super.ifbckModel.parameters[@"languages"] componentsSeparatedByString:@";"];
    if(languagesArray.count>1){
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Language" style:UIBarButtonItemStylePlain target:self action:@selector(openLanguagesViewController)];
        NSString *flagKey = [languages currentLanguageKey];
        
        [_languageSelectorButton addTarget:self action:@selector(openLanguagesViewController) forControlEvents:UIControlEventTouchUpInside];
        
        _languageSelectorImageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.gif",flagKey]];;
        
    }
}

- (void)openLanguagesViewController{
    [self.navigationController pushViewController:[super.ifbckModel getLanguageSelectViewController] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0)
        return;
    
    super.ifbckModel.campaign = _campaigns[indexPath.row-1];
    super.ifbckModel.campaignID = [_campaigns[indexPath.row-1][@"uid"] intValue];
    super.ifbckModel.surveyTopics = _campaigns[indexPath.row-1][@"survey_topics"];
    super.ifbckModel.parameters = _campaigns[indexPath.row-1][@"parameters"];
    super.ifbckModel.dynamicFields = _campaigns[indexPath.row-1][@"dynamicfields"];
    
    if(_campaigns[indexPath.row-1][@"parameters"][@"skip_to_contactform"] != nil && [_campaigns[indexPath.row-1][@"parameters"][@"skip_to_contactform"] isEqualToString:@"yes"]){
        UIViewController *vc = [super.ifbckModel getDetailViewController];
        
        [self.navigationController pushViewController:vc animated:YES];
        
        [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
        
        return;
    }
    else {
        TopicListViewController *vc = (TopicListViewController*)[super.ifbckModel getTopicViewController];
        
        [self.navigationController pushViewController:vc animated:YES];
        
        [vc initRootViewController];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        if(IDIOM==IPAD){
            return 164.0f * screenMultiplicator-130+45;
        }
        
        return 164.0f * screenMultiplicator+45;
    }
    
    if(IDIOM==IPAD){
        return 90;
    }
    
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _campaigns.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        CampaignSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        
        cell.separatorInset = UIEdgeInsetsMake(0, screenWidth, 500, 0);
        
        cell.tag = indexPath.row-1;
        
        if(super.ifbckModel.parameters[@"app_header_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_header_text_color"] isEmptyString]){
            //cell.headerLabel.textColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_header_text_color"]];
        }
        
        // if we have a custom header view set
        if(super.ifbckModel.parameters[@"app_header_image"]!=nil && ![super.ifbckModel.parameters[@"app_header_image"] isEmptyString]){
            // load the image in the background
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
                UIImage *image = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_header_image"]  versionIdentifier:[NSString stringWithFormat:@"pid%li",(long)super.ifbckModel.pID] width:screenWidth];
                
                if(image && cell.tag == indexPath.row-1){
                    // update cell in main queue
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.headerImage.image = image;
                        [cell setNeedsLayout];
                    });
                }
            });
        }
        else {
            // else set the default header view
            _headerView.width = screenWidth;
            _headerView.height = [self tableView:tableView heightForRowAtIndexPath:indexPath];
            
            [cell addSubview:_headerView];
        }
        
        cell.headerLabel.text = super.ifbckModel.place[@"description"];
        
        return cell;
    }
    
    CampaignSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"campaignCell"];
    
    cell.tag = indexPath.row-1;
    
    cell.backgroundColor = super.tableViewCellBackgroundColor;
    cell.nameLabel.textColor = super.tableViewCellTintColor;
    cell.nameLabel.text = _campaigns[indexPath.row-1][@"name"];
    
    if(indexPath.row-1 == _campaigns.count-1){
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    else {
        if(IDIOM!=IPAD)
            cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        else
            cell.separatorInset = UIEdgeInsetsMake(0, 45, 0, 0);
    }
    
    return cell;
}

@end
