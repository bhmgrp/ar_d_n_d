//
//  CampaignSelectTableViewCell.m
//  if_framework_terminal
//
//  Created by User on 5/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "CampaignSelectionTableViewCell.h"

@implementation CampaignSelectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if(IDIOM!=IPAD || IFBCKINTEGRATED){
        [self resizeFontFor:_nameLabel];
        [self resizeFontFor:_headerLabel];
        
        _leftSpaceNameLabel.constant = 15;
        _leftSpaceHeaderLabel.constant = 15;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
