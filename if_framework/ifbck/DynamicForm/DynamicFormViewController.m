//
//  DynamicFormViewController.m
//  if_framework
//
//  Created by User on 31.08.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "DynamicFormViewController.h"
#import "HCSStarRatingView.h"
#import "dynamicFields.h"

#import "ifbckNavigationViewController.h"
#import "LanguageSelectViewController.h"

#import "ifbckSecretSettingsViewController.h"

@interface DynamicFormViewController (){
    float startingViewHeight;
    UIColor *placeHolderColor;
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
    BOOL showRequired, showAskOnce;
    float imageUploadProgress;
}

//@property (nonatomic) NSMutableArray *valueArray;

@property (nonatomic) int numOfRequiredFields,numOfAskOnceFields,numOfConditionalRequiredFields;

//@property (nonatomic) localStorage *dynamicUserSettingsStorage;

@property (nonatomic) NSMutableArray *requiredFieldIndexes;

@property (nonatomic) NSMutableArray *askOnceIndexes;

@property (nonatomic) NSString *languageKey;

@property (nonatomic) BOOL feedbackSent,sendingFeedback;

@property (nonatomic) UIAlertView *successAlertView;

@property (nonatomic) UIImage *starRatingImageFilled, *starRatingImageEmpty;

@property (nonatomic) localStorage *dynamicUserSettingsStorage;

@property (nonatomic) UIActionSheet *imageSelectionActionSheet;

@property (nonatomic) NSIndexPath *selectedIndexPath;

@property (nonatomic) UIImage *image;

@end

@implementation DynamicFormViewController


#pragma mark - View methods

- (void)viewDidLoad {
    [super viewDidLoad];

    _sendButtonWrapperBottomSpace.constant = 0;

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable", @"");
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    placeHolderColor = [UIColor colorWithRGBHex:0xBBBAC2];

    _dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self registerForKeyboardNotifications];
    [self translateOutlets];
    [self initNavigationBarButtons];
    [self updateButtonStyle];
    
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, 30, 0);
    
    
    _sendButtonBackground.layer.cornerRadius = 5.0f;
    
    if(super.ifbckModel.parameters[@"app_navigation_bar_logo"]!=nil && ![super.ifbckModel.parameters[@"app_navigation_bar_logo"] isEmptyString]){
        // load image
        UIImage *titleImage = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_navigation_bar_logo"] versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)super.ifbckModel.pID,150] width:300];
        UIView *titleView = [[UIImageView alloc] initWithImage:titleImage];
        
        titleView.contentMode = UIViewContentModeScaleAspectFit;
        titleView.height = 44;
        
        // update UI
        self.navigationItem.titleView = titleView;
    }
    
    // star images
    CGSize scaledSize = CGSizeMake(57, 57);
    UIImage *starImage;
    
    starImage = [UIImage imageNamed:@"star-white.png"];
    
    if(super.ifbckModel.parameters[@"app_unselected_star_image"]!=nil && ![super.ifbckModel.parameters[@"app_unselected_star_image"] isEmptyString]){
        starImage = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_unselected_star_image"] versionIdentifier:super.ifbckModel.parameters[@"app_version"] width:57];
    }
    
    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [starImage drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    _starRatingImageEmpty = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    starImage = [UIImage imageNamed:@"star-gold.png"];
    
    if(super.ifbckModel.parameters[@"app_selected_star_image"]!=nil && ![super.ifbckModel.parameters[@"app_selected_star_image"] isEmptyString]){
        starImage = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_selected_star_image"] versionIdentifier:super.ifbckModel.parameters[@"app_version"] width:57];
    }
    
    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [starImage drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    _starRatingImageFilled = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(IDIOM!=IPAD){
        [_sendButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
        _sendButtonWrapperHeight.constant = 64;
    }
    _imageUploadProgressView.progress = imageUploadProgress;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    if(_languageKey!=nil){
        if(![_languageKey isEqualToString:[languages currentLanguageKey]]){
            _languageKey = [languages currentLanguageKey];

            [super.ifbckModel loadClient];

            [self translateOutlets];
            [self initNavigationBarButtons];
        }
    }
    else {
        _languageKey = [languages currentLanguageKey];
    }

    [self showTestingLabel];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self updateButtonStyle];
}


- (void)initNavigationBarButtons{
    
    // right bar button item
    NSArray *languagesArray = [(NSString*)super.ifbckModel.parameters[@"languages"] componentsSeparatedByString:@";"];
    if(languagesArray.count>1){
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Language" style:UIBarButtonItemStylePlain target:self action:@selector(openLanguagesViewController)];
        NSString *flagKey = [languages currentLanguageKey];
        
        [_languageSelectorButton addTarget:self action:@selector(openLanguagesViewController) forControlEvents:UIControlEventTouchUpInside];
        
        _languageSelectorImageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.gif",flagKey]];;
        
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];
    
    //[self.navigationItem setHidesBackButton:YES animated:YES];
}

- (void)showTestingLabel{
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"appInTestingMode"]){
        UIView *v = [[UIView alloc] initWithFrame:[UIApplication sharedApplication].statusBarFrame];
        
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 22)];
        l.text = @"testing mode";
        l.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        l.textColor = [UIColor redColor];
        [l sizeToFit];
        
        l.center = CGPointMake(screenWidth/4*3, v.center.y);
        
        
        [v addSubview:l];
        
        [[UIApplication sharedApplication].keyWindow addSubview:v];
    }
}

- (void)openLanguagesViewController{
    [self.navigationController pushViewController:[super.ifbckModel getLanguageSelectViewController] animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void)translateOutlets{
    self.navigationItem.title = super.ifbckModel.place[@"name"];
}

- (void)updateButtonStyle{
    
    [_sendButton setTitle:[[super.ifbckModel getTranslationForKey:@"pi1_contactform_feedbacksubmit"] uppercaseString] forState:UIControlStateNormal];
    [_sendButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [_sendButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    
    
    // button background
    if(super.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![super.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        // creating normal background
        _sendButton.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
    }
    else {
        _sendButton.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    }
    
    [_sendButton.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [_sendButton.layer setBorderWidth:1.0f];
    [_sendButton.layer setCornerRadius:5.0f];
    
    
    // button text color
    if(super.ifbckModel.parameters[@"app_send_button_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_send_button_text_color"] isEmptyString]){
        [_sendButton setTitleColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_sendButton.layer setBorderColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
    }
    
    
    // button border color
    if(super.ifbckModel.parameters[@"app_send_button_border_color"]!=nil && ![super.ifbckModel.parameters[@"app_send_button_border_color"] isEmptyString]){
        [_sendButtonWrapperBackground setBackgroundColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_border_color"]]];
    }
    
    // button background
    if(super.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![super.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        _sendButtonBackground.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
        _sendButtonBackground.hidden  = NO;
        _sendButtonDefaultBackground.hidden = YES;
    }
    
}

- (void)viewDidLayoutSubviews{
    if(startingViewHeight == 0.0){
        startingViewHeight = self.view.height;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadTable{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    //_dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    
    NSMutableArray *dynamicFieldsArray = [@[] mutableCopy];
    
    _requiredFieldIndexes = [@[] mutableCopy];
    _askOnceIndexes = [@[] mutableCopy];
    
    int row = 0;
    
    if(super.ifbckModel.dynamicFields.count>0){
        if([super.ifbckModel.dynamicFields[0][@"field_type"] intValue] != DYNAMICFIELD_APP_IMAGE){
            NSMutableArray *array = [@[@{@"field_type":@(DYNAMICFIELD_APP_IMAGE),@"options":@""}] mutableCopy];
            [array addObjectsFromArray:super.ifbckModel.dynamicFields];
            super.ifbckModel.dynamicFields = array;
        }
    }

    for (NSDictionary *dict in super.ifbckModel.dynamicFields){
        NSMutableDictionary *newDict = [dict mutableCopy];
        
        int inputType = [newDict[@"field_type"] intValue];
        
        switch (inputType) {
            case DYNAMICFIELD_DROPDOWN:{
                newDict[@"options"] = [newDict[@"options"] componentsSeparatedByString:@";"];
                
                break;
            }
            case DYNAMICFIELD_DATE:{
                
                break;
            }
            case DYNAMICFIELD_STARRATING:{
                break;
            }
            case DYNAMICFIELD_HTML:{
                continue;
            }
            case DYNAMICFIELD_PAGE:{
                continue;
            }
            case DYNAMICFIELD_COMBINEDCHECKBOXES:{
                continue;
            }
            default:{
                break;
            }
        }
        
        if([newDict[@"required"] intValue]==1){
            _numOfRequiredFields++;
        }
        
        if([newDict[@"required"] intValue]==2){
            _numOfAskOnceFields++;
        }
        
        if(newDict[@"conditionalreqfields"] !=nil && [newDict[@"conditionalreqfields"] isKindOfClass:[NSString class]] && ![newDict[@"conditionalreqfields"] isEqualToString:@""]){
            _numOfConditionalRequiredFields++;
        }
        
        
        [self preFillInputDict:newDict];
        
        
        [dynamicFieldsArray addObject:[newDict mutableCopy]];
        
        row++;
    }
    
    super.ifbckModel.dynamicFields = dynamicFieldsArray;
    
    [_tableView reloadData];
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}

- (BOOL)hideCellAtIndexPath:(NSIndexPath*)indexPath{
    int inputType = [super.ifbckModel.dynamicFields[indexPath.row][@"field_type"] intValue];

    switch (inputType){

        case DYNAMICFIELD_REGISTRATION:{
            return YES;
        }
        case DYNAMICFIELD_APP_PROFILE_IMAGE:{
            return YES;
        }
        case DYNAMICFIELD_PHOTOUPLOAD:{
            return NO;
        }
        case DYNAMICFIELD_APP_SETTINGS_CONFIRMED:{
            return ![ifbckGlobals sharedInstance].userLoggedIn;
        }
        default:break;
    }

    // if the user is not logged in don't hide any other fields
    if(![ifbckGlobals sharedInstance].userLoggedIn){
        return NO;
    }

    // if the user is logged in hide these fields
    if(inputType==DYNAMICFIELD_EMAILINPUT){
        return !([_dynamicUserSettingsStorage objectForKey:@"email"]==nil || [[_dynamicUserSettingsStorage objectForKey:@"email"] isEmptyString]);
    }
    if(inputType==DYNAMICFIELD_NAME){
        return NO;
    }
    if(inputType==DYNAMICFIELD_REGISTRATION){
        return YES;
    }


    return NO;

}

- (NSMutableDictionary*)preFillInputDict:(NSMutableDictionary*)inputDict{
    int inputType = [inputDict[@"field_type"] intValue];
    if(inputType==DYNAMICFIELD_EMAILINPUT){
        inputDict[@"value"]=[_dynamicUserSettingsStorage objectForKey:@"email"];
    }
    if(inputType==DYNAMICFIELD_NAME){
        NSString *firstName = ([_dynamicUserSettingsStorage objectForKey:@"firstName"]!=nil)?[_dynamicUserSettingsStorage objectForKey:@"firstName"]:@"";
        NSString *lastName = ([_dynamicUserSettingsStorage objectForKey:@"lastName"]!=nil)?[_dynamicUserSettingsStorage objectForKey:@"lastName"]:@"";

        NSString *value = [[NSString stringWithFormat:@"%@ %@",firstName,lastName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        if(![value isEqualToString:@""])
            inputDict[@"value"] = value;
    }
    if(inputType==DYNAMICFIELD_REGISTRATION){
        NSDictionary *dynamicUserSettingsValues = [_dynamicUserSettingsStorage objectForKey:@"dynamicUserSettingsKeyValues"];
        inputDict[@"value"]=dynamicUserSettingsValues;
    }
    if(inputType==DYNAMICFIELD_APP_PROFILE_IMAGE) {

        NSString *profileImageFileName = [_dynamicUserSettingsStorage objectForKey:@"profileImageName"];
        NSString *profileImageFilePath = [_dynamicUserSettingsStorage objectForKey:@"profileImagePath"];
        if(profileImageFileName!=nil && profileImageFilePath!=nil){
            NSString *value = [NSString stringWithFormat:@"%@/%@",profileImageFilePath,profileImageFileName];

            inputDict[@"value"] = value;
        }
    }
    if(inputDict[@"fill_from_user_attribute"]!=nil && inputDict[@"fill_from_user_attribute"] != 0){
        NSString *userAttributeKey = [NSString stringWithFormat:@"%@", inputDict[@"fill_from_user_attribute"]];

        NSDictionary *dynamicUserSettingsKeyValues = [_dynamicUserSettingsStorage objectForKey:@"dynamicUserSettingsKeyValues"];

        // return if that isn't a dictionary of settings
        if(![dynamicUserSettingsKeyValues isKindOfClass:[NSDictionary class]])
            return inputDict;


        id value = dynamicUserSettingsKeyValues[userAttributeKey];


        if(value!=nil && value!=0 && !([value isKindOfClass:[NSString class]] && [value isEqualToString:@""]))
            inputDict[@"value"] = value;

    }

    return inputDict;
}


#pragma mark - TableView datasource and delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    _selectedIndexPath = indexPath;

    if(super.ifbckModel.dynamicFields.count>indexPath.row){
        NSMutableDictionary *inputDict = super.ifbckModel.dynamicFields[indexPath.row];
        if(![inputDict isKindOfClass:[NSDictionary class]]){
            return;
        }
        int fieldType = [inputDict[@"field_type"] intValue];
        
        switch(fieldType){
            case DYNAMICFIELD_DROPDOWN:{
                [self.view endEditing:YES];
                _pickerSelectedInputRow = indexPath.row;
                [self showDropdownPickerWithOptionsArray:inputDict[@"options"]];
                [self.view endEditing:YES];
                self.editing = NO;
                return;
            }
            case DYNAMICFIELD_DATE:{
                [self.view endEditing:YES];
                _pickerSelectedInputRow = indexPath.row;
                [self showDatePicker];
                [self.view endEditing:YES];
                self.editing = NO;
                return;
            }
            case DYNAMICFIELD_TEXT:{
                [self.view endEditing:YES];
                self.editing = NO;
                break;
            }
            case DYNAMICFIELD_TEXTAREA:{
                [self.view endEditing:YES];
                self.editing = NO;
                break;
            }
            case DYNAMICFIELD_EMAILINPUT:{
                [self.view endEditing:YES];
                self.editing = NO;
                break;
            }
            case DYNAMICFIELD_PHOTOUPLOAD:{

                self.imageSelectionActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Camera",@""),NSLocalizedString(@"Album",@""),NSLocalizedString(@"Delete photo",@""), nil];
                [self.imageSelectionActionSheet setDestructiveButtonIndex:2];

                [self.imageSelectionActionSheet showInView:self.view];
                return;
            }
            default:{
                
            }
        }
    }
    else {
        [self.view endEditing:YES];
        self.editing = NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSMutableDictionary *inputDict = super.ifbckModel.dynamicFields[indexPath.row];
    if([self hideCellAtIndexPath:indexPath]){
        return 0;
    }
    
    int fieldTypeNumber = [inputDict[@"field_type"] intValue];
    
    switch (fieldTypeNumber) {
        case DYNAMICFIELD_HTML:return 0.0f;
        case DYNAMICFIELD_PHOTOUPLOAD:{

            if(IDIOM!=IPAD)
                return 60.0f;
            else
                return 80.0f;
        }
        case DYNAMICFIELD_DROPDOWN:{
            
            if(IDIOM!=IPAD)
                return 60.0f;
            else
                return 80.0f;
        }
        case DYNAMICFIELD_DATE:{
            
            if(IDIOM!=IPAD)
                return 60.0f;
            else
                return 80.0f;
        }
        case DYNAMICFIELD_APP_IMAGE:{
            if(IDIOM==IPAD){
                return 164.0f * screenMultiplicator-130;
            }
            
            return 164.0f * screenMultiplicator;
            
        }
        case DYNAMICFIELD_TEXTAREA:{
            if(IDIOM!=IPAD)
                return 90.0f;
            else
                return 120.0f;
        }
        case DYNAMICFIELD_STARRATING:{
            if([inputDict[@"has_textfield"] intValue]==1 || [inputDict[@"has_textfield"] intValue]==2){
                if(inputDict[@"value"]==nil || [inputDict[@"value"] intValue]==0){
                    if(IDIOM!=IPAD)
                        return 90;
                    return 107;
                }
                return 205+24;
            }
            else {
                if(IDIOM!=IPAD)
                    return 90;
                return 90+24;
            }
        }
        case DYNAMICFIELD_APP_DIVIDER:{
            float deviderTitleHeight = 45.0f;
            
            if(
               inputDict[@"label"] != nil &&
               inputDict[@"description"] != nil &&
               [inputDict[@"label"] isKindOfClass:[NSString class]] &&
               [inputDict[@"description"] isKindOfClass:[NSString class]] &&
               ![inputDict[@"label"] isEqualToString:@""] &&
               ![inputDict[@"description"] isEqualToString:@""]
               ){
                UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(8, 0, screenWidth, 0)];
                tv.text = inputDict[@"description"];
                [tv sizeToFit];
                return deviderTitleHeight+tv.height-8.0f;
            }
            if(
               inputDict[@"label"]!=nil &&
               [inputDict[@"label"] isKindOfClass:[NSString class]] &&
               ![inputDict[@"label"] isEqualToString:@""]
               ){
                return deviderTitleHeight;
            }
            if(
               inputDict[@"description"]!=nil &&
               [inputDict[@"description"] isKindOfClass:[NSString class]] &&
               ![inputDict[@"description"] isEqualToString:@""]
               ){
                UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(8, 0, screenWidth, 0)];
                tv.text = inputDict[@"description"];
                [tv sizeToFit];
                return tv.height;
            }
        }
            
        default:return 60;
    }
    
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return super.ifbckModel.dynamicFields.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *fieldTypeIdentifier = @"";
    
    int fieldType = [super.ifbckModel.dynamicFields[indexPath.row][@"field_type"] intValue];
    
    fieldTypeIdentifier = [dynamicFields getFieldStringForTypeNumber:fieldType];
    
    
    // init the cell
    DynamicFormTableViewCell *c;
    c = [tableView dequeueReusableCellWithIdentifier:fieldTypeIdentifier];
        
    // remove all visible texts
    c.label.text = nil;
    c.dropdownSelectedValueLabel.text = nil;
    
    // if the input for this sell should be set automatically
    if([self hideCellAtIndexPath:indexPath] || c == nil){
        // return the empty cell (which has a height of 0)
        if(c==nil){
            c = [[DynamicFormTableViewCell alloc] init];
        }
        [c.contentView removeFromSuperview];
        c.accessoryType = UITableViewCellAccessoryNone;
        return c;
    }

    CGSize scaledSize = CGSizeMake(c.indicatorIcon.width, c.indicatorIcon.height);

    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [c.indicatorIcon.image drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    c.indicatorIcon.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    c.indicatorIcon.image = [c.indicatorIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    c.indicatorIcon.tintColor = placeHolderColor;
    
    // remove separator
    if(indexPath.row!=super.ifbckModel.dynamicFields.count-1){
        int nextFieldType = [super.ifbckModel.dynamicFields[indexPath.row+1][@"field_type"] intValue];
        
        switch(nextFieldType){
            case DYNAMICFIELD_APP_DIVIDER:{
                c.separatorInset = UIEdgeInsetsMake(0.f, screenWidth, 0.f, 0.f);
                break;
            }
            case DYNAMICFIELD_APP_IMAGE:{
                c.separatorInset = UIEdgeInsetsMake(0.f, screenWidth, 0.f, 0.f);
                break;
            }
            default:{
                switch(fieldType){
                    case DYNAMICFIELD_APP_DIVIDER:{
                        c.separatorInset = UIEdgeInsetsMake(0.f, screenWidth, 0.f, 0.f);
                        break;
                    }
                    case DYNAMICFIELD_APP_IMAGE:{
                        c.separatorInset = UIEdgeInsetsMake(0.f, screenWidth, 0.f, 0.f);
                        break;
                    }
                    default:{
                        c.separatorInset = UIEdgeInsetsMake(c.height, 14, 0, 0);
                        break;
                    }
                }
            }
        }
    }
    else {
        c.separatorInset = UIEdgeInsetsMake(0.f, screenWidth, 0.f, 0.f);
    }
    
    
    // fill cell with default information
    [self fillDyamicFieldCell:c forIndexPath:indexPath];
    
    
    // fill type specific information into cell
    [self fillDynamicFieldCell:c forFieldType:fieldType forIndexPath:indexPath];
    
    
    // return our cell
    return c;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(((DynamicFormTableViewCell*)cell).textInput != nil && [((DynamicFormTableViewCell*)cell).textInput isFirstResponder])
        [((DynamicFormTableViewCell*)cell).textInput resignFirstResponder];
    
    if(((DynamicFormTableViewCell*)cell).textView != nil && [((DynamicFormTableViewCell*)cell).textView isFirstResponder])
        [((DynamicFormTableViewCell*)cell).textView resignFirstResponder];
    
}

- (void)fillDyamicFieldCell:(DynamicFormTableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath{
    
    cell.delegate = self;
    
    // label
    if([super.ifbckModel.dynamicFields[indexPath.row][@"label"] isKindOfClass:[NSString class]] && ![[super.ifbckModel.dynamicFields[indexPath.row][@"label"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        cell.label.text = super.ifbckModel.dynamicFields[indexPath.row][@"label"];
    }
    else if([super.ifbckModel.dynamicFields[indexPath.row][@"description"] isKindOfClass:[NSString class]] && ![[super.ifbckModel.dynamicFields[indexPath.row][@"description"]  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        cell.label.text = super.ifbckModel.dynamicFields[indexPath.row][@"description"];
    }
    
    
    // check for required fields
    if(([super.ifbckModel.dynamicFields[indexPath.row][@"required"] intValue]==1 || [super.ifbckModel.dynamicFields[indexPath.row][@"cond_required"] intValue]==1) && showRequired){
        if(
           super.ifbckModel.dynamicFields[indexPath.row][@"value"] == nil ||
           ([super.ifbckModel.dynamicFields[indexPath.row][@"value"] isKindOfClass:[NSString class]] && [[super.ifbckModel.dynamicFields[indexPath.row][@"value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) ||
           ([super.ifbckModel.dynamicFields[indexPath.row][@"value"] intValue] == 0)
           ){
            cell.label.textColor = [UIColor redColor];
        }
        else {
            cell.label.textColor = [UIColor colorWithRGBHex:0x5A5A5F];
        }
    }
    
    // check for ask once fields
    else if([super.ifbckModel.dynamicFields[indexPath.row][@"required"] intValue]==2 && showAskOnce){
        if(
           super.ifbckModel.dynamicFields[indexPath.row][@"value"] == nil ||
           ([super.ifbckModel.dynamicFields[indexPath.row][@"value"] isKindOfClass:[NSString class]] && [[super.ifbckModel.dynamicFields[indexPath.row][@"value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) ||
           (super.ifbckModel.dynamicFields[indexPath.row][@"value"] == 0)
           ){
            cell.label.textColor = [UIColor redColor];
        }
        else {
            cell.label.textColor = [UIColor colorWithRGBHex:0x5A5A5F];
        }
    }
    
    // if its not required and not conditional required and not ask once
    else {
        cell.label.textColor = [UIColor colorWithRGBHex:0x5A5A5F];
    }
    
    
    // textView
    cell.textView.text = super.ifbckModel.dynamicFields[indexPath.row][@"placeholder"];
    cell.textView.tag = indexPath.row;
    cell.textView.delegate = self;
    if(super.ifbckModel.dynamicFields[indexPath.row][@"value"]!=nil && [super.ifbckModel.dynamicFields[indexPath.row][@"value"] isKindOfClass:[NSString class]])
        cell.textView.text = super.ifbckModel.dynamicFields[indexPath.row][@"value"];
    
    
    // textInput
    cell.textInput.placeholder = super.ifbckModel.dynamicFields[indexPath.row][@"placeholder"];
    cell.textInput.tag = indexPath.row;
    cell.textInput.delegate = self;
    if(super.ifbckModel.dynamicFields[indexPath.row][@"value"]!=nil && [super.ifbckModel.dynamicFields[indexPath.row][@"value"] isKindOfClass:[NSString class]])
        cell.textInput.text = super.ifbckModel.dynamicFields[indexPath.row][@"value"];
    
    
    // set the tag
    cell.checkboxSwitch.tag = indexPath.row;
}

- (void)fillDynamicFieldCell:(DynamicFormTableViewCell*)cell forFieldType:(int)fieldType forIndexPath:(NSIndexPath*)indexPath{
    if(![super.ifbckModel.dynamicFields[indexPath.row] isKindOfClass:[NSDictionary class]]){
        return;
    }
    switch(fieldType){
        case DYNAMICFIELD_EMAILINPUT:{
            cell.textInput.keyboardType = UIKeyboardTypeEmailAddress;
            break;
        }
        case DYNAMICFIELD_TEXT:{
            // get all options
            NSArray *options = [super.ifbckModel.dynamicFields[indexPath.row][@"options"] componentsSeparatedByString:@";"];

            // check all options
            for (NSString *option in options) {
                // if the date must be in the future
                if ([option isEqualToString:@"validate_number"]) {
                    cell.textInput.keyboardType = UIKeyboardTypeNumberPad;
                }
                else if([option isEqualToString:@"validate_phone"]){
                    cell.textInput.keyboardType = UIKeyboardTypePhonePad;
                }
                else {
                    cell.textInput.keyboardType = UIKeyboardTypeAlphabet;
                }
            }

            break;
        }
        case DYNAMICFIELD_DROPDOWN:{
            int selectedDropdownValue = [super.ifbckModel.dynamicFields[indexPath.row][@"value"] intValue];
            cell.dropdownSelectedValueLabel.text = ([super.ifbckModel.dynamicFields[indexPath.row][@""] count] > selectedDropdownValue)?super.ifbckModel.dynamicFields[indexPath.row][@"options"][[super.ifbckModel.dynamicFields[indexPath.row][@"value"] intValue]]:@"";

            break;
        }
        case DYNAMICFIELD_PHOTOUPLOAD:{
            cell.label.text = super.ifbckModel.dynamicFields[indexPath.row][@"description"];
            cell.dropdownSelectedValueLabel.text = super.ifbckModel.dynamicFields[indexPath.row][@"label"];

            if(super.ifbckModel.dynamicFields[indexPath.row][@"value"]!=nil && ![super.ifbckModel.dynamicFields[indexPath.row][@"value"] isEmptyString] && [super.ifbckModel.dynamicFields[indexPath.row][@"value"] isEqualToString:@"uploading"]){
                cell.activityIndicator.hidden = NO;
                [cell.activityIndicator startAnimating];
                //cell.uploadImageProgressView = _imageUploadProgressView;
                cell.imageUploadImage.hidden = YES;
                cell.imageViewWrapper.hidden = NO;
                cell.imageUploadDropdownLeftSpace.constant = 90;
                cell.imageUploadLabelLeftSpace.constant = 90;
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.indicatorIcon.hidden = NO;
            }
            else if(super.ifbckModel.dynamicFields[indexPath.row][@"value"]!=nil && ![super.ifbckModel.dynamicFields[indexPath.row][@"value"] isEmptyString]){
                cell.activityIndicator.hidden = YES;
                cell.imageUploadImage.hidden = NO;
                cell.imageUploadImage.layer.cornerRadius = cell.imageUploadImage.width/2.0f;
                cell.imageUploadImage.layer.borderWidth = 0.5f;
                cell.imageUploadImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
                cell.imageUploadImage.image = _image;
                cell.imageViewWrapper.hidden = NO;
                cell.imageUploadDropdownLeftSpace.constant = 90;
                cell.imageUploadLabelLeftSpace.constant = 90;
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                cell.indicatorIcon.hidden = YES;
            }
            else{
                cell.activityIndicator.hidden = YES;
                cell.imageUploadImage.hidden = YES;
                cell.imageViewWrapper.hidden = YES;
                cell.imageUploadDropdownLeftSpace.constant = 15;
                cell.imageUploadLabelLeftSpace.constant = 15;
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.indicatorIcon.hidden = NO;
            }

            break;
        }
        case DYNAMICFIELD_DATE:{
            BOOL onlyDate = [super.ifbckModel.dynamicFields[indexPath.row][@"options"] rangeOfString:@"show_time"].location == NSNotFound;
            BOOL onlyTime = [super.ifbckModel.dynamicFields[indexPath.row][@"options"] rangeOfString:@"only_time"].location != NSNotFound;
            BOOL onlyMonthAndYear = [super.ifbckModel.dynamicFields[indexPath.row][@"options"] rangeOfString:@"only_month_and_year"].location != NSNotFound;

            if(([super.ifbckModel.dynamicFields[indexPath.row][@"value"] floatValue]==0)){
                cell.dropdownSelectedValueLabel.text = super.ifbckModel.dynamicFields[indexPath.row][@"placeholder"];
            }
            else {
                cell.dropdownSelectedValueLabel.text = [miscFunctions localizedStringFromDate:[NSDate dateWithTimeIntervalSince1970:[super.ifbckModel.dynamicFields[indexPath.row][@"value"] intValue]] :onlyDate :onlyTime :onlyMonthAndYear];
            }
            
            break;
        }
        case DYNAMICFIELD_STARRATING:{
            float starRatingValue = [super.ifbckModel.dynamicFields[indexPath.row][@"value"] intValue];
            
            cell.label.text = super.ifbckModel.dynamicFields[indexPath.row][@"description"];
            cell.starRatingCommentDescription.text = super.ifbckModel.dynamicFields[indexPath.row][@"label"];
            cell.starRatingView.tag = indexPath.row;
            cell.starRatingView.maximumValue = 5;
            cell.starRatingView.minimumValue = 0;
            cell.starRatingView.value = starRatingValue;
            
            cell.starRatingView.emptyStarImage = _starRatingImageEmpty;
            cell.starRatingView.filledStarImage = _starRatingImageFilled;
            
            [cell.starRatingView addTarget:cell action:@selector(starRatingValueChanged:) forControlEvents:UIControlEventValueChanged];
            [cell.starRatingView addTarget:cell action:@selector(starRatingValueDidChange:) forControlEvents:UIControlEventTouchUpOutside];
            [cell.starRatingView addTarget:cell action:@selector(starRatingValueDidChange:) forControlEvents:UIControlEventTouchUpInside];
            
            //[cell.starRatingWrapper addSubview:starRatingView];
            
            if(super.ifbckModel.dynamicFields[indexPath.row][@"comment"] != nil &&
               [super.ifbckModel.dynamicFields[indexPath.row][@"comment"] isKindOfClass:[NSString class]] &&
               ![[super.ifbckModel.dynamicFields[indexPath.row][@"comment"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
                cell.textView.text = super.ifbckModel.dynamicFields[indexPath.row][@"comment"];
            }
            
            if([super.ifbckModel.dynamicFields[indexPath.row][@"has_textfield"] intValue]==1 || [super.ifbckModel.dynamicFields[indexPath.row][@"has_textfield"] intValue]==2){
                if(super.ifbckModel.dynamicFields[indexPath.row][@"value"]==nil || [super.ifbckModel.dynamicFields[indexPath.row][@"value"] intValue] < 1){
                    //[cell.starRatingCommentWrapper removeFromSuperview];
                    //[cell.textView removeFromSuperview];
                    //[cell.starRatingCommentDescription removeFromSuperview];
                }
                if(super.ifbckModel.dynamicFields[indexPath.row][@"comment"]==nil ||
                   (
                    [super.ifbckModel.dynamicFields[indexPath.row][@"comment"] isKindOfClass:[NSString class]] &&
                    [[super.ifbckModel.dynamicFields[indexPath.row][@"comment"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]
                    )
                   ){
                    
                    cell.textView.text = super.ifbckModel.dynamicFields[indexPath.row][@"placeholder"];
                    cell.textView.textColor = placeHolderColor;
                    
                    if(showRequired && [super.ifbckModel.dynamicFields[indexPath.row][@"has_textfield"] intValue]==2){
                        cell.starRatingCommentDescription.textColor = [UIColor redColor];
                    }
                }
            }
            else {
                //[cell.starRatingCommentWrapper removeFromSuperview];
                //[cell.textView removeFromSuperview];
                //[cell.starRatingCommentDescription removeFromSuperview];
            }
            
            cell.starValueDescriptions = [super.ifbckModel.dynamicFields[indexPath.row][@"options"] componentsSeparatedByString:@";"];
            
            cell.starRatingSelectedValueLabel.text = [cell getStarRatingValueDescription:starRatingValue];
            
            break;
        }
        case DYNAMICFIELD_APP_IMAGE:{
            if(![super.ifbckModel.dynamicFields[indexPath.row][@"options"] isEmptyString])
                cell.image.image = [self loadImageForItem:super.ifbckModel.dynamicFields[indexPath.row] atPath:super.ifbckModel.dynamicFields[indexPath.row][@"options"] width:320*screenMultiplicator];
            else {
                cell.image.image = [UIImage imageNamed:@"onboarding_header_1.jpg"];
                // else set the default header view
                _headerView.width = screenWidth;
                _headerView.height = [self tableView:_tableView heightForRowAtIndexPath:indexPath]+45;
                
                [cell addSubview:_headerView];
            }
            
        }
        case DYNAMICFIELD_CHECKBOX:{
            cell.checkboxSwitch.selected = [super.ifbckModel.dynamicFields[indexPath.row][@"value"] boolValue];
        }
        case DYNAMICFIELD_APP_DIVIDER:{
            // if we have a label and a description
            if(
               super.ifbckModel.dynamicFields[indexPath.row][@"label"] != nil &&
               [super.ifbckModel.dynamicFields[indexPath.row][@"label"] isKindOfClass:[NSString class]] &&
               ![super.ifbckModel.dynamicFields[indexPath.row][@"label"] isEqualToString:@""] &&
               super.ifbckModel.dynamicFields[indexPath.row][@"description"] != nil &&
               [super.ifbckModel.dynamicFields[indexPath.row][@"description"] isKindOfClass:[NSString class]] &&
               ![super.ifbckModel.dynamicFields[indexPath.row][@"description"] isEqualToString:@""]
               ){
                // set textlabel and textview
                cell.textView.text = super.ifbckModel.dynamicFields[indexPath.row][@"description"];
                cell.textView.userInteractionEnabled = NO;
            }
            // if we just have a label
            else if(
                    super.ifbckModel.dynamicFields[indexPath.row][@"label"]!=nil &&
                    [super.ifbckModel.dynamicFields[indexPath.row][@"label"] isKindOfClass:[NSString class]] &&
                    ![super.ifbckModel.dynamicFields[indexPath.row][@"label"] isEqualToString:@""]
                    ){
                // remove textView
                [cell.textView removeFromSuperview];
            }
            // if we just have a description
            else if(
                    super.ifbckModel.dynamicFields[indexPath.row][@"description"]!=nil &&
                    [super.ifbckModel.dynamicFields[indexPath.row][@"description"] isKindOfClass:[NSString class]] &&
                    ![super.ifbckModel.dynamicFields[indexPath.row][@"description"] isEqualToString:@""]
                    ){
                // set textView but remove textLabel
                cell.textView.text = super.ifbckModel.dynamicFields[indexPath.row][@"description"];
                cell.textView.userInteractionEnabled = NO;
                cell.dividerDescriptionTopSpace.constant = 0;
                [cell.label removeFromSuperview];
                //cell.label.text = @"";
            }
        }
        default:{
            
        }
    }
}


#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWasShown:)
//                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidChangeFrame:)
//                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyBoardDidChangeFrame:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight - (kbSize.height));
    NSLog(@"Keyboard Height: %f", kbSize.height);
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight - (kbSize.height));
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    //    NSDictionary* info = [aNotification userInfo];
    //
    //    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    //
    //    [UIView animateWithDuration:0.5 animations:^(void){
    //        self.navigationController.view.frame = CGRectMake(0, -(64+_headerView.height), [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) +_headerView.height+64);
    //    }];
    //
    //
    //    //self.headerView.height = 0;
    //    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.30
                          delay:0.0
                        options:(7 << 16) // This curve ignores durations
                     animations:^{
                         self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight - (kbSize.height));
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification{
    [UIView animateWithDuration:0.30
                          delay:0.0
                        options:(7 << 16) // This curve ignores durations
                     animations:^{
                         self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight);
                         [self.view layoutIfNeeded];
                         
                     }
                     completion:nil];
}


#pragma mark - Textfield and Textview delegate methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    
    // if the field where the textView belongs to is a star rating
    if([super.ifbckModel.dynamicFields[textView.tag][@"field_type"] intValue] == DYNAMICFIELD_STARRATING){
        // set the text as @"comment" so that we don't overwrite the value (star rating)
        super.ifbckModel.dynamicFields[textView.tag][@"comment"] = newString;
        return YES;
    }
    
    super.ifbckModel.dynamicFields[textView.tag][@"value"] = newString;
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    NSDictionary *fieldDict = @{};
    
    if(textView.tag<super.ifbckModel.dynamicFields.count){
        fieldDict = super.ifbckModel.dynamicFields[textView.tag];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:textView.tag inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
    
    if([textView.text isEqualToString:fieldDict[@"placeholder"]]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSDictionary *fieldDict = @{};
    
    if(textView.tag<super.ifbckModel.dynamicFields.count){
        fieldDict = super.ifbckModel.dynamicFields[textView.tag];
    }
    
    if([textView.text isEqualToString:@""]){
        textView.text = fieldDict[@"placeholder"];
        textView.textColor = placeHolderColor;
    }
    
    //if([super.ifbckModel.dynamicFields[textView.tag][@"required"] intValue]==1 && showRequired)
    //[self.tableView reloadRowsAtIndexPaths:_requiredFieldIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
    
    //if([super.ifbckModel.dynamicFields[textView.tag][@"required"] intValue]==2 && showRequired)
    //[self.tableView reloadRowsAtIndexPaths:_askOnceIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    super.ifbckModel.dynamicFields[textField.tag][@"value"] = newString;
    
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //    if([super.ifbckModel.dynamicFields[textField.tag][@"required"] intValue]==1 && showRequired)
    //        [self.tableView reloadRowsAtIndexPaths:_requiredFieldIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
    //
    //    if([super.ifbckModel.dynamicFields[textField.tag][@"required"] intValue]==2 && showRequired)
    //        [self.tableView reloadRowsAtIndexPaths:_askOnceIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return [textField isFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textDidChange:(id<UITextInput>)textInput{
    
}

- (void)selectionDidChange:(id<UITextInput>)textInput{
    
}

- (void)textWillChange:(id<UITextInput>)textInput{
    
}

- (void)selectionWillChange:(id<UITextInput>)textInput{
    
}


#pragma mark - pickerView delegate & datasource methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView==_dropdownPicker)return _dropdownArray.count;
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30.0f;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView==_dropdownPicker)return _dropdownArray[row];
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView==_dropdownPicker)_dropdownSelectedRow = (int)row;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


#pragma mark - general picker methods

- (void)hidePickerView{
    [UIView animateWithDuration:0.5f animations:^(void){
        _pickerView.frame = CGRectMake(0, self.view.height, self.view.width, 0);
    } completion:^(BOOL finished){
        [_pickerView removeFromSuperview];
    }];
}

- (void)pickerCancelButtonClicked{
    [self hidePickerView];
}

- (void)pickerRemoveButtonClicked{
    super.ifbckModel.dynamicFields[_pickerSelectedInputRow][@"value"] = @(0);
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_pickerSelectedInputRow inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [self hidePickerView];
}

#pragma mark - dropdown picker methods

- (void)showDropdownPickerWithOptionsArray:(NSArray*)optionsArray{
    // hide other picker immediately
    [_pickerView removeFromSuperview];
    
    _dropdownSelectedRow = 0;
    _dropdownArray = [optionsArray mutableCopy];
    
    // init dropdownView
    float dropdownViewHeight = 225+80;
    _pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height, self.view.width, dropdownViewHeight+40)];
    
    
    
    // add background first of all
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *pickerviewBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    pickerviewBackground.frame = CGRectMake(0, 0, _pickerView.width, _pickerView.height);
    
    [_pickerView addSubview:pickerviewBackground];
    
    
    
    // init picker for dropdown and add it to our view
    float dropdownHeight = 225-44+40;
    _dropdownPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, _pickerView.width, dropdownHeight)];
    
    _dropdownPicker.dataSource = self;
    _dropdownPicker.delegate = self;
    [_dropdownPicker reloadAllComponents];
    
    [_pickerView addSubview:_dropdownPicker];
    
    
    
    // add toolbar with flexible space and "Done"-Button
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,_dropdownPicker.width,44)];
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dropdownPickerDoneButtonClicked)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerCancelButtonClicked)];
    UIBarButtonItem *removeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(pickerRemoveButtonClicked)];

    toolBar.items = @[removeButton,cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    cancelButton.tintColor= [UIColor blackColor];
    removeButton.tintColor= [UIColor blackColor];

    UIView *bar = [[UIView alloc] init];
    bar.frame = CGRectMake(0,toolBar.height,toolBar.width,0.5);
    bar.backgroundColor = [UIColor lightGrayColor];

    [toolBar addSubview:bar];

    [_pickerView addSubview:toolBar];

    
    
    
    // add view to our controllers view and animate
    [self.view insertSubview:_pickerView belowSubview:_sendButtonWrapperBackground];
    [UIView animateWithDuration:0.5 animations:^(void){
        _pickerView.frame = CGRectMake(0, self.view.height-dropdownViewHeight-40, self.view.width, dropdownViewHeight+40);
    }];
}

- (void)dropdownPickerDoneButtonClicked{
    super.ifbckModel.dynamicFields[_pickerSelectedInputRow][@"value"] = @(_dropdownSelectedRow);
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_pickerSelectedInputRow inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [self hidePickerView];
}


#pragma mark - date picker methods

- (void)showDatePicker{
    // hide other picker immediately
    [_pickerView removeFromSuperview];
    
    // init dropdownView
    float dropdownViewHeight = 225+80;
    _pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height, self.view.width, dropdownViewHeight+40)];
    
    
    // add background first of all
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *pickerviewBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    pickerviewBackground.frame = CGRectMake(0, 0, _pickerView.width, _pickerView.height+40);
    
    [_pickerView addSubview:pickerviewBackground];
    
    
    // init picker for dropdown and add it to our view
    float dropdownHeight = 225-44+40;
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, _pickerView.width, dropdownHeight)];
    _datePicker.datePickerMode = UIDatePickerModeDate;

    if([super.ifbckModel.dynamicFields[_pickerSelectedInputRow][@"value"] intValue] != 0){
        _datePicker.date = [NSDate dateWithTimeIntervalSince1970:[super.ifbckModel.dynamicFields[_pickerSelectedInputRow][@"value"] intValue]];
    }
    else {
        _datePicker.date = [NSDate date];
    }

    // get all options
    NSArray *datePickerOptions = [super.ifbckModel.dynamicFields[_pickerSelectedInputRow][@"options"] componentsSeparatedByString:@";"];

    // check all options for this datepicker
    for (NSString *option in datePickerOptions) {
        // get the date picker mode
        if([option isEqualToString:@"show_time"]){
            _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
            continue;
        }
        else if([option isEqualToString:@"only_time"]){
            _datePicker.datePickerMode = UIDatePickerModeTime;
            continue;
        }
        else if([option isEqualToString:@"only_month_and_year"]){
            _datePicker.datePickerMode = UIDatePickerModeDate;
            [self hideDayColumns:_datePicker];
            continue;
        }

        if([option isEqualToString:@"must_be_now_or_future"]){
            _datePicker.minimumDate = [NSDate date];
            continue;
        }
        else if([option isEqualToString:@"must_be_now_or_past"]){
            _datePicker.maximumDate = [NSDate date];
            continue;
        }
        else if([option isEqualToString:@"must_be_future"]){
            _datePicker.minimumDate = [NSDate date];
            continue;
        }
        else if([option isEqualToString:@"must_be_past"]){
            _datePicker.maximumDate = [NSDate date];
            continue;
        }
    }
    
    [_pickerView addSubview:_datePicker];
    
    
    // add toolbar with flexible space and "Done"-Button
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,_datePicker.width,44)];
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDoneButtonClicked)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerCancelButtonClicked)];
    UIBarButtonItem *removeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(pickerRemoveButtonClicked)];

    toolBar.items = @[removeButton,cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    cancelButton.tintColor= [UIColor blackColor];
    removeButton.tintColor= [UIColor blackColor];

    UIView *bar = [[UIView alloc] init];
    bar.frame = CGRectMake(0,toolBar.height,toolBar.width,0.5);
    bar.backgroundColor = [UIColor lightGrayColor];

    [toolBar addSubview:bar];

    [_pickerView addSubview:toolBar];

    
    // add view to our controllers view and animate
    [self.view insertSubview:_pickerView belowSubview:_sendButtonWrapperBackground];
    [UIView animateWithDuration:0.5 animations:^(void){
        _pickerView.frame = CGRectMake(0, self.view.height-dropdownViewHeight-40, self.view.width, dropdownViewHeight+40);
    }];
}

- (void)hideDayColumns:(UIView*)view{
    
    for(UIView *v in view.subviews){
        if(![v isKindOfClass:NSClassFromString(@"UIPickerColumnView")]){
            [self hideDayColumns:v];
        }
        else {
            v.hidden = [self columnShouldBeHidden:v];
        }
    }
}

- (BOOL)columnShouldBeHidden:(UIView*)view{
    BOOL returnValue = NO;
    
    if([view isKindOfClass:[UILabel class]]){
        if([((UILabel*)view).text intValue] > 0 && [((UILabel*)view).text intValue] < 31)
            return YES;
    }
    else {
        for(UIView *v in view.subviews){
            returnValue = [self columnShouldBeHidden:v];
            if(returnValue)
                return YES;
        }
    }
    
    return returnValue;
}

- (void)datePickerDoneButtonClicked{
    NSMutableDictionary *selectedDatePickerField = super.ifbckModel.dynamicFields[_pickerSelectedInputRow];

    // get all options
    NSArray *datePickerOptions = [selectedDatePickerField[@"options"] componentsSeparatedByString:@";"];

    // check all options for this datepicker
    for (NSString *option in datePickerOptions){
        // if the date must be in the future
        if([option isEqualToString:@"must_be_future"]){
            if(_datePicker.date.timeIntervalSince1970 <= [NSDate date].timeIntervalSince1970){
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:NSLocalizedStringFromTable(@"Selected date / time must be in the future.", @"ifbckLocalizable", @"Selected date / time must be in the future.")
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"OK")
                                  otherButtonTitles:nil] show];
                return;
            }
        }

        // if the date must be in the future
        if([option isEqualToString:@"must_be_now_or_future"]){
            if(_datePicker.date.timeIntervalSince1970 < [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateBySettingHour:00 minute:0 second:0 ofDate:[NSDate date] options:0].timeIntervalSince1970){
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:NSLocalizedStringFromTable(@"Selected date / time should not be in the past.", @"ifbckLocalizable", @"Selected date / time must be in the future.")
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"OK")
                                  otherButtonTitles:nil] show];
                return;
            }
        }

        // if the date must be in the past
        if([option isEqualToString:@"must_be_past"]){
            if(_datePicker.date.timeIntervalSince1970 >= [NSDate date].timeIntervalSince1970){
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:NSLocalizedStringFromTable(@"Selected date / time must be in the past.", @"ifbckLocalizable", @"Selected date / time must be in the past.")
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"OK")
                                  otherButtonTitles:nil] show];
                return;
            }
        }

        // if the date must be in the past
        if([option isEqualToString:@"must_be_now_or_past"]){
            if(_datePicker.date.timeIntervalSince1970 > [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateBySettingHour:23 minute:59 second:59 ofDate:[NSDate date] options:0].timeIntervalSince1970){
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:NSLocalizedStringFromTable(@"Selected date / time should not be in the future.", @"ifbckLocalizable", @"Selected date / time must be in the past.")
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"OK")
                                  otherButtonTitles:nil] show];
                return;
            }
        }

        // if this date must be higher than another date
        if([option isEqualToString:@"must_be_higher_than"]){
            if(selectedDatePickerField[@"conditionalreqfields"] !=nil && [selectedDatePickerField[@"conditionalreqfields"] isKindOfClass:[NSString class]] && ![selectedDatePickerField[@"conditionalreqfields"] isEqualToString:@""]){
                NSArray *conditionalReqFields = [selectedDatePickerField[@"conditionalreqfields"] componentsSeparatedByString:@","];
                for(NSString *condRectField in conditionalReqFields){
                    for(NSMutableDictionary *field in super.ifbckModel.dynamicFields){
                        if(field[@"uid"] == condRectField){
                            if(floor(_datePicker.date.timeIntervalSince1970 / 360.0f) * 360.0f <= floor([field[@"value"] intValue]/ 360.0f) * 360.0f && [field[@"value"] intValue] != 0){
                                [[[UIAlertView alloc] initWithTitle:nil
                                                            message:[NSString stringWithFormat:NSLocalizedStringFromTable(@"Selected date / time must be higher than the date / time of the field \"%@\".", @"ifbckLocalizable", @"selected date must be higher"),field[@"label"]]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"OK")
                                                  otherButtonTitles:nil] show];
                                return;
                            }
                        }
                    }
                }
            }
        }

        // if this date must be lower than another date
        if([option isEqualToString:@"must_be_lower_than"]){
            if(selectedDatePickerField[@"conditionalreqfields"] !=nil && [selectedDatePickerField[@"conditionalreqfields"] isKindOfClass:[NSString class]] && ![selectedDatePickerField[@"conditionalreqfields"] isEqualToString:@""]){
                NSArray *conditionalReqFields = [selectedDatePickerField[@"conditionalreqfields"] componentsSeparatedByString:@","];
                for(NSString *condRectField in conditionalReqFields){
                    for(NSMutableDictionary *field in super.ifbckModel.dynamicFields){
                        if(field[@"uid"] == condRectField){
                            if(floor(_datePicker.date.timeIntervalSince1970 / 360.0f) * 360.0f >= floor([field[@"value"] intValue]/ 360.0f) * 360.0f && [field[@"value"] intValue] != 0){
                                [[[UIAlertView alloc] initWithTitle:nil
                                                            message:[NSString stringWithFormat:NSLocalizedStringFromTable(@"Selected date / time must be lower than the date / time of the field \"%@\".", @"ifbckLocalizable", @"selected date must be higher"),field[@"label"]]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"OK")
                                                  otherButtonTitles:nil] show];
                                return;
                            }
                        }
                    }
                }
            }
        }

    }

    super.ifbckModel.dynamicFields[_pickerSelectedInputRow][@"value"] = @(_datePicker.date.timeIntervalSince1970);
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_pickerSelectedInputRow inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [self hidePickerView];
}


#pragma mark - starrating delegate methods

- (void)sliderValueChanged:(UISlider *)slider{
    
}


#pragma mark - sending process

- (IBAction)sendFeedbackButtonPressed:(id)sender {
    
    if(_sendingFeedback){
        return;
    }
    
    [self.view endEditing:YES];
    self.editing = NO;
    
    // check for conditionally required fields
    if([self conditionalRequiredInputsMissingInformation]){
        showRequired = YES;
        [self.tableView reloadData];
        [self showRequiredFieldsMissingPopup];
        return;
    }
    
    // check for required fields
    if([self requiredInputsMissingInformation]){
        showRequired = YES;
        [self.tableView reloadData];
        [self showRequiredFieldsMissingPopup];
        return;
    }
    
    // check for ask once fields
    if([self showNextAskOncePopup]){
        showAskOnce = YES;
        [self.tableView reloadData];
        return;
    }
    
    // check if the user has left everything empty
    if([self nothingFilledOut]){
        [self showNothingFilledOutPopup];
        return;
    }
    
    _sendingFeedback = YES;
    
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.mode = MBProgressHUDModeIndeterminate;
    //    hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable", @"");
    //
    showRequired = NO;
    showAskOnce = NO;
    
    [self.tableView reloadData];

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //send in background
        [super.ifbckModel sendFeedback];
        super.ifbckModel.languageSelected = NO;
    });
    
    _sendingFeedback = NO;
    _feedbackSent = YES;
    
    _successAlertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedStringFromTable(@"Thank you for your message.", @"ifbckLocalizable", @"") delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"") otherButtonTitles:nil];
    [_successAlertView show];
    
    [self performSelector:@selector(hideAlertView:) withObject:_successAlertView afterDelay:10.0];
}

- (void)hideAlertView:(UIAlertView *)alertView{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)showRequiredFieldsMissingPopup{
    [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedStringFromTable(@"Please fill out all required fields.", @"ifbckLocalizable", @"") delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"") otherButtonTitles:nil] show];
}

- (BOOL)conditionalRequiredInputsMissingInformation{
    if(_numOfConditionalRequiredFields==0)return NO;
    
    
    NSMutableArray *requiredFieldIDs = [@[] mutableCopy];
    NSMutableArray *notRequiredFieldIDs = [@[] mutableCopy];
    
    for(NSMutableDictionary *fieldDict in super.ifbckModel.dynamicFields){
        id value = fieldDict[@"value"];
        
        if(
           ([value isKindOfClass:[NSString class]] && ![[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) ||
           ([value intValue] > 0)
           ){
            if(
               fieldDict[@"conditionalreqfields"] !=nil &&
               [fieldDict[@"conditionalreqfields"] isKindOfClass:[NSString class]] &&
               ![fieldDict[@"conditionalreqfields"] isEqualToString:@""]
               ){
                NSArray *conditionalReqFields = [fieldDict[@"conditionalreqfields"] componentsSeparatedByString:@","];
                
                [requiredFieldIDs addObjectsFromArray:conditionalReqFields];
                
                NSLog(@"\nfielddict: %@\nhas value: %@\nand has conditionalrequired fields: %@", fieldDict[@"label"], value,conditionalReqFields);
            }
        }
        else if(
                fieldDict[@"conditionalreqfields"] !=nil &&
                [fieldDict[@"conditionalreqfields"] isKindOfClass:[NSString class]] &&
                ![fieldDict[@"conditionalreqfields"] isEqualToString:@""]
                ){
            NSArray *conditionalReqFields = [fieldDict[@"conditionalreqfields"] componentsSeparatedByString:@","];
            
            [notRequiredFieldIDs addObjectsFromArray:conditionalReqFields];
        }
        
    }
    
    if(requiredFieldIDs.count>0 || notRequiredFieldIDs.count>0){
        for(NSMutableDictionary *fieldDict in super.ifbckModel.dynamicFields){
            if([requiredFieldIDs containsObject:fieldDict[@"uid"]]){
                fieldDict[@"cond_required"]=@1;
            }
            if([notRequiredFieldIDs containsObject:fieldDict[@"uid"]]){
                fieldDict[@"cond_required"]=@0;
            }
        }
        return [self requiredInputsMissingInformation];
    }
    
    return NO;
}

- (BOOL)requiredInputsMissingInformation{
    if(_numOfRequiredFields==0)return NO;
    
    for(NSMutableDictionary* fieldDict in super.ifbckModel.dynamicFields){
        
        if(fieldDict!=nil && ([fieldDict[@"required"] intValue]==1 || [fieldDict[@"cond_required"] intValue]==1)){
            NSLog(@"required field: %@", fieldDict);
            if(fieldDict[@"value"] == nil){
                return YES;
            }
            else if([fieldDict[@"value"] isKindOfClass:[NSString class]]){
                if([[fieldDict[@"value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
                    return YES;
                }
                else if([[fieldDict[@"value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"0"]){
                    return YES;
                }
            }
            else if([fieldDict[@"value"] intValue] == 0){
                return YES;
            }
            else if([fieldDict[@"value"] intValue] < 1){
                return YES;
            }
            
            if([fieldDict[@"field_type"] intValue]==DYNAMICFIELD_STARRATING){
                if([fieldDict[@"has_textfield"] intValue]==2){
                    if(fieldDict[@"comment"]==nil ||
                       (
                        [fieldDict[@"comment"] isKindOfClass:[NSString class]] &&
                        [[fieldDict[@"comment"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]
                        )
                       ){
                        return YES;
                    }
                }
            }
            
        }
    }
    
    return NO;
}

- (BOOL)showNextAskOncePopup{
    if(_numOfAskOnceFields==0)return NO;
    
    for(NSMutableDictionary* fieldDict in super.ifbckModel.dynamicFields){
        if(fieldDict!=nil && [fieldDict[@"required"] intValue]==2){
            id value = fieldDict[@"value"];
            
            if(![fieldDict[@"was_shown_once"] boolValue] &&
               (([value isKindOfClass:[NSString class]] && [[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) || value == 0)){
                
                fieldDict[@"was_shown_once"] = @YES;
                
                [[[UIAlertView alloc] initWithTitle:nil message:fieldDict[@"ask_once"] delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK",@"ifbckLocalizable", @"") otherButtonTitles:nil] show];
                
                return YES;
            }
            
        }
    }
    
    return NO;
}

- (BOOL)nothingFilledOut{
    for(NSMutableDictionary* fieldDict in super.ifbckModel.dynamicFields){
        if(fieldDict[@"value"] != nil &&
           !([fieldDict[@"value"] isKindOfClass:[NSString class]] &&
             [[fieldDict[@"value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] &&
             [[fieldDict[@"value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"0"]) &&
           fieldDict[@"value"] > 0){
            return NO;
        }
    }
    return YES;
}

- (void)showNothingFilledOutPopup{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedStringFromTable(@"Please fill out at least one field.", @"ifbckLocalizable", @"") delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"") otherButtonTitles:nil];
    
    [alert show];
    
    [self performSelector:@selector(hideAlertView:) withObject:alert afterDelay:10.0];
}


#pragma mark - cell delegate methods

- (void)starRatingValueChanged:(HCSStarRatingView*)starRatingView{
    //NSMutableDictionary *fieldDict = super.ifbckModel.dynamicFields[starRatingView.tag];
    //float newValue = starRatingView.value;
    //NSUInteger unsignedValue = (NSUInteger)newValue;
    
    //fieldDict[@"value"] = [NSNumber numberWithInteger:unsignedValue];
}

- (void)starRatingValueDidChange:(HCSStarRatingView *)starRatingView{
    long int row = starRatingView.tag;
    
    NSMutableDictionary *fieldDict = super.ifbckModel.dynamicFields[row];
    
    float newValue = starRatingView.value;
    int unsignedValue = (int)fabsf(newValue);
    
    if(unsignedValue < 1 && [fieldDict[@"refreshed"] isEqual:@YES]){
        fieldDict[@"value"] = @0;
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
        fieldDict[@"refreshed"] = @NO;
        
        return;
    }
    else if(unsignedValue > 0 && ![fieldDict[@"refreshed"] isEqual:@YES]){
        fieldDict[@"value"] = @(unsignedValue);
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
        fieldDict[@"refreshed"] = @YES;
        
        return;
    }
    else {
        fieldDict[@"value"] = @(unsignedValue);
    }
    //    fieldDict[@"value"] = [NSNumber numberWithInteger:unsignedValue];
    //
    //    if([fieldDict[@"value"] intValue] < 1 && ![fieldDict[@"refreshed"] isEqual:@YES]){
    //        return;
    //    }
    //
    //
    //    if(![fieldDict[@"refreshed"] isEqual:@YES] || fieldDict[@"value"] < [NSNumber numberWithInt:1]){
    //        if([fieldDict[@"has_textfield"] intValue]==1 || [fieldDict[@"has_textfield"] intValue]==2){
    //            fieldDict[@"refreshed"] = @YES;
    //
    //            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    //        }
    //
    //        if(starRatingView.value==0){
    //            fieldDict[@"refreshed"] = @NO;
    //        }
    //    }
}

- (void)switchSwitched:(UISwitch *)checkboxSwitch{
    super.ifbckModel.dynamicFields[checkboxSwitch.tag][@"value"] = @(checkboxSwitch.isOn);
}


#pragma mark - alert view delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(_feedbackSent){
        _feedbackSent = NO;
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable", @"");
        
        [super.ifbckModel loadClient];
    }
}


#pragma mark - method for loading images for DYNAMICFIELD_APP_IMAGE fields

- (UIImage*)loadImageForItem:(NSDictionary*)item atPath:(NSString*)path width:(float)width{
    
    NSString *fileName = path.lastPathComponent.stringByDeletingPathExtension;
    
    imageMethods *images = [[imageMethods alloc] init];
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@-%@",fileName, item[@"tstamp"],item[@"uid"]];
    
    UIImage *cellImage = nil;
    NSString *structureImageSm = path;
    if(![structureImageSm isEqualToString:@""] && structureImageSm != nil){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            //NSLog(@"image reused: %@", imageNameFromTstamp);
            return cellImage;
        }
        else {
            //Get Image From URL
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm,width*2];
            
            UIImage *imageFromURL = [images getImageFromURL:urlString];
            
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //NSLog(@"new image saved: %@", imageNameFromTstamp);
            return cellImage;
        }
        
    }
    
    return cellImage;
}

- (IBAction)cornerButtonClicked:(UIButton *)sender {
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"appInTestingMode"])
        return;
    for(UIButton *cornerButton in _cornerButtons){
        if(!cornerButton.isTouchInside){
            return;
        }
    }
    
    NSLog(@" dfvc - opening secret features");
}


#pragma mark - imagePicker delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    _image = selectedImage;
    NSLog(@"image info: %@",info);

    super.ifbckModel.dynamicFields[_selectedIndexPath.row][@"value"] = @"uploading";

    [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];

    imageMethods *im = [imageMethods new];
    im.uploadDelegate = self;
    [im uploadImage:selectedImage withParameters:@{@"uploadType":@"dynamicFieldUpload"}];

    [self.tableView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - uiaction sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(actionSheet==self.imageSelectionActionSheet){
        if(buttonIndex==2){
            super.ifbckModel.dynamicFields[_selectedIndexPath.row][@"value"] = @"";

            [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];

            return;
        }
        else if(buttonIndex==0 || buttonIndex==1){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.navigationBar.tintColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
            picker.navigationBar.barTintColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];

            [picker.navigationBar setTitleTextAttributes: @{
                    NSForegroundColorAttributeName: picker.navigationBar.tintColor,
                    NSFontAttributeName: [UIFont fontWithName:@"Arial" size:17.0f]
            }];

            picker.delegate = self;
            picker.allowsEditing = YES;
            if(buttonIndex==0){
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                }
            }
            if(buttonIndex==1){
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }

            UIView *navigationBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
            navigationBackgroundView.backgroundColor = [UIColor whiteColor];

            [picker.navigationController.view addSubview:navigationBackgroundView];

            [self presentViewController:picker animated:YES completion:nil];
        }
        return;
    }
}

- (void)uploadingWithProgress:(float)progress {
    imageUploadProgress = progress;
}

- (void)uploadFinishedWithResponse:(NSDictionary *)response {
    super.ifbckModel.dynamicFields[_selectedIndexPath.row][@"value"] = [NSString stringWithFormat:@"%@%@",response[@"filePath"],response[@"fileName"]];
    [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
}



#pragma mark - trash

- (void)trashfillDyamicFieldCell:(DynamicFormTableViewCell*)cell fromDictionary:(NSDictionary*)dynamicFieldDict forIndexPath:(NSIndexPath*)indexPath{

    cell.delegate = self;

    // label
    if([dynamicFieldDict[@"label"] isKindOfClass:[NSString class]] && ![[dynamicFieldDict[@"label"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        cell.label.text = dynamicFieldDict[@"label"];
    }
    else if([dynamicFieldDict[@"description"] isKindOfClass:[NSString class]] && ![[dynamicFieldDict[@"description"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        cell.label.text = dynamicFieldDict[@"description"];
    }

    if([dynamicFieldDict[@"required"] intValue]==1 && showRequired){
        id value = super.ifbckModel.dynamicFields[indexPath.row][@"value"];

        if(
                ([value isKindOfClass:[NSString class]] && [[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) ||
                        (value == 0)
                ){
            cell.label.textColor = [UIColor redColor];
        }
        else {
            cell.label.textColor = [UIColor blackColor];
        }
    }

    if([dynamicFieldDict[@"required"] intValue]==2 && showAskOnce){
        id value = super.ifbckModel.dynamicFields[indexPath.row][@"value"];

        if(
                ([value isKindOfClass:[NSString class]] && [[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) ||
                        (value == 0)
                ){
            cell.label.textColor = [UIColor redColor];
        }
        else {
            cell.label.textColor = [UIColor blackColor];
        }
    }


    // textView
    cell.textView.text = dynamicFieldDict[@"placeholder"];
    cell.textView.tag = indexPath.row;
    cell.textView.delegate = self;
    if(dynamicFieldDict[@"value"]!=nil && [dynamicFieldDict[@"value"] isKindOfClass:[NSString class]])
        cell.textView.text = dynamicFieldDict[@"value"];


    // textInput
    cell.textInput.placeholder = dynamicFieldDict[@"placeholder"];
    cell.textInput.tag = indexPath.row;
    cell.textInput.delegate = self;
    if(dynamicFieldDict[@"value"]!=nil && [dynamicFieldDict[@"value"] isKindOfClass:[NSString class]])
        cell.textInput.text = dynamicFieldDict[@"value"];


    //
    cell.checkboxSwitch.tag = indexPath.row;
}

- (void)trashfillDynamicFieldCell:(DynamicFormTableViewCell*)cell fromDictionary:(NSDictionary*)dynamicFieldDict forFieldType:(int)fieldType forIndexPath:(NSIndexPath*)indexPath{
    if(![dynamicFieldDict isKindOfClass:[NSDictionary class]]){
        return;
    }
    switch(fieldType){
        case DYNAMICFIELD_DROPDOWN:{
            int selectedDropdownValue = [dynamicFieldDict[@"value"] intValue];
            cell.dropdownSelectedValueLabel.text = ([dynamicFieldDict[@"options"] count] > selectedDropdownValue)?dynamicFieldDict[@"options"][[dynamicFieldDict[@"value"] intValue]]:@"";
            break;
        }
        case DYNAMICFIELD_DATE:{
            BOOL onlyDate = [dynamicFieldDict[@"options"] rangeOfString:@"show_time"].location == NSNotFound;
            BOOL onlyTime = ([dynamicFieldDict[@"options"] rangeOfString:@"only_time"].location != NSNotFound);
            NSDate *date = (dynamicFieldDict[@"value"]==0)?[NSDate date]:[NSDate dateWithTimeIntervalSince1970:[dynamicFieldDict[@"value"] intValue]];
            cell.dropdownSelectedValueLabel.text = [miscFunctions localizedStringFromDate:date onlyDate:onlyDate onlyTime:onlyTime];
            break;
        }
        case DYNAMICFIELD_STARRATING:{
            float starRatingValue = [dynamicFieldDict[@"value"] intValue];

            cell.label.text = dynamicFieldDict[@"description"];
            cell.starRatingCommentDescription.text = dynamicFieldDict[@"label"];
            HCSStarRatingView *starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0, 0, cell.starRatingWrapper.width, cell.starRatingWrapper.height)];
            starRatingView.tag = indexPath.row;
            starRatingView.maximumValue = 5;
            starRatingView.minimumValue = 0;
            starRatingView.backgroundColor = placeHolderColor;
            starRatingView.value = starRatingValue;
            //            starRatingView.tintColor = [UIColor colorWithRGBHex:0xCF9432];
            starRatingView.tintColor = [UIColor colorWithRGBHex:0xF5BB00];
            [starRatingView addTarget:cell action:@selector(starRatingValueChanged:) forControlEvents:UIControlEventValueChanged];
            [starRatingView addTarget:cell action:@selector(starRatingValueDidChange:) forControlEvents:UIControlEventTouchUpOutside];
            [starRatingView addTarget:cell action:@selector(starRatingValueDidChange:) forControlEvents:UIControlEventTouchUpInside];

            [cell.starRatingWrapper addSubview:starRatingView];

            if(dynamicFieldDict[@"comment"] != nil &&
                    [dynamicFieldDict[@"comment"] isKindOfClass:[NSString class]] &&
                    ![[dynamicFieldDict[@"comment"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
                cell.textView.text = dynamicFieldDict[@"comment"];
            }

            if([dynamicFieldDict[@"has_textfield"] intValue]==1 || [dynamicFieldDict[@"has_textfield"] intValue]==2){
                if(dynamicFieldDict[@"value"]==nil || [dynamicFieldDict[@"value"] intValue] < 1){
                    //[cell.starRatingCommentWrapper removeFromSuperview];
                    //[cell.textView removeFromSuperview];
                    //[cell.starRatingCommentDescription removeFromSuperview];
                }
                if(dynamicFieldDict[@"comment"]==nil ||
                        (
                                [dynamicFieldDict[@"comment"] isKindOfClass:[NSString class]] &&
                                        [[dynamicFieldDict[@"comment"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]
                        )
                        ){

                    cell.textView.text = dynamicFieldDict[@"placeholder"];
                    cell.textView.textColor = placeHolderColor;

                    if(showRequired && [dynamicFieldDict[@"has_textfield"] intValue]==2){
                        cell.starRatingCommentDescription.textColor = [UIColor redColor];
                    }
                }
            }
            else {
                //[cell.starRatingCommentWrapper removeFromSuperview];
                //[cell.textView removeFromSuperview];
                //[cell.starRatingCommentDescription removeFromSuperview];
            }

            cell.starValueDescriptions = [dynamicFieldDict[@"options"] componentsSeparatedByString:@";"];

            cell.starRatingSelectedValueLabel.text = [cell getStarRatingValueDescription:starRatingValue];

            break;
        }
        case DYNAMICFIELD_APP_IMAGE:{
            cell.image.image = [self loadImageForItem:dynamicFieldDict atPath:dynamicFieldDict[@"options"] width:320*screenMultiplicator];
        }
        case DYNAMICFIELD_CHECKBOX:{
            cell.checkboxSwitch.selected = [dynamicFieldDict[@"value"] boolValue];
        }
        case DYNAMICFIELD_APP_DIVIDER:{
            // if we have a label and a description
            if(
                    dynamicFieldDict[@"label"] != nil &&
                            [dynamicFieldDict[@"label"] isKindOfClass:[NSString class]] &&
                            ![dynamicFieldDict[@"label"] isEqualToString:@""] &&
                            dynamicFieldDict[@"description"] != nil &&
                            [dynamicFieldDict[@"description"] isKindOfClass:[NSString class]] &&
                            ![dynamicFieldDict[@"description"] isEqualToString:@""]
                    ){
                // set textlabel and textview
                cell.textView.text = dynamicFieldDict[@"description"];
                cell.textView.userInteractionEnabled = NO;
            }
                // if we just have a label
            else if(
                    dynamicFieldDict[@"label"]!=nil &&
                            [dynamicFieldDict[@"label"] isKindOfClass:[NSString class]] &&
                            ![dynamicFieldDict[@"label"] isEqualToString:@""]
                    ){
                // remove textView
                [cell.textView removeFromSuperview];
            }
                // if we just have a description
            else if(
                    dynamicFieldDict[@"description"]!=nil &&
                            [dynamicFieldDict[@"description"] isKindOfClass:[NSString class]] &&
                            ![dynamicFieldDict[@"description"] isEqualToString:@""]
                    ){
                // set textView but remove textLabel
                cell.textView.text = dynamicFieldDict[@"description"];
                cell.textView.userInteractionEnabled = NO;
                cell.dividerDescriptionTopSpace.constant = 0;
                [cell.label removeFromSuperview];
                //cell.label.text = @"";
            }
        }
    }
}
@end