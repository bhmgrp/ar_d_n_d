//
//  DynamicFormViewController.h
//  if_framework
//
//  Created by User on 31.08.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackViewController.h"

#import "DynamicFormTableViewCell.h"


@interface DynamicFormViewController : iFeedbackViewController <
        UITableViewDelegate,
        UITableViewDataSource,
        UITextViewDelegate,
        UITextInputDelegate,
        UITextFieldDelegate,
        DynamicFormTableViewCellDelegate,
        UIPickerViewDelegate,
        UIPickerViewDataSource,
        UIAlertViewDelegate,
        UIActionSheetDelegate,
        UIImagePickerControllerDelegate,
        UINavigationControllerDelegate,
        ImageUploadDelegate
>

@property (nonatomic) NSUInteger pickerSelectedInputRow;

@property (nonatomic) UIView *pickerView;

@property (nonatomic) UIDatePicker *datePicker;

@property (nonatomic) UIPickerView *dropdownPicker;
@property (nonatomic) NSMutableArray *dropdownArray;
@property (nonatomic) int dropdownSelectedRow;


@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (weak, nonatomic) IBOutlet UIImageView *languageSelectorImageFlag;
@property (weak, nonatomic) IBOutlet UIButton *languageSelectorButton;

-(void)loadTable;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *sendButtonDefaultBackground;
@property (weak, nonatomic) IBOutlet UIView *sendButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *sendButtonWrapper;
@property (weak, nonatomic) IBOutlet UIView *sendButtonWrapperBackground;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonWrapperBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonWrapperHeight;


- (IBAction)sendFeedbackButtonPressed:(id)sender;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cornerButtons;
@property (strong, nonatomic) IBOutlet UIProgressView *imageUploadProgressView;
- (IBAction)cornerButtonClicked:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *headerView;

@end