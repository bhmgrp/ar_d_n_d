//
//  DynamicFormTableViewCell.h
//  if_framework
//
//  Created by User on 31.08.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HCSStarRatingView.h"

#import "iFeedbackTableViewCell.h"

@protocol DynamicFormTableViewCellDelegate <NSObject>
-(void)sliderValueChanged:(UISlider*)slider;
-(void)starRatingValueChanged:(HCSStarRatingView*)starRatingView;
-(void)starRatingValueDidChange:(HCSStarRatingView*)starRatingView;
-(void)switchSwitched:(UISwitch*)checkboxSwitch;
@end;

@interface DynamicFormTableViewCell : iFeedbackTableViewCell

@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (nonatomic) id<DynamicFormTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UITextField *textInput;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UIView *starRatingWrapper;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@property (weak, nonatomic) IBOutlet UILabel *starRatingSelectedValueLabel;

@property (weak, nonatomic) IBOutlet UISwitch *checkboxSwitch;

@property (weak, nonatomic) IBOutlet UILabel *starRatingCommentDescription;
@property (weak, nonatomic) IBOutlet UIView *starRatingCommentWrapper;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dividerDescriptionTopSpace;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *imageViewWrapper;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageUploadDropdownLeftSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageUploadLabelLeftSpace;
@property (weak, nonatomic) IBOutlet UIImageView *imageUploadImage;

@property (weak, nonatomic) IBOutlet UIImageView *indicatorIcon;

- (IBAction)switchSwitched:(id)sender;

- (IBAction)sliderValueChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *dropdownSelectedValueLabel;

@property (nonatomic) NSArray *starValueDescriptions;

-(NSString*)getStarRatingValueDescription:(int)value;

@end