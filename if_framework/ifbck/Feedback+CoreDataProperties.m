//
//  Feedback+CoreDataProperties.m
//  native ifbck app
//
//  Created by User on 6/1/16.
//  Copyright © 2016 BHM Media Solutions. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Feedback+CoreDataProperties.h"

@implementation Feedback (CoreDataProperties)

@dynamic campaignID;
@dynamic dynamicFieldsArray;
@dynamic entryID;
@dynamic entryPointType;
@dynamic languageID;
@dynamic placeID;
@dynamic sent;
@dynamic topicRatings;
@dynamic tstamp;

@end
