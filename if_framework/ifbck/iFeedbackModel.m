//
//  CampaignModel.m
//  if_framework
//
//  Created by User on 12/15/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackModel.h"

#import "cronjob.h"

#import "Feedback.h"

#import "ifbckNavigationViewController.h"

#import "iFeedbackSidebarViewController.h"
#import "ifbckWebViewController.h"

#import "LanguageSelectViewController.h"
#import "PlaceSelectionViewController.h"
#import "CampaignSelectionViewController.h"
#import "TopicListViewController.h"
#import "DynamicFormViewController.h"

#define clientPath @"clients"

@interface iFeedbackModel (){
    
}

@property (nonatomic) int loadingType;

enum {
    LOADINGTYPE_CAMPAIGN = 0,
    LOADINGTYPE_CLIENT = 1,
    LOADINGTYPE_CLIENTUPDATECHECK = 3,
    LOADINGTYPE_CLIENTUPDATE = 4
};

@property (nonatomic) NSMutableData *downloadedData;

@property (nonatomic) NSDictionary *translations;

@property (nonatomic) NSString *sSYSTEM_DOMAIN, *sSECURED_API_URL, *sAPI_URL;

#pragma mark - Core Data stack

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSURL *applicationDocumentsDirectory;

@end

@implementation iFeedbackModel

-(instancetype)init{
    self = [super init];
    
    [ifbckGlobals sharedInstance];
    
    //globaliFeedbackModel = self;
    globalCronjob = [cronjob initCrojob];
    globalCronjob.ifbckModel = self;
    
    _sSYSTEM_DOMAIN = SYSTEM_DOMAIN;
    _sSECURED_API_URL = SECURED_API_URL;
    _sAPI_URL = API_URL;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"appInTestingMode"]){
        _sSECURED_API_URL = @"http://dev.ifbck.com/scripts/api/";
        _sAPI_URL = @"http://dev.ifbck.com/scripts/api/";
        _sSYSTEM_DOMAIN = @"http://dev.ifbck.com";
    }
        
    NSLog(@" ifm - server used: %@", _sSYSTEM_DOMAIN);
    
    _titleString = @"iFeedback®";
    
    _entryPoint = @"";
    _campaignID = 0;
    _dynamicFields = [@[] mutableCopy];
    
    _entryURL = [NSString stringWithFormat:@"%@/%@", _sSYSTEM_DOMAIN, _entryPoint];
    
    _translations = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"translations" ofType:@"plist"]];
    
    // check for app timeouts
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(idleTimeout:) name:kApplicationIdleTimeoutNotification object:nil];
    
    NSLog(@" ifm - documents: %@", NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0]);
    
    return self;
}

- (void)idleTimeout:(NSNotification*) notification{
    NSLog(@" ifm - idle timeout");
    
    // sorting dynamic field values
    NSInteger fieldUID;
    id value;
    NSString *starRatingComment;
    
    NSMutableArray *sortedDynamicFields = [@[] mutableCopy];
    
    for(NSDictionary *dynamicFieldDict in _dynamicFields){
        fieldUID = [dynamicFieldDict[@"uid"] integerValue];
        value = dynamicFieldDict[@"value"];
        starRatingComment = dynamicFieldDict[@"comment"];
        if(fieldUID> 0 && value!=nil){
            [sortedDynamicFields addObject:@{
                                             @"uid":@(fieldUID),
                                             @"value":value,
                                             @"comment":(starRatingComment!=nil)?starRatingComment:@"",
                                             }];
        }
    }
    
    
    if(self.topicRatings.count>0 || sortedDynamicFields.count>0){
        NSLog(@" ifm - sending feedbacks");
        [self sendFeedback];
        [self loadClient];
    }
    else if(_startViewController.navigationController.topViewController != _startViewController){
        NSLog(@" ifm - just going back to start view");
        [self loadClient];
    }
}

-(NSString*)storageFileName{
    return [self storageFileNameForLanguage:[languages getCurentLanguageKey]];
}

-(NSString*)storageFileNameForLanguage:(NSString*)languageKey{
    //NSString *str = [NSString stringWithFormat:@"client_%@_%@_%@",_entryPoint,[miscFunctions enStringFromDate:[NSDate date] onlyDate:YES],[languages getCurentLanguageKey]];
    NSString *str = [NSString stringWithFormat:@"client_%@",_entryPoint];
    return str;
}

// custom setter to overwrite the whole url when the entry changes
- (void)setEntryPoint:(NSString *)entryPoint{
    _entryPoint = entryPoint;
    _entryURL = [NSString stringWithFormat:@"%@/%@", _sSYSTEM_DOMAIN, _entryPoint];
}

- (NSString*)getTranslationForKey:(NSString *)key{
    NSString *translation = _translations[[languages currentLanguageKey]][key];
    return (translation!=nil && ![translation isEmptyString])?translation:_translations[@"en"][key];
}

# pragma mark load methods

- (void)loadClient{
    
    NSDictionary *clientDict = [[localStorage storageWithFilename:[self storageFileNameForLanguage:@""] atPath:clientPath] objectForKey:@"client"];
    
    if(clientDict){
        NSLog(@" ifm - loading client from file");
        [self initClientFromLanguagesData:clientDict];
    }
    else {
        NSLog(@" ifm - loading client from web");
        [self loadClientFromWeb];
    }
}

- (void)loadClientFromWeb{
    
    _loadingType = LOADINGTYPE_CLIENT;
    
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get.php?clientForEntry&entryPoint=%@",
                           _sAPI_URL,
                           _entryPoint
                           ];
    
    // Create the request
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)loadClientUpdate{
    
    _loadingType = LOADINGTYPE_CLIENT;
    
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get.php?clientForEntry&entryPoint=%@",
                           _sAPI_URL,
                           _entryPoint
                           ];
    
    // Create the request
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)checkForClientUpdate{
    NSLog(@" ifm - check for client update");
    if (![NSThread currentThread].isMainThread) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _loadingType = LOADINGTYPE_CLIENTUPDATECHECK;
            
            // defining the URL
            NSString *urlString = [NSString stringWithFormat:@"%@get.php?clientForEntry&entryPoint=%@&md5",
                                   _sAPI_URL,
                                   _entryPoint
                                   ];
            
            NSLog(@" ifm - check for update url: %@", urlString);
            
            // Create the request
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
            
            // Create the NSURLConnection
            [NSURLConnection connectionWithRequest:request delegate:self];
        });
    }
}


#pragma mark - everything that will be done after the connection finished loading

- (void)dataLoaded{
    
    if(_loadingType == LOADINGTYPE_CLIENT){
        NSLog(@" ifm - client loaded");
        if(_downloadedData==nil)_downloadedData=[[NSData data] mutableCopy];
        // Parse the JSON that came in
        NSError *error;
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
        NSString *jsonStringMD5 = [[[NSString alloc] initWithData:_downloadedData encoding:NSUTF8StringEncoding] MD5String].lowercaseString;
        
        // if it was not successfull
        if([jsonDictionary[@"success"] intValue]==0){
            if([_delegate respondsToSelector:@selector(iFeedbackModelDidLoadData)]){
                [_delegate iFeedbackModelDidLoadData];
            } else if([_delegate respondsToSelector:@selector(iFeedbackModelDidLoadDataWithViewController: forIndex:)]){
                UIViewController *vc = [self loadIFeedbackViewController];
                [_delegate iFeedbackModelDidLoadDataWithViewController:vc forIndex:_index];
            }
            return;
        }
        
        
        // store the data in our dictionary
        NSMutableDictionary *languageClients = [jsonDictionary[@"data"] mutableCopy];
        
        localStorage *clientStorage = [localStorage storageWithFilename:[self storageFileNameForLanguage:@""] atPath:clientPath];
        
        [clientStorage setObject:languageClients forKey:@"client"];
        [clientStorage setObject:jsonStringMD5 forKey:@"md5"];
        
        [self initClientFromLanguagesData:languageClients];
    }
    
    if(_loadingType == LOADINGTYPE_CLIENTUPDATECHECK){
        NSLog(@" ifm - update check loaded");
        if(_downloadedData!=nil){
            // Parse the JSON that came in
            NSError *error;
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
            
            if([jsonDictionary[@"md5"] isKindOfClass:[NSString class]]){
                localStorage *clientStorage = [localStorage storageWithFilename:[self storageFileNameForLanguage:@""] atPath:clientPath];
                if(![jsonDictionary[@"md5"] isEqualToString:[clientStorage objectForKey:@"md5"]]){
                    NSLog(@" ifm - update available, current md5: %@, server md5: %@",[clientStorage objectForKey:@"md5"],jsonDictionary[@"md5"]);
                    
                    [self loadClientUpdate];
                }
            }
        }
    }
}

- (void)initClientFromLanguagesData:(NSDictionary*)languageClients{
    // variables that we need later in the loop
    __block NSDictionary *clientDict = @{};
    __block BOOL languageDefined = _languageSelected;
    
    // save clients for each language in a file
    [languageClients enumerateKeysAndObjectsUsingBlock:^(id languageKey, id data, BOOL* stop) {
        // if we have not yet defined the language to use
        if(!languageDefined){
            NSString *entryLanguageKey = [languages getLanguageKeyForID:[data[@"entry"][@"language_id"] integerValue]];
            
            [languages setLanguage:entryLanguageKey];
            
            // we have defined our language now, don't do this again
            languageDefined = YES;
            clientDict = data;
        }
        
        // if the languageKey of this data matches our wanted language, save this data separatly for later use
        if([[languages currentLanguageKey] isEqualToString:languageKey]){
            clientDict = data;
        }
    }];
    
    // finished
    [self initClientFromData:[clientDict mutableCopy]];
}

- (void)initClientFromData:(NSMutableDictionary*)clientDict{
    
    NSLog(@" ifm - init client");
        
    _pID = [clientDict[@"entry"][@"pid"] integerValue];
    
    _parameters = clientDict[@"parameters"];
    
    
    _campaign = clientDict[@"campaign"];
    _campaignID = [_campaign[@"uid"] integerValue];
    
    _place = clientDict[@"place"];
    _placeID = [_place[@"uid"] integerValue];
    
    _entry = clientDict[@"entry"];
    _entryID = [_entry[@"uid"] intValue];
    
    _places = clientDict[@"places"];
    // define _campaigns depending on the selected place
    
    _dynamicFields = [[NSMutableArray alloc] initWithArray:clientDict[@"dynamic_fields"]];
    
    _surveyTopics = [[NSMutableArray alloc] initWithArray:clientDict[@"survey_topics"]];
    
    _topicRatings = [@{} mutableCopy];
    
    [self clientInitialized];
}

- (void)clientInitialized{
    if([_delegate respondsToSelector:@selector(iFeedbackModelDidLoadData)]){
        [_delegate iFeedbackModelDidLoadData];
    } else if([_delegate respondsToSelector:@selector(iFeedbackModelDidLoadDataWithViewController: forIndex:)]){
        UIViewController *vc = [self loadIFeedbackViewController];
        [_delegate iFeedbackModelDidLoadDataWithViewController:vc forIndex:_index];
    }
}


#pragma mark - NSURLConnectionDataProtocol methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self dataLoaded];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [self dataLoaded];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });
}


#pragma mark - getting view controllers

- (UIViewController*)loadIFeedbackViewController{

    UIViewController *svc;

    if(_campaign != nil &&
       _parameters!=nil &&
       [_parameters isKindOfClass:[NSDictionary class]] &&
       _parameters[@"skip_to_contactform"] != nil &&
       [_parameters[@"skip_to_contactform"] isKindOfClass:[NSString class]] &&
       [_parameters[@"skip_to_contactform"] isEqualToString:@"yes"]
       ){

        svc = [self getDetailViewController];
    }
    else if(_campaign != nil && _place != nil){

        svc = [self getTopicViewController];
    }
    else if(_place != nil){
        if([_place[@"campaigns"] isKindOfClass:[NSArray class]] && [_place[@"campaigns"] count] > 0) {
            _campaigns = _place[@"campaigns"];

            svc = [self getCampaignSelectViewController];
        }
        else if([_place[@"children"] isKindOfClass:[NSArray class]] && [_place[@"children"] count] > 0){
            _places = _place[@"children"];
            
            svc = [self getPlaceSelectViewController];
        }
    }

    if(svc != nil){
        //[svc setSidebarButton];

        ifbckNavigationViewController *nvc;
        if([_entry[@"entrytype"] intValue] == 3 && !_languageSelected){
            UIViewController *vc = [self getLanguageSelectViewController];
            [vc setSidebarButton];
            nvc = [[ifbckNavigationViewController alloc] initWithRootViewController:vc ifbckModel:self];
        }
        else {
            nvc = [[ifbckNavigationViewController alloc] initWithRootViewController:svc ifbckModel:self];
        }

        if([svc isKindOfClass:[TopicListViewController class]]){
            [((TopicListViewController *) svc) initRootViewController];
        }

        iFeedbackSidebarViewController *osvc = [iFeedbackSidebarViewController loadNowFromStoryboard:@"iFeedbackSidebarViewController"];
        [osvc setStartViewController:nvc];
        osvc.ifbckModel = self;


        SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:osvc frontViewController:nvc];
        [rvc setRightViewController:osvc animated:YES];
        [rvc setFrontViewShadowRadius:15.f];
        [rvc setRightViewRevealWidth:[UIScreen mainScreen].bounds.size.width / 2];
        [rvc setRightViewRevealOverdraw:0.1f];
        [rvc setRearViewRevealWidth:[UIScreen mainScreen].bounds.size.width / 2];
        [rvc setRearViewRevealOverdraw:0.1f];

        _startViewController = svc;

        return rvc;
    }

    ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
    wvc.urlGiven = YES;
    wvc.url = [NSURL URLWithString:_entryURL];
    wvc.titleString = _titleString;
    
    return wvc;
}

- (UIViewController*)getPlaceSelectViewController{
    PlaceSelectionViewController *psvc = [PlaceSelectionViewController loadNowFromStoryboard:@"PlaceSelectionViewController"];
    psvc.place = _place;
    psvc.places = _places;

    psvc.ifbckModel = self;

    return psvc;
}

- (UIViewController*)getCampaignSelectViewController{
    CampaignSelectionViewController *csvc = [CampaignSelectionViewController loadNowFromStoryboard:@"CampaignSelectionViewController"];
    csvc.campaigns = _campaigns;

    csvc.ifbckModel = self;

    return csvc;
}

- (UIViewController*)getDetailViewController{
    DynamicFormViewController *dfvc = [DynamicFormViewController loadNowFromStoryboard:@"DynamicFormViewController"];
    dfvc.ifbckModel = self;

    [dfvc loadTable];
    
    dfvc.title = _place[@"name"];

    return dfvc;
}

- (UIViewController *)getLanguageSelectViewController{
    LanguageSelectViewController *vc = [LanguageSelectViewController loadNowFromStoryboard:@"LanguageSelectViewController"];

    vc.ifbckModel = self;

    return vc;
}

- (UIViewController*)getTopicViewController{
    _topicPages = [@{} mutableCopy];
    _topicRatings = [@{} mutableCopy];
    
    TopicListViewController *tlvc = [TopicListViewController newRootList];
    
    _topicPages[@"0"] = tlvc;
    
    tlvc.topics = [_surveyTopics mutableCopy];
    tlvc.title = _place[@"name"];

    tlvc.ifbckModel = self;
    
    return tlvc;
}


#pragma mark - send feedback methods

- (BOOL)sendFeedbacksFromQueue{
    
    // init menaged context
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    // define a request and set search properties
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Feedback" inManagedObjectContext:moc]; // only feedbacks
    request.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"tstamp" ascending:YES]]; // ordered by tstamp
    request.predicate = [NSPredicate predicateWithFormat:@"sent == NO OR sent == nil"]; // only feedbacks, that are not sent
    
    // fetch the feedbacks from our request in an array
    NSError *error;
    NSArray *feedbacks = [[moc executeFetchRequest:request error:&error] mutableCopy];
    if(error)
        NSLog(@" ifm - error: %@", error);
    
    NSLog(@" ifm - found %lu feedbacks",(unsigned long)feedbacks.count);
    
    // loop through all feedbacks
    for(Feedback *feedback in feedbacks){
        // try to send feedback
        [feedback log];
        
        NSDictionary *response = [iFeedbackModel sendFeedbackWithPlaceID:feedback.placeID
                                                              campaignID:feedback.campaignID
                                                                 entryID:feedback.entryID
                                                          entryPointType:feedback.entryPointType
                                                                  tstamp:feedback.tstamp
                                                              languageID:feedback.languageID
                                                      dynamicFieldsArray:(NSArray*)feedback.dynamicFieldsArray
                                                  topicRatingsDictionary:(NSDictionary*)feedback.topicRatings
                                                                   toAPI:_sSECURED_API_URL
                                                             ignoreQueue:YES];
        
        // if the feedback was sent successfully, mark it as sent in our model
        feedback.sent = [response[@"success"] boolValue];
    }
    
    if (![NSThread currentThread].isMainThread) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // save / update edited feedbacks
            NSError *error;
            [moc save:&error];
            if(error)
                NSLog(@" ifm - error: %@", error);
        });
    }
    
    return YES;
}

- (NSDictionary*)sendFeedback{
    return [iFeedbackModel sendFeedbackWithPlaceID:(int)_placeID
                                        campaignID:(int)_campaignID
                                           entryID:(int)_entryID
                                    entryPointType:[_entry[@"ep_type"] intValue]
                                            tstamp:[[NSDate date] timeIntervalSince1970]
                                        languageID:[languages currentLanguageID]
                                dynamicFieldsArray:_dynamicFields
                            topicRatingsDictionary:_topicRatings
                                             toAPI:_sSECURED_API_URL
                                       ignoreQueue:NO];
}

+ (NSDictionary*)sendFeedbackWithPlaceID:(int)placeID
                              campaignID:(int)campaignID
                                 entryID:(int)entryID
                          entryPointType:(int)entryPointType
                                  tstamp:(double)tstamp
                              languageID:(int)languageID
                      dynamicFieldsArray:(NSArray*)dynamicFieldsArray
                  topicRatingsDictionary:(NSDictionary*)topicRatings
                                   toAPI:(NSString*)api
                             ignoreQueue:(BOOL)ignoreQueue{
    
    if(topicRatings==nil)topicRatings=@{};
    
    if(dynamicFieldsArray==nil)dynamicFieldsArray=@[];
    
    // sorting dynamic fields array, remove overhead
    NSMutableArray *sortedDynamicFields = [@[] mutableCopy];
    
    NSUInteger fieldUID;
    id value;
    NSString *starRatingComment;
    
    
    for(NSDictionary *dynamicFieldDict in dynamicFieldsArray){
        fieldUID = [dynamicFieldDict[@"uid"] integerValue];
        value = dynamicFieldDict[@"value"];
        starRatingComment = dynamicFieldDict[@"comment"];
        if(fieldUID> 0 && value!=nil){
            [sortedDynamicFields addObject:@{
                                             @"uid":@(fieldUID),
                                             @"value":value,
                                             @"comment":(starRatingComment!=nil)?starRatingComment:@"",
                                             }];
        }
    }
    
    dynamicFieldsArray = sortedDynamicFields;
    
    if(!(dynamicFieldsArray.count || topicRatings.count)){
        return @{@"success":@NO,@"message":NSLocalizedStringFromTable(@"Please fill out at least one field.",@"ifbckLocalizable",@"")};
    }
    // dynamic fields sorted
    
    
    // sending process
    int success = 0;
    int requestSent = 0;
    NSString *errorMessage = @"";
    
    NSLog(@" ifm - trying to send feedback");
    
    @try{
        NSError *error;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dynamicFieldsArray
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@""];
        
        NSDictionary *postDict = @{@"dynamicFieldValues":dynamicFieldsArray,@"topicRatings":topicRatings};
        
        NSString *post = [postDict urlEncodedString];
        
        //        NSString *post =[[NSString alloc] initWithFormat:@"dynamicFieldValues=%@", jsonString];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@save_feedback.php?place_id=%i&campaign_id=%i&entry_id=%i&ep_type=%i&tstamp=%.0f&language_id=%i",api,placeID,campaignID,entryID,entryPointType,tstamp,languageID]];
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)postData.length];
        
        NSLog(@" ifm - request, url: %@", url);
        NSLog(@" ifm - request, post: %@", post);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        request.URL = url;
        request.HTTPMethod = @"POST";
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        request.HTTPBody = postData;
        request.timeoutInterval = 240;
        
        
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        
        NSLog(@" ifm - request, response: %@", [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding]);
        
        if (response.statusCode >= 200 && response.statusCode < 300){
            requestSent = 1;
            NSError *error = nil;
            NSDictionary *jsonData = [NSJSONSerialization
                                      JSONObjectWithData:urlData
                                      options:NSJSONReadingMutableContainers
                                      error:&error];
            
            success = [jsonData[@"success"] intValue];
            errorMessage = jsonData[@"message"];
        }
        else {
            
        }
    }
    @catch (NSException * e) {
        NSLog(@" ifm - error: %@", e);
    }
    
    if(errorMessage==nil)errorMessage=@"";
    
    if(!requestSent && !ignoreQueue){
        if (![NSThread currentThread].isMainThread) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@" ifm - saving ifeedback to queue");
                
                NSManagedObjectContext *moc = [iFeedbackModel new].managedObjectContext;
                
                Feedback *feedback = (Feedback*)[NSEntityDescription insertNewObjectForEntityForName:@"Feedback" inManagedObjectContext:moc];
                
                feedback.placeID = placeID;
                feedback.campaignID = campaignID;
                feedback.entryID = entryID;
                feedback.entryPointType = entryPointType;
                feedback.tstamp = tstamp;
                feedback.languageID = languageID;
                feedback.dynamicFieldsArray = dynamicFieldsArray;
                feedback.topicRatings = topicRatings;
                
                NSError *error;
                [moc save:&error];
                
                if(error)
                    NSLog(@" ifm - error: %@", error);
                
            });
        }
    }
    
    return @{@"success":@(success), @"message":errorMessage};
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.bhmms.CoreDataTests" in the application's documents directory.
    return [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].lastObject;
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iFeedbackTerminal" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"iFeedbackTerminal.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:@{
                                                                   NSMigratePersistentStoresAutomaticallyOption : @YES,
                                                                   NSInferMappingModelAutomaticallyOption : @YES
                                                                   }
                                                           error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _managedObjectContext.persistentStoreCoordinator = coordinator;
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if (managedObjectContext.hasChanges && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
            abort();
        }
    }
}

@end
