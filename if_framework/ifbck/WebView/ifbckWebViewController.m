//
//  WebViewController.m
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckWebViewController.h"
#import "MBProgressHUD.h"

@interface ifbckWebViewController ()

@end

@implementation ifbckWebViewController

+(ifbckWebViewController*)getWebViewController{
    ifbckWebViewController *wvc = [ifbckWebViewController loadNowFromStoryboard:@"ifbckWebView"];
    
    return wvc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.webView.delegate = self;
    
    if(!self.urlGiven){
        self.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]; //if it's not given use
    }
    
    //prepare request for the webView
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:30.0];
    
    if(self.useHTML){
        // workaround but the only possible solution:
        // wrapping the content in a span, this sets a "default" font, if nothing is given in the html
        // we dont have access to modify the default font of the webkit
        _htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@;\">%@</span>",
                       @"Gotham-Book",
                       _htmlString];
        [self.webView loadHTMLString:self.htmlString baseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]];
    } else {
        //load the request
        [self.webView loadRequest:request];
        NSLog(@"%@",self.url);
        
        
        self.isLoading = YES;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    return self.navigationController == nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)prefillRegistrationForm{
//    NSString *head = [_webView stringByEvaluatingJavaScriptFromString: @"document.head.innerHTML"];
//    NSString *body = [_webView stringByEvaluatingJavaScriptFromString: @"document.body.innerHTML"];
//    NSString *page = [NSString stringWithFormat:@"<head>%@</head><body>%@</body>",head,body];
//    
//    [_webView stringByEvaluatingJavaScriptFromString:@"$('#registrationliteform-username').val('christopher.wulff@bhmgrp.com');"];
//    
}

- (void) webViewDidStartLoad:(UIWebView *)webView{
    
    self.isLoading = YES;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable", @"");
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    self.isLoading = NO;
    
    self.currentURL = self.webView.request.URL.absoluteString;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    
    [self.webView setBackgroundColor:[UIColor whiteColor]];
    [self.webView setOpaque:YES];
    
    [self prefillRegistrationForm];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if(error.code==NSURLErrorCancelled){
        return;
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertView *webError = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Error",@"ifbckLocalizable", @"") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel",@"ifbckLocalizable", @"") otherButtonTitles: nil];
    
    [webError show];
    
    NSLog(@"%@", error);
}

-(void)hideModalWebView{
    if(self.navigationController!=nil){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return NO;
}
@end