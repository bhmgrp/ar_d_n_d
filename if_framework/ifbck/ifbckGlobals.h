//
//  ifbckGlobals.h
//  if_framework
//
//  Created by User on 12/4/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "iFeedbackModel.h"
#import "cronjob.h"

//#define globaliFeedbackModel [ifbckGlobals sharedInstance].iFeedbackModel
#define globalCronjob [ifbckGlobals sharedInstance].cronjob

@interface ifbckGlobals : NSObject

+(ifbckGlobals *)sharedInstance;


@property (nonatomic) NSString *sSYSTEM_DOMAIN, *sSECURED_API_URL, *sAPI_URL;

@property (nonatomic) iFeedbackModel *iFeedbackModel;
@property (nonatomic) cronjob *cronjob;

@property (nonatomic) NSString *currentLanguageKey;

@property (nonatomic, readonly) BOOL userLoggedIn,terminalMode;

@end