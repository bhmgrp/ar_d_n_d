//
//  ifbckNavigationViewController.m
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckNavigationViewController.h"

@interface ifbckNavigationViewController (){
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) NSMutableArray *navigationBarBackgroundViews;

@property (nonatomic) BOOL hideStatusBar;

@end

@implementation ifbckNavigationViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    
    [self updateNavigationBarStyle];
    
    //[self.view addGestureRecognizer:[[SWRevealViewController alloc] init].panGestureRecognizer];
    self.interactivePopGestureRecognizer.enabled = NO;
    
    _navigationBarBackgroundViews = [@[] mutableCopy];
}

- (void)viewDidLayoutSubviews {
    if(_footerToolbar!=nil && _footerView!=nil){
        _footerToolbar.frame = CGRectMake(0,self.view.height-49,screenWidth,49);
        _footerView.frame = CGRectMake(0,self.view.height-50-64,screenWidth,64);
        _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-50-64-94,screenWidth,94);
        _topicOkButton.frame = CGRectMake(10,15,_topicOkButtonWrapper.width-20,62);
        if(IDIOM!=IPAD){
            _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-50-64-64,screenWidth,64);
            _topicOkButton.frame = CGRectMake(10,10,_topicOkButtonWrapper.width-20,44);
        }
    }
    else if(_footerToolbar!=nil){
        _footerToolbar.frame = CGRectMake(0,self.view.height-49,screenWidth,49);
        _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-50-94,screenWidth,94);
        if(IDIOM!=IPAD){
            _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-50-64,screenWidth,64);
            _topicOkButton.frame = CGRectMake(10,10,_topicOkButtonWrapper.width-20,44);
        }
    }
    else if(_footerView!=nil){
        _footerView.frame = CGRectMake(0,self.view.height-64,screenWidth,64);
        _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-64-94,screenWidth,94);
        if(IDIOM!=IPAD){
            _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-64-64,screenWidth,64);
            _topicOkButton.frame = CGRectMake(10,10,_topicOkButtonWrapper.width-20,44);
        }
    }
    else {
        _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-94,screenWidth,94);
        if(IDIOM!=IPAD){
            _topicOkButtonWrapper.frame = CGRectMake(0,self.view.height-64,screenWidth,64);
            _topicOkButton.frame = CGRectMake(10,10,_topicOkButtonWrapper.width-20,44);
        }
    }
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController ifbckModel:(iFeedbackModel*)ifbckModel{


    self = [super initWithRootViewController:rootViewController];

    self.ifbckModel = ifbckModel;

    UIView *navigationBarBackground = [self getNavigationBarBackgroundView];

    [_navigationBarBackgroundViews addObject:navigationBarBackground];

    [rootViewController.view addSubview:navigationBarBackground];

    ((iFeedbackViewController*)rootViewController).hasNavigationBarbackground = YES;

    [self updateNavigationBarStyle];

    [self updateFooterBarStyle];

    return self;
}

- (UIView*)getNavigationBarBackgroundView{

    if(self.ifbckModel.navigationBarHeight!=0){
        _NAVIGATION_BACKGROUND_HEIGHT = self.ifbckModel.navigationBarHeight;
    }
    else if(self.ifbckModel.parameters[@"app_hide_statusbar"]!=nil && ![self.ifbckModel.parameters[@"app_hide_statusbar"] isEmptyString] && [self.ifbckModel.parameters[@"app_hide_statusbar"] isEqualToString:@"yes"]){
        _hideStatusBar = YES;
        _NAVIGATION_BACKGROUND_HEIGHT = 44;

        [self setNeedsStatusBarAppearanceUpdate];
    }
    else {
        _hideStatusBar = NO;
        _NAVIGATION_BACKGROUND_HEIGHT = 64;
    }

    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIView *navigationBarBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    CGRect backgroundFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _NAVIGATION_BACKGROUND_HEIGHT);
    navigationBarBackground.frame = backgroundFrame;
    
    navigationBarBackground = [[UIView alloc] initWithFrame:backgroundFrame];
    navigationBarBackground.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    
    // if we have the parameter "app_navigation_bar_color"
    if(self.ifbckModel.parameters[@"app_navigation_bar_color"]!=nil && ![self.ifbckModel.parameters[@"app_navigation_bar_color"] isEmptyString]){
        navigationBarBackground = [[UIView alloc] initWithFrame:backgroundFrame];
        navigationBarBackground.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_navigation_bar_color"]];
    }
    
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, navigationBarBackground.height, navigationBarBackground.width, 1.0f)];
    
    bar.backgroundColor = [UIColor colorWithRGBHex:0x4682BE];
    
    if(self.ifbckModel.parameters[@"app_navigation_line_color"]!=nil && ![self.ifbckModel.parameters[@"app_navigation_line_color"] isEmptyString]){
        bar.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_navigation_line_color"]];
    }
    
    [navigationBarBackground addSubview:bar];
    
    return navigationBarBackground;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    if([_textColor colorIsLight])
        return UIStatusBarStyleLightContent;
    else
        return UIStatusBarStyleDefault;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

- (BOOL)prefersStatusBarHidden{
    return _hideStatusBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateNavigationBarStyle{

    _textColor = [UIColor colorWithRGBHex:0x4682BE];
    
    if(self.ifbckModel.parameters[@"app_navigation_text_color"]!=nil){
        _textColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_navigation_text_color"]];
    }
    
    //navigation Bar style
    //[UINavigationBar appearance].barTintColor = [UIColor clearColor];
    
    self.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationBar.tintColor = _textColor;
    self.navigationBar.titleTextAttributes = @{
                                                  NSForegroundColorAttributeName: _textColor,
                                                  NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:17.0f]
                                                  };
    
    
    self.navigationBar.translucent = YES;
    self.navigationBar.shadowImage = [[UIImage alloc]init];
    self.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];

    if(self.ifbckModel.navigationBarHeight!=0){
        _NAVIGATION_BACKGROUND_HEIGHT = self.ifbckModel.navigationBarHeight;
    }
    else if(self.ifbckModel.parameters[@"app_hide_statusbar"]!=nil && ![self.ifbckModel.parameters[@"app_hide_statusbar"] isEmptyString] && [self.ifbckModel.parameters[@"app_hide_statusbar"] isEqualToString:@"yes"]){
        _hideStatusBar = YES;
        _NAVIGATION_BACKGROUND_HEIGHT = 44;
    }
    else {
        _hideStatusBar = NO;
        _NAVIGATION_BACKGROUND_HEIGHT = 64;
    }

    [self updateNavigationBackgroundViews];

    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)updateNavigationBackgroundViews{
    [self hideNavigationBarBackground:NO animated:YES];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    // show the new view controller
    [super pushViewController:viewController animated:animated];
    
    if(self.viewControllers.count>0){
        if(!((iFeedbackViewController*)viewController).hasNavigationBarbackground){
            
            UIView *navigationBarBackground = [self getNavigationBarBackgroundView];
            
            [viewController.view addSubview:navigationBarBackground];
            
            [_navigationBarBackgroundViews addObject:navigationBarBackground];
            
            ((iFeedbackViewController*)viewController).hasNavigationBarbackground = YES;
        }
    }
    
    // if the new view controller is a topic list
    if([viewController isKindOfClass:[TopicListViewController class]]){
        // show the footer
        [self showTopicFooter];
        
        // if we switch to the root view
        if(viewController == self.rootTopicListViewController){
            // unselect all tab bar items
            [_footerToolbar setSelectedItem:nil];
        }
        // if it is a normal topic list
        else if(self.ifbckModel.topicRatings.count>0 && !((TopicListViewController*)viewController).hasNextSectionButton){
            [self addNextSectionButtonToViewController:viewController];
        }
    }
    else {
        // else hide the topic footer
        [self hideTopicFooter];
    }
    
}

- (void)setViewControllers:(NSArray<UIViewController *> *)viewControllers animated:(BOOL)animated{
    
    for(iFeedbackViewController *vc in viewControllers){
        if(!vc.hasNavigationBarbackground){
            UIView *navigationBarBackground = [self getNavigationBarBackgroundView];
            
            [vc.view addSubview:navigationBarBackground];
            
            [_navigationBarBackgroundViews addObject:navigationBarBackground];
            
            vc.hasNavigationBarbackground = YES;
        }
        
        // if the new view controller is a topic list
        if([vc isKindOfClass:[TopicListViewController class]]){
            // show the footer
            [self showTopicFooter];
            
            // if it is a normal topic list
            if(self.ifbckModel.topicRatings.count>0 && !((TopicListViewController*)vc).hasNextSectionButton && vc != self.rootTopicListViewController){
                [self addNextSectionButtonToViewController:vc];
            }
        }
    }
    
    
    [super setViewControllers:viewControllers animated:animated];
    
    // if the new view controller is a topic list
    if([viewControllers[viewControllers.count-1] isKindOfClass:[TopicListViewController class]]){
        
    }
    else {
        [self hideTopicFooter];
    }
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    // pop the view controller
    UIViewController *vc = [super popViewControllerAnimated:animated];
    
    // if our new view controller is a topic list
    if([self.topViewController isKindOfClass:[TopicListViewController class]]){
        // show the topic footer
        [self showTopicFooter];
        
        // if we switch back to the root view
        if(self.topViewController == self.rootTopicListViewController){
            // unselect all tab bar items
            [_footerToolbar setSelectedItem:nil];
        }
        else if(self.ifbckModel.topicRatings.count>0 && !((TopicListViewController*)self.topViewController).hasNextSectionButton){
            [self addNextSectionButtonToViewController:self.topViewController];
        }
    }
    else {
        // else hide the topic footer
        [self hideTopicFooter];
        
        [_footerToolbar removeFromSuperview];
        [_footerView removeFromSuperview];
        
        _footerToolbar = nil;
        _footerView = nil;
    }
    
    return vc;
}

- (void)addNextSectionButtonToViewController:(UIViewController*)viewController{
    // create a "next section" button and add it to our view
    
    
    // define the frame of the main button
    CGRect buttonFrame = CGRectMake(10, 15, screenWidth-20, 62);
    CGRect buttonWrapperFrame = CGRectMake(0, self.view.height - _footerHeight, screenWidth, 104);
    if(IDIOM!=IPAD){
        buttonFrame = CGRectMake(10, 10, screenWidth-20, 44);
        buttonWrapperFrame = CGRectMake(0, self.view.height - _footerHeight, screenWidth, 64);
    }
    
    
    // creating button
    UIButton *nextSectionButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [nextSectionButton setTitle:[[self.ifbckModel getTranslationForKey:@"pi1_button_closing_fb_long"] uppercaseString] forState:UIControlStateNormal];
    [nextSectionButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [nextSectionButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    
    if(IDIOM!=IPAD)
        [nextSectionButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    
    [nextSectionButton addTarget:self action:@selector(openTopicRoot) forControlEvents:UIControlEventTouchUpInside];
    
    
    // button background
    if(self.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![self.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        // creating normal background
        nextSectionButton.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_background_color"]];
    }
    else {
        nextSectionButton.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    }
    
    [nextSectionButton.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [nextSectionButton.layer setBorderWidth:1.0f];
    [nextSectionButton.layer setCornerRadius:5.0f];
    
    
    // button text color
    if(self.ifbckModel.parameters[@"app_send_button_text_color"]!=nil && ![self.ifbckModel.parameters[@"app_send_button_text_color"] isEmptyString]){
        [nextSectionButton setTitleColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [nextSectionButton.layer setBorderColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
    }
    
    
    // create wrapper
    UIView *buttonWrapper = [[UIView alloc] initWithFrame:buttonWrapperFrame];
    
    // button border color
    if(self.ifbckModel.parameters[@"app_send_button_border_color"]!=nil && ![self.ifbckModel.parameters[@"app_send_button_border_color"] isEmptyString]){
        [buttonWrapper setBackgroundColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_border_color"]]];
    }

    // creating wrapper
    [buttonWrapper addSubview:nextSectionButton];
    
    // add the button to our main view
    [viewController.view addSubview:buttonWrapper];
    
    ((TopicListViewController*)viewController).hasNextSectionButton = YES;
}

- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated{
    [super setNavigationBarHidden:hidden animated:animated];
    [self hideNavigationBarBackground:hidden animated:animated];
}

- (void)hideNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated{
    if(hidden){
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, _NAVIGATION_BACKGROUND_HEIGHT);
                    for(UIView *bar in navigationBarBackground.subviews){
                        bar.y = _NAVIGATION_BACKGROUND_HEIGHT;
                    }
                }];
            }
            else {
                navigationBarBackground.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, _NAVIGATION_BACKGROUND_HEIGHT);
                for(UIView *bar in navigationBarBackground.subviews){
                    bar.y = _NAVIGATION_BACKGROUND_HEIGHT;
                }
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _NAVIGATION_BACKGROUND_HEIGHT);
                    for(UIView *bar in navigationBarBackground.subviews){
                        bar.y = _NAVIGATION_BACKGROUND_HEIGHT;
                    }
                }];
            }
            else {
                navigationBarBackground.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _NAVIGATION_BACKGROUND_HEIGHT);
                for(UIView *bar in navigationBarBackground.subviews){
                    bar.y = _NAVIGATION_BACKGROUND_HEIGHT;
                }
            }
        }
    }
}

- (void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated{
    if(hidden){
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 0.0;
                }];
            }
            else {
                navigationBarBackground.alpha = 0.0;
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 1.0;
                }];
            }
            else {
                navigationBarBackground.alpha = 1.0;
            }
        }
    }
}

- (void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated finished:(void(^)(void))finished{
    void(^finishedfinishedFunction)() = finished;
    
    if(hidden){
        NSLog(@"hide navigation background");
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 0.0;
                } completion:^(BOOL finished){
                    if(finished){
                        finishedfinishedFunction();
                    }
                }];
            }
            else {
                navigationBarBackground.alpha = 0.0;
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 1.0;
                } completion:^(BOOL finished){
                    finishedfinishedFunction();
                }];
            }
            else {
                navigationBarBackground.alpha = 1.0;
                [self setNavigationBarHidden:NO animated:NO];
            }
        }
    }
}


- (void)hideStatusBar:(BOOL)hide{
    _hideStatusBar = hide;

    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)initHeaderView{
    if(self.ifbckModel.parameters[@"app_navigation_bar_logo"]!=nil && ![self.ifbckModel.parameters[@"app_navigation_bar_logo"] isEmptyString]){
        // load image
        UIImage *titleImage = [imageMethods UIImageWithServerPathName:self.ifbckModel.parameters[@"app_navigation_bar_logo"] versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)self.ifbckModel.pID,150] width:150];
        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-116, 44)];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:titleImage];
        imageView.frame = titleView.bounds;
        
        [titleView addSubview:imageView];
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;
//        titleView.width = 300;
//        
//        // update UI
//        if(_hideStatusBar)
//            titleView.y = 0;
//        else
//            titleView.y = 20;
//        
//        titleView.center = CGPointMake(screenWidth/2, titleView.center.y);
//        
//        [self.view addSubview:titleView];
        [self.topViewController.navigationItem setTitleView:titleView];
    }
}

- (void)initFooterForTopics:(NSMutableArray*)topics{

    float currentFooterHeight = 0;
    
    
    float footerSendButtonHeight = 94;
    if(IDIOM!=IPAD)
        footerSendButtonHeight = 64;

    float footerBackgroundHeight = 64;
    
    _footerToolbar.hidden = YES;
    _footerView.hidden = YES;
    [_footerToolbar removeFromSuperview];
    [_footerView removeFromSuperview];
    _footerToolbar = nil;
    _footerView = nil;
    
    
    // footer tab bar
    if(self.ifbckModel.parameters[@"hide_footer"]==nil || ![self.ifbckModel.parameters[@"hide_footer"] isEqualToString:@"yes"]){
        
        NSMutableArray *toolBarItems = [@[] mutableCopy];
        
        int i = 0;
        for(NSDictionary *topic in topics){
            if([topic[@"children"] count] > 0){
                UIImage *tabBarItemImage;
                NSString *tabBarItemTitle;
                
                if(topic[@"image2"]!= nil && ![topic[@"image2"] isEmptyString]){
                    
                    CGSize scaledSize = CGSizeMake(25, 25);
                    
                    NSMutableString *imageString = [topic[@"image_sm"] mutableCopy];
                    
                    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                                  @"(\\_[0-9]+\\.png)" options:0 error:nil];
                    
                    [regex replaceMatchesInString:imageString options:0 range:NSMakeRange(0, [imageString length]) withTemplate:@".png"];
                    
                    UIImage *tabButton = [imageMethods UIImageWithServerPathName:[NSString stringWithFormat:@"files/icons/%@",imageString] versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)self.ifbckModel.pID,100] width:100];
                    
                    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
                    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
                    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                } else {
                    tabBarItemImage = nil;
                }
                
                tabBarItemTitle = ![topic[@"shortname"] isEmptyString]?topic[@"shortname"]:topic[@"name"];
                
                tabBarItemImage = [tabBarItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                
                UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:i];
                
                [toolBarItems addObject:tabBarItem];
            }
            
            i++;
        }
        
        if(toolBarItems.count>0){
            [toolBarItems addObject:[[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:toolBarItems.count]];
            
            if(_footerToolbar==nil)_footerToolbar = [[UITabBar alloc] initWithFrame:CGRectMake(0,self.view.height-49,screenWidth,49)];

            _footerToolbar.hidden = YES;
            _footerToolbar.alpha = 0.0f;
            [self updateFooterBarStyle];

            if(_footerToolbar.superview!=self.view)
                [self.view addSubview:_footerToolbar];

            [_footerToolbar setItems:toolBarItems animated:YES];

            _footerToolbar.delegate = self;
            currentFooterHeight += 49;
        }
        else {
            
        }
    }
    else {
        
    }

    // if we dont have a logo in the header
    if((self.ifbckModel.parameters[@"app_navigation_bar_logo"]==nil || [self.ifbckModel.parameters[@"app_navigation_bar_logo"] isEmptyString]) && (self.ifbckModel.parameters[@"footer_logo"]!=nil && ![self.ifbckModel.parameters[@"footer_logo"] isEmptyString])){
        // footer background
        if(self.ifbckModel.parameters[@"hide_footer_bg"]!=nil && ![self.ifbckModel.parameters[@"hide_footer_bg"] isEqualToString:@"yes"]){
            _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height-currentFooterHeight-footerBackgroundHeight, screenWidth, footerBackgroundHeight)];
            _footerView.backgroundColor = [UIColor whiteColor];
            _footerView.hidden = YES;
            _footerView.alpha = 0.0f;
            [self.view addSubview:_footerView];
        }
        else if(self.ifbckModel.parameters[@"footer_bgcolor"]!=nil && ![self.ifbckModel.parameters[@"footer_bgcolor"] isEmptyString]){
            _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height-currentFooterHeight-footerBackgroundHeight, screenWidth, footerBackgroundHeight)];
            _footerView.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"footer_bgcolor"]];
            _footerView.hidden = YES;
            _footerView.alpha = 0.0f;
            [self.view addSubview:_footerView];
        }
        else {
            _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height-currentFooterHeight-footerBackgroundHeight, screenWidth, footerBackgroundHeight)];
            _footerView.backgroundColor = [UIColor clearColor];
            _footerView.hidden = YES;
            _footerView.alpha = 0.0f;
            [self.view addSubview:_footerView];
        }
        
        // footer logo
        if(self.ifbckModel.parameters[@"footer_logo"]!=nil && ![self.ifbckModel.parameters[@"footer_logo"] isEmptyString]){
            // load image in background
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                UIImage *footerLogoImage = [imageMethods UIImageWithServerPathName:self.ifbckModel.parameters[@"footer_logo"] versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)self.ifbckModel.pID,300] width:300];
                UIView *footerLogoView = [[UIImageView alloc] initWithImage:footerLogoImage];
                
                footerLogoView.contentMode = UIViewContentModeScaleAspectFit;
                footerLogoView.height = 52;
                
                // update UI in main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_footerView addSubview:footerLogoView];
                    footerLogoView.center = CGPointMake(screenWidth/2, _footerView.height/2);
                });
            });
            currentFooterHeight += 64;
        }
        else {
            _footerView = nil;
        }
    }
    
    // send button
    currentFooterHeight += footerSendButtonHeight;
    
    _footerHeight = currentFooterHeight;
    
    [self initRootSendButton];
    
    [self showTopicFooter];
}

- (void)initRootSendButton{
    
    // define the frame of the main button
    CGRect buttonFrame = CGRectMake(10, 15, screenWidth-20, 62);
    if(IDIOM!=IPAD)
        buttonFrame = CGRectMake(10,10,screenWidth-20,44);

    CGRect buttonWrapperFrame = CGRectMake(0, self.view.height - _footerHeight, screenWidth, 104);
    
    // creating button
    _topicOkButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [_topicOkButton setTitle:[[self.ifbckModel getTranslationForKey:@"pi1_button_closing_fb"] uppercaseString] forState:UIControlStateNormal];
    [_topicOkButton addTarget:self action:@selector(openDynamicForm) forControlEvents:UIControlEventTouchUpInside];
    [_topicOkButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [_topicOkButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    
    if(IDIOM!=IPAD)
        [_topicOkButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    
    
    // button background
    if(self.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![self.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        // creating normal background
        _topicOkButtonBackgroundView = [[UIView alloc] initWithFrame:buttonFrame];
        _topicOkButtonBackgroundView.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_background_color"]];
    }
    else {
        _topicOkButtonBackgroundView = [[UIView alloc] initWithFrame:buttonFrame];
        _topicOkButtonBackgroundView.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    }
    
    [_topicOkButtonBackgroundView.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [_topicOkButtonBackgroundView.layer setBorderWidth:1.0f];
    [_topicOkButtonBackgroundView.layer setCornerRadius:5.0f];
    
    // button text color
    if(self.ifbckModel.parameters[@"app_send_button_text_color"]!=nil && ![self.ifbckModel.parameters[@"app_send_button_text_color"] isEmptyString]){
        [_topicOkButton setTitleColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_topicOkButtonBackgroundView.layer setBorderColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
    }
    
    // creating wrapper
    _topicOkButtonWrapper = [[UIView alloc] initWithFrame:buttonWrapperFrame];
    [_topicOkButtonWrapper setHidden:YES];
    [_topicOkButtonWrapper addSubview:_topicOkButtonBackgroundView];
    [_topicOkButtonWrapper addSubview:_topicOkButtonBackground];
    [_topicOkButtonWrapper addSubview:_topicOkButton];
    
    // button border color
    if(self.ifbckModel.parameters[@"app_send_button_border_color"]!=nil && ![self.ifbckModel.parameters[@"app_send_button_border_color"] isEmptyString]){
        _topicOkButtonWrapper.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_border_color"]];
    }
    
    
    // add the button to our main view
    [_rootTopicListViewController.view addSubview:_topicOkButtonWrapper];
}

- (void)updateFooterBarStyle{
    [_footerToolbar setBarTintColor:[UIColor clearColor]];
    [_footerToolbar setTranslucent:YES];
    [_footerToolbar setBackgroundImage:[UIImage new]];
    [_footerToolbar setShadowImage:[UIImage new]];
    [_footerToolbar setBarStyle:UIBarStyleDefault];
    [_footerToolbar setTintColor:[UIColor blackColor]];
    
    _footerToolbar.tintColor = [UIColor colorWithRGBHex:0x4682BE];
    
    if(self.ifbckModel.parameters[@"app_footerbar_text_color"]!=nil && ![self.ifbckModel.parameters[@"app_footerbar_text_color"] isEmptyString]){
        [_footerToolbar setTintColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_footerbar_text_color"]]];
    }
    
    if(self.ifbckModel.parameters[@"app_footerbar_color"]!=nil && ![self.ifbckModel.parameters[@"app_footerbar_color"] isEmptyString]){
        UIView *barBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _footerToolbar.height)];
        [barBackground setBackgroundColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_footerbar_color"]]];
        [_footerToolbar addSubview:barBackground];
    }
    else {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIView *barBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        [barBackground setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _footerToolbar.height)];
        [_footerToolbar addSubview:barBackground];
    }

    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, -1.0f, [UIScreen mainScreen].bounds.size.width, 1.0f)];
    [bar setBackgroundColor:[UIColor colorWithRGBHex:0x4682BE]];

    if(self.ifbckModel.parameters[@"app_footerbar_text_color"]!=nil && ![self.ifbckModel.parameters[@"app_footerbar_text_color"] isEmptyString]){
        [bar setBackgroundColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_footerbar_text_color"]]];
    }

    if(self.ifbckModel.parameters[@"app_navigation_line_color"]!=nil && ![self.ifbckModel.parameters[@"app_navigation_line_color"] isEmptyString]){
        bar.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_navigation_line_color"]];
    }
    
    [_footerToolbar addSubview:bar];


    // button background
    if(self.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![self.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        // creating normal background
        _topicOkButtonBackgroundView.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_background_color"]];
    }
    else {
        _topicOkButtonBackgroundView.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    }

    // button text color
    if(self.ifbckModel.parameters[@"app_send_button_text_color"]!=nil && ![self.ifbckModel.parameters[@"app_send_button_text_color"] isEmptyString]){
        [_topicOkButton setTitleColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_topicOkButtonBackgroundView.layer setBorderColor:[UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
    }

    // button border color
    if(self.ifbckModel.parameters[@"app_send_button_border_color"]!=nil && ![self.ifbckModel.parameters[@"app_send_button_border_color"] isEmptyString]){
        _topicOkButtonWrapper.backgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_send_button_border_color"]];
    }

}

- (void)openTopicRoot{
    NSMutableArray *viewControllers = [@[] mutableCopy];
    
    for(UIViewController *vc in self.viewControllers){
        [viewControllers addObject:vc];
        if(vc == _rootTopicListViewController){
            break;
        }
    }
    
    [self setViewControllers:viewControllers animated:NO];
}

- (void)openDynamicForm{
    if(self.ifbckModel.topicRatings != nil){
        [self pushViewController:[self.ifbckModel getDetailViewController] animated:YES];
    }
}

- (void)hideTopicFooter{
    [UIView animateWithDuration:0.5 animations:^{
        _footerView.alpha = 0.0f;
        _footerToolbar.alpha = 0.0f;
        _topicOkButtonWrapper.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _footerView.hidden = YES;
        _footerToolbar.hidden = YES;
        _topicOkButtonWrapper.hidden = YES;
    }];
}

- (void)showTopicFooter{
    if(_footerView!=nil || _footerToolbar != nil || _topicOkButton != nil){
        NSLog(@"showing footer");
        _footerView.hidden = NO;
        _footerToolbar.hidden = NO;
        _topicOkButtonWrapper.hidden = !(self.ifbckModel.topicRatings.count>0);
        [UIView animateWithDuration:0.5 animations:^{
            _footerView.alpha = 1.0f;
            _footerToolbar.alpha = 1.0f;
            _topicOkButtonWrapper.alpha = 1.0f;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)removeTopicFooter{
    [UIView animateWithDuration:0.5 animations:^{
        _footerView.alpha = 0.0f;
        _footerToolbar.alpha = 0.0f;
        _topicOkButtonWrapper.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _footerView.hidden = YES;
        _footerToolbar.hidden = YES;
        _topicOkButtonWrapper.hidden = YES;
        
        [_footerView removeFromSuperview];
        [_footerToolbar removeFromSuperview];
        [_topicOkButtonWrapper removeFromSuperview];
        
        _footerView = nil;
        _footerToolbar = nil;
        _topicOkButtonWrapper = nil;
    }];
}

#pragma mark - tabBar delegates

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(item.tag == tabBar.items.count-1){
        // more button
        [self.revealViewController rightRevealToggle:nil];
        [tabBar setSelectedItem:nil];
        return;
    }
    
    _rootTopicListViewController.rowSelected = YES;
    _rootTopicListViewController.selectedRow = item.tag;
    
    UIViewController *vc2 = [_rootTopicListViewController getViewControllerForIndex:item.tag];
    
    if(self.topViewController == vc2){
        return;
    }
    
    if(self.topViewController == _rootTopicListViewController){
        [self pushViewController:vc2 animated:YES];
    }
    
    BOOL animated = self.topViewController == _rootTopicListViewController;
    
    NSMutableArray *viewControllers = [@[] mutableCopy];
    
    for(UIViewController *vc in self.viewControllers){
        [viewControllers addObject:vc];
        if(vc == _rootTopicListViewController){
            break;
        }
    }
    
    [viewControllers addObject:vc2];
    
    [self setViewControllers:viewControllers animated:animated];
}


#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.view.frame = CGRectMake(0, 0, screenWidth, screenHeight - (kbSize.height));
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    //    NSDictionary* info = [aNotification userInfo];
    //
    //    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    //
    //    [UIView animateWithDuration:0.5 animations:^(void){
    //        self.navigationController.view.frame = CGRectMake(0, -(64+_headerView.height), [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) +_headerView.height+64);
    //    }];
    //
    //
    //    //self.headerView.height = 0;
    //    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}

-(void)updateBadge:(NSString*)badge ForIndex:(NSInteger)index{
    _footerToolbar.items[index].badgeValue = badge;
}

@end