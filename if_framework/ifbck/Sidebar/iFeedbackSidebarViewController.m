//
//  iFeedbackSidebarViewController.m
//  if_framework_terminal
//
//  Created by User on 4/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackSidebarViewController.h"
#import "ifbckNavigationViewController.h"
#import "ifbckWebViewController.h"

@interface iFeedbackSidebarViewController ()

@property (nonatomic) NSArray *data;

enum{OPEN_START = 0, OPEN_WEB = 1, OPEN_GUIDED_ACCESS = 2, OPEN_IFEEDBACK_START = 3};

@property (nonatomic) BOOL opening;

@end

@implementation iFeedbackSidebarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _data = @[
              @{
                  @"sectionTitle":@"Title",
                  @"sectionCells":
                      @[
                          @{
                              @"title":NSLocalizedStringFromTable(@"Start", @"ifbckLocalizable", @""),
                              @"open":@(OPEN_IFEEDBACK_START),
                              @"openString":@"",
                              },
                          ],
                  
                  },
              @{
                  @"sectionTitle":@"Title",
                  @"sectionCells":
                      @[
                          @{
                              @"title":NSLocalizedStringFromTable(@"Imprint",@"ifbckLocalizable", @""),
                              @"open":@(OPEN_WEB),
                              @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/IMPRINT", @"ifbckLocalizable", @""),
                              },
                          @{
                              @"title":NSLocalizedStringFromTable(@"Privacy policy",@"ifbckLocalizable", @""),
                              @"open":@(OPEN_WEB),
                              @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/PRIVACYPOLICY", @"ifbckLocalizable", @""),
                              },
                          ],
                  
                  },
              ];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    _opening = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return [self.startViewController preferredStatusBarStyle];
}

-(BOOL)prefersStatusBarHidden{
    return (self.ifbckModel.parameters[@"app_hide_statusbar"]!=nil && ![self.ifbckModel.parameters[@"app_hide_statusbar"] isEmptyString] && [self.ifbckModel.parameters[@"app_hide_statusbar"] isEqualToString:@"yes"]);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _data.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)_data[section][@"sectionCells"]).count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = _data[indexPath.section][@"sectionCells"][indexPath.row][@"title"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(_opening)
        return;
    
    NSInteger open = [_data[indexPath.section][@"sectionCells"][indexPath.row][@"open"] integerValue];
    
    _opening = YES;
    
    if(open == OPEN_START) {
    }
    
    if(open == OPEN_IFEEDBACK_START) {
        [self.revealViewController pushFrontViewController:_startViewController animated:YES];
    }
    
    if(open == OPEN_WEB) {
        NSString *openString = _data[indexPath.section][@"sectionCells"][indexPath.row][@"openString"];
        if([_data[indexPath.section][@"sectionCells"][indexPath.row][@"openString"] hasPrefix:@"http"]){
            ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
            
            wvc.titleString = [_data[indexPath.section][@"sectionCells"][indexPath.row][@"title"] uppercaseString];
            wvc.url = [NSURL URLWithString:openString];
            wvc.urlGiven = YES;
            
            [wvc setSidebarButton];
            
            ifbckNavigationViewController *onvc = [[ifbckNavigationViewController alloc] initWithRootViewController:wvc];
            
            [self.revealViewController pushFrontViewController:onvc animated:YES];
            
            return;
        }
        
        ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
        
        wvc.titleString = [_data[indexPath.section][@"sectionCells"][indexPath.row][@"title"] uppercaseString];
        wvc.htmlString = openString;
        wvc.useHTML = YES;
        
        [wvc setSidebarButton];
        
        ifbckNavigationViewController *onvc = [[ifbckNavigationViewController alloc] initWithRootViewController:wvc];
        
        [self.revealViewController pushFrontViewController:onvc animated:YES];
        
        return;
    }
    
}

@end
