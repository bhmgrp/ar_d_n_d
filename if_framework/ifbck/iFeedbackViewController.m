//
//  iFeedbackViewController.m
//  if_framework_terminal
//
//  Created by User on 5/12/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackViewController.h"

@interface iFeedbackViewController(){
    BOOL resizedForIPhone;
}

@end

@implementation iFeedbackViewController

- (instancetype)init{
    if(!self)
        self = [super init];
    
    
    
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    _tableViewCellBackgroundColor = [UIColor whiteColor];
    _tableViewBackgroundColor = [UIColor colorWithRGBHex:0xf7f7f7];
    _tableViewCellTintColor = [UIColor colorWithRGBHex:0x5A5A5F];
    
    if(self.ifbckModel.parameters[@"app_background_color"]!=nil && ![self.ifbckModel.parameters[@"app_background_color"] isEmptyString])
        _tableViewBackgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_background_color"]];
    
    
    if(self.ifbckModel.parameters[@"app_cell_color"]!=nil && ![self.ifbckModel.parameters[@"app_cell_color"] isEmptyString])
        _tableViewCellBackgroundColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_cell_color"]];
    
    if(self.ifbckModel.parameters[@"app_cell_text_color"]!=nil && ![self.ifbckModel.parameters[@"app_cell_text_color"] isEmptyString])
        _tableViewCellTintColor = [UIColor colorFromHexString:self.ifbckModel.parameters[@"app_cell_text_color"]];
    
}

- (BOOL)prefersStatusBarHidden{
    return [self.navigationController prefersStatusBarHidden];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    float iPhoneScreenMultiplicator = [UIScreen mainScreen].bounds.size.height / 667;
    
    if(IDIOM!=IPAD && !resizedForIPhone){
        resizedForIPhone = YES;
        for(UIButton *button in _buttons){
            [button.titleLabel setFont:[UIFont fontWithName:button.titleLabel.font.fontName size:button.titleLabel.font.pointSize/4*2*iPhoneScreenMultiplicator]];
        }
        
        for(UILabel *label in _labels){
            [label setFont:[UIFont fontWithName:label.font.fontName size:label.font.pointSize/3*2*iPhoneScreenMultiplicator]];
        }
        
        for(NSLayoutConstraint *constraint in _constraintsToShrinkBy3of4){
            constraint.constant = constraint.constant/4*3*iPhoneScreenMultiplicator;
        }
        
        for(NSLayoutConstraint *constraint in _constraintsToShrinkBy1of2){
            constraint.constant = constraint.constant/2*1*iPhoneScreenMultiplicator;
        }

        for(NSLayoutConstraint *constraint in _constraintsToShrinkBy1of4){
            constraint.constant = constraint.constant/4*1*iPhoneScreenMultiplicator;
        }
        for(UITextView *textView in _textViews){
            [textView setFont:[UIFont fontWithName:textView.font.fontName size:textView.font.pointSize/4*3*iPhoneScreenMultiplicator]];
        }

    }
}

@end
