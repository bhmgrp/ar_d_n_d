//
//  Feedback+CoreDataProperties.h
//  native ifbck app
//
//  Created by User on 6/1/16.
//  Copyright © 2016 BHM Media Solutions. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Feedback.h"

NS_ASSUME_NONNULL_BEGIN

@interface Feedback (CoreDataProperties)

@property (nonatomic) int16_t campaignID;
@property (nullable, nonatomic, retain) id dynamicFieldsArray;
@property (nonatomic) int16_t entryID;
@property (nonatomic) int16_t entryPointType;
@property (nonatomic) int16_t languageID;
@property (nonatomic) int16_t placeID;
@property (nonatomic) BOOL sent;
@property (nullable, nonatomic, retain) id topicRatings;
@property (nonatomic) double tstamp;

@end

NS_ASSUME_NONNULL_END
