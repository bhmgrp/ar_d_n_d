//
//  OnboardingNavigationViewController.m
//  pickalbatros
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckOnboardingNavigationViewController.h"

@interface ifbckOnboardingNavigationViewController ()

@property (nonatomic) UIColor *textColor;

@property (nonatomic) NSMutableArray *navigationBarBackgroundViews;

@property (nonatomic) BOOL hideStatusBar;

@end

@implementation ifbckOnboardingNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _hideStatusBar = NO;
    
    NAVIGATION_BACKGROUND_HEIGHT = ifbckNavigationBarHeight;
    
    [self updateNavigationBarStyle];
    
    //[self.view addGestureRecognizer:[[SWRevealViewController alloc] init].panGestureRecognizer];
    
    _navigationBarBackgroundViews = [@[] mutableCopy];
}

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    self = [super initWithRootViewController:rootViewController];
    
    UIView *navigationBarBackground = [self getNavigationBarBackgroundView];
    
    [rootViewController.view addSubview:navigationBarBackground];
    
    [_navigationBarBackgroundViews addObject:navigationBarBackground];
    
    return self;
}

-(UIView*)getNavigationBarBackgroundView{
    
    CGRect backgroundFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, NAVIGATION_BACKGROUND_HEIGHT);
    
    UIView *navigationBarBackground = [[UIView alloc] initWithFrame:backgroundFrame];
    navigationBarBackground.backgroundColor = [UIColor colorWithRGBHex:0x4682BE];
    
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, navigationBarBackground.height, navigationBarBackground.width, 1.5f)];
    
    bar.backgroundColor = [UIColor colorWithRGBHex:0x4682BE];
    
    [navigationBarBackground addSubview:bar];
    
    return navigationBarBackground;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    if([_textColor colorIsLight])
        return UIStatusBarStyleLightContent;
    else
        return UIStatusBarStyleDefault;
}

-(BOOL)prefersStatusBarHidden{
    return _hideStatusBar;
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateNavigationBarStyle{
    
    _textColor = [UIColor colorWithRGBHex:0xffffff];
    
    //navigation Bar style
    [UINavigationBar appearance].barTintColor = [UIColor clearColor];
    
    self.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationBar.tintColor = _textColor;
    self.navigationBar.titleTextAttributes = @{
                                                  NSForegroundColorAttributeName: _textColor,
                                                  NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:21.0]
                                                  };
    
    
    self.navigationBar.translucent = YES;
    self.navigationBar.shadowImage = [[UIImage alloc]init];
    self.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
 
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    UIView *navigationBarBackground = [self getNavigationBarBackgroundView];
    
    [_navigationBarBackgroundViews addObject:navigationBarBackground];
    
    if(self.viewControllers.count>0){
        [viewController.view addSubview:navigationBarBackground];
    }
    
    //[self.view insertSubview:bluredEffectView belowSubview:self.navigationBar];
    [super pushViewController:viewController animated:animated];
}

-(void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated{
    [super setNavigationBarHidden:hidden animated:animated];
    [self hideNavigationBarBackground:hidden animated:animated];
}

-(void)hideNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated{
    if(hidden){
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, NAVIGATION_BACKGROUND_HEIGHT);
                }];
            }
            else {
                navigationBarBackground.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, NAVIGATION_BACKGROUND_HEIGHT);
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, NAVIGATION_BACKGROUND_HEIGHT);
                }];
            }
            else {
                navigationBarBackground.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, NAVIGATION_BACKGROUND_HEIGHT);
            }
        }
    }
}

-(void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated{
    if(hidden){
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 0.0;
                }];
            }
            else {
                navigationBarBackground.alpha = 0.0;
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 1.0;
                }];
            }
            else {
                navigationBarBackground.alpha = 1.0;
            }
        }
    }
}

-(void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated finished:(void(^)(void))finished{
    void(^finishedfinishedFunction)() = finished;
    
    if(hidden){
        NSLog(@"hide navigation background");
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 0.0;
                } completion:^(BOOL finished){
                    if(finished){
                        finishedfinishedFunction();
                    }
                }];
            }
            else {
                navigationBarBackground.alpha = 0.0;
            }
        }
    }
    else {
        for(UIView *navigationBarBackground in _navigationBarBackgroundViews){
            if(animated){
                [UIView animateWithDuration:0.2 animations:^(void){
                    navigationBarBackground.alpha = 1.0;
                } completion:^(BOOL finished){
                    finishedfinishedFunction();
                }];
            }
            else {
                navigationBarBackground.alpha = 1.0;
                [self setNavigationBarHidden:NO animated:NO];
            }
        }
    }
}

- (void)hideStatusBar:(BOOL)hide{
    _hideStatusBar = hide;
    NAVIGATION_BACKGROUND_HEIGHT = NAVIGATION_BACKGROUND_HEIGHT - 20;
        
    [self setNeedsStatusBarAppearanceUpdate];
}

@end