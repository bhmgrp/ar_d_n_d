//
//  InsertEntryPointViewController.m
//  if_framework_terminal
//
//  Created by User on 4/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckInsertEntrypointViewController.h"
#import "ifbckWebViewController.h"
#import "ifbck.h"

@interface ifbckInsertEntrypointViewController ()

@property (nonatomic) NSString *entry;

@end

@implementation ifbckInsertEntrypointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super registerForKeyboardNotifications];
    _iFeedbackLinkTextfield.delegate = self;
    
    if(IDIOM!=IPAD){
        _iFeedbackLinkTextfield.font = [UIFont fontWithName:_iFeedbackLinkTextfield.font.fontName size:_iFeedbackLinkTextfield.font.pointSize/3*2];
    }
 
    self.hintButtonLabel.attributedText = [self translateAttribString:self.hintButtonLabel.attributedText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_iFeedbackLinkTextfield resignFirstResponder];
}

- (IBAction)submitButton:(id)sender {
    NSString *iFeedbackLink = (NSString*)_iFeedbackLinkTextfield.text;
    if(![iFeedbackLink isEqualToString:@""]){
        [self showPopupForEntry:iFeedbackLink];
    }
    [self.view endEditing:YES];
    [self.iFeedbackLinkTextfield resignFirstResponder];
}

- (IBAction)hintButton:(id)sender {
    ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
    
    wvc.url = [NSURL URLWithString:@"https://ifbck.com/admin/"];
    wvc.urlGiven = YES;
    wvc.titleString = NSLocalizedStringFromTable(@"Dashboard", @"ifbckLocalizable", @"");
    
    [self.navigationController pushViewController:wvc animated:YES];
}

-(void)showPopupForEntry:(NSString*)entry{
    _entry = entry;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Advice",@"ifbckLocalizable", @"")
                                                    message:NSLocalizedStringFromTable(@"If you would like to use this app in a public area, we suggest you to activate the \"guided access\".",@"ifbckLocalizable", @"")
                                                   delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK",@"ifbckLocalizable", @"") otherButtonTitles:NSLocalizedStringFromTable(@"More information", @"ifbckLocalizable", @""), nil];
    alert.cancelButtonIndex = 1;
    
    [alert show];
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        [self loadiFeedback:_entry];
    }
    else {
        ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
        
        wvc.url = [NSURL URLWithString:NSLocalizedStringFromTable(@"GUIDED_ACCESS_STRING", @"ifbckLocalizable", @"")];
        wvc.titleString = NSLocalizedStringFromTable(@"Guided Access", @"ifbckLocalizable", @"");
        wvc.urlGiven = YES;
        
        UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(loadiFeedback)];
        
        wvc.navigationItem.rightBarButtonItem = doneBarButtonItem;
        
        [self.navigationController pushViewController:wvc animated:YES];
    }
}

-(void)loadiFeedback{
    [self loadiFeedback:_entry];
}

-(void)loadiFeedback:(NSString*)iFeedbackLink{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable", @"");
    
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardDefaults setObject:iFeedbackLink forKey:@"selectedEntryPoint"];
    
    [standardDefaults synchronize];
    
    [super.ifbckModel loadViewControllerForEntryPoint:iFeedbackLink];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textWillChange:(id<UITextInput>)textInput{
    
}

-(void)textDidChange:(id<UITextInput>)textInput{
    
}

-(void)selectionWillChange:(id<UITextInput>)textInput{
    
}

-(void)selectionDidChange:(id<UITextInput>)textInput{
    
}

-(NSAttributedString*)translateAttribString:(NSAttributedString*)attribString{
    NSMutableAttributedString *returnString = [[NSMutableAttributedString alloc]init];
    
    NSRange totalRange = NSMakeRange (0, attribString.length);
    
    [attribString enumerateAttributesInRange: totalRange options: 0 usingBlock: ^(NSDictionary *attributes, NSRange range, BOOL *stop)
     {
         NSLog (@"range: %@ attributes: %@", NSStringFromRange(range), attributes);
         
         NSString *string = [[attribString string] substringWithRange:range];
         
         NSLog(@"string at range %@", string);
         
         //Translate 'string' based on 'language' here.
         
         NSString *trans = NSLocalizedStringFromTable(string, @"ifbckInsertEntrypointViewController", @"");
         
         NSAttributedString *translatedString = [[NSAttributedString alloc]initWithString:trans attributes:attributes];
         
         [returnString appendAttributedString:translatedString];
         
     }];
    
    return returnString;
}

@end
