//
//  OnboardingViewController.m
//  if_framework_terminal
//
//  Created by User on 4/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckOnboardingViewController.h"

@interface ifbckOnboardingViewController (){
    BOOL resizedForIPhone, didLayout;
}

@property (nonatomic) CGRect originalFrame;

@end

@implementation ifbckOnboardingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _iconFrameView.layer.borderWidth = 2.0f;
    _iconFrameView.layer.borderColor = [[UIColor whiteColor] CGColor];
    _iconFrameView.layer.cornerRadius = 10.0f;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Back", @"ifbckLocalizable", @"") style:UIBarButtonItemStylePlain target:self action:nil];

    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLayoutSubviews{

    if(!didLayout){
        _originalFrame = self.view.frame;
        didLayout = YES;
    }


    float iPhoneScreenMultiplicator = [UIScreen mainScreen].bounds.size.height / 667;
    
    if(IDIOM!=IPAD && !resizedForIPhone){
        resizedForIPhone = YES;
        for(UIButton *button in _buttons){
            [button.titleLabel setFont:[UIFont fontWithName:button.titleLabel.font.fontName size:button.titleLabel.font.pointSize/3*2*iPhoneScreenMultiplicator]];
        }
        
        for(UILabel *label in _labels){
            [label setFont:[UIFont fontWithName:label.font.fontName size:label.font.pointSize/3*2*iPhoneScreenMultiplicator]];
        }
        
        for(NSLayoutConstraint *constraint in _constraintsToShrinkBy3of4){
            constraint.constant = constraint.constant/4*3*iPhoneScreenMultiplicator;
        }
        
        for(NSLayoutConstraint *constraint in _constraintsToShrinkBy1of2){
            constraint.constant = constraint.constant/2*1*iPhoneScreenMultiplicator;
        }
    }
    
    // resize template images
    UIImage *image;
    UIImage *newImage;
    CGSize scaledSize;
    
    for(UIImageView *imageView in _templateImages){
        image = imageView.image;
        scaledSize = CGSizeMake(imageView.width, imageView.height);
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
        [image drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        imageView.image = [newImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
}


#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(_originalFrame.origin.x, _originalFrame.origin.y, _originalFrame.size.width, _originalFrame.size.height - (kbSize.height)+ifbckParentFooterHeight);
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    //    NSDictionary* info = [aNotification userInfo];
    //
    //    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    //
    //    [UIView animateWithDuration:0.5 animations:^(void){
    //        self.navigationController.view.frame = CGRectMake(0, -(64+_headerView.height), [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) +_headerView.height+64);
    //    }];
    //
    //
    //    //self.headerView.height = 0;
    //    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = _originalFrame;
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}

@end
