//
//  OnboardingNavigationViewController.h
//  pickalbatros
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ifbckOnboardingNavigationViewController : UINavigationController{
    float NAVIGATION_BACKGROUND_HEIGHT;
}

- (void)updateNavigationBarStyle;

- (void)hideNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated;

- (void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated;

- (void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated finished:(void(^)(void))finished;

- (void)hideStatusBar:(BOOL)hide;

@end
