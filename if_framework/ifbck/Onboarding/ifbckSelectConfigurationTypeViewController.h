//
//  SelectConfigurationTypeViewController.h
//  if_framework_terminal
//
//  Created by User on 4/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ifbckOnboardingViewController.h"

@interface ifbckSelectConfigurationTypeViewController : ifbckOnboardingViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *pleaseSelectLabel;
@property (weak, nonatomic) IBOutlet UIButton *insertEntrypointButton;
@property (weak, nonatomic) IBOutlet UIButton *scanEntrypointButton;

@end
