//
//  TerminalLandingPageViewController.m
//  if_framework_terminal
//
//  Created by User on 4/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckLandingPageViewController.h"

#import "ifbckWebViewController.h"

#import "ifbckSelectConfigurationTypeViewController.h"

@interface ifbckLandingPageViewController ()

@end

@implementation ifbckLandingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self translateOutlets];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)translateOutlets{
    //self.title = NSLocalizedStringFromTable(@"WELCOME", @"landing page - welcome message");
}

- (IBAction)openSettingsViewController:(id)sender {
    ifbckSelectConfigurationTypeViewController *sctvc = [ifbckSelectConfigurationTypeViewController loadNowFromStoryboard:@"ifbckSelectConfigurationTypeViewController"];
    
    //sctvc.title = self.alreadyCustomerButton.currentTitle;
    
    [self.navigationController pushViewController:sctvc animated:YES];
}

- (IBAction)openRegisterViewController:(id)sender {
    ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
    
    wvc.url = [NSURL URLWithString:@"http://dev.ifbck.com/admin/user/registration-lite"];
    wvc.urlGiven = YES;
    wvc.titleString = [self.registerNewAccountButton.currentTitle uppercaseString];
    
    [self.navigationController pushViewController:wvc animated:YES];
}

- (IBAction)moreButtonPressed:(id)sender {
    ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
    
    wvc.url = [NSURL URLWithString:@"http://ifeedback.de?headerless=1"];
    wvc.urlGiven = YES;
    wvc.titleString = [self.moreButton.currentTitle uppercaseString];
    
    [self.navigationController pushViewController:wvc animated:YES];
}

@end
