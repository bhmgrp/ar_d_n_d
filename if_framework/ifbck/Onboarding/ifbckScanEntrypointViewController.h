//
//  ScanEntrypointViewController.h
//  if_framework_terminal
//
//  Created by User on 4/21/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckOnboardingViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ifbckScanEntrypointViewController : ifbckOnboardingViewController <AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UIView *footerView;

@end
