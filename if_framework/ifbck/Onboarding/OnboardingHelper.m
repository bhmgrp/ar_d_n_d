//
//  OnboardingHelper.m
//  if_framework_terminal
//
//  Created by User on 4/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "OnboardingHelper.h"


@implementation OnboardingHelper

+(UIViewController*)getOnboardingViewController{
    
    ifbckSelectConfigurationTypeViewController *scvc = [ifbckSelectConfigurationTypeViewController loadNowFromStoryboard:@"ifbckSelectConfigurationTypeViewController"];
    
    ifbckOnboardingSidebarViewController *osvc = [ifbckOnboardingSidebarViewController loadNowFromStoryboard:@"ifbckOnboardingSidebarViewController"];
    
    ifbckOnboardingNavigationViewController *onvc = [[ifbckOnboardingNavigationViewController alloc] initWithRootViewController:scvc];
    
    
    [scvc setSidebarButton];
    
    
    return [[SWRevealViewController alloc] initWithRearViewController:osvc frontViewController:onvc];
}

+(UIViewController*)getOnboardingViewControllerForIfbck:(ifbck*)ifbck{

    ifbckSelectConfigurationTypeViewController *scvc = [ifbckSelectConfigurationTypeViewController loadNowFromStoryboard:@"ifbckSelectConfigurationTypeViewController"];
    scvc.ifbckModel = ifbck;

    ifbckOnboardingSidebarViewController *osvc = [ifbckOnboardingSidebarViewController loadNowFromStoryboard:@"ifbckOnboardingSidebarViewController"];

    ifbckOnboardingNavigationViewController *onvc = [[ifbckOnboardingNavigationViewController alloc] initWithRootViewController:scvc];


    [scvc setSidebarButton];


    return [[SWRevealViewController alloc] initWithRearViewController:osvc frontViewController:onvc];
}

@end
