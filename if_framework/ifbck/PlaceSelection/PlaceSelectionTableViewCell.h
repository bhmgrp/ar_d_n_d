//
//  PlaceSelectionTableViewCell.h
//  if_framework_terminal
//
//  Created by User on 5/23/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iFeedbackTableViewCell.h"

@interface PlaceSelectionTableViewCell : iFeedbackTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *headerImage;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSpaceHeaderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSpaceNameLabel;


@end
