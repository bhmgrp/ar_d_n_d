//
//  PlaceSelectionViewController.m
//  if_framework_terminal
//
//  Created by User on 5/23/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "PlaceSelectionViewController.h"
#import "PlaceSelectionTableViewCell.h"
#import "ifbckNavigationViewController.h"

#import "CampaignSelectionViewController.h"

@interface PlaceSelectionViewController (){
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) NSString *languageKey;

@end

@implementation PlaceSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    if(super.ifbckModel.place[@"header"] != nil && ![super.ifbckModel.place[@"header"] isEmptyString]){
        self.title = super.ifbckModel.place[@"header"];
    }
    else {
        self.title = [super.ifbckModel getTranslationForKey:@"choose_location"];
    }
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:nil action:nil]];

    [self initNavigationBarButtons];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    super.ifbckModel.place = _place;
    super.ifbckModel.placeID = [_place[@"uid"] intValue];
    super.ifbckModel.parameters = [_place[@"parameters"] isKindOfClass:[NSDictionary class]]?_place[@"parameters"]:@{};
    
    super.ifbckModel.topicRatings = [@{} mutableCopy];

    
    if(_languageKey!=nil){
        if(![_languageKey isEqualToString:[languages currentLanguageKey]]){
            _languageKey = [languages currentLanguageKey];
            
            [super.ifbckModel loadClient];
            
            [self initNavigationBarButtons];
        }
    }
    else {
        _languageKey = [languages currentLanguageKey];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
    [((ifbckNavigationViewController*)self.navigationController) removeTopicFooter];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = super.tableViewBackgroundColor;
}

- (void)viewDidLayoutSubviews {
    [self initFooter];
}

- (void)initFooter{
    
    // footer background
    if(_place[@"footer_bgcolor"]!=nil && ![_place[@"footer_bgcolor"] isEmptyString]){
        _footerBackgroundView.backgroundColor = [UIColor colorFromHexString:_place[@"footer_bgcolor"]];
    }
    else {
        _footerBackgroundView.hidden = YES;
        _footerBackgroundView.backgroundColor = [UIColor clearColor];
    }
    
    // footer logo
    if(_place[@"footer_logo"]!=nil && ![_place[@"footer_logo"] isEmptyString]){
        // load image in background
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            UIImage *footerLogoImage = [imageMethods UIImageWithServerPathName:_place[@"footer_logo"] versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)super.ifbckModel.pID,350] width:350];
            
            // update UI in main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                _footerImage.image = footerLogoImage;
            });
        });
    }
    else {
        _footerImage.hidden = YES;
    }
    
    //_tableView.contentInset = UIEdgeInsetsMake(0, 0, footerHeight, 0);
    //_tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, footerHeight, 0);
}

- (void)initNavigationBarButtons{
    // right bar button item
    NSArray *languagesArray = [(NSString*)super.ifbckModel.parameters[@"languages"] componentsSeparatedByString:@";"];
    if(languagesArray.count>1){
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Language" style:UIBarButtonItemStylePlain target:self action:@selector(openLanguagesViewController)];
        NSString *flagKey = [languages currentLanguageKey];
        
        [_languageSelectorButton addTarget:self action:@selector(openLanguagesViewController) forControlEvents:UIControlEventTouchUpInside];
        
        _languageSelectorImageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.gif",flagKey]];;
        
    }
}

- (void)openLanguagesViewController{
    [self.navigationController pushViewController:[super.ifbckModel getLanguageSelectViewController] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0)
        return;
    
    NSDictionary *selectedPlace = _places[indexPath.row-1];
    
    super.ifbckModel.place = selectedPlace;
    super.ifbckModel.placeID = [selectedPlace[@"uid"] intValue];
    super.ifbckModel.parameters = selectedPlace[@"parameters"];
    
    if([selectedPlace[@"children"] isKindOfClass:[NSArray class]] && [selectedPlace[@"children"] count] > 0){
        PlaceSelectionViewController *psvc = [PlaceSelectionViewController loadNowFromStoryboard:@"PlaceSelectionViewController"];
        psvc.place = selectedPlace;
        psvc.places = selectedPlace[@"children"];
        psvc.ifbckModel = super.ifbckModel;
        [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
        
        [self.navigationController pushViewController:psvc animated:YES];
    }
    else if([selectedPlace[@"campaigns"] isKindOfClass:[NSArray class]]){
        if([selectedPlace[@"campaigns"] count] == 1){
            super.ifbckModel.campaign = selectedPlace[@"campaigns"][0];
            super.ifbckModel.campaignID = [super.ifbckModel.campaign[@"uid"] intValue];
            super.ifbckModel.surveyTopics = super.ifbckModel.campaign[@"survey_topics"];
            super.ifbckModel.parameters = super.ifbckModel.campaign[@"parameters"];
            super.ifbckModel.dynamicFields = super.ifbckModel.campaign[@"dynamicfields"];
            super.ifbckModel.parameters = super.ifbckModel.campaign[@"parameters"];
            
            [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
            
            if(super.ifbckModel.parameters[@"skip_to_contactform"] != nil &&
               [super.ifbckModel.parameters[@"skip_to_contactform"] isKindOfClass:[NSString class]] &&
               [super.ifbckModel.parameters[@"skip_to_contactform"] isEqualToString:@"yes"]){
                [self.navigationController pushViewController:[super.ifbckModel getDetailViewController] animated:YES];
            }
            else {
                TopicListViewController *vc = (TopicListViewController*)[super.ifbckModel getTopicViewController];
                
                [self.navigationController pushViewController:vc animated:YES];
                
                [vc initRootViewController];
            }
        }
        else if([selectedPlace[@"campaigns"] count] > 1){
            super.ifbckModel.campaigns = selectedPlace[@"campaigns"];
            
            [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
            
            [self.navigationController pushViewController:[super.ifbckModel getCampaignSelectViewController] animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        if(IDIOM==IPAD){
            return 164.0f * screenMultiplicator-130+45;
        }
        
        return 164.0f * screenMultiplicator+45;
    }
    
    if(IDIOM==IPAD){
        return 90;
    }
    
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _places.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        PlaceSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        
        cell.separatorInset = UIEdgeInsetsMake(0, screenWidth, 500, 0);
        
        cell.tag = indexPath.row-1;
        
        if(super.ifbckModel.parameters[@"app_header_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_header_text_color"] isEmptyString]){
            //cell.headerLabel.textColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_header_text_color"]];
        }
        
        // if we have a custom header view set
        if(super.ifbckModel.parameters[@"app_header_image"]!=nil && ![super.ifbckModel.parameters[@"app_header_image"] isEmptyString]){
            // load the image in the background
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
                UIImage *image = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_header_image"] versionIdentifier:[NSString stringWithFormat:@"pid%li",(long)super.ifbckModel.pID] width:screenWidth];
                
                if(image && cell.tag == indexPath.row-1){
                    // update cell in main queue
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.headerImage.image = image;
                        [cell setNeedsLayout];
                    });
                }
            });
        }
        else {
            // else set the default header view
            _headerView.width = screenWidth;
            _headerView.height = [self tableView:tableView heightForRowAtIndexPath:indexPath];
            
            [cell addSubview:_headerView];
        }
        
        cell.headerLabel.text = super.ifbckModel.place[@"description"];
        
        return cell;
    }
    
    PlaceSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"placeCell"];
    
    cell.tag = indexPath.row-1;
    
    cell.backgroundColor = super.tableViewCellBackgroundColor;
    cell.nameLabel.textColor = super.tableViewCellTintColor;
    cell.nameLabel.text = _places[indexPath.row-1][@"name"];
    
    
    if(indexPath.row-1 == _places.count-1){
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    else {
        if(IDIOM!=IPAD)
            cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        else
            cell.separatorInset = UIEdgeInsetsMake(0, 45, 0, 0);
    }
    
    return cell;
}

@end
