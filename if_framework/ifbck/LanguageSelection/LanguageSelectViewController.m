//
//  LanguageSelectViewController.m
//  if_framework_terminal
//
//  Created by User on 2/17/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "LanguageSelectViewController.h"
#import "LanguageSelectTableViewCell.h"
#import "ifbckNavigationViewController.h"
#import "TopicListViewController.h"

@interface LanguageSelectViewController (){
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) id<iFeedbackModelDelegate> oldDelegate;

@end

@implementation LanguageSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    _languages = @[];
    
    [self loadLanguageTable];
    
    [self.tableView setBackgroundColor:super.tableViewBackgroundColor];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = [super.ifbckModel getTranslationForKey:@"pi1_langselect_title"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadLanguageTable{
    
    NSDictionary *params = super.ifbckModel.parameters;
    
    NSString *languagesString = params[@"languages"];
    
    if(
       languagesString!=nil &&
       [languagesString isKindOfClass:[NSString class]] &&
       ![languagesString isEqualToString:@""]
       ){
        NSMutableArray *tmpLanguages = [@[] mutableCopy];
        
        NSArray *paramLanguages = [languagesString componentsSeparatedByString:@";"];
        
        for (NSString *language in paramLanguages){
            [tmpLanguages addObject:
             @{
               @"languageKey":[language.lowercaseString substringToIndex:2],
               @"flagKey":[language.lowercaseString substringFromIndex: language.length - 2],
               }
             ];
        }
        
        _languages = tmpLanguages;
    }
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
}

#pragma mark - TableView datasource and delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row==0)
        return;
    
    super.ifbckModel.languageSelected = YES;
    if(self.navigationController.viewControllers[0]!=self){
        [languages setLanguage:(NSString *)_languages[indexPath.row-1][@"languageKey"]];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        if([[languages currentLanguageKey] isEqualToString:_languages[indexPath.row-1][@"languageKey"]]){
            [self.navigationController pushViewController:super.ifbckModel.startViewController animated:YES];
        
            if([super.ifbckModel.startViewController isKindOfClass:[TopicListViewController class]])
                [((TopicListViewController*)super.ifbckModel.startViewController) initRootViewController];
        }
        else {
            [languages setLanguage:(NSString *)_languages[indexPath.row-1][@"languageKey"]];
            self.oldDelegate = super.ifbckModel.delegate;
            super.ifbckModel.delegate = self;
            [super.ifbckModel loadClient];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        if(IDIOM==IPAD){
            return 164.0f * screenMultiplicator-130+45;
        }
        
        return 164.0f * screenMultiplicator+45;
    }
    
    if(IDIOM==IPAD){
        return 90;
    }
    
    return 60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _languages.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LanguageSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"languageCell"];
    
    if(indexPath.row==0){
        LanguageSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, screenWidth, 500, 0);
        
        cell.tag = indexPath.row-1;
        
        if(super.ifbckModel.parameters[@"app_header_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_header_text_color"] isEmptyString]){
            //cell.headerLabel.textColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_header_text_color"]];
        }
        
        // if we have a custom header view set
        if(super.ifbckModel.parameters[@"app_header_image"]!=nil && ![super.ifbckModel.parameters[@"app_header_image"] isEmptyString]){
            // load the image in the background
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
                UIImage *image = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_header_image"] versionIdentifier:[NSString stringWithFormat:@"pid%li",(long)super.ifbckModel.pID] width:screenWidth];
                
                if(image && cell.tag == indexPath.row-1){
                    // update cell in main queue
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.headerImage.image = image;
                        [cell setNeedsLayout];
                    });
                }
            });
        }
        else {
            // else set the default header view
            _headerView.width = screenWidth;
            _headerView.height = [self tableView:tableView heightForRowAtIndexPath:indexPath];
            
            [cell addSubview:_headerView];
        }
        
        cell.headerLabel.text = [super.ifbckModel getTranslationForKey:@"pi1_langselect_headline"];
        
        return cell;
    }
    
    NSString *languageKey = _languages[indexPath.row-1][@"languageKey"];
    NSString *flagKey = _languages[indexPath.row-1][@"flagKey"];
    
    cell.labelLanguageDescription.text = [super.ifbckModel getTranslationForKey:[NSString stringWithFormat:@"pi1_langselect_%@",languageKey]];
    cell.imageLanguageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.gif",flagKey]];
    
    cell.imageLanguageFlag.layer.borderWidth = 1.0f;
    cell.imageLanguageFlag.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.imageLanguageFlag.layer.cornerRadius = 3.0f;
    
    cell.backgroundColor = super.tableViewCellBackgroundColor;
    cell.labelLanguageDescription.textColor = super.tableViewCellTintColor;
    
    return cell;
}

-(void)iFeedbackModelDidLoadData{
    [super.ifbckModel loadIFeedbackViewController];
    [self.navigationController pushViewController:super.ifbckModel.startViewController animated:YES];
    if([super.ifbckModel.startViewController isKindOfClass:[TopicListViewController class]])
        [((TopicListViewController*)super.ifbckModel.startViewController) initRootViewController];
    
    super.ifbckModel.languageSelected = NO;
    super.ifbckModel.delegate = self.oldDelegate;
}

@end
