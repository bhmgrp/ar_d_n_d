//
//  LanguageSelectTableViewCell.m
//  if_framework_terminal
//
//  Created by User on 2/17/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "LanguageSelectTableViewCell.h"

@implementation LanguageSelectTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    if(IDIOM!=IPAD || IFBCKINTEGRATED){
        [self resizeFontFor:_labelLanguageDescription];
        [self resizeFontFor:_headerLabel];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
