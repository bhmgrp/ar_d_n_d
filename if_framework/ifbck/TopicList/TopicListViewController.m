//
//  TopicListViewController.m
//  if_framework_terminal
//
//  Created by User on 4/18/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "TopicListViewController.h"
#import "TopicListTableViewCell.h"

#import "ifbckNavigationViewController.h"

#import "SWRevealViewController.h"

#import "HCSStarRatingView.h"

@interface TopicListViewController (){
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) NSString *languageKey;

@property (nonatomic) NSMutableDictionary *childTopicValues;

@end


@implementation TopicListViewController

+(TopicListViewController*)newRootList{
    TopicListViewController *vc = [TopicListViewController loadNowFromStoryboard:@"TopicListViewController"];
    
    vc.isRoot = YES;
    
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    
    
    if(IDIOM!=IPAD)
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14.0]} forState:UIControlStateNormal];
    
    
    [self.tableView setBackgroundColor:super.tableViewBackgroundColor];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:self.title style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    self.title = [self.title uppercaseString];
}

- (void)viewDidLayoutSubviews{
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, _footerHeight, 0);
    _tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _footerHeight, 0);
    
    UIView *okButtonFrame = ((ifbckNavigationViewController*)self.navigationController).topicOkButtonWrapper;
    if(!okButtonFrame || okButtonFrame.hidden){
        _tableView.contentInset = UIEdgeInsetsMake(0, -0, _footerHeight-okButtonFrame.height+0, 0);
        _tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _footerHeight-okButtonFrame.height+0, 0);
    }
}

- (void)initRootViewController{

    [((ifbckNavigationViewController*)self.navigationController) setTopicOkButtonWrapper:nil];
    [((ifbckNavigationViewController*)self.navigationController) setFooterToolbar:nil];
    [((ifbckNavigationViewController*)self.navigationController) setRootTopicListViewController:self];
    [((ifbckNavigationViewController*)self.navigationController) initFooterForTopics:_topics];
    [((ifbckNavigationViewController*)self.navigationController) initHeaderView];
    [((ifbckNavigationViewController*)self.navigationController) updateNavigationBarStyle];
    
    _footerHeight = ((ifbckNavigationViewController*)self.navigationController).footerHeight;

    [self initNavigationBarButtons];
    [self initStarImages];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(_rowSelected){
        _rowSelected = NO;
        //[_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_selectedRow+1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    if(_languageKey!=nil){
        if(![_languageKey isEqualToString:[languages currentLanguageKey]]){
            _languageKey = [languages currentLanguageKey];
            
            [super.ifbckModel loadClient];
            
            [self initNavigationBarButtons];
        }
    }
    else {
        _languageKey = [languages currentLanguageKey];
    }
    
    //[((ifbckNavigationViewController*)self.navigationController) showTopicFooter];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if(super.ifbckModel.topicRatings.count>0 && !_isRoot){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[super.ifbckModel getTranslationForKey:@"pi1_button_closing_fb_short"]
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:((ifbckNavigationViewController*)self.navigationController)
                                                                                 action:@selector(openDynamicForm)];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:[NSString stringWithFormat:@"  %@  ", [super.ifbckModel getTranslationForKey:@"pi1_button_closing_fb_short"]] forState:UIControlStateNormal];
        [button setTitleColor:((ifbckNavigationViewController*)self.navigationController).textColor forState:UIControlStateNormal];
        [button sizeToFit];
        [button addTarget:(ifbckNavigationViewController*)self.navigationController action:@selector(openDynamicForm) forControlEvents:UIControlEventTouchUpInside];
        
        button.layer.cornerRadius = 3.5f;
        button.layer.borderWidth = 1.0f;
        button.layer.borderColor = ((ifbckNavigationViewController*)self.navigationController).textColor.CGColor;
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    else if(!_isRoot){
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)initStarImages{
    
}

- (void)initNavigationBarButtons{
    // right bar button item
    NSArray *languagesArray = [(NSString*)super.ifbckModel.parameters[@"languages"] componentsSeparatedByString:@";"];
    if(languagesArray.count>1){
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Language" style:UIBarButtonItemStylePlain target:self action:@selector(openLanguagesViewController)];
        NSString *flagKey = [languages currentLanguageKey];
        
        [_languageSelectorButton addTarget:self action:@selector(openLanguagesViewController) forControlEvents:UIControlEventTouchUpInside];
        
        _languageSelectorImageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.gif",flagKey]];;
        
    }
    
    self.navigationItem.title = super.ifbckModel.place[@"name"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openLanguagesViewController{
    [self.navigationController pushViewController:[super.ifbckModel getLanguageSelectViewController] animated:YES];
}

#pragma mark - helper

- (UIViewController *)getViewControllerForIndex:(NSInteger)index{
    NSString *indexString = [NSString stringWithFormat:@"%@", _topics[index][@"uid"]];
    if(super.ifbckModel.topicPages[indexString]!=nil){
        return super.ifbckModel.topicPages[indexString];
    }
    
    if(((NSArray*)_topics[index][@"children"]).count > 0 ){
        TopicListViewController *tlvc = [TopicListViewController loadNowFromStoryboard:@"TopicListViewController"];
        
        tlvc.topics = [_topics[index][@"children"] mutableCopy];
        
        tlvc.title = _topics[index][@"name"];
        
        tlvc.delegate = self;
        
        tlvc.footerHeight = self.footerHeight;

        tlvc.ifbckModel = super.ifbckModel;
        
        super.ifbckModel.topicPages[indexString] = tlvc;
        
        return tlvc;
    }
    else {
        TopicDetailViewController *tdvc = [TopicDetailViewController loadNowFromStoryboard:@"TopicDetailViewController"];
        
        tdvc.topic = [_topics[index] mutableCopy];
        
        tdvc.title = _topics[index][@"name"];
        
        tdvc.delegate = self;

        tdvc.ifbckModel = super.ifbckModel;
        
        super.ifbckModel.topicPages[indexString] = tdvc;
        
        return tdvc;
    }
}

#pragma mark - TableView datasource and delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0)
        return;
    
    _rowSelected = YES;
    _selectedRow = indexPath.row-1;
    
    
    [self.navigationController pushViewController:[self getViewControllerForIndex:indexPath.row-1] animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        if(IDIOM==IPAD){
            return 164.0f * screenMultiplicator-130+45;
        }
        
        return 164.0f * screenMultiplicator+45;
    }
    
    if(IDIOM==IPAD){
        return 90;
    }
    
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _topics.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        TopicListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        
        cell.separatorInset = UIEdgeInsetsMake(0, screenWidth, 500, 0);
        
        cell.headerQuestionLabel.text = super.ifbckModel.campaign[@"root_topic_question"];
        
        cell.tag = indexPath.row-1;
        
        if(super.ifbckModel.parameters[@"app_header_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_header_text_color"] isEmptyString]){
            //cell.headerQuestionLabel.textColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_header_text_color"]];
        }
        
        // if we have a custom header view set
        if(super.ifbckModel.parameters[@"app_header_image"]!=nil && ![super.ifbckModel.parameters[@"app_header_image"] isEmptyString]){
            // load the image in the background
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
                UIImage *image = [imageMethods UIImageWithServerPathName:super.ifbckModel.parameters[@"app_header_image"] versionIdentifier:[NSString stringWithFormat:@"pid%li",(long)super.ifbckModel.pID] width:screenWidth];
                
                if(image && cell.tag == indexPath.row-1){
                    // update cell in main queue
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.headerImage.image = image;
                        [cell setNeedsLayout];
                    });
                }
            });
        }
        else {
            // else set the default header view
            _headerView.width = screenWidth;
            _headerView.height = [self tableView:tableView heightForRowAtIndexPath:indexPath];
            
            [cell addSubview:_headerView];
        }
        
        return cell;
    }
    
    TopicListTableViewCell *cell;
    
    
    if(_topics[indexPath.row-1][@"image_sm"]!= nil && ![_topics[indexPath.row-1][@"image_sm"] isEmptyString]){
        cell = [tableView dequeueReusableCellWithIdentifier:@"topicCellImage"];
        
        cell.tag = indexPath.row-1;
        
        NSMutableString *imageString = [_topics[indexPath.row-1][@"image_sm"] mutableCopy];
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                      @"(\\_[0-9]+\\.png)" options:0 error:nil];
        
        [regex replaceMatchesInString:imageString options:0 range:NSMakeRange(0, [imageString length]) withTemplate:@".png"];
        
        // load image in background
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
            UIImage *image = [imageMethods UIImageWithServerPathName:[[NSString alloc] initWithFormat:@"files/icons/%@",imageString] versionIdentifier:[NSString stringWithFormat:@"pid%liw%i",(long)super.ifbckModel.pID,60] width:60];
            if(image && cell.tag == indexPath.row-1)
                // update cell in main queue
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.iconImageView.image = [image imageWithRenderingMode:UIImageRenderingModeAutomatic];
                    cell.iconImageView.tintColor = super.tableViewCellTintColor;
                    [cell setNeedsLayout];
                });
        });
        
        if(indexPath.row-1 == _topics.count-1){
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        else {
            if(IDIOM!=IPAD)
                cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
            else
                cell.separatorInset = UIEdgeInsetsMake(0, 45, 0, 0);
        }
        
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"topicCell"];
        
        if(indexPath.row-1 == _topics.count-1){
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        else {
            if(IDIOM!=IPAD)
                cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
            else
                cell.separatorInset = UIEdgeInsetsMake(0, 45, 0, 0);
        }
    }
    
    cell.backgroundColor = super.tableViewCellBackgroundColor;
    cell.nameLabel.textColor = super.tableViewCellTintColor;
    cell.nameLabel.text = _topics[indexPath.row-1][@"name"];
    
    if([_topics[indexPath.row-1][@"value"] intValue]>0){
        cell.starRatingWrapper.hidden = NO;
        cell.starRatingView.value = round([_topics[indexPath.row-1][@"value"] floatValue]);
    }
    else {
        cell.starRatingWrapper.hidden = YES;
    }
    

    
    return cell;
}

#pragma mark -

- (void)starRatingValueDidChange:(HCSStarRatingView *)starRatingView{
    
    _topics = [_topics mutableCopy];
    _topics[_selectedRow] = [_topics[_selectedRow] mutableCopy];
    
    
    _topics[_selectedRow][@"value"] = @(starRatingView.value);
    
    if([_delegate respondsToSelector:@selector(numOfTopicsChanged:)]){
        [_delegate numOfTopicsChanged:0];
    }
    
    
    if([_delegate respondsToSelector:@selector(averageRatingChanged:)]){
        [_delegate averageRatingChanged:[self getAverageRating]];
    }
    
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_selectedRow+1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)averageRatingChanged:(float)averageRating{
    if([_topics[_selectedRow][@"value"] floatValue] > averageRating || [_topics[_selectedRow][@"value"] floatValue] < averageRating){
        
        _topics = [_topics mutableCopy];
        _topics[_selectedRow] = [_topics[_selectedRow] mutableCopy];
        
        
        _topics[_selectedRow][@"value"] = @(averageRating);
        
        if([_delegate respondsToSelector:@selector(averageRatingChanged:)]){
            [_delegate averageRatingChanged:[self getAverageRating]];
        }
        
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_selectedRow+1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)numOfTopicsChanged:(int)numOfTopics{
    if(_isRoot){
        int uid = [_topics[_selectedRow][@"uid"] intValue];
        
        [self getChildTopicValuesForID:uid topics:_topics[_selectedRow][@"children"]];
        
        long int numOfTopics = [_childTopicValues[[NSString stringWithFormat:@"%i",uid]] count];
        
        if(numOfTopics){
            [((ifbckNavigationViewController*)self.navigationController) updateBadge:[NSString stringWithFormat:@"%li",numOfTopics] ForIndex:_selectedRow];
        }
        else {
            [((ifbckNavigationViewController*)self.navigationController) updateBadge:nil ForIndex:_selectedRow];
        }
    }
    else if([_delegate respondsToSelector:@selector(numOfTopicsChanged:)]){
        [_delegate numOfTopicsChanged:numOfTopics];
    }
}

-(void)getChildTopicValuesForID:(int)uid topics:(NSArray*)topics{
    if(_childTopicValues==nil)
        _childTopicValues = [[NSMutableDictionary alloc] init];
    
    if(_childTopicValues[[NSString stringWithFormat:@"%i",uid]]==nil)
        _childTopicValues[[NSString stringWithFormat:@"%i",uid]] = [[NSMutableDictionary alloc] init];
    
    for(NSDictionary *topic in topics){
        if(super.ifbckModel.topicRatings[topic[@"uid"]]!=nil)
            _childTopicValues[[NSString stringWithFormat:@"%i",uid]][topic[@"uid"]] = @1;
        else
            [_childTopicValues[[NSString stringWithFormat:@"%i",uid]] removeObjectForKey:topic[@"uid"]];
        
        
        [self getChildTopicValuesForID:uid topics:topic[@"children"]];
    }
}

- (float)getAverageRating{
    float ratings = 0.0f;
    float rating = 0.0f;
    
    for(NSDictionary* topic in _topics){
        if([topic[@"value"] floatValue] > 0){
            ratings++;
            rating += [topic[@"value"] floatValue];
        }
    }
    
    if(ratings > 0){
        return rating/ratings;
    }
    
    return 0.0f;
}

@end
