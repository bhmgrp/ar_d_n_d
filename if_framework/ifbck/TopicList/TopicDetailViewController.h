//
//  TopicDetailViewController.h
//  if_framework_terminal
//
//  Created by User on 4/18/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackViewController.h"

#import "HCSStarRatingView.h"

@protocol TopicDetailDelegate<NSObject>
-(void)starRatingValueDidChange:(HCSStarRatingView*)starRatingView;
@end

@interface TopicDetailViewController : iFeedbackViewController <UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *topicNameLabel;

@property (weak, nonatomic) IBOutlet UIView *starRatingWrapper;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@property (weak, nonatomic) IBOutlet UILabel *starRatingSelectedValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *starRatingCommentDescription;

@property (weak, nonatomic) IBOutlet UITextView *starRatingCommentTextfield;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starRatingCommentTextfieldHeight;

@property (weak, nonatomic) IBOutlet UITextView *starRatingCommentDescriptionTest;

@property (weak, nonatomic) IBOutlet UIView *topicOkButtonWrapper;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicOkButtonWrapperBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicOkButtonHeight;
@property (weak, nonatomic) IBOutlet UIView *topicOkButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;


@property (nonatomic) id<TopicDetailDelegate> delegate;

@property (nonatomic) NSArray *starValueDescriptions;

@property (nonatomic) NSMutableDictionary *topic;


- (IBAction)okButtonPressed:(id)sender;

@end
