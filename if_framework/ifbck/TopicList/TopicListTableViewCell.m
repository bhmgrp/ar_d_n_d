//
//  TopicListTableViewCell.m
//  if_framework_terminal
//
//  Created by User on 4/18/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "TopicListTableViewCell.h"

@implementation TopicListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if(IDIOM!=IPAD || IFBCKINTEGRATED){
        _nameLabel.font = [UIFont fontWithName:_nameLabel.font.fontName size:17.0f];
        _headerQuestionLabel.font = [UIFont fontWithName:_headerQuestionLabel.font.fontName size:19.0f];
        
        _leftSpaceNameLabel.constant = 15;
        _leftSpaceHeaderLabel.constant = 15;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
