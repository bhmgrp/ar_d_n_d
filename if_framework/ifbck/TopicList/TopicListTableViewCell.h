//
//  TopicListTableViewCell.h
//  if_framework_terminal
//
//  Created by User on 4/18/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface TopicListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIView *starRatingWrapper;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;


@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UILabel *headerQuestionLabel;

@property (weak, nonatomic) IBOutlet UIView *separatorView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSpaceHeaderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSpaceNameLabel;

@end
