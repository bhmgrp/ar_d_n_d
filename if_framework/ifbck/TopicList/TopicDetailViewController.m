//
//  TopicDetailViewController.m
//  if_framework_terminal
//
//  Created by User on 4/18/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "TopicDetailViewController.h"


@interface TopicDetailViewController (){
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) NSString *textFieldPlaceHolder;
@property (nonatomic) UIColor *placeHolderColor;

@property (nonatomic) float savedValue;

@property (nonatomic) CGRect originalViewFrame;

@property (nonatomic) BOOL didLayoutSubviews;

@end

@implementation TopicDetailViewController

- (void)viewDidLoad {
    _topic = [_topic mutableCopy];
    [super viewDidLoad];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:self.title style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    self.title = [self.title uppercaseString];
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    // adding self as target for starrating changes
    [_starRatingView addTarget:self action:@selector(starRatingValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_starRatingView addTarget:self action:@selector(starRatingValueChanged:) forControlEvents:UIControlEventTouchDragInside];
    [_starRatingView addTarget:self action:@selector(starRatingValueChanged:) forControlEvents:UIControlEventTouchDragOutside];
    [_starRatingView addTarget:self action:@selector(starRatingValueDidChange:) forControlEvents:UIControlEventTouchUpOutside];
    [_starRatingView addTarget:self action:@selector(starRatingValueDidChange:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *questionString = [NSString stringWithFormat:@"<span style='font-size:28px;font-family:HelveticaNeue'><center>%@<center></span>",_topic[@"question"]];
    
    if(IDIOM!=IPAD)
        questionString = [NSString stringWithFormat:@"<span style='font-size:17px;font-family:HelveticaNeue'><center>%@<center></span>",_topic[@"question"]];
    
    // defining text to use for the question
    NSMutableAttributedString *attributedDescription;
    if(![_topic[@"question"] isEmptyString])
        attributedDescription = [[NSMutableAttributedString alloc] initWithData:[questionString dataUsingEncoding:NSUTF8StringEncoding]
                                                                  options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}
                                                       documentAttributes:nil
                                                                    error:nil];
    else
        attributedDescription = [[NSMutableAttributedString alloc] initWithString:[super.ifbckModel getTranslationForKey:@"pi1_rating_message_question"]];
    
    // trim whitespaces at the end of our attributd string
    NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSRange range;
    range = [attributedDescription.string rangeOfCharacterFromSet:charSet
                                              options:NSBackwardsSearch];
    while (range.length != 0 && NSMaxRange(range) == attributedDescription.length)
    {
        [attributedDescription replaceCharactersInRange:range withString:@""];
        range = [attributedDescription.string rangeOfCharacterFromSet:charSet options:NSBackwardsSearch];
    }
    
    // assigning texts to the outlets
    _starRatingCommentDescription.attributedText = attributedDescription;
    _starRatingCommentTextfield.text = _textFieldPlaceHolder = [super.ifbckModel getTranslationForKey:@"pi1_rating_message_placeholder"];
    _starRatingCommentTextfield.layer.cornerRadius = 5.0f;
    _starRatingCommentTextfield.layer.borderWidth = 1.5f;
    _starRatingCommentTextfield.layer.borderColor = [[UIColor grayColor] CGColor];
    _starValueDescriptions = [_topic[@"rating_strings"] componentsSeparatedByString:@";"];
    _starRatingSelectedValueLabel.text = [self getStarRatingValueDescription:(int)_starRatingView.value];
    
    // others
    _starRatingCommentTextfield.delegate = self;
    
    _placeHolderColor = [UIColor colorWithRGBHex:0xBBBAC2];
    
    [_okButton setTitle:[super.ifbckModel getTranslationForKey:@"pi1_rating_submit_btn"] forState:UIControlStateNormal];
    
    if(IDIOM!=IPAD)
        [_okButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    
    [self registerForKeyboardNotifications];
    [self updateSendbuttonStyle];

    _topicOkButtonWrapperBottomSpace.constant = 0;
    if(IDIOM!=IPAD){
        _topicOkButtonHeight.constant = 64;
    }

    UIGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:_starRatingCommentTextfield action:@selector(resignFirstResponder)];
    [self.view addGestureRecognizer:tab];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    if(!_didLayoutSubviews){
        _originalViewFrame = self.view.frame;
        _didLayoutSubviews = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [self saveTopic];
    
    [super viewWillDisappear:animated];
    
    [self.starRatingCommentTextfield resignFirstResponder];
    [self.view endEditing:YES];
    self.navigationController.view.frame = self.originalViewFrame;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.starRatingCommentTextfield resignFirstResponder];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)starRatingValueChanged:(HCSStarRatingView*)starRatingView{
    _starRatingSelectedValueLabel.text = [self getStarRatingValueDescription:(int)starRatingView.value];

}

-(void)starRatingValueDidChange:(HCSStarRatingView*)starRatingView{
    [self saveTopic];
}

-(NSString*)getStarRatingValueDescription:(int)value{
    if(_starValueDescriptions != nil && _starValueDescriptions.count==7 && ![_starValueDescriptions[value] isEmptyString]){
        return _starValueDescriptions[value];
    }
    return [super.ifbckModel getTranslationForKey:[NSString stringWithFormat:@"pi1_rating_rateit_5star_%i",value]];
    
    return @"";
}

- (void)updateSendbuttonStyle{
    
    // creating button
    [_okButton setTitle:[[super.ifbckModel getTranslationForKey:@"pi1_rating_submit_btn"] uppercaseString] forState:UIControlStateNormal];
    [_okButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [_okButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    
    
    // button background
    if(super.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![super.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        // creating normal background
        _okButton.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
    }
    else {
        _okButton.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    }
    
    [_okButton.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [_okButton.layer setBorderWidth:1.0f];
    [_okButton.layer setCornerRadius:5.0f];
    
    
    // button text color
    if(super.ifbckModel.parameters[@"app_send_button_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_send_button_text_color"] isEmptyString]){
        [_okButton setTitleColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_okButton.layer setBorderColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
    }

    
    // button border color
    if(super.ifbckModel.parameters[@"app_send_button_border_color"]!=nil && ![super.ifbckModel.parameters[@"app_send_button_border_color"] isEmptyString]){
        [_topicOkButtonWrapper setBackgroundColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_border_color"]]];
    }

}

#pragma mark - Textfield and Textview delegate methods

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        //[textView resignFirstResponder];
        //return NO;
    }
    
    if(![textView.text isEmptyString] || ![text isEmptyString]){
        _starRatingCommentTextfield.layer.borderColor = [[UIColor grayColor] CGColor];
    }
    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView{
    if(textView == _starRatingCommentTextfield){
        if(textView.contentSize.height > 180)
            _starRatingCommentTextfieldHeight.constant = textView.contentSize.height + 20;
        else
            _starRatingCommentTextfieldHeight.constant = 200;
    }
    
    
    [self saveTopic];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:_textFieldPlaceHolder]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        textView.text = _textFieldPlaceHolder;
        textView.textColor = _placeHolderColor;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    //    if([_dynamicFieldsArray[textField.tag][@"required"] intValue]==1 && showRequired)
    //        [self.tableView reloadRowsAtIndexPaths:_requiredFieldIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
    //
    //    if([_dynamicFieldsArray[textField.tag][@"required"] intValue]==2 && showRequired)
    //        [self.tableView reloadRowsAtIndexPaths:_askOnceIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)okButtonPressed:(id)sender {
    
    if(_starRatingView.value < 1){
        [[[UIAlertView alloc] initWithTitle:nil message:[super.ifbckModel getTranslationForKey:@"pi1_rating_rateit_5star_0"] delegate:nil cancelButtonTitle:[super.ifbckModel getTranslationForKey:@"pi1_rating_submit_btn"] otherButtonTitles: nil] show];
        
        return;
    }
    
    if(super.ifbckModel.parameters[@"comment_required_at_value"] != nil && [super.ifbckModel.parameters[@"comment_required_at_value"] intValue] > 0){
        if([super.ifbckModel.parameters[@"comment_required_at_value"] intValue] >= _starRatingView.value){
            if([_starRatingCommentTextfield.text isEqualToString:_textFieldPlaceHolder] || [_starRatingCommentTextfield.text isEmptyString]){
                [[[UIAlertView alloc] initWithTitle:nil message:[super.ifbckModel getTranslationForKey:@"no_stars_comment_required"] delegate:nil cancelButtonTitle:[super.ifbckModel getTranslationForKey:@"pi1_rating_submit_btn"] otherButtonTitles: nil] show];
                _starRatingCommentTextfield.layer.borderColor = [[UIColor redColor] CGColor];
                return;
            }
            else {
                _starRatingCommentTextfield.layer.borderColor = [[UIColor grayColor] CGColor];
            }
        }
    }
    
    [self saveTopic];
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)saveTopic{
    
    _savedValue = _starRatingView.value;
    
    [_delegate starRatingValueDidChange:_starRatingView];
    
    // if the topic ratings dictionary was not yet initialized
    if(super.ifbckModel.topicRatings == nil)
        // do it
        super.ifbckModel.topicRatings = [@{} mutableCopy];
    
    // define the rating
    NSDictionary *rating = @{
                             @"topic_id":_topic[@"uid"],
                             @"rating_value":@(_savedValue),
                             @"comment":(![_starRatingCommentTextfield.text isEqualToString:_textFieldPlaceHolder])?_starRatingCommentTextfield.text:@"",
                             @"tstamp":@((int)[[NSDate date] timeIntervalSince1970]),
                             };
    
    
    // if our selected value is higher then 0
    if(_starRatingView.value > 0)
        // add the topic-value-comment connection
        [super.ifbckModel.topicRatings setObject:rating forKey:_topic[@"uid"]];
    else
        [super.ifbckModel.topicRatings removeObjectForKey:_topic[@"uid"]];

}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;

    self.navigationController.view.frame = CGRectMake(_originalViewFrame.origin.x, _originalViewFrame.origin.y, screenWidth, _originalViewFrame.size.height - (kbSize.height) + 0);
    self.view.frame = CGRectMake(_originalViewFrame.origin.x, _originalViewFrame.origin.y, screenWidth, _originalViewFrame.size.height - (kbSize.height) + 0);
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    [UIView animateWithDuration:0.5 animations:^(void){
        self.navigationController.view.frame = CGRectMake(_originalViewFrame.origin.x, _originalViewFrame.origin.y, screenWidth, _originalViewFrame.size.height - (kbSize.height) + 0);
        self.view.frame = CGRectMake(_originalViewFrame.origin.x, _originalViewFrame.origin.y, screenWidth, _originalViewFrame.size.height - (kbSize.height) + 0);
    }];
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.view.height = _originalViewFrame.size.height;
    self.navigationController.view.height = _originalViewFrame.size.height;
    [self.view layoutIfNeeded];
    [self.navigationController.view layoutIfNeeded];
}

@end
