//
//  MyOfferEditViewController.swift
//  if_framework
//
//  Created by Christopher on 10/5/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

import UIKit

import GooglePlacePicker

class MyOfferEditViewController: AdminViewController, UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImageUploadDelegate, GMSAutocompleteViewControllerDelegate {

    // MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: constants
    let rules:[String:NSArray] = ["required":[
        "pid",
        "description",
        "name",
        "saving",
        "header_color",
        "image_header",
        "icon",
        "address",
        "availability",
        "starttime",
        //"endtime",
        ]
    ]
    let labels:[String:String] = [
        "description":"Description".localized,
        "name":"Title".localized,
        "header_color":"Color".localized,
        "availability":"\("Availability".localized) - \("Seats".localized)",
        "image_header":"Background image".localized,
        "icon":"Icon".localized,
        "address":"Place".localized,
        "starttime":"\("Availability".localized) - \("From".localized)",
        "endtime":"\("Availability".localized) - \("To".localized)",
        "saving":"Price".localized,
        ]
    
    
    // MARK: variables
    var placePicker:GMSPlacePicker?
    var offer:[String:Any?] = ["pid":897,"header_color":"#ffffff","icon":"files/icons/ardnd_icon_white.png"] {
        didSet {
            self.didSetOffer()
        }
    }
    var offerCustomContent:[String:Any] = [:]
    var editForm:[[[String: Any]]] = []
    
    var backgroundImage:UIImage? = nil
    
    var completed:([String:Any])->Void = {offer in}
    var selectedIndexPath:IndexPath? = nil
    
    // MARK: methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.keyboardDismissMode = .interactive
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorInset = UIEdgeInsetsMake(0, UIScreen.main.bounds.size.width, 0, 0)
        
        self.initTitleView()
        
        self.initBarButtonItems()
        
        self.initForm()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.selectedIndexPath != nil {
            self.tableView.reloadRows(at: [self.selectedIndexPath!], with: .fade)
        }
    }
    
    func initForm(){
        self.editForm = [
            [
                [
                    "height":160.0,
                    "identifier":"header",
                    ],
//                [
//                    "height":45.0,
//                    "identifier":"color",
//                    "placeholder":"Color".localized,
//                    "text":self.offer["header_color"],
//                    "editing":"header_color",
//                    ],
//                [
//                    "height":120.0,
//                    "identifier":"textview",
//                    "placeholder":"Description".localized,
//                    "text":self.offer["description"],
//                    "editing":"description"
//                ],
            ],
            [
                [
                    "sectionTitle":"Description".localized,
                    "height":100.0,
                    "identifier":"textview",
                    "label":"AR D'N'D Special".localized,
                    "placeholder":"e.g. free pre-dinner drink".localized,
                    "getText":{()->String in
                        return self.offerCustomContent["special"] as? String ?? ""
                    },
                    "onTextChanged":{ (text:String) in
                        self.offerCustomContent["special"] = text
                    },
                ],
                [
                    "sectionTitle":"Description".localized,
                    "height":100.0,
                    "identifier":"textview",
                    "label":"About the location".localized,
                    "placeholder":"Tell about the location and why it's special for you".localized,
                    "getText":{()->String in
                        NSLog("location_desc: \(String(describing: self.offerCustomContent["location_desc"]))")
                        return self.offerCustomContent["location_desc"] as? String ?? ""
                    },
                    "onTextChanged":{ (text:String) in
                        self.offerCustomContent["location_desc"] = text
                    },
                    ],
                [
                    "height":100.0,
                    "identifier":"textview",
                    "label":"About the food".localized,
                    "placeholder":"Tell something about the food / the drinks".localized,
                    "getText":{()->String in
                        NSLog("food_desc: \(String(describing: self.offerCustomContent["food_desc"]))")
                        return self.offerCustomContent["food_desc"] as? String ?? ""
                    },
                    "onTextChanged":{ (text:String) in
                        self.offerCustomContent["food_desc"] = text
                    },
                    ],
                [
                    "height":100.0,
                    "identifier":"textview",
                    "label":"About the host".localized,
                    "placeholder":"Tell something about who you are".localized,
                    "getText":{()->String in
                        return self.offerCustomContent["about_me_desc"] as? String ?? ""
                    },
                    "onTextChanged":{ (text:String) in
                        self.offerCustomContent["about_me_desc"] = text
                    },
                ],
                [
                    "height":100.0,
                    "identifier":"textview",
                    "label":"Hashtags",
                    "placeholder":"e.g. #food #drink",
                    "getText":{()->String in
                        return self.offerCustomContent["hashtags"] as? String ?? ""
                    },
                    "onTextChanged":{ (text:String) in
                        self.offerCustomContent["hashtags"] = text
                    },
                ],
//                [
//                    "height":100.0,
//                    "identifier":"textview",
//                    "label":"About the event".localized,
//                    "placeholder":"Tell something about the event / Who do you want to meet".localized,
//                    "getText":{()->String in
//                        return self.offerCustomContent["event_desc"] as? String ?? ""
//                    },
//                    "onTextChanged":{ (text:String) in
//                        self.offerCustomContent["event_desc"] = text
//                    },
//                    ],
//                [
//                    "height":45.0,
//                    "identifier":"selector",
//                    "placeholder":"AR D'N'D Special".localized,
//                    "text":self.offerCustomContent["special"] as? String ?? "",
//                    "getText":{()->String in
//                        return self.offerCustomContent["special"] as? String ?? ""
//                    },
//                    "onClick":{
//                        let selector = SingleSelectorViewController.getSingleSelectorWith(optionsFromUrl: SECURED_API_URL + "admin/offers/options/ardnd_special")
//                        
//                        selector.onHide = {
//                            self.offerCustomContent["special"] = selector.selectedOption ?? nil
//                        }
//                        
//                        self.navigationController?.pushViewController(selector, animated: true)
//                    }
//                    ],
            ],
            [
                [
                    "sectionTitle":"Price".localized,
                    "sectionHint":"AR D'N'D Service fee will be added automatically".localized,
                    "height":45.0,
                    "identifier":"textinput",
                    "placeholder":"EUR".localized,
                    "text":unwrap(object: self.offer["saving"], defaultValue: ""),
                    "keyBoardType":UIKeyboardType.decimalPad,
                    "editing":"saving",
                    "onEnd":{ (textInput:UITextField, text:String) in
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .currency
                        formatter.locale = Locale(identifier: "de_DE")
                        
                        let price = (Double(text.replacingOccurrences(of: formatter.decimalSeparator, with: ".")) ?? 0) + 2.99
                        let newPrice = formatter.string(from: price as NSNumber)
                        
                        self.offer["saving"] = newPrice
                        textInput.text = newPrice
                    },
                    "onStart":{ (textInput:UITextField, text:String) in
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .currency
                        formatter.locale = Locale(identifier: "de_DE")
                        
                        let price = formatter.number(from: text) ?? 0
                        let newPrice = Double(price) - 2.99 > 0 ? "\(Double(price) - 2.99)".replacingOccurrences(of: ".", with: formatter.decimalSeparator) : ""
                        
                        textInput.text = newPrice
                        self.offer["saving"] = newPrice
                    },
                    "accessoryType":UITableViewCellAccessoryType.detailButton,
                    "toolTip":"The guest needs to pay additional 2.99€ as a deposit and AR D'N'D service fee, if his request was accepted and if he wants to take part. This ensures, that he really wants to take part.".localized
                ]
            ],
            [
                [
                    "sectionTitle":"Where?".localized,
                    "height":60.0,
                    "identifier":"place",
                    "placeholder":"Place".localized,
                    ]
            ],
            [
                [
                "sectionTitle":"When?".localized,
                "height":45.0,
                "identifier":"date",
                "editing":"starttime",
                "getText":{()->String in
                    var date:Date? = nil
                    
                    switch self.offer["starttime"] {
                    case is String:
                        let tstampString = self.offer["starttime"] as! String
                        if !tstampString.isEmpty && tstampString != "0" {
                            date = Date(timeIntervalSince1970: Double(tstampString)!)
                        }
                        break
                        
                    case is Double:
                        let tstampInt = self.offer["starttime"] as! Double
                        if tstampInt > 0 {
                            date = Date(timeIntervalSince1970:tstampInt)
                        }
                        break
                        
                    default:
                        break
                    }
                    
                    NSLog("custom content on save: \(self.offerCustomContent)")
                    
                    if date == nil {
                        return "Please select ...".localized
                    }
                    else {
                        return miscFunctions.localizedString(from: date!, onlyDate: false)
                    }
                },
                "placeholder":"From".localized,
                "onClick":{
                    var date:Date = Date(timeIntervalSinceNow: 0)
                    
                    switch self.offer["starttime"] {
                    case is String:
                        let tstampString = self.offer["starttime"] as! String
                        if !tstampString.isEmpty && tstampString != "0"{
                            date = Date(timeIntervalSince1970: Double(tstampString)!)
                        }
                        break
                        
                    case is Double:
                        let tstampInt = self.offer["starttime"] as! Double
                        if tstampInt > 0 {
                            date = Date(timeIntervalSince1970:tstampInt)
                        }
                        break
                        
                    default:
                        break
                    }
                    
                    let datePicker = CWDatePicker(withDate: date, fromView: self.view)
                    
                    datePicker.hideTimeSwitcher()
                    datePicker.selected = {date in
                        
                        let calendar = NSCalendar.autoupdatingCurrent
                        let components = calendar.dateComponents([.hour, .minute], from: date)
                        let hour = components.hour
                        let minute = components.minute
                        
                        self.offerCustomContent["timerange"] = [
                            "from":"\(String(format: "%02d", hour!)):\(String(format: "%02d", minute!))",
                            "to":((self.offerCustomContent["timerange"] as? [String:String] ?? [:])["to"] ?? "")
                        ]
                        
                        self.offer["starttime"] = date.timeIntervalSince1970
                        if self.selectedIndexPath != nil {
                            self.tableView.reloadRows(at: [self.selectedIndexPath!], with: .fade)
                        }
                    }
                    
                    datePicker.show()
                    
                    NSLog("custom content on click: \(self.offerCustomContent)")
                }
                ],
                [
                    "height":45.0,
                    "identifier":"date",
                    "editing":"endtime",
                    "getText":{()->String in
                        let timeRange = self.offerCustomContent["timerange"] as? [String:String] ?? [:]
                        if let from = timeRange["from"], let to = timeRange["to"] {
                            if (!from.isEmpty && !to.isEmpty) {
                                return "\(to)"
                            }
                            else {
                                return "Please select ...".localized
                            }
                        }
                        else {
                            return "Please select ...".localized
                        }
                    },
                    "placeholder":"To".localized,
                    "onClick":{
                        var date:Date = Date(timeIntervalSinceNow: 0)
                        
                        switch self.offer["endtime"] {
                        case is String:
                            let tstampString = self.offer["endtime"] as! String
                            if !tstampString.isEmpty && tstampString != "0"{
                                date = Date(timeIntervalSince1970: Double(tstampString)!)
                            }
                            break
                            
                        case is Double:
                            let tstampInt = self.offer["endtime"] as! Double
                            if tstampInt > 0 {
                                date = Date(timeIntervalSince1970:tstampInt)
                            }
                            break
                            
                        default:
                            break
                        }
                        
                        let datePicker = CWDatePicker(withDate: date, fromView: self.view)
                        
                        datePicker.hideTimeSwitcher()
                        datePicker.datePicker.datePickerMode = .time
                        datePicker.selected = {date in
                            
                            let calendar = NSCalendar.autoupdatingCurrent
                            let components = calendar.dateComponents([.hour, .minute], from: date)
                            let hour = components.hour
                            let minute = components.minute
                            
                            self.offerCustomContent["timerange"] = [
                                "to":"\(String(format: "%02d", hour!)):\(String(format: "%02d", minute!))",
                                "from":((self.offerCustomContent["timerange"] as? [String:String] ?? [:])["from"] ?? "")
                            ]
                            
                            self.offer["endtime"] = date.timeIntervalSince1970
                            if self.selectedIndexPath != nil {
                                self.tableView.reloadRows(at: [self.selectedIndexPath!], with: .fade)
                            }
                        }
                        
                        datePicker.show()
                    }
                ],
                [
                    "height":45.0,
                    "identifier":"days",
                    "editing":"starttime",
                    "placeholder":"Weekdays".localized,
                    "getText":{()->String in
                        let selected = self.offerCustomContent["days"] as? [Int] ?? []
                        var usedWeekdays:[String] = []
                        
                        var i = 0
                        for str in weekdaysShort {
                            if(selected.contains(i)){
                                usedWeekdays.append(str)
                            }
                            i += 1
                        }
                        
                        return usedWeekdays.joined(separator: ", ").isEmpty ? "Please select ...".localized : usedWeekdays.joined(separator: ", ")
                    },
                    "onClick":{
                        let wkSelector = get(viewController:"WeekDaySelectorViewController") as! WeekDaySelectorViewController
                        
                        if(self.offerCustomContent["days"] is [Int]){
                            wkSelector.selected = self.offerCustomContent["days"] as! [Int]
                        }
                        wkSelector.title = "Weekdays".localized
                        wkSelector.onHide = {
                            self.offerCustomContent["days"] = wkSelector.selected
                        }
                        self.navigationController?.pushViewController(wkSelector, animated: true)
                    }
                ],
                [
                    "height":45.0,
                    "identifier":"switch",
                    "placeholder":"Repeat".localized,
                    "getStatus":{()->Bool in
                        if self.offerCustomContent["repeat"] as? Int == 1 || self.offerCustomContent["repeat"] as? String == "1" {
                            return true
                        }
                        
                        return false
                    },
                    "onSwitch":{(switcher:UISwitch)->Void in
                        if switcher.isOn {
                            self.offerCustomContent["repeat"] = 1
                            self.offer["endtime"] = 0
                        }
                        else {
                            self.offerCustomContent["repeat"] = 0
                            self.offer["endtime"] = self.offer["starttime"]
                        }
                    }
                ],
//                [
//                    "height":45.0,
//                    "identifier":"timerange",
//                    "editing":"endtime",
//                    "text":self.offerCustomContent["timerange"],
//                    "placeholder":"Timerange".localized,
//                    "getText":{()->String in
//                        let timeRange = self.offerCustomContent["timerange"] as? [String:String] ?? [:]
//                        if let from = timeRange["from"], let to = timeRange["to"] {
//                            if (!from.isEmpty && !to.isEmpty) {
//                                return "\(from) - \(to)"
//                            }
//                            else {
//                                return "Please select ...".localized
//                            }
//                        }
//                        else {
//                            return "Please select ...".localized
//                        }
//                    },
//                    "onClick":{
//                        let trSelector = get(viewController: "TimeRangeSelectorViewController") as! TimeRangeSelectorViewController
//                        
//                        if(self.offerCustomContent["timerange"] is [String:String]){
//                            trSelector.from = (self.offerCustomContent["timerange"] as! [String:String])["from"] ?? ""
//                            trSelector.to = (self.offerCustomContent["timerange"] as! [String:String])["to"] ?? ""
//                        }
//                        trSelector.title = "Timerange".localized
//                        trSelector.onHide = {
//                            let timeRange = [
//                                "from":trSelector.from,
//                                "to":trSelector.to,
//                                ]
//                            self.offerCustomContent["timerange"] = timeRange
//                        }
//                        
//                        self.navigationController?.pushViewController(trSelector, animated: true)
//                    }
//                ],
                ],
            [
                [
                    "sectionTitle":"Availability".localized,
                    "height":45.0,
                    "identifier":"textinput",
                    "placeholder":"Seats".localized,
                    "text":unwrap(object: self.offer["availability"], defaultValue: ""),
                    "keyBoardType":UIKeyboardType.decimalPad,
                    "editing":"availability",
                    ],
                ],
        ]
        
        self.tableView.reloadData()
    }
    
    func initTitleView() {
        let textField = UITextField(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
        textField.placeholder = "Title".localized
        textField.textAlignment = NSTextAlignment.center
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.text = self.offer["name"] as? String
        textField.tag = 999
        textField.delegate = self
        textField.font = UIFont(name: "HelveticaNeue-Bold", size: 18.0)
        
        self.navigationItem.titleView = textField
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "   ", style: .plain, target: nil, action: nil)
    }
    
    func initBarButtonItems() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .save, target: self, action: #selector(save))
    }
    
    // MARK: update and save
    func beforeSave(){
        self.showLoading()
    }
    
    func afterSave(response:NSDictionary?){
        if((response?["success"]) != nil && (response?["success"] as! Int) == 1){
            if let uid = response?["id"] as AnyObject? {
                if let id = Int(String(format: "%@", uid as! CVarArg)){
                    self.offer["uid"] = id
                }
            }
            self.completed(unwrap(object: self.offer, defaultValue: [:]) as! [String:Any])
            _ = self.navigationController?.popViewController(animated: true)
        }
        else if let error = response?["error"] as? NSError {
            let alert = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: {
                self.endLoading()
            })
        }
        else if let error = response?["error"] as? String {
            let alert = UIAlertController(title: "Error".localized, message: error.localized, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: {
                self.endLoading()
            })
        }
        else {
        }
        
        self.endLoading()
    }
    
    func validateOffer() -> Bool {
        NSLog("rules: \(rules) \n offer: \(offer)")
        
        for (rule,fields) in rules {
            if(rule == "required"){
                for field in fields {
                    if(self.offer[field as! String]==nil){
                        var fieldText:String? = nil
                        
                        if let fieldDescription = labels[field as! String] {
                            fieldText = fieldDescription
                        }
                        
                        self.missingAlert(field: fieldText)
                        return false
                    }
                    else {
                        switch self.offer[field as! String] {
                        case is String:
                            if (self.offer[field as! String] as! String).isEmpty || (self.offer[field as! String] as! String) == "0" { 
                                var fieldText:String? = nil
                                
                                if let fieldDescription = labels[field as! String] {
                                    fieldText = fieldDescription
                                }
                                
                                self.missingAlert(field: fieldText)
                                return false
                            }
                            break
                        case is Double:
                            if (self.offer[field as! String] as! Double) == 0 {
                                var fieldText:String? = nil
                                
                                if let fieldDescription = labels[field as! String] {
                                    fieldText = fieldDescription
                                }
                                
                                self.missingAlert(field: fieldText)
                                return false
                            }
                            break
                        case is Int:
                            if (self.offer[field as! String] as! Int) == 0 {
                                var fieldText:String? = nil
                                
                                if let fieldDescription = labels[field as! String] {
                                    fieldText = fieldDescription
                                }
                                
                                self.missingAlert(field: fieldText)
                                return false
                            }
                            break
                        default:
                            break
                        }
                    }
                }
            }
        }
        
        return true
    }
    
    func missingAlert(field: String?){
        self.missingAlert(field: field, on:self)
    }
    
    func missingAlert(field: String?, on: UIViewController?){
        self .missingAlert(field: field, on: on, okPressed: nil)
    }
    
    func missingAlert(field: String?, on: UIViewController?, okPressed: (()->Void)?){
        var message:String? = nil
        
        if field != nil {
            message = String(format:"%@ is needed.".localized,field!)
        }
        
        let alert = UIAlertController(title: "Missing information".localized, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: { (nil) in
            okPressed?()
        }))
        
        var vc:UIViewController? = nil
        
        if on != nil {
            vc = on
        }
        else {
            vc = self
        }
        
        vc!.present(alert, animated: true, completion: {})
    }
    
    func didSetOffer(){
        if self.offerCustomContent.isEmpty {
            switch self.offer["dynamic_content"] {
            case is [String:Any]:
                self.offerCustomContent = self.offer["dynamic_content"] as! [String:Any]
                break
            case is String:
                let contentString = self.offer["dynamic_content"] as! String
                do {
                    let customContent = try JSONSerialization.jsonObject(with: contentString.data(using: .utf8)!, options: .allowFragments) as? [String:Any] ?? [:]
                    self.offerCustomContent = customContent
                }
                catch {
                    NSLog(" ADMIN | MyOffers - Error: \(error)")
                }
                break
            default:
                self.offerCustomContent = [:]
                break
            }
        }
    }
    
    func createNew(){
        if(validateOffer()){
            DispatchQueue.global().async {
                let response = API.post(toURL: SECURED_API_URL + "offer/create", withPost: ["save":true,"offer":self.offer]) as? NSDictionary
                
                DispatchQueue.main.async {
                    self.afterSave(response: response)
                }
            }
        }
        else {
            self.afterSave(response: ["success":0])
        }
    }
    
    func update(){
        if(validateOffer()){
            DispatchQueue.global().async {
                let response = API.post(toURL: SECURED_API_URL + "offer/update", withPost: ["save":true,"offer":self.offer]) as? NSDictionary
                
                DispatchQueue.main.async {
                    self.afterSave(response: response)
                }
            }
        }
        else {
            self.afterSave(response: ["success":0])
        }
    }
    
    @objc func save(){
        self.beforeSave()
        offer["dynamic_content"] = self.offerCustomContent
        
        var description = ""
        var space = ""
        if self.offerCustomContent["location_desc"] is String && !(self.offerCustomContent["location_desc"] as! String).isEmpty{
            description.append("\(space)\("About the location".localized.uppercased()):\n\(self.offerCustomContent["location_desc"] as! String)")
            space = "\n\n"
        }
        
        if self.offerCustomContent["food_desc"] is String && !(self.offerCustomContent["food_desc"] as! String).isEmpty{
            description.append("\(space)\("About the food".localized.uppercased()):\n\(self.offerCustomContent["food_desc"] as! String)")
            space = "\n\n"
        }
        
        if self.offerCustomContent["about_me_desc"] is String && !(self.offerCustomContent["about_me_desc"] as! String).isEmpty{
            description.append("\(space)\("About the host".localized.uppercased()):\n\(self.offerCustomContent["about_me_desc"] as! String)")
            space = "\n\n"
        }
        
        offer["description"] = description
        
        if(offer["uid"] == nil || offer["uid"] as? Int == 0){
            self.createNew()
        }
        else {
            self.update()
        }
    }
    
    // MARK: uploader and icon picker
    @objc func openImageUploader(){
        let imageSelectorAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        imageSelectorAlert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { (nil) in
            let picker = UIImagePickerController()
            
            picker.delegate = self;
            picker.allowsEditing = true;
            picker.sourceType = .camera;
            
            self.present(picker, animated: true, completion: {
                
            })
        }))
        
        imageSelectorAlert.addAction(UIAlertAction(title: "Library".localized, style: .default, handler: { (nil) in
            let picker = UIImagePickerController()
            
            picker.delegate = self;
            picker.allowsEditing = true;
            picker.sourceType = .photoLibrary;
            
            self.present(picker, animated: true, completion: { 
                
            })
        }))
        
        imageSelectorAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (nil) in
            
        }))
        
        self.present(imageSelectorAlert, animated: true) { 
            
        }
    }
    
    @objc func openIconPicker(){
        let vc = IconPickerViewController.load("IconPickerViewController", fromStoryboard: "IconPickerViewController") as! IconPickerViewController
        
        vc.hidesWhenSelected = true
        
        vc.selectedIcon = {(path: String) in
            self.offer["icon"] = path
            self.tableView.reloadRows(at: [NSIndexPath(row: 0, section: 0) as IndexPath], with: .fade)
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: tableView delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.editForm.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.editForm[indexPath.section][indexPath.row]["identifier"] as! String) as! MyOfferEditTableViewCell
        
        cell.tag = (indexPath.section * 1000) + indexPath.row
        
        var text:String?
        
        if self.editForm[indexPath.section][indexPath.row]["getText"] is ()->String {
            text = (self.editForm[indexPath.section][indexPath.row]["getText"] as! ()->String)()
        }
        else {
            text = self.offer[self.editForm[indexPath.section][indexPath.row]["editing"] as? String ?? ""] as? String ?? self.editForm[indexPath.section][indexPath.row]["text"] as? String ?? ""
        }
        
        
        cell.tableView = tableView
        cell.configure(text: text, placeholder:  self.editForm[indexPath.section][indexPath.row]["placeholder"] as? String, label: self.editForm[indexPath.section][indexPath.row]["label"] as? String)
        
        cell.textView?.delegate = self
        cell.textInput?.delegate = self
        
        cell.colorIndicator?.backgroundColor = UIColor(fromHexString: self.editForm[indexPath.section][indexPath.row]["text"] as? String)
        
        if(self.editForm[indexPath.section][indexPath.row]["keyBoardType"] != nil){
            switch self.editForm[indexPath.section][indexPath.row]["keyBoardType"] {
            case is UIKeyboardType:
                cell.textInput?.keyboardType = self.editForm[indexPath.section][indexPath.row]["keyBoardType"] as! UIKeyboardType
                cell.textView?.keyboardType = self.editForm[indexPath.section][indexPath.row]["keyBoardType"] as! UIKeyboardType
                break
            default:
                break
            }
        }
        else {
            
        }
        
        if self.editForm[indexPath.section][indexPath.row]["accessoryType"] != nil {
            switch self.editForm[indexPath.section][indexPath.row]["accessoryType"] {
            case is UITableViewCellAccessoryType:
                cell.accessoryType = self.editForm[indexPath.section][indexPath.row]["accessoryType"] as! UITableViewCellAccessoryType
                break
            default:
                break
            }
        }
        else {
            
        }
        
        if( self.editForm[indexPath.section][indexPath.row]["identifier"] as! String == "header"){
            cell.buttonBackgroundImage?.addTarget(self, action: #selector(openImageUploader), for: .touchUpInside)
            
            if(self.backgroundImage==nil){
                if(self.offer["image_header"] != nil){
                    DispatchQueue.global().async {
                        let image:UIImage? = imageMethods.uiImage(withServerPathName: self.offer["image_header"] as? String, width: 90)
                        
                        DispatchQueue.main.async {
                            if(indexPath.section==cell.tag && image != nil){
                                cell.backgroundImage.image = image
                                cell.setNeedsLayout()
                            }
                            else {
                                cell.backgroundImage.image = nil
                                cell.setNeedsLayout()
                            }
                        }
                    }
                }
            }
            else {
                cell.backgroundImage.image = self.backgroundImage
            }
            
            if(self.offer["icon"] != nil){
                DispatchQueue.global().async {
                    let image:UIImage? = imageMethods.resize(imageMethods.uiImage(withServerPathName: self.offer["icon"] as? String, width: 59), for: CGSize(width: 59, height: 59))
                
                    
                    DispatchQueue.main.async {
                        if(indexPath.section==cell.tag && image != nil){
                            cell.iconImage.image = image!.withRenderingMode(.alwaysTemplate)
                            
                            if let colorString = self.offer["header_color"] as? String{
                                cell.iconImage.tintColor = UIColor(fromHexString:  colorString)
                            }
                            else {
                                cell.iconImage.tintColor = UIColor.white
                            }
                            
                            cell.setNeedsLayout()
                        }
                        else {
                            cell.iconImage.image = nil
                            cell.setNeedsLayout()
                        }
                    }
                }
            }
            
            cell.buttonIcon?.addTarget(self, action: #selector(openIconPicker), for: .touchUpInside)
        }
        
        else if( self.editForm[indexPath.section][indexPath.row]["identifier"] as! String == "place"){
            
            cell.label2.text = "Please select ...".localized
            
            if self.offer["address"] != nil {
                switch self.offer["address"]! {
                case is NSDictionary:
                    if let address = (self.offer["address"] as! NSDictionary)["formated_address"] as? String{
                        cell.label2.text = address
                    }
                    break
                    
                case is NSString:
                    do {
                        if let dict = try JSONSerialization.jsonObject(with: (self.offer["address"] as! String).data(using: .utf8)!) as? NSDictionary {
                            if let address = dict["formated_address"] {
                                cell.label2.text = address as? String
                            }
                        } else if let string = self.offer["address"] as? String{
                            cell.label2.text = string
                        }
                        else {
                            cell.label2.text = "Please select ...".localized
                        }
                    }
                    catch {
                        
                    }
                    
                    break
                    
                default:
                    
                    break
                }
            }
        }
        
        else if( self.editForm[indexPath.section][indexPath.row]["identifier"] as! String == "switch"){
            if self.editForm[indexPath.section][indexPath.row]["onSwitch"] is (UISwitch)->Void {
                cell.switchSwitched = self.editForm[indexPath.section][indexPath.row]["onSwitch"] as! (UISwitch)->Void
            }
            
            if self.editForm[indexPath.section][indexPath.row]["getStatus"] is ()->Bool {
                cell.switch1.isOn = (self.editForm[indexPath.section][indexPath.row]["getStatus"] as! ()->Bool)()
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.isEditing = false
        self.view.endEditing(true)
        
        selectedIndexPath = indexPath
        
        if self.editForm[indexPath.section][indexPath.row]["onClick"] is ()->Void {
            (self.editForm[indexPath.section][indexPath.row]["onClick"] as! ()->Void)()
            return
        }
        
        else if(self.editForm[indexPath.section][indexPath.row]["identifier"] as! String == "color"){
            let colorPickerController = ColorPickerViewController.load("ColorPickerViewController", fromStoryboard: "ColorPickerViewController") as! ColorPickerViewController
            
            colorPickerController.view.height = UIScreen.main.bounds.size.height*0.7
            
            
            let colorPickerHeight:NSLayoutConstraint = NSLayoutConstraint(item: colorPickerController.view,
                                                                          attribute: .height,
                                                                          relatedBy: .equal,
                                                                          toItem: nil,
                                                                          attribute: .notAnAttribute,
                                                                          multiplier: 1,
                                                                          constant: self.view.frame.height * 0.8)
            colorPickerController.view.addConstraint(colorPickerHeight);
            
            
            if let color:UIColor = UIColor(fromHexString: self.editForm[indexPath.section][indexPath.row]["text"] as? String){
                colorPickerController.color = color
            }
            
            
            let alertController = UIAlertController(title: self.editForm[indexPath.section][indexPath.row]["placeholder"] as? String, message: nil, preferredStyle: .actionSheet)
            
            alertController.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: { (nil) in
                if let colorString:String = colorPickerController.color?.hexString()!{
                    self.editForm[indexPath.section][indexPath.row]["text"] = colorString
                    self.offer[self.editForm[indexPath.section][indexPath.row]["editing"] as! String] = colorString
                    self.tableView.reloadRows(at: [indexPath, IndexPath(row: 0, section: 0)], with: .fade)
                }
            }))

            alertController.setValue(colorPickerController, forKey: "contentViewController")
            
            self.present(alertController, animated: true, completion: {

            })
            
            return
        }
        
        else if(self.editForm[indexPath.section][indexPath.row]["identifier"] as! String == "place"){
            self.pickPlace()
            
            return
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.editForm[section].count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.editForm[indexPath.section][indexPath.row]["height"] as! Double)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section==0){
            return 0
        }
        if let _ = self.editForm[section][0]["sectionTitle"] as? String {
            return 45
        }
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let titleString = self.editForm[section][0]["sectionTitle"] as? String {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45))
            view.backgroundColor = .groupTableViewBackground
            let label = UILabel()
            label.text = titleString
            label.font = UIFont(name: "HelveticaNeue-Bold", size: 17)!
            label.sizeToFit()
            view.addSubview(label)
            label.center = view.center
            label.x = 15
            
            if let hintString = self.editForm[section][0]["sectionHint"] as? String {
                let label2 = UILabel()
                label2.text = "(\(hintString))"
                label2.font = UIFont(name: "HelveticaNeue", size: 12)!
                label2.numberOfLines = 0
                label2.lineBreakMode = .byWordWrapping
                view.addSubview(label2)
                label2.width = UIScreen.main.bounds.size.width - 15 - label.width - 32
                label2.sizeToFit()
                label2.center = view.center
                label2.x = label.x + label.width + 8
            }
            
            return view
        }
        
        return super.tableView(tableView, viewForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if self.editForm[indexPath.section][indexPath.row]["toolTip"] is String {
            let cell = tableView.cellForRow(at: indexPath)
            CWAlert.toolTip(withTitle: "Info".localized,
                                    message: self.editForm[indexPath.section][indexPath.row]["toolTip"] as? String,
                                    onViewController: self,
                                    fromView: cell!,
                                    fromRect:CGRect(x: UIScreen.main.bounds.size.width - 25, y: 22.5, width: 1, height: 1))
        }
    }
    
    
    // MARK: keyboard notifications
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
        } else {
            self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        self.tableView.scrollIndicatorInsets = self.tableView.contentInset
    }
    
    
    // MARK: textField delegate    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        
        
        if(textField.tag == 999){
            self.offer["name"] = newString
            return true
        }
        
        if  self.editForm[textField.tag / 1000][textField.tag % 1000]["onTextChanged"] != nil &&
            self.editForm[textField.tag / 1000][textField.tag % 1000]["onTextChanged"] is ((String)->Void) {
            (self.editForm[textField.tag / 1000][textField.tag % 1000]["onTextChanged"] as! (String)->Void)(newString!)
            return true
        }
        
        self.offer[self.editForm[textField.tag / 1000][textField.tag % 1000]["editing"] as? String ?? "0"] = newString
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(self.tableView.numberOfSections>=(textField.tag / 1000) && self.tableView.numberOfRows(inSection: textField.tag / 1000) >= (textField.tag % 1000)){
            self.tableView.scrollToRow(at: IndexPath(row: textField.tag % 1000, section: textField.tag / 1000), at: .bottom, animated: true);
        }
        self.showDoneEditingButton()
        
        if(textField.tag == 999){
            return
        }
        if self.editForm[textField.tag / 1000][textField.tag % 1000]["onStart"] is (UITextField,String)->Void {
            (self.editForm[textField.tag / 1000][textField.tag % 1000]["onStart"] as! (UITextField,String)->Void)(textField,textField.text ?? "")
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.hideDoneEditingButton()
        
        if(textField.tag == 999){
            return
        }
        if self.editForm[textField.tag / 1000][textField.tag % 1000]["onEnd"] is (UITextField,String)->Void {
            (self.editForm[textField.tag / 1000][textField.tag % 1000]["onEnd"] as! (UITextField,String)->Void)(textField,textField.text ?? "")
            return
        }
    }
    
    
    // MARK: textView delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let nsString = textView.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: text) ?? ""
        
        
        if  self.editForm[textView.tag / 1000][textView.tag % 1000]["onTextChanged"] != nil &&
            self.editForm[textView.tag / 1000][textView.tag % 1000]["onTextChanged"] is ((String)->Void) {
            (self.editForm[textView.tag / 1000][textView.tag % 1000]["onTextChanged"] as! (String)->Void)(newString)
            return true
        }
        
        self.offer[self.editForm[textView.tag / 1000][textView.tag % 1000]["editing"] as! String] = newString
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if(textView.text.isEmpty){
            textView.text = self.editForm[textView.tag / 1000][textView.tag % 1000]["placeholder"] as! String;
            textView.textColor = UIColor(rgbHex: 0xBBBAC2)
        }
        
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if(textView.text == self.editForm[textView.tag / 1000][textView.tag % 1000]["placeholder"] as! String){
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.showDoneEditingButton()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.hideDoneEditingButton()
        
        if(textView.tag == 999){
            return
        }
        if self.editForm[textView.tag / 1000][textView.tag % 1000]["onEnd"] is (UITextView,String)->Void {
            (self.editForm[textView.tag / 1000][textView.tag % 1000]["onEnd"] as! (UITextView,String)->Void)(textView,textView.text ?? "")
            return
        }
    }
    
    // MARK: image picking
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerEditedImage]
        
        self.showLoading()
    
        
        let imagemethods = imageMethods()
        imagemethods.uploadDelegate = self
        
        imagemethods.uploadImage(image as! UIImage!, withParameters: ["uploadType":"offerImage"])
        
        picker.dismiss(animated: true, completion: nil)
        
        self.backgroundImage = image as! UIImage?
    }
    
    func uploadFinished(withResponse response: [AnyHashable : Any]!) {
        
        DispatchQueue.main.async {
            self.endLoading()
        }
        
        if response != nil {
            if response["success"] as? Int == 1 || response["success"] as? String == "1" {
                self.offer["image_header"] = (response["filePath"] as? String ?? "") + (response["fileName"] as? String ?? "")
                
                self.tableView.reloadRows(at: [NSIndexPath(row: 0, section: 0) as IndexPath], with: .fade)
            }
        }
    }
    
    
    // MARK: GMSAutoCompleteViewController delegate
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if(self.selected(place: place, on: viewController)){
            viewController.dismiss(animated: true) {
                
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true) {
            
        }
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true) { 
            
        }
    }
    
    func selected(place: GMSPlace, okPressed:(()->Void)?) -> Bool{
        return selected(place: place, on: self, okPressed: okPressed)
    }
    
    func selected(place: GMSPlace, on: UIViewController) -> Bool{
        return selected(place: place, on: on, okPressed: nil)
    }
    
    func selected(place: GMSPlace) -> Bool{
        return selected(place: place, on: self, okPressed: nil)
    }
    
    func selected(place: GMSPlace, on: UIViewController, okPressed:(()->Void)?) -> Bool{
        var placeInfo:[String:String] = [:]
        
        if place.addressComponents != nil {
            for addressComponent in place.addressComponents! {
                if addressComponent.type == "locality" {
                    placeInfo["city"] = addressComponent.name
                }
                
                if addressComponent.type == "street_number" {
                    placeInfo["street_number"] = addressComponent.name
                }
                
                if addressComponent.type == "postal_code" {
                    placeInfo["zip"] = addressComponent.name
                }
                
                if addressComponent.type == "route" {
                    placeInfo["street"] = addressComponent.name
                }
                
                if addressComponent.type == "country" {
                    placeInfo["country"] = addressComponent.name
                }
            }
            
            if(placeInfo["city"] == nil){
                missingAlert(field: "City".localized, on: on, okPressed: okPressed)
                return false
            }
            
            if(placeInfo["street"] == nil){
                //missingAlert(field: "Street".localized, on: on)
                //return false
            }
            
            if(placeInfo["street_number"] == nil){
                //missingAlert(field: "Street number".localized, on: on, okPressed: okPressed)
                //return false
            }
        }
        
        placeInfo["formated_address"] = place.formattedAddress
        
        NSLog("placeInfo: \(placeInfo)")
        
        self.offer["address"] = placeInfo
        
        return true
    }
    
    func pickPlace(){
        //// HEAVY PLACE PICKER
        
//        // Create a place picker.
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePicker(config: config)
//        
//        // Present it fullscreen.
//        placePicker.pickPlace { (place, error) in
//            
//            // Handle the selection if it was successful.
//            if let place = place {
//                // Create the next view controller we are going to display and present it.
//                NSLog("Place selected: \(place)")
//                let newPlace = place
//                GMSPlacesClient().lookUpPlaceID(place.placeID, callback: { (place, error) in
//                    var placeToUse:GMSPlace!
//                    
//                    if place != nil {
//                        placeToUse = place!
//                    }
//                    else {
//                        placeToUse = newPlace
//                    }
//                    
//                    if !self.selected(place: placeToUse, on: self, okPressed: { 
//                        self.pickPlace()
//                    }){
//                        
//                    }
//                    else {
//                        if self.selectedIndexPath != nil {
//                            self.tableView.reloadRows(at: [self.selectedIndexPath!], with: .fade)
//                        }
//                    }
//                })
//            } else if error != nil {
//                // In your own app you should handle this better, but for the demo we are just going to log
//                // a message.
//                NSLog("An error occurred while picking a place: \(error)")
//            } else {
//                NSLog("Looks like the place picker was canceled by the user")
//            }
//            
//            // Release the reference to the place picker, we don't need it anymore and it can be freed.
//            self.placePicker = nil
//        }
        
        // Store a reference to the place picker until it's finished picking. As specified in the docs
        // we have to hold onto it otherwise it will be deallocated before it can return us a result.
//        self.placePicker = placePicker
        
        //// SIMPLE PLACE PICKER
        
         let vc = GMSAutocompleteViewController()
         vc.delegate = self;
         self.present(vc, animated: true, completion: {
         
         })
 
    }
}
