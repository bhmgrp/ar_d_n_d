//
//  ProfileCollectionViewController.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/23/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class ProfileCollectionViewController: StartStreamCollectionViewController {
    
    var headerData : [String:Any] = [:]
    var profileId:String = "0"
    
    var userImage:UIImage = UIImage(named: "emptyProfileImage")!
    
    // MARK: lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableLayout.delegate = self as TableCollectionViewLayoutDelegate
        
        let headerNib = UINib.init(nibName: "ProfileCollectionViewHeader", bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "collectionHeader")
        
        let cellNib = UINib.init(nibName: "ProfileCollectionViewCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: "profileCollectionViewCell")
        
        loadProfileInfo()
        
        gridLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 150)
        tableLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 150)
        tableLayout.spacingsHeight = 170
        
        viewMode = .gridViewMode
        
        loadData()
    }
    
    override func initNavigationItems() {
        return
    }
    
    override func searchForFilters() {
        
    }
    
    func loadProfileInfo() {
        DispatchQueue.global().async {
            if let data = AdminAPI.get(fromUrl: "ardnd/user/profile-info/\(self.profileId)") as? [String:Any] {
                self.headerData = data
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.collectionView.collectionViewLayout.invalidateLayout()
                }
            }
        }
    }
    
    @objc func openOffers() {
        let vc = get(viewController: "OfferViewController") as! OfferViewController
        
        vc.userID = UInt(self.profileId) ?? 0
        vc.title = self.title
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: collection view delegate
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionHeader", for: indexPath) as! ProfileCollectionViewHeader
        
        view.imageView.image = userImage
        
        view.dragViewButton.addTarget(self, action: #selector(setTableViewMode), for: .touchUpInside)
        view.gridViewButton.addTarget(self, action: #selector(setGridViewMode), for: .touchUpInside)
        
        if viewMode == .gridViewMode {
            view.gridViewButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
            view.dragViewButton.tintColor = UIColor.lightGray
        } else {
            view.gridViewButton.tintColor = UIColor.lightGray
            view.dragViewButton.tintColor = UIColor(red: 54/255, green: 150/255, blue: 240/255, alpha: 1)
        }
        
        view.postsLabel.text = self.headerData["posts_count"] as? String ?? ""
        view.offersLabel.text = self.headerData["offers_count"] as? String ?? ""
        
        if (Int(self.headerData["offers_count"] as? String ?? "0") ?? 0 > 0) {
            view.viewOffersButton.setTitle("View offers".localized, for: .normal)
            view.viewOffersButton.addTarget(self, action: #selector(self.openOffers), for: .touchUpInside)
        }
        else {
            view.viewOffersButton.setTitle("No offers".localized, for: .normal)
        }
        
        return view
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch viewMode {
        case .tableViewMode:
            
            var cell: ProfileCollectionViewCell
            if let offerId = data[indexPath.item]["entity_type"] as? String, offerId == "1" {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offerProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
            } else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
            }
            
            cell.tag = indexPath.row
            cell.delegate = self
            
            cell.immediateCommentTextField.tag = indexPath.row
            cell.immediateCommentTextField.delegate = self
            cell.immediateCommentTextField.returnKeyType = .send
            
            cell.commentsProfileImageView.image = LoggedUserCredentials.sharedInstance.userImage
            
            let headerText = self.data[indexPath.row]["location_name"] as? String ?? ""
            let postText = self.data[indexPath.row]["content"] as? String ?? ""
            
            // take only top 5 comments which is not replies
            if let allComments = data[indexPath.row]["comments"] as? [[String:Any]] {
                let comments = getCommentsFrom(array: allComments)
                cell.setComments(comments)
                
                cell.showAllCommentsTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.commentsProfileImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: false)
                }
                
                cell.commentButtonTapHandler = {
                    let topComment = Comment(text: postText, author: headerText, authorImage: cell.commentsProfileImageView.image!)
                    self.getToCommentsViewController(topComment, focusOnComment: true)
                }
            }
            
            if let postImageString = self.data[indexPath.row]["image"] as? String {
                if self.imageMap[postImageString] != nil {
                    let image = self.imageMap[postImageString]
                    cell.imageView.image = image
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
//                else if let imageDownloadOperation = imageDownloadOperations[indexPath], let image = imageDownloadOperation.image {
//                    imageMap[postImageString] = image
//                    cell.imageView?.image = image
//                }
//                else {
//                    let imageDownloadOperation = ImageDownloadOperation.init(imageUrlString: postImageString)
//                    imageDownloadOperation.completionHandler = { [weak self] (image) in
//                        guard let strongSelf = self else {
//                            return
//                        }
//
//                        DispatchQueue.main.async {
//                            cell.imageView?.image = image
//                        }
//
//                        strongSelf.imageMap[postImageString] = image
//                        strongSelf.imageDownloadOperations.removeValue(forKey: indexPath)
//                    }
//
//                    imageDownloadOperationQueue.addOperation(imageDownloadOperation)
//                    imageDownloadOperations[indexPath] = imageDownloadOperation
//                }
                else {
                    DispatchQueue.global().async {
                        let postImage:UIImage? = imageMethods.uiImage(withServerPathName: postImageString, width: Float(UIScreen.main.bounds.width))
                        DispatchQueue.main.async {
                            if postImage != nil && cell.tag == indexPath.row {
                                self.imageMap[postImageString] = postImage
                                let image = self.imageMap[postImageString]

                                cell.imageView?.image = image
                                cell.setNeedsLayout()
                                cell.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
            
            cell.headerLabel.text = headerText
            cell.contentLabel.text = postText
            cell.contentLabel.numberOfLines = 0
            cell.contentLabel.lineBreakMode = .byWordWrapping
            cell.contentLabel.font = UIFont.systemFont(ofSize: 17)
            
            if cell.contentLabel.text != "" {
                cell.contentLabel.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 13
                cell.contentLabel.sizeToFit()
                cell.contentLabelHeight.constant = cell.contentLabel.height
            } else {
                cell.contentLabelHeight.constant = 0
            }
            
            cell.offerButtonTapHandler = {
                self.offerButtonTapHandler(indexPath: indexPath)
            }
            
            let foodHashtags = self.data[indexPath.row]["food_hashtags"] as? String ?? ""
            let drinkHashtags = self.data[indexPath.row]["drink_hashtags"] as? String ?? ""
            cell.hashtagsTextView?.attributedText = createHashtagsText(foodString: foodHashtags, drinksString: drinkHashtags)
            if cell.hashtagsTextView.text != "" {
                cell.hashtagsTextView?.width = collectionView.width - 5
                cell.hashtagsTextView?.sizeToFit()
                cell.hashtagsTextViewHeight?.constant = (cell.hashtagsTextView?.height)!
            } else {
                cell.hashtagsTextViewHeight?.constant = 0
            }
            
            cell.locationButtonTapHandler = {
                self.getLocationScreenHandler(indexPath: indexPath)
            }
            
            var likesCount = data[indexPath.row]["likes_number"] as? String ?? "0"
            cell.likeButton.setTitle(likesCount, for: .normal)
            
            if let likedPost = data[indexPath.row]["liked_by_me"] as? String {
                if likedPost == "1" {
                    cell.liked = true
                } else {
                    cell.liked = false
                }
            }
            
            cell.likeButtonTapHandler = {
                var likeCount = Int(likesCount)!
                if !cell.liked == true {
                    likeCount += 1
                    self.data[indexPath.row]["liked_by_me"] = "1"
                } else {
                    if likeCount != 0{
                        likeCount -= 1
                    }
                    self.data[indexPath.row]["liked_by_me"] = "0"
                }
                
                likesCount = String(likeCount)
                self.data[indexPath.row]["likes_number"] = likeCount
                cell.likeButton.setTitle(likesCount, for: .normal)
                
                cell.liked = !cell.liked
                
                if let postId = self.data[indexPath.row]["id"] as? String {
                    let post = ["id":postId]
                    _ = AdminAPI.post(url: "ardnd/post/toggle-like/\(postId)", post: post)
                }
            }
            
            cell.shareButtonTapHandler = {
                let text = cell.contentLabel.text ?? ""
                let image = cell.imageView.image ?? UIImage()
                self.shareButtonHandler(text: text, image: image)
            }
            
            return cell
        
        case .gridViewMode:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
}
