//
//  ProfileCollectionViewHeader.swift
//  gestgid
//
//  Created by Vadym Patalakh on 7/23/18.
//  Copyright © 2018 BHM Media Solutions GmbH. All rights reserved.
//

import Foundation

class ProfileCollectionViewHeader: UICollectionReusableView {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var postsLabel: UILabel!
    @IBOutlet weak var offersLabel: UILabel!
    
    @IBOutlet weak var viewOffersButton: UIButton!
    
    @IBOutlet weak var gridViewButton: UIButton!
    @IBOutlet weak var dragViewButton: UIButton!
    
    @IBOutlet weak var borderViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        borderViewHeight.constant = 1 / UIScreen.main.scale
        
        var image = UIImage(named: "grid")
        gridViewButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        image = UIImage(named: "drag")
        dragViewButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal) 
    }
}
