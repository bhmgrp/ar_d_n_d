//
//  MiscFunctions.m
//  if_framework
//
//  Created by User on 02.07.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "miscFunctions.h"

@implementation miscFunctions

+(NSString*)stringByParsingHtmlString:(NSString*)s{

    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)s = [s stringByReplacingCharactersInRange:r withString:@""];

    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    s = [s stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    s = [s stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    s = [s stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];


    //    NSData *stringData = [s dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    NSDictionary *options = @{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
    //    NSAttributedString *decodedString;
    //    decodedString = [[NSAttributedString alloc] initWithData:stringData
    //                                                     options:options
    //                                          documentAttributes:NULL
    //                                                       error:NULL];
    //
    //    return [decodedString string];

    return s;
}

+(NSString*)localizedStringFromDate:(NSDate*)myNSDateInstance onlyDate:(BOOL)onlyDate{
    return [miscFunctions localizedStringFromDate:myNSDateInstance onlyDate:onlyDate onlyTime:NO];
}

+(NSString*)localizedStringFromDate:(NSDate *)myNSDateInstance onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime{
    NSString *stringFromDate = [[self getLocalizedDateFormatterWithOnlyDate:onlyDate onlyTime:onlyTime] stringFromDate:myNSDateInstance];
    return stringFromDate;
}

+ (NSString *)localizedStringFromDate:(NSDate *)myNSDateInstance :(BOOL)onlyDate :(BOOL)onlyTime :(BOOL)onlyMonthAndYear {
    NSString *stringFromDate = [[self getLocalizedDateFormatterWithOnlyDate:onlyDate onlyTime:onlyTime onlyMonthAndYear:onlyMonthAndYear] stringFromDate:myNSDateInstance];
    return stringFromDate;
}


+(NSString*)enStringFromDate:(NSDate *)myNSDateInstance onlyDate:(BOOL)onlyDate{
    return [self enStringFromDate:myNSDateInstance onlyDate:onlyDate onlyTime:NO];
}

+(NSString*)enStringFromDate:(NSDate *)myNSDateInstance onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime{
    NSString *stringFromDate = [[self getLocalizedDateFormatterForLanguageID:1 WithOnlyDate:onlyDate onlyTime:onlyTime onlyMonthAndYear:NO] stringFromDate:myNSDateInstance];
    return stringFromDate;
}

+(NSDate*)dateFromLocalizedString:(NSString*)string onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime{
    return [[self getLocalizedDateFormatterWithOnlyDate:onlyDate onlyTime:onlyTime] dateFromString:string];
}

+(NSDate*)dateFromEnString:(NSString*)string onlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime{
    return [[self getLocalizedDateFormatterForLanguageID:1 WithOnlyDate:onlyDate onlyTime:onlyTime onlyMonthAndYear:NO] dateFromString:string];
}

+(NSDateFormatter*)getLocalizedDateFormatterWithOnlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime{
    return [self getLocalizedDateFormatterForLanguageID:[languages currentLanguageID] WithOnlyDate:onlyDate onlyTime:onlyTime onlyMonthAndYear:NO];
}

+(NSDateFormatter*)getLocalizedDateFormatterWithOnlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime onlyMonthAndYear:(BOOL)onlyMonthAndYear{
    return [self getLocalizedDateFormatterForLanguageID:[languages currentLanguageID] WithOnlyDate:onlyDate onlyTime:onlyTime onlyMonthAndYear:onlyMonthAndYear];
}

+(NSDateFormatter*)getLocalizedDateFormatterForLanguageID:(int) languageID WithOnlyDate:(BOOL)onlyDate onlyTime:(BOOL)onlyTime onlyMonthAndYear:(BOOL)onlyMonthAndYear{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    switch (languageID){
        case 1:{
            if(onlyMonthAndYear){
                formatter.dateFormat = @"yyyy-MM";
            }
            else if (onlyDate) {
                formatter.dateFormat = @"yyyy-MM-dd";
            }
            else if(onlyTime){
                formatter.dateFormat = @"HH:mm";
            }
            else{
                formatter.dateFormat = @"yyyy-MM-dd HH:mm";
            }

            break;
        }
        default:{
            if(onlyMonthAndYear){
                formatter.dateFormat = @"MM.yyyy";
            }
            else if (onlyDate) {
                formatter.dateFormat = @"dd.MM.yyyy";
            }
            else if(onlyTime){
                formatter.dateFormat = @"HH:mm";
            }
            else{
                formatter.dateFormat = @"dd.MM.yyyy HH:mm";
            }

            break;
        }
    }

    return formatter;
}

+(void)showLoadingAnimationForView:(UIView*)view{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable",@"");
}


@end
