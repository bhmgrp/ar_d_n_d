//
//  API.m
//  if_framework
//
//  Created by Julian Böhnke on 16.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "API.h"
#import "Unirest/UNIRest.h"
#import "SSKeychain.h"

@implementation API

- (id) request:(NSString *)url :(NSDictionary *)params {
    
    NSDictionary* headers = @{@"accept": @"application/json"};
    
    UNIHTTPJsonResponse *response = [[UNIRest post:^(UNISimpleRequest *request) {
        [request setUrl: url];
        [request setHeaders: headers];
        [request setParameters: params];
    }] asJson];
    
    return response;
}

+ (id)postToURL:(NSString*)urlString WithPost:(NSDictionary*)postDict {
    
    postDict = [postDict mutableCopy];
    
    [postDict setValuesForKeysWithDictionary:[self authorizationDict]];
    
    NSString *postString = [postDict urlEncodedString];
    
    NSLog(@" api - post, url: %@, dict: %@", urlString, postString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    NSHTTPURLResponse *response = nil;
    
    NSError *error;
    
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSLog(@" api - response: %@", [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding]);
    
    if(error){
        NSLog(@" api - error: %@", error.localizedDescription);
        return @{@"success":@0, @"error":error};
    }
    
    return [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];;
}

+ (id)getFrom:(NSString *)urlString {
    urlString = [NSString stringWithFormat:@"%@/?%@",urlString, [[self authorizationDict] urlEncodedString]];
    
    NSLog(@" api - get: %@", urlString);
    NSError *error;
    
    NSData *urlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    if (urlData != nil){
        
        NSLog(@" api - response: %@", [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding]);
        
        if(error){
            NSLog(@" api - error: %@", error.localizedDescription);
            return @{@"success":@0, @"error":error};
        }
    
        return [NSJSONSerialization JSONObjectWithData:urlData options:0 error:&error];
    }
    else {
        return nil;
    }
}

+ (id)jsonObject:(NSData*)data {
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
}

+ (NSDictionary*)authorizationDict {
    // default values
    NSInteger userID = [[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"];
    
    localStorage *dynamicUserSettingsStorage = [localStorage storageWithFilename:@"dynamicUserSettingsValues"];
    __block NSString *loginName = [dynamicUserSettingsStorage objectForKey:@"email"];
    __block NSString *loginPassword = [SSKeychain passwordForService:@"PickalbatrosLogin" account:loginName];
    NSInteger overSocial = [[dynamicUserSettingsStorage objectForKey:@"overSocial"] intValue];
    NSString *client_id = [dynamicUserSettingsStorage objectForKey:@"client_id"];
    NSString *client = [dynamicUserSettingsStorage objectForKey:@"client"];
        
    if(userID==0 || ([loginName isEmptyString] && !overSocial)){        
        return @{};
    }
    else if([loginPassword isEmptyString] && !overSocial){
        UIAlertController *revalidatePasswordController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Password required", 0)
                                                                                              message:NSLocalizedString(@"Your password is required for authorization. If your password isn't saved after this message, try reinstalling the app.", @"")
                                                                                       preferredStyle:UIAlertControllerStyleAlert];
        
        [revalidatePasswordController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.secureTextEntry = YES;
        }];
        
        [revalidatePasswordController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [revalidatePasswordController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            loginPassword = revalidatePasswordController.textFields[0].text;
            
            [SSKeychain setPassword:loginPassword forService:@"PickalbatrosLogin" account:loginName];
        }]];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController showViewController:revalidatePasswordController sender:nil];
        
        return @{};
    }
    
    NSDictionary *postData;
    
    if (overSocial == NO) {
        
        postData = @{
                     @"customerID": @(CUSTOMERID),
                     @"userID": [@(userID) stringValue],
                     @"validationPassword": loginPassword,
                     @"overSocial": [@(overSocial) stringValue],
//#warning remove!!!
//                     @"debug":@YES
                     };
        
    } else {
        
        postData = @{
                     @"customerID": @(CUSTOMERID),
                     @"userID": [@(userID) stringValue],
                     @"overSocial": [@(overSocial) stringValue],
                     @"client_id": client_id,
                     @"client": client,
//#warning remove!!!
//                     @"debug":@YES
                     };
    }
    
    return postData;
}

@end
