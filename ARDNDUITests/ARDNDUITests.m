//
//  ARDNDUITests.m
//  ARDNDUITests
//
//  Created by Christopher on 9/28/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ARDNDUITests-Swift.h"

@interface ARDNDUITests : XCTestCase

@end

@implementation ARDNDUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication *app = [[XCUIApplication alloc] init];
    //app.launchArguments = @[@"isUITesting"];
    [app launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (BOOL)login{
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    
    XCUIElement *loginButton = app.buttons[@"Have an account? Log in"];
    
    if(loginButton.exists){
        
        [app.buttons[@"Have an account? Log in"] tap];
        
        XCUIElement *emailAddressTextField = app.textFields[@"Email address"];
        [emailAddressTextField tap];
        [emailAddressTextField typeText:@"christopher.wulff@bhmgrp.com"];
        
        XCUIElement *passwordSecureTextField = app.secureTextFields[@"Password"];
        [passwordSecureTextField tap];
        [passwordSecureTextField typeText:@"testPW"];
        
        [app.buttons[@"Login"] tap];
    }
    
    
    return YES;
}

- (void)testScreenshots {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    //[app.buttons[@"Register"] tap];
    //[app.navigationBars[@"Registration"].buttons[@"Back"] tap];
    //[app.buttons[@"Proceed without registration"] tap];
    //[app.alerts.buttons[@"OK"] tap];
    
    [Snapshot setupSnapshot:app];
    [app launch];
    
    if([self login]){
        
        [app.staticTexts[@"Find your host ..."] tap];
        
        [Snapshot snapshot:@"overview" waitForLoadingIndicator:YES];
        
        [app.buttons[@"Cancel"] tap];
        
        
        XCUIElementQuery *tabBarsQuery = app.tabBars;
        
        [tabBarsQuery.buttons[@"Requests"] tap];
        [Snapshot snapshot:@"requests" waitForLoadingIndicator:YES];
        
        [tabBarsQuery.buttons[@"Profile"] tap];
        [app.sheets.buttons[@"Guest"] tap];
        
        [Snapshot snapshot:@"profile" waitForLoadingIndicator:YES];
    }
}

- (void)testExample {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

@end
